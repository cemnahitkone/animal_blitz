﻿//#define TEST_TESTBLE

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class testBle : MonoBehaviour
{
	private static string _connectedID = null;
	private static string _serviceUUID = "FFE0";
	private static string _writeCharacteristicUUID = "FFE1";
	private string _readCharacteristicUUID = "FFE1";//fff4 eski versiyonlarda
	private string _address = ""; 
	private string _name = "TabToy06"; 
	private static string BleDataDone = "";
	private string BleDataNew = "";
	private bool dataState = false;
	private List<baseLegID> idList ;
	private static int _uniqID=0;
#if TEST_TESTBLE
	private string debugData1 = " ";
	private string debugData2 = " ";
	private string debugData3 = " ";
	private byte[] bytetest = new byte[]{2, 222, 223, 3, 132, 111,22,122,123,23,124,125} ;
#endif

	// Use this for initialization
	private class baseLegID
	{
		public int id;
		public int x;
		public int y;
	}
	
	void Start ()
	{
		BluetoothLEHardwareInterface.Initialize (true, false, () => { 
			BluetoothLEHardwareInterface.Log ("Initialize Done");
#if TEST_TESTBLE
			debugData1 = "Initialize Done";
#endif
			SearchAndConnectDevice ();
		}, (error) => {
			BluetoothLEHardwareInterface.Log ("Initialize Error");
#if TEST_TESTBLE
			debugData1 = "Initialize Error";
#endif
		});		
	}
	private void Connect ()
	{
		//address yerine isim yazilarak test edilecek
		BluetoothLEHardwareInterface.ConnectToPeripheral (_address, (address) => {
			BluetoothLEHardwareInterface.Log ("Device Connected");
#if TEST_TESTBLE
			debugData1 = "Device Connected";
			debugData2 = " ";
#endif
		},
		(address, serviceUUID) => {
		},
		(address, serviceUUID, characteristicUUID) => {
			if (IsEqual (serviceUUID, _serviceUUID)) {
				_connectedID = address;
				if (IsEqual (characteristicUUID, _readCharacteristicUUID)) {
					BluetoothLEHardwareInterface.SubscribeCharacteristicWithDeviceAddress (_connectedID, _serviceUUID, _readCharacteristicUUID, (deviceAddress, notification) => {
					}, (deviceAddress2, characteristic, data) => {
						if (deviceAddress2.CompareTo (_connectedID) == 0) {
							if (IsEqual (characteristicUUID, _readCharacteristicUUID)) {
								BleDataNew += changeDataFormat (data);	
								if (dataState == true) {
									BleDataDone = BleDataNew;
									dataState = false;
									BleDataNew = "";
									_uniqID++;
								}
#if TEST_TESTBLE
								debugData2 = BleDataDone;
#endif
							}
						}
					});
				}
			}
		}, (address) => {
			BluetoothLEHardwareInterface.Log ("Device Disconnect");
#if TEST_TESTBLE
			debugData1 = "Device Disconnected";
#endif
			SearchAndConnectDevice ();
			// this will get called when the device disconnects
			// be aware that this will also get called when the disconnect
			// is called above. both methods get call for the same action
			// this is for backwards compatibility;
		});
		
	}
	
	
	private bool IsEqual (string uuid1, string uuid2)
	{
		return (uuid1.ToUpper ().CompareTo (uuid2.ToUpper ()) == 0);
	}
	private void SearchAndConnectDevice ()
	{
#if TEST_TESTBLE			
		debugData2 = "Device searching ";
#endif
		BluetoothLEHardwareInterface.ScanForPeripheralsWithServices (null, (address, name) => {
			if (name == _name) {
				_address = address;
				BluetoothLEHardwareInterface.StopScan ();
				Connect ();
			}
		}, (address, name, rssi, advertisingInfo) => {
			
			if (advertisingInfo != null)
				BluetoothLEHardwareInterface.Log (string.Format ("Device: {0} RSSI: {1} Data Length: {2} Bytes: {3}", name, rssi, advertisingInfo.Length, System.Text.Encoding.UTF8.GetString (advertisingInfo)));
		});
	}
	// Update is called once per frame
	public static string getBleData ()
	{
		return BleDataDone;	
	}
	public static int uniqId 
	{
		get {return _uniqID;}
	}
	private string changeDataFormat (byte[] data)
	{
		string pData = "";
		int counter = 0;
		idList = new List<baseLegID> ();
		var baseLeg = new baseLegID ();
		foreach (byte c in data) {
			if (c == 0xFF) {
				dataState = true;
			} else {
				counter++;
				switch (counter) {
				case 1: 
					baseLeg.id = (int)c;
					break;
				case 2:
					baseLeg.x = ((int)c) * 6;
					break;
				case 3: 
					baseLeg.y = ((int)c) * 4;
					idList.Add (baseLeg);
					counter = 0;
					baseLeg = new baseLegID ();
					break;
				default:
					counter = 0;
					break;
				} 	
			}
		}
		bool id_flag=false;
		foreach (baseLegID bId in idList) {
			id_flag=false;
			if ((bId.id % 2) == 0) {
				foreach (baseLegID bNextId in idList) {
					if ((bId.id + 1) == bNextId.id) {
						pData += (bId.id.ToString () + " " + (bId.x).ToString () + " " + bId.y.ToString () + "-");
						id_flag=true;
					}
				}
				if(id_flag==false)
					pData += (bId.id.ToString () + " " + (bId.x).ToString () + " " + bId.y.ToString () );

			} else {
				pData += (bId.id.ToString () + " " + bId.x.ToString () + " " + bId.y.ToString () + "#");
			}
		}
		if (dataState == true && pData == "")
			pData += "#";
		return pData;
	}
	public void SendBytes (byte[] data)
	{
		BluetoothLEHardwareInterface.WriteCharacteristic (_connectedID, _serviceUUID, _writeCharacteristicUUID ,data, data.Length, true, (characteristicUUID) => {
			Debug.Log ("Bytes sent");
		});
	}
	public static void SendString (string s)
	{
		byte[] data = System.Text.Encoding.Default.GetBytes(s);
		BluetoothLEHardwareInterface.WriteCharacteristic (_connectedID, _serviceUUID, _writeCharacteristicUUID ,data, data.Length, true, (characteristicUUID) => {
			Debug.Log ("String sent");
		});
	}
	void Update ()
	{
		/*
	 	BleDataNew += changeDataFormat (bytetest);	
		if (dataState == true) {
			BleDataDone = BleDataNew;
			dataState = false;
			BleDataNew = "";
		}
		debugData2 = BleDataDone;	
	*/
	}
#if TEST_TESTBLE
	void OnGUI ()
	{
		if(GUILayout.Button ("id size  " + TabBase.GetCount())){

		}
		GUILayout.TextArea (debugData1);
		GUILayout.TextArea (debugData2);
		GUILayout.TextArea (debugData3);
		GUILayout.TextArea (_address);
	}
#endif
}
