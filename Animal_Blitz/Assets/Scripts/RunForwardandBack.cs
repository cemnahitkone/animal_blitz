﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunForwardandBack : MonoBehaviour {

	public float speed = 3;
	private Vector3 right;
	private Vector3 left;
	// Use this for initialization
	void Start () {
		right = new Vector3(0,-90,0);
		left = new Vector3(0,90,0);
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log(transform.rotation.eulerAngles.y);

		if(this.name == "Cat"){
			if(transform.position.x < -6 && transform.rotation.eulerAngles.y == 270)
				transform.eulerAngles = new Vector3(0, 90, 0);
			else if(transform.position.x > 5 && transform.rotation.eulerAngles.y >= 90)
				transform.eulerAngles = new Vector3(0, -90, 0);
		}

		if(this.name == "Turtle"){
			if(transform.position.x < -3 && transform.rotation.eulerAngles.y == 270)
				transform.eulerAngles = new Vector3(0, 90, 0);
			else if(transform.position.x > 1.7 && transform.rotation.eulerAngles.y >= 90)
				transform.eulerAngles = new Vector3(0, -90, 0);
		}

		if(this.name == "Rat"){
			if(transform.position.x < -3 && transform.rotation.eulerAngles.y == 270)
				transform.eulerAngles = new Vector3(0, 90, 0);
			else if(transform.position.x > 3 && transform.rotation.eulerAngles.y >= 90)
				transform.eulerAngles = new Vector3(0, -90, 0);
		}

		transform.Translate(Vector3.forward * Time.deltaTime * speed);
	}
}
