﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDetection : MonoBehaviour {

	public GameObject gameCo;
	// Use this for initialization
	void Start () {
		gameCo = GameObject.Find("GameController");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other) {
		Debug.Log("collision: "+other.name);
		if(this.name == "P1Avatar")
			gameCo.GetComponent<IconControl>().pl1collider = other.name;
		if(this.name == "P2Avatar")
			gameCo.GetComponent<IconControl>().pl2collider = other.name;
		gameCo.GetComponent<IconControl>().checkAnswer();

	}

	void OnTriggerExit2D(Collider2D other) {
		//Debug.Log("collision: "+other.name);
		if(this.name == "P1Avatar")
			gameCo.GetComponent<IconControl>().pl1collider = null;
		if(this.name == "P2Avatar")
			gameCo.GetComponent<IconControl>().pl2collider = null;
	}


}
