﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using UnityEngine.UI;
using System;

public class IconControl : MonoBehaviour {

	public Dictionary<int, tBase> _base;
	public GameObject EventSys;

	public GameObject catIcon;
	public bool catDestroyed;
	public float catSpeed;

	public GameObject ratIcon;
	public bool ratDestroyed;
	public float ratSpeed;

	public GameObject dogIcon;
	public bool dogDestroyed;
	public float dogSpeed;

	public GameObject owlIcon;
	public bool owlDestroyed;
	public float owlSpeed;

	public GameObject turtleIcon;
	public bool turtleDestroyed;
	public float turtleSpeed;

	public GameObject Player2Score;
	public GameObject Player1Score;

	public GameObject pointSound;
	public GameObject failSound;

	Vector3 dogLastPosition = Vector3.zero;
	Vector3 catLastPosition = Vector3.zero;
	Vector3 owlLastPosition = Vector3.zero;
	Vector3 ratLastPosition = Vector3.zero;
	Vector3 turtleLastPosition = Vector3.zero;

	public string pl1collider;
	public string pl2collider;

	public bool success;
	// Use this for initialization
	void Start () {
		Player1Score.GetComponent<Text>().text = "Score: "+PlayerPrefs.GetInt("Player1Score").ToString();
		Player2Score.GetComponent<Text>().text = "Score: "+PlayerPrefs.GetInt("Player2Score").ToString();

		dogDestroyed = true;
		catDestroyed = true;
		ratDestroyed = true;
		owlDestroyed = true;
		turtleDestroyed = true;

		catSpeed = 0.0f;
		dogSpeed = 0.0f;
		owlSpeed = 0.0f;
		ratSpeed = 0.0f;
		turtleSpeed = 0.0f;

		pl1collider = null;
		pl2collider = null;

		dogLastPosition = Vector3.zero;
		//dogIcon.transform.position = Vector3.zero;
		catLastPosition = Vector3.zero;
		//catIcon.transform.position = Vector3.zero;
		owlLastPosition = Vector3.zero;
		//owlIcon.transform.position = Vector3.zero;
		ratLastPosition = Vector3.zero;
		//ratIcon.transform.position = Vector3.zero;
		turtleLastPosition = Vector3.zero;
		//turtleIcon.transform.position = Vector3.zero;

		success = false;
		InvokeRepeating("calculateSpeeds",0.5f,0.2f);
	}
	
	// Update is called once per frame


	void calculateSpeeds(){
		dogSpeed = (dogIcon.transform.position - dogLastPosition).magnitude;
		dogLastPosition = dogIcon.transform.position;
		if(dogSpeed < 15.0f){
			Invoke("destroyDog",0.5f);
		}
		else{
			dogDestroyed = false;
			dogIcon.GetComponent<Image>().enabled = true;
		}

		catSpeed = (catIcon.transform.position - catLastPosition).magnitude;
		catLastPosition = catIcon.transform.position;
		if(catSpeed < 15.0f){
			Invoke("destroyCat",0.5f);
		}
		else{
			catDestroyed = false;
			catIcon.GetComponent<Image>().enabled = true;
		}

		ratSpeed = (ratIcon.transform.position - ratLastPosition).magnitude;
		ratLastPosition = ratIcon.transform.position;
		if(ratSpeed < 15.0f){
			Invoke("destroyRat",0.5f);
		}
		else{
			ratDestroyed = false;
			ratIcon.GetComponent<Image>().enabled = true;
		}

		owlSpeed = (owlIcon.transform.position - owlLastPosition).magnitude;
		owlLastPosition = owlIcon.transform.position;
		if(owlSpeed < 15.0f){
			Invoke("destroyOwl",0.5f);
		}
		else{
			owlDestroyed = false;
			owlIcon.GetComponent<Image>().enabled = true;
		}

		turtleSpeed = (turtleIcon.transform.position - turtleLastPosition).magnitude;
		turtleLastPosition = turtleIcon.transform.position;
		if(turtleSpeed < 15.0f){
			Invoke("destroyTurtle",0.5f);
		}
		else{
			turtleDestroyed = false;
			turtleIcon.GetComponent<Image>().enabled = true;
		}
	}

	void Update () {
		
		if(TabBase.checkDataState()){
			_base = TabBase.GetBase();

			//--------------------------------------------------- DOG ----------------------------------------------------//
			if(_base[1].state){
				float xxx, zzz;
				xxx = ((Mathf.CeilToInt(_base[1].x) - 60)*Screen.width)/1300;
				zzz = ((Mathf.CeilToInt(_base[1].y) - 60)*Screen.height)/800;

//				if(dogDestroyed){
//					dogDestroyed = false;
//					dogIcon.SetActive(true);
//				}

				dogIcon.transform.position = Vector3.Lerp (dogIcon.transform.position, new Vector3(xxx,zzz,0), 6 * Time.deltaTime);
			}
			else{
				if(!dogDestroyed){
						destroyDog();
				}
			}
			//--------------------------------------------------- CAT ----------------------------------------------------//
			if(_base[6].state){
				float xxx, zzz;
				xxx = ((Mathf.CeilToInt(_base[6].x) - 60)*Screen.width)/1300;
				zzz = ((Mathf.CeilToInt(_base[6].y) - 60)*Screen.height)/800;

//				if(catDestroyed){
//					catDestroyed = false;
//					catIcon.SetActive(true);
//				}

				catIcon.transform.position = Vector3.Lerp (catIcon.transform.position, new Vector3(xxx,zzz,0), 6 * Time.deltaTime);
			}
			else{
				if(!catDestroyed){
					destroyCat();
				}
			}
			//--------------------------------------------------- RAT ----------------------------------------------------//
			if(_base[4].state){
				float xxx, zzz;
				xxx = ((Mathf.CeilToInt(_base[4].x) - 60)*Screen.width)/1300;
				zzz = ((Mathf.CeilToInt(_base[4].y) - 60)*Screen.height)/800;

//				if(ratDestroyed){
//					ratDestroyed = false;
//					ratIcon.SetActive(true);
//				}

				ratIcon.transform.position = Vector3.Lerp (ratIcon.transform.position, new Vector3(xxx,zzz,0), 6 * Time.deltaTime);
			}
			else{
				if(!ratDestroyed){
					destroyRat();
				}
			}
			//--------------------------------------------------- OWL ----------------------------------------------------//
			if(_base[5].state){
				float xxx, zzz;
				xxx = ((Mathf.CeilToInt(_base[5].x) - 60)*Screen.width)/1300;
				zzz = ((Mathf.CeilToInt(_base[5].y) - 60)*Screen.height)/800;

//				if(owlDestroyed){
//					owlDestroyed = false;
//					owlIcon.SetActive(true);
//				}

				owlIcon.transform.position = Vector3.Lerp (owlIcon.transform.position, new Vector3(xxx,zzz,0), 6 * Time.deltaTime);
			}
			else{
				if(!owlDestroyed){
					destroyOwl();
				}
			}
			//--------------------------------------------------- TURTLE ----------------------------------------------------//
			if(_base[2].state){
				float xxx, zzz;
				xxx = ((Mathf.CeilToInt(_base[2].x) - 60)*Screen.width)/1300;
				zzz = ((Mathf.CeilToInt(_base[2].y) - 60)*Screen.height)/800;

//				if(turtleDestroyed){
//					turtleDestroyed = false;
//					turtleIcon.SetActive(true);
//				}

				turtleIcon.transform.position = Vector3.Lerp (turtleIcon.transform.position, new Vector3(xxx,zzz,0), 6 * Time.deltaTime);
			}
			else{
				if(!turtleDestroyed){
					destroyTurtle();
				}
			}
				
		}
	}

	private string correctAnswer;

	public void checkAnswer(){
		
		if(Application.loadedLevel == 2){
			correctAnswer = "TurtleIcon";
		}else if(Application.loadedLevel == 3){
			correctAnswer = "DogIcon";
		}else if(Application.loadedLevel == 4){
			correctAnswer = "DogIcon";
		}else if(Application.loadedLevel == 5){
			correctAnswer = "RatIcon";
		}else if(Application.loadedLevel == 6){
			correctAnswer = "DogIcon";
		}else if(Application.loadedLevel == 7){
			correctAnswer = "DogIcon";
		}else if(Application.loadedLevel == 8){
			correctAnswer = "OwlIcon";
		}else if(Application.loadedLevel == 9){
			correctAnswer = "DogIcon";
		}else if(Application.loadedLevel == 10){
			correctAnswer = "TurtleIcon";
		}else if(Application.loadedLevel == 11){
			correctAnswer = "CatIcon";
		}else if(Application.loadedLevel == 12){
			correctAnswer = "RatIcon";
		}else if(Application.loadedLevel == 13){
			correctAnswer = "TurtleIcon";
		}else if(Application.loadedLevel == 14){
			correctAnswer = "RatIcon";
		}else if(Application.loadedLevel == 15){
			correctAnswer = "OwlIcon";
		}else if(Application.loadedLevel == 16){
			correctAnswer = "RatIcon";
		}else if(Application.loadedLevel == 17){
			correctAnswer = "TurtleIcon";
		}else if(Application.loadedLevel == 18){
			correctAnswer = "CatIcon";
		}else if(Application.loadedLevel == 19){
			correctAnswer = "CatIcon";
		}else if(Application.loadedLevel == 20){
			correctAnswer = "CatIcon";
		}else if(Application.loadedLevel == 21){
			correctAnswer = "OwlIcon";
		}else if(Application.loadedLevel == 22){
			correctAnswer = "RatIcon";
		}

		Debug.Log("Loaded level: "+Application.loadedLevel);
		Debug.Log("correct answer: "+correctAnswer);
		Debug.Log("pl2 collider: "+pl2collider);

		if(pl2collider != null && pl2collider == correctAnswer && ! success){
			PlayerPrefs.SetInt("Player2Score",PlayerPrefs.GetInt("Player2Score")+1);
			Player2Score.GetComponent<Text>().text = "Score: "+PlayerPrefs.GetInt("Player2Score").ToString();
			success = true;
			pointSound.GetComponent<AudioSource>().Play();
			Invoke("finishLevel",2.0f);
		}else if(pl1collider != null && pl1collider == correctAnswer && ! success){
			PlayerPrefs.SetInt("Player1Score",PlayerPrefs.GetInt("Player1Score")+1);
			Player1Score.GetComponent<Text>().text = "Score: "+PlayerPrefs.GetInt("Player1Score").ToString();
			success = true;
			pointSound.GetComponent<AudioSource>().Play();
			Invoke("finishLevel",2.0f);
		}else if(pl2collider != null && pl2collider != correctAnswer && PlayerPrefs.GetInt("Player2Score")>0 && ! success){
			PlayerPrefs.SetInt("Player2Score",PlayerPrefs.GetInt("Player2Score")-1);
			Player2Score.GetComponent<Text>().text = "Score: "+PlayerPrefs.GetInt("Player2Score").ToString();
			failSound.GetComponent<AudioSource>().Play();
		}else if(pl1collider != null && pl1collider != correctAnswer && PlayerPrefs.GetInt("Player1Score")>0 && ! success){
			PlayerPrefs.SetInt("Player1Score",PlayerPrefs.GetInt("Player1Score")-1);
			Player1Score.GetComponent<Text>().text = "Score: "+PlayerPrefs.GetInt("Player1Score").ToString();
			failSound.GetComponent<AudioSource>().Play();
		}
	}

	void finishLevel(){
		CancelInvoke("finishLevel");
		EventSys.GetComponent<UIActions>().closeCurtains();
	}

	void destroyDog(){
		dogDestroyed = true;
		//Destroy(DogCharacter);
		dogIcon.GetComponent<Image>().enabled = false;
	}

	void destroyCat(){
		catDestroyed = true;
		//Destroy(DogCharacter);
		catIcon.GetComponent<Image>().enabled = false;
	}

	void destroyOwl(){
		owlDestroyed = true;
		//Destroy(DogCharacter);
		owlIcon.GetComponent<Image>().enabled = false;
	}

	void destroyRat(){
		ratDestroyed = true;
		//Destroy(DogCharacter);
		ratIcon.GetComponent<Image>().enabled = false;
	}

	void destroyTurtle(){
		turtleDestroyed = true;
		//Destroy(DogCharacter);
		turtleIcon.GetComponent<Image>().enabled = false;
	}
	
	}

	
