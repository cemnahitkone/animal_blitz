#define DEBUG_ONGUI

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System;
/*
// 
2 322 32-3 3342 324 532#4 432 43-5 43 345# // data format
tabtoydan gelen dataler tabBack data ya aktarilacak oradan da tabbackData daki counter durumuna gore tabdata ya aktarilacak.
tek id yoksa sistem id yi silmeyecek 
silme ve ekleme isi icin bir counter olacak.
10 tane id create etme fonk yaz.
eger 1 tanesn gormuyorsa update etme 
yeni fonksiyon var (rate i arttırma ve azaltma)

*/
public class TabBase : MonoBehaviour 
{
	public int idSize = 11;
	public int addBaseCounter = 5;
	public int removeBaseCounter = 5;
	public int PosAverageCounter =2;
	private static Dictionary<int, tBase > tabData;
	private Dictionary<int, tabID> tabIdData;
	private static Boolean dataState = false;
	private static string testID; 
	private static int _uniqID=0;
	public static int switch_state1 = 0;
	public static int switch_state5 = 0;
	private int buttonCounter;
	private class baseLegID
	{
		public int id;
		public int x;
		public int y;
	}
	private Dictionary<int,baseLegID> idDictionary;
	private List<baseLegID> idList;
	#if UNITY_EDITOR
	static SerialPort com1;
	private string BleDataNew = "";
	private bool b_dataState = false;
		
	private int comCounterError =0;
	private static System.Timers.Timer aTimer;
	private static int speedTest=0;
	private static int speedTestStatic=0;
	#elif UNITY_IOS
	private int testBleLastUniqId =0;
	#endif 
	string testData ;
	private class tabID
	{
		public tBase getPos = new tBase ();
		public int id;
		public Boolean changedData;
		public int addCounter;
		public int removeCounter;
		public int posAverageCounter;
	}
	void Start ()
	{
		#if UNITY_EDITOR 
		com1 = new SerialPort ();
		com1.PortName = "/dev/cu.usbserial";//"COM3";// "/dev/cu.HC-05-DevB";
		com1.BaudRate = 115200;
		OpenConnection ();
		//SendData("33333");
		//com1.Close();
		//com1.BaudRate=57600;
		//OpenConnection();
		
		aTimer = new System.Timers.Timer(10000);
		
		// Hook up the Elapsed event for the timer. 
		aTimer.Elapsed += OnTimedEvent;
		
		// Have the timer fire repeated events (true is the default)
		aTimer.AutoReset = true;
		
		// Start the timer
		aTimer.Enabled = true;
		
		#elif UNITY_ANDROID
		try {
			BtConnection.moduleName ("HC-05");
			BtConnection.connect ();
		} catch (Exception ex) {
			
		}
		
		#endif
		tabData = new Dictionary<int, tBase> ();
		tabIdData = new Dictionary<int, tabID> ();
		idDictionary = new Dictionary<int, baseLegID> ();
		CreateId ();	
		
	}
	
	void Update ()
	{ 
		string data = "";
		try {
			#if UNITY_EDITOR    
			data = null;
			byte [] b_data = new byte[500];  
			if(com1 != null && com1.BytesToRead==0){
				comCounterError++;
				if(comCounterError>10){			
					comCounterError=0;
					com1.Close();
					com1.Open();
					Debug.Log("port reset1");
				}
			}else comCounterError=0;
			
			if ( com1 != null && com1.BytesToRead<500 )
			{
				com1.Read(b_data,0,com1.BytesToRead);
				BleDataNew += changeDataFormat(b_data);
				if (b_dataState == true)
				{
					speedTest++;
					data = BleDataNew;
					b_dataState = false;
					BleDataNew = "";
				}
			}else  {
				com1.Close();
				com1.Open();
				Debug.Log("port reset2");
			}
			#elif UNITY_ANDROID
			data = BtConnection.read();
			#elif UNITY_IOS
			if(testBle.uniqId != testBleLastUniqId){
				testBleLastUniqId=testBle.uniqId;
				data= testBle.getBleData();
			}
			#endif
			//Debug.Log (data);
		} catch (TimeoutException) {
			//Debug.Log("test4");
		}
		if (!String.IsNullOrEmpty (data)) {
			var lst = data.Split ('#');
			//  Debug.Log (lst.Length.ToString ());
			testData = data;
			foreach (var d in tabIdData) {
				d.Value.changedData = false;
			}
			int id1, id2, x1, x2, y2, y1;
			foreach (var s in lst) {
				tabID base1 = new tabID ();
				var leds = s.Split ('-');
				if (leds.Length == 2) {
					var c_id1 = leds [0].Split (' ');
					var c_id2 = leds [1].Split (' ');
					if (c_id1.Length == 3 && c_id2.Length == 3) {// id x y
						if (Int32.TryParse (c_id1 [0], out id1) && Int32.TryParse (c_id1 [1], out x1) && Int32.TryParse (c_id1 [2], out y1) && Int32.TryParse (c_id2 [0], out id2) && Int32.TryParse (c_id2 [1], out x2) && Int32.TryParse (c_id2 [2], out y2)) {
							base1.id = id1 / 2; //id numarasini sirali yapmak icin
							if (tabIdData.ContainsKey (base1.id)) {

								if(base1.id==1){
									switch_state1=0;
									buttonCounter = 0;
//									Debug.Log("switch1: "+switch_state1);
								}
								if(base1.id==5){
									switch_state5=0;
									//Debug.Log("switch5: "+switch_state5);
								}

								base1.getPos.x = (x1 + x2) / 2; 
								base1.getPos.y = (y1 + y2) / 2;
								
								int x_pos = x1 - x2; 
								int y_pos = y1 - y2;
								base1.getPos.rotation = Math.Abs (Convert.ToSingle (x_pos)) / Math.Abs (Convert.ToSingle (y_pos));
								base1.getPos.rotation = Convert.ToSingle (Math.Atan (base1.getPos.rotation));
								base1.getPos.rotation = Convert.ToSingle (base1.getPos.rotation * (180 / Math.PI));
								//debugData2="angle = " +Convert.ToString(newCube.rotation);
								if (y_pos > 0 && x_pos >= 0) {
									base1.getPos.rotation = 360 - base1.getPos.rotation;
								} else if (y_pos >= 0 && x_pos < 0) {
									base1.getPos.rotation += 0;
								} else if (y_pos < 0 && x_pos < 0) {
									base1.getPos.rotation = 180 - base1.getPos.rotation;
								} else if (y_pos <= 0 && x_pos >= 0) {
									base1.getPos.rotation = 180 + base1.getPos.rotation;
								}
								base1.getPos.rotation = (-base1.getPos.rotation);
								
								if (tabIdData.ContainsKey (base1.id)) {
									tabIdData [base1.id].id = base1.id;
									tabIdData [base1.id].getPos.x += base1.getPos.x;
									tabIdData [base1.id].getPos.y += base1.getPos.y;
									tabIdData [base1.id].getPos.rotation = base1.getPos.rotation;
									tabIdData [base1.id].getPos.state = base1.getPos.state;
									tabIdData [base1.id].addCounter += 1;
									tabIdData [base1.id].removeCounter = 0;
									tabIdData [base1.id].changedData = true; 
									tabIdData [base1.id].posAverageCounter += 1;
								}
							}
						}
					}
				} else if (leds.Length == 1) {
					var c_id1 = leds [0].Split (' ');
					if (c_id1.Length == 3) {// id x y
						if (Int32.TryParse (c_id1 [0], out id1) && Int32.TryParse (c_id1 [1], out x1) && Int32.TryParse (c_id1 [2], out y1)) {
							if(id1==25 || id1==23){
								int button;  
								if(id1==25)
									button=3;
									else 
									button=11;
								if(idDictionary.ContainsKey(button)){
									base1.id=idDictionary[button].id/2;
									var x22 = idDictionary[button].x ;
									var y22 = idDictionary[button].y;
									if (tabIdData.ContainsKey (base1.id)) {
										base1.getPos.x = (x1 + x22) / 2; 
										base1.getPos.y = (y1 + y22) / 2;
			Debug.Log("x1 " + x1 + "  y1 " + y1+"  x22 "+x22 +"  y22 " +y22 );
			Debug.Log("x1 " + base1.getPos.x + "  y1 " + base1.getPos.y);
										int x_pos = x1 - x22; 
										int y_pos = y1 - y22;
										base1.getPos.rotation = Math.Abs (Convert.ToSingle (x_pos)) / Math.Abs (Convert.ToSingle (y_pos));
										base1.getPos.rotation = Convert.ToSingle (Math.Atan (base1.getPos.rotation));
										base1.getPos.rotation = Convert.ToSingle (base1.getPos.rotation * (180 / Math.PI));
										//debugData2="angle = " +Convert.ToString(newCube.rotation);
										if (y_pos > 0 && x_pos >= 0) {
											base1.getPos.rotation = 360 - base1.getPos.rotation;
										} else if (y_pos >= 0 && x_pos < 0) {
											base1.getPos.rotation += 0;
										} else if (y_pos < 0 && x_pos < 0) {
											base1.getPos.rotation = 180 - base1.getPos.rotation;
										} else if (y_pos <= 0 && x_pos >= 0) {
											base1.getPos.rotation = 180 + base1.getPos.rotation;
										}
										base1.getPos.rotation = (-base1.getPos.rotation);
										
										if (tabIdData.ContainsKey (base1.id)) {
											tabIdData [base1.id].id = base1.id;
											tabIdData [base1.id].getPos.x += base1.getPos.x;
											tabIdData [base1.id].getPos.y += base1.getPos.y;
											tabIdData [base1.id].getPos.rotation = base1.getPos.rotation;
											tabIdData [base1.id].getPos.state = base1.getPos.state;
											tabIdData [base1.id].addCounter += 1;
											tabIdData [base1.id].removeCounter = 0;
											tabIdData [base1.id].changedData = true; 
											tabIdData [base1.id].posAverageCounter += 1;
										}
									}
								}
								if(id1==25){
									buttonCounter++;
									if(buttonCounter == 2)
										switch_state1=1;
									Debug.Log("switch1: "+switch_state1);
								}
								if(id1==23){
									switch_state5=1;
									Debug.Log("switch5: "+switch_state5);
								}
								
							}
							else{
							base1.id = id1 / 2; //id numarasini sirali yapmak icin 
								if (tabIdData.ContainsKey (base1.id)) {
									if(id1==3){
										if(idDictionary.ContainsKey(3)){
											idDictionary[3].x=x1;
											idDictionary[3].y=y1;  
										}else{
											baseLegID bId = new baseLegID ();
											bId.id = id1;
											bId.x = x1;
											bId.y = y1;
											idDictionary.Add (id1, bId); 
										}

									}
									if(id1==11){
										if(idDictionary.ContainsKey(11)){
											idDictionary[11].x=x1;
											idDictionary[11].y=y1;  
										}else{
											baseLegID bId = new baseLegID ();
											bId.id = id1;
											bId.x = x1;
											bId.y = y1;
											idDictionary.Add (id1, bId); 
										}

								}
									if (tabIdData [base1.id].getPos.state == false) {
										tabIdData [base1.id].getPos.x += x1;
										tabIdData [base1.id].getPos.y += y1;	
										//tabIdData [base1.id].posAverageCounter += 1;
									}
									//tabIdData [base1.id].getPos.x += x1;
									//tabIdData [base1.id].getPos.y += y1;	
									//tabIdData [base1.id].posAverageCounter += 1;
									tabIdData [base1.id].changedData = true; 
									tabIdData [base1.id].addCounter += 1;
									tabIdData [base1.id].removeCounter = 0;
									
								}
							}
						}
					}
				}
			}
			// TODO bu noktaya tabdata nin duzgun gitmesi icin switch konabilir test etmek lazim
			foreach (var d in tabIdData) {
				//Debug.Log ("state21  "+d.Value.id+" : "+d.Value.getPos.state);
				if (d.Value.changedData == false ) {
					if (removeBaseCounter< d.Value.removeCounter ) {
						d.Value.getPos.state = false;
						tabData [d.Value.id].state = d.Value.getPos.state;
					}
					d.Value.addCounter = 0;
					d.Value.removeCounter += 1;      
				} else if (d.Value.addCounter >= addBaseCounter) { 
					d.Value.getPos.state = true;
					tabData [d.Value.id].state = d.Value.getPos.state;
				}
				if(d.Value.posAverageCounter>=PosAverageCounter){
					tabData [d.Value.id].x = d.Value.getPos.x/PosAverageCounter;
					tabData [d.Value.id].y = d.Value.getPos.y/PosAverageCounter;
					tabData [d.Value.id].rotation = d.Value.getPos.rotation;
					
					d.Value.getPos.x=0;
					d.Value.getPos.y=0;
					d.Value.getPos.rotation=0;
					d.Value.posAverageCounter=0;
				}
			}
			dataState = true;
			//Debug.Log("data state true");
			_uniqID++;
		}
	}
	public static Boolean checkDataState ()
	{
		return dataState;
	}
	public static Boolean getDataState ()
	{
		return dataState;
	}
	public static int uniqId 
	{
		get {return _uniqID;}
	}

//			public static int switchState(){
//			return switch_state;
//			}
	private static int lastUniqId=0;
	private static int lastCount=0;
	private static string lastTestId="";
	public static int GetCount(){ // cihazın üzerindeki base sayısını verir. // TODO bu fonksiyonun içini değiştir.
		if(lastUniqId != _uniqID){
			lastUniqId = _uniqID;
			var BaseData = new Dictionary<int, tBase> ();
			BaseData = tabData;
			int count = 0;
			testID = "#";
			if (BaseData != null) {
				foreach (var bd in BaseData) {
					
					if (bd.Value.state == true) {
						count++;
						testID = testID + " " + bd.Key+ " " + bd.Value.x + " " + bd.Value.y + " " + ((int)bd.Value.rotation) + "//";
					}
				}
			}
			lastTestId = testID;
			lastCount = count;
		}
		return lastCount;
	}
	public static Dictionary<int, tBase> GetBase ()
	{
		//dataState = false;
			//Debug.Log("data state false");
		return tabData;
	}
	
	#if UNITY_EDITOR
	private void OpenConnection ()
	{
		
		if (com1 != null) {
			if (com1.IsOpen) {
				com1.Close ();
				Debug.Log ("Closing port, because it was already open!");
			} else {
				com1.Open ();  // opens the connection
				com1.ReadTimeout = 10;  // sets the timeout value before reporting error
			}
		} else {
			if (com1.IsOpen) {
				Debug.Log ("Port is already open");
			} else {
				Debug.Log ("Port == null");
			}
		}
		
	}
	#endif
	#if UNITY_EDITOR	
	public static void SendData (string s)
	{
		com1.Write (s);
	}
	#elif UNITY_IOS	
	public static void SendData (string s)
	{
		testBle.SendString (s);
	}
	#endif
	public static void CloseTabToy()
	{
		SendData("close");
	}
	private void CreateId ()
	{        
		for (int i= 1; i<=idSize; i++) { // create id
			tBase creatBase = new tBase ();
			
			tabID creatId = new tabID ();
			creatBase.state = false;
			creatBase.x = 0;
			creatBase.y = 0;
			creatBase.rotation = 0;
			creatId.addCounter = 0;
			creatId.removeCounter = 0;
			creatId.getPos.rotation = 0;
			creatId.getPos.y = 0;
			creatId.getPos.x = 0;
			creatId.getPos.state = false;
			creatId.changedData = false;
			tabData.Add (i, creatBase);  
			creatId.id = i;     
			tabIdData.Add (i, creatId); 
		}
	}
	
	bool button_state = false;
	GUIStyle myStyle = new GUIStyle ();
	
#if DEBUG_ONGUI
	void OnGUI ()
	{
		GUILayout.BeginArea (new Rect (Screen.width - 200, 0, 200, 200));
		if (GUILayout.Button ("   ", myStyle, GUILayout.Height (200), GUILayout.Width (200))) {
			button_state = !button_state;
		}
		GUILayout.EndArea ();
		
//		GUILayout.BeginArea (new Rect (Screen.width - 200,Screen.height - 200, 200, 200));
//		if (GUILayout.Button ("   ", myStyle, GUILayout.Height (200), GUILayout.Width (200))) {
//			CloseTabToy();
//			Debug.Log ("tabtoy close!!!!");
//		}
//		GUILayout.EndArea ();
		if (button_state) {
			if(GUILayout.Button ("id size  " + GetCount())){
				//SendData("IRSTT");
			}
			if(GUILayout.Button (lastTestId)){
				
			}
			#if UNITY_EDITOR	
			GUILayout.Button (speedTestStatic.ToString());
			#endif 	
			//GUILayout.Button (testData);
		}
		
	}
#endif
	#if UNITY_EDITOR	
	private string changeDataFormat(byte[] data)
	{
		string pData = "";
		int counter = 0;
		idList = new List<baseLegID>();
		var baseLeg = new baseLegID();
		foreach (byte c in data)
		{
			if (c == 0xFF)
			{
				b_dataState = true;
			}
			else
			{
				counter++;
				switch (counter)
				{
				case 1:
					baseLeg.id = (int)c;
					break;
				case 2:
					baseLeg.x = ((int)c) * 6;
					break;
				case 3:
					baseLeg.y = ((int)c) * 4;
					idList.Add(baseLeg);
					counter = 0;
					baseLeg = new baseLegID();
					break;
				default:
					counter = 0;
					break;
				}
			}
		}
		foreach (baseLegID bId in idList)
		{
			if ((bId.id % 2) == 0)
			{
				foreach (baseLegID bNextId in idList)
				{
					if ((bId.id + 1) == bNextId.id)
					{
						pData += (bId.id.ToString() + " " + (bId.x).ToString() + " " + bId.y.ToString() + "-");
					}
				}
			}
			else
			{
				pData += (bId.id.ToString() + " " + bId.x.ToString() + " " + bId.y.ToString() + "#");
			}
		}
		if (b_dataState == true && pData == "")
			pData += "#";
		return pData;
	}
	
	void OnApplicationQuit() 
	{
		Debug.Log ("com close!!!!");
		CloseTabToy();
	}
	private static void OnTimedEvent(System.Object source, System.Timers.ElapsedEventArgs e)
	{
		speedTestStatic=speedTest++;
		speedTest = 0;
	}
	
	#endif
}

public class tBase
{
	public Boolean state;
	public int x;
	public int y;
	public float rotation;
}
