﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System;

public class CharacterControl : MonoBehaviour {

	//public DictionaryBase _base;
	public GameObject EventSys;
	private bool success;
	public GameObject successSound;

	public Dictionary<int, tBase> _base;

	public GameObject DogCharacter;
	public bool dogDestroyed;
	public bool dogLanded;
	public float dogSpeed;

	public GameObject CatCharacter;
	public bool catDestroyed;
	public bool catLanded;
	public float catSpeed;

	public GameObject OwlCharacter;
	public bool owlDestroyed;
	public bool owlLanded;
	public float owlSpeed;

	public GameObject TurtleCharacter;
	public bool turtleDestroyed;
	public bool turtleLanded;
	public float turtleSpeed;

	public GameObject RatCharacter;
	public bool ratDestroyed;
	public bool ratLanded;
	public float ratSpeed;

	public GameObject puff;
	Vector3 dogLastPosition = Vector3.zero;
	Vector3 catLastPosition = Vector3.zero;
	Vector3 owlLastPosition = Vector3.zero;
	Vector3 ratLastPosition = Vector3.zero;
	Vector3 turtleLastPosition = Vector3.zero;

	public bool dogSpace;
	public bool catSpace;
	public bool owlSpace;
	public bool ratSpace;
	public bool turtleSpace;

	public bool freeze;
//	public bool freezeWand;
//
//	public GameObject WandCharacter;
//	private bool wandDestroyed;
//	public float wandSpeed;
//	Vector3 wandLastPosition = Vector3.zero;

	private bool talkDog;
	// Use this for initialization
	void Awake(){
		Application.targetFrameRate = 60;
	}

	void Start () {
		Application.targetFrameRate = 60;
		talkDog = true;
		dogLanded = true;
		dogDestroyed = true;
		catLanded = true;
		catDestroyed = true;
		owlLanded = true;
		owlDestroyed = true;
		ratLanded = true;
		ratDestroyed = true;
		turtleLanded = true;
		turtleDestroyed = true;
		success = false;
		//wandDestroyed = true;
		InvokeRepeating("playAnimations",0.5f,0.2f);
		DogCharacter.GetComponent<Animation>().Play("Idle Dog");
		CatCharacter.GetComponent<Animation>().Play("Idle Cat");
		OwlCharacter.GetComponent<Animation>().Play("Idle Owl");
		TurtleCharacter.GetComponent<Animation>().Play("Idle Turtle");
		RatCharacter.GetComponent<Animation>().Play("Idle Rat");
	}


	void FixedUpdate()
	{
		


	}

	void playAnimations(){

		if(freeze){
			CancelInvoke("DogIdle2");
		}
			//return;
		
		if(!freeze){
			dogSpeed = (DogCharacter.transform.position - dogLastPosition).magnitude;
			dogLastPosition = DogCharacter.transform.position;

			if(dogLanded && !dogDestroyed){
				//Debug.Log("speed: "+speed);
				if(dogSpeed > 0.05f && dogSpeed < 0.25f){
					CancelInvoke("DogIdle2");
					if(!DogCharacter.GetComponent<Animation>().IsPlaying("Walk Dog"))
						DogCharacter.GetComponent<Animation>().PlayQueued("Walk Dog", QueueMode.PlayNow);
				}else if (dogSpeed >= 0.25f){
					if(!DogCharacter.GetComponent<Animation>().IsPlaying("Run Dog"))
						DogCharacter.GetComponent<Animation>().PlayQueued("Run Dog", QueueMode.PlayNow);
				}else{
					
					if(!DogCharacter.GetComponent<Animation>().IsPlaying("Idle Dog") && !DogCharacter.GetComponent<Animation>().IsPlaying("Idle 2 Dog") && !success){
						DogCharacter.GetComponent<Animation>().PlayQueued("Idle Dog", QueueMode.PlayNow);
						Invoke("DogIdle2",5.0f);
						//CancelInvoke("DogIdle2");
					}
				}
			}
		}

		catSpeed = (CatCharacter.transform.position - catLastPosition).magnitude;
		catLastPosition = CatCharacter.transform.position;

		if(catLanded && !catDestroyed){
			//Debug.Log("speed: "+speed);
			if(catSpeed > 0.05f && catSpeed < 0.25f){
				CancelInvoke("CatIdle2");
				if(!CatCharacter.GetComponent<Animation>().IsPlaying("Walk Cat"))
					CatCharacter.GetComponent<Animation>().PlayQueued("Walk Cat", QueueMode.PlayNow);
			}else if (catSpeed >= 0.25f){
				//Debug.Log("cat run");
				if(!CatCharacter.GetComponent<Animation>().IsPlaying("Run Cat"))
					CatCharacter.GetComponent<Animation>().PlayQueued("Run Cat", QueueMode.PlayNow);
			}else{
				if(!CatCharacter.GetComponent<Animation>().IsPlaying("Idle Cat") && !CatCharacter.GetComponent<Animation>().IsPlaying("Idle 2 Cat") && !success){
					CatCharacter.GetComponent<Animation>().PlayQueued("Idle Cat", QueueMode.PlayNow);
					Invoke("CatIdle2",5.0f);
				}
			}
		}

		owlSpeed = (OwlCharacter.transform.position - owlLastPosition).magnitude;
		owlLastPosition = OwlCharacter.transform.position;

		if(owlLanded && !owlDestroyed){
			//Debug.Log("speed: "+speed);
			if(owlSpeed > 0.05f && owlSpeed < 0.25f){
				CancelInvoke("OwlIdle2");
				if(!OwlCharacter.GetComponent<Animation>().IsPlaying("Walk Owl"))
					OwlCharacter.GetComponent<Animation>().PlayQueued("Walk Owl", QueueMode.PlayNow);
			}else if (owlSpeed >= 0.25f){
				//Debug.Log("cat run");
				if(!OwlCharacter.GetComponent<Animation>().IsPlaying("Run Owl"))
					OwlCharacter.GetComponent<Animation>().PlayQueued("Run Owl", QueueMode.PlayNow);
			}else{
				if(!OwlCharacter.GetComponent<Animation>().IsPlaying("Idle Owl") && !OwlCharacter.GetComponent<Animation>().IsPlaying("Idle 2 Owl") && !success){
					OwlCharacter.GetComponent<Animation>().PlayQueued("Idle Owl", QueueMode.PlayNow);
					Invoke("OwlIdle2",5.0f);
				}
			}
		}

		turtleSpeed = (TurtleCharacter.transform.position - turtleLastPosition).magnitude;
		turtleLastPosition = TurtleCharacter.transform.position;

		if(turtleLanded && !turtleDestroyed){
			//Debug.Log("speed: "+speed);
			if(turtleSpeed > 0.05f && turtleSpeed < 0.25f){
				CancelInvoke("TurtleIdle2");
				if(!TurtleCharacter.GetComponent<Animation>().IsPlaying("Walk Turtle"))
					TurtleCharacter.GetComponent<Animation>().PlayQueued("Walk Turtle", QueueMode.PlayNow);
			}else if (turtleSpeed >= 0.25f){
				//Debug.Log("cat run");
				if(!TurtleCharacter.GetComponent<Animation>().IsPlaying("Run Turtle"))
					TurtleCharacter.GetComponent<Animation>().PlayQueued("Run Turtle", QueueMode.PlayNow);
			}else{
				if(!TurtleCharacter.GetComponent<Animation>().IsPlaying("Idle Turtle") && !TurtleCharacter.GetComponent<Animation>().IsPlaying("Idle 2 Turtle") && !success){
					TurtleCharacter.GetComponent<Animation>().PlayQueued("Idle Turtle", QueueMode.PlayNow);
					Invoke("TurtleIdle2",5.0f);
				}
			}
		}

		ratSpeed = (RatCharacter.transform.position - ratLastPosition).magnitude;
		ratLastPosition = RatCharacter.transform.position;

		if(ratLanded && !ratDestroyed){
			//Debug.Log("speed: "+speed);
			if(ratSpeed > 0.05f && ratSpeed < 0.25f){
				CancelInvoke("RatIdle2");
				if(!RatCharacter.GetComponent<Animation>().IsPlaying("Walk Rat"))
					RatCharacter.GetComponent<Animation>().PlayQueued("Walk Rat", QueueMode.PlayNow);
			}else if (ratSpeed >= 0.25f){
				//Debug.Log("cat run");
				if(!RatCharacter.GetComponent<Animation>().IsPlaying("Run Rat"))
					RatCharacter.GetComponent<Animation>().PlayQueued("Run Rat", QueueMode.PlayNow);
			}else{
				if(!RatCharacter.GetComponent<Animation>().IsPlaying("Idle Rat") && !TurtleCharacter.GetComponent<Animation>().IsPlaying("Idle 2 Rat") && !success){
					RatCharacter.GetComponent<Animation>().PlayQueued("Idle Rat", QueueMode.PlayNow);
					Invoke("RatIdle2",5.0f);
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update (){

		//Debug.Log(TabBase.checkDataState());
		if(TabBase.checkDataState()){
			_base = TabBase.GetBase();
		

			//--------------------------------------------------- DOG ----------------------------------------------------//
			if(_base[1].state){
				//TabBase.switch_state = 0;
				//Debug.Log("switch: "+TabBase.switch_state);
				float xxx, zzz;
				xxx = ((Mathf.CeilToInt(_base[1].x) - 105)*Screen.width)/1300;
				zzz = ((Mathf.CeilToInt(_base[1].y) - 145)*Screen.height)/800;
				//Debug.Log("zzz: "+zzz);
				Ray destination = Camera.main.ScreenPointToRay(new Vector3(xxx, 0, zzz));
				Quaternion target = Quaternion.Euler (0, _base[1].rotation+45, 0);

				if(IsInvoking("destroyDog"))
					CancelInvoke("destroyDog");
				
				if(dogDestroyed){
					CancelInvoke("destroyDog");
					dogDestroyed = false;

					DogCharacter.transform.position = new Vector3(destination.direction.x,DogCharacter.transform.position.y,zzz/Screen.height);
					Instantiate(puff,DogCharacter.transform.position,target);
					DogCharacter.SetActive(true);
					DogCharacter.GetComponent<Animation>().Play("Fall Dog");
				}

				if(DogCharacter.transform.position.y <= 0.06f && DogCharacter.transform.position.y > 0.026f && !dogLanded){
					dogLanded = true;
					DogCharacter.GetComponent<Animation>().PlayQueued("Land Dog",QueueMode.PlayNow);
				}

				Debug.Log("Dog z:"+zzz);

				DogCharacter.transform.rotation = Quaternion.Slerp (DogCharacter.transform.rotation, target, Time.deltaTime * 6);
				DogCharacter.transform.position = Vector3.Lerp (DogCharacter.transform.position, new Vector3(destination.direction.x*10,0.025f,zzz*-6/Screen.height), 6 * Time.deltaTime);


			}
			else{
				
				if(!DogCharacter.GetComponent<Animation>().IsPlaying("Fall Dog") && dogLanded && !dogDestroyed){
					
					if(!freeze){
						dogLanded = false;
						DogCharacter.transform.position = Vector3.Lerp (DogCharacter.transform.position,new Vector3(DogCharacter.transform.position.x,5.0f,DogCharacter.transform.position.z),6 * Time.deltaTime);
						DogCharacter.GetComponent<Animation>().PlayQueued("Fall Dog",QueueMode.PlayNow);
						Invoke("destroyDog",2.0f);
						CancelInvoke("DogIdle2");
					}
				}
			}

			//---------------------------------------------------- CAT --------------------------------------------------//
			if(_base[6].state){
				float xxx, zzz;
				xxx = ((Mathf.CeilToInt(_base[6].x) - 105)*Screen.width)/1368;
				zzz = ((Mathf.CeilToInt(_base[6].y) - 145)*Screen.height)/845;
				//Debug.Log("zzz: "+zzz);
				Ray destination = Camera.main.ScreenPointToRay(new Vector3(xxx, 0, zzz));
				Quaternion target = Quaternion.Euler (0, _base[6].rotation, 0);

				if(IsInvoking("destroyCat"))
					CancelInvoke("destroyCat");

				if(catDestroyed){
					catDestroyed = false;

					//CatCharacter = Instantiate(CatCharacter,new Vector3(destination.direction.x,0.025f,destination.direction.y),target);

					CatCharacter.transform.position = new Vector3(destination.direction.x,CatCharacter.transform.position.y,zzz/Screen.height);
					Instantiate(puff,CatCharacter.transform.position,target);
					//GameObject.Find("IBMWatson").GetComponent<IBMWatsonToyji>().helloCat();
					CatCharacter.SetActive(true);
					CatCharacter.GetComponent<Animation>().Play("Fall Cat");
				}

				if(CatCharacter.transform.position.y <= 0.06f && CatCharacter.transform.position.y > 0.026f && !catLanded){
					catLanded = true;
					CatCharacter.GetComponent<Animation>().PlayQueued("Land Cat",QueueMode.PlayNow);
				}
				

				CatCharacter.transform.rotation = Quaternion.Slerp (CatCharacter.transform.rotation, target, Time.deltaTime * 6);
				CatCharacter.transform.position = Vector3.Lerp (CatCharacter.transform.position, new Vector3(destination.direction.x*10,0.025f,zzz*-6/Screen.height), 6 * Time.deltaTime);
				//Debug.Log("destination: "+destination.direction);

			}
			else{

				if(!CatCharacter.GetComponent<Animation>().IsPlaying("Fall Cat") && catLanded && !catDestroyed){
					catLanded = false;
					//BlendAllToZero(DogCharacter);
					CatCharacter.transform.position = Vector3.Lerp (CatCharacter.transform.position,new Vector3(CatCharacter.transform.position.x,5.0f,CatCharacter.transform.position.z),6 * Time.deltaTime);
					CatCharacter.GetComponent<Animation>().PlayQueued("Fall Cat",QueueMode.PlayNow);
					Invoke("destroyCat",2.0f);
					CancelInvoke("CatIdle2");
				}
			}

			//---------------------------------------------------- OWL --------------------------------------------------//
			if(_base[5].state){
				float xxx, zzz;
				xxx = ((Mathf.CeilToInt(_base[5].x) - 105)*Screen.width)/1368;
				zzz = ((Mathf.CeilToInt(_base[5].y) - 145)*Screen.height)/845;
				//Debug.Log("zzz: "+zzz);
				Ray destination = Camera.main.ScreenPointToRay(new Vector3(xxx, 0, zzz));
				Quaternion target = Quaternion.Euler (0, _base[5].rotation+45, 0);

				if(IsInvoking("destroyOwl"))
					CancelInvoke("destroyOwl");

				if(owlDestroyed){
					owlDestroyed = false;

					//CatCharacter = Instantiate(CatCharacter,new Vector3(destination.direction.x,0.025f,destination.direction.y),target);

					OwlCharacter.transform.position = new Vector3(destination.direction.x,OwlCharacter.transform.position.y,zzz/Screen.height);
					Instantiate(puff,OwlCharacter.transform.position,target);
					//GameObject.Find("IBMWatson").GetComponent<IBMWatsonToyji>().helloCat();
					OwlCharacter.SetActive(true);
					OwlCharacter.GetComponent<Animation>().Play("Fall Owl");
				}

				if(OwlCharacter.transform.position.y <= 0.06f && OwlCharacter.transform.position.y > 0.026f && !owlLanded){
					owlLanded = true;
					OwlCharacter.GetComponent<Animation>().PlayQueued("Land Owl",QueueMode.PlayNow);
				}


				OwlCharacter.transform.rotation = Quaternion.Slerp (OwlCharacter.transform.rotation, target, Time.deltaTime * 6);
				OwlCharacter.transform.position = Vector3.Lerp (OwlCharacter.transform.position, new Vector3(destination.direction.x*10,0.025f,zzz*-6/Screen.height), 6 * Time.deltaTime);
				//Debug.Log("destination: "+destination.direction);

			}
			else{

				if(!OwlCharacter.GetComponent<Animation>().IsPlaying("Fall Owl") && owlLanded && !owlDestroyed){
					owlLanded = false;
					//BlendAllToZero(DogCharacter);
					OwlCharacter.transform.position = Vector3.Lerp (OwlCharacter.transform.position,new Vector3(OwlCharacter.transform.position.x,5.0f,OwlCharacter.transform.position.z),6 * Time.deltaTime);
					OwlCharacter.GetComponent<Animation>().PlayQueued("Fall Owl",QueueMode.PlayNow);
					Invoke("destroyOwl",2.0f);
					CancelInvoke("OwlIdle2");
				}
			}
			//---------------------------------------------------- RAT --------------------------------------------------//
			if(_base[4].state){
				float xxx, zzz;
				xxx = ((Mathf.CeilToInt(_base[4].x) - 105)*Screen.width)/1368;
				zzz = ((Mathf.CeilToInt(_base[4].y) - 145)*Screen.height)/845;
				//Debug.Log("zzz: "+zzz);
				Ray destination = Camera.main.ScreenPointToRay(new Vector3(xxx, 0, zzz));
				Quaternion target = Quaternion.Euler (0, _base[4].rotation+60, 0);

				if(IsInvoking("destroyRat"))
					CancelInvoke("destroyRat");

				if(ratDestroyed){
					ratDestroyed = false;

					//CatCharacter = Instantiate(CatCharacter,new Vector3(destination.direction.x,0.025f,destination.direction.y),target);

					RatCharacter.transform.position = new Vector3(destination.direction.x,RatCharacter.transform.position.y,zzz/Screen.height);
					Instantiate(puff,RatCharacter.transform.position,target);
					//GameObject.Find("IBMWatson").GetComponent<IBMWatsonToyji>().helloCat();
					RatCharacter.SetActive(true);
					RatCharacter.GetComponent<Animation>().Play("Fall Rat");
				}

				if(RatCharacter.transform.position.y <= 0.06f && RatCharacter.transform.position.y > 0.026f && !ratLanded){
					ratLanded = true;
					RatCharacter.GetComponent<Animation>().PlayQueued("Land Rat",QueueMode.PlayNow);
				}


				RatCharacter.transform.rotation = Quaternion.Slerp (RatCharacter.transform.rotation, target, Time.deltaTime * 6);
				RatCharacter.transform.position = Vector3.Lerp (RatCharacter.transform.position, new Vector3(destination.direction.x*10,0.025f,zzz*-6/Screen.height), 6 * Time.deltaTime);
				//Debug.Log("destination: "+destination.direction);

			}
			else{

				if(!RatCharacter.GetComponent<Animation>().IsPlaying("Fall Rat") && ratLanded && !ratDestroyed){
					ratLanded = false;
					//BlendAllToZero(DogCharacter);
					RatCharacter.transform.position = Vector3.Lerp (RatCharacter.transform.position,new Vector3(RatCharacter.transform.position.x,5.0f,RatCharacter.transform.position.z),6 * Time.deltaTime);
					RatCharacter.GetComponent<Animation>().PlayQueued("Fall Rat",QueueMode.PlayNow);
					Invoke("destroyRat",2.0f);
					CancelInvoke("RatIdle2");
				}
			}
			//---------------------------------------------------- TURTLE --------------------------------------------------//
			if(_base[2].state){
				float xxx, zzz;
				xxx = ((Mathf.CeilToInt(_base[2].x) - 105)*Screen.width)/1368;
				zzz = ((Mathf.CeilToInt(_base[2].y) - 145)*Screen.height)/845;
				//Debug.Log("zzz: "+zzz);
				Ray destination = Camera.main.ScreenPointToRay(new Vector3(xxx, 0, zzz));
				Quaternion target = Quaternion.Euler (0, _base[2].rotation+50, 0);

				if(IsInvoking("destroyTurtle"))
					CancelInvoke("destroyTurtle");

				if(turtleDestroyed){
					turtleDestroyed = false;

					//CatCharacter = Instantiate(CatCharacter,new Vector3(destination.direction.x,0.025f,destination.direction.y),target);

					TurtleCharacter.transform.position = new Vector3(destination.direction.x,TurtleCharacter.transform.position.y,zzz/Screen.height);
					Instantiate(puff,TurtleCharacter.transform.position,target);
					//GameObject.Find("IBMWatson").GetComponent<IBMWatsonToyji>().helloCat();
					TurtleCharacter.SetActive(true);
					TurtleCharacter.GetComponent<Animation>().Play("Fall Turtle");
				}

				if(TurtleCharacter.transform.position.y <= 0.06f && TurtleCharacter.transform.position.y > 0.026f && !turtleLanded){
					turtleLanded = true;
					TurtleCharacter.GetComponent<Animation>().PlayQueued("Land Turtle",QueueMode.PlayNow);
				}


				TurtleCharacter.transform.rotation = Quaternion.Slerp (TurtleCharacter.transform.rotation, target, Time.deltaTime * 6);
				TurtleCharacter.transform.position = Vector3.Lerp (TurtleCharacter.transform.position, new Vector3(destination.direction.x*10,0.025f,zzz*-6/Screen.height), 6 * Time.deltaTime);
				//Debug.Log("destination: "+destination.direction);

			}
			else{

				if(!TurtleCharacter.GetComponent<Animation>().IsPlaying("Fall Turtle") && turtleLanded && !turtleDestroyed){
					turtleLanded = false;
					//BlendAllToZero(DogCharacter);
					TurtleCharacter.transform.position = Vector3.Lerp (TurtleCharacter.transform.position,new Vector3(TurtleCharacter.transform.position.x,5.0f,TurtleCharacter.transform.position.z),6 * Time.deltaTime);
					TurtleCharacter.GetComponent<Animation>().PlayQueued("Fall Turtle",QueueMode.PlayNow);
					Invoke("destroyTurtle",2.0f);
					CancelInvoke("TurtleIdle2");
				}
			}

			if(dogSpace && catSpace && owlSpace && ratSpace && turtleSpace){

				if(!IsInvoking("getOut") && !success){
					success = true;
					Invoke("successLevel",0.5f);
					Invoke("getOut",2.0f);
				}


			}
		}
			
	}

	void successLevel(){
		successSound.GetComponent<AudioSource>().Play();
		DogCharacter.GetComponent<Animation>().Play("Success Dog");
		CatCharacter.GetComponent<Animation>().Play("Success Cat");
		OwlCharacter.GetComponent<Animation>().Play("Success Owl");
		RatCharacter.GetComponent<Animation>().Play("Success Rat");
		TurtleCharacter.GetComponent<Animation>().Play("Success Turtle");
	}

	void getOut(){
		EventSys.GetComponent<UIActions>().closeCurtains();
	}

	void BlendAllToZero(GameObject character){
		var animation = character.GetComponent<Animation>();
		foreach(AnimationState state in animation){
			animation.Blend(state.name, 0);
		}
			
	}

	void destroyDog(){
		Instantiate(puff,DogCharacter.transform.position,DogCharacter.transform.rotation);
		dogDestroyed = true;
		//Destroy(DogCharacter);
		DogCharacter.SetActive(false);
	}

	void destroyCat(){
		Instantiate(puff,CatCharacter.transform.position,CatCharacter.transform.rotation);
		catDestroyed = true;
		//Destroy(CatCharacter);
		CatCharacter.SetActive(false);
	}

	void destroyOwl(){
		Instantiate(puff,OwlCharacter.transform.position,OwlCharacter.transform.rotation);
		owlDestroyed = true;
		//Destroy(DogCharacter);
		OwlCharacter.SetActive(false);
	}

	void destroyTurtle(){
		Instantiate(puff,TurtleCharacter.transform.position,TurtleCharacter.transform.rotation);
		turtleDestroyed = true;
		//Destroy(DogCharacter);
		TurtleCharacter.SetActive(false);
	}

	void destroyRat(){
		Instantiate(puff,RatCharacter.transform.position,RatCharacter.transform.rotation);
		ratDestroyed = true;
		//Destroy(CatCharacter);
		RatCharacter.SetActive(false);
	}

//	void destroyWand(){
//		Instantiate(puff,WandCharacter.transform.position,WandCharacter.transform.rotation);
//		wandDestroyed = true;
//		//Destroy(CatCharacter);
//		WandCharacter.SetActive(false);
//	}

	void DogIdle2(){
		DogCharacter.GetComponent<Animation>().PlayQueued("Idle 2 Dog", QueueMode.PlayNow);
	}

	void CatIdle2(){
		CatCharacter.GetComponent<Animation>().PlayQueued("Idle 2 Cat", QueueMode.PlayNow);
	}

	void OwlIdle2(){
		OwlCharacter.GetComponent<Animation>().PlayQueued("Idle 2 Owl", QueueMode.PlayNow);
	}

	void RatIdle2(){
		RatCharacter.GetComponent<Animation>().PlayQueued("Idle 2 Rat", QueueMode.PlayNow);
	}

	void TurtleIdle2(){
		TurtleCharacter.GetComponent<Animation>().PlayQueued("Idle 2 Turtle", QueueMode.PlayNow);
	}

	void startRecording(){
		freeze=true;
		GameObject.Find("GUIMic").GetComponent<SpriteRenderer>().enabled = true;
		//GameObject.Find("IBMWatson").GetComponent<IBMWatsonToyji>().startRecording();

		
	}
}
