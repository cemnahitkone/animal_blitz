﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceDetection : MonoBehaviour {

	public GameObject CC;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other) {

		if(other.name == "Dog" && this.name =="DogSpace")
			CC.GetComponent<CharacterControl>().dogSpace = true;
		if(other.name == "Cat" && this.name =="CatSpace")
			CC.GetComponent<CharacterControl>().catSpace = true;
		if(other.name == "Owl" && this.name =="OwlSpace")
			CC.GetComponent<CharacterControl>().owlSpace = true;
		if(other.name == "Rat" && this.name =="RatSpace")
			CC.GetComponent<CharacterControl>().ratSpace = true;
		if(other.name == "Turtle" && this.name =="TurtleSpace")
			CC.GetComponent<CharacterControl>().turtleSpace = true;
	}

	void OnTriggerExit(Collider other) {

		if(other.name == "Dog" && this.name =="DogSpace")
			CC.GetComponent<CharacterControl>().dogSpace = false;
		if(other.name == "Cat" && this.name =="CatSpace")
			CC.GetComponent<CharacterControl>().catSpace = false;
		if(other.name == "Owl" && this.name =="OwlSpace")
			CC.GetComponent<CharacterControl>().owlSpace = false;
		if(other.name == "Rat" && this.name =="RatSpace")
			CC.GetComponent<CharacterControl>().ratSpace = false;
		if(other.name == "Turtle" && this.name =="TurtleSpace")
			CC.GetComponent<CharacterControl>().turtleSpace = false;
	}
		
}
