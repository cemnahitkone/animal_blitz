﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIActions : MonoBehaviour {

	public GameObject leftCurtain;
	public GameObject leftCurtainReverse;
	public GameObject rightCurtain;
	public GameObject rightCurtainReverse;

	public GameObject curtainCloseSound;

	private Animator anim;

	// Use this for initialization

	void Awake(){
		DontDestroyOnLoad(GameObject.Find("ToyjiConnection"));
	}

	void Start () {
		leftCurtainReverse.SetActive(true);
		rightCurtainReverse.SetActive(true);

		if(Application.loadedLevel == 0){
			PlayerPrefs.SetInt("Player1Score",0);
			PlayerPrefs.SetInt("Player2Score",0);
			PlayerPrefs.SetInt("levelCount",0);
		}else{
			PlayerPrefs.SetInt("levelCount",PlayerPrefs.GetInt("levelCount")+1);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void TwoPlayerGame(){

	}

	public void closeCurtains(){
		curtainCloseSound.GetComponent<AudioSource>().Play();
		leftCurtain.SetActive(true);
		rightCurtain.SetActive(true);
		Invoke("loadLevel",2.0f);
	}

	public void loadLevel(){
		if(Application.loadedLevel == 0){
			Application.LoadLevel(1);
		}else if(PlayerPrefs.GetInt("levelCount")== 1){
			Application.LoadLevel(3);
		}else if(PlayerPrefs.GetInt("levelCount")== 2){
			Application.LoadLevel(18);
		}else if(PlayerPrefs.GetInt("levelCount")== 3){
			Application.LoadLevel(2);
		}else if(PlayerPrefs.GetInt("levelCount")== 4){
			Application.LoadLevel(17);
		}else if(PlayerPrefs.GetInt("levelCount")== 5){
			Application.LoadLevel(20);
		}else if(PlayerPrefs.GetInt("levelCount")== 6){
			Application.LoadLevel(16);
		}else if(PlayerPrefs.GetInt("levelCount")== 7){
			Application.LoadLevel(23);
		}else if(PlayerPrefs.GetInt("levelCount")== 8){
			Application.LoadLevel(5);
		}else if(PlayerPrefs.GetInt("levelCount")== 9){
			Application.LoadLevel(6);
		}else if(PlayerPrefs.GetInt("levelCount")== 10){
			Application.LoadLevel(21);
		}else if(PlayerPrefs.GetInt("levelCount")== 11){
			Application.LoadLevel(7);
		}else if(PlayerPrefs.GetInt("levelCount")== 12){
			Application.LoadLevel(8);
		}else if(PlayerPrefs.GetInt("levelCount")== 13){
			Application.LoadLevel(9);
		}else if(PlayerPrefs.GetInt("levelCount")== 14){
			Application.LoadLevel(24);
		}else if(PlayerPrefs.GetInt("levelCount")== 15){
			Application.LoadLevel(22);
		}else if(PlayerPrefs.GetInt("levelCount")== 16){
			Application.LoadLevel(10);
		}else if(PlayerPrefs.GetInt("levelCount")== 17){
			Application.LoadLevel(11);
		}else if(PlayerPrefs.GetInt("levelCount")== 18){
			Application.LoadLevel(12);
		}else if(PlayerPrefs.GetInt("levelCount")== 19){
			Application.LoadLevel(13);
		}else if(PlayerPrefs.GetInt("levelCount")== 20){
			Application.LoadLevel(19);
		}else if(PlayerPrefs.GetInt("levelCount")== 21){
			Application.LoadLevel(15);
		}else if(PlayerPrefs.GetInt("levelCount")== 22){
			Application.LoadLevel(14);
		}else if(PlayerPrefs.GetInt("levelCount")== 23){
			Application.LoadLevel(4);
		}else if(PlayerPrefs.GetInt("levelCount")== 24){
			DestroyObject(GameObject.Find("ToyjiConnection"));
			Application.LoadLevel(0);
		}
	}
}
