﻿
using UnityEngine;

public static  class  BtConnection  {
	#if UNITY_ANDROID

	private static AndroidJavaClass ajc =new AndroidJavaClass ("com.badran.bluetoothcontroller.Bridge") ;

	public static void askEnableBluetooth(){
		ajc.CallStatic ("askEnableBluetooth");
		}
	public static int connect(){
				return ajc.CallStatic<int> ("connect");
		}
	//close connection
	public static void close(){
				ajc.CallStatic  ("close");
		}
	//returns true if data there's a data to read
	public static bool available (){
		return ajc.CallStatic <bool> ("available");
		}


	//read from Microcontroller
	public static string read(){
		return ajc.CallStatic<string> ("read");
		}
	//read Control data, for testing
	public static int controlData(){
		return ajc.CallStatic<int>("controlData");
		}

	public static byte [] readBuffer(int length){
		return ajc.CallStatic<byte []>("readBuffer",length);
		}
	public static byte [] readBuffer(int length,byte stopByte){
		return ajc.CallStatic<byte []>("readBuffer",length,stopByte);
		}

	public static void sendBytes(byte [] message){
		ajc.CallStatic ("sendBytes", message);
		}

	//send string
	public static void sendString(string message){
		ajc.CallStatic("sendString",message);
		}
	//send 1 char
	public static void sendChar(char message){
		ajc.CallStatic ("sendChar", message);
		}
	//change the default Bluetooth Module name 
	public static void moduleName(string name){
		ajc.CallStatic ("moduleName", name);
		}
	//start listening for string lines
	public static void listen(bool start){
		ajc.CallStatic ("listen", start);
		}
	//start listening for bytes
	public static void listen(bool start,int length){
		ajc.CallStatic ("listen", start,length);
		}
	//stop listening for anything, it will start listen again when you call any read method
	public static void stopListen(){
		BtConnection.listen (false);
		}
	public static void moduleMAC(string name){
		ajc.CallStatic ("moduleMac", name);
		}

	public static bool isConnected (){
		return ajc.CallStatic<bool> ("isConnected");
		}
	public static bool isSending (){
		return ajc.CallStatic<bool>("isSending");
		}

	public static bool enableBluetooth(){
		return ajc.CallStatic<bool>("enableBluetooth");
		}


	public static bool isBluetoothEnabled() {
		return ajc.CallStatic<bool>("isBluetoothEnabled");
		}

	public static string readControlData(){
		
		switch(BtConnection.controlData()){
		case 1 : return "Connected"; break;
		case 2 : return "Disconnected"; break;
		case -1 : return "found your Bluetooth Module but unable to connect to it";break;
		case -2 : return "Bluetooth module with the name or the MAC you provided can't be found";break;
		case -3 : return "Connection Failed, usually because your Bluetooth module is off ";break;
		case -4 : return "error while closing";break;
		case -5 : return "error while writing";break;
		case -6 : return "error while reading";break;
		default : return "Processing...";break;
		}
	}

	#endif


}
