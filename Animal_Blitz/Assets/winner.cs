﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class winner : MonoBehaviour {


	void Awake(){
		if(PlayerPrefs.GetInt("Player1Score") > PlayerPrefs.GetInt("Player2Score"))
			this.GetComponent<Text>().text = "Player 1 Wins!";
		else if(PlayerPrefs.GetInt("Player2Score") > PlayerPrefs.GetInt("Player1Score"))
			this.GetComponent<Text>().text = "Player 2 Wins!";	
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
