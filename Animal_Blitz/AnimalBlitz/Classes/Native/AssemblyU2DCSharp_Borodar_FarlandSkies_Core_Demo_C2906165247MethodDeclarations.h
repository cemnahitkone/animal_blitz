﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.Core.Demo.ColorPicker
struct ColorPicker_t2906165247;
// Borodar.FarlandSkies.Core.Demo.BaseColorButton
struct BaseColorButton_t3091645806;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_Demo_B3091645806.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void Borodar.FarlandSkies.Core.Demo.ColorPicker::.ctor()
extern "C"  void ColorPicker__ctor_m3394259222 (ColorPicker_t2906165247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Borodar.FarlandSkies.Core.Demo.BaseColorButton Borodar.FarlandSkies.Core.Demo.ColorPicker::get_ColorButton()
extern "C"  BaseColorButton_t3091645806 * ColorPicker_get_ColorButton_m397482303 (ColorPicker_t2906165247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.Core.Demo.ColorPicker::set_ColorButton(Borodar.FarlandSkies.Core.Demo.BaseColorButton)
extern "C"  void ColorPicker_set_ColorButton_m1108687192 (ColorPicker_t2906165247 * __this, BaseColorButton_t3091645806 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.Core.Demo.ColorPicker::Awake()
extern "C"  void ColorPicker_Awake_m989297835 (ColorPicker_t2906165247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.Core.Demo.ColorPicker::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ColorPicker_OnDrag_m1067314999 (ColorPicker_t2906165247 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.Core.Demo.ColorPicker::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ColorPicker_OnEndDrag_m1100624358 (ColorPicker_t2906165247 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.Core.Demo.ColorPicker::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ColorPicker_OnPointerClick_m3952310034 (ColorPicker_t2906165247 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.Core.Demo.ColorPicker::OnPickColor(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ColorPicker_OnPickColor_m2654928355 (ColorPicker_t2906165247 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
