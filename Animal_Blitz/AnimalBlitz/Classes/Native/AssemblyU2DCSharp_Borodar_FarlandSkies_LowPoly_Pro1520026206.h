﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Slider
struct Slider_t297367283;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Pro1741473933.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.PropertySlider
struct  PropertySlider_t1520026206  : public MonoBehaviour_t1158329972
{
public:
	// Borodar.FarlandSkies.LowPoly.PropertySlider/Type Borodar.FarlandSkies.LowPoly.PropertySlider::SliderType
	int32_t ___SliderType_2;
	// UnityEngine.UI.Slider Borodar.FarlandSkies.LowPoly.PropertySlider::_slider
	Slider_t297367283 * ____slider_3;

public:
	inline static int32_t get_offset_of_SliderType_2() { return static_cast<int32_t>(offsetof(PropertySlider_t1520026206, ___SliderType_2)); }
	inline int32_t get_SliderType_2() const { return ___SliderType_2; }
	inline int32_t* get_address_of_SliderType_2() { return &___SliderType_2; }
	inline void set_SliderType_2(int32_t value)
	{
		___SliderType_2 = value;
	}

	inline static int32_t get_offset_of__slider_3() { return static_cast<int32_t>(offsetof(PropertySlider_t1520026206, ____slider_3)); }
	inline Slider_t297367283 * get__slider_3() const { return ____slider_3; }
	inline Slider_t297367283 ** get_address_of__slider_3() { return &____slider_3; }
	inline void set__slider_3(Slider_t297367283 * value)
	{
		____slider_3 = value;
		Il2CppCodeGenWriteBarrier(&____slider_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
