﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.SoundButton
struct SoundButton_t2589941643;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.SoundButton::.ctor()
extern "C"  void SoundButton__ctor_m2349403693 (SoundButton_t2589941643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.SoundButton::Start()
extern "C"  void SoundButton_Start_m214350909 (SoundButton_t2589941643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.SoundButton::Toggle()
extern "C"  void SoundButton_Toggle_m3869163779 (SoundButton_t2589941643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.SoundButton::ToggleSprite()
extern "C"  void SoundButton_ToggleSprite_m2492578444 (SoundButton_t2589941643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
