﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.SceneTransition
struct  SceneTransition_t2696223135  : public MonoBehaviour_t1158329972
{
public:
	// System.String Ricimi.SceneTransition::scene
	String_t* ___scene_2;
	// System.Single Ricimi.SceneTransition::duration
	float ___duration_3;
	// UnityEngine.Color Ricimi.SceneTransition::color
	Color_t2020392075  ___color_4;

public:
	inline static int32_t get_offset_of_scene_2() { return static_cast<int32_t>(offsetof(SceneTransition_t2696223135, ___scene_2)); }
	inline String_t* get_scene_2() const { return ___scene_2; }
	inline String_t** get_address_of_scene_2() { return &___scene_2; }
	inline void set_scene_2(String_t* value)
	{
		___scene_2 = value;
		Il2CppCodeGenWriteBarrier(&___scene_2, value);
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(SceneTransition_t2696223135, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(SceneTransition_t2696223135, ___color_4)); }
	inline Color_t2020392075  get_color_4() const { return ___color_4; }
	inline Color_t2020392075 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color_t2020392075  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
