﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Ricimi.BackgroundMusic
struct BackgroundMusic_t2182474183;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.BackgroundMusic
struct  BackgroundMusic_t2182474183  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioSource Ricimi.BackgroundMusic::m_audioSource
	AudioSource_t1135106623 * ___m_audioSource_3;

public:
	inline static int32_t get_offset_of_m_audioSource_3() { return static_cast<int32_t>(offsetof(BackgroundMusic_t2182474183, ___m_audioSource_3)); }
	inline AudioSource_t1135106623 * get_m_audioSource_3() const { return ___m_audioSource_3; }
	inline AudioSource_t1135106623 ** get_address_of_m_audioSource_3() { return &___m_audioSource_3; }
	inline void set_m_audioSource_3(AudioSource_t1135106623 * value)
	{
		___m_audioSource_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_audioSource_3, value);
	}
};

struct BackgroundMusic_t2182474183_StaticFields
{
public:
	// Ricimi.BackgroundMusic Ricimi.BackgroundMusic::Instance
	BackgroundMusic_t2182474183 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(BackgroundMusic_t2182474183_StaticFields, ___Instance_2)); }
	inline BackgroundMusic_t2182474183 * get_Instance_2() const { return ___Instance_2; }
	inline BackgroundMusic_t2182474183 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(BackgroundMusic_t2182474183 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
