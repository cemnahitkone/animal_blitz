﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpeedTilt
struct  SpeedTilt_t451906044  : public MonoBehaviour_t1158329972
{
public:
	// System.Single SpeedTilt::multiplier
	float ___multiplier_2;
	// System.Single SpeedTilt::tiltDamp
	float ___tiltDamp_3;
	// UnityEngine.Quaternion SpeedTilt::startingRotation
	Quaternion_t4030073918  ___startingRotation_4;
	// System.Single SpeedTilt::currentTilt
	float ___currentTilt_5;
	// System.Single SpeedTilt::targetTilt
	float ___targetTilt_6;
	// System.Single SpeedTilt::velocityTilt
	float ___velocityTilt_7;
	// System.Single SpeedTilt::currentXspeed
	float ___currentXspeed_8;
	// System.Single SpeedTilt::previousXPosition
	float ___previousXPosition_9;

public:
	inline static int32_t get_offset_of_multiplier_2() { return static_cast<int32_t>(offsetof(SpeedTilt_t451906044, ___multiplier_2)); }
	inline float get_multiplier_2() const { return ___multiplier_2; }
	inline float* get_address_of_multiplier_2() { return &___multiplier_2; }
	inline void set_multiplier_2(float value)
	{
		___multiplier_2 = value;
	}

	inline static int32_t get_offset_of_tiltDamp_3() { return static_cast<int32_t>(offsetof(SpeedTilt_t451906044, ___tiltDamp_3)); }
	inline float get_tiltDamp_3() const { return ___tiltDamp_3; }
	inline float* get_address_of_tiltDamp_3() { return &___tiltDamp_3; }
	inline void set_tiltDamp_3(float value)
	{
		___tiltDamp_3 = value;
	}

	inline static int32_t get_offset_of_startingRotation_4() { return static_cast<int32_t>(offsetof(SpeedTilt_t451906044, ___startingRotation_4)); }
	inline Quaternion_t4030073918  get_startingRotation_4() const { return ___startingRotation_4; }
	inline Quaternion_t4030073918 * get_address_of_startingRotation_4() { return &___startingRotation_4; }
	inline void set_startingRotation_4(Quaternion_t4030073918  value)
	{
		___startingRotation_4 = value;
	}

	inline static int32_t get_offset_of_currentTilt_5() { return static_cast<int32_t>(offsetof(SpeedTilt_t451906044, ___currentTilt_5)); }
	inline float get_currentTilt_5() const { return ___currentTilt_5; }
	inline float* get_address_of_currentTilt_5() { return &___currentTilt_5; }
	inline void set_currentTilt_5(float value)
	{
		___currentTilt_5 = value;
	}

	inline static int32_t get_offset_of_targetTilt_6() { return static_cast<int32_t>(offsetof(SpeedTilt_t451906044, ___targetTilt_6)); }
	inline float get_targetTilt_6() const { return ___targetTilt_6; }
	inline float* get_address_of_targetTilt_6() { return &___targetTilt_6; }
	inline void set_targetTilt_6(float value)
	{
		___targetTilt_6 = value;
	}

	inline static int32_t get_offset_of_velocityTilt_7() { return static_cast<int32_t>(offsetof(SpeedTilt_t451906044, ___velocityTilt_7)); }
	inline float get_velocityTilt_7() const { return ___velocityTilt_7; }
	inline float* get_address_of_velocityTilt_7() { return &___velocityTilt_7; }
	inline void set_velocityTilt_7(float value)
	{
		___velocityTilt_7 = value;
	}

	inline static int32_t get_offset_of_currentXspeed_8() { return static_cast<int32_t>(offsetof(SpeedTilt_t451906044, ___currentXspeed_8)); }
	inline float get_currentXspeed_8() const { return ___currentXspeed_8; }
	inline float* get_address_of_currentXspeed_8() { return &___currentXspeed_8; }
	inline void set_currentXspeed_8(float value)
	{
		___currentXspeed_8 = value;
	}

	inline static int32_t get_offset_of_previousXPosition_9() { return static_cast<int32_t>(offsetof(SpeedTilt_t451906044, ___previousXPosition_9)); }
	inline float get_previousXPosition_9() const { return ___previousXPosition_9; }
	inline float* get_address_of_previousXPosition_9() { return &___previousXPosition_9; }
	inline void set_previousXPosition_9(float value)
	{
		___previousXPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
