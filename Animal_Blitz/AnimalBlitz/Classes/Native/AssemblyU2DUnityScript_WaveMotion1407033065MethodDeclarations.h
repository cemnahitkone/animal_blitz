﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WaveMotion
struct WaveMotion_t1407033065;

#include "codegen/il2cpp-codegen.h"

// System.Void WaveMotion::.ctor()
extern "C"  void WaveMotion__ctor_m2016441949 (WaveMotion_t1407033065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaveMotion::Start()
extern "C"  void WaveMotion_Start_m2459040061 (WaveMotion_t1407033065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaveMotion::Update()
extern "C"  void WaveMotion_Update_m2595368154 (WaveMotion_t1407033065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaveMotion::Main()
extern "C"  void WaveMotion_Main_m2255484154 (WaveMotion_t1407033065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
