﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.Core.Demo.PanelsDropdown
struct  PanelsDropdown_t24103620  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] Borodar.FarlandSkies.Core.Demo.PanelsDropdown::Panels
	GameObjectU5BU5D_t3057952154* ___Panels_2;
	// UnityEngine.UI.Dropdown Borodar.FarlandSkies.Core.Demo.PanelsDropdown::_dropdown
	Dropdown_t1985816271 * ____dropdown_3;

public:
	inline static int32_t get_offset_of_Panels_2() { return static_cast<int32_t>(offsetof(PanelsDropdown_t24103620, ___Panels_2)); }
	inline GameObjectU5BU5D_t3057952154* get_Panels_2() const { return ___Panels_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_Panels_2() { return &___Panels_2; }
	inline void set_Panels_2(GameObjectU5BU5D_t3057952154* value)
	{
		___Panels_2 = value;
		Il2CppCodeGenWriteBarrier(&___Panels_2, value);
	}

	inline static int32_t get_offset_of__dropdown_3() { return static_cast<int32_t>(offsetof(PanelsDropdown_t24103620, ____dropdown_3)); }
	inline Dropdown_t1985816271 * get__dropdown_3() const { return ____dropdown_3; }
	inline Dropdown_t1985816271 ** get_address_of__dropdown_3() { return &____dropdown_3; }
	inline void set__dropdown_3(Dropdown_t1985816271 * value)
	{
		____dropdown_3 = value;
		Il2CppCodeGenWriteBarrier(&____dropdown_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
