﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DrawCircle
struct DrawCircle_t4100096050;

#include "codegen/il2cpp-codegen.h"

// System.Void DrawCircle::.ctor()
extern "C"  void DrawCircle__ctor_m1424131541 (DrawCircle_t4100096050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DrawCircle::Start()
extern "C"  void DrawCircle_Start_m1633008669 (DrawCircle_t4100096050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DrawCircle::Update()
extern "C"  void DrawCircle_Update_m2990907444 (DrawCircle_t4100096050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
