﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.MusicButton
struct MusicButton_t3792610623;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.MusicButton::.ctor()
extern "C"  void MusicButton__ctor_m787963339 (MusicButton_t3792610623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.MusicButton::Start()
extern "C"  void MusicButton_Start_m2918416319 (MusicButton_t3792610623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.MusicButton::Toggle()
extern "C"  void MusicButton_Toggle_m334455065 (MusicButton_t3792610623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.MusicButton::ToggleSprite()
extern "C"  void MusicButton_ToggleSprite_m1817137360 (MusicButton_t3792610623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
