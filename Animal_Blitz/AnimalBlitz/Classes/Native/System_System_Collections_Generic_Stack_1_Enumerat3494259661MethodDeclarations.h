﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerato132208513MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<UnityEngine.GameObject>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m141765961(__this, ___t0, method) ((  void (*) (Enumerator_t3494259661 *, Stack_1_t2844261301 *, const MethodInfo*))Enumerator__ctor_m2816143215_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<UnityEngine.GameObject>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m992048741(__this, method) ((  void (*) (Enumerator_t3494259661 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m456699159_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<UnityEngine.GameObject>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3682369829(__this, method) ((  Il2CppObject * (*) (Enumerator_t3494259661 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1270503615_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<UnityEngine.GameObject>::Dispose()
#define Enumerator_Dispose_m1074728712(__this, method) ((  void (*) (Enumerator_t3494259661 *, const MethodInfo*))Enumerator_Dispose_m1520016780_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<UnityEngine.GameObject>::MoveNext()
#define Enumerator_MoveNext_m32313265(__this, method) ((  bool (*) (Enumerator_t3494259661 *, const MethodInfo*))Enumerator_MoveNext_m689054299_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<UnityEngine.GameObject>::get_Current()
#define Enumerator_get_Current_m1109380106(__this, method) ((  GameObject_t1756533147 * (*) (Enumerator_t3494259661 *, const MethodInfo*))Enumerator_get_Current_m2076859656_gshared)(__this, method)
