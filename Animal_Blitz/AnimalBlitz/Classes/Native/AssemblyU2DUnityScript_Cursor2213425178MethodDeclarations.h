﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cursor
struct Cursor_t2213425178;

#include "codegen/il2cpp-codegen.h"

// System.Void Cursor::.ctor()
extern "C"  void Cursor__ctor_m2004136328 (Cursor_t2213425178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cursor::Start()
extern "C"  void Cursor_Start_m3626726184 (Cursor_t2213425178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cursor::Update()
extern "C"  void Cursor_Update_m2213252325 (Cursor_t2213425178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cursor::Main()
extern "C"  void Cursor_Main_m3802382111 (Cursor_t2213425178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
