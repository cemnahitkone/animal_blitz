﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/InheritVelocityModule
struct InheritVelocityModule_t1475438411;
struct InheritVelocityModule_t1475438411_marshaled_pinvoke;
struct InheritVelocityModule_t1475438411_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_InheritVelo1475438411.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/InheritVelocityModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void InheritVelocityModule__ctor_m1419731093 (InheritVelocityModule_t1475438411 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct InheritVelocityModule_t1475438411;
struct InheritVelocityModule_t1475438411_marshaled_pinvoke;

extern "C" void InheritVelocityModule_t1475438411_marshal_pinvoke(const InheritVelocityModule_t1475438411& unmarshaled, InheritVelocityModule_t1475438411_marshaled_pinvoke& marshaled);
extern "C" void InheritVelocityModule_t1475438411_marshal_pinvoke_back(const InheritVelocityModule_t1475438411_marshaled_pinvoke& marshaled, InheritVelocityModule_t1475438411& unmarshaled);
extern "C" void InheritVelocityModule_t1475438411_marshal_pinvoke_cleanup(InheritVelocityModule_t1475438411_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct InheritVelocityModule_t1475438411;
struct InheritVelocityModule_t1475438411_marshaled_com;

extern "C" void InheritVelocityModule_t1475438411_marshal_com(const InheritVelocityModule_t1475438411& unmarshaled, InheritVelocityModule_t1475438411_marshaled_com& marshaled);
extern "C" void InheritVelocityModule_t1475438411_marshal_com_back(const InheritVelocityModule_t1475438411_marshaled_com& marshaled, InheritVelocityModule_t1475438411& unmarshaled);
extern "C" void InheritVelocityModule_t1475438411_marshal_com_cleanup(InheritVelocityModule_t1475438411_marshaled_com& marshaled);
