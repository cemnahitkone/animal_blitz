﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.MusicManager
struct MusicManager_t1668724344;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.MusicManager::.ctor()
extern "C"  void MusicManager__ctor_m3685575956 (MusicManager_t1668724344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.MusicManager::Start()
extern "C"  void MusicManager_Start_m2148288624 (MusicManager_t1668724344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.MusicManager::SwitchMusic()
extern "C"  void MusicManager_SwitchMusic_m1655761173 (MusicManager_t1668724344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
