﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Ricimi.BackgroundMusic
struct BackgroundMusic_t2182474183;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Ricimi_BackgroundMusic_Fade3857387351.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0
struct  U3CFadeAudioU3Ec__Iterator0_t1632642824  : public Il2CppObject
{
public:
	// Ricimi.BackgroundMusic/Fade Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::fadeType
	int32_t ___fadeType_0;
	// System.Single Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::<start>__0
	float ___U3CstartU3E__0_1;
	// System.Single Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::<end>__1
	float ___U3CendU3E__1_2;
	// System.Single Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::<i>__2
	float ___U3CiU3E__2_3;
	// System.Single Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::time
	float ___time_4;
	// System.Single Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::<step>__3
	float ___U3CstepU3E__3_5;
	// Ricimi.BackgroundMusic Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::$this
	BackgroundMusic_t2182474183 * ___U24this_6;
	// System.Object Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_fadeType_0() { return static_cast<int32_t>(offsetof(U3CFadeAudioU3Ec__Iterator0_t1632642824, ___fadeType_0)); }
	inline int32_t get_fadeType_0() const { return ___fadeType_0; }
	inline int32_t* get_address_of_fadeType_0() { return &___fadeType_0; }
	inline void set_fadeType_0(int32_t value)
	{
		___fadeType_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFadeAudioU3Ec__Iterator0_t1632642824, ___U3CstartU3E__0_1)); }
	inline float get_U3CstartU3E__0_1() const { return ___U3CstartU3E__0_1; }
	inline float* get_address_of_U3CstartU3E__0_1() { return &___U3CstartU3E__0_1; }
	inline void set_U3CstartU3E__0_1(float value)
	{
		___U3CstartU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CendU3E__1_2() { return static_cast<int32_t>(offsetof(U3CFadeAudioU3Ec__Iterator0_t1632642824, ___U3CendU3E__1_2)); }
	inline float get_U3CendU3E__1_2() const { return ___U3CendU3E__1_2; }
	inline float* get_address_of_U3CendU3E__1_2() { return &___U3CendU3E__1_2; }
	inline void set_U3CendU3E__1_2(float value)
	{
		___U3CendU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__2_3() { return static_cast<int32_t>(offsetof(U3CFadeAudioU3Ec__Iterator0_t1632642824, ___U3CiU3E__2_3)); }
	inline float get_U3CiU3E__2_3() const { return ___U3CiU3E__2_3; }
	inline float* get_address_of_U3CiU3E__2_3() { return &___U3CiU3E__2_3; }
	inline void set_U3CiU3E__2_3(float value)
	{
		___U3CiU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(U3CFadeAudioU3Ec__Iterator0_t1632642824, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}

	inline static int32_t get_offset_of_U3CstepU3E__3_5() { return static_cast<int32_t>(offsetof(U3CFadeAudioU3Ec__Iterator0_t1632642824, ___U3CstepU3E__3_5)); }
	inline float get_U3CstepU3E__3_5() const { return ___U3CstepU3E__3_5; }
	inline float* get_address_of_U3CstepU3E__3_5() { return &___U3CstepU3E__3_5; }
	inline void set_U3CstepU3E__3_5(float value)
	{
		___U3CstepU3E__3_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CFadeAudioU3Ec__Iterator0_t1632642824, ___U24this_6)); }
	inline BackgroundMusic_t2182474183 * get_U24this_6() const { return ___U24this_6; }
	inline BackgroundMusic_t2182474183 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(BackgroundMusic_t2182474183 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CFadeAudioU3Ec__Iterator0_t1632642824, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CFadeAudioU3Ec__Iterator0_t1632642824, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CFadeAudioU3Ec__Iterator0_t1632642824, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
