﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Ricimi.TintedButton/ButtonClickedEvent
struct ButtonClickedEvent_t2671903891;

#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.TintedButton
struct  TintedButton_t315702378  : public UIBehaviour_t3960014691
{
public:
	// Ricimi.TintedButton/ButtonClickedEvent Ricimi.TintedButton::m_OnClick
	ButtonClickedEvent_t2671903891 * ___m_OnClick_2;
	// System.Boolean Ricimi.TintedButton::m_pointerInside
	bool ___m_pointerInside_4;
	// System.Boolean Ricimi.TintedButton::m_pointerPressed
	bool ___m_pointerPressed_5;

public:
	inline static int32_t get_offset_of_m_OnClick_2() { return static_cast<int32_t>(offsetof(TintedButton_t315702378, ___m_OnClick_2)); }
	inline ButtonClickedEvent_t2671903891 * get_m_OnClick_2() const { return ___m_OnClick_2; }
	inline ButtonClickedEvent_t2671903891 ** get_address_of_m_OnClick_2() { return &___m_OnClick_2; }
	inline void set_m_OnClick_2(ButtonClickedEvent_t2671903891 * value)
	{
		___m_OnClick_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnClick_2, value);
	}

	inline static int32_t get_offset_of_m_pointerInside_4() { return static_cast<int32_t>(offsetof(TintedButton_t315702378, ___m_pointerInside_4)); }
	inline bool get_m_pointerInside_4() const { return ___m_pointerInside_4; }
	inline bool* get_address_of_m_pointerInside_4() { return &___m_pointerInside_4; }
	inline void set_m_pointerInside_4(bool value)
	{
		___m_pointerInside_4 = value;
	}

	inline static int32_t get_offset_of_m_pointerPressed_5() { return static_cast<int32_t>(offsetof(TintedButton_t315702378, ___m_pointerPressed_5)); }
	inline bool get_m_pointerPressed_5() const { return ___m_pointerPressed_5; }
	inline bool* get_address_of_m_pointerPressed_5() { return &___m_pointerPressed_5; }
	inline void set_m_pointerPressed_5(bool value)
	{
		___m_pointerPressed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
