﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.PausetButton
struct PausetButton_t3683727040;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.PausetButton::.ctor()
extern "C"  void PausetButton__ctor_m654550377 (PausetButton_t3683727040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.PausetButton::OnClick()
extern "C"  void PausetButton_OnClick_m4122208252 (PausetButton_t3683727040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
