﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// testBle
struct testBle_t3025851879;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// testBle/<Connect>c__AnonStorey0
struct  U3CConnectU3Ec__AnonStorey0_t350889135  : public Il2CppObject
{
public:
	// System.String testBle/<Connect>c__AnonStorey0::characteristicUUID
	String_t* ___characteristicUUID_0;
	// testBle testBle/<Connect>c__AnonStorey0::$this
	testBle_t3025851879 * ___U24this_1;

public:
	inline static int32_t get_offset_of_characteristicUUID_0() { return static_cast<int32_t>(offsetof(U3CConnectU3Ec__AnonStorey0_t350889135, ___characteristicUUID_0)); }
	inline String_t* get_characteristicUUID_0() const { return ___characteristicUUID_0; }
	inline String_t** get_address_of_characteristicUUID_0() { return &___characteristicUUID_0; }
	inline void set_characteristicUUID_0(String_t* value)
	{
		___characteristicUUID_0 = value;
		Il2CppCodeGenWriteBarrier(&___characteristicUUID_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CConnectU3Ec__AnonStorey0_t350889135, ___U24this_1)); }
	inline testBle_t3025851879 * get_U24this_1() const { return ___U24this_1; }
	inline testBle_t3025851879 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(testBle_t3025851879 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
