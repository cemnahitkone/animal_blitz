﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;

#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILineRenderer
struct  UILineRenderer_t1431191837  : public Graphic_t2426225576
{
public:
	// UnityEngine.Texture UILineRenderer::m_Texture
	Texture_t2243626319 * ___m_Texture_19;
	// UnityEngine.Rect UILineRenderer::m_UVRect
	Rect_t3681755626  ___m_UVRect_20;
	// System.Single UILineRenderer::LineThickness
	float ___LineThickness_21;
	// System.Boolean UILineRenderer::UseMargins
	bool ___UseMargins_22;
	// UnityEngine.Vector2 UILineRenderer::Margin
	Vector2_t2243707579  ___Margin_23;
	// UnityEngine.Vector2[] UILineRenderer::Points
	Vector2U5BU5D_t686124026* ___Points_24;
	// System.Boolean UILineRenderer::relativeSize
	bool ___relativeSize_25;

public:
	inline static int32_t get_offset_of_m_Texture_19() { return static_cast<int32_t>(offsetof(UILineRenderer_t1431191837, ___m_Texture_19)); }
	inline Texture_t2243626319 * get_m_Texture_19() const { return ___m_Texture_19; }
	inline Texture_t2243626319 ** get_address_of_m_Texture_19() { return &___m_Texture_19; }
	inline void set_m_Texture_19(Texture_t2243626319 * value)
	{
		___m_Texture_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_Texture_19, value);
	}

	inline static int32_t get_offset_of_m_UVRect_20() { return static_cast<int32_t>(offsetof(UILineRenderer_t1431191837, ___m_UVRect_20)); }
	inline Rect_t3681755626  get_m_UVRect_20() const { return ___m_UVRect_20; }
	inline Rect_t3681755626 * get_address_of_m_UVRect_20() { return &___m_UVRect_20; }
	inline void set_m_UVRect_20(Rect_t3681755626  value)
	{
		___m_UVRect_20 = value;
	}

	inline static int32_t get_offset_of_LineThickness_21() { return static_cast<int32_t>(offsetof(UILineRenderer_t1431191837, ___LineThickness_21)); }
	inline float get_LineThickness_21() const { return ___LineThickness_21; }
	inline float* get_address_of_LineThickness_21() { return &___LineThickness_21; }
	inline void set_LineThickness_21(float value)
	{
		___LineThickness_21 = value;
	}

	inline static int32_t get_offset_of_UseMargins_22() { return static_cast<int32_t>(offsetof(UILineRenderer_t1431191837, ___UseMargins_22)); }
	inline bool get_UseMargins_22() const { return ___UseMargins_22; }
	inline bool* get_address_of_UseMargins_22() { return &___UseMargins_22; }
	inline void set_UseMargins_22(bool value)
	{
		___UseMargins_22 = value;
	}

	inline static int32_t get_offset_of_Margin_23() { return static_cast<int32_t>(offsetof(UILineRenderer_t1431191837, ___Margin_23)); }
	inline Vector2_t2243707579  get_Margin_23() const { return ___Margin_23; }
	inline Vector2_t2243707579 * get_address_of_Margin_23() { return &___Margin_23; }
	inline void set_Margin_23(Vector2_t2243707579  value)
	{
		___Margin_23 = value;
	}

	inline static int32_t get_offset_of_Points_24() { return static_cast<int32_t>(offsetof(UILineRenderer_t1431191837, ___Points_24)); }
	inline Vector2U5BU5D_t686124026* get_Points_24() const { return ___Points_24; }
	inline Vector2U5BU5D_t686124026** get_address_of_Points_24() { return &___Points_24; }
	inline void set_Points_24(Vector2U5BU5D_t686124026* value)
	{
		___Points_24 = value;
		Il2CppCodeGenWriteBarrier(&___Points_24, value);
	}

	inline static int32_t get_offset_of_relativeSize_25() { return static_cast<int32_t>(offsetof(UILineRenderer_t1431191837, ___relativeSize_25)); }
	inline bool get_relativeSize_25() const { return ___relativeSize_25; }
	inline bool* get_address_of_relativeSize_25() { return &___relativeSize_25; }
	inline void set_relativeSize_25(bool value)
	{
		___relativeSize_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
