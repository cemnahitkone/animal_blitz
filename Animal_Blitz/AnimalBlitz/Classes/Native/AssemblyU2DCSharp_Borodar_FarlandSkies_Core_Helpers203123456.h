﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Borodar.FarlandSkies.LowPoly.SkyboxCycleManager
struct SkyboxCycleManager_t61875227;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.Core.Helpers.Singleton`1<Borodar.FarlandSkies.LowPoly.SkyboxCycleManager>
struct  Singleton_1_t203123456  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t203123456_StaticFields
{
public:
	// T Borodar.FarlandSkies.Core.Helpers.Singleton`1::_instance
	SkyboxCycleManager_t61875227 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t203123456_StaticFields, ____instance_2)); }
	inline SkyboxCycleManager_t61875227 * get__instance_2() const { return ____instance_2; }
	inline SkyboxCycleManager_t61875227 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(SkyboxCycleManager_t61875227 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
