﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/ColorBySpeedModule
struct ColorBySpeedModule_t3282895500;
struct ColorBySpeedModule_t3282895500_marshaled_pinvoke;
struct ColorBySpeedModule_t3282895500_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ColorBySpee3282895500.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/ColorBySpeedModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ColorBySpeedModule__ctor_m1451770058 (ColorBySpeedModule_t3282895500 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct ColorBySpeedModule_t3282895500;
struct ColorBySpeedModule_t3282895500_marshaled_pinvoke;

extern "C" void ColorBySpeedModule_t3282895500_marshal_pinvoke(const ColorBySpeedModule_t3282895500& unmarshaled, ColorBySpeedModule_t3282895500_marshaled_pinvoke& marshaled);
extern "C" void ColorBySpeedModule_t3282895500_marshal_pinvoke_back(const ColorBySpeedModule_t3282895500_marshaled_pinvoke& marshaled, ColorBySpeedModule_t3282895500& unmarshaled);
extern "C" void ColorBySpeedModule_t3282895500_marshal_pinvoke_cleanup(ColorBySpeedModule_t3282895500_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ColorBySpeedModule_t3282895500;
struct ColorBySpeedModule_t3282895500_marshaled_com;

extern "C" void ColorBySpeedModule_t3282895500_marshal_com(const ColorBySpeedModule_t3282895500& unmarshaled, ColorBySpeedModule_t3282895500_marshaled_com& marshaled);
extern "C" void ColorBySpeedModule_t3282895500_marshal_com_back(const ColorBySpeedModule_t3282895500_marshaled_com& marshaled, ColorBySpeedModule_t3282895500& unmarshaled);
extern "C" void ColorBySpeedModule_t3282895500_marshal_com_cleanup(ColorBySpeedModule_t3282895500_marshaled_com& marshaled);
