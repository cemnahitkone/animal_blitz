﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/ColorOverLifetimeModule
struct ColorOverLifetimeModule_t474557797;
struct ColorOverLifetimeModule_t474557797_marshaled_pinvoke;
struct ColorOverLifetimeModule_t474557797_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ColorOverLif474557797.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/ColorOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ColorOverLifetimeModule__ctor_m2462495471 (ColorOverLifetimeModule_t474557797 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct ColorOverLifetimeModule_t474557797;
struct ColorOverLifetimeModule_t474557797_marshaled_pinvoke;

extern "C" void ColorOverLifetimeModule_t474557797_marshal_pinvoke(const ColorOverLifetimeModule_t474557797& unmarshaled, ColorOverLifetimeModule_t474557797_marshaled_pinvoke& marshaled);
extern "C" void ColorOverLifetimeModule_t474557797_marshal_pinvoke_back(const ColorOverLifetimeModule_t474557797_marshaled_pinvoke& marshaled, ColorOverLifetimeModule_t474557797& unmarshaled);
extern "C" void ColorOverLifetimeModule_t474557797_marshal_pinvoke_cleanup(ColorOverLifetimeModule_t474557797_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ColorOverLifetimeModule_t474557797;
struct ColorOverLifetimeModule_t474557797_marshaled_com;

extern "C" void ColorOverLifetimeModule_t474557797_marshal_com(const ColorOverLifetimeModule_t474557797& unmarshaled, ColorOverLifetimeModule_t474557797_marshaled_com& marshaled);
extern "C" void ColorOverLifetimeModule_t474557797_marshal_com_back(const ColorOverLifetimeModule_t474557797_marshaled_com& marshaled, ColorOverLifetimeModule_t474557797& unmarshaled);
extern "C" void ColorOverLifetimeModule_t474557797_marshal_com_cleanup(ColorOverLifetimeModule_t474557797_marshaled_com& marshaled);
