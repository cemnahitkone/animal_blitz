﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t189460977;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// SelectedButton
struct SelectedButton_t2241840337;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationSelect
struct  AnimationSelect_t1701553070  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera AnimationSelect::sceneCamera
	Camera_t189460977 * ___sceneCamera_2;
	// System.String AnimationSelect::currentAnimation
	String_t* ___currentAnimation_3;
	// UnityEngine.GameObject AnimationSelect::dog
	GameObject_t1756533147 * ___dog_4;
	// UnityEngine.GameObject AnimationSelect::cat
	GameObject_t1756533147 * ___cat_5;
	// UnityEngine.GameObject AnimationSelect::turtle
	GameObject_t1756533147 * ___turtle_6;
	// UnityEngine.GameObject AnimationSelect::owl
	GameObject_t1756533147 * ___owl_7;
	// UnityEngine.GameObject AnimationSelect::rat
	GameObject_t1756533147 * ___rat_8;
	// UnityEngine.Transform AnimationSelect::idleButton
	Transform_t3275118058 * ___idleButton_9;
	// UnityEngine.Transform AnimationSelect::talkButton
	Transform_t3275118058 * ___talkButton_10;
	// UnityEngine.Transform AnimationSelect::rollingButton
	Transform_t3275118058 * ___rollingButton_11;
	// UnityEngine.Transform AnimationSelect::successButton
	Transform_t3275118058 * ___successButton_12;
	// UnityEngine.Transform AnimationSelect::jumpButton
	Transform_t3275118058 * ___jumpButton_13;
	// UnityEngine.Transform AnimationSelect::idle2Button
	Transform_t3275118058 * ___idle2Button_14;
	// UnityEngine.Transform AnimationSelect::runButton
	Transform_t3275118058 * ___runButton_15;
	// UnityEngine.Transform AnimationSelect::failureButton
	Transform_t3275118058 * ___failureButton_16;
	// UnityEngine.Transform AnimationSelect::sleepButton
	Transform_t3275118058 * ___sleepButton_17;
	// UnityEngine.Transform AnimationSelect::walkButton
	Transform_t3275118058 * ___walkButton_18;
	// UnityEngine.Transform AnimationSelect::selectedButton
	Transform_t3275118058 * ___selectedButton_19;
	// SelectedButton AnimationSelect::selectedButtonScript
	SelectedButton_t2241840337 * ___selectedButtonScript_20;

public:
	inline static int32_t get_offset_of_sceneCamera_2() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___sceneCamera_2)); }
	inline Camera_t189460977 * get_sceneCamera_2() const { return ___sceneCamera_2; }
	inline Camera_t189460977 ** get_address_of_sceneCamera_2() { return &___sceneCamera_2; }
	inline void set_sceneCamera_2(Camera_t189460977 * value)
	{
		___sceneCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___sceneCamera_2, value);
	}

	inline static int32_t get_offset_of_currentAnimation_3() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___currentAnimation_3)); }
	inline String_t* get_currentAnimation_3() const { return ___currentAnimation_3; }
	inline String_t** get_address_of_currentAnimation_3() { return &___currentAnimation_3; }
	inline void set_currentAnimation_3(String_t* value)
	{
		___currentAnimation_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentAnimation_3, value);
	}

	inline static int32_t get_offset_of_dog_4() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___dog_4)); }
	inline GameObject_t1756533147 * get_dog_4() const { return ___dog_4; }
	inline GameObject_t1756533147 ** get_address_of_dog_4() { return &___dog_4; }
	inline void set_dog_4(GameObject_t1756533147 * value)
	{
		___dog_4 = value;
		Il2CppCodeGenWriteBarrier(&___dog_4, value);
	}

	inline static int32_t get_offset_of_cat_5() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___cat_5)); }
	inline GameObject_t1756533147 * get_cat_5() const { return ___cat_5; }
	inline GameObject_t1756533147 ** get_address_of_cat_5() { return &___cat_5; }
	inline void set_cat_5(GameObject_t1756533147 * value)
	{
		___cat_5 = value;
		Il2CppCodeGenWriteBarrier(&___cat_5, value);
	}

	inline static int32_t get_offset_of_turtle_6() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___turtle_6)); }
	inline GameObject_t1756533147 * get_turtle_6() const { return ___turtle_6; }
	inline GameObject_t1756533147 ** get_address_of_turtle_6() { return &___turtle_6; }
	inline void set_turtle_6(GameObject_t1756533147 * value)
	{
		___turtle_6 = value;
		Il2CppCodeGenWriteBarrier(&___turtle_6, value);
	}

	inline static int32_t get_offset_of_owl_7() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___owl_7)); }
	inline GameObject_t1756533147 * get_owl_7() const { return ___owl_7; }
	inline GameObject_t1756533147 ** get_address_of_owl_7() { return &___owl_7; }
	inline void set_owl_7(GameObject_t1756533147 * value)
	{
		___owl_7 = value;
		Il2CppCodeGenWriteBarrier(&___owl_7, value);
	}

	inline static int32_t get_offset_of_rat_8() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___rat_8)); }
	inline GameObject_t1756533147 * get_rat_8() const { return ___rat_8; }
	inline GameObject_t1756533147 ** get_address_of_rat_8() { return &___rat_8; }
	inline void set_rat_8(GameObject_t1756533147 * value)
	{
		___rat_8 = value;
		Il2CppCodeGenWriteBarrier(&___rat_8, value);
	}

	inline static int32_t get_offset_of_idleButton_9() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___idleButton_9)); }
	inline Transform_t3275118058 * get_idleButton_9() const { return ___idleButton_9; }
	inline Transform_t3275118058 ** get_address_of_idleButton_9() { return &___idleButton_9; }
	inline void set_idleButton_9(Transform_t3275118058 * value)
	{
		___idleButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___idleButton_9, value);
	}

	inline static int32_t get_offset_of_talkButton_10() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___talkButton_10)); }
	inline Transform_t3275118058 * get_talkButton_10() const { return ___talkButton_10; }
	inline Transform_t3275118058 ** get_address_of_talkButton_10() { return &___talkButton_10; }
	inline void set_talkButton_10(Transform_t3275118058 * value)
	{
		___talkButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___talkButton_10, value);
	}

	inline static int32_t get_offset_of_rollingButton_11() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___rollingButton_11)); }
	inline Transform_t3275118058 * get_rollingButton_11() const { return ___rollingButton_11; }
	inline Transform_t3275118058 ** get_address_of_rollingButton_11() { return &___rollingButton_11; }
	inline void set_rollingButton_11(Transform_t3275118058 * value)
	{
		___rollingButton_11 = value;
		Il2CppCodeGenWriteBarrier(&___rollingButton_11, value);
	}

	inline static int32_t get_offset_of_successButton_12() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___successButton_12)); }
	inline Transform_t3275118058 * get_successButton_12() const { return ___successButton_12; }
	inline Transform_t3275118058 ** get_address_of_successButton_12() { return &___successButton_12; }
	inline void set_successButton_12(Transform_t3275118058 * value)
	{
		___successButton_12 = value;
		Il2CppCodeGenWriteBarrier(&___successButton_12, value);
	}

	inline static int32_t get_offset_of_jumpButton_13() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___jumpButton_13)); }
	inline Transform_t3275118058 * get_jumpButton_13() const { return ___jumpButton_13; }
	inline Transform_t3275118058 ** get_address_of_jumpButton_13() { return &___jumpButton_13; }
	inline void set_jumpButton_13(Transform_t3275118058 * value)
	{
		___jumpButton_13 = value;
		Il2CppCodeGenWriteBarrier(&___jumpButton_13, value);
	}

	inline static int32_t get_offset_of_idle2Button_14() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___idle2Button_14)); }
	inline Transform_t3275118058 * get_idle2Button_14() const { return ___idle2Button_14; }
	inline Transform_t3275118058 ** get_address_of_idle2Button_14() { return &___idle2Button_14; }
	inline void set_idle2Button_14(Transform_t3275118058 * value)
	{
		___idle2Button_14 = value;
		Il2CppCodeGenWriteBarrier(&___idle2Button_14, value);
	}

	inline static int32_t get_offset_of_runButton_15() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___runButton_15)); }
	inline Transform_t3275118058 * get_runButton_15() const { return ___runButton_15; }
	inline Transform_t3275118058 ** get_address_of_runButton_15() { return &___runButton_15; }
	inline void set_runButton_15(Transform_t3275118058 * value)
	{
		___runButton_15 = value;
		Il2CppCodeGenWriteBarrier(&___runButton_15, value);
	}

	inline static int32_t get_offset_of_failureButton_16() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___failureButton_16)); }
	inline Transform_t3275118058 * get_failureButton_16() const { return ___failureButton_16; }
	inline Transform_t3275118058 ** get_address_of_failureButton_16() { return &___failureButton_16; }
	inline void set_failureButton_16(Transform_t3275118058 * value)
	{
		___failureButton_16 = value;
		Il2CppCodeGenWriteBarrier(&___failureButton_16, value);
	}

	inline static int32_t get_offset_of_sleepButton_17() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___sleepButton_17)); }
	inline Transform_t3275118058 * get_sleepButton_17() const { return ___sleepButton_17; }
	inline Transform_t3275118058 ** get_address_of_sleepButton_17() { return &___sleepButton_17; }
	inline void set_sleepButton_17(Transform_t3275118058 * value)
	{
		___sleepButton_17 = value;
		Il2CppCodeGenWriteBarrier(&___sleepButton_17, value);
	}

	inline static int32_t get_offset_of_walkButton_18() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___walkButton_18)); }
	inline Transform_t3275118058 * get_walkButton_18() const { return ___walkButton_18; }
	inline Transform_t3275118058 ** get_address_of_walkButton_18() { return &___walkButton_18; }
	inline void set_walkButton_18(Transform_t3275118058 * value)
	{
		___walkButton_18 = value;
		Il2CppCodeGenWriteBarrier(&___walkButton_18, value);
	}

	inline static int32_t get_offset_of_selectedButton_19() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___selectedButton_19)); }
	inline Transform_t3275118058 * get_selectedButton_19() const { return ___selectedButton_19; }
	inline Transform_t3275118058 ** get_address_of_selectedButton_19() { return &___selectedButton_19; }
	inline void set_selectedButton_19(Transform_t3275118058 * value)
	{
		___selectedButton_19 = value;
		Il2CppCodeGenWriteBarrier(&___selectedButton_19, value);
	}

	inline static int32_t get_offset_of_selectedButtonScript_20() { return static_cast<int32_t>(offsetof(AnimationSelect_t1701553070, ___selectedButtonScript_20)); }
	inline SelectedButton_t2241840337 * get_selectedButtonScript_20() const { return ___selectedButtonScript_20; }
	inline SelectedButton_t2241840337 ** get_address_of_selectedButtonScript_20() { return &___selectedButtonScript_20; }
	inline void set_selectedButtonScript_20(SelectedButton_t2241840337 * value)
	{
		___selectedButtonScript_20 = value;
		Il2CppCodeGenWriteBarrier(&___selectedButtonScript_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
