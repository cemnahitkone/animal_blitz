﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TrailElement
struct TrailElement_t1685395368;
// SpriteTrail
struct SpriteTrail_t2284883325;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SpriteTrail2284883325.h"

// System.Void TrailElement::.ctor()
extern "C"  void TrailElement__ctor_m2219600159 (TrailElement_t1685395368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrailElement::ClearFreeElements()
extern "C"  void TrailElement_ClearFreeElements_m65079503 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrailElement::Initialise(SpriteTrail)
extern "C"  void TrailElement_Initialise_m1205103621 (TrailElement_t1685395368 * __this, SpriteTrail_t2284883325 * ___trail0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrailElement::Awake()
extern "C"  void TrailElement_Awake_m336711492 (TrailElement_t1685395368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrailElement::Update()
extern "C"  void TrailElement_Update_m3641136998 (TrailElement_t1685395368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrailElement::LateUpdate()
extern "C"  void TrailElement_LateUpdate_m3561634826 (TrailElement_t1685395368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrailElement::ApplyAddSpriteEffect(System.Int32)
extern "C"  void TrailElement_ApplyAddSpriteEffect_m2236787785 (TrailElement_t1685395368 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrailElement::ApplyFrameEffect()
extern "C"  void TrailElement_ApplyFrameEffect_m1827553553 (TrailElement_t1685395368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrailElement::ApplyModificationFromRatio(System.Single)
extern "C"  void TrailElement_ApplyModificationFromRatio_m4018682883 (TrailElement_t1685395368 * __this, float ___ratio0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject TrailElement::GetFreeElement()
extern "C"  GameObject_t1756533147 * TrailElement_GetFreeElement_m700256560 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrailElement::Hide(System.Boolean)
extern "C"  void TrailElement_Hide_m3076722378 (TrailElement_t1685395368 * __this, bool ___AddToFree0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrailElement::.cctor()
extern "C"  void TrailElement__cctor_m2464911458 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
