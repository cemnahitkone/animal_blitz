﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TittleAnimation
struct TittleAnimation_t2018927818;

#include "codegen/il2cpp-codegen.h"

// System.Void TittleAnimation::.ctor()
extern "C"  void TittleAnimation__ctor_m3558554646 (TittleAnimation_t2018927818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TittleAnimation::Start()
extern "C"  void TittleAnimation_Start_m3528589182 (TittleAnimation_t2018927818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TittleAnimation::Update()
extern "C"  void TittleAnimation_Update_m3752553301 (TittleAnimation_t2018927818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TittleAnimation::Main()
extern "C"  void TittleAnimation_Main_m497288007 (TittleAnimation_t2018927818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
