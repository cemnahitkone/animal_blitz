﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AnimationSelect
struct AnimationSelect_t1701553070;
// UnityEngine.Animation
struct Animation_t2068071072;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// BackgroundScroll
struct BackgroundScroll_t1444628405;
// UnityEngine.Renderer
struct Renderer_t257310565;
// BlendTwoAnimations
struct BlendTwoAnimations_t1525971836;
// CharacterMotor
struct CharacterMotor_t262030084;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t4070855101;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// CharacterMotor/$SubtractNewPlatformVelocity$19
struct U24SubtractNewPlatformVelocityU2419_t3518813419;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// CharacterMotor/$SubtractNewPlatformVelocity$19/$
struct U24_t1079724576;
// CharacterMotorJumping
struct CharacterMotorJumping_t1708272304;
// CharacterMotorMovement
struct CharacterMotorMovement_t944153967;
// CharacterMotorMovingPlatform
struct CharacterMotorMovingPlatform_t365475463;
// CharacterMotorSliding
struct CharacterMotorSliding_t3749388514;
// CharacterSpin
struct CharacterSpin_t1044941751;
// Cursor
struct Cursor_t2213425178;
// FPSInputController
struct FPSInputController_t4241249601;
// HideCursor
struct HideCursor_t48087058;
// JumpOnClick
struct JumpOnClick_t2456543707;
// UnityEngine.Collider
struct Collider_t3497673348;
// JumpTest
struct JumpTest_t640307972;
// PlatformInputController
struct PlatformInputController_t4273899755;
// Rotate
struct Rotate_t4255939432;
// SelectedButton
struct SelectedButton_t2241840337;
// Shadow
struct Shadow_t2876782136;
// SpeedTilt
struct SpeedTilt_t451906044;
// TittleAnimation
struct TittleAnimation_t2018927818;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// Translate
struct Translate_t3008136214;
// WaveMotion
struct WaveMotion_t1407033065;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DUnityScript_AnimationSelect1701553070.h"
#include "AssemblyU2DUnityScript_AnimationSelect1701553070MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation2068071072MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Animation2068071072.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DUnityScript_SelectedButton2241840337.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics634932869MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_Single2076509932.h"
#include "AssemblyU2DUnityScript_SelectedButton2241840337MethodDeclarations.h"
#include "AssemblyU2DUnityScript_JumpTest640307972.h"
#include "UnityScript_Lang_UnityScript_Lang_UnityRuntimeServ3303336867MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices1910041954MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationState1303741697MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationState1303741697.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DUnityScript_BackgroundScroll1444628405.h"
#include "AssemblyU2DUnityScript_BackgroundScroll1444628405MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "AssemblyU2DUnityScript_BlendTwoAnimations1525971836.h"
#include "AssemblyU2DUnityScript_BlendTwoAnimations1525971836MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationClip3510324950.h"
#include "AssemblyU2DUnityScript_CharacterMotor262030084.h"
#include "AssemblyU2DUnityScript_CharacterMotor262030084MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "AssemblyU2DUnityScript_CharacterMotorMovement944153967MethodDeclarations.h"
#include "AssemblyU2DUnityScript_CharacterMotorJumping1708272304MethodDeclarations.h"
#include "AssemblyU2DUnityScript_CharacterMotorMovingPlatform365475463MethodDeclarations.h"
#include "AssemblyU2DUnityScript_CharacterMotorSliding3749388514MethodDeclarations.h"
#include "AssemblyU2DUnityScript_CharacterMotorMovement944153967.h"
#include "AssemblyU2DUnityScript_CharacterMotorJumping1708272304.h"
#include "AssemblyU2DUnityScript_CharacterMotorMovingPlatform365475463.h"
#include "AssemblyU2DUnityScript_CharacterMotorSliding3749388514.h"
#include "UnityEngine_UnityEngine_CharacterController4094781467.h"
#include "UnityEngine_UnityEngine_CharacterController4094781467MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_CollisionFlags4046947985.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "AssemblyU2DUnityScript_MovementTransferOnJump3438008145.h"
#include "UnityEngine_UnityEngine_SendMessageOptions1414041951.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "AssemblyU2DUnityScript_CharacterMotor_U24SubtractN3518813419MethodDeclarations.h"
#include "AssemblyU2DUnityScript_CharacterMotor_U24SubtractN3518813419.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"
#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen3108987245MethodDeclarations.h"
#include "AssemblyU2DUnityScript_CharacterMotor_U24SubtractN1079724576MethodDeclarations.h"
#include "AssemblyU2DUnityScript_CharacterMotor_U24SubtractN1079724576.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen3445420457MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate3968615785MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen3445420457.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate3968615785.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340MethodDeclarations.h"
#include "AssemblyU2DUnityScript_CharacterSpin1044941751.h"
#include "AssemblyU2DUnityScript_CharacterSpin1044941751MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Cursor2213425178.h"
#include "AssemblyU2DUnityScript_Cursor2213425178MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "AssemblyU2DUnityScript_FPSInputController4241249601.h"
#include "AssemblyU2DUnityScript_FPSInputController4241249601MethodDeclarations.h"
#include "AssemblyU2DUnityScript_HideCursor48087058.h"
#include "AssemblyU2DUnityScript_HideCursor48087058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Cursor873194084MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "AssemblyU2DUnityScript_JumpOnClick2456543707.h"
#include "AssemblyU2DUnityScript_JumpOnClick2456543707MethodDeclarations.h"
#include "AssemblyU2DUnityScript_JumpTest640307972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayout2579273657MethodDeclarations.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOption4183744904.h"
#include "AssemblyU2DUnityScript_MovementTransferOnJump3438008145MethodDeclarations.h"
#include "AssemblyU2DUnityScript_PlatformInputController4273899755.h"
#include "AssemblyU2DUnityScript_PlatformInputController4273899755MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Rotate4255939431.h"
#include "AssemblyU2DUnityScript_Rotate4255939431MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Space4278750806.h"
#include "AssemblyU2DUnityScript_Shadow2876782136.h"
#include "AssemblyU2DUnityScript_Shadow2876782136MethodDeclarations.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DUnityScript_SpeedTilt451906044.h"
#include "AssemblyU2DUnityScript_SpeedTilt451906044MethodDeclarations.h"
#include "AssemblyU2DUnityScript_TittleAnimation2018927818.h"
#include "AssemblyU2DUnityScript_TittleAnimation2018927818MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Translate3008136214.h"
#include "AssemblyU2DUnityScript_Translate3008136214MethodDeclarations.h"
#include "AssemblyU2DUnityScript_WaveMotion1407033065.h"
#include "AssemblyU2DUnityScript_WaveMotion1407033065MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2650145732(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animation>()
#define GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(__this, method) ((  Animation_t2068071072 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t257310565_m141370815(__this, method) ((  Renderer_t257310565 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animation>()
#define Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, method) ((  Animation_t2068071072 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
#define Component_GetComponent_TisCollider_t3497673348_m2974738468(__this, method) ((  Collider_t3497673348 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInChildren_TisIl2CppObject_m3978412804_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m3978412804(__this, method) ((  ObjectU5BU5D_t3614634134* (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3978412804_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Transform>()
#define Component_GetComponentsInChildren_TisTransform_t3275118058_m3272947936(__this, method) ((  TransformU5BU5D_t3764228911* (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3978412804_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AnimationSelect::.ctor()
extern "C"  void AnimationSelect__ctor_m2218473122 (AnimationSelect_t1701553070 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationSelect::Start()
extern const Il2CppType* SelectedButton_t2241840337_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* SelectedButton_t2241840337_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2214702349;
extern Il2CppCodeGenString* _stringLiteral957794615;
extern Il2CppCodeGenString* _stringLiteral4185001875;
extern Il2CppCodeGenString* _stringLiteral3558811877;
extern Il2CppCodeGenString* _stringLiteral3337354156;
extern const uint32_t AnimationSelect_Start_m4276554258_MetadataUsageId;
extern "C"  void AnimationSelect_Start_m4276554258 (AnimationSelect_t1701553070 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationSelect_Start_m4276554258_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_dog_4();
		NullCheck(L_0);
		Animation_t2068071072 * L_1 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_1);
		Animation_Play_m976361057(L_1, _stringLiteral2214702349, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_cat_5();
		NullCheck(L_2);
		Animation_t2068071072 * L_3 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_2, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_3);
		Animation_Play_m976361057(L_3, _stringLiteral957794615, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_turtle_6();
		NullCheck(L_4);
		Animation_t2068071072 * L_5 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_4, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_5);
		Animation_Play_m976361057(L_5, _stringLiteral4185001875, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = __this->get_owl_7();
		NullCheck(L_6);
		Animation_t2068071072 * L_7 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_6, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_7);
		Animation_Play_m976361057(L_7, _stringLiteral3558811877, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = __this->get_rat_8();
		NullCheck(L_8);
		Animation_t2068071072 * L_9 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_8, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_9);
		Animation_Play_m976361057(L_9, _stringLiteral3337354156, /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = __this->get_selectedButton_19();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(SelectedButton_t2241840337_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		Component_t3819376471 * L_12 = Component_GetComponent_m4225719715(L_10, L_11, /*hidden argument*/NULL);
		__this->set_selectedButtonScript_20(((SelectedButton_t2241840337 *)CastclassClass(L_12, SelectedButton_t2241840337_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AnimationSelect::Update()
extern const Il2CppType* JumpTest_t640307972_0_0_0_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* RaycastHit_t87180320_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JumpTest_t640307972_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1266177044;
extern Il2CppCodeGenString* _stringLiteral3859689906;
extern Il2CppCodeGenString* _stringLiteral1238469632;
extern Il2CppCodeGenString* _stringLiteral1237912906;
extern Il2CppCodeGenString* _stringLiteral2515580378;
extern Il2CppCodeGenString* _stringLiteral1930985707;
extern Il2CppCodeGenString* _stringLiteral2778558984;
extern Il2CppCodeGenString* _stringLiteral2214736174;
extern Il2CppCodeGenString* _stringLiteral957828436;
extern Il2CppCodeGenString* _stringLiteral4221837234;
extern Il2CppCodeGenString* _stringLiteral3558845702;
extern Il2CppCodeGenString* _stringLiteral3337387977;
extern Il2CppCodeGenString* _stringLiteral1758206781;
extern Il2CppCodeGenString* _stringLiteral2062681337;
extern Il2CppCodeGenString* _stringLiteral899881999;
extern Il2CppCodeGenString* _stringLiteral1039728955;
extern Il2CppCodeGenString* _stringLiteral993990481;
extern Il2CppCodeGenString* _stringLiteral899881728;
extern Il2CppCodeGenString* _stringLiteral2659939651;
extern Il2CppCodeGenString* _stringLiteral790464395;
extern Il2CppCodeGenString* _stringLiteral340125689;
extern Il2CppCodeGenString* _stringLiteral2639511933;
extern Il2CppCodeGenString* _stringLiteral434234387;
extern Il2CppCodeGenString* _stringLiteral340126154;
extern Il2CppCodeGenString* _stringLiteral842948034;
extern Il2CppCodeGenString* _stringLiteral4123922542;
extern Il2CppCodeGenString* _stringLiteral2084789048;
extern Il2CppCodeGenString* _stringLiteral2293208678;
extern Il2CppCodeGenString* _stringLiteral629701712;
extern Il2CppCodeGenString* _stringLiteral2145397984;
extern Il2CppCodeGenString* _stringLiteral4208616433;
extern Il2CppCodeGenString* _stringLiteral2309169097;
extern Il2CppCodeGenString* _stringLiteral52761621;
extern Il2CppCodeGenString* _stringLiteral3897390051;
extern Il2CppCodeGenString* _stringLiteral4285502775;
extern Il2CppCodeGenString* _stringLiteral1121452413;
extern Il2CppCodeGenString* _stringLiteral3897389524;
extern Il2CppCodeGenString* _stringLiteral3399990044;
extern Il2CppCodeGenString* _stringLiteral1288891394;
extern Il2CppCodeGenString* _stringLiteral2095460340;
extern Il2CppCodeGenString* _stringLiteral2396966566;
extern Il2CppCodeGenString* _stringLiteral1645121530;
extern Il2CppCodeGenString* _stringLiteral2095459875;
extern Il2CppCodeGenString* _stringLiteral2583081087;
extern Il2CppCodeGenString* _stringLiteral1654152079;
extern Il2CppCodeGenString* _stringLiteral135122037;
extern Il2CppCodeGenString* _stringLiteral3042187665;
extern Il2CppCodeGenString* _stringLiteral585461095;
extern Il2CppCodeGenString* _stringLiteral135122758;
extern Il2CppCodeGenString* _stringLiteral2778558953;
extern Il2CppCodeGenString* _stringLiteral2214702349;
extern Il2CppCodeGenString* _stringLiteral957794615;
extern Il2CppCodeGenString* _stringLiteral4185001875;
extern Il2CppCodeGenString* _stringLiteral3558811877;
extern Il2CppCodeGenString* _stringLiteral3337354156;
extern const uint32_t AnimationSelect_Update_m304034889_MetadataUsageId;
extern "C"  void AnimationSelect_Update_m304034889 (AnimationSelect_t1701553070 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationSelect_Update_m304034889_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t2469606224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit_t87180320  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Transform_t3275118058 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_085e;
		}
	}
	{
		Camera_t189460977 * L_1 = __this->get_sceneCamera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_2 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Ray_t2469606224  L_3 = Camera_ScreenPointToRay_m614889538(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Initobj (RaycastHit_t87180320_il2cpp_TypeInfo_var, (&V_1));
		Ray_t2469606224  L_4 = V_0;
		bool L_5 = Physics_Raycast_m2736931691(NULL /*static, unused*/, L_4, (&V_1), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_085e;
		}
	}
	{
		Transform_t3275118058 * L_6 = RaycastHit_get_transform_m3290290036((&V_1), /*hidden argument*/NULL);
		V_2 = L_6;
		Transform_t3275118058 * L_7 = V_2;
		Transform_t3275118058 * L_8 = __this->get_idleButton_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0104;
		}
	}
	{
		String_t* L_10 = __this->get_currentAnimation_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_10, _stringLiteral1266177044, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00ff;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void AnimationSelect::BlendAllCharactersToZero() */, __this);
		SelectedButton_t2241840337 * L_12 = __this->get_selectedButtonScript_20();
		float L_13 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< float >::Invoke(6 /* System.Void SelectedButton::SetAnimationStart(System.Single) */, L_12, L_13);
		GameObject_t1756533147 * L_14 = __this->get_dog_4();
		NullCheck(L_14);
		Animation_t2068071072 * L_15 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_14, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_15);
		Animation_Blend_m4029095666(L_15, _stringLiteral3859689906, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_16 = __this->get_cat_5();
		NullCheck(L_16);
		Animation_t2068071072 * L_17 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_16, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_17);
		Animation_Blend_m4029095666(L_17, _stringLiteral1238469632, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_18 = __this->get_turtle_6();
		NullCheck(L_18);
		Animation_t2068071072 * L_19 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_18, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_19);
		Animation_Blend_m4029095666(L_19, _stringLiteral1237912906, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = __this->get_owl_7();
		NullCheck(L_20);
		Animation_t2068071072 * L_21 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_20, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_21);
		Animation_Blend_m4029095666(L_21, _stringLiteral2515580378, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_22 = __this->get_rat_8();
		NullCheck(L_22);
		Animation_t2068071072 * L_23 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_22, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_23);
		Animation_Blend_m4029095666(L_23, _stringLiteral1930985707, /*hidden argument*/NULL);
		Transform_t3275118058 * L_24 = __this->get_selectedButton_19();
		Transform_t3275118058 * L_25 = __this->get_idleButton_9();
		NullCheck(L_25);
		Vector3_t2243707580  L_26 = Transform_get_position_m1104419803(L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_position_m2469242620(L_24, L_26, /*hidden argument*/NULL);
		__this->set_currentAnimation_3(_stringLiteral1266177044);
	}

IL_00ff:
	{
		goto IL_085e;
	}

IL_0104:
	{
		Transform_t3275118058 * L_27 = V_2;
		Transform_t3275118058 * L_28 = __this->get_talkButton_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_29 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_01cf;
		}
	}
	{
		String_t* L_30 = __this->get_currentAnimation_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_31 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_30, _stringLiteral2778558984, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_01ca;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void AnimationSelect::BlendAllCharactersToZero() */, __this);
		SelectedButton_t2241840337 * L_32 = __this->get_selectedButtonScript_20();
		float L_33 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_32);
		VirtActionInvoker1< float >::Invoke(6 /* System.Void SelectedButton::SetAnimationStart(System.Single) */, L_32, L_33);
		GameObject_t1756533147 * L_34 = __this->get_dog_4();
		NullCheck(L_34);
		Animation_t2068071072 * L_35 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_34, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_35);
		Animation_Blend_m4029095666(L_35, _stringLiteral2214736174, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_36 = __this->get_cat_5();
		NullCheck(L_36);
		Animation_t2068071072 * L_37 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_36, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_37);
		Animation_Blend_m4029095666(L_37, _stringLiteral957828436, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_38 = __this->get_turtle_6();
		NullCheck(L_38);
		Animation_t2068071072 * L_39 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_38, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_39);
		Animation_Blend_m4029095666(L_39, _stringLiteral4221837234, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_40 = __this->get_owl_7();
		NullCheck(L_40);
		Animation_t2068071072 * L_41 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_40, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_41);
		Animation_Blend_m4029095666(L_41, _stringLiteral3558845702, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_42 = __this->get_rat_8();
		NullCheck(L_42);
		Animation_t2068071072 * L_43 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_42, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_43);
		Animation_Blend_m4029095666(L_43, _stringLiteral3337387977, /*hidden argument*/NULL);
		Transform_t3275118058 * L_44 = __this->get_selectedButton_19();
		Transform_t3275118058 * L_45 = __this->get_talkButton_10();
		NullCheck(L_45);
		Vector3_t2243707580  L_46 = Transform_get_position_m1104419803(L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		Transform_set_position_m2469242620(L_44, L_46, /*hidden argument*/NULL);
		__this->set_currentAnimation_3(_stringLiteral2778558984);
	}

IL_01ca:
	{
		goto IL_085e;
	}

IL_01cf:
	{
		Transform_t3275118058 * L_47 = V_2;
		Transform_t3275118058 * L_48 = __this->get_rollingButton_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_49 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_47, L_48, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_029a;
		}
	}
	{
		String_t* L_50 = __this->get_currentAnimation_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_51 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_50, _stringLiteral1758206781, /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_0295;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void AnimationSelect::BlendAllCharactersToZero() */, __this);
		SelectedButton_t2241840337 * L_52 = __this->get_selectedButtonScript_20();
		float L_53 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_52);
		VirtActionInvoker1< float >::Invoke(6 /* System.Void SelectedButton::SetAnimationStart(System.Single) */, L_52, L_53);
		GameObject_t1756533147 * L_54 = __this->get_dog_4();
		NullCheck(L_54);
		Animation_t2068071072 * L_55 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_54, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_55);
		Animation_Blend_m4029095666(L_55, _stringLiteral2062681337, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_56 = __this->get_cat_5();
		NullCheck(L_56);
		Animation_t2068071072 * L_57 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_56, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_57);
		Animation_Blend_m4029095666(L_57, _stringLiteral899881999, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_58 = __this->get_turtle_6();
		NullCheck(L_58);
		Animation_t2068071072 * L_59 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_58, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_59);
		Animation_Blend_m4029095666(L_59, _stringLiteral1039728955, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_60 = __this->get_owl_7();
		NullCheck(L_60);
		Animation_t2068071072 * L_61 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_60, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_61);
		Animation_Blend_m4029095666(L_61, _stringLiteral993990481, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_62 = __this->get_rat_8();
		NullCheck(L_62);
		Animation_t2068071072 * L_63 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_62, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_63);
		Animation_Blend_m4029095666(L_63, _stringLiteral899881728, /*hidden argument*/NULL);
		Transform_t3275118058 * L_64 = __this->get_selectedButton_19();
		Transform_t3275118058 * L_65 = __this->get_rollingButton_11();
		NullCheck(L_65);
		Vector3_t2243707580  L_66 = Transform_get_position_m1104419803(L_65, /*hidden argument*/NULL);
		NullCheck(L_64);
		Transform_set_position_m2469242620(L_64, L_66, /*hidden argument*/NULL);
		__this->set_currentAnimation_3(_stringLiteral1758206781);
	}

IL_0295:
	{
		goto IL_085e;
	}

IL_029a:
	{
		Transform_t3275118058 * L_67 = V_2;
		Transform_t3275118058 * L_68 = __this->get_successButton_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_69 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_67, L_68, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_0365;
		}
	}
	{
		String_t* L_70 = __this->get_currentAnimation_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_71 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_70, _stringLiteral2659939651, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_0360;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void AnimationSelect::BlendAllCharactersToZero() */, __this);
		SelectedButton_t2241840337 * L_72 = __this->get_selectedButtonScript_20();
		float L_73 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_72);
		VirtActionInvoker1< float >::Invoke(6 /* System.Void SelectedButton::SetAnimationStart(System.Single) */, L_72, L_73);
		GameObject_t1756533147 * L_74 = __this->get_dog_4();
		NullCheck(L_74);
		Animation_t2068071072 * L_75 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_74, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_75);
		Animation_Blend_m4029095666(L_75, _stringLiteral790464395, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_76 = __this->get_cat_5();
		NullCheck(L_76);
		Animation_t2068071072 * L_77 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_76, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_77);
		Animation_Blend_m4029095666(L_77, _stringLiteral340125689, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_78 = __this->get_turtle_6();
		NullCheck(L_78);
		Animation_t2068071072 * L_79 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_78, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_79);
		Animation_Blend_m4029095666(L_79, _stringLiteral2639511933, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_80 = __this->get_owl_7();
		NullCheck(L_80);
		Animation_t2068071072 * L_81 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_80, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_81);
		Animation_Blend_m4029095666(L_81, _stringLiteral434234387, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_82 = __this->get_rat_8();
		NullCheck(L_82);
		Animation_t2068071072 * L_83 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_82, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_83);
		Animation_Blend_m4029095666(L_83, _stringLiteral340126154, /*hidden argument*/NULL);
		Transform_t3275118058 * L_84 = __this->get_selectedButton_19();
		Transform_t3275118058 * L_85 = __this->get_successButton_12();
		NullCheck(L_85);
		Vector3_t2243707580  L_86 = Transform_get_position_m1104419803(L_85, /*hidden argument*/NULL);
		NullCheck(L_84);
		Transform_set_position_m2469242620(L_84, L_86, /*hidden argument*/NULL);
		__this->set_currentAnimation_3(_stringLiteral2659939651);
	}

IL_0360:
	{
		goto IL_085e;
	}

IL_0365:
	{
		Transform_t3275118058 * L_87 = V_2;
		Transform_t3275118058 * L_88 = __this->get_jumpButton_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_89 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_87, L_88, /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_0467;
		}
	}
	{
		String_t* L_90 = __this->get_currentAnimation_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_91 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_90, _stringLiteral842948034, /*hidden argument*/NULL);
		if (!L_91)
		{
			goto IL_0462;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void AnimationSelect::BlendAllCharactersToZero() */, __this);
		SelectedButton_t2241840337 * L_92 = __this->get_selectedButtonScript_20();
		float L_93 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_92);
		VirtActionInvoker1< float >::Invoke(6 /* System.Void SelectedButton::SetAnimationStart(System.Single) */, L_92, L_93);
		GameObject_t1756533147 * L_94 = __this->get_dog_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_95 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JumpTest_t640307972_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_94);
		Component_t3819376471 * L_96 = GameObject_GetComponent_m306258075(L_94, L_95, /*hidden argument*/NULL);
		NullCheck(((JumpTest_t640307972 *)CastclassClass(L_96, JumpTest_t640307972_il2cpp_TypeInfo_var)));
		Behaviour_set_enabled_m1796096907(((JumpTest_t640307972 *)CastclassClass(L_96, JumpTest_t640307972_il2cpp_TypeInfo_var)), (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_97 = __this->get_cat_5();
		Type_t * L_98 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JumpTest_t640307972_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_97);
		Component_t3819376471 * L_99 = GameObject_GetComponent_m306258075(L_97, L_98, /*hidden argument*/NULL);
		NullCheck(((JumpTest_t640307972 *)CastclassClass(L_99, JumpTest_t640307972_il2cpp_TypeInfo_var)));
		Behaviour_set_enabled_m1796096907(((JumpTest_t640307972 *)CastclassClass(L_99, JumpTest_t640307972_il2cpp_TypeInfo_var)), (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_100 = __this->get_turtle_6();
		Type_t * L_101 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JumpTest_t640307972_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_100);
		Component_t3819376471 * L_102 = GameObject_GetComponent_m306258075(L_100, L_101, /*hidden argument*/NULL);
		NullCheck(((JumpTest_t640307972 *)CastclassClass(L_102, JumpTest_t640307972_il2cpp_TypeInfo_var)));
		Behaviour_set_enabled_m1796096907(((JumpTest_t640307972 *)CastclassClass(L_102, JumpTest_t640307972_il2cpp_TypeInfo_var)), (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_103 = __this->get_owl_7();
		Type_t * L_104 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JumpTest_t640307972_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_103);
		Component_t3819376471 * L_105 = GameObject_GetComponent_m306258075(L_103, L_104, /*hidden argument*/NULL);
		NullCheck(((JumpTest_t640307972 *)CastclassClass(L_105, JumpTest_t640307972_il2cpp_TypeInfo_var)));
		Behaviour_set_enabled_m1796096907(((JumpTest_t640307972 *)CastclassClass(L_105, JumpTest_t640307972_il2cpp_TypeInfo_var)), (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_106 = __this->get_rat_8();
		Type_t * L_107 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JumpTest_t640307972_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_106);
		Component_t3819376471 * L_108 = GameObject_GetComponent_m306258075(L_106, L_107, /*hidden argument*/NULL);
		NullCheck(((JumpTest_t640307972 *)CastclassClass(L_108, JumpTest_t640307972_il2cpp_TypeInfo_var)));
		Behaviour_set_enabled_m1796096907(((JumpTest_t640307972 *)CastclassClass(L_108, JumpTest_t640307972_il2cpp_TypeInfo_var)), (bool)1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_109 = __this->get_selectedButton_19();
		Transform_t3275118058 * L_110 = __this->get_jumpButton_13();
		NullCheck(L_110);
		Vector3_t2243707580  L_111 = Transform_get_position_m1104419803(L_110, /*hidden argument*/NULL);
		NullCheck(L_109);
		Transform_set_position_m2469242620(L_109, L_111, /*hidden argument*/NULL);
		__this->set_currentAnimation_3(_stringLiteral842948034);
	}

IL_0462:
	{
		goto IL_085e;
	}

IL_0467:
	{
		Transform_t3275118058 * L_112 = V_2;
		Transform_t3275118058 * L_113 = __this->get_idle2Button_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_114 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_112, L_113, /*hidden argument*/NULL);
		if (!L_114)
		{
			goto IL_0532;
		}
	}
	{
		String_t* L_115 = __this->get_currentAnimation_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_116 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_115, _stringLiteral4123922542, /*hidden argument*/NULL);
		if (!L_116)
		{
			goto IL_052d;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void AnimationSelect::BlendAllCharactersToZero() */, __this);
		SelectedButton_t2241840337 * L_117 = __this->get_selectedButtonScript_20();
		float L_118 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_117);
		VirtActionInvoker1< float >::Invoke(6 /* System.Void SelectedButton::SetAnimationStart(System.Single) */, L_117, L_118);
		GameObject_t1756533147 * L_119 = __this->get_dog_4();
		NullCheck(L_119);
		Animation_t2068071072 * L_120 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_119, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_120);
		Animation_Blend_m4029095666(L_120, _stringLiteral2084789048, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_121 = __this->get_cat_5();
		NullCheck(L_121);
		Animation_t2068071072 * L_122 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_121, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_122);
		Animation_Blend_m4029095666(L_122, _stringLiteral2293208678, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_123 = __this->get_turtle_6();
		NullCheck(L_123);
		Animation_t2068071072 * L_124 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_123, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_124);
		Animation_Blend_m4029095666(L_124, _stringLiteral629701712, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_125 = __this->get_owl_7();
		NullCheck(L_125);
		Animation_t2068071072 * L_126 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_125, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_126);
		Animation_Blend_m4029095666(L_126, _stringLiteral2145397984, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_127 = __this->get_rat_8();
		NullCheck(L_127);
		Animation_t2068071072 * L_128 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_127, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_128);
		Animation_Blend_m4029095666(L_128, _stringLiteral4208616433, /*hidden argument*/NULL);
		Transform_t3275118058 * L_129 = __this->get_selectedButton_19();
		Transform_t3275118058 * L_130 = __this->get_idle2Button_14();
		NullCheck(L_130);
		Vector3_t2243707580  L_131 = Transform_get_position_m1104419803(L_130, /*hidden argument*/NULL);
		NullCheck(L_129);
		Transform_set_position_m2469242620(L_129, L_131, /*hidden argument*/NULL);
		__this->set_currentAnimation_3(_stringLiteral4123922542);
	}

IL_052d:
	{
		goto IL_085e;
	}

IL_0532:
	{
		Transform_t3275118058 * L_132 = V_2;
		Transform_t3275118058 * L_133 = __this->get_runButton_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_134 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_132, L_133, /*hidden argument*/NULL);
		if (!L_134)
		{
			goto IL_05fd;
		}
	}
	{
		String_t* L_135 = __this->get_currentAnimation_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_136 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_135, _stringLiteral2309169097, /*hidden argument*/NULL);
		if (!L_136)
		{
			goto IL_05f8;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void AnimationSelect::BlendAllCharactersToZero() */, __this);
		SelectedButton_t2241840337 * L_137 = __this->get_selectedButtonScript_20();
		float L_138 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_137);
		VirtActionInvoker1< float >::Invoke(6 /* System.Void SelectedButton::SetAnimationStart(System.Single) */, L_137, L_138);
		GameObject_t1756533147 * L_139 = __this->get_dog_4();
		NullCheck(L_139);
		Animation_t2068071072 * L_140 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_139, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_140);
		Animation_Blend_m4029095666(L_140, _stringLiteral52761621, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_141 = __this->get_cat_5();
		NullCheck(L_141);
		Animation_t2068071072 * L_142 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_141, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_142);
		Animation_Blend_m4029095666(L_142, _stringLiteral3897390051, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_143 = __this->get_turtle_6();
		NullCheck(L_143);
		Animation_t2068071072 * L_144 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_143, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_144);
		Animation_Blend_m4029095666(L_144, _stringLiteral4285502775, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_145 = __this->get_owl_7();
		NullCheck(L_145);
		Animation_t2068071072 * L_146 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_145, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_146);
		Animation_Blend_m4029095666(L_146, _stringLiteral1121452413, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_147 = __this->get_rat_8();
		NullCheck(L_147);
		Animation_t2068071072 * L_148 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_147, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_148);
		Animation_Blend_m4029095666(L_148, _stringLiteral3897389524, /*hidden argument*/NULL);
		Transform_t3275118058 * L_149 = __this->get_selectedButton_19();
		Transform_t3275118058 * L_150 = __this->get_runButton_15();
		NullCheck(L_150);
		Vector3_t2243707580  L_151 = Transform_get_position_m1104419803(L_150, /*hidden argument*/NULL);
		NullCheck(L_149);
		Transform_set_position_m2469242620(L_149, L_151, /*hidden argument*/NULL);
		__this->set_currentAnimation_3(_stringLiteral2309169097);
	}

IL_05f8:
	{
		goto IL_085e;
	}

IL_05fd:
	{
		Transform_t3275118058 * L_152 = V_2;
		Transform_t3275118058 * L_153 = __this->get_failureButton_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_154 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_152, L_153, /*hidden argument*/NULL);
		if (!L_154)
		{
			goto IL_06c8;
		}
	}
	{
		String_t* L_155 = __this->get_currentAnimation_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_156 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_155, _stringLiteral3399990044, /*hidden argument*/NULL);
		if (!L_156)
		{
			goto IL_06c3;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void AnimationSelect::BlendAllCharactersToZero() */, __this);
		SelectedButton_t2241840337 * L_157 = __this->get_selectedButtonScript_20();
		float L_158 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_157);
		VirtActionInvoker1< float >::Invoke(6 /* System.Void SelectedButton::SetAnimationStart(System.Single) */, L_157, L_158);
		GameObject_t1756533147 * L_159 = __this->get_dog_4();
		NullCheck(L_159);
		Animation_t2068071072 * L_160 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_159, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_160);
		Animation_Blend_m4029095666(L_160, _stringLiteral1288891394, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_161 = __this->get_cat_5();
		NullCheck(L_161);
		Animation_t2068071072 * L_162 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_161, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_162);
		Animation_Blend_m4029095666(L_162, _stringLiteral2095460340, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_163 = __this->get_turtle_6();
		NullCheck(L_163);
		Animation_t2068071072 * L_164 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_163, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_164);
		Animation_Blend_m4029095666(L_164, _stringLiteral2396966566, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_165 = __this->get_owl_7();
		NullCheck(L_165);
		Animation_t2068071072 * L_166 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_165, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_166);
		Animation_Blend_m4029095666(L_166, _stringLiteral1645121530, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_167 = __this->get_rat_8();
		NullCheck(L_167);
		Animation_t2068071072 * L_168 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_167, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_168);
		Animation_Blend_m4029095666(L_168, _stringLiteral2095459875, /*hidden argument*/NULL);
		Transform_t3275118058 * L_169 = __this->get_selectedButton_19();
		Transform_t3275118058 * L_170 = __this->get_failureButton_16();
		NullCheck(L_170);
		Vector3_t2243707580  L_171 = Transform_get_position_m1104419803(L_170, /*hidden argument*/NULL);
		NullCheck(L_169);
		Transform_set_position_m2469242620(L_169, L_171, /*hidden argument*/NULL);
		__this->set_currentAnimation_3(_stringLiteral3399990044);
	}

IL_06c3:
	{
		goto IL_085e;
	}

IL_06c8:
	{
		Transform_t3275118058 * L_172 = V_2;
		Transform_t3275118058 * L_173 = __this->get_sleepButton_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_174 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_172, L_173, /*hidden argument*/NULL);
		if (!L_174)
		{
			goto IL_0793;
		}
	}
	{
		String_t* L_175 = __this->get_currentAnimation_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_176 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_175, _stringLiteral2583081087, /*hidden argument*/NULL);
		if (!L_176)
		{
			goto IL_078e;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void AnimationSelect::BlendAllCharactersToZero() */, __this);
		SelectedButton_t2241840337 * L_177 = __this->get_selectedButtonScript_20();
		float L_178 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_177);
		VirtActionInvoker1< float >::Invoke(6 /* System.Void SelectedButton::SetAnimationStart(System.Single) */, L_177, L_178);
		GameObject_t1756533147 * L_179 = __this->get_dog_4();
		NullCheck(L_179);
		Animation_t2068071072 * L_180 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_179, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_180);
		Animation_Blend_m4029095666(L_180, _stringLiteral1654152079, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_181 = __this->get_cat_5();
		NullCheck(L_181);
		Animation_t2068071072 * L_182 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_181, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_182);
		Animation_Blend_m4029095666(L_182, _stringLiteral135122037, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_183 = __this->get_turtle_6();
		NullCheck(L_183);
		Animation_t2068071072 * L_184 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_183, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_184);
		Animation_Blend_m4029095666(L_184, _stringLiteral3042187665, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_185 = __this->get_owl_7();
		NullCheck(L_185);
		Animation_t2068071072 * L_186 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_185, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_186);
		Animation_Blend_m4029095666(L_186, _stringLiteral585461095, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_187 = __this->get_rat_8();
		NullCheck(L_187);
		Animation_t2068071072 * L_188 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_187, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_188);
		Animation_Blend_m4029095666(L_188, _stringLiteral135122758, /*hidden argument*/NULL);
		Transform_t3275118058 * L_189 = __this->get_selectedButton_19();
		Transform_t3275118058 * L_190 = __this->get_sleepButton_17();
		NullCheck(L_190);
		Vector3_t2243707580  L_191 = Transform_get_position_m1104419803(L_190, /*hidden argument*/NULL);
		NullCheck(L_189);
		Transform_set_position_m2469242620(L_189, L_191, /*hidden argument*/NULL);
		__this->set_currentAnimation_3(_stringLiteral2583081087);
	}

IL_078e:
	{
		goto IL_085e;
	}

IL_0793:
	{
		Transform_t3275118058 * L_192 = V_2;
		Transform_t3275118058 * L_193 = __this->get_walkButton_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_194 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_192, L_193, /*hidden argument*/NULL);
		if (!L_194)
		{
			goto IL_085e;
		}
	}
	{
		String_t* L_195 = __this->get_currentAnimation_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_196 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_195, _stringLiteral2778558953, /*hidden argument*/NULL);
		if (!L_196)
		{
			goto IL_0859;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void AnimationSelect::BlendAllCharactersToZero() */, __this);
		SelectedButton_t2241840337 * L_197 = __this->get_selectedButtonScript_20();
		float L_198 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_197);
		VirtActionInvoker1< float >::Invoke(6 /* System.Void SelectedButton::SetAnimationStart(System.Single) */, L_197, L_198);
		GameObject_t1756533147 * L_199 = __this->get_dog_4();
		NullCheck(L_199);
		Animation_t2068071072 * L_200 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_199, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_200);
		Animation_Blend_m4029095666(L_200, _stringLiteral2214702349, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_201 = __this->get_cat_5();
		NullCheck(L_201);
		Animation_t2068071072 * L_202 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_201, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_202);
		Animation_Blend_m4029095666(L_202, _stringLiteral957794615, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_203 = __this->get_turtle_6();
		NullCheck(L_203);
		Animation_t2068071072 * L_204 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_203, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_204);
		Animation_Blend_m4029095666(L_204, _stringLiteral4185001875, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_205 = __this->get_owl_7();
		NullCheck(L_205);
		Animation_t2068071072 * L_206 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_205, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_206);
		Animation_Blend_m4029095666(L_206, _stringLiteral3558811877, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_207 = __this->get_rat_8();
		NullCheck(L_207);
		Animation_t2068071072 * L_208 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_207, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		NullCheck(L_208);
		Animation_Blend_m4029095666(L_208, _stringLiteral3337354156, /*hidden argument*/NULL);
		Transform_t3275118058 * L_209 = __this->get_selectedButton_19();
		Transform_t3275118058 * L_210 = __this->get_walkButton_18();
		NullCheck(L_210);
		Vector3_t2243707580  L_211 = Transform_get_position_m1104419803(L_210, /*hidden argument*/NULL);
		NullCheck(L_209);
		Transform_set_position_m2469242620(L_209, L_211, /*hidden argument*/NULL);
		__this->set_currentAnimation_3(_stringLiteral2778558953);
	}

IL_0859:
	{
		goto IL_085e;
	}

IL_085e:
	{
		return;
	}
}
// System.Void AnimationSelect::BlendAllCharactersToZero()
extern const Il2CppType* JumpTest_t640307972_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JumpTest_t640307972_il2cpp_TypeInfo_var;
extern const uint32_t AnimationSelect_BlendAllCharactersToZero_m2146318859_MetadataUsageId;
extern "C"  void AnimationSelect_BlendAllCharactersToZero_m2146318859 (AnimationSelect_t1701553070 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationSelect_BlendAllCharactersToZero_m2146318859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_dog_4();
		VirtActionInvoker1< GameObject_t1756533147 * >::Invoke(7 /* System.Void AnimationSelect::BlendAllToZero(UnityEngine.GameObject) */, __this, L_0);
		GameObject_t1756533147 * L_1 = __this->get_cat_5();
		VirtActionInvoker1< GameObject_t1756533147 * >::Invoke(7 /* System.Void AnimationSelect::BlendAllToZero(UnityEngine.GameObject) */, __this, L_1);
		GameObject_t1756533147 * L_2 = __this->get_turtle_6();
		VirtActionInvoker1< GameObject_t1756533147 * >::Invoke(7 /* System.Void AnimationSelect::BlendAllToZero(UnityEngine.GameObject) */, __this, L_2);
		GameObject_t1756533147 * L_3 = __this->get_owl_7();
		VirtActionInvoker1< GameObject_t1756533147 * >::Invoke(7 /* System.Void AnimationSelect::BlendAllToZero(UnityEngine.GameObject) */, __this, L_3);
		GameObject_t1756533147 * L_4 = __this->get_rat_8();
		VirtActionInvoker1< GameObject_t1756533147 * >::Invoke(7 /* System.Void AnimationSelect::BlendAllToZero(UnityEngine.GameObject) */, __this, L_4);
		GameObject_t1756533147 * L_5 = __this->get_dog_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JumpTest_t640307972_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_5);
		Component_t3819376471 * L_7 = GameObject_GetComponent_m306258075(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(((JumpTest_t640307972 *)CastclassClass(L_7, JumpTest_t640307972_il2cpp_TypeInfo_var)));
		Behaviour_set_enabled_m1796096907(((JumpTest_t640307972 *)CastclassClass(L_7, JumpTest_t640307972_il2cpp_TypeInfo_var)), (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = __this->get_cat_5();
		Type_t * L_9 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JumpTest_t640307972_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_8);
		Component_t3819376471 * L_10 = GameObject_GetComponent_m306258075(L_8, L_9, /*hidden argument*/NULL);
		NullCheck(((JumpTest_t640307972 *)CastclassClass(L_10, JumpTest_t640307972_il2cpp_TypeInfo_var)));
		Behaviour_set_enabled_m1796096907(((JumpTest_t640307972 *)CastclassClass(L_10, JumpTest_t640307972_il2cpp_TypeInfo_var)), (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = __this->get_turtle_6();
		Type_t * L_12 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JumpTest_t640307972_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_11);
		Component_t3819376471 * L_13 = GameObject_GetComponent_m306258075(L_11, L_12, /*hidden argument*/NULL);
		NullCheck(((JumpTest_t640307972 *)CastclassClass(L_13, JumpTest_t640307972_il2cpp_TypeInfo_var)));
		Behaviour_set_enabled_m1796096907(((JumpTest_t640307972 *)CastclassClass(L_13, JumpTest_t640307972_il2cpp_TypeInfo_var)), (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_14 = __this->get_owl_7();
		Type_t * L_15 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JumpTest_t640307972_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_14);
		Component_t3819376471 * L_16 = GameObject_GetComponent_m306258075(L_14, L_15, /*hidden argument*/NULL);
		NullCheck(((JumpTest_t640307972 *)CastclassClass(L_16, JumpTest_t640307972_il2cpp_TypeInfo_var)));
		Behaviour_set_enabled_m1796096907(((JumpTest_t640307972 *)CastclassClass(L_16, JumpTest_t640307972_il2cpp_TypeInfo_var)), (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_17 = __this->get_rat_8();
		Type_t * L_18 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JumpTest_t640307972_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_17);
		Component_t3819376471 * L_19 = GameObject_GetComponent_m306258075(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(((JumpTest_t640307972 *)CastclassClass(L_19, JumpTest_t640307972_il2cpp_TypeInfo_var)));
		Behaviour_set_enabled_m1796096907(((JumpTest_t640307972 *)CastclassClass(L_19, JumpTest_t640307972_il2cpp_TypeInfo_var)), (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationSelect::BlendAllToZero(UnityEngine.GameObject)
extern const Il2CppType* AnimationState_t1303741697_0_0_0_var;
extern Il2CppClass* UnityRuntimeServices_t3303336867_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationState_t1303741697_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var;
extern const uint32_t AnimationSelect_BlendAllToZero_m2855194785_MetadataUsageId;
extern "C"  void AnimationSelect_BlendAllToZero_m2855194785 (AnimationSelect_t1701553070 * __this, GameObject_t1756533147 * ___character0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationSelect_BlendAllToZero_m2855194785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Animation_t2068071072 * V_0 = NULL;
	AnimationState_t1303741697 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	Il2CppObject * G_B2_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___character0;
		NullCheck(L_0);
		Animation_t2068071072 * L_1 = GameObject_GetComponent_TisAnimation_t2068071072_m3567791250(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimation_t2068071072_m3567791250_MethodInfo_var);
		V_0 = L_1;
		Animation_t2068071072 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityRuntimeServices_t3303336867_il2cpp_TypeInfo_var);
		Il2CppObject * L_3 = UnityRuntimeServices_GetEnumerator_m1135949016(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		goto IL_004e;
	}

IL_0013:
	{
		Il2CppObject * L_4 = V_2;
		NullCheck(L_4);
		Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_4);
		Il2CppObject * L_6 = L_5;
		G_B2_0 = L_6;
		if (((AnimationState_t1303741697 *)IsInstSealed(L_6, AnimationState_t1303741697_il2cpp_TypeInfo_var)))
		{
			G_B3_0 = L_6;
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(AnimationState_t1303741697_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = RuntimeServices_Coerce_m43784504(NULL /*static, unused*/, G_B2_0, L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0033:
	{
		V_1 = ((AnimationState_t1303741697 *)CastclassSealed(G_B3_0, AnimationState_t1303741697_il2cpp_TypeInfo_var));
		Animation_t2068071072 * L_9 = V_0;
		AnimationState_t1303741697 * L_10 = V_1;
		NullCheck(L_10);
		String_t* L_11 = AnimationState_get_name_m1043580151(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Animation_Blend_m1748264933(L_9, L_11, (((float)((float)0))), /*hidden argument*/NULL);
		Il2CppObject * L_12 = V_2;
		AnimationState_t1303741697 * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityRuntimeServices_t3303336867_il2cpp_TypeInfo_var);
		UnityRuntimeServices_Update_m1436857700(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
	}

IL_004e:
	{
		Il2CppObject * L_14 = V_2;
		NullCheck(L_14);
		bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_14);
		if (L_15)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void AnimationSelect::Main()
extern "C"  void AnimationSelect_Main_m3430688715 (AnimationSelect_t1701553070 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void BackgroundScroll::.ctor()
extern "C"  void BackgroundScroll__ctor_m2467004305 (BackgroundScroll_t1444628405 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BackgroundScroll::Start()
extern "C"  void BackgroundScroll_Start_m838642729 (BackgroundScroll_t1444628405 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void BackgroundScroll::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4026354833;
extern const uint32_t BackgroundScroll_Update_m3270540836_MetadataUsageId;
extern "C"  void BackgroundScroll_Update_m3270540836 (BackgroundScroll_t1444628405 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BackgroundScroll_Update_m3270540836_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Renderer_t257310565 * L_0 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var);
		NullCheck(L_0);
		Material_t193706927 * L_1 = Renderer_get_material_m2553789785(L_0, /*hidden argument*/NULL);
		float L_2 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = cosf(((float)((float)L_3*(float)(0.1f))));
		Vector2_t2243707579  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2__ctor_m3067419446(&L_5, ((float)((float)((-L_2))*(float)(0.1f))), L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		Material_SetTextureOffset_m3084369360(L_1, _stringLiteral4026354833, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BackgroundScroll::Main()
extern "C"  void BackgroundScroll_Main_m2678384016 (BackgroundScroll_t1444628405 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void BlendTwoAnimations::.ctor()
extern "C"  void BlendTwoAnimations__ctor_m1463316726 (BlendTwoAnimations_t1525971836 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BlendTwoAnimations::Start()
extern "C"  void BlendTwoAnimations_Start_m1690927234 (BlendTwoAnimations_t1525971836 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void BlendTwoAnimations::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var;
extern const uint32_t BlendTwoAnimations_Update_m3816926907_MetadataUsageId;
extern "C"  void BlendTwoAnimations_Update_m3816926907 (BlendTwoAnimations_t1525971836 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlendTwoAnimations_Update_m3816926907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_blend_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_blend_4(L_1);
		Animation_t2068071072 * L_2 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_3 = __this->get_firstAnimation_2();
		NullCheck(L_3);
		String_t* L_4 = Object_get_name_m2079638459(L_3, /*hidden argument*/NULL);
		float L_5 = __this->get_blend_4();
		NullCheck(L_2);
		Animation_Blend_m2530155026(L_2, L_4, L_5, (0.05f), /*hidden argument*/NULL);
		Animation_t2068071072 * L_6 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_7 = __this->get_secondAnimation_3();
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m2079638459(L_7, /*hidden argument*/NULL);
		float L_9 = __this->get_blend_4();
		NullCheck(L_6);
		Animation_Blend_m2530155026(L_6, L_8, ((float)((float)(((float)((float)1)))-(float)L_9)), (0.05f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void BlendTwoAnimations::Main()
extern "C"  void BlendTwoAnimations_Main_m2689698233 (BlendTwoAnimations_t1525971836 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CharacterMotor::.ctor()
extern Il2CppClass* CharacterMotorMovement_t944153967_il2cpp_TypeInfo_var;
extern Il2CppClass* CharacterMotorJumping_t1708272304_il2cpp_TypeInfo_var;
extern Il2CppClass* CharacterMotorMovingPlatform_t365475463_il2cpp_TypeInfo_var;
extern Il2CppClass* CharacterMotorSliding_t3749388514_il2cpp_TypeInfo_var;
extern const uint32_t CharacterMotor__ctor_m3361430202_MetadataUsageId;
extern "C"  void CharacterMotor__ctor_m3361430202 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor__ctor_m3361430202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_canControl_2((bool)1);
		__this->set_useFixedUpdate_3((bool)1);
		Vector3_t2243707580  L_0 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_inputMoveDirection_4(L_0);
		CharacterMotorMovement_t944153967 * L_1 = (CharacterMotorMovement_t944153967 *)il2cpp_codegen_object_new(CharacterMotorMovement_t944153967_il2cpp_TypeInfo_var);
		CharacterMotorMovement__ctor_m1502129899(L_1, /*hidden argument*/NULL);
		__this->set_movement_6(L_1);
		CharacterMotorJumping_t1708272304 * L_2 = (CharacterMotorJumping_t1708272304 *)il2cpp_codegen_object_new(CharacterMotorJumping_t1708272304_il2cpp_TypeInfo_var);
		CharacterMotorJumping__ctor_m1668297276(L_2, /*hidden argument*/NULL);
		__this->set_jumping_7(L_2);
		CharacterMotorMovingPlatform_t365475463 * L_3 = (CharacterMotorMovingPlatform_t365475463 *)il2cpp_codegen_object_new(CharacterMotorMovingPlatform_t365475463_il2cpp_TypeInfo_var);
		CharacterMotorMovingPlatform__ctor_m3422518867(L_3, /*hidden argument*/NULL);
		__this->set_movingPlatform_8(L_3);
		CharacterMotorSliding_t3749388514 * L_4 = (CharacterMotorSliding_t3749388514 *)il2cpp_codegen_object_new(CharacterMotorSliding_t3749388514_il2cpp_TypeInfo_var);
		CharacterMotorSliding__ctor_m2145873446(L_4, /*hidden argument*/NULL);
		__this->set_sliding_9(L_4);
		__this->set_grounded_10((bool)1);
		Vector3_t2243707580  L_5 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_groundNormal_11(L_5);
		Vector3_t2243707580  L_6 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastGroundNormal_12(L_6);
		return;
	}
}
// System.Void CharacterMotor::Awake()
extern const Il2CppType* CharacterController_t4094781467_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharacterController_t4094781467_il2cpp_TypeInfo_var;
extern const uint32_t CharacterMotor_Awake_m795959529_MetadataUsageId;
extern "C"  void CharacterMotor_Awake_m795959529 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_Awake_m795959529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(CharacterController_t4094781467_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_1 = Component_GetComponent_m4225719715(__this, L_0, /*hidden argument*/NULL);
		__this->set_controller_14(((CharacterController_t4094781467 *)CastclassSealed(L_1, CharacterController_t4094781467_il2cpp_TypeInfo_var)));
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_tr_13(L_2);
		return;
	}
}
// System.Void CharacterMotor::UpdateFunction()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral843240766;
extern Il2CppCodeGenString* _stringLiteral487010350;
extern const uint32_t CharacterMotor_UpdateFunction_m2269033867_MetadataUsageId;
extern "C"  void CharacterMotor_UpdateFunction_m2269033867 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_UpdateFunction_m2269033867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Quaternion_t4030073918  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Quaternion_t4030073918  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	{
		CharacterMotorMovement_t944153967 * L_0 = __this->get_movement_6();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = L_0->get_velocity_9();
		V_0 = L_1;
		Vector3_t2243707580  L_2 = V_0;
		Vector3_t2243707580  L_3 = CharacterMotor_ApplyInputVelocityChange_m4116974862(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector3_t2243707580  L_4 = V_0;
		Vector3_t2243707580  L_5 = CharacterMotor_ApplyGravityAndJumping_m3598474464(__this, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Vector3_t2243707580  L_6 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_6;
		bool L_7 = CharacterMotor_MoveWithPlatform_m4019888974(__this, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00dd;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_8 = __this->get_movingPlatform_8();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = L_8->get_activePlatform_3();
		CharacterMotorMovingPlatform_t365475463 * L_10 = __this->get_movingPlatform_8();
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = L_10->get_activeLocalPoint_4();
		NullCheck(L_9);
		Vector3_t2243707580  L_12 = Transform_TransformPoint_m3272254198(L_9, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		Vector3_t2243707580  L_13 = V_2;
		CharacterMotorMovingPlatform_t365475463 * L_14 = __this->get_movingPlatform_8();
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = L_14->get_activeGlobalPoint_5();
		Vector3_t2243707580  L_16 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		Vector3_t2243707580  L_17 = V_1;
		Vector3_t2243707580  L_18 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_19 = Vector3_op_Inequality_m799191452(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0078;
		}
	}
	{
		CharacterController_t4094781467 * L_20 = __this->get_controller_14();
		Vector3_t2243707580  L_21 = V_1;
		NullCheck(L_20);
		CharacterController_Move_m3456882757(L_20, L_21, /*hidden argument*/NULL);
	}

IL_0078:
	{
		CharacterMotorMovingPlatform_t365475463 * L_22 = __this->get_movingPlatform_8();
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = L_22->get_activePlatform_3();
		NullCheck(L_23);
		Quaternion_t4030073918  L_24 = Transform_get_rotation_m1033555130(L_23, /*hidden argument*/NULL);
		CharacterMotorMovingPlatform_t365475463 * L_25 = __this->get_movingPlatform_8();
		NullCheck(L_25);
		Quaternion_t4030073918  L_26 = L_25->get_activeLocalRotation_6();
		Quaternion_t4030073918  L_27 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_24, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		Quaternion_t4030073918  L_28 = V_3;
		CharacterMotorMovingPlatform_t365475463 * L_29 = __this->get_movingPlatform_8();
		NullCheck(L_29);
		Quaternion_t4030073918  L_30 = L_29->get_activeGlobalRotation_7();
		Quaternion_t4030073918  L_31 = Quaternion_Inverse_m3931399088(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_32 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_28, L_31, /*hidden argument*/NULL);
		V_4 = L_32;
		Vector3_t2243707580  L_33 = Quaternion_get_eulerAngles_m3302573991((&V_4), /*hidden argument*/NULL);
		V_12 = L_33;
		float L_34 = (&V_12)->get_y_2();
		V_5 = L_34;
		float L_35 = V_5;
		if ((((float)L_35) == ((float)(((float)((float)0))))))
		{
			goto IL_00dd;
		}
	}
	{
		Transform_t3275118058 * L_36 = __this->get_tr_13();
		float L_37 = V_5;
		NullCheck(L_36);
		Transform_Rotate_m4255273365(L_36, (((float)((float)0))), L_37, (((float)((float)0))), /*hidden argument*/NULL);
	}

IL_00dd:
	{
		Transform_t3275118058 * L_38 = __this->get_tr_13();
		NullCheck(L_38);
		Vector3_t2243707580  L_39 = Transform_get_position_m1104419803(L_38, /*hidden argument*/NULL);
		V_6 = L_39;
		Vector3_t2243707580  L_40 = V_0;
		float L_41 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_42 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		V_7 = L_42;
		CharacterController_t4094781467 * L_43 = __this->get_controller_14();
		NullCheck(L_43);
		float L_44 = CharacterController_get_stepOffset_m2492821916(L_43, /*hidden argument*/NULL);
		float L_45 = (&V_7)->get_x_1();
		float L_46 = (&V_7)->get_z_3();
		Vector3_t2243707580  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Vector3__ctor_m2638739322(&L_47, L_45, (((float)((float)0))), L_46, /*hidden argument*/NULL);
		V_13 = L_47;
		float L_48 = Vector3_get_magnitude_m860342598((&V_13), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_49 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_44, L_48, /*hidden argument*/NULL);
		V_8 = L_49;
		bool L_50 = __this->get_grounded_10();
		if (!L_50)
		{
			goto IL_0147;
		}
	}
	{
		Vector3_t2243707580  L_51 = V_7;
		float L_52 = V_8;
		Vector3_t2243707580  L_53 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_55 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_51, L_54, /*hidden argument*/NULL);
		V_7 = L_55;
	}

IL_0147:
	{
		CharacterMotorMovingPlatform_t365475463 * L_56 = __this->get_movingPlatform_8();
		NullCheck(L_56);
		L_56->set_hitPlatform_2((Transform_t3275118058 *)NULL);
		Vector3_t2243707580  L_57 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_groundNormal_11(L_57);
		CharacterMotorMovement_t944153967 * L_58 = __this->get_movement_6();
		CharacterController_t4094781467 * L_59 = __this->get_controller_14();
		Vector3_t2243707580  L_60 = V_7;
		NullCheck(L_59);
		int32_t L_61 = CharacterController_Move_m3456882757(L_59, L_60, /*hidden argument*/NULL);
		NullCheck(L_58);
		L_58->set_collisionFlags_8(L_61);
		CharacterMotorMovement_t944153967 * L_62 = __this->get_movement_6();
		CharacterMotorMovement_t944153967 * L_63 = __this->get_movement_6();
		NullCheck(L_63);
		Vector3_t2243707580  L_64 = L_63->get_hitPoint_11();
		NullCheck(L_62);
		L_62->set_lastHitPoint_12(L_64);
		Vector3_t2243707580  L_65 = __this->get_groundNormal_11();
		__this->set_lastGroundNormal_12(L_65);
		CharacterMotorMovingPlatform_t365475463 * L_66 = __this->get_movingPlatform_8();
		NullCheck(L_66);
		bool L_67 = L_66->get_enabled_0();
		if (!L_67)
		{
			goto IL_021b;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_68 = __this->get_movingPlatform_8();
		NullCheck(L_68);
		Transform_t3275118058 * L_69 = L_68->get_activePlatform_3();
		CharacterMotorMovingPlatform_t365475463 * L_70 = __this->get_movingPlatform_8();
		NullCheck(L_70);
		Transform_t3275118058 * L_71 = L_70->get_hitPlatform_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_72 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_69, L_71, /*hidden argument*/NULL);
		if (!L_72)
		{
			goto IL_021b;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_73 = __this->get_movingPlatform_8();
		NullCheck(L_73);
		Transform_t3275118058 * L_74 = L_73->get_hitPlatform_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_75 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_74, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_75)
		{
			goto IL_021b;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_76 = __this->get_movingPlatform_8();
		CharacterMotorMovingPlatform_t365475463 * L_77 = __this->get_movingPlatform_8();
		NullCheck(L_77);
		Transform_t3275118058 * L_78 = L_77->get_hitPlatform_2();
		NullCheck(L_76);
		L_76->set_activePlatform_3(L_78);
		CharacterMotorMovingPlatform_t365475463 * L_79 = __this->get_movingPlatform_8();
		CharacterMotorMovingPlatform_t365475463 * L_80 = __this->get_movingPlatform_8();
		NullCheck(L_80);
		Transform_t3275118058 * L_81 = L_80->get_hitPlatform_2();
		NullCheck(L_81);
		Matrix4x4_t2933234003  L_82 = Transform_get_localToWorldMatrix_m2868579006(L_81, /*hidden argument*/NULL);
		NullCheck(L_79);
		L_79->set_lastMatrix_8(L_82);
		CharacterMotorMovingPlatform_t365475463 * L_83 = __this->get_movingPlatform_8();
		NullCheck(L_83);
		L_83->set_newPlatform_10((bool)1);
	}

IL_021b:
	{
		float L_84 = (&V_0)->get_x_1();
		float L_85 = (&V_0)->get_z_3();
		Vector3_t2243707580  L_86;
		memset(&L_86, 0, sizeof(L_86));
		Vector3__ctor_m2638739322(&L_86, L_84, (((float)((float)0))), L_85, /*hidden argument*/NULL);
		V_9 = L_86;
		CharacterMotorMovement_t944153967 * L_87 = __this->get_movement_6();
		Transform_t3275118058 * L_88 = __this->get_tr_13();
		NullCheck(L_88);
		Vector3_t2243707580  L_89 = Transform_get_position_m1104419803(L_88, /*hidden argument*/NULL);
		Vector3_t2243707580  L_90 = V_6;
		Vector3_t2243707580  L_91 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_89, L_90, /*hidden argument*/NULL);
		float L_92 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_93 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_91, L_92, /*hidden argument*/NULL);
		NullCheck(L_87);
		L_87->set_velocity_9(L_93);
		CharacterMotorMovement_t944153967 * L_94 = __this->get_movement_6();
		NullCheck(L_94);
		Vector3_t2243707580 * L_95 = L_94->get_address_of_velocity_9();
		float L_96 = L_95->get_x_1();
		CharacterMotorMovement_t944153967 * L_97 = __this->get_movement_6();
		NullCheck(L_97);
		Vector3_t2243707580 * L_98 = L_97->get_address_of_velocity_9();
		float L_99 = L_98->get_z_3();
		Vector3_t2243707580  L_100;
		memset(&L_100, 0, sizeof(L_100));
		Vector3__ctor_m2638739322(&L_100, L_96, (((float)((float)0))), L_99, /*hidden argument*/NULL);
		V_10 = L_100;
		Vector3_t2243707580  L_101 = V_9;
		Vector3_t2243707580  L_102 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_103 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_101, L_102, /*hidden argument*/NULL);
		if (!L_103)
		{
			goto IL_02bc;
		}
	}
	{
		CharacterMotorMovement_t944153967 * L_104 = __this->get_movement_6();
		CharacterMotorMovement_t944153967 * L_105 = __this->get_movement_6();
		NullCheck(L_105);
		Vector3_t2243707580 * L_106 = L_105->get_address_of_velocity_9();
		float L_107 = L_106->get_y_2();
		Vector3_t2243707580  L_108;
		memset(&L_108, 0, sizeof(L_108));
		Vector3__ctor_m2638739322(&L_108, (((float)((float)0))), L_107, (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_104);
		L_104->set_velocity_9(L_108);
		goto IL_0307;
	}

IL_02bc:
	{
		Vector3_t2243707580  L_109 = V_10;
		Vector3_t2243707580  L_110 = V_9;
		float L_111 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_109, L_110, /*hidden argument*/NULL);
		float L_112 = Vector3_get_sqrMagnitude_m1814096310((&V_9), /*hidden argument*/NULL);
		V_11 = ((float)((float)L_111/(float)L_112));
		CharacterMotorMovement_t944153967 * L_113 = __this->get_movement_6();
		Vector3_t2243707580  L_114 = V_9;
		float L_115 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_116 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_115, /*hidden argument*/NULL);
		Vector3_t2243707580  L_117 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_114, L_116, /*hidden argument*/NULL);
		CharacterMotorMovement_t944153967 * L_118 = __this->get_movement_6();
		NullCheck(L_118);
		Vector3_t2243707580 * L_119 = L_118->get_address_of_velocity_9();
		float L_120 = L_119->get_y_2();
		Vector3_t2243707580  L_121 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_122 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_120, L_121, /*hidden argument*/NULL);
		Vector3_t2243707580  L_123 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_117, L_122, /*hidden argument*/NULL);
		NullCheck(L_113);
		L_113->set_velocity_9(L_123);
	}

IL_0307:
	{
		CharacterMotorMovement_t944153967 * L_124 = __this->get_movement_6();
		NullCheck(L_124);
		Vector3_t2243707580 * L_125 = L_124->get_address_of_velocity_9();
		float L_126 = L_125->get_y_2();
		float L_127 = (&V_0)->get_y_2();
		if ((((float)L_126) >= ((float)((float)((float)L_127-(float)(0.001f))))))
		{
			goto IL_0368;
		}
	}
	{
		CharacterMotorMovement_t944153967 * L_128 = __this->get_movement_6();
		NullCheck(L_128);
		Vector3_t2243707580 * L_129 = L_128->get_address_of_velocity_9();
		float L_130 = L_129->get_y_2();
		if ((((float)L_130) >= ((float)(((float)((float)0))))))
		{
			goto IL_035c;
		}
	}
	{
		CharacterMotorMovement_t944153967 * L_131 = __this->get_movement_6();
		NullCheck(L_131);
		Vector3_t2243707580 * L_132 = L_131->get_address_of_velocity_9();
		float L_133 = (&V_0)->get_y_2();
		L_132->set_y_2(L_133);
		goto IL_0368;
	}

IL_035c:
	{
		CharacterMotorJumping_t1708272304 * L_134 = __this->get_jumping_7();
		NullCheck(L_134);
		L_134->set_holdingJumpButton_6((bool)0);
	}

IL_0368:
	{
		bool L_135 = __this->get_grounded_10();
		if (!L_135)
		{
			goto IL_042b;
		}
	}
	{
		bool L_136 = CharacterMotor_IsGroundedTest_m3567633754(__this, /*hidden argument*/NULL);
		if (L_136)
		{
			goto IL_042b;
		}
	}
	{
		__this->set_grounded_10((bool)0);
		CharacterMotorMovingPlatform_t365475463 * L_137 = __this->get_movingPlatform_8();
		NullCheck(L_137);
		bool L_138 = L_137->get_enabled_0();
		if (!L_138)
		{
			goto IL_03f3;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_139 = __this->get_movingPlatform_8();
		NullCheck(L_139);
		int32_t L_140 = L_139->get_movementTransfer_1();
		if ((((int32_t)L_140) == ((int32_t)1)))
		{
			goto IL_03b7;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_141 = __this->get_movingPlatform_8();
		NullCheck(L_141);
		int32_t L_142 = L_141->get_movementTransfer_1();
		if ((!(((uint32_t)L_142) == ((uint32_t)2))))
		{
			goto IL_03f3;
		}
	}

IL_03b7:
	{
		CharacterMotorMovement_t944153967 * L_143 = __this->get_movement_6();
		CharacterMotorMovingPlatform_t365475463 * L_144 = __this->get_movingPlatform_8();
		NullCheck(L_144);
		Vector3_t2243707580  L_145 = L_144->get_platformVelocity_9();
		NullCheck(L_143);
		L_143->set_frameVelocity_10(L_145);
		CharacterMotorMovement_t944153967 * L_146 = __this->get_movement_6();
		CharacterMotorMovement_t944153967 * L_147 = __this->get_movement_6();
		NullCheck(L_147);
		Vector3_t2243707580  L_148 = L_147->get_velocity_9();
		CharacterMotorMovingPlatform_t365475463 * L_149 = __this->get_movingPlatform_8();
		NullCheck(L_149);
		Vector3_t2243707580  L_150 = L_149->get_platformVelocity_9();
		Vector3_t2243707580  L_151 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_148, L_150, /*hidden argument*/NULL);
		NullCheck(L_146);
		L_146->set_velocity_9(L_151);
	}

IL_03f3:
	{
		Component_SendMessage_m4199581575(__this, _stringLiteral843240766, 1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_152 = __this->get_tr_13();
		Transform_t3275118058 * L_153 = __this->get_tr_13();
		NullCheck(L_153);
		Vector3_t2243707580  L_154 = Transform_get_position_m1104419803(L_153, /*hidden argument*/NULL);
		float L_155 = V_8;
		Vector3_t2243707580  L_156 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_157 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_155, L_156, /*hidden argument*/NULL);
		Vector3_t2243707580  L_158 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_154, L_157, /*hidden argument*/NULL);
		NullCheck(L_152);
		Transform_set_position_m2469242620(L_152, L_158, /*hidden argument*/NULL);
		goto IL_046d;
	}

IL_042b:
	{
		bool L_159 = __this->get_grounded_10();
		if (L_159)
		{
			goto IL_046d;
		}
	}
	{
		bool L_160 = CharacterMotor_IsGroundedTest_m3567633754(__this, /*hidden argument*/NULL);
		if (!L_160)
		{
			goto IL_046d;
		}
	}
	{
		__this->set_grounded_10((bool)1);
		CharacterMotorJumping_t1708272304 * L_161 = __this->get_jumping_7();
		NullCheck(L_161);
		L_161->set_jumping_5((bool)0);
		Il2CppObject * L_162 = CharacterMotor_SubtractNewPlatformVelocity_m2006412010(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_162, /*hidden argument*/NULL);
		Component_SendMessage_m4199581575(__this, _stringLiteral487010350, 1, /*hidden argument*/NULL);
	}

IL_046d:
	{
		bool L_163 = CharacterMotor_MoveWithPlatform_m4019888974(__this, /*hidden argument*/NULL);
		if (!L_163)
		{
			goto IL_053b;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_164 = __this->get_movingPlatform_8();
		Transform_t3275118058 * L_165 = __this->get_tr_13();
		NullCheck(L_165);
		Vector3_t2243707580  L_166 = Transform_get_position_m1104419803(L_165, /*hidden argument*/NULL);
		Vector3_t2243707580  L_167 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		CharacterController_t4094781467 * L_168 = __this->get_controller_14();
		NullCheck(L_168);
		Vector3_t2243707580  L_169 = CharacterController_get_center_m3262687436(L_168, /*hidden argument*/NULL);
		V_14 = L_169;
		float L_170 = (&V_14)->get_y_2();
		CharacterController_t4094781467 * L_171 = __this->get_controller_14();
		NullCheck(L_171);
		float L_172 = CharacterController_get_height_m2830713110(L_171, /*hidden argument*/NULL);
		CharacterController_t4094781467 * L_173 = __this->get_controller_14();
		NullCheck(L_173);
		float L_174 = CharacterController_get_radius_m2751828411(L_173, /*hidden argument*/NULL);
		Vector3_t2243707580  L_175 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_167, ((float)((float)((float)((float)L_170-(float)((float)((float)L_172*(float)(0.5f)))))+(float)L_174)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_176 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_166, L_175, /*hidden argument*/NULL);
		NullCheck(L_164);
		L_164->set_activeGlobalPoint_5(L_176);
		CharacterMotorMovingPlatform_t365475463 * L_177 = __this->get_movingPlatform_8();
		CharacterMotorMovingPlatform_t365475463 * L_178 = __this->get_movingPlatform_8();
		NullCheck(L_178);
		Transform_t3275118058 * L_179 = L_178->get_activePlatform_3();
		CharacterMotorMovingPlatform_t365475463 * L_180 = __this->get_movingPlatform_8();
		NullCheck(L_180);
		Vector3_t2243707580  L_181 = L_180->get_activeGlobalPoint_5();
		NullCheck(L_179);
		Vector3_t2243707580  L_182 = Transform_InverseTransformPoint_m2648491174(L_179, L_181, /*hidden argument*/NULL);
		NullCheck(L_177);
		L_177->set_activeLocalPoint_4(L_182);
		CharacterMotorMovingPlatform_t365475463 * L_183 = __this->get_movingPlatform_8();
		Transform_t3275118058 * L_184 = __this->get_tr_13();
		NullCheck(L_184);
		Quaternion_t4030073918  L_185 = Transform_get_rotation_m1033555130(L_184, /*hidden argument*/NULL);
		NullCheck(L_183);
		L_183->set_activeGlobalRotation_7(L_185);
		CharacterMotorMovingPlatform_t365475463 * L_186 = __this->get_movingPlatform_8();
		CharacterMotorMovingPlatform_t365475463 * L_187 = __this->get_movingPlatform_8();
		NullCheck(L_187);
		Transform_t3275118058 * L_188 = L_187->get_activePlatform_3();
		NullCheck(L_188);
		Quaternion_t4030073918  L_189 = Transform_get_rotation_m1033555130(L_188, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_190 = Quaternion_Inverse_m3931399088(NULL /*static, unused*/, L_189, /*hidden argument*/NULL);
		CharacterMotorMovingPlatform_t365475463 * L_191 = __this->get_movingPlatform_8();
		NullCheck(L_191);
		Quaternion_t4030073918  L_192 = L_191->get_activeGlobalRotation_7();
		Quaternion_t4030073918  L_193 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_190, L_192, /*hidden argument*/NULL);
		NullCheck(L_186);
		L_186->set_activeLocalRotation_6(L_193);
	}

IL_053b:
	{
		return;
	}
}
// System.Void CharacterMotor::FixedUpdate()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t CharacterMotor_FixedUpdate_m1016746259_MetadataUsageId;
extern "C"  void CharacterMotor_FixedUpdate_m1016746259 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_FixedUpdate_m1016746259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t2933234003  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		CharacterMotorMovingPlatform_t365475463 * L_0 = __this->get_movingPlatform_8();
		NullCheck(L_0);
		bool L_1 = L_0->get_enabled_0();
		if (!L_1)
		{
			goto IL_00d6;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_2 = __this->get_movingPlatform_8();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = L_2->get_activePlatform_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00c6;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_5 = __this->get_movingPlatform_8();
		NullCheck(L_5);
		bool L_6 = L_5->get_newPlatform_10();
		if (L_6)
		{
			goto IL_009a;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_7 = __this->get_movingPlatform_8();
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = L_7->get_platformVelocity_9();
		V_0 = L_8;
		CharacterMotorMovingPlatform_t365475463 * L_9 = __this->get_movingPlatform_8();
		CharacterMotorMovingPlatform_t365475463 * L_10 = __this->get_movingPlatform_8();
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = L_10->get_activePlatform_3();
		NullCheck(L_11);
		Matrix4x4_t2933234003  L_12 = Transform_get_localToWorldMatrix_m2868579006(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		CharacterMotorMovingPlatform_t365475463 * L_13 = __this->get_movingPlatform_8();
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = L_13->get_activeLocalPoint_4();
		Vector3_t2243707580  L_15 = Matrix4x4_MultiplyPoint3x4_m1007952212((&V_1), L_14, /*hidden argument*/NULL);
		CharacterMotorMovingPlatform_t365475463 * L_16 = __this->get_movingPlatform_8();
		NullCheck(L_16);
		Matrix4x4_t2933234003 * L_17 = L_16->get_address_of_lastMatrix_8();
		CharacterMotorMovingPlatform_t365475463 * L_18 = __this->get_movingPlatform_8();
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = L_18->get_activeLocalPoint_4();
		Vector3_t2243707580  L_20 = Matrix4x4_MultiplyPoint3x4_m1007952212(L_17, L_19, /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_15, L_20, /*hidden argument*/NULL);
		float L_22 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_23 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_platformVelocity_9(L_23);
	}

IL_009a:
	{
		CharacterMotorMovingPlatform_t365475463 * L_24 = __this->get_movingPlatform_8();
		CharacterMotorMovingPlatform_t365475463 * L_25 = __this->get_movingPlatform_8();
		NullCheck(L_25);
		Transform_t3275118058 * L_26 = L_25->get_activePlatform_3();
		NullCheck(L_26);
		Matrix4x4_t2933234003  L_27 = Transform_get_localToWorldMatrix_m2868579006(L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		L_24->set_lastMatrix_8(L_27);
		CharacterMotorMovingPlatform_t365475463 * L_28 = __this->get_movingPlatform_8();
		NullCheck(L_28);
		L_28->set_newPlatform_10((bool)0);
		goto IL_00d6;
	}

IL_00c6:
	{
		CharacterMotorMovingPlatform_t365475463 * L_29 = __this->get_movingPlatform_8();
		Vector3_t2243707580  L_30 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_29);
		L_29->set_platformVelocity_9(L_30);
	}

IL_00d6:
	{
		bool L_31 = __this->get_useFixedUpdate_3();
		if (!L_31)
		{
			goto IL_00e7;
		}
	}
	{
		CharacterMotor_UpdateFunction_m2269033867(__this, /*hidden argument*/NULL);
	}

IL_00e7:
	{
		return;
	}
}
// System.Void CharacterMotor::Update()
extern "C"  void CharacterMotor_Update_m3811321445 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_useFixedUpdate_3();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		CharacterMotor_UpdateFunction_m2269033867(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// UnityEngine.Vector3 CharacterMotor::ApplyInputVelocityChange(UnityEngine.Vector3)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t CharacterMotor_ApplyInputVelocityChange_m4116974862_MetadataUsageId;
extern "C"  Vector3_t2243707580  CharacterMotor_ApplyInputVelocityChange_m4116974862 (CharacterMotor_t262030084 * __this, Vector3_t2243707580  ___velocity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_ApplyInputVelocityChange_m4116974862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		bool L_0 = __this->get_canControl_2();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_inputMoveDirection_4(L_1);
	}

IL_0016:
	{
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		bool L_2 = __this->get_grounded_10();
		if (!L_2)
		{
			goto IL_00b8;
		}
	}
	{
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean CharacterMotor::TooSteep() */, __this);
		if (!L_3)
		{
			goto IL_00b8;
		}
	}
	{
		Vector3_t2243707580 * L_4 = __this->get_address_of_groundNormal_11();
		float L_5 = L_4->get_x_1();
		Vector3_t2243707580 * L_6 = __this->get_address_of_groundNormal_11();
		float L_7 = L_6->get_z_3();
		Vector3_t2243707580  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m2638739322(&L_8, L_5, (((float)((float)0))), L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		Vector3_t2243707580  L_9 = Vector3_get_normalized_m936072361((&V_4), /*hidden argument*/NULL);
		V_0 = L_9;
		Vector3_t2243707580  L_10 = __this->get_inputMoveDirection_4();
		Vector3_t2243707580  L_11 = V_0;
		Vector3_t2243707580  L_12 = Vector3_Project_m1396027688(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		Vector3_t2243707580  L_13 = V_0;
		Vector3_t2243707580  L_14 = V_1;
		CharacterMotorSliding_t3749388514 * L_15 = __this->get_sliding_9();
		NullCheck(L_15);
		float L_16 = L_15->get_speedControl_3();
		Vector3_t2243707580  L_17 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/NULL);
		Vector3_t2243707580  L_18 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_13, L_17, /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = __this->get_inputMoveDirection_4();
		Vector3_t2243707580  L_20 = V_1;
		Vector3_t2243707580  L_21 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		CharacterMotorSliding_t3749388514 * L_22 = __this->get_sliding_9();
		NullCheck(L_22);
		float L_23 = L_22->get_sidewaysControl_2();
		Vector3_t2243707580  L_24 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_18, L_24, /*hidden argument*/NULL);
		V_0 = L_25;
		Vector3_t2243707580  L_26 = V_0;
		CharacterMotorSliding_t3749388514 * L_27 = __this->get_sliding_9();
		NullCheck(L_27);
		float L_28 = L_27->get_slidingSpeed_1();
		Vector3_t2243707580  L_29 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
		V_0 = L_29;
		goto IL_00bf;
	}

IL_00b8:
	{
		Vector3_t2243707580  L_30 = CharacterMotor_GetDesiredHorizontalVelocity_m555853785(__this, /*hidden argument*/NULL);
		V_0 = L_30;
	}

IL_00bf:
	{
		CharacterMotorMovingPlatform_t365475463 * L_31 = __this->get_movingPlatform_8();
		NullCheck(L_31);
		bool L_32 = L_31->get_enabled_0();
		if (!L_32)
		{
			goto IL_00fb;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_33 = __this->get_movingPlatform_8();
		NullCheck(L_33);
		int32_t L_34 = L_33->get_movementTransfer_1();
		if ((!(((uint32_t)L_34) == ((uint32_t)2))))
		{
			goto IL_00fb;
		}
	}
	{
		Vector3_t2243707580  L_35 = V_0;
		CharacterMotorMovement_t944153967 * L_36 = __this->get_movement_6();
		NullCheck(L_36);
		Vector3_t2243707580  L_37 = L_36->get_frameVelocity_10();
		Vector3_t2243707580  L_38 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_35, L_37, /*hidden argument*/NULL);
		V_0 = L_38;
		(&V_0)->set_y_2((((float)((float)0))));
	}

IL_00fb:
	{
		bool L_39 = __this->get_grounded_10();
		if (!L_39)
		{
			goto IL_0119;
		}
	}
	{
		Vector3_t2243707580  L_40 = V_0;
		Vector3_t2243707580  L_41 = __this->get_groundNormal_11();
		Vector3_t2243707580  L_42 = CharacterMotor_AdjustGroundVelocityToNormal_m3451990381(__this, L_40, L_41, /*hidden argument*/NULL);
		V_0 = L_42;
		goto IL_0126;
	}

IL_0119:
	{
		(&___velocity0)->set_y_2((((float)((float)0))));
	}

IL_0126:
	{
		bool L_43 = __this->get_grounded_10();
		float L_44 = VirtFuncInvoker1< float, bool >::Invoke(8 /* System.Single CharacterMotor::GetMaxAcceleration(System.Boolean) */, __this, L_43);
		float L_45 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_44*(float)L_45));
		Vector3_t2243707580  L_46 = V_0;
		Vector3_t2243707580  L_47 = ___velocity0;
		Vector3_t2243707580  L_48 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_46, L_47, /*hidden argument*/NULL);
		V_3 = L_48;
		float L_49 = Vector3_get_sqrMagnitude_m1814096310((&V_3), /*hidden argument*/NULL);
		float L_50 = V_2;
		float L_51 = V_2;
		if ((((float)L_49) <= ((float)((float)((float)L_50*(float)L_51)))))
		{
			goto IL_015e;
		}
	}
	{
		Vector3_t2243707580  L_52 = Vector3_get_normalized_m936072361((&V_3), /*hidden argument*/NULL);
		float L_53 = V_2;
		Vector3_t2243707580  L_54 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		V_3 = L_54;
	}

IL_015e:
	{
		bool L_55 = __this->get_grounded_10();
		if (L_55)
		{
			goto IL_0174;
		}
	}
	{
		bool L_56 = __this->get_canControl_2();
		if (!L_56)
		{
			goto IL_0181;
		}
	}

IL_0174:
	{
		Vector3_t2243707580  L_57 = ___velocity0;
		Vector3_t2243707580  L_58 = V_3;
		Vector3_t2243707580  L_59 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
		___velocity0 = L_59;
	}

IL_0181:
	{
		bool L_60 = __this->get_grounded_10();
		if (!L_60)
		{
			goto IL_01a9;
		}
	}
	{
		float L_61 = (&___velocity0)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_62 = Mathf_Min_m1648492575(NULL /*static, unused*/, L_61, (((float)((float)0))), /*hidden argument*/NULL);
		(&___velocity0)->set_y_2(L_62);
	}

IL_01a9:
	{
		Vector3_t2243707580  L_63 = ___velocity0;
		return L_63;
	}
}
// UnityEngine.Vector3 CharacterMotor::ApplyGravityAndJumping(UnityEngine.Vector3)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2053352307;
extern const uint32_t CharacterMotor_ApplyGravityAndJumping_m3598474464_MetadataUsageId;
extern "C"  Vector3_t2243707580  CharacterMotor_ApplyGravityAndJumping_m3598474464 (CharacterMotor_t262030084 * __this, Vector3_t2243707580  ___velocity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_ApplyGravityAndJumping_m3598474464_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_inputJump_5();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_canControl_2();
		if (L_1)
		{
			goto IL_0030;
		}
	}

IL_0016:
	{
		CharacterMotorJumping_t1708272304 * L_2 = __this->get_jumping_7();
		NullCheck(L_2);
		L_2->set_holdingJumpButton_6((bool)0);
		CharacterMotorJumping_t1708272304 * L_3 = __this->get_jumping_7();
		NullCheck(L_3);
		L_3->set_lastButtonDownTime_8((((float)((float)((int32_t)-100)))));
	}

IL_0030:
	{
		bool L_4 = __this->get_inputJump_5();
		if (!L_4)
		{
			goto IL_0068;
		}
	}
	{
		CharacterMotorJumping_t1708272304 * L_5 = __this->get_jumping_7();
		NullCheck(L_5);
		float L_6 = L_5->get_lastButtonDownTime_8();
		if ((((float)L_6) >= ((float)(((float)((float)0))))))
		{
			goto IL_0068;
		}
	}
	{
		bool L_7 = __this->get_canControl_2();
		if (!L_7)
		{
			goto IL_0068;
		}
	}
	{
		CharacterMotorJumping_t1708272304 * L_8 = __this->get_jumping_7();
		float L_9 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_lastButtonDownTime_8(L_9);
	}

IL_0068:
	{
		bool L_10 = __this->get_grounded_10();
		if (!L_10)
		{
			goto IL_00a7;
		}
	}
	{
		float L_11 = (&___velocity0)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Min_m1648492575(NULL /*static, unused*/, (((float)((float)0))), L_11, /*hidden argument*/NULL);
		CharacterMotorMovement_t944153967 * L_13 = __this->get_movement_6();
		NullCheck(L_13);
		float L_14 = L_13->get_gravity_6();
		float L_15 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&___velocity0)->set_y_2(((float)((float)L_12-(float)((float)((float)L_14*(float)L_15)))));
		goto IL_017f;
	}

IL_00a7:
	{
		CharacterMotorMovement_t944153967 * L_16 = __this->get_movement_6();
		NullCheck(L_16);
		Vector3_t2243707580 * L_17 = L_16->get_address_of_velocity_9();
		float L_18 = L_17->get_y_2();
		CharacterMotorMovement_t944153967 * L_19 = __this->get_movement_6();
		NullCheck(L_19);
		float L_20 = L_19->get_gravity_6();
		float L_21 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&___velocity0)->set_y_2(((float)((float)L_18-(float)((float)((float)L_20*(float)L_21)))));
		CharacterMotorJumping_t1708272304 * L_22 = __this->get_jumping_7();
		NullCheck(L_22);
		bool L_23 = L_22->get_jumping_5();
		if (!L_23)
		{
			goto IL_0158;
		}
	}
	{
		CharacterMotorJumping_t1708272304 * L_24 = __this->get_jumping_7();
		NullCheck(L_24);
		bool L_25 = L_24->get_holdingJumpButton_6();
		if (!L_25)
		{
			goto IL_0158;
		}
	}
	{
		float L_26 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		CharacterMotorJumping_t1708272304 * L_27 = __this->get_jumping_7();
		NullCheck(L_27);
		float L_28 = L_27->get_lastStartTime_7();
		CharacterMotorJumping_t1708272304 * L_29 = __this->get_jumping_7();
		NullCheck(L_29);
		float L_30 = L_29->get_extraHeight_2();
		CharacterMotorJumping_t1708272304 * L_31 = __this->get_jumping_7();
		NullCheck(L_31);
		float L_32 = L_31->get_baseHeight_1();
		float L_33 = VirtFuncInvoker1< float, float >::Invoke(9 /* System.Single CharacterMotor::CalculateJumpVerticalSpeed(System.Single) */, __this, L_32);
		if ((((float)L_26) >= ((float)((float)((float)L_28+(float)((float)((float)L_30/(float)L_33)))))))
		{
			goto IL_0158;
		}
	}
	{
		Vector3_t2243707580  L_34 = ___velocity0;
		CharacterMotorJumping_t1708272304 * L_35 = __this->get_jumping_7();
		NullCheck(L_35);
		Vector3_t2243707580  L_36 = L_35->get_jumpDir_9();
		CharacterMotorMovement_t944153967 * L_37 = __this->get_movement_6();
		NullCheck(L_37);
		float L_38 = L_37->get_gravity_6();
		Vector3_t2243707580  L_39 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_36, L_38, /*hidden argument*/NULL);
		float L_40 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_42 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_34, L_41, /*hidden argument*/NULL);
		___velocity0 = L_42;
	}

IL_0158:
	{
		float L_43 = (&___velocity0)->get_y_2();
		CharacterMotorMovement_t944153967 * L_44 = __this->get_movement_6();
		NullCheck(L_44);
		float L_45 = L_44->get_maxFallSpeed_7();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_46 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_43, ((-L_45)), /*hidden argument*/NULL);
		(&___velocity0)->set_y_2(L_46);
	}

IL_017f:
	{
		bool L_47 = __this->get_grounded_10();
		if (!L_47)
		{
			goto IL_030f;
		}
	}
	{
		CharacterMotorJumping_t1708272304 * L_48 = __this->get_jumping_7();
		NullCheck(L_48);
		bool L_49 = L_48->get_enabled_0();
		if (!L_49)
		{
			goto IL_0303;
		}
	}
	{
		bool L_50 = __this->get_canControl_2();
		if (!L_50)
		{
			goto IL_0303;
		}
	}
	{
		float L_51 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		CharacterMotorJumping_t1708272304 * L_52 = __this->get_jumping_7();
		NullCheck(L_52);
		float L_53 = L_52->get_lastButtonDownTime_8();
		if ((((float)((float)((float)L_51-(float)L_53))) >= ((float)(0.2f))))
		{
			goto IL_0303;
		}
	}
	{
		__this->set_grounded_10((bool)0);
		CharacterMotorJumping_t1708272304 * L_54 = __this->get_jumping_7();
		NullCheck(L_54);
		L_54->set_jumping_5((bool)1);
		CharacterMotorJumping_t1708272304 * L_55 = __this->get_jumping_7();
		float L_56 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_55);
		L_55->set_lastStartTime_7(L_56);
		CharacterMotorJumping_t1708272304 * L_57 = __this->get_jumping_7();
		NullCheck(L_57);
		L_57->set_lastButtonDownTime_8((((float)((float)((int32_t)-100)))));
		CharacterMotorJumping_t1708272304 * L_58 = __this->get_jumping_7();
		NullCheck(L_58);
		L_58->set_holdingJumpButton_6((bool)1);
		bool L_59 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean CharacterMotor::TooSteep() */, __this);
		if (!L_59)
		{
			goto IL_0233;
		}
	}
	{
		CharacterMotorJumping_t1708272304 * L_60 = __this->get_jumping_7();
		Vector3_t2243707580  L_61 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_62 = __this->get_groundNormal_11();
		CharacterMotorJumping_t1708272304 * L_63 = __this->get_jumping_7();
		NullCheck(L_63);
		float L_64 = L_63->get_steepPerpAmount_4();
		Vector3_t2243707580  L_65 = Vector3_Slerp_m846771032(NULL /*static, unused*/, L_61, L_62, L_64, /*hidden argument*/NULL);
		NullCheck(L_60);
		L_60->set_jumpDir_9(L_65);
		goto IL_0259;
	}

IL_0233:
	{
		CharacterMotorJumping_t1708272304 * L_66 = __this->get_jumping_7();
		Vector3_t2243707580  L_67 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_68 = __this->get_groundNormal_11();
		CharacterMotorJumping_t1708272304 * L_69 = __this->get_jumping_7();
		NullCheck(L_69);
		float L_70 = L_69->get_perpAmount_3();
		Vector3_t2243707580  L_71 = Vector3_Slerp_m846771032(NULL /*static, unused*/, L_67, L_68, L_70, /*hidden argument*/NULL);
		NullCheck(L_66);
		L_66->set_jumpDir_9(L_71);
	}

IL_0259:
	{
		(&___velocity0)->set_y_2((((float)((float)0))));
		Vector3_t2243707580  L_72 = ___velocity0;
		CharacterMotorJumping_t1708272304 * L_73 = __this->get_jumping_7();
		NullCheck(L_73);
		Vector3_t2243707580  L_74 = L_73->get_jumpDir_9();
		CharacterMotorJumping_t1708272304 * L_75 = __this->get_jumping_7();
		NullCheck(L_75);
		float L_76 = L_75->get_baseHeight_1();
		float L_77 = VirtFuncInvoker1< float, float >::Invoke(9 /* System.Single CharacterMotor::CalculateJumpVerticalSpeed(System.Single) */, __this, L_76);
		Vector3_t2243707580  L_78 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_74, L_77, /*hidden argument*/NULL);
		Vector3_t2243707580  L_79 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_72, L_78, /*hidden argument*/NULL);
		___velocity0 = L_79;
		CharacterMotorMovingPlatform_t365475463 * L_80 = __this->get_movingPlatform_8();
		NullCheck(L_80);
		bool L_81 = L_80->get_enabled_0();
		if (!L_81)
		{
			goto IL_02f2;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_82 = __this->get_movingPlatform_8();
		NullCheck(L_82);
		int32_t L_83 = L_82->get_movementTransfer_1();
		if ((((int32_t)L_83) == ((int32_t)1)))
		{
			goto IL_02c5;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_84 = __this->get_movingPlatform_8();
		NullCheck(L_84);
		int32_t L_85 = L_84->get_movementTransfer_1();
		if ((!(((uint32_t)L_85) == ((uint32_t)2))))
		{
			goto IL_02f2;
		}
	}

IL_02c5:
	{
		CharacterMotorMovement_t944153967 * L_86 = __this->get_movement_6();
		CharacterMotorMovingPlatform_t365475463 * L_87 = __this->get_movingPlatform_8();
		NullCheck(L_87);
		Vector3_t2243707580  L_88 = L_87->get_platformVelocity_9();
		NullCheck(L_86);
		L_86->set_frameVelocity_10(L_88);
		Vector3_t2243707580  L_89 = ___velocity0;
		CharacterMotorMovingPlatform_t365475463 * L_90 = __this->get_movingPlatform_8();
		NullCheck(L_90);
		Vector3_t2243707580  L_91 = L_90->get_platformVelocity_9();
		Vector3_t2243707580  L_92 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_89, L_91, /*hidden argument*/NULL);
		___velocity0 = L_92;
	}

IL_02f2:
	{
		Component_SendMessage_m4199581575(__this, _stringLiteral2053352307, 1, /*hidden argument*/NULL);
		goto IL_030f;
	}

IL_0303:
	{
		CharacterMotorJumping_t1708272304 * L_93 = __this->get_jumping_7();
		NullCheck(L_93);
		L_93->set_holdingJumpButton_6((bool)0);
	}

IL_030f:
	{
		Vector3_t2243707580  L_94 = ___velocity0;
		return L_94;
	}
}
// System.Void CharacterMotor::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void CharacterMotor_OnControllerColliderHit_m3731083134 (CharacterMotor_t262030084 * __this, ControllerColliderHit_t4070855101 * ___hit0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		ControllerColliderHit_t4070855101 * L_0 = ___hit0;
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = ControllerColliderHit_get_normal_m1098215280(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_2();
		if ((((float)L_2) <= ((float)(((float)((float)0))))))
		{
			goto IL_00d9;
		}
	}
	{
		ControllerColliderHit_t4070855101 * L_3 = ___hit0;
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = ControllerColliderHit_get_normal_m1098215280(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_y_2();
		Vector3_t2243707580 * L_6 = __this->get_address_of_groundNormal_11();
		float L_7 = L_6->get_y_2();
		if ((((float)L_5) <= ((float)L_7)))
		{
			goto IL_00d9;
		}
	}
	{
		ControllerColliderHit_t4070855101 * L_8 = ___hit0;
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = ControllerColliderHit_get_moveDirection_m3053186297(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = (&V_2)->get_y_2();
		if ((((float)L_10) >= ((float)(((float)((float)0))))))
		{
			goto IL_00d9;
		}
	}
	{
		ControllerColliderHit_t4070855101 * L_11 = ___hit0;
		NullCheck(L_11);
		Vector3_t2243707580  L_12 = ControllerColliderHit_get_point_m3573703281(L_11, /*hidden argument*/NULL);
		CharacterMotorMovement_t944153967 * L_13 = __this->get_movement_6();
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = L_13->get_lastHitPoint_12();
		Vector3_t2243707580  L_15 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = Vector3_get_sqrMagnitude_m1814096310((&V_3), /*hidden argument*/NULL);
		if ((((float)L_16) > ((float)(0.001f))))
		{
			goto IL_0085;
		}
	}
	{
		Vector3_t2243707580  L_17 = __this->get_lastGroundNormal_12();
		Vector3_t2243707580  L_18 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_19 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0096;
		}
	}

IL_0085:
	{
		ControllerColliderHit_t4070855101 * L_20 = ___hit0;
		NullCheck(L_20);
		Vector3_t2243707580  L_21 = ControllerColliderHit_get_normal_m1098215280(L_20, /*hidden argument*/NULL);
		__this->set_groundNormal_11(L_21);
		goto IL_00a2;
	}

IL_0096:
	{
		Vector3_t2243707580  L_22 = __this->get_lastGroundNormal_12();
		__this->set_groundNormal_11(L_22);
	}

IL_00a2:
	{
		CharacterMotorMovingPlatform_t365475463 * L_23 = __this->get_movingPlatform_8();
		ControllerColliderHit_t4070855101 * L_24 = ___hit0;
		NullCheck(L_24);
		Collider_t3497673348 * L_25 = ControllerColliderHit_get_collider_m3897495767(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_t3275118058 * L_26 = Component_get_transform_m2697483695(L_25, /*hidden argument*/NULL);
		NullCheck(L_23);
		L_23->set_hitPlatform_2(L_26);
		CharacterMotorMovement_t944153967 * L_27 = __this->get_movement_6();
		ControllerColliderHit_t4070855101 * L_28 = ___hit0;
		NullCheck(L_28);
		Vector3_t2243707580  L_29 = ControllerColliderHit_get_point_m3573703281(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		L_27->set_hitPoint_11(L_29);
		CharacterMotorMovement_t944153967 * L_30 = __this->get_movement_6();
		Vector3_t2243707580  L_31 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_30);
		L_30->set_frameVelocity_10(L_31);
	}

IL_00d9:
	{
		return;
	}
}
// System.Collections.IEnumerator CharacterMotor::SubtractNewPlatformVelocity()
extern Il2CppClass* U24SubtractNewPlatformVelocityU2419_t3518813419_il2cpp_TypeInfo_var;
extern const uint32_t CharacterMotor_SubtractNewPlatformVelocity_m2006412010_MetadataUsageId;
extern "C"  Il2CppObject * CharacterMotor_SubtractNewPlatformVelocity_m2006412010 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_SubtractNewPlatformVelocity_m2006412010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U24SubtractNewPlatformVelocityU2419_t3518813419 * L_0 = (U24SubtractNewPlatformVelocityU2419_t3518813419 *)il2cpp_codegen_object_new(U24SubtractNewPlatformVelocityU2419_t3518813419_il2cpp_TypeInfo_var);
		U24SubtractNewPlatformVelocityU2419__ctor_m1645740665(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24SubtractNewPlatformVelocityU2419_GetEnumerator_m1086755717(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean CharacterMotor::MoveWithPlatform()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t CharacterMotor_MoveWithPlatform_m4019888974_MetadataUsageId;
extern "C"  bool CharacterMotor_MoveWithPlatform_m4019888974 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_MoveWithPlatform_m4019888974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	bool G_B1_0 = false;
	bool G_B2_0 = false;
	int32_t G_B5_0 = 0;
	int32_t G_B4_0 = 0;
	{
		CharacterMotorMovingPlatform_t365475463 * L_0 = __this->get_movingPlatform_8();
		NullCheck(L_0);
		bool L_1 = L_0->get_enabled_0();
		bool L_2 = L_1;
		G_B1_0 = L_2;
		if (!L_2)
		{
			G_B3_0 = ((int32_t)(L_2));
			goto IL_002d;
		}
	}
	{
		bool L_3 = __this->get_grounded_10();
		bool L_4 = L_3;
		G_B2_0 = L_4;
		if (L_4)
		{
			G_B3_0 = ((int32_t)(L_4));
			goto IL_002d;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_5 = __this->get_movingPlatform_8();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_movementTransfer_1();
		G_B3_0 = ((((int32_t)L_6) == ((int32_t)3))? 1 : 0);
	}

IL_002d:
	{
		int32_t L_7 = G_B3_0;
		G_B4_0 = L_7;
		if (!L_7)
		{
			G_B5_0 = L_7;
			goto IL_0045;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_8 = __this->get_movingPlatform_8();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = L_8->get_activePlatform_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_10));
	}

IL_0045:
	{
		return (bool)G_B5_0;
	}
}
// UnityEngine.Vector3 CharacterMotor::GetDesiredHorizontalVelocity()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t CharacterMotor_GetDesiredHorizontalVelocity_m555853785_MetadataUsageId;
extern "C"  Vector3_t2243707580  CharacterMotor_GetDesiredHorizontalVelocity_m555853785 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_GetDesiredHorizontalVelocity_m555853785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Transform_t3275118058 * L_0 = __this->get_tr_13();
		Vector3_t2243707580  L_1 = __this->get_inputMoveDirection_4();
		NullCheck(L_0);
		Vector3_t2243707580  L_2 = Transform_InverseTransformDirection_m3595190459(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = V_0;
		float L_4 = VirtFuncInvoker1< float, Vector3_t2243707580  >::Invoke(17 /* System.Single CharacterMotor::MaxSpeedInDirection(UnityEngine.Vector3) */, __this, L_3);
		V_1 = L_4;
		bool L_5 = __this->get_grounded_10();
		if (!L_5)
		{
			goto IL_005d;
		}
	}
	{
		CharacterMotorMovement_t944153967 * L_6 = __this->get_movement_6();
		NullCheck(L_6);
		Vector3_t2243707580 * L_7 = L_6->get_address_of_velocity_9();
		Vector3_t2243707580  L_8 = Vector3_get_normalized_m936072361(L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = (&V_3)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_10 = asinf(L_9);
		V_2 = ((float)((float)L_10*(float)(57.29578f)));
		float L_11 = V_1;
		CharacterMotorMovement_t944153967 * L_12 = __this->get_movement_6();
		NullCheck(L_12);
		AnimationCurve_t3306541151 * L_13 = L_12->get_slopeSpeedMultiplier_3();
		float L_14 = V_2;
		NullCheck(L_13);
		float L_15 = AnimationCurve_Evaluate_m3698879322(L_13, L_14, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_11*(float)L_15));
	}

IL_005d:
	{
		Transform_t3275118058 * L_16 = __this->get_tr_13();
		Vector3_t2243707580  L_17 = V_0;
		float L_18 = V_1;
		Vector3_t2243707580  L_19 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t2243707580  L_20 = Transform_TransformDirection_m1639585047(L_16, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// UnityEngine.Vector3 CharacterMotor::AdjustGroundVelocityToNormal(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  CharacterMotor_AdjustGroundVelocityToNormal_m3451990381 (CharacterMotor_t262030084 * __this, Vector3_t2243707580  ___hVelocity0, Vector3_t2243707580  ___groundNormal1, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t2243707580  L_0 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = ___hVelocity0;
		Vector3_t2243707580  L_2 = Vector3_Cross_m4149044051(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = V_0;
		Vector3_t2243707580  L_4 = ___groundNormal1;
		Vector3_t2243707580  L_5 = Vector3_Cross_m4149044051(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector3_t2243707580  L_6 = Vector3_get_normalized_m936072361((&V_1), /*hidden argument*/NULL);
		float L_7 = Vector3_get_magnitude_m860342598((&___hVelocity0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean CharacterMotor::IsGroundedTest()
extern "C"  bool CharacterMotor_IsGroundedTest_m3567633754 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580 * L_0 = __this->get_address_of_groundNormal_11();
		float L_1 = L_0->get_y_2();
		return (bool)((((float)L_1) > ((float)(0.01f)))? 1 : 0);
	}
}
// System.Single CharacterMotor::GetMaxAcceleration(System.Boolean)
extern "C"  float CharacterMotor_GetMaxAcceleration_m1364223961 (CharacterMotor_t262030084 * __this, bool ___grounded0, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___grounded0;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CharacterMotorMovement_t944153967 * L_1 = __this->get_movement_6();
		NullCheck(L_1);
		float L_2 = L_1->get_maxGroundAcceleration_4();
		G_B3_0 = L_2;
		goto IL_0026;
	}

IL_0016:
	{
		CharacterMotorMovement_t944153967 * L_3 = __this->get_movement_6();
		NullCheck(L_3);
		float L_4 = L_3->get_maxAirAcceleration_5();
		G_B3_0 = L_4;
		goto IL_0026;
	}

IL_0026:
	{
		return G_B3_0;
	}
}
// System.Single CharacterMotor::CalculateJumpVerticalSpeed(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t CharacterMotor_CalculateJumpVerticalSpeed_m2495405676_MetadataUsageId;
extern "C"  float CharacterMotor_CalculateJumpVerticalSpeed_m2495405676 (CharacterMotor_t262030084 * __this, float ___targetJumpHeight0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_CalculateJumpVerticalSpeed_m2495405676_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___targetJumpHeight0;
		CharacterMotorMovement_t944153967 * L_1 = __this->get_movement_6();
		NullCheck(L_1);
		float L_2 = L_1->get_gravity_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = sqrtf(((float)((float)((float)((float)(((float)((float)2)))*(float)L_0))*(float)L_2)));
		return L_3;
	}
}
// System.Boolean CharacterMotor::IsJumping()
extern "C"  bool CharacterMotor_IsJumping_m149011362 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	{
		CharacterMotorJumping_t1708272304 * L_0 = __this->get_jumping_7();
		NullCheck(L_0);
		bool L_1 = L_0->get_jumping_5();
		return L_1;
	}
}
// System.Boolean CharacterMotor::IsSliding()
extern "C"  bool CharacterMotor_IsSliding_m1686387796 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	bool G_B2_0 = false;
	bool G_B1_0 = false;
	bool G_B4_0 = false;
	bool G_B3_0 = false;
	{
		bool L_0 = __this->get_grounded_10();
		bool L_1 = L_0;
		G_B1_0 = L_1;
		if (!L_1)
		{
			G_B2_0 = L_1;
			goto IL_0018;
		}
	}
	{
		CharacterMotorSliding_t3749388514 * L_2 = __this->get_sliding_9();
		NullCheck(L_2);
		bool L_3 = L_2->get_enabled_0();
		G_B2_0 = L_3;
	}

IL_0018:
	{
		bool L_4 = G_B2_0;
		G_B3_0 = L_4;
		if (!L_4)
		{
			G_B4_0 = L_4;
			goto IL_0025;
		}
	}
	{
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean CharacterMotor::TooSteep() */, __this);
		G_B4_0 = L_5;
	}

IL_0025:
	{
		return G_B4_0;
	}
}
// System.Boolean CharacterMotor::IsTouchingCeiling()
extern "C"  bool CharacterMotor_IsTouchingCeiling_m996322722 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	{
		CharacterMotorMovement_t944153967 * L_0 = __this->get_movement_6();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_collisionFlags_8();
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_1&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean CharacterMotor::IsGrounded()
extern "C"  bool CharacterMotor_IsGrounded_m2399482598 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_grounded_10();
		return L_0;
	}
}
// System.Boolean CharacterMotor::TooSteep()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t CharacterMotor_TooSteep_m4157442919_MetadataUsageId;
extern "C"  bool CharacterMotor_TooSteep_m4157442919 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_TooSteep_m4157442919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t2243707580 * L_0 = __this->get_address_of_groundNormal_11();
		float L_1 = L_0->get_y_2();
		CharacterController_t4094781467 * L_2 = __this->get_controller_14();
		NullCheck(L_2);
		float L_3 = CharacterController_get_slopeLimit_m1930597355(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = cosf(((float)((float)L_3*(float)(0.0174532924f))));
		return (bool)((((int32_t)((((float)L_1) > ((float)L_4))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Vector3 CharacterMotor::GetDirection()
extern "C"  Vector3_t2243707580  CharacterMotor_GetDirection_m1937069325 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_inputMoveDirection_4();
		return L_0;
	}
}
// System.Void CharacterMotor::SetControllable(System.Boolean)
extern "C"  void CharacterMotor_SetControllable_m3986683404 (CharacterMotor_t262030084 * __this, bool ___controllable0, const MethodInfo* method)
{
	{
		bool L_0 = ___controllable0;
		__this->set_canControl_2(L_0);
		return;
	}
}
// System.Single CharacterMotor::MaxSpeedInDirection(UnityEngine.Vector3)
extern "C"  float CharacterMotor_MaxSpeedInDirection_m1344582432 (CharacterMotor_t262030084 * __this, Vector3_t2243707580  ___desiredMovementDirection0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float G_B6_0 = 0.0f;
	float G_B5_0 = 0.0f;
	{
		Vector3_t2243707580  L_0 = ___desiredMovementDirection0;
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		G_B6_0 = (((float)((float)0)));
		goto IL_00ac;
	}

IL_0017:
	{
		float L_3 = (&___desiredMovementDirection0)->get_z_3();
		if ((((float)L_3) <= ((float)(((float)((float)0))))))
		{
			goto IL_0039;
		}
	}
	{
		CharacterMotorMovement_t944153967 * L_4 = __this->get_movement_6();
		NullCheck(L_4);
		float L_5 = L_4->get_maxForwardSpeed_0();
		G_B5_0 = L_5;
		goto IL_0044;
	}

IL_0039:
	{
		CharacterMotorMovement_t944153967 * L_6 = __this->get_movement_6();
		NullCheck(L_6);
		float L_7 = L_6->get_maxBackwardsSpeed_2();
		G_B5_0 = L_7;
	}

IL_0044:
	{
		CharacterMotorMovement_t944153967 * L_8 = __this->get_movement_6();
		NullCheck(L_8);
		float L_9 = L_8->get_maxSidewaysSpeed_1();
		V_0 = ((float)((float)G_B5_0/(float)L_9));
		float L_10 = (&___desiredMovementDirection0)->get_x_1();
		float L_11 = (&___desiredMovementDirection0)->get_z_3();
		float L_12 = V_0;
		Vector3_t2243707580  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m2638739322(&L_13, L_10, (((float)((float)0))), ((float)((float)L_11/(float)L_12)), /*hidden argument*/NULL);
		V_3 = L_13;
		Vector3_t2243707580  L_14 = Vector3_get_normalized_m936072361((&V_3), /*hidden argument*/NULL);
		V_1 = L_14;
		float L_15 = (&V_1)->get_x_1();
		float L_16 = (&V_1)->get_z_3();
		float L_17 = V_0;
		Vector3_t2243707580  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2638739322(&L_18, L_15, (((float)((float)0))), ((float)((float)L_16*(float)L_17)), /*hidden argument*/NULL);
		V_4 = L_18;
		float L_19 = Vector3_get_magnitude_m860342598((&V_4), /*hidden argument*/NULL);
		CharacterMotorMovement_t944153967 * L_20 = __this->get_movement_6();
		NullCheck(L_20);
		float L_21 = L_20->get_maxSidewaysSpeed_1();
		V_2 = ((float)((float)L_19*(float)L_21));
		float L_22 = V_2;
		G_B6_0 = L_22;
		goto IL_00ac;
	}

IL_00ac:
	{
		return G_B6_0;
	}
}
// System.Void CharacterMotor::SetVelocity(UnityEngine.Vector3)
extern Il2CppCodeGenString* _stringLiteral2724755691;
extern const uint32_t CharacterMotor_SetVelocity_m1132550922_MetadataUsageId;
extern "C"  void CharacterMotor_SetVelocity_m1132550922 (CharacterMotor_t262030084 * __this, Vector3_t2243707580  ___velocity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_SetVelocity_m1132550922_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_grounded_10((bool)0);
		CharacterMotorMovement_t944153967 * L_0 = __this->get_movement_6();
		Vector3_t2243707580  L_1 = ___velocity0;
		NullCheck(L_0);
		L_0->set_velocity_9(L_1);
		CharacterMotorMovement_t944153967 * L_2 = __this->get_movement_6();
		Vector3_t2243707580  L_3 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_frameVelocity_10(L_3);
		Component_SendMessage_m3615678587(__this, _stringLiteral2724755691, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterMotor::Main()
extern "C"  void CharacterMotor_Main_m2676787163 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CharacterMotor/$SubtractNewPlatformVelocity$19::.ctor(CharacterMotor)
extern const MethodInfo* GenericGenerator_1__ctor_m409073663_MethodInfo_var;
extern const uint32_t U24SubtractNewPlatformVelocityU2419__ctor_m1645740665_MetadataUsageId;
extern "C"  void U24SubtractNewPlatformVelocityU2419__ctor_m1645740665 (U24SubtractNewPlatformVelocityU2419_t3518813419 * __this, CharacterMotor_t262030084 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24SubtractNewPlatformVelocityU2419__ctor_m1645740665_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GenericGenerator_1__ctor_m409073663(__this, /*hidden argument*/GenericGenerator_1__ctor_m409073663_MethodInfo_var);
		CharacterMotor_t262030084 * L_0 = ___self_0;
		__this->set_U24self_U2422_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> CharacterMotor/$SubtractNewPlatformVelocity$19::GetEnumerator()
extern Il2CppClass* U24_t1079724576_il2cpp_TypeInfo_var;
extern const uint32_t U24SubtractNewPlatformVelocityU2419_GetEnumerator_m1086755717_MetadataUsageId;
extern "C"  Il2CppObject* U24SubtractNewPlatformVelocityU2419_GetEnumerator_m1086755717 (U24SubtractNewPlatformVelocityU2419_t3518813419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24SubtractNewPlatformVelocityU2419_GetEnumerator_m1086755717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharacterMotor_t262030084 * L_0 = __this->get_U24self_U2422_0();
		U24_t1079724576 * L_1 = (U24_t1079724576 *)il2cpp_codegen_object_new(U24_t1079724576_il2cpp_TypeInfo_var);
		U24__ctor_m3521653002(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void CharacterMotor/$SubtractNewPlatformVelocity$19/$::.ctor(CharacterMotor)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m509214543_MethodInfo_var;
extern const uint32_t U24__ctor_m3521653002_MetadataUsageId;
extern "C"  void U24__ctor_m3521653002 (U24_t1079724576 * __this, CharacterMotor_t262030084 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m3521653002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m509214543(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m509214543_MethodInfo_var);
		CharacterMotor_t262030084 * L_0 = ___self_0;
		__this->set_U24self_U2421_3(L_0);
		return;
	}
}
// System.Boolean CharacterMotor/$SubtractNewPlatformVelocity$19/$::MoveNext()
extern Il2CppClass* WaitForFixedUpdate_t3968615785_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m3421350270_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m799132706_MethodInfo_var;
extern const uint32_t U24_MoveNext_m2114066784_MetadataUsageId;
extern "C"  bool U24_MoveNext_m2114066784 (U24_t1079724576 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m2114066784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B13_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t3445420457 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_001f;
		}
		if (L_0 == 1)
		{
			goto IL_012c;
		}
		if (L_0 == 2)
		{
			goto IL_009c;
		}
		if (L_0 == 3)
		{
			goto IL_00ad;
		}
		if (L_0 == 4)
		{
			goto IL_00ef;
		}
	}

IL_001f:
	{
		CharacterMotor_t262030084 * L_1 = __this->get_U24self_U2421_3();
		NullCheck(L_1);
		CharacterMotorMovingPlatform_t365475463 * L_2 = L_1->get_movingPlatform_8();
		NullCheck(L_2);
		bool L_3 = L_2->get_enabled_0();
		if (!L_3)
		{
			goto IL_0124;
		}
	}
	{
		CharacterMotor_t262030084 * L_4 = __this->get_U24self_U2421_3();
		NullCheck(L_4);
		CharacterMotorMovingPlatform_t365475463 * L_5 = L_4->get_movingPlatform_8();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_movementTransfer_1();
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_0060;
		}
	}
	{
		CharacterMotor_t262030084 * L_7 = __this->get_U24self_U2421_3();
		NullCheck(L_7);
		CharacterMotorMovingPlatform_t365475463 * L_8 = L_7->get_movingPlatform_8();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_movementTransfer_1();
		if ((!(((uint32_t)L_9) == ((uint32_t)2))))
		{
			goto IL_0124;
		}
	}

IL_0060:
	{
		CharacterMotor_t262030084 * L_10 = __this->get_U24self_U2421_3();
		NullCheck(L_10);
		CharacterMotorMovingPlatform_t365475463 * L_11 = L_10->get_movingPlatform_8();
		NullCheck(L_11);
		bool L_12 = L_11->get_newPlatform_10();
		if (!L_12)
		{
			goto IL_00ef;
		}
	}
	{
		CharacterMotor_t262030084 * L_13 = __this->get_U24self_U2421_3();
		NullCheck(L_13);
		CharacterMotorMovingPlatform_t365475463 * L_14 = L_13->get_movingPlatform_8();
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = L_14->get_activePlatform_3();
		__this->set_U24platformU2420_2(L_15);
		WaitForFixedUpdate_t3968615785 * L_16 = (WaitForFixedUpdate_t3968615785 *)il2cpp_codegen_object_new(WaitForFixedUpdate_t3968615785_il2cpp_TypeInfo_var);
		WaitForFixedUpdate__ctor_m3781413380(L_16, /*hidden argument*/NULL);
		bool L_17 = GenericGeneratorEnumerator_1_Yield_m3421350270(__this, 2, L_16, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m3421350270_MethodInfo_var);
		G_B13_0 = ((int32_t)(L_17));
		goto IL_012d;
	}

IL_009c:
	{
		WaitForFixedUpdate_t3968615785 * L_18 = (WaitForFixedUpdate_t3968615785 *)il2cpp_codegen_object_new(WaitForFixedUpdate_t3968615785_il2cpp_TypeInfo_var);
		WaitForFixedUpdate__ctor_m3781413380(L_18, /*hidden argument*/NULL);
		bool L_19 = GenericGeneratorEnumerator_1_Yield_m3421350270(__this, 3, L_18, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m3421350270_MethodInfo_var);
		G_B13_0 = ((int32_t)(L_19));
		goto IL_012d;
	}

IL_00ad:
	{
		CharacterMotor_t262030084 * L_20 = __this->get_U24self_U2421_3();
		NullCheck(L_20);
		bool L_21 = L_20->get_grounded_10();
		if (!L_21)
		{
			goto IL_00ef;
		}
	}
	{
		Transform_t3275118058 * L_22 = __this->get_U24platformU2420_2();
		CharacterMotor_t262030084 * L_23 = __this->get_U24self_U2421_3();
		NullCheck(L_23);
		CharacterMotorMovingPlatform_t365475463 * L_24 = L_23->get_movingPlatform_8();
		NullCheck(L_24);
		Transform_t3275118058 * L_25 = L_24->get_activePlatform_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_22, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00ef;
		}
	}
	{
		int32_t L_27 = 1;
		Il2CppObject * L_28 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_27);
		bool L_29 = GenericGeneratorEnumerator_1_Yield_m3421350270(__this, 4, L_28, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m3421350270_MethodInfo_var);
		G_B13_0 = ((int32_t)(L_29));
		goto IL_012d;
	}

IL_00ef:
	{
		CharacterMotor_t262030084 * L_30 = __this->get_U24self_U2421_3();
		NullCheck(L_30);
		CharacterMotorMovement_t944153967 * L_31 = L_30->get_movement_6();
		CharacterMotor_t262030084 * L_32 = __this->get_U24self_U2421_3();
		NullCheck(L_32);
		CharacterMotorMovement_t944153967 * L_33 = L_32->get_movement_6();
		NullCheck(L_33);
		Vector3_t2243707580  L_34 = L_33->get_velocity_9();
		CharacterMotor_t262030084 * L_35 = __this->get_U24self_U2421_3();
		NullCheck(L_35);
		CharacterMotorMovingPlatform_t365475463 * L_36 = L_35->get_movingPlatform_8();
		NullCheck(L_36);
		Vector3_t2243707580  L_37 = L_36->get_platformVelocity_9();
		Vector3_t2243707580  L_38 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_34, L_37, /*hidden argument*/NULL);
		NullCheck(L_31);
		L_31->set_velocity_9(L_38);
	}

IL_0124:
	{
		GenericGeneratorEnumerator_1_YieldDefault_m799132706(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m799132706_MethodInfo_var);
	}

IL_012c:
	{
		G_B13_0 = 0;
	}

IL_012d:
	{
		return (bool)G_B13_0;
	}
}
// System.Void CharacterMotorJumping::.ctor()
extern "C"  void CharacterMotorJumping__ctor_m1668297276 (CharacterMotorJumping_t1708272304 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_enabled_0((bool)1);
		__this->set_baseHeight_1((1.0f));
		__this->set_extraHeight_2((4.1f));
		__this->set_steepPerpAmount_4((0.5f));
		__this->set_lastButtonDownTime_8((((float)((float)((int32_t)-100)))));
		Vector3_t2243707580  L_0 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_jumpDir_9(L_0);
		return;
	}
}
// System.Void CharacterMotorMovement::.ctor()
extern Il2CppClass* KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurve_t3306541151_il2cpp_TypeInfo_var;
extern const uint32_t CharacterMotorMovement__ctor_m1502129899_MetadataUsageId;
extern "C"  void CharacterMotorMovement__ctor_m1502129899 (CharacterMotorMovement_t944153967 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotorMovement__ctor_m1502129899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_maxForwardSpeed_0((10.0f));
		__this->set_maxSidewaysSpeed_1((10.0f));
		__this->set_maxBackwardsSpeed_2((10.0f));
		KeyframeU5BU5D_t449065829* L_0 = ((KeyframeU5BU5D_t449065829*)SZArrayNew(KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		Keyframe_t1449471340  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Keyframe__ctor_m2042404667(&L_1, (((float)((float)((int32_t)-90)))), (((float)((float)1))), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		KeyframeU5BU5D_t449065829* L_2 = L_0;
		NullCheck(L_2);
		Keyframe_t1449471340  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Keyframe__ctor_m2042404667(&L_3, (((float)((float)0))), (((float)((float)1))), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		KeyframeU5BU5D_t449065829* L_4 = L_2;
		NullCheck(L_4);
		Keyframe_t1449471340  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Keyframe__ctor_m2042404667(&L_5, (((float)((float)((int32_t)90)))), (((float)((float)0))), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_5;
		AnimationCurve_t3306541151 * L_6 = (AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2814448007(L_6, L_4, /*hidden argument*/NULL);
		__this->set_slopeSpeedMultiplier_3(L_6);
		__this->set_maxGroundAcceleration_4((30.0f));
		__this->set_maxAirAcceleration_5((20.0f));
		__this->set_gravity_6((10.0f));
		__this->set_maxFallSpeed_7((20.0f));
		Vector3_t2243707580  L_7 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_frameVelocity_10(L_7);
		Vector3_t2243707580  L_8 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_hitPoint_11(L_8);
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2638739322(&L_9, (std::numeric_limits<float>::infinity()), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		__this->set_lastHitPoint_12(L_9);
		return;
	}
}
// System.Void CharacterMotorMovingPlatform::.ctor()
extern "C"  void CharacterMotorMovingPlatform__ctor_m3422518867 (CharacterMotorMovingPlatform_t365475463 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_enabled_0((bool)1);
		__this->set_movementTransfer_1(2);
		return;
	}
}
// System.Void CharacterMotorSliding::.ctor()
extern "C"  void CharacterMotorSliding__ctor_m2145873446 (CharacterMotorSliding_t3749388514 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_enabled_0((bool)1);
		__this->set_slidingSpeed_1((((float)((float)((int32_t)15)))));
		__this->set_sidewaysControl_2((1.0f));
		__this->set_speedControl_3((0.4f));
		return;
	}
}
// System.Void CharacterSpin::.ctor()
extern "C"  void CharacterSpin__ctor_m901834203 (CharacterSpin_t1044941751 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_speed_3((25.0f));
		__this->set_bias_4((1.0f));
		return;
	}
}
// System.Void CharacterSpin::Start()
extern "C"  void CharacterSpin_Start_m1628683855 (CharacterSpin_t1044941751 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CharacterSpin::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t CharacterSpin_Update_m1963858220_MetadataUsageId;
extern "C"  void CharacterSpin_Update_m1963858220 (CharacterSpin_t1044941751 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterSpin_Update_m1963858220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t4030073918  L_1 = Transform_get_rotation_m1033555130(L_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = __this->get_sceneCamera_2();
		NullCheck(L_2);
		Quaternion_t4030073918  L_3 = Transform_get_rotation_m1033555130(L_2, /*hidden argument*/NULL);
		float L_4 = Quaternion_Dot_m952616600(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_5 = fabsf(L_4);
		__this->set_facingCamera_5(L_5);
		float L_6 = __this->get_bias_4();
		float L_7 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_6, (0.01f), /*hidden argument*/NULL);
		__this->set_bias_4(L_7);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = __this->get_speed_3();
		Vector3_t2243707580  L_11 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		float L_12 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		float L_14 = __this->get_facingCamera_5();
		float L_15 = __this->get_bias_4();
		Vector3_t2243707580  L_16 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_13, ((float)((float)L_14+(float)((float)((float)(((float)((float)1)))/(float)L_15)))), /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_Rotate_m1743927093(L_8, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterSpin::Main()
extern "C"  void CharacterSpin_Main_m2315659268 (CharacterSpin_t1044941751 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Cursor::.ctor()
extern "C"  void Cursor__ctor_m2004136328 (Cursor_t2213425178 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_clickImageDuration_6((0.05f));
		return;
	}
}
// System.Void Cursor::Start()
extern "C"  void Cursor_Start_m3626726184 (Cursor_t2213425178 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Cursor::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4026354833;
extern const uint32_t Cursor_Update_m2213252325_MetadataUsageId;
extern "C"  void Cursor_Update_m2213252325 (Cursor_t2213425178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cursor_Update_m2213252325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Camera_t189460977 * L_1 = __this->get_sceneCamera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_2 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_1();
		Vector3_t2243707580  L_4 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_y_2();
		Vector3_t2243707580  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2638739322(&L_6, L_3, L_5, (1.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_7 = Camera_ScreenToWorldPoint_m929392728(L_1, L_6, /*hidden argument*/NULL);
		Camera_t189460977 * L_8 = __this->get_sceneCamera_2();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(L_8, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_10 = __this->get_address_of_offset_3();
		float L_11 = L_10->get_x_0();
		Vector2_t2243707579 * L_12 = __this->get_address_of_offset_3();
		float L_13 = L_12->get_y_1();
		NullCheck(L_9);
		Vector3_t2243707580  L_14 = Transform_TransformDirection_m382623859(L_9, L_11, L_13, (((float)((float)0))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_7, L_14, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m2469242620(L_0, L_15, /*hidden argument*/NULL);
		bool L_16 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_007d;
		}
	}
	{
		float L_17 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastClickTime_7(L_17);
	}

IL_007d:
	{
		float L_18 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_19 = __this->get_lastClickTime_7();
		float L_20 = __this->get_clickImageDuration_6();
		if ((((float)L_18) <= ((float)((float)((float)L_19+(float)L_20)))))
		{
			goto IL_00b4;
		}
	}
	{
		Renderer_t257310565 * L_21 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var);
		NullCheck(L_21);
		Material_t193706927 * L_22 = Renderer_get_material_m2553789785(L_21, /*hidden argument*/NULL);
		Texture_t2243626319 * L_23 = __this->get_defaultTexture_4();
		NullCheck(L_22);
		Material_SetTexture_m141095205(L_22, _stringLiteral4026354833, L_23, /*hidden argument*/NULL);
		goto IL_00cf;
	}

IL_00b4:
	{
		Renderer_t257310565 * L_24 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var);
		NullCheck(L_24);
		Material_t193706927 * L_25 = Renderer_get_material_m2553789785(L_24, /*hidden argument*/NULL);
		Texture_t2243626319 * L_26 = __this->get_clickTexture_5();
		NullCheck(L_25);
		Material_SetTexture_m141095205(L_25, _stringLiteral4026354833, L_26, /*hidden argument*/NULL);
	}

IL_00cf:
	{
		return;
	}
}
// System.Void Cursor::Main()
extern "C"  void Cursor_Main_m3802382111 (Cursor_t2213425178 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FPSInputController::.ctor()
extern "C"  void FPSInputController__ctor_m2375921577 (FPSInputController_t4241249601 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FPSInputController::Awake()
extern const Il2CppType* CharacterMotor_t262030084_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharacterMotor_t262030084_il2cpp_TypeInfo_var;
extern const uint32_t FPSInputController_Awake_m1855081626_MetadataUsageId;
extern "C"  void FPSInputController_Awake_m1855081626 (FPSInputController_t4241249601 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FPSInputController_Awake_m1855081626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(CharacterMotor_t262030084_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_1 = Component_GetComponent_m4225719715(__this, L_0, /*hidden argument*/NULL);
		__this->set_motor_2(((CharacterMotor_t262030084 *)CastclassClass(L_1, CharacterMotor_t262030084_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FPSInputController::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral855845486;
extern Il2CppCodeGenString* _stringLiteral1635882288;
extern Il2CppCodeGenString* _stringLiteral842948034;
extern const uint32_t FPSInputController_Update_m3258574412_MetadataUsageId;
extern "C"  void FPSInputController_Update_m3258574412 (FPSInputController_t4241249601 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FPSInputController_Update_m3258574412_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		float L_1 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1635882288, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m2638739322(&L_2, L_0, (((float)((float)0))), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = V_0;
		Vector3_t2243707580  L_4 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = Vector3_op_Inequality_m799191452(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0051;
		}
	}
	{
		float L_6 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		V_1 = L_6;
		Vector3_t2243707580  L_7 = V_0;
		float L_8 = V_1;
		Vector3_t2243707580  L_9 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		float L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_11 = Mathf_Min_m1648492575(NULL /*static, unused*/, (((float)((float)1))), L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		float L_12 = V_1;
		float L_13 = V_1;
		V_1 = ((float)((float)L_12*(float)L_13));
		Vector3_t2243707580  L_14 = V_0;
		float L_15 = V_1;
		Vector3_t2243707580  L_16 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
	}

IL_0051:
	{
		CharacterMotor_t262030084 * L_17 = __this->get_motor_2();
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Quaternion_t4030073918  L_19 = Transform_get_rotation_m1033555130(L_18, /*hidden argument*/NULL);
		Vector3_t2243707580  L_20 = V_0;
		Vector3_t2243707580  L_21 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_17);
		L_17->set_inputMoveDirection_4(L_21);
		CharacterMotor_t262030084 * L_22 = __this->get_motor_2();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_23 = Input_GetButton_m38251721(NULL /*static, unused*/, _stringLiteral842948034, /*hidden argument*/NULL);
		NullCheck(L_22);
		L_22->set_inputJump_5(L_23);
		return;
	}
}
// System.Void FPSInputController::Main()
extern "C"  void FPSInputController_Main_m1154765804 (FPSInputController_t4241249601 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HideCursor::.ctor()
extern "C"  void HideCursor__ctor_m272866968 (HideCursor_t48087058 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HideCursor::Start()
extern "C"  void HideCursor_Start_m3141370208 (HideCursor_t48087058 * __this, const MethodInfo* method)
{
	{
		Cursor_set_visible_m860533511(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HideCursor::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t HideCursor_Update_m3788233949_MetadataUsageId;
extern "C"  void HideCursor_Update_m3788233949 (HideCursor_t48087058 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HideCursor_Update_m3788233949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Cursor_set_visible_m860533511(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void HideCursor::Main()
extern "C"  void HideCursor_Main_m3625278983 (HideCursor_t48087058 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void JumpOnClick::.ctor()
extern "C"  void JumpOnClick__ctor_m1234540515 (JumpOnClick_t2456543707 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JumpOnClick::.cctor()
extern Il2CppClass* JumpOnClick_t2456543707_il2cpp_TypeInfo_var;
extern const uint32_t JumpOnClick__cctor_m2206386944_MetadataUsageId;
extern "C"  void JumpOnClick__cctor_m2206386944 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JumpOnClick__cctor_m2206386944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((JumpOnClick_t2456543707_StaticFields*)JumpOnClick_t2456543707_il2cpp_TypeInfo_var->static_fields)->set_gravity_5((0.3f));
		((JumpOnClick_t2456543707_StaticFields*)JumpOnClick_t2456543707_il2cpp_TypeInfo_var->static_fields)->set_jumpSpeed_6((0.03f));
		return;
	}
}
// System.Void JumpOnClick::Start()
extern "C"  void JumpOnClick_Start_m1567940455 (JumpOnClick_t2456543707 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_2();
		__this->set_groundY_3(L_2);
		return;
	}
}
// System.Void JumpOnClick::Update()
extern Il2CppClass* JumpOnClick_t2456543707_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* RaycastHit_t87180320_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider_t3497673348_m2974738468_MethodInfo_var;
extern const uint32_t JumpOnClick_Update_m453552980_MetadataUsageId;
extern "C"  void JumpOnClick_Update_m453552980 (JumpOnClick_t2456543707 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JumpOnClick_Update_m453552980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t2469606224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit_t87180320  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float V_10 = 0.0f;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	float V_13 = 0.0f;
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	float V_15 = 0.0f;
	Vector3_t2243707580  V_16;
	memset(&V_16, 0, sizeof(V_16));
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		V_8 = L_1;
		float L_2 = (&V_8)->get_y_2();
		float L_3 = __this->get_groundY_3();
		if ((((float)L_2) <= ((float)L_3)))
		{
			goto IL_0083;
		}
	}
	{
		float L_4 = __this->get_ySpeed_4();
		IL2CPP_RUNTIME_CLASS_INIT(JumpOnClick_t2456543707_il2cpp_TypeInfo_var);
		float L_5 = ((JumpOnClick_t2456543707_StaticFields*)JumpOnClick_t2456543707_il2cpp_TypeInfo_var->static_fields)->get_gravity_5();
		float L_6 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_ySpeed_4(((float)((float)L_4-(float)((float)((float)L_5*(float)L_6)))));
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_position_m1104419803(L_7, /*hidden argument*/NULL);
		V_9 = L_8;
		float L_9 = (&V_9)->get_y_2();
		float L_10 = __this->get_ySpeed_4();
		float L_11 = ((float)((float)L_9+(float)L_10));
		V_2 = L_11;
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = L_13;
		V_3 = L_14;
		float L_15 = V_2;
		float L_16 = L_15;
		V_10 = L_16;
		(&V_3)->set_y_2(L_16);
		float L_17 = V_10;
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = V_3;
		Vector3_t2243707580  L_20 = L_19;
		V_11 = L_20;
		NullCheck(L_18);
		Transform_set_position_m2469242620(L_18, L_20, /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = V_11;
	}

IL_0083:
	{
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t2243707580  L_23 = Transform_get_position_m1104419803(L_22, /*hidden argument*/NULL);
		V_12 = L_23;
		float L_24 = (&V_12)->get_y_2();
		float L_25 = __this->get_groundY_3();
		if ((((float)L_24) > ((float)L_25)))
		{
			goto IL_00e5;
		}
	}
	{
		float L_26 = __this->get_groundY_3();
		float L_27 = L_26;
		V_4 = L_27;
		Transform_t3275118058 * L_28 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t2243707580  L_29 = Transform_get_position_m1104419803(L_28, /*hidden argument*/NULL);
		Vector3_t2243707580  L_30 = L_29;
		V_5 = L_30;
		float L_31 = V_4;
		float L_32 = L_31;
		V_13 = L_32;
		(&V_5)->set_y_2(L_32);
		float L_33 = V_13;
		Transform_t3275118058 * L_34 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_35 = V_5;
		Vector3_t2243707580  L_36 = L_35;
		V_14 = L_36;
		NullCheck(L_34);
		Transform_set_position_m2469242620(L_34, L_36, /*hidden argument*/NULL);
		Vector3_t2243707580  L_37 = V_14;
		__this->set_ySpeed_4((((float)((float)0))));
	}

IL_00e5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_38 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0179;
		}
	}
	{
		Camera_t189460977 * L_39 = __this->get_sceneCamera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_40 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_39);
		Ray_t2469606224  L_41 = Camera_ScreenPointToRay_m614889538(L_39, L_40, /*hidden argument*/NULL);
		V_0 = L_41;
		Initobj (RaycastHit_t87180320_il2cpp_TypeInfo_var, (&V_1));
		Ray_t2469606224  L_42 = V_0;
		bool L_43 = Physics_Raycast_m2736931691(NULL /*static, unused*/, L_42, (&V_1), /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0179;
		}
	}
	{
		Collider_t3497673348 * L_44 = RaycastHit_get_collider_m301198172((&V_1), /*hidden argument*/NULL);
		Collider_t3497673348 * L_45 = Component_GetComponent_TisCollider_t3497673348_m2974738468(__this, /*hidden argument*/Component_GetComponent_TisCollider_t3497673348_m2974738468_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_46 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0179;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JumpOnClick_t2456543707_il2cpp_TypeInfo_var);
		float L_47 = ((JumpOnClick_t2456543707_StaticFields*)JumpOnClick_t2456543707_il2cpp_TypeInfo_var->static_fields)->get_jumpSpeed_6();
		__this->set_ySpeed_4(L_47);
		float L_48 = __this->get_groundY_3();
		float L_49 = ((JumpOnClick_t2456543707_StaticFields*)JumpOnClick_t2456543707_il2cpp_TypeInfo_var->static_fields)->get_jumpSpeed_6();
		float L_50 = ((float)((float)L_48+(float)L_49));
		V_6 = L_50;
		Transform_t3275118058 * L_51 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_51);
		Vector3_t2243707580  L_52 = Transform_get_position_m1104419803(L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = L_52;
		V_7 = L_53;
		float L_54 = V_6;
		float L_55 = L_54;
		V_15 = L_55;
		(&V_7)->set_y_2(L_55);
		float L_56 = V_15;
		Transform_t3275118058 * L_57 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_58 = V_7;
		Vector3_t2243707580  L_59 = L_58;
		V_16 = L_59;
		NullCheck(L_57);
		Transform_set_position_m2469242620(L_57, L_59, /*hidden argument*/NULL);
		Vector3_t2243707580  L_60 = V_16;
	}

IL_0179:
	{
		return;
	}
}
// System.Void JumpOnClick::Main()
extern "C"  void JumpOnClick_Main_m4282046944 (JumpOnClick_t2456543707 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void JumpTest::.ctor()
extern "C"  void JumpTest__ctor_m2489370914 (JumpTest_t640307972 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_multiplier_9((1.0f));
		return;
	}
}
// System.Void JumpTest::.cctor()
extern Il2CppClass* JumpTest_t640307972_il2cpp_TypeInfo_var;
extern const uint32_t JumpTest__cctor_m3066781921_MetadataUsageId;
extern "C"  void JumpTest__cctor_m3066781921 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JumpTest__cctor_m3066781921_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((JumpTest_t640307972_StaticFields*)JumpTest_t640307972_il2cpp_TypeInfo_var->static_fields)->set_maxVerticalSpeed_13((5.0f));
		((JumpTest_t640307972_StaticFields*)JumpTest_t640307972_il2cpp_TypeInfo_var->static_fields)->set_landBlendDuration_18((0.5f));
		return;
	}
}
// System.Void JumpTest::Start()
extern const MethodInfo* Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var;
extern const uint32_t JumpTest_Start_m82108526_MetadataUsageId;
extern "C"  void JumpTest_Start_m82108526 (JumpTest_t640307972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JumpTest_Start_m82108526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t2068071072 * L_0 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_1 = __this->get_upAnimation_5();
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m2079638459(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		AnimationState_t1303741697 * L_3 = Animation_get_Item_m4198128320(L_0, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		AnimationState_set_layer_m139053567(L_3, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_4 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_5 = __this->get_downAnimation_6();
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m2079638459(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		AnimationState_t1303741697 * L_7 = Animation_get_Item_m4198128320(L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		AnimationState_set_layer_m139053567(L_7, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_8 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_9 = __this->get_landAnimation_7();
		NullCheck(L_9);
		String_t* L_10 = Object_get_name_m2079638459(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		AnimationState_t1303741697 * L_11 = Animation_get_Item_m4198128320(L_8, L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		AnimationState_set_layer_m139053567(L_11, 0, /*hidden argument*/NULL);
		Animation_t2068071072 * L_12 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_13 = __this->get_idleAnimation_8();
		NullCheck(L_13);
		String_t* L_14 = Object_get_name_m2079638459(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		AnimationState_t1303741697 * L_15 = Animation_get_Item_m4198128320(L_12, L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		AnimationState_set_layer_m139053567(L_15, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JumpTest::Update()
extern Il2CppClass* JumpTest_t640307972_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var;
extern const uint32_t JumpTest_Update_m2386774551_MetadataUsageId;
extern "C"  void JumpTest_Update_m2386774551 (JumpTest_t640307972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JumpTest_Update_m2386774551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		AnimationCurve_t3306541151 * L_0 = __this->get_heightCurve_4();
		float L_1 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_startTime_2();
		NullCheck(L_0);
		float L_3 = AnimationCurve_Evaluate_m3698879322(L_0, ((float)((float)L_1-(float)L_2)), /*hidden argument*/NULL);
		float L_4 = __this->get_multiplier_9();
		float L_5 = __this->get_offset_10();
		float L_6 = ((float)((float)((float)((float)L_3*(float)L_4))+(float)L_5));
		V_0 = L_6;
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_position_m1104419803(L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = L_8;
		V_1 = L_9;
		float L_10 = V_0;
		float L_11 = L_10;
		V_2 = L_11;
		(&V_1)->set_y_2(L_11);
		float L_12 = V_2;
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = V_1;
		Vector3_t2243707580  L_15 = L_14;
		V_3 = L_15;
		NullCheck(L_13);
		Transform_set_position_m2469242620(L_13, L_15, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = V_3;
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t2243707580  L_18 = Transform_get_position_m1104419803(L_17, /*hidden argument*/NULL);
		V_4 = L_18;
		float L_19 = (&V_4)->get_y_2();
		float L_20 = __this->get_previousYPosition_12();
		float L_21 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_verticalSpeed_11(((float)((float)((float)((float)L_19-(float)L_20))/(float)L_21)));
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t2243707580  L_23 = Transform_get_position_m1104419803(L_22, /*hidden argument*/NULL);
		V_5 = L_23;
		float L_24 = (&V_5)->get_y_2();
		__this->set_previousYPosition_12(L_24);
		float L_25 = __this->get_verticalSpeed_11();
		IL2CPP_RUNTIME_CLASS_INIT(JumpTest_t640307972_il2cpp_TypeInfo_var);
		float L_26 = ((JumpTest_t640307972_StaticFields*)JumpTest_t640307972_il2cpp_TypeInfo_var->static_fields)->get_maxVerticalSpeed_13();
		float L_27 = ((JumpTest_t640307972_StaticFields*)JumpTest_t640307972_il2cpp_TypeInfo_var->static_fields)->get_maxVerticalSpeed_13();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_28 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, ((float)((float)((float)((float)L_25+(float)L_26))/(float)((float)((float)L_27*(float)(((float)((float)2))))))), /*hidden argument*/NULL);
		__this->set_upDownBlend_14(L_28);
		AnimationCurve_t3306541151 * L_29 = __this->get_upDownBlendCurve_16();
		float L_30 = __this->get_upDownBlend_14();
		NullCheck(L_29);
		float L_31 = AnimationCurve_Evaluate_m3698879322(L_29, L_30, /*hidden argument*/NULL);
		__this->set_upDownBlend_14(L_31);
		Transform_t3275118058 * L_32 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_t2243707580  L_33 = Transform_get_position_m1104419803(L_32, /*hidden argument*/NULL);
		V_6 = L_33;
		float L_34 = (&V_6)->get_y_2();
		float L_35 = __this->get_offset_10();
		if ((((float)L_34) >= ((float)((float)((float)(0.1f)+(float)L_35)))))
		{
			goto IL_00fb;
		}
	}
	{
		__this->set_isGrounded_20((bool)1);
		goto IL_0102;
	}

IL_00fb:
	{
		__this->set_isGrounded_20((bool)0);
	}

IL_0102:
	{
		bool L_36 = __this->get_isGrounded_20();
		if (!L_36)
		{
			goto IL_01ce;
		}
	}
	{
		Animation_t2068071072 * L_37 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_38 = __this->get_upAnimation_5();
		NullCheck(L_38);
		String_t* L_39 = Object_get_name_m2079638459(L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		Animation_Blend_m2530155026(L_37, L_39, (((float)((float)0))), (0.05f), /*hidden argument*/NULL);
		Animation_t2068071072 * L_40 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_41 = __this->get_downAnimation_6();
		NullCheck(L_41);
		String_t* L_42 = Object_get_name_m2079638459(L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		Animation_Blend_m2530155026(L_40, L_42, (((float)((float)0))), (0.05f), /*hidden argument*/NULL);
		bool L_43 = __this->get_isLanding_17();
		if (L_43)
		{
			goto IL_0164;
		}
	}
	{
		__this->set_isLanding_17((bool)1);
		float L_44 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_landStartTime_19(L_44);
	}

IL_0164:
	{
		float L_45 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_46 = __this->get_landStartTime_19();
		IL2CPP_RUNTIME_CLASS_INIT(JumpTest_t640307972_il2cpp_TypeInfo_var);
		float L_47 = ((JumpTest_t640307972_StaticFields*)JumpTest_t640307972_il2cpp_TypeInfo_var->static_fields)->get_landBlendDuration_18();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_48 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, ((float)((float)((float)((float)L_45-(float)L_46))/(float)L_47)), /*hidden argument*/NULL);
		__this->set_idleLandBlend_15(((float)((float)(((float)((float)1)))-(float)L_48)));
		Animation_t2068071072 * L_49 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_50 = __this->get_landAnimation_7();
		NullCheck(L_50);
		String_t* L_51 = Object_get_name_m2079638459(L_50, /*hidden argument*/NULL);
		float L_52 = __this->get_idleLandBlend_15();
		NullCheck(L_49);
		Animation_Blend_m2530155026(L_49, L_51, L_52, (0.05f), /*hidden argument*/NULL);
		Animation_t2068071072 * L_53 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_54 = __this->get_idleAnimation_8();
		NullCheck(L_54);
		String_t* L_55 = Object_get_name_m2079638459(L_54, /*hidden argument*/NULL);
		float L_56 = __this->get_idleLandBlend_15();
		NullCheck(L_53);
		Animation_Blend_m2530155026(L_53, L_55, ((float)((float)(((float)((float)1)))-(float)L_56)), (0.05f), /*hidden argument*/NULL);
		goto IL_0222;
	}

IL_01ce:
	{
		__this->set_isLanding_17((bool)0);
		Animation_t2068071072 * L_57 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_58 = __this->get_upAnimation_5();
		NullCheck(L_58);
		String_t* L_59 = Object_get_name_m2079638459(L_58, /*hidden argument*/NULL);
		float L_60 = __this->get_upDownBlend_14();
		NullCheck(L_57);
		Animation_Blend_m2530155026(L_57, L_59, L_60, (0.05f), /*hidden argument*/NULL);
		Animation_t2068071072 * L_61 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_62 = __this->get_downAnimation_6();
		NullCheck(L_62);
		String_t* L_63 = Object_get_name_m2079638459(L_62, /*hidden argument*/NULL);
		float L_64 = __this->get_upDownBlend_14();
		NullCheck(L_61);
		Animation_Blend_m2530155026(L_61, L_63, ((float)((float)(((float)((float)1)))-(float)L_64)), (0.05f), /*hidden argument*/NULL);
		__this->set_idleLandBlend_15((((float)((float)0))));
	}

IL_0222:
	{
		return;
	}
}
// System.Void JumpTest::OnGUI()
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4164253619;
extern Il2CppCodeGenString* _stringLiteral2162992842;
extern Il2CppCodeGenString* _stringLiteral525481662;
extern const uint32_t JumpTest_OnGUI_m3982212818_MetadataUsageId;
extern "C"  void JumpTest_OnGUI_m3982212818 (JumpTest_t640307972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JumpTest_OnGUI_m3982212818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_debug_3();
		if (!L_0)
		{
			goto IL_006b;
		}
	}
	{
		float* L_1 = __this->get_address_of_verticalSpeed_11();
		String_t* L_2 = Single_ToString_m1813392066(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		String_t* L_3 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, _stringLiteral4164253619, L_2, /*hidden argument*/NULL);
		GUILayout_Label_m3466110979(NULL /*static, unused*/, L_3, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_upDownBlend_14();
		String_t* L_5 = Single_ToString_m1813392066(L_4, /*hidden argument*/NULL);
		String_t* L_6 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, _stringLiteral2162992842, L_5, /*hidden argument*/NULL);
		GUILayout_Label_m3466110979(NULL /*static, unused*/, L_6, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		float* L_7 = __this->get_address_of_idleLandBlend_15();
		String_t* L_8 = Single_ToString_m1813392066(L_7, /*hidden argument*/NULL);
		String_t* L_9 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, _stringLiteral525481662, L_8, /*hidden argument*/NULL);
		GUILayout_Label_m3466110979(NULL /*static, unused*/, L_9, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// System.Void JumpTest::Main()
extern "C"  void JumpTest_Main_m1331742801 (JumpTest_t640307972 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlatformInputController::.ctor()
extern "C"  void PlatformInputController__ctor_m2569273223 (PlatformInputController_t4273899755 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_autoRotate_2((bool)1);
		__this->set_maxRotationSpeed_3((((float)((float)((int32_t)360)))));
		return;
	}
}
// System.Void PlatformInputController::Awake()
extern const Il2CppType* CharacterMotor_t262030084_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharacterMotor_t262030084_il2cpp_TypeInfo_var;
extern const uint32_t PlatformInputController_Awake_m679619692_MetadataUsageId;
extern "C"  void PlatformInputController_Awake_m679619692 (PlatformInputController_t4273899755 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformInputController_Awake_m679619692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(CharacterMotor_t262030084_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_1 = Component_GetComponent_m4225719715(__this, L_0, /*hidden argument*/NULL);
		__this->set_motor_4(((CharacterMotor_t262030084 *)CastclassClass(L_1, CharacterMotor_t262030084_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void PlatformInputController::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral855845486;
extern Il2CppCodeGenString* _stringLiteral1635882288;
extern Il2CppCodeGenString* _stringLiteral842948034;
extern const uint32_t PlatformInputController_Update_m3789481214_MetadataUsageId;
extern "C"  void PlatformInputController_Update_m3789481214 (PlatformInputController_t4273899755 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformInputController_Update_m3789481214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Quaternion_t4030073918  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		float L_1 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1635882288, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m2638739322(&L_2, L_0, L_1, (((float)((float)0))), /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = V_0;
		Vector3_t2243707580  L_4 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = Vector3_op_Inequality_m799191452(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0051;
		}
	}
	{
		float L_6 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		V_1 = L_6;
		Vector3_t2243707580  L_7 = V_0;
		float L_8 = V_1;
		Vector3_t2243707580  L_9 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		float L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_11 = Mathf_Min_m1648492575(NULL /*static, unused*/, (((float)((float)1))), L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		float L_12 = V_1;
		float L_13 = V_1;
		V_1 = ((float)((float)L_12*(float)L_13));
		Vector3_t2243707580  L_14 = V_0;
		float L_15 = V_1;
		Vector3_t2243707580  L_16 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
	}

IL_0051:
	{
		Camera_t189460977 * L_17 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Quaternion_t4030073918  L_19 = Transform_get_rotation_m1033555130(L_18, /*hidden argument*/NULL);
		Vector3_t2243707580  L_20 = V_0;
		Vector3_t2243707580  L_21 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		V_0 = L_21;
		Camera_t189460977 * L_22 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = Component_get_transform_m2697483695(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t2243707580  L_24 = Transform_get_forward_m1833488937(L_23, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = Vector3_op_UnaryNegation_m3383802608(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Transform_t3275118058 * L_26 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		Vector3_t2243707580  L_27 = Transform_get_up_m1603627763(L_26, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_28 = Quaternion_FromToRotation_m1685306068(NULL /*static, unused*/, L_25, L_27, /*hidden argument*/NULL);
		V_2 = L_28;
		Quaternion_t4030073918  L_29 = V_2;
		Vector3_t2243707580  L_30 = V_0;
		Vector3_t2243707580  L_31 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		CharacterMotor_t262030084 * L_32 = __this->get_motor_4();
		Vector3_t2243707580  L_33 = V_0;
		NullCheck(L_32);
		L_32->set_inputMoveDirection_4(L_33);
		CharacterMotor_t262030084 * L_34 = __this->get_motor_4();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_35 = Input_GetButton_m38251721(NULL /*static, unused*/, _stringLiteral842948034, /*hidden argument*/NULL);
		NullCheck(L_34);
		L_34->set_inputJump_5(L_35);
		bool L_36 = __this->get_autoRotate_2();
		if (!L_36)
		{
			goto IL_011f;
		}
	}
	{
		float L_37 = Vector3_get_sqrMagnitude_m1814096310((&V_0), /*hidden argument*/NULL);
		if ((((float)L_37) <= ((float)(0.01f))))
		{
			goto IL_011f;
		}
	}
	{
		Transform_t3275118058 * L_38 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_38);
		Vector3_t2243707580  L_39 = Transform_get_forward_m1833488937(L_38, /*hidden argument*/NULL);
		Vector3_t2243707580  L_40 = V_0;
		float L_41 = __this->get_maxRotationSpeed_3();
		float L_42 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = VirtFuncInvoker3< Vector3_t2243707580 , Vector3_t2243707580 , Vector3_t2243707580 , float >::Invoke(7 /* UnityEngine.Vector3 PlatformInputController::ConstantSlerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single) */, __this, L_39, L_40, ((float)((float)L_41*(float)L_42)));
		V_3 = L_43;
		Vector3_t2243707580  L_44 = V_3;
		Transform_t3275118058 * L_45 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_45);
		Vector3_t2243707580  L_46 = Transform_get_up_m1603627763(L_45, /*hidden argument*/NULL);
		Vector3_t2243707580  L_47 = VirtFuncInvoker2< Vector3_t2243707580 , Vector3_t2243707580 , Vector3_t2243707580  >::Invoke(6 /* UnityEngine.Vector3 PlatformInputController::ProjectOntoPlane(UnityEngine.Vector3,UnityEngine.Vector3) */, __this, L_44, L_46);
		V_3 = L_47;
		Transform_t3275118058 * L_48 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_49 = V_3;
		Transform_t3275118058 * L_50 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_50);
		Vector3_t2243707580  L_51 = Transform_get_up_m1603627763(L_50, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_52 = Quaternion_LookRotation_m700700634(NULL /*static, unused*/, L_49, L_51, /*hidden argument*/NULL);
		NullCheck(L_48);
		Transform_set_rotation_m3411284563(L_48, L_52, /*hidden argument*/NULL);
	}

IL_011f:
	{
		return;
	}
}
// UnityEngine.Vector3 PlatformInputController::ProjectOntoPlane(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  PlatformInputController_ProjectOntoPlane_m3298465390 (PlatformInputController_t4273899755 * __this, Vector3_t2243707580  ___v0, Vector3_t2243707580  ___normal1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___v0;
		Vector3_t2243707580  L_1 = ___v0;
		Vector3_t2243707580  L_2 = ___normal1;
		Vector3_t2243707580  L_3 = Vector3_Project_m1396027688(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector3 PlatformInputController::ConstantSlerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t PlatformInputController_ConstantSlerp_m1768991218_MetadataUsageId;
extern "C"  Vector3_t2243707580  PlatformInputController_ConstantSlerp_m1768991218 (PlatformInputController_t4273899755 * __this, Vector3_t2243707580  ___from0, Vector3_t2243707580  ___to1, float ___angle2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformInputController_ConstantSlerp_m1768991218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___angle2;
		Vector3_t2243707580  L_1 = ___from0;
		Vector3_t2243707580  L_2 = ___to1;
		float L_3 = Vector3_Angle_m2552334978(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Min_m1648492575(NULL /*static, unused*/, (((float)((float)1))), ((float)((float)L_0/(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		Vector3_t2243707580  L_5 = ___from0;
		Vector3_t2243707580  L_6 = ___to1;
		float L_7 = V_0;
		Vector3_t2243707580  L_8 = Vector3_Slerp_m846771032(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void PlatformInputController::Main()
extern "C"  void PlatformInputController_Main_m410404690 (PlatformInputController_t4273899755 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Rotate::.ctor()
extern "C"  void Rotate__ctor_m1201418695 (Rotate_t4255939432 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Rotate::Start()
extern "C"  void Rotate_Start_m1763714195 (Rotate_t4255939432 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Rotate::Update()
extern "C"  void Rotate_Update_m3953539786 (Rotate_t4255939432 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = __this->get_eulerAngles_2();
		float L_2 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_relativeTo_3();
		NullCheck(L_0);
		Transform_Rotate_m2612876682(L_0, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Rotate::Main()
extern "C"  void Rotate_Main_m236250746 (Rotate_t4255939432 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SelectedButton::.ctor()
extern "C"  void SelectedButton__ctor_m1911760377 (SelectedButton_t2241840337 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_animationSpeed_6((1.0f));
		return;
	}
}
// System.Void SelectedButton::.cctor()
extern Il2CppClass* SelectedButton_t2241840337_il2cpp_TypeInfo_var;
extern const uint32_t SelectedButton__cctor_m3332729448_MetadataUsageId;
extern "C"  void SelectedButton__cctor_m3332729448 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectedButton__cctor_m3332729448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((SelectedButton_t2241840337_StaticFields*)SelectedButton_t2241840337_il2cpp_TypeInfo_var->static_fields)->set_maxFrame_7(5);
		return;
	}
}
// System.Void SelectedButton::Start()
extern "C"  void SelectedButton_Start_m33131737 (SelectedButton_t2241840337 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SelectedButton::Update()
extern Il2CppClass* SelectedButton_t2241840337_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4026354833;
extern const uint32_t SelectedButton_Update_m1751266396_MetadataUsageId;
extern "C"  void SelectedButton_Update_m1751266396 (SelectedButton_t2241840337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectedButton_Update_m1751266396_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_animationStart_2();
		__this->set_animationTime_4(((float)((float)L_0-(float)L_1)));
		float L_2 = __this->get_animationTime_4();
		float L_3 = __this->get_animationSpeed_6();
		IL2CPP_RUNTIME_CLASS_INIT(SelectedButton_t2241840337_il2cpp_TypeInfo_var);
		int32_t L_4 = ((SelectedButton_t2241840337_StaticFields*)SelectedButton_t2241840337_il2cpp_TypeInfo_var->static_fields)->get_maxFrame_7();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_5 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, ((float)((float)L_2*(float)L_3)), (((float)((float)0))), (((float)((float)L_4))), /*hidden argument*/NULL);
		int32_t L_6 = Mathf_FloorToInt_m4005035722(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_currentFrame_3(L_6);
		int32_t L_7 = ((SelectedButton_t2241840337_StaticFields*)SelectedButton_t2241840337_il2cpp_TypeInfo_var->static_fields)->get_maxFrame_7();
		int32_t L_8 = __this->get_currentFrame_3();
		__this->set_textureOffset_5(((float)((float)((float)((float)(1.0f)/(float)(((float)((float)((int32_t)((int32_t)L_7+(int32_t)1)))))))*(float)(((float)((float)L_8))))));
		Renderer_t257310565 * L_9 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var);
		NullCheck(L_9);
		Material_t193706927 * L_10 = Renderer_get_material_m2553789785(L_9, /*hidden argument*/NULL);
		float L_11 = __this->get_textureOffset_5();
		Vector2_t2243707579  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector2__ctor_m3067419446(&L_12, L_11, (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_10);
		Material_SetTextureOffset_m3084369360(L_10, _stringLiteral4026354833, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SelectedButton::SetAnimationStart(System.Single)
extern "C"  void SelectedButton_SetAnimationStart_m4141702936 (SelectedButton_t2241840337 * __this, float ___time0, const MethodInfo* method)
{
	{
		float L_0 = ___time0;
		__this->set_animationStart_2(L_0);
		__this->set_currentFrame_3(0);
		return;
	}
}
// System.Void SelectedButton::Main()
extern "C"  void SelectedButton_Main_m3514946796 (SelectedButton_t2241840337 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Shadow::.ctor()
extern "C"  void Shadow__ctor_m2668981182 (Shadow_t2876782136 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_distanceTolerance_2((0.4f));
		__this->set_maxOpacity_3((1.0f));
		__this->set_multiplier_4((1.2f));
		__this->set_opacity_5((1.0f));
		__this->set_buffer_7((0.02f));
		return;
	}
}
// System.Void Shadow::Start()
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3947305777;
extern const uint32_t Shadow_Start_m817252038_MetadataUsageId;
extern "C"  void Shadow_Start_m817252038 (Shadow_t2876782136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Shadow_Start_m817252038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Renderer_t257310565 * L_0 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var);
		NullCheck(L_0);
		Renderer_set_enabled_m142717579(L_0, (bool)1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = Transform_Find_m3323476454(L_1, _stringLiteral3947305777, /*hidden argument*/NULL);
		__this->set_castingPoint_6(L_2);
		Transform_t3275118058 * L_3 = __this->get_castingPoint_6();
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Transform_get_parent_m147407266(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_parent_m3281327839(L_3, L_5, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Transform_get_root_m3891257842(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_parent_m3281327839(L_6, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Shadow::LateUpdate()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var;
extern const uint32_t Shadow_LateUpdate_m2850280077_MetadataUsageId;
extern "C"  void Shadow_LateUpdate_m2850280077 (Shadow_t2876782136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Shadow_LateUpdate_m2850280077_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHitU5BU5D_t1214023521* V_0 = NULL;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	RaycastHit_t87180320  V_3;
	memset(&V_3, 0, sizeof(V_3));
	String_t* V_4 = NULL;
	bool V_5 = false;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	float V_9 = 0.0f;
	Color_t2020392075  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	float V_14 = 0.0f;
	Vector3_t2243707580  V_15;
	memset(&V_15, 0, sizeof(V_15));
	float V_16 = 0.0f;
	Color_t2020392075  V_17;
	memset(&V_17, 0, sizeof(V_17));
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_1 = __this->get_castingPoint_6();
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m2469242620(L_0, L_2, /*hidden argument*/NULL);
		V_0 = (RaycastHitU5BU5D_t1214023521*)NULL;
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_position_m1104419803(L_3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_5, (0.5f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Vector3_op_UnaryNegation_m3383802608(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		RaycastHitU5BU5D_t1214023521* L_10 = Physics_RaycastAll_m3650851272(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		V_1 = (((float)((float)((int32_t)-999999))));
		V_2 = 0;
		goto IL_0145;
	}

IL_0055:
	{
		RaycastHitU5BU5D_t1214023521* L_11 = V_0;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		V_3 = (*(RaycastHit_t87180320 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12))));
		Transform_t3275118058 * L_13 = RaycastHit_get_transform_m3290290036((&V_3), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t3275118058 * L_14 = Transform_get_root_m3891257842(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m2079638459(L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		V_5 = (bool)1;
		String_t* L_16 = V_4;
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_t3275118058 * L_18 = Transform_get_root_m3891257842(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_19 = Object_get_name_m2079638459(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_16, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0097;
		}
	}
	{
		V_5 = (bool)0;
	}

IL_0097:
	{
		bool L_21 = V_5;
		if (!L_21)
		{
			goto IL_0141;
		}
	}
	{
		Vector3_t2243707580  L_22 = RaycastHit_get_point_m326143462((&V_3), /*hidden argument*/NULL);
		V_11 = L_22;
		float L_23 = (&V_11)->get_y_2();
		float L_24 = __this->get_buffer_7();
		float L_25 = V_1;
		if ((((float)((float)((float)L_23+(float)L_24))) <= ((float)L_25)))
		{
			goto IL_0141;
		}
	}
	{
		Vector3_t2243707580  L_26 = RaycastHit_get_point_m326143462((&V_3), /*hidden argument*/NULL);
		V_12 = L_26;
		float L_27 = (&V_12)->get_y_2();
		float L_28 = __this->get_buffer_7();
		V_1 = ((float)((float)L_27+(float)L_28));
		Vector3_t2243707580  L_29 = RaycastHit_get_point_m326143462((&V_3), /*hidden argument*/NULL);
		V_13 = L_29;
		float L_30 = (&V_13)->get_y_2();
		float L_31 = __this->get_buffer_7();
		float L_32 = ((float)((float)L_30+(float)L_31));
		V_7 = L_32;
		Transform_t3275118058 * L_33 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t2243707580  L_34 = Transform_get_position_m1104419803(L_33, /*hidden argument*/NULL);
		Vector3_t2243707580  L_35 = L_34;
		V_8 = L_35;
		float L_36 = V_7;
		float L_37 = L_36;
		V_14 = L_37;
		(&V_8)->set_y_2(L_37);
		float L_38 = V_14;
		Transform_t3275118058 * L_39 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_40 = V_8;
		Vector3_t2243707580  L_41 = L_40;
		V_15 = L_41;
		NullCheck(L_39);
		Transform_set_position_m2469242620(L_39, L_41, /*hidden argument*/NULL);
		Vector3_t2243707580  L_42 = V_15;
		Transform_t3275118058 * L_43 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_44 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_44);
		Vector3_t2243707580  L_45 = Transform_get_position_m1104419803(L_44, /*hidden argument*/NULL);
		Vector3_t2243707580  L_46 = RaycastHit_get_normal_m817665579((&V_3), /*hidden argument*/NULL);
		Vector3_t2243707580  L_47 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		NullCheck(L_43);
		Transform_LookAt_m3314153180(L_43, L_47, /*hidden argument*/NULL);
	}

IL_0141:
	{
		int32_t L_48 = V_2;
		V_2 = ((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_0145:
	{
		int32_t L_49 = V_2;
		RaycastHitU5BU5D_t1214023521* L_50 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_50);
		int32_t L_51 = Array_get_Length_m1498215565((Il2CppArray *)(Il2CppArray *)L_50, /*hidden argument*/NULL);
		if ((((int32_t)L_49) < ((int32_t)L_51)))
		{
			goto IL_0055;
		}
	}
	{
		Transform_t3275118058 * L_52 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_52);
		Vector3_t2243707580  L_53 = Transform_get_position_m1104419803(L_52, /*hidden argument*/NULL);
		Transform_t3275118058 * L_54 = __this->get_castingPoint_6();
		NullCheck(L_54);
		Vector3_t2243707580  L_55 = Transform_get_position_m1104419803(L_54, /*hidden argument*/NULL);
		float L_56 = Vector3_Distance_m1859670022(NULL /*static, unused*/, L_53, L_55, /*hidden argument*/NULL);
		V_6 = L_56;
		float L_57 = __this->get_maxOpacity_3();
		float L_58 = V_6;
		float L_59 = __this->get_distanceTolerance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_60 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_57, (((float)((float)0))), ((float)((float)L_58*(float)((float)((float)(((float)((float)1)))/(float)L_59)))), /*hidden argument*/NULL);
		__this->set_opacity_5(L_60);
		RaycastHitU5BU5D_t1214023521* L_61 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_61);
		int32_t L_62 = Array_get_Length_m1498215565((Il2CppArray *)(Il2CppArray *)L_61, /*hidden argument*/NULL);
		if (L_62)
		{
			goto IL_01a0;
		}
	}
	{
		__this->set_opacity_5((((float)((float)0))));
	}

IL_01a0:
	{
		float L_63 = __this->get_opacity_5();
		float L_64 = __this->get_multiplier_4();
		__this->set_opacity_5(((float)((float)L_63*(float)L_64)));
		float L_65 = __this->get_opacity_5();
		float L_66 = L_65;
		V_9 = L_66;
		Renderer_t257310565 * L_67 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var);
		NullCheck(L_67);
		Material_t193706927 * L_68 = Renderer_get_material_m2553789785(L_67, /*hidden argument*/NULL);
		NullCheck(L_68);
		Color_t2020392075  L_69 = Material_get_color_m668215843(L_68, /*hidden argument*/NULL);
		Color_t2020392075  L_70 = L_69;
		V_10 = L_70;
		float L_71 = V_9;
		float L_72 = L_71;
		V_16 = L_72;
		(&V_10)->set_a_3(L_72);
		float L_73 = V_16;
		Renderer_t257310565 * L_74 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var);
		NullCheck(L_74);
		Material_t193706927 * L_75 = Renderer_get_material_m2553789785(L_74, /*hidden argument*/NULL);
		Color_t2020392075  L_76 = V_10;
		Color_t2020392075  L_77 = L_76;
		V_17 = L_77;
		NullCheck(L_75);
		Material_set_color_m577844242(L_75, L_77, /*hidden argument*/NULL);
		Color_t2020392075  L_78 = V_17;
		return;
	}
}
// System.Void Shadow::Main()
extern "C"  void Shadow_Main_m726902191 (Shadow_t2876782136 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SpeedTilt::.ctor()
extern "C"  void SpeedTilt__ctor_m164590832 (SpeedTilt_t451906044 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_multiplier_2((1.0f));
		__this->set_tiltDamp_3((0.1f));
		return;
	}
}
// System.Void SpeedTilt::Start()
extern "C"  void SpeedTilt_Start_m821491888 (SpeedTilt_t451906044 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t4030073918  L_1 = Transform_get_rotation_m1033555130(L_0, /*hidden argument*/NULL);
		__this->set_startingRotation_4(L_1);
		return;
	}
}
// System.Void SpeedTilt::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t SpeedTilt_Update_m2355129713_MetadataUsageId;
extern "C"  void SpeedTilt_Update_m2355129713 (SpeedTilt_t451906044 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpeedTilt_Update_m2355129713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_1();
		float L_3 = __this->get_previousXPosition_9();
		__this->set_currentXspeed_8(((float)((float)L_2-(float)L_3)));
		float L_4 = __this->get_currentXspeed_8();
		float L_5 = __this->get_multiplier_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, ((float)((float)L_4*(float)L_5)), (((float)((float)((int32_t)-90)))), (((float)((float)((int32_t)90)))), /*hidden argument*/NULL);
		__this->set_targetTilt_6(L_6);
		float L_7 = __this->get_currentTilt_5();
		float L_8 = __this->get_targetTilt_6();
		float* L_9 = __this->get_address_of_velocityTilt_7();
		float L_10 = __this->get_tiltDamp_3();
		float L_11 = Mathf_SmoothDamp_m1166236953(NULL /*static, unused*/, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		__this->set_currentTilt_5(L_11);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		float L_14 = (&V_1)->get_x_1();
		__this->set_previousXPosition_9(L_14);
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_16 = __this->get_startingRotation_4();
		NullCheck(L_15);
		Transform_set_rotation_m3411284563(L_15, L_16, /*hidden argument*/NULL);
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = Transform_get_position_m1104419803(L_18, /*hidden argument*/NULL);
		Vector3_t2243707580  L_20 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_21 = __this->get_currentTilt_5();
		NullCheck(L_17);
		Transform_RotateAround_m3410686872(L_17, L_19, L_20, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpeedTilt::Main()
extern "C"  void SpeedTilt_Main_m337249227 (SpeedTilt_t451906044 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TittleAnimation::.ctor()
extern "C"  void TittleAnimation__ctor_m3558554646 (TittleAnimation_t2018927818 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_waveLength_4((1.0f));
		__this->set_waveSpeed_5((1.0f));
		__this->set_waveAmplitude_6((1.0f));
		return;
	}
}
// System.Void TittleAnimation::Start()
extern Il2CppClass* Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisTransform_t3275118058_m3272947936_MethodInfo_var;
extern const uint32_t TittleAnimation_Start_m3528589182_MetadataUsageId;
extern "C"  void TittleAnimation_Start_m3528589182 (TittleAnimation_t2018927818 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TittleAnimation_Start_m3528589182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		TransformU5BU5D_t3764228911* L_0 = Component_GetComponentsInChildren_TisTransform_t3275118058_m3272947936(__this, /*hidden argument*/Component_GetComponentsInChildren_TisTransform_t3275118058_m3272947936_MethodInfo_var);
		__this->set_letters_2(L_0);
		TransformU5BU5D_t3764228911* L_1 = __this->get_letters_2();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)(Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_letterStartPosition_3(((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)L_2)));
		V_0 = 0;
		goto IL_004b;
	}

IL_0029:
	{
		Vector3U5BU5D_t1172311765* L_3 = __this->get_letterStartPosition_3();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		TransformU5BU5D_t3764228911* L_5 = __this->get_letters_2();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Transform_t3275118058 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = Transform_get_position_m1104419803(L_8, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))) = L_9;
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_004b:
	{
		int32_t L_11 = V_0;
		TransformU5BU5D_t3764228911* L_12 = __this->get_letters_2();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_12);
		int32_t L_13 = Array_get_Length_m1498215565((Il2CppArray *)(Il2CppArray *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0029;
		}
	}
	{
		return;
	}
}
// System.Void TittleAnimation::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t TittleAnimation_Update_m3752553301_MetadataUsageId;
extern "C"  void TittleAnimation_Update_m3752553301 (TittleAnimation_t2018927818 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TittleAnimation_Update_m3752553301_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = __this->get_waveLength_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_0, (0.01f), /*hidden argument*/NULL);
		__this->set_waveLength_4(L_1);
		V_0 = 0;
		goto IL_0083;
	}

IL_001d:
	{
		TransformU5BU5D_t3764228911* L_2 = __this->get_letters_2();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Transform_t3275118058 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		Vector3U5BU5D_t1172311765* L_6 = __this->get_letterStartPosition_3();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		Vector3_t2243707580  L_8 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = __this->get_waveSpeed_5();
		TransformU5BU5D_t3764228911* L_11 = __this->get_letters_2();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Transform_t3275118058 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = Transform_get_position_m1104419803(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		float L_16 = (&V_1)->get_x_1();
		float L_17 = __this->get_waveLength_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_18 = sinf(((float)((float)((float)((float)L_9*(float)L_10))+(float)((float)((float)L_16/(float)L_17)))));
		float L_19 = __this->get_waveAmplitude_6();
		Vector3_t2243707580  L_20 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_8, ((float)((float)L_18*(float)L_19)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, (*(Vector3_t2243707580 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)))), L_20, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_position_m2469242620(L_5, L_21, /*hidden argument*/NULL);
		int32_t L_22 = V_0;
		V_0 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_23 = V_0;
		TransformU5BU5D_t3764228911* L_24 = __this->get_letters_2();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_24);
		int32_t L_25 = Array_get_Length_m1498215565((Il2CppArray *)(Il2CppArray *)L_24, /*hidden argument*/NULL);
		if ((((int32_t)L_23) < ((int32_t)L_25)))
		{
			goto IL_001d;
		}
	}
	{
		return;
	}
}
// System.Void TittleAnimation::Main()
extern "C"  void TittleAnimation_Main_m497288007 (TittleAnimation_t2018927818 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Translate::.ctor()
extern "C"  void Translate__ctor_m2672653978 (Translate_t3008136214 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Translate::Start()
extern "C"  void Translate_Start_m2271988410 (Translate_t3008136214 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Translate::Update()
extern "C"  void Translate_Update_m2682358481 (Translate_t3008136214 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = __this->get_translation_2();
		float L_2 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_relativeTo_3();
		NullCheck(L_0);
		Transform_Translate_m423862381(L_0, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Translate::Main()
extern "C"  void Translate_Main_m3370282563 (Translate_t3008136214 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void WaveMotion::.ctor()
extern "C"  void WaveMotion__ctor_m2016441949 (WaveMotion_t1407033065 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_speed_2((1.0f));
		__this->set_multiplier_3((1.0f));
		return;
	}
}
// System.Void WaveMotion::Start()
extern "C"  void WaveMotion_Start_m2459040061 (WaveMotion_t1407033065 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t4030073918  L_1 = Transform_get_rotation_m1033555130(L_0, /*hidden argument*/NULL);
		__this->set_startingRotation_5(L_1);
		return;
	}
}
// System.Void WaveMotion::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t WaveMotion_Update_m2595368154_MetadataUsageId;
extern "C"  void WaveMotion_Update_m2595368154 (WaveMotion_t1407033065 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaveMotion_Update_m2595368154_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_speed_2();
		float L_2 = __this->get_offset_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = sinf(((float)((float)((float)((float)L_0*(float)L_1))+(float)L_2)));
		float L_4 = __this->get_multiplier_3();
		__this->set_waveAngle_6(((float)((float)L_3*(float)L_4)));
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_6 = __this->get_startingRotation_5();
		NullCheck(L_5);
		Transform_set_rotation_m3411284563(L_5, L_6, /*hidden argument*/NULL);
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = Transform_get_position_m1104419803(L_8, /*hidden argument*/NULL);
		Vector3_t2243707580  L_10 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = __this->get_waveAngle_6();
		NullCheck(L_7);
		Transform_RotateAround_m3410686872(L_7, L_9, L_10, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaveMotion::Main()
extern "C"  void WaveMotion_Main_m2255484154 (WaveMotion_t1407033065 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
