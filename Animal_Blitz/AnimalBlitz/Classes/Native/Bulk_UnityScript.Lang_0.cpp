﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityScript.Lang.Array
struct Array_t1396575355;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Array
struct Il2CppArray;
// System.String
struct String_t;
// UnityScript.Lang.ListUpdateableEnumerator
struct ListUpdateableEnumerator_t2462779323;
// System.Collections.IList
struct IList_t3321498491;
// UnityScript.Lang.UnityRuntimeServices
struct UnityRuntimeServices_t3303336867;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityScript_Lang_U3CModuleU3E3783534214.h"
#include "UnityScript_Lang_U3CModuleU3E3783534214MethodDeclarations.h"
#include "UnityScript_Lang_UnityScript_Lang_Array1396575355.h"
#include "UnityScript_Lang_UnityScript_Lang_Array1396575355MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Collections_CollectionBase1101587467MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_Collections_ArrayList4252133567MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "Boo_Lang_Boo_Lang_Builtins3763248930MethodDeclarations.h"
#include "UnityScript_Lang_UnityScript_Lang_Extensions2406697300.h"
#include "UnityScript_Lang_UnityScript_Lang_Extensions2406697300MethodDeclarations.h"
#include "UnityScript_Lang_UnityScript_Lang_ListUpdateableEn2462779323.h"
#include "UnityScript_Lang_UnityScript_Lang_ListUpdateableEn2462779323MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityScript_Lang_UnityScript_Lang_UnityRuntimeServ3303336867.h"
#include "UnityScript_Lang_UnityScript_Lang_UnityRuntimeServ3303336867MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices1910041954MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityScript.Lang.Array::.ctor()
extern "C"  void Array__ctor_m4252655432 (Array_t1396575355 * __this, const MethodInfo* method)
{
	{
		CollectionBase__ctor_m2525885291(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityScript.Lang.Array::Coerce(System.Type)
extern "C"  Il2CppObject * Array_Coerce_m2367934489 (Array_t1396575355 * __this, Type_t * ___toType0, const MethodInfo* method)
{
	Il2CppArray * G_B3_0 = NULL;
	{
		Type_t * L_0 = ___toType0;
		NullCheck(L_0);
		bool L_1 = Type_get_IsArray_m811277129(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Type_t * L_2 = ___toType0;
		NullCheck(L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(44 /* System.Type System.Type::GetElementType() */, L_2);
		Il2CppArray * L_4 = Array_ToBuiltin_m3086745688(__this, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_001d;
	}

IL_001c:
	{
		G_B3_0 = ((Il2CppArray *)(__this));
	}

IL_001d:
	{
		return G_B3_0;
	}
}
// System.Array UnityScript.Lang.Array::ToBuiltin(System.Type)
extern "C"  Il2CppArray * Array_ToBuiltin_m3086745688 (Array_t1396575355 * __this, Type_t * ___type0, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = CollectionBase_get_InnerList_m44314156(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		Il2CppArray * L_2 = VirtFuncInvoker1< Il2CppArray *, Type_t * >::Invoke(48 /* System.Array System.Collections.ArrayList::ToArray(System.Type) */, L_0, L_1);
		return L_2;
	}
}
// System.String UnityScript.Lang.Array::ToString()
extern Il2CppCodeGenString* _stringLiteral372029314;
extern const uint32_t Array_ToString_m1547520517_MetadataUsageId;
extern "C"  String_t* Array_ToString_m1547520517 (Array_t1396575355 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Array_ToString_m1547520517_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = Array_Join_m140080931(__this, _stringLiteral372029314, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityScript.Lang.Array::Join(System.String)
extern "C"  String_t* Array_Join_m140080931 (Array_t1396575355 * __this, String_t* ___seperator0, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = CollectionBase_get_InnerList_m44314156(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___seperator0;
		String_t* L_2 = Builtins_join_m2036613869(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityScript.Lang.Array::OnValidate(System.Object)
extern "C"  void Array_OnValidate_m1064559095 (Array_t1396575355 * __this, Il2CppObject * ___newValue0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityScript.Lang.ListUpdateableEnumerator::.ctor(System.Collections.IList)
extern "C"  void ListUpdateableEnumerator__ctor_m2483709717 (ListUpdateableEnumerator_t2462779323 * __this, Il2CppObject * ___list0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set__current_1((-1));
		Il2CppObject * L_0 = ___list0;
		__this->set__list_0(L_0);
		return;
	}
}
// System.Void UnityScript.Lang.ListUpdateableEnumerator::Reset()
extern "C"  void ListUpdateableEnumerator_Reset_m4186908375 (ListUpdateableEnumerator_t2462779323 * __this, const MethodInfo* method)
{
	{
		__this->set__current_1((-1));
		return;
	}
}
// System.Boolean UnityScript.Lang.ListUpdateableEnumerator::MoveNext()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t ListUpdateableEnumerator_MoveNext_m1727071326_MetadataUsageId;
extern "C"  bool ListUpdateableEnumerator_MoveNext_m1727071326 (ListUpdateableEnumerator_t2462779323 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListUpdateableEnumerator_MoveNext_m1727071326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get__current_1();
		if (((int64_t)L_0 + (int64_t)1 < (int64_t)kIl2CppInt32Min) || ((int64_t)L_0 + (int64_t)1 > (int64_t)kIl2CppInt32Max))
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		__this->set__current_1(((int32_t)((int32_t)L_0+(int32_t)1)));
		int32_t L_1 = __this->get__current_1();
		Il2CppObject * L_2 = __this->get__list_0();
		NullCheck(L_2);
		int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t91669223_il2cpp_TypeInfo_var, L_2);
		return (bool)((((int32_t)L_1) < ((int32_t)L_3))? 1 : 0);
	}
}
// System.Object UnityScript.Lang.ListUpdateableEnumerator::get_Current()
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t ListUpdateableEnumerator_get_Current_m1239640447_MetadataUsageId;
extern "C"  Il2CppObject * ListUpdateableEnumerator_get_Current_m1239640447 (ListUpdateableEnumerator_t2462779323 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListUpdateableEnumerator_get_Current_m1239640447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get__list_0();
		int32_t L_1 = __this->get__current_1();
		NullCheck(L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t3321498491_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void UnityScript.Lang.ListUpdateableEnumerator::Update(System.Object)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t ListUpdateableEnumerator_Update_m670620225_MetadataUsageId;
extern "C"  void ListUpdateableEnumerator_Update_m670620225 (ListUpdateableEnumerator_t2462779323 * __this, Il2CppObject * ___newValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ListUpdateableEnumerator_Update_m670620225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get__list_0();
		int32_t L_1 = __this->get__current_1();
		Il2CppObject * L_2 = ___newValue0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(3 /* System.Void System.Collections.IList::set_Item(System.Int32,System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void UnityScript.Lang.UnityRuntimeServices::.cctor()
extern const Il2CppType* Extensions_t2406697300_0_0_0_var;
extern Il2CppClass* UnityRuntimeServices_t3303336867_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern const uint32_t UnityRuntimeServices__cctor_m277244785_MetadataUsageId;
extern "C"  void UnityRuntimeServices__cctor_m277244785 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityRuntimeServices__cctor_m277244785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityRuntimeServices_U24static_initializerU24_m2178156047(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Extensions_t2406697300_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		RuntimeServices_RegisterExtensions_m3796711317(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		((UnityRuntimeServices_t3303336867_StaticFields*)UnityRuntimeServices_t3303336867_il2cpp_TypeInfo_var->static_fields)->set_Initialized_2((bool)1);
		return;
	}
}
// System.Void UnityScript.Lang.UnityRuntimeServices::.ctor()
extern "C"  void UnityRuntimeServices__ctor_m1066678718 (UnityRuntimeServices_t3303336867 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityScript.Lang.UnityRuntimeServices::GetEnumerator(System.Object)
extern const Il2CppType* IList_t3321498491_0_0_0_var;
extern Il2CppClass* UnityRuntimeServices_t3303336867_il2cpp_TypeInfo_var;
extern Il2CppClass* Array_t1396575355_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern Il2CppClass* ListUpdateableEnumerator_t2462779323_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t UnityRuntimeServices_GetEnumerator_m1135949016_MetadataUsageId;
extern "C"  Il2CppObject * UnityRuntimeServices_GetEnumerator_m1135949016 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityRuntimeServices_GetEnumerator_m1135949016_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B12_0 = NULL;
	Il2CppObject * G_B6_0 = NULL;
	Il2CppObject * G_B5_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityRuntimeServices_t3303336867_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((UnityRuntimeServices_t3303336867_StaticFields*)UnityRuntimeServices_t3303336867_il2cpp_TypeInfo_var->static_fields)->get_EmptyEnumerator_0();
		G_B12_0 = L_1;
		goto IL_0086;
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityRuntimeServices_t3303336867_il2cpp_TypeInfo_var);
		bool L_3 = UnityRuntimeServices_IsValueTypeArray_m651527436(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = ___obj0;
		if (!((Array_t1396575355 *)IsInstClass(L_4, Array_t1396575355_il2cpp_TypeInfo_var)))
		{
			goto IL_0050;
		}
	}

IL_0026:
	{
		Il2CppObject * L_5 = ___obj0;
		Il2CppObject * L_6 = L_5;
		G_B5_0 = L_6;
		if (((Il2CppObject *)IsInst(L_6, IList_t3321498491_il2cpp_TypeInfo_var)))
		{
			G_B6_0 = L_6;
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(IList_t3321498491_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = RuntimeServices_Coerce_m43784504(NULL /*static, unused*/, G_B5_0, L_7, /*hidden argument*/NULL);
		G_B6_0 = L_8;
	}

IL_0041:
	{
		ListUpdateableEnumerator_t2462779323 * L_9 = (ListUpdateableEnumerator_t2462779323 *)il2cpp_codegen_object_new(ListUpdateableEnumerator_t2462779323_il2cpp_TypeInfo_var);
		ListUpdateableEnumerator__ctor_m2483709717(L_9, ((Il2CppObject *)Castclass(G_B6_0, IList_t3321498491_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		G_B12_0 = ((Il2CppObject *)(L_9));
		goto IL_0086;
	}

IL_0050:
	{
		Il2CppObject * L_10 = ___obj0;
		V_0 = ((Il2CppObject *)IsInst(L_10, IEnumerable_t2911409499_il2cpp_TypeInfo_var));
		Il2CppObject * L_11 = V_0;
		if (!L_11)
		{
			goto IL_0068;
		}
	}
	{
		Il2CppObject * L_12 = V_0;
		NullCheck(L_12);
		Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_12);
		G_B12_0 = L_13;
		goto IL_0086;
	}

IL_0068:
	{
		Il2CppObject * L_14 = ___obj0;
		V_1 = ((Il2CppObject *)IsInst(L_14, IEnumerator_t1466026749_il2cpp_TypeInfo_var));
		Il2CppObject * L_15 = V_1;
		if (!L_15)
		{
			goto IL_007b;
		}
	}
	{
		Il2CppObject * L_16 = V_1;
		G_B12_0 = L_16;
		goto IL_0086;
	}

IL_007b:
	{
		Il2CppObject * L_17 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		Il2CppObject * L_18 = RuntimeServices_GetEnumerable_m664169683(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Il2CppObject * L_19 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_18);
		G_B12_0 = L_19;
	}

IL_0086:
	{
		return G_B12_0;
	}
}
// System.Void UnityScript.Lang.UnityRuntimeServices::Update(System.Collections.IEnumerator,System.Object)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ListUpdateableEnumerator_t2462779323_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029369;
extern const uint32_t UnityRuntimeServices_Update_m1436857700_MetadataUsageId;
extern "C"  void UnityRuntimeServices_Update_m1436857700 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___e0, Il2CppObject * ___newValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityRuntimeServices_Update_m1436857700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___e0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral372029369, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___e0;
		if (((ListUpdateableEnumerator_t2462779323 *)IsInstClass(L_2, ListUpdateableEnumerator_t2462779323_il2cpp_TypeInfo_var)))
		{
			goto IL_0021;
		}
	}
	{
		goto IL_002d;
	}

IL_0021:
	{
		Il2CppObject * L_3 = ___e0;
		Il2CppObject * L_4 = ___newValue1;
		NullCheck(((ListUpdateableEnumerator_t2462779323 *)CastclassClass(L_3, ListUpdateableEnumerator_t2462779323_il2cpp_TypeInfo_var)));
		ListUpdateableEnumerator_Update_m670620225(((ListUpdateableEnumerator_t2462779323 *)CastclassClass(L_3, ListUpdateableEnumerator_t2462779323_il2cpp_TypeInfo_var)), L_4, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Boolean UnityScript.Lang.UnityRuntimeServices::IsValueTypeArray(System.Object)
extern Il2CppClass* Il2CppArray_il2cpp_TypeInfo_var;
extern const uint32_t UnityRuntimeServices_IsValueTypeArray_m651527436_MetadataUsageId;
extern "C"  bool UnityRuntimeServices_IsValueTypeArray_m651527436 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityRuntimeServices_IsValueTypeArray_m651527436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		if (((Il2CppArray *)IsInstClass(L_0, Il2CppArray_il2cpp_TypeInfo_var)))
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0021;
	}

IL_0011:
	{
		Il2CppObject * L_1 = ___obj0;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m191970594(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(44 /* System.Type System.Type::GetElementType() */, L_2);
		NullCheck(L_3);
		bool L_4 = Type_get_IsValueType_m1733572463(L_3, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_4));
	}

IL_0021:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityScript.Lang.UnityRuntimeServices::$static_initializer$()
extern const Il2CppType* IEnumerator_t1466026749_0_0_0_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityRuntimeServices_t3303336867_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityRuntimeServices_U24static_initializerU24_m2178156047_MetadataUsageId;
extern "C"  void UnityRuntimeServices_U24static_initializerU24_m2178156047 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityRuntimeServices_U24static_initializerU24_m2178156047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppArray *)(Il2CppArray *)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)));
		Il2CppObject * L_0 = Array_GetEnumerator_m2284404958((Il2CppArray *)(Il2CppArray *)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityRuntimeServices_t3303336867_il2cpp_TypeInfo_var);
		((UnityRuntimeServices_t3303336867_StaticFields*)UnityRuntimeServices_t3303336867_il2cpp_TypeInfo_var->static_fields)->set_EmptyEnumerator_0(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(IEnumerator_t1466026749_0_0_0_var), /*hidden argument*/NULL);
		((UnityRuntimeServices_t3303336867_StaticFields*)UnityRuntimeServices_t3303336867_il2cpp_TypeInfo_var->static_fields)->set_EnumeratorType_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
