﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Ricimi.TintedButton/ButtonClickedEvent
struct ButtonClickedEvent_t2671903891;
// Ricimi.Transition
struct Transition_t2490031921;
// UnityEngine.Canvas
struct Canvas_t209405766;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Ricimi.Transition/<RunFade>c__Iterator0
struct U3CRunFadeU3Ec__Iterator0_t4246392297;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// Rotate
struct Rotate_t4255939431;
// RunForwardandBack
struct RunForwardandBack_t3788415244;
// SimpleCharacterControler2D
struct SimpleCharacterControler2D_t2053125211;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// SmoothMouseLook
struct SmoothMouseLook_t128928814;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// SpaceDetection
struct SpaceDetection_t1772284253;
// UnityEngine.Collider
struct Collider_t3497673348;
// CharacterControl
struct CharacterControl_t3164190352;
// SpriteTrail
struct SpriteTrail_t2284883325;
// UnityEngine.Transform
struct Transform_t3275118058;
// TrailPreset
struct TrailPreset_t465077881;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// TrailElement
struct TrailElement_t1685395368;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// TabBase
struct TabBase_t319583306;
// System.Collections.Generic.Dictionary`2<System.Int32,tBase>
struct Dictionary_2_t728786214;
// TabBase/baseLegID
struct baseLegID_t3972271149;
// TabBase/tabID
struct tabID_t570832297;
// tBase
struct tBase_t1720960579;
// testBle
struct testBle_t3025851879;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// testBle/<Connect>c__AnonStorey0
struct U3CConnectU3Ec__AnonStorey0_t350889135;
// testBle/baseLegID
struct baseLegID_t1303619648;
// TestSceneManager
struct TestSceneManager_t2345075657;
// TrailManagerExample
struct TrailManagerExample_t3762595367;
// UIActions
struct UIActions_t467790635;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UIDetection
struct UIDetection_t3199971681;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// IconControl
struct IconControl_t564227302;
// UILineRenderer
struct UILineRenderer_t1431191837;
// UnityEngine.Texture
struct Texture_t2243626319;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t573379950;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// winner
struct winner_t1142905907;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Ricimi_TintedButton_ButtonClicke2671903891.h"
#include "AssemblyU2DCSharp_Ricimi_TintedButton_ButtonClicke2671903891MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097MethodDeclarations.h"
#include "AssemblyU2DCSharp_Ricimi_Transition2490031921.h"
#include "AssemblyU2DCSharp_Ricimi_Transition2490031921MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas209405766MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_RenderMode4280533217.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharp_Ricimi_Transition_U3CRunFadeU3Ec4246392297MethodDeclarations.h"
#include "AssemblyU2DCSharp_Ricimi_Transition_U3CRunFadeU3Ec4246392297.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite309593783MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CanvasRenderer261436805MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UnityEngine_Texture2243626319MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_CanvasRenderer261436805.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_Rotate4255939431.h"
#include "AssemblyU2DCSharp_Rotate4255939431MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "AssemblyU2DCSharp_RunForwardandBack3788415244.h"
#include "AssemblyU2DCSharp_RunForwardandBack3788415244MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "AssemblyU2DCSharp_SimpleCharacterControler2D2053125211.h"
#include "AssemblyU2DCSharp_SimpleCharacterControler2D2053125211MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "UnityEngine_UnityEngine_ForceMode2D4177575466.h"
#include "AssemblyU2DCSharp_SmoothMouseLook128928814.h"
#include "AssemblyU2DCSharp_SmoothMouseLook128928814MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1445631064MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1445631064.h"
#include "AssemblyU2DCSharp_SmoothMouseLook_RotationAxes1370738632.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat980360738.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat980360738MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "AssemblyU2DCSharp_SmoothMouseLook_RotationAxes1370738632MethodDeclarations.h"
#include "AssemblyU2DCSharp_SpaceDetection1772284253.h"
#include "AssemblyU2DCSharp_SpaceDetection1772284253MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "AssemblyU2DCSharp_CharacterControl3164190352.h"
#include "AssemblyU2DCSharp_SpriteTrail2284883325.h"
#include "AssemblyU2DCSharp_SpriteTrail2284883325MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_gen1505052203MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_gen1505052203.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "AssemblyU2DCSharp_TrailPreset465077881.h"
#include "AssemblyU2DCSharp_TrailActivationCondition372043875.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198.h"
#include "AssemblyU2DCSharp_TrailElement1685395368MethodDeclarations.h"
#include "AssemblyU2DCSharp_TrailElement1685395368.h"
#include "AssemblyU2DCSharp_TrailDisactivationCondition1222614977.h"
#include "AssemblyU2DCSharp_TrailElementSpawnCondition2660531040.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821MethodDeclarations.h"
#include "AssemblyU2DCSharp_TabBase319583306.h"
#include "AssemblyU2DCSharp_TabBase319583306MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge728786214MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3873625228MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2980096784MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge728786214.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3873625228.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2980096784.h"
#include "AssemblyU2DCSharp_testBle3025851879MethodDeclarations.h"
#include "AssemblyU2DCSharp_TabBase_tabID570832297MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "AssemblyU2DCSharp_TabBase_baseLegID3972271149MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21630970450.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En898682634.h"
#include "AssemblyU2DCSharp_TabBase_tabID570832297.h"
#include "AssemblyU2DCSharp_TabBase_baseLegID3972271149.h"
#include "mscorlib_System_TimeoutException3246754798.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En898682634MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21630970450MethodDeclarations.h"
#include "AssemblyU2DCSharp_tBase1720960579.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22781098732.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2048810916.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2048810916MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22781098732MethodDeclarations.h"
#include "AssemblyU2DCSharp_tBase1720960579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayout2579273657MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_GUILayoutOption4183744904.h"
#include "AssemblyU2DCSharp_testBle3025851879.h"
#include "System_Core_System_Action3226471752MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1831019615MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothLEHardwareI4023789856MethodDeclarations.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Action_1_gen1831019615.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothDeviceScript810775987.h"
#include "System_Core_System_Action_2_gen4234541925MethodDeclarations.h"
#include "System_Core_System_Action_3_gen3256166369MethodDeclarations.h"
#include "System_Core_System_Action_2_gen4234541925.h"
#include "System_Core_System_Action_3_gen3256166369.h"
#include "System_Core_System_Action_4_gen2314457268MethodDeclarations.h"
#include "System_Core_System_Action_4_gen2314457268.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Collections_Generic_List_1_gen672740780MethodDeclarations.h"
#include "AssemblyU2DCSharp_testBle_baseLegID1303619648MethodDeclarations.h"
#include "AssemblyU2DCSharp_testBle_baseLegID1303619648.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat207470454.h"
#include "mscorlib_System_Collections_Generic_List_1_gen672740780.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat207470454MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "AssemblyU2DCSharp_testBle_U3CConnectU3Ec__AnonStore350889135MethodDeclarations.h"
#include "System_Core_System_Action_3_gen329312853MethodDeclarations.h"
#include "AssemblyU2DCSharp_testBle_U3CConnectU3Ec__AnonStore350889135.h"
#include "System_Core_System_Action_3_gen329312853.h"
#include "AssemblyU2DCSharp_TestSceneManager2345075657.h"
#include "AssemblyU2DCSharp_TestSceneManager2345075657MethodDeclarations.h"
#include "AssemblyU2DCSharp_TrailActivationCondition372043875MethodDeclarations.h"
#include "AssemblyU2DCSharp_TrailDisactivationCondition1222614977MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen2844261301MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen2844261301.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat2015115283.h"
#include "AssemblyU2DCSharp_TrailElementDurationCondition3016874467.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat2015115283MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Gradient3600583008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Gradient3600583008.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"
#include "AssemblyU2DCSharp_TrailElementDurationCondition3016874467MethodDeclarations.h"
#include "AssemblyU2DCSharp_TrailElementSpawnCondition2660531040MethodDeclarations.h"
#include "AssemblyU2DCSharp_TrailManagerExample3762595367.h"
#include "AssemblyU2DCSharp_TrailManagerExample3762595367MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_TrailPreset465077881MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIActions467790635.h"
#include "AssemblyU2DCSharp_UIActions467790635MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3325146001MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "AssemblyU2DCSharp_UIDetection3199971681.h"
#include "AssemblyU2DCSharp_UIDetection3199971681MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "AssemblyU2DCSharp_IconControl564227302MethodDeclarations.h"
#include "AssemblyU2DCSharp_IconControl564227302.h"
#include "AssemblyU2DCSharp_UILineRenderer1431191837.h"
#include "AssemblyU2DCSharp_UILineRenderer1431191837MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen573379950.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828711MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen573379950MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828711.h"
#include "UnityEngine_UnityEngine_Color32874517518MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "AssemblyU2DCSharp_winner1142905907.h"
#include "AssemblyU2DCSharp_winner1142905907MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"

// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3813873105(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Canvas>()
#define GameObject_AddComponent_TisCanvas_t209405766_m2311451932(__this, method) ((  Canvas_t209405766 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Ricimi.Transition>()
#define GameObject_AddComponent_TisTransition_t2490031921_m3134363720(__this, method) ((  Transition_t2490031921 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2650145732(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Ricimi.Transition>()
#define GameObject_GetComponent_TisTransition_t2490031921_m2092107195(__this, method) ((  Transition_t2490031921 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.UI.Image>()
#define GameObject_AddComponent_TisImage_t2042527209_m4249278385(__this, method) ((  Image_t2042527209 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
#define GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(__this, method) ((  RectTransform_t3349966182 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, method) ((  Rigidbody2D_t502193897 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, method) ((  Rigidbody_t4233889191 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<CharacterControl>()
#define GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381(__this, method) ((  CharacterControl_t3164190352 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.SpriteRenderer>()
#define GameObject_AddComponent_TisSpriteRenderer_t1209076198_m3314035363(__this, method) ((  SpriteRenderer_t1209076198 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<TrailElement>()
#define GameObject_AddComponent_TisTrailElement_t1685395368_m2634836246(__this, method) ((  TrailElement_t1685395368 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<TrailElement>()
#define GameObject_GetComponent_TisTrailElement_t1685395368_m898602943(__this, method) ((  TrailElement_t1685395368 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(__this, method) ((  SpriteRenderer_t1209076198 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m447919519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m447919519(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t1756533147_m3664764861(__this /* static, unused */, p0, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody2D>()
#define GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(__this, method) ((  Rigidbody2D_t502193897 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<SimpleCharacterControler2D>()
#define GameObject_GetComponent_TisSimpleCharacterControler2D_t2053125211_m438366674(__this, method) ((  SimpleCharacterControler2D_t2053125211 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
#define GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039(__this, method) ((  AudioSource_t1135106623 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<IconControl>()
#define GameObject_GetComponent_TisIconControl_t564227302_m435800415(__this, method) ((  IconControl_t564227302 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t356221433_m1342661039(__this, method) ((  Text_t356221433 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Ricimi.TintedButton/ButtonClickedEvent::.ctor()
extern "C"  void ButtonClickedEvent__ctor_m2446830550 (ButtonClickedEvent_t2671903891 * __this, const MethodInfo* method)
{
	{
		UnityEvent__ctor_m588741179(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ricimi.Transition::.ctor()
extern "C"  void Transition__ctor_m1835182235 (Transition_t2490031921 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ricimi.Transition::Awake()
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Transition_t2490031921_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisCanvas_t209405766_m2311451932_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1009703027;
extern const uint32_t Transition_Awake_m3044031284_MetadataUsageId;
extern "C"  void Transition_Awake_m3044031284 (Transition_t2490031921 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transition_Awake_m3044031284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Canvas_t209405766 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_0, _stringLiteral1009703027, /*hidden argument*/NULL);
		((Transition_t2490031921_StaticFields*)Transition_t2490031921_il2cpp_TypeInfo_var->static_fields)->set_m_canvas_2(L_0);
		GameObject_t1756533147 * L_1 = ((Transition_t2490031921_StaticFields*)Transition_t2490031921_il2cpp_TypeInfo_var->static_fields)->get_m_canvas_2();
		NullCheck(L_1);
		Canvas_t209405766 * L_2 = GameObject_AddComponent_TisCanvas_t209405766_m2311451932(L_1, /*hidden argument*/GameObject_AddComponent_TisCanvas_t209405766_m2311451932_MethodInfo_var);
		V_0 = L_2;
		Canvas_t209405766 * L_3 = V_0;
		NullCheck(L_3);
		Canvas_set_renderMode_m4114781335(L_3, 0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = ((Transition_t2490031921_StaticFields*)Transition_t2490031921_il2cpp_TypeInfo_var->static_fields)->get_m_canvas_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ricimi.Transition::LoadLevel(System.String,System.Single,UnityEngine.Color)
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Transition_t2490031921_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisTransition_t2490031921_m3134363720_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTransition_t2490031921_m2092107195_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3952796043;
extern const uint32_t Transition_LoadLevel_m3399519432_MetadataUsageId;
extern "C"  void Transition_LoadLevel_m3399519432 (Il2CppObject * __this /* static, unused */, String_t* ___level0, float ___duration1, Color_t2020392075  ___color2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transition_LoadLevel_m3399519432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_0, _stringLiteral3952796043, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = V_0;
		NullCheck(L_1);
		GameObject_AddComponent_TisTransition_t2490031921_m3134363720(L_1, /*hidden argument*/GameObject_AddComponent_TisTransition_t2490031921_m3134363720_MethodInfo_var);
		GameObject_t1756533147 * L_2 = V_0;
		NullCheck(L_2);
		Transition_t2490031921 * L_3 = GameObject_GetComponent_TisTransition_t2490031921_m2092107195(L_2, /*hidden argument*/GameObject_GetComponent_TisTransition_t2490031921_m2092107195_MethodInfo_var);
		String_t* L_4 = ___level0;
		float L_5 = ___duration1;
		Color_t2020392075  L_6 = ___color2;
		NullCheck(L_3);
		Transition_StartFade_m3181661324(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = V_0;
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_9 = ((Transition_t2490031921_StaticFields*)Transition_t2490031921_il2cpp_TypeInfo_var->static_fields)->get_m_canvas_2();
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_SetParent_m1963830867(L_8, L_10, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = V_0;
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_SetAsLastSibling_m1528402907(L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ricimi.Transition::StartFade(System.String,System.Single,UnityEngine.Color)
extern "C"  void Transition_StartFade_m3181661324 (Transition_t2490031921 * __this, String_t* ___level0, float ___duration1, Color_t2020392075  ___fadeColor2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___level0;
		float L_1 = ___duration1;
		Color_t2020392075  L_2 = ___fadeColor2;
		Il2CppObject * L_3 = Transition_RunFade_m2930965847(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Ricimi.Transition::RunFade(System.String,System.Single,UnityEngine.Color)
extern Il2CppClass* U3CRunFadeU3Ec__Iterator0_t4246392297_il2cpp_TypeInfo_var;
extern const uint32_t Transition_RunFade_m2930965847_MetadataUsageId;
extern "C"  Il2CppObject * Transition_RunFade_m2930965847 (Transition_t2490031921 * __this, String_t* ___level0, float ___duration1, Color_t2020392075  ___fadeColor2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transition_RunFade_m2930965847_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CRunFadeU3Ec__Iterator0_t4246392297 * V_0 = NULL;
	{
		U3CRunFadeU3Ec__Iterator0_t4246392297 * L_0 = (U3CRunFadeU3Ec__Iterator0_t4246392297 *)il2cpp_codegen_object_new(U3CRunFadeU3Ec__Iterator0_t4246392297_il2cpp_TypeInfo_var);
		U3CRunFadeU3Ec__Iterator0__ctor_m617716714(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRunFadeU3Ec__Iterator0_t4246392297 * L_1 = V_0;
		Color_t2020392075  L_2 = ___fadeColor2;
		NullCheck(L_1);
		L_1->set_fadeColor_1(L_2);
		U3CRunFadeU3Ec__Iterator0_t4246392297 * L_3 = V_0;
		float L_4 = ___duration1;
		NullCheck(L_3);
		L_3->set_duration_7(L_4);
		U3CRunFadeU3Ec__Iterator0_t4246392297 * L_5 = V_0;
		String_t* L_6 = ___level0;
		NullCheck(L_5);
		L_5->set_level_9(L_6);
		U3CRunFadeU3Ec__Iterator0_t4246392297 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_U24this_10(__this);
		U3CRunFadeU3Ec__Iterator0_t4246392297 * L_8 = V_0;
		return L_8;
	}
}
// System.Void Ricimi.Transition/<RunFade>c__Iterator0::.ctor()
extern "C"  void U3CRunFadeU3Ec__Iterator0__ctor_m617716714 (U3CRunFadeU3Ec__Iterator0_t4246392297 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Ricimi.Transition/<RunFade>c__Iterator0::MoveNext()
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Transition_t2490031921_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisImage_t2042527209_m4249278385_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var;
extern const uint32_t U3CRunFadeU3Ec__Iterator0_MoveNext_m603888794_MetadataUsageId;
extern "C"  bool U3CRunFadeU3Ec__Iterator0_MoveNext_m603888794 (U3CRunFadeU3Ec__Iterator0_t4246392297 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CRunFadeU3Ec__Iterator0_MoveNext_m603888794_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_13();
		V_0 = L_0;
		__this->set_U24PC_13((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002d;
		}
		if (L_1 == 1)
		{
			goto IL_0235;
		}
		if (L_1 == 2)
		{
			goto IL_027a;
		}
		if (L_1 == 3)
		{
			goto IL_02f2;
		}
		if (L_1 == 4)
		{
			goto IL_0337;
		}
	}
	{
		goto IL_0348;
	}

IL_002d:
	{
		Texture2D_t3542995729 * L_2 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3598323350(L_2, 1, 1, /*hidden argument*/NULL);
		__this->set_U3CbgTexU3E__0_0(L_2);
		Texture2D_t3542995729 * L_3 = __this->get_U3CbgTexU3E__0_0();
		Color_t2020392075  L_4 = __this->get_fadeColor_1();
		NullCheck(L_3);
		Texture2D_SetPixel_m609991514(L_3, 0, 0, L_4, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_5 = __this->get_U3CbgTexU3E__0_0();
		NullCheck(L_5);
		Texture2D_Apply_m3543341930(L_5, /*hidden argument*/NULL);
		Transition_t2490031921 * L_6 = __this->get_U24this_10();
		GameObject_t1756533147 * L_7 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m498247354(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->set_m_overlay_3(L_7);
		Transition_t2490031921 * L_8 = __this->get_U24this_10();
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = L_8->get_m_overlay_3();
		NullCheck(L_9);
		Image_t2042527209 * L_10 = GameObject_AddComponent_TisImage_t2042527209_m4249278385(L_9, /*hidden argument*/GameObject_AddComponent_TisImage_t2042527209_m4249278385_MethodInfo_var);
		__this->set_U3CimageU3E__1_2(L_10);
		Texture2D_t3542995729 * L_11 = __this->get_U3CbgTexU3E__0_0();
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_11);
		Texture2D_t3542995729 * L_13 = __this->get_U3CbgTexU3E__0_0();
		NullCheck(L_13);
		int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_13);
		Rect_t3681755626  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Rect__ctor_m1220545469(&L_15, (0.0f), (0.0f), (((float)((float)L_12))), (((float)((float)L_14))), /*hidden argument*/NULL);
		__this->set_U3CrectU3E__2_3(L_15);
		Texture2D_t3542995729 * L_16 = __this->get_U3CbgTexU3E__0_0();
		Rect_t3681755626  L_17 = __this->get_U3CrectU3E__2_3();
		Vector2_t2243707579  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector2__ctor_m3067419446(&L_18, (0.5f), (0.5f), /*hidden argument*/NULL);
		Sprite_t309593783 * L_19 = Sprite_Create_m634597153(NULL /*static, unused*/, L_16, L_17, L_18, (1.0f), /*hidden argument*/NULL);
		__this->set_U3CspriteU3E__3_4(L_19);
		Image_t2042527209 * L_20 = __this->get_U3CimageU3E__1_2();
		NullCheck(L_20);
		Material_t193706927 * L_21 = VirtFuncInvoker0< Material_t193706927 * >::Invoke(32 /* UnityEngine.Material UnityEngine.UI.Graphic::get_material() */, L_20);
		Texture2D_t3542995729 * L_22 = __this->get_U3CbgTexU3E__0_0();
		NullCheck(L_21);
		Material_set_mainTexture_m3584203343(L_21, L_22, /*hidden argument*/NULL);
		Image_t2042527209 * L_23 = __this->get_U3CimageU3E__1_2();
		Sprite_t309593783 * L_24 = __this->get_U3CspriteU3E__3_4();
		NullCheck(L_23);
		Image_set_sprite_m1800056820(L_23, L_24, /*hidden argument*/NULL);
		Image_t2042527209 * L_25 = __this->get_U3CimageU3E__1_2();
		NullCheck(L_25);
		Color_t2020392075  L_26 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_25);
		__this->set_U3CnewColorU3E__4_5(L_26);
		Image_t2042527209 * L_27 = __this->get_U3CimageU3E__1_2();
		Color_t2020392075  L_28 = __this->get_U3CnewColorU3E__4_5();
		NullCheck(L_27);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_27, L_28);
		Image_t2042527209 * L_29 = __this->get_U3CimageU3E__1_2();
		NullCheck(L_29);
		CanvasRenderer_t261436805 * L_30 = Graphic_get_canvasRenderer_m2902370808(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		CanvasRenderer_SetAlpha_m3758481827(L_30, (0.0f), /*hidden argument*/NULL);
		Transition_t2490031921 * L_31 = __this->get_U24this_10();
		NullCheck(L_31);
		GameObject_t1756533147 * L_32 = L_31->get_m_overlay_3();
		NullCheck(L_32);
		Transform_t3275118058 * L_33 = GameObject_get_transform_m909382139(L_32, /*hidden argument*/NULL);
		Vector3_t2243707580  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector3__ctor_m2638739322(&L_34, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_set_localScale_m2325460848(L_33, L_34, /*hidden argument*/NULL);
		Transition_t2490031921 * L_35 = __this->get_U24this_10();
		NullCheck(L_35);
		GameObject_t1756533147 * L_36 = L_35->get_m_overlay_3();
		NullCheck(L_36);
		RectTransform_t3349966182 * L_37 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_36, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		GameObject_t1756533147 * L_38 = ((Transition_t2490031921_StaticFields*)Transition_t2490031921_il2cpp_TypeInfo_var->static_fields)->get_m_canvas_2();
		NullCheck(L_38);
		RectTransform_t3349966182 * L_39 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_38, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		NullCheck(L_39);
		Vector2_t2243707579  L_40 = RectTransform_get_sizeDelta_m2157326342(L_39, /*hidden argument*/NULL);
		NullCheck(L_37);
		RectTransform_set_sizeDelta_m2319668137(L_37, L_40, /*hidden argument*/NULL);
		Transition_t2490031921 * L_41 = __this->get_U24this_10();
		NullCheck(L_41);
		GameObject_t1756533147 * L_42 = L_41->get_m_overlay_3();
		NullCheck(L_42);
		Transform_t3275118058 * L_43 = GameObject_get_transform_m909382139(L_42, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_44 = ((Transition_t2490031921_StaticFields*)Transition_t2490031921_il2cpp_TypeInfo_var->static_fields)->get_m_canvas_2();
		NullCheck(L_44);
		Transform_t3275118058 * L_45 = GameObject_get_transform_m909382139(L_44, /*hidden argument*/NULL);
		NullCheck(L_43);
		Transform_SetParent_m1963830867(L_43, L_45, (bool)0, /*hidden argument*/NULL);
		Transition_t2490031921 * L_46 = __this->get_U24this_10();
		NullCheck(L_46);
		GameObject_t1756533147 * L_47 = L_46->get_m_overlay_3();
		NullCheck(L_47);
		Transform_t3275118058 * L_48 = GameObject_get_transform_m909382139(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		Transform_SetAsFirstSibling_m3606528771(L_48, /*hidden argument*/NULL);
		__this->set_U3CtimeU3E__5_6((0.0f));
		float L_49 = __this->get_duration_7();
		__this->set_U3ChalfDurationU3E__6_8(((float)((float)L_49/(float)(2.0f))));
		goto IL_0235;
	}

IL_01d8:
	{
		float L_50 = __this->get_U3CtimeU3E__5_6();
		float L_51 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtimeU3E__5_6(((float)((float)L_50+(float)L_51)));
		Image_t2042527209 * L_52 = __this->get_U3CimageU3E__1_2();
		NullCheck(L_52);
		CanvasRenderer_t261436805 * L_53 = Graphic_get_canvasRenderer_m2902370808(L_52, /*hidden argument*/NULL);
		float L_54 = __this->get_U3CtimeU3E__5_6();
		float L_55 = __this->get_U3ChalfDurationU3E__6_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_56 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, (0.0f), (1.0f), ((float)((float)L_54/(float)L_55)), /*hidden argument*/NULL);
		NullCheck(L_53);
		CanvasRenderer_SetAlpha_m3758481827(L_53, L_56, /*hidden argument*/NULL);
		WaitForEndOfFrame_t1785723201 * L_57 = (WaitForEndOfFrame_t1785723201 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m3062480170(L_57, /*hidden argument*/NULL);
		__this->set_U24current_11(L_57);
		bool L_58 = __this->get_U24disposing_12();
		if (L_58)
		{
			goto IL_0230;
		}
	}
	{
		__this->set_U24PC_13(1);
	}

IL_0230:
	{
		goto IL_034a;
	}

IL_0235:
	{
		float L_59 = __this->get_U3CtimeU3E__5_6();
		float L_60 = __this->get_U3ChalfDurationU3E__6_8();
		if ((((float)L_59) < ((float)L_60)))
		{
			goto IL_01d8;
		}
	}
	{
		Image_t2042527209 * L_61 = __this->get_U3CimageU3E__1_2();
		NullCheck(L_61);
		CanvasRenderer_t261436805 * L_62 = Graphic_get_canvasRenderer_m2902370808(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		CanvasRenderer_SetAlpha_m3758481827(L_62, (1.0f), /*hidden argument*/NULL);
		WaitForEndOfFrame_t1785723201 * L_63 = (WaitForEndOfFrame_t1785723201 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m3062480170(L_63, /*hidden argument*/NULL);
		__this->set_U24current_11(L_63);
		bool L_64 = __this->get_U24disposing_12();
		if (L_64)
		{
			goto IL_0275;
		}
	}
	{
		__this->set_U24PC_13(2);
	}

IL_0275:
	{
		goto IL_034a;
	}

IL_027a:
	{
		String_t* L_65 = __this->get_level_9();
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		__this->set_U3CtimeU3E__5_6((0.0f));
		goto IL_02f2;
	}

IL_0295:
	{
		float L_66 = __this->get_U3CtimeU3E__5_6();
		float L_67 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtimeU3E__5_6(((float)((float)L_66+(float)L_67)));
		Image_t2042527209 * L_68 = __this->get_U3CimageU3E__1_2();
		NullCheck(L_68);
		CanvasRenderer_t261436805 * L_69 = Graphic_get_canvasRenderer_m2902370808(L_68, /*hidden argument*/NULL);
		float L_70 = __this->get_U3CtimeU3E__5_6();
		float L_71 = __this->get_U3ChalfDurationU3E__6_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_72 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, (1.0f), (0.0f), ((float)((float)L_70/(float)L_71)), /*hidden argument*/NULL);
		NullCheck(L_69);
		CanvasRenderer_SetAlpha_m3758481827(L_69, L_72, /*hidden argument*/NULL);
		WaitForEndOfFrame_t1785723201 * L_73 = (WaitForEndOfFrame_t1785723201 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m3062480170(L_73, /*hidden argument*/NULL);
		__this->set_U24current_11(L_73);
		bool L_74 = __this->get_U24disposing_12();
		if (L_74)
		{
			goto IL_02ed;
		}
	}
	{
		__this->set_U24PC_13(3);
	}

IL_02ed:
	{
		goto IL_034a;
	}

IL_02f2:
	{
		float L_75 = __this->get_U3CtimeU3E__5_6();
		float L_76 = __this->get_U3ChalfDurationU3E__6_8();
		if ((((float)L_75) < ((float)L_76)))
		{
			goto IL_0295;
		}
	}
	{
		Image_t2042527209 * L_77 = __this->get_U3CimageU3E__1_2();
		NullCheck(L_77);
		CanvasRenderer_t261436805 * L_78 = Graphic_get_canvasRenderer_m2902370808(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		CanvasRenderer_SetAlpha_m3758481827(L_78, (0.0f), /*hidden argument*/NULL);
		WaitForEndOfFrame_t1785723201 * L_79 = (WaitForEndOfFrame_t1785723201 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m3062480170(L_79, /*hidden argument*/NULL);
		__this->set_U24current_11(L_79);
		bool L_80 = __this->get_U24disposing_12();
		if (L_80)
		{
			goto IL_0332;
		}
	}
	{
		__this->set_U24PC_13(4);
	}

IL_0332:
	{
		goto IL_034a;
	}

IL_0337:
	{
		GameObject_t1756533147 * L_81 = ((Transition_t2490031921_StaticFields*)Transition_t2490031921_il2cpp_TypeInfo_var->static_fields)->get_m_canvas_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_81, /*hidden argument*/NULL);
		__this->set_U24PC_13((-1));
	}

IL_0348:
	{
		return (bool)0;
	}

IL_034a:
	{
		return (bool)1;
	}
}
// System.Object Ricimi.Transition/<RunFade>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRunFadeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1503239844 (U3CRunFadeU3Ec__Iterator0_t4246392297 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_11();
		return L_0;
	}
}
// System.Object Ricimi.Transition/<RunFade>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRunFadeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1326117884 (U3CRunFadeU3Ec__Iterator0_t4246392297 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_11();
		return L_0;
	}
}
// System.Void Ricimi.Transition/<RunFade>c__Iterator0::Dispose()
extern "C"  void U3CRunFadeU3Ec__Iterator0_Dispose_m4194118907 (U3CRunFadeU3Ec__Iterator0_t4246392297 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_12((bool)1);
		__this->set_U24PC_13((-1));
		return;
	}
}
// System.Void Ricimi.Transition/<RunFade>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CRunFadeU3Ec__Iterator0_Reset_m2195541101_MetadataUsageId;
extern "C"  void U3CRunFadeU3Ec__Iterator0_Reset_m2195541101 (U3CRunFadeU3Ec__Iterator0_t4246392297 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CRunFadeU3Ec__Iterator0_Reset_m2195541101_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Rotate::.ctor()
extern "C"  void Rotate__ctor_m1487335446 (Rotate_t4255939431 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Rotate::Update()
extern "C"  void Rotate_Update_m4217433871 (Rotate_t4255939431 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = __this->get_m_RotationSpeed_2();
		float L_2 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_Rotate_m1743927093(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RunForwardandBack::.ctor()
extern "C"  void RunForwardandBack__ctor_m4113362341 (RunForwardandBack_t3788415244 * __this, const MethodInfo* method)
{
	{
		__this->set_speed_2((3.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RunForwardandBack::Start()
extern "C"  void RunForwardandBack_Start_m1280865789 (RunForwardandBack_t3788415244 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2638739322(&L_0, (0.0f), (-90.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_right_3(L_0);
		Vector3_t2243707580  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m2638739322(&L_1, (0.0f), (90.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_left_4(L_1);
		return;
	}
}
// System.Void RunForwardandBack::Update()
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral696030502;
extern Il2CppCodeGenString* _stringLiteral783931732;
extern Il2CppCodeGenString* _stringLiteral696030967;
extern const uint32_t RunForwardandBack_Update_m3659136634_MetadataUsageId;
extern "C"  void RunForwardandBack_Update_m3659136634 (RunForwardandBack_t3788415244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RunForwardandBack_Update_m3659136634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Quaternion_t4030073918  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Quaternion_t4030073918  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Quaternion_t4030073918  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Quaternion_t4030073918  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Quaternion_t4030073918  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t2243707580  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Vector3_t2243707580  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Quaternion_t4030073918  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector3_t2243707580  V_19;
	memset(&V_19, 0, sizeof(V_19));
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t4030073918  L_1 = Transform_get_rotation_m1033555130(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t2243707580  L_2 = Quaternion_get_eulerAngles_m3302573991((&V_0), /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = (&V_1)->get_y_2();
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		String_t* L_6 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, _stringLiteral696030502, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0105;
		}
	}
	{
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = Transform_get_position_m1104419803(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = (&V_2)->get_x_1();
		if ((!(((float)L_10) < ((float)(-6.0f)))))
		{
			goto IL_00a1;
		}
	}
	{
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Quaternion_t4030073918  L_12 = Transform_get_rotation_m1033555130(L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		Vector3_t2243707580  L_13 = Quaternion_get_eulerAngles_m3302573991((&V_3), /*hidden argument*/NULL);
		V_4 = L_13;
		float L_14 = (&V_4)->get_y_2();
		if ((!(((float)L_14) == ((float)(270.0f)))))
		{
			goto IL_00a1;
		}
	}
	{
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m2638739322(&L_16, (0.0f), (90.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_eulerAngles_m2881310872(L_15, L_16, /*hidden argument*/NULL);
		goto IL_0105;
	}

IL_00a1:
	{
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t2243707580  L_18 = Transform_get_position_m1104419803(L_17, /*hidden argument*/NULL);
		V_5 = L_18;
		float L_19 = (&V_5)->get_x_1();
		if ((!(((float)L_19) > ((float)(5.0f)))))
		{
			goto IL_0105;
		}
	}
	{
		Transform_t3275118058 * L_20 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Quaternion_t4030073918  L_21 = Transform_get_rotation_m1033555130(L_20, /*hidden argument*/NULL);
		V_6 = L_21;
		Vector3_t2243707580  L_22 = Quaternion_get_eulerAngles_m3302573991((&V_6), /*hidden argument*/NULL);
		V_7 = L_22;
		float L_23 = (&V_7)->get_y_2();
		if ((!(((float)L_23) >= ((float)(90.0f)))))
		{
			goto IL_0105;
		}
	}
	{
		Transform_t3275118058 * L_24 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector3__ctor_m2638739322(&L_25, (0.0f), (-90.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_eulerAngles_m2881310872(L_24, L_25, /*hidden argument*/NULL);
	}

IL_0105:
	{
		String_t* L_26 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_27 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_26, _stringLiteral783931732, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_01ec;
		}
	}
	{
		Transform_t3275118058 * L_28 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t2243707580  L_29 = Transform_get_position_m1104419803(L_28, /*hidden argument*/NULL);
		V_8 = L_29;
		float L_30 = (&V_8)->get_x_1();
		if ((!(((float)L_30) < ((float)(-3.0f)))))
		{
			goto IL_0183;
		}
	}
	{
		Transform_t3275118058 * L_31 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		Quaternion_t4030073918  L_32 = Transform_get_rotation_m1033555130(L_31, /*hidden argument*/NULL);
		V_9 = L_32;
		Vector3_t2243707580  L_33 = Quaternion_get_eulerAngles_m3302573991((&V_9), /*hidden argument*/NULL);
		V_10 = L_33;
		float L_34 = (&V_10)->get_y_2();
		if ((!(((float)L_34) == ((float)(270.0f)))))
		{
			goto IL_0183;
		}
	}
	{
		Transform_t3275118058 * L_35 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Vector3__ctor_m2638739322(&L_36, (0.0f), (90.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_set_eulerAngles_m2881310872(L_35, L_36, /*hidden argument*/NULL);
		goto IL_01ec;
	}

IL_0183:
	{
		Transform_t3275118058 * L_37 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		Vector3_t2243707580  L_38 = Transform_get_position_m1104419803(L_37, /*hidden argument*/NULL);
		V_11 = L_38;
		float L_39 = (&V_11)->get_x_1();
		if ((!(((double)(((double)((double)L_39)))) > ((double)(1.7)))))
		{
			goto IL_01ec;
		}
	}
	{
		Transform_t3275118058 * L_40 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		Quaternion_t4030073918  L_41 = Transform_get_rotation_m1033555130(L_40, /*hidden argument*/NULL);
		V_12 = L_41;
		Vector3_t2243707580  L_42 = Quaternion_get_eulerAngles_m3302573991((&V_12), /*hidden argument*/NULL);
		V_13 = L_42;
		float L_43 = (&V_13)->get_y_2();
		if ((!(((float)L_43) >= ((float)(90.0f)))))
		{
			goto IL_01ec;
		}
	}
	{
		Transform_t3275118058 * L_44 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Vector3__ctor_m2638739322(&L_45, (0.0f), (-90.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_44);
		Transform_set_eulerAngles_m2881310872(L_44, L_45, /*hidden argument*/NULL);
	}

IL_01ec:
	{
		String_t* L_46 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_47 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_46, _stringLiteral696030967, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_02ce;
		}
	}
	{
		Transform_t3275118058 * L_48 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_48);
		Vector3_t2243707580  L_49 = Transform_get_position_m1104419803(L_48, /*hidden argument*/NULL);
		V_14 = L_49;
		float L_50 = (&V_14)->get_x_1();
		if ((!(((float)L_50) < ((float)(-3.0f)))))
		{
			goto IL_026a;
		}
	}
	{
		Transform_t3275118058 * L_51 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_51);
		Quaternion_t4030073918  L_52 = Transform_get_rotation_m1033555130(L_51, /*hidden argument*/NULL);
		V_15 = L_52;
		Vector3_t2243707580  L_53 = Quaternion_get_eulerAngles_m3302573991((&V_15), /*hidden argument*/NULL);
		V_16 = L_53;
		float L_54 = (&V_16)->get_y_2();
		if ((!(((float)L_54) == ((float)(270.0f)))))
		{
			goto IL_026a;
		}
	}
	{
		Transform_t3275118058 * L_55 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_56;
		memset(&L_56, 0, sizeof(L_56));
		Vector3__ctor_m2638739322(&L_56, (0.0f), (90.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_55);
		Transform_set_eulerAngles_m2881310872(L_55, L_56, /*hidden argument*/NULL);
		goto IL_02ce;
	}

IL_026a:
	{
		Transform_t3275118058 * L_57 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_57);
		Vector3_t2243707580  L_58 = Transform_get_position_m1104419803(L_57, /*hidden argument*/NULL);
		V_17 = L_58;
		float L_59 = (&V_17)->get_x_1();
		if ((!(((float)L_59) > ((float)(3.0f)))))
		{
			goto IL_02ce;
		}
	}
	{
		Transform_t3275118058 * L_60 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_60);
		Quaternion_t4030073918  L_61 = Transform_get_rotation_m1033555130(L_60, /*hidden argument*/NULL);
		V_18 = L_61;
		Vector3_t2243707580  L_62 = Quaternion_get_eulerAngles_m3302573991((&V_18), /*hidden argument*/NULL);
		V_19 = L_62;
		float L_63 = (&V_19)->get_y_2();
		if ((!(((float)L_63) >= ((float)(90.0f)))))
		{
			goto IL_02ce;
		}
	}
	{
		Transform_t3275118058 * L_64 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_65;
		memset(&L_65, 0, sizeof(L_65));
		Vector3__ctor_m2638739322(&L_65, (0.0f), (-90.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_64);
		Transform_set_eulerAngles_m2881310872(L_64, L_65, /*hidden argument*/NULL);
	}

IL_02ce:
	{
		Transform_t3275118058 * L_66 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_67 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_68 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_69 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_67, L_68, /*hidden argument*/NULL);
		float L_70 = __this->get_speed_2();
		Vector3_t2243707580  L_71 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_69, L_70, /*hidden argument*/NULL);
		NullCheck(L_66);
		Transform_Translate_m3316827744(L_66, L_71, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleCharacterControler2D::.ctor()
extern "C"  void SimpleCharacterControler2D__ctor_m2437712998 (SimpleCharacterControler2D_t2053125211 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleCharacterControler2D::Awake()
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const uint32_t SimpleCharacterControler2D_Awake_m3067464971_MetadataUsageId;
extern "C"  void SimpleCharacterControler2D_Awake_m3067464971 (SimpleCharacterControler2D_t2053125211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleCharacterControler2D_Awake_m3067464971_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t502193897 * L_0 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		__this->set_m_RigidBody_2(L_0);
		Rigidbody2D_t502193897 * L_1 = __this->get_m_RigidBody_2();
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, (5.0f), (5.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		Rigidbody2D_set_velocity_m3592751374(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleCharacterControler2D::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t SimpleCharacterControler2D_Update_m2157894855_MetadataUsageId;
extern "C"  void SimpleCharacterControler2D_Update_m2157894855 (SimpleCharacterControler2D_t2053125211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SimpleCharacterControler2D_Update_m2157894855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_m_RigidBody_2();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = Rigidbody2D_get_velocity_m3310151195(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_0();
		float L_3 = __this->get_m_MaxVelocityToBoost_4();
		if ((!(((float)L_2) <= ((float)L_3))))
		{
			goto IL_0049;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)275), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0049;
		}
	}
	{
		Rigidbody2D_t502193897 * L_5 = __this->get_m_RigidBody_2();
		float L_6 = __this->get_m_Force_3();
		Vector2_t2243707579  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector2__ctor_m3067419446(&L_7, L_6, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		Rigidbody2D_AddForce_m4245830473(L_5, L_7, 0, /*hidden argument*/NULL);
	}

IL_0049:
	{
		Rigidbody2D_t502193897 * L_8 = __this->get_m_RigidBody_2();
		NullCheck(L_8);
		Vector2_t2243707579  L_9 = Rigidbody2D_get_velocity_m3310151195(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		float L_10 = (&V_1)->get_x_0();
		float L_11 = __this->get_m_MaxVelocityToBoost_4();
		if ((!(((float)L_10) >= ((float)((-L_11))))))
		{
			goto IL_0094;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_12 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)276), /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0094;
		}
	}
	{
		Rigidbody2D_t502193897 * L_13 = __this->get_m_RigidBody_2();
		float L_14 = __this->get_m_Force_3();
		Vector2_t2243707579  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector2__ctor_m3067419446(&L_15, ((-L_14)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		Rigidbody2D_AddForce_m4245830473(L_13, L_15, 0, /*hidden argument*/NULL);
	}

IL_0094:
	{
		Rigidbody2D_t502193897 * L_16 = __this->get_m_RigidBody_2();
		NullCheck(L_16);
		Vector2_t2243707579  L_17 = Rigidbody2D_get_velocity_m3310151195(L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		float L_18 = (&V_2)->get_y_1();
		float L_19 = __this->get_m_MaxVelocityToBoost_4();
		if ((!(((float)L_18) <= ((float)L_19))))
		{
			goto IL_00dd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_20 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)273), /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00dd;
		}
	}
	{
		Rigidbody2D_t502193897 * L_21 = __this->get_m_RigidBody_2();
		float L_22 = __this->get_m_Force_3();
		Vector2_t2243707579  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector2__ctor_m3067419446(&L_23, (0.0f), L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		Rigidbody2D_AddForce_m4245830473(L_21, L_23, 0, /*hidden argument*/NULL);
	}

IL_00dd:
	{
		Rigidbody2D_t502193897 * L_24 = __this->get_m_RigidBody_2();
		NullCheck(L_24);
		Vector2_t2243707579  L_25 = Rigidbody2D_get_velocity_m3310151195(L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		float L_26 = (&V_3)->get_y_1();
		float L_27 = __this->get_m_MaxVelocityToBoost_4();
		if ((!(((float)L_26) >= ((float)((-L_27))))))
		{
			goto IL_0128;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_28 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)274), /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_0128;
		}
	}
	{
		Rigidbody2D_t502193897 * L_29 = __this->get_m_RigidBody_2();
		float L_30 = __this->get_m_Force_3();
		Vector2_t2243707579  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector2__ctor_m3067419446(&L_31, (0.0f), ((-L_30)), /*hidden argument*/NULL);
		NullCheck(L_29);
		Rigidbody2D_AddForce_m4245830473(L_29, L_31, 0, /*hidden argument*/NULL);
	}

IL_0128:
	{
		return;
	}
}
// System.Void SmoothMouseLook::.ctor()
extern Il2CppClass* List_1_t1445631064_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1509370154_MethodInfo_var;
extern const uint32_t SmoothMouseLook__ctor_m2602337615_MetadataUsageId;
extern "C"  void SmoothMouseLook__ctor_m2602337615 (SmoothMouseLook_t128928814 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothMouseLook__ctor_m2602337615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_SensitivityX_3((15.0f));
		__this->set_SensitivityY_4((15.0f));
		__this->set_MinimumX_5((-360.0f));
		__this->set_MaximumX_6((360.0f));
		__this->set_MinimumY_7((-60.0f));
		__this->set_MaximumY_8((60.0f));
		__this->set_FrameCounter_9((20.0f));
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)il2cpp_codegen_object_new(List_1_t1445631064_il2cpp_TypeInfo_var);
		List_1__ctor_m1509370154(L_0, /*hidden argument*/List_1__ctor_m1509370154_MethodInfo_var);
		__this->set__rotArrayX_12(L_0);
		List_1_t1445631064 * L_1 = (List_1_t1445631064 *)il2cpp_codegen_object_new(List_1_t1445631064_il2cpp_TypeInfo_var);
		List_1__ctor_m1509370154(L_1, /*hidden argument*/List_1__ctor_m1509370154_MethodInfo_var);
		__this->set__rotArrayY_13(L_1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmoothMouseLook::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m913687102_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3406166720_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m3361987988_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4068660677_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m251960377_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3485606345_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3373976255_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3645101712;
extern Il2CppCodeGenString* _stringLiteral1307534078;
extern Il2CppCodeGenString* _stringLiteral1307534077;
extern const uint32_t SmoothMouseLook_Update_m121977692_MetadataUsageId;
extern "C"  void SmoothMouseLook_Update_m121977692 (SmoothMouseLook_t128928814 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothMouseLook_Update_m121977692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	Enumerator_t980360738  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Enumerator_t980360738  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Quaternion_t4030073918  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Quaternion_t4030073918  V_6;
	memset(&V_6, 0, sizeof(V_6));
	float V_7 = 0.0f;
	Enumerator_t980360738  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Quaternion_t4030073918  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float V_10 = 0.0f;
	Enumerator_t980360738  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Quaternion_t4030073918  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetButton_m38251721(NULL /*static, unused*/, _stringLiteral3645101712, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		int32_t L_1 = __this->get_axes_2();
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0257;
		}
	}
	{
		goto IL_0367;
	}

IL_0029:
	{
		__this->set__rotAverageY_15((0.0f));
		__this->set__rotAverageX_14((0.0f));
		float L_4 = __this->get__rotationY_11();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_5 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534078, /*hidden argument*/NULL);
		float L_6 = __this->get_SensitivityY_4();
		__this->set__rotationY_11(((float)((float)L_4+(float)((float)((float)L_5*(float)L_6)))));
		float L_7 = __this->get__rotationY_11();
		float L_8 = __this->get_MinimumY_7();
		float L_9 = __this->get_MaximumY_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_10 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
		__this->set__rotationY_11(L_10);
		float L_11 = __this->get__rotationX_10();
		float L_12 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534077, /*hidden argument*/NULL);
		float L_13 = __this->get_SensitivityX_3();
		__this->set__rotationX_10(((float)((float)L_11+(float)((float)((float)L_12*(float)L_13)))));
		List_1_t1445631064 * L_14 = __this->get__rotArrayY_13();
		float L_15 = __this->get__rotationY_11();
		NullCheck(L_14);
		List_1_Add_m913687102(L_14, L_15, /*hidden argument*/List_1_Add_m913687102_MethodInfo_var);
		List_1_t1445631064 * L_16 = __this->get__rotArrayX_12();
		float L_17 = __this->get__rotationX_10();
		NullCheck(L_16);
		List_1_Add_m913687102(L_16, L_17, /*hidden argument*/List_1_Add_m913687102_MethodInfo_var);
		List_1_t1445631064 * L_18 = __this->get__rotArrayY_13();
		NullCheck(L_18);
		int32_t L_19 = List_1_get_Count_m3406166720(L_18, /*hidden argument*/List_1_get_Count_m3406166720_MethodInfo_var);
		float L_20 = __this->get_FrameCounter_9();
		if ((!(((float)(((float)((float)L_19)))) >= ((float)L_20))))
		{
			goto IL_00dd;
		}
	}
	{
		List_1_t1445631064 * L_21 = __this->get__rotArrayY_13();
		NullCheck(L_21);
		List_1_RemoveAt_m3361987988(L_21, 0, /*hidden argument*/List_1_RemoveAt_m3361987988_MethodInfo_var);
	}

IL_00dd:
	{
		List_1_t1445631064 * L_22 = __this->get__rotArrayX_12();
		NullCheck(L_22);
		int32_t L_23 = List_1_get_Count_m3406166720(L_22, /*hidden argument*/List_1_get_Count_m3406166720_MethodInfo_var);
		float L_24 = __this->get_FrameCounter_9();
		if ((!(((float)(((float)((float)L_23)))) >= ((float)L_24))))
		{
			goto IL_0100;
		}
	}
	{
		List_1_t1445631064 * L_25 = __this->get__rotArrayX_12();
		NullCheck(L_25);
		List_1_RemoveAt_m3361987988(L_25, 0, /*hidden argument*/List_1_RemoveAt_m3361987988_MethodInfo_var);
	}

IL_0100:
	{
		List_1_t1445631064 * L_26 = __this->get__rotArrayY_13();
		NullCheck(L_26);
		Enumerator_t980360738  L_27 = List_1_GetEnumerator_m4068660677(L_26, /*hidden argument*/List_1_GetEnumerator_m4068660677_MethodInfo_var);
		V_2 = L_27;
	}

IL_010c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0127;
		}

IL_0111:
		{
			float L_28 = Enumerator_get_Current_m251960377((&V_2), /*hidden argument*/Enumerator_get_Current_m251960377_MethodInfo_var);
			V_1 = L_28;
			float L_29 = __this->get__rotAverageY_15();
			float L_30 = V_1;
			__this->set__rotAverageY_15(((float)((float)L_29+(float)L_30)));
		}

IL_0127:
		{
			bool L_31 = Enumerator_MoveNext_m3485606345((&V_2), /*hidden argument*/Enumerator_MoveNext_m3485606345_MethodInfo_var);
			if (L_31)
			{
				goto IL_0111;
			}
		}

IL_0133:
		{
			IL2CPP_LEAVE(0x146, FINALLY_0138);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0138;
	}

FINALLY_0138:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3373976255((&V_2), /*hidden argument*/Enumerator_Dispose_m3373976255_MethodInfo_var);
		IL2CPP_END_FINALLY(312)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(312)
	{
		IL2CPP_JUMP_TBL(0x146, IL_0146)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0146:
	{
		List_1_t1445631064 * L_32 = __this->get__rotArrayX_12();
		NullCheck(L_32);
		Enumerator_t980360738  L_33 = List_1_GetEnumerator_m4068660677(L_32, /*hidden argument*/List_1_GetEnumerator_m4068660677_MethodInfo_var);
		V_4 = L_33;
	}

IL_0153:
	try
	{ // begin try (depth: 1)
		{
			goto IL_016e;
		}

IL_0158:
		{
			float L_34 = Enumerator_get_Current_m251960377((&V_4), /*hidden argument*/Enumerator_get_Current_m251960377_MethodInfo_var);
			V_3 = L_34;
			float L_35 = __this->get__rotAverageX_14();
			float L_36 = V_3;
			__this->set__rotAverageX_14(((float)((float)L_35+(float)L_36)));
		}

IL_016e:
		{
			bool L_37 = Enumerator_MoveNext_m3485606345((&V_4), /*hidden argument*/Enumerator_MoveNext_m3485606345_MethodInfo_var);
			if (L_37)
			{
				goto IL_0158;
			}
		}

IL_017a:
		{
			IL2CPP_LEAVE(0x18D, FINALLY_017f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_017f;
	}

FINALLY_017f:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3373976255((&V_4), /*hidden argument*/Enumerator_Dispose_m3373976255_MethodInfo_var);
		IL2CPP_END_FINALLY(383)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(383)
	{
		IL2CPP_JUMP_TBL(0x18D, IL_018d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_018d:
	{
		float L_38 = __this->get__rotAverageY_15();
		List_1_t1445631064 * L_39 = __this->get__rotArrayY_13();
		NullCheck(L_39);
		int32_t L_40 = List_1_get_Count_m3406166720(L_39, /*hidden argument*/List_1_get_Count_m3406166720_MethodInfo_var);
		__this->set__rotAverageY_15(((float)((float)L_38/(float)(((float)((float)L_40))))));
		float L_41 = __this->get__rotAverageX_14();
		List_1_t1445631064 * L_42 = __this->get__rotArrayX_12();
		NullCheck(L_42);
		int32_t L_43 = List_1_get_Count_m3406166720(L_42, /*hidden argument*/List_1_get_Count_m3406166720_MethodInfo_var);
		__this->set__rotAverageX_14(((float)((float)L_41/(float)(((float)((float)L_43))))));
		float L_44 = __this->get__rotAverageY_15();
		float L_45 = __this->get_MinimumY_7();
		float L_46 = __this->get_MaximumY_8();
		float L_47 = SmoothMouseLook_ClampAngle_m476947378(NULL /*static, unused*/, L_44, L_45, L_46, /*hidden argument*/NULL);
		__this->set__rotAverageY_15(L_47);
		float L_48 = __this->get__rotAverageX_14();
		float L_49 = __this->get_MinimumX_5();
		float L_50 = __this->get_MaximumX_6();
		float L_51 = SmoothMouseLook_ClampAngle_m476947378(NULL /*static, unused*/, L_48, L_49, L_50, /*hidden argument*/NULL);
		__this->set__rotAverageX_14(L_51);
		float L_52 = __this->get__rotAverageY_15();
		Vector3_t2243707580  L_53 = Vector3_get_left_m2429378123(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_54 = Quaternion_AngleAxis_m2806222563(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		V_5 = L_54;
		float L_55 = __this->get__rotAverageX_14();
		Vector3_t2243707580  L_56 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_57 = Quaternion_AngleAxis_m2806222563(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
		V_6 = L_57;
		Transform_t3275118058 * L_58 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_59 = __this->get__originalRotation_16();
		Quaternion_t4030073918  L_60 = V_5;
		Quaternion_t4030073918  L_61 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_59, L_60, /*hidden argument*/NULL);
		NullCheck(L_58);
		Transform_set_localRotation_m2055111962(L_58, L_61, /*hidden argument*/NULL);
		Transform_t3275118058 * L_62 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_62);
		Transform_t3275118058 * L_63 = Transform_get_parent_m147407266(L_62, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_64 = __this->get__parentOriginalRotation_17();
		Quaternion_t4030073918  L_65 = V_6;
		Quaternion_t4030073918  L_66 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_64, L_65, /*hidden argument*/NULL);
		NullCheck(L_63);
		Transform_set_localRotation_m2055111962(L_63, L_66, /*hidden argument*/NULL);
		goto IL_0472;
	}

IL_0257:
	{
		__this->set__rotAverageX_14((0.0f));
		float L_67 = __this->get__rotationX_10();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_68 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534077, /*hidden argument*/NULL);
		float L_69 = __this->get_SensitivityX_3();
		__this->set__rotationX_10(((float)((float)L_67+(float)((float)((float)L_68*(float)L_69)))));
		List_1_t1445631064 * L_70 = __this->get__rotArrayX_12();
		float L_71 = __this->get__rotationX_10();
		NullCheck(L_70);
		List_1_Add_m913687102(L_70, L_71, /*hidden argument*/List_1_Add_m913687102_MethodInfo_var);
		List_1_t1445631064 * L_72 = __this->get__rotArrayX_12();
		NullCheck(L_72);
		int32_t L_73 = List_1_get_Count_m3406166720(L_72, /*hidden argument*/List_1_get_Count_m3406166720_MethodInfo_var);
		float L_74 = __this->get_FrameCounter_9();
		if ((!(((float)(((float)((float)L_73)))) >= ((float)L_74))))
		{
			goto IL_02b4;
		}
	}
	{
		List_1_t1445631064 * L_75 = __this->get__rotArrayX_12();
		NullCheck(L_75);
		List_1_RemoveAt_m3361987988(L_75, 0, /*hidden argument*/List_1_RemoveAt_m3361987988_MethodInfo_var);
	}

IL_02b4:
	{
		List_1_t1445631064 * L_76 = __this->get__rotArrayX_12();
		NullCheck(L_76);
		Enumerator_t980360738  L_77 = List_1_GetEnumerator_m4068660677(L_76, /*hidden argument*/List_1_GetEnumerator_m4068660677_MethodInfo_var);
		V_8 = L_77;
	}

IL_02c1:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02de;
		}

IL_02c6:
		{
			float L_78 = Enumerator_get_Current_m251960377((&V_8), /*hidden argument*/Enumerator_get_Current_m251960377_MethodInfo_var);
			V_7 = L_78;
			float L_79 = __this->get__rotAverageX_14();
			float L_80 = V_7;
			__this->set__rotAverageX_14(((float)((float)L_79+(float)L_80)));
		}

IL_02de:
		{
			bool L_81 = Enumerator_MoveNext_m3485606345((&V_8), /*hidden argument*/Enumerator_MoveNext_m3485606345_MethodInfo_var);
			if (L_81)
			{
				goto IL_02c6;
			}
		}

IL_02ea:
		{
			IL2CPP_LEAVE(0x2FD, FINALLY_02ef);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_02ef;
	}

FINALLY_02ef:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3373976255((&V_8), /*hidden argument*/Enumerator_Dispose_m3373976255_MethodInfo_var);
		IL2CPP_END_FINALLY(751)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(751)
	{
		IL2CPP_JUMP_TBL(0x2FD, IL_02fd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_02fd:
	{
		float L_82 = __this->get__rotAverageX_14();
		List_1_t1445631064 * L_83 = __this->get__rotArrayX_12();
		NullCheck(L_83);
		int32_t L_84 = List_1_get_Count_m3406166720(L_83, /*hidden argument*/List_1_get_Count_m3406166720_MethodInfo_var);
		__this->set__rotAverageX_14(((float)((float)L_82/(float)(((float)((float)L_84))))));
		float L_85 = __this->get__rotAverageX_14();
		float L_86 = __this->get_MinimumX_5();
		float L_87 = __this->get_MaximumX_6();
		float L_88 = SmoothMouseLook_ClampAngle_m476947378(NULL /*static, unused*/, L_85, L_86, L_87, /*hidden argument*/NULL);
		__this->set__rotAverageX_14(L_88);
		float L_89 = __this->get__rotAverageX_14();
		Vector3_t2243707580  L_90 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_91 = Quaternion_AngleAxis_m2806222563(NULL /*static, unused*/, L_89, L_90, /*hidden argument*/NULL);
		V_9 = L_91;
		Transform_t3275118058 * L_92 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_92);
		Transform_t3275118058 * L_93 = Transform_get_parent_m147407266(L_92, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_94 = __this->get__parentOriginalRotation_17();
		Quaternion_t4030073918  L_95 = V_9;
		Quaternion_t4030073918  L_96 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_94, L_95, /*hidden argument*/NULL);
		NullCheck(L_93);
		Transform_set_localRotation_m2055111962(L_93, L_96, /*hidden argument*/NULL);
		goto IL_0472;
	}

IL_0367:
	{
		__this->set__rotAverageY_15((0.0f));
		float L_97 = __this->get__rotationY_11();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_98 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534078, /*hidden argument*/NULL);
		float L_99 = __this->get_SensitivityY_4();
		__this->set__rotationY_11(((float)((float)L_97+(float)((float)((float)L_98*(float)L_99)))));
		List_1_t1445631064 * L_100 = __this->get__rotArrayY_13();
		float L_101 = __this->get__rotationY_11();
		NullCheck(L_100);
		List_1_Add_m913687102(L_100, L_101, /*hidden argument*/List_1_Add_m913687102_MethodInfo_var);
		List_1_t1445631064 * L_102 = __this->get__rotArrayY_13();
		NullCheck(L_102);
		int32_t L_103 = List_1_get_Count_m3406166720(L_102, /*hidden argument*/List_1_get_Count_m3406166720_MethodInfo_var);
		float L_104 = __this->get_FrameCounter_9();
		if ((!(((float)(((float)((float)L_103)))) >= ((float)L_104))))
		{
			goto IL_03c4;
		}
	}
	{
		List_1_t1445631064 * L_105 = __this->get__rotArrayY_13();
		NullCheck(L_105);
		List_1_RemoveAt_m3361987988(L_105, 0, /*hidden argument*/List_1_RemoveAt_m3361987988_MethodInfo_var);
	}

IL_03c4:
	{
		List_1_t1445631064 * L_106 = __this->get__rotArrayY_13();
		NullCheck(L_106);
		Enumerator_t980360738  L_107 = List_1_GetEnumerator_m4068660677(L_106, /*hidden argument*/List_1_GetEnumerator_m4068660677_MethodInfo_var);
		V_11 = L_107;
	}

IL_03d1:
	try
	{ // begin try (depth: 1)
		{
			goto IL_03ee;
		}

IL_03d6:
		{
			float L_108 = Enumerator_get_Current_m251960377((&V_11), /*hidden argument*/Enumerator_get_Current_m251960377_MethodInfo_var);
			V_10 = L_108;
			float L_109 = __this->get__rotAverageY_15();
			float L_110 = V_10;
			__this->set__rotAverageY_15(((float)((float)L_109+(float)L_110)));
		}

IL_03ee:
		{
			bool L_111 = Enumerator_MoveNext_m3485606345((&V_11), /*hidden argument*/Enumerator_MoveNext_m3485606345_MethodInfo_var);
			if (L_111)
			{
				goto IL_03d6;
			}
		}

IL_03fa:
		{
			IL2CPP_LEAVE(0x40D, FINALLY_03ff);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_03ff;
	}

FINALLY_03ff:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3373976255((&V_11), /*hidden argument*/Enumerator_Dispose_m3373976255_MethodInfo_var);
		IL2CPP_END_FINALLY(1023)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1023)
	{
		IL2CPP_JUMP_TBL(0x40D, IL_040d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_040d:
	{
		float L_112 = __this->get__rotAverageY_15();
		List_1_t1445631064 * L_113 = __this->get__rotArrayY_13();
		NullCheck(L_113);
		int32_t L_114 = List_1_get_Count_m3406166720(L_113, /*hidden argument*/List_1_get_Count_m3406166720_MethodInfo_var);
		__this->set__rotAverageY_15(((float)((float)L_112/(float)(((float)((float)L_114))))));
		float L_115 = __this->get__rotAverageY_15();
		float L_116 = __this->get_MinimumY_7();
		float L_117 = __this->get_MaximumY_8();
		float L_118 = SmoothMouseLook_ClampAngle_m476947378(NULL /*static, unused*/, L_115, L_116, L_117, /*hidden argument*/NULL);
		__this->set__rotAverageY_15(L_118);
		float L_119 = __this->get__rotAverageY_15();
		Vector3_t2243707580  L_120 = Vector3_get_left_m2429378123(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_121 = Quaternion_AngleAxis_m2806222563(NULL /*static, unused*/, L_119, L_120, /*hidden argument*/NULL);
		V_12 = L_121;
		Transform_t3275118058 * L_122 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_123 = __this->get__originalRotation_16();
		Quaternion_t4030073918  L_124 = V_12;
		Quaternion_t4030073918  L_125 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_123, L_124, /*hidden argument*/NULL);
		NullCheck(L_122);
		Transform_set_localRotation_m2055111962(L_122, L_125, /*hidden argument*/NULL);
		goto IL_0472;
	}

IL_0472:
	{
		return;
	}
}
// System.Void SmoothMouseLook::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t4233889191_m520013213_MethodInfo_var;
extern const uint32_t SmoothMouseLook_Start_m774693555_MetadataUsageId;
extern "C"  void SmoothMouseLook_Start_m774693555 (SmoothMouseLook_t128928814 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothMouseLook_Start_m774693555_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody_t4233889191 * V_0 = NULL;
	{
		Rigidbody_t4233889191 * L_0 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_MethodInfo_var);
		V_0 = L_0;
		Rigidbody_t4233889191 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		Rigidbody_t4233889191 * L_3 = V_0;
		NullCheck(L_3);
		Rigidbody_set_freezeRotation_m2131864169(L_3, (bool)1, /*hidden argument*/NULL);
	}

IL_0019:
	{
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Quaternion_t4030073918  L_5 = Transform_get_localRotation_m4001487205(L_4, /*hidden argument*/NULL);
		__this->set__originalRotation_16(L_5);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = Transform_get_parent_m147407266(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Quaternion_t4030073918  L_8 = Transform_get_localRotation_m4001487205(L_7, /*hidden argument*/NULL);
		__this->set__parentOriginalRotation_17(L_8);
		return;
	}
}
// System.Single SmoothMouseLook::ClampAngle(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t SmoothMouseLook_ClampAngle_m476947378_MetadataUsageId;
extern "C"  float SmoothMouseLook_ClampAngle_m476947378 (Il2CppObject * __this /* static, unused */, float ___angle0, float ___min1, float ___max2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothMouseLook_ClampAngle_m476947378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___angle0;
		___angle0 = (fmodf(L_0, (360.0f)));
		float L_1 = ___angle0;
		if ((!(((float)L_1) >= ((float)(-360.0f)))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = ___angle0;
		if ((!(((float)L_2) <= ((float)(360.0f)))))
		{
			goto IL_0047;
		}
	}
	{
		float L_3 = ___angle0;
		if ((!(((float)L_3) < ((float)(-360.0f)))))
		{
			goto IL_0033;
		}
	}
	{
		float L_4 = ___angle0;
		___angle0 = ((float)((float)L_4+(float)(360.0f)));
	}

IL_0033:
	{
		float L_5 = ___angle0;
		if ((!(((float)L_5) > ((float)(360.0f)))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = ___angle0;
		___angle0 = ((float)((float)L_6-(float)(360.0f)));
	}

IL_0047:
	{
		float L_7 = ___angle0;
		float L_8 = ___min1;
		float L_9 = ___max2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_10 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void SpaceDetection::.ctor()
extern "C"  void SpaceDetection__ctor_m3473158040 (SpaceDetection_t1772284253 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpaceDetection::Start()
extern "C"  void SpaceDetection_Start_m329268168 (SpaceDetection_t1772284253 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SpaceDetection::Update()
extern "C"  void SpaceDetection_Update_m3454572105 (SpaceDetection_t1772284253 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SpaceDetection::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1502599448;
extern Il2CppCodeGenString* _stringLiteral1563601862;
extern Il2CppCodeGenString* _stringLiteral696030502;
extern Il2CppCodeGenString* _stringLiteral2275519968;
extern Il2CppCodeGenString* _stringLiteral1146369312;
extern Il2CppCodeGenString* _stringLiteral4267164206;
extern Il2CppCodeGenString* _stringLiteral696030967;
extern Il2CppCodeGenString* _stringLiteral2276027343;
extern Il2CppCodeGenString* _stringLiteral783931732;
extern Il2CppCodeGenString* _stringLiteral3711976978;
extern const uint32_t SpaceDetection_OnTriggerEnter_m3888486468_MetadataUsageId;
extern "C"  void SpaceDetection_OnTriggerEnter_m3888486468 (SpaceDetection_t1772284253 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpaceDetection_OnTriggerEnter_m3888486468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t3497673348 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m2079638459(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral1502599448, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_3 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, _stringLiteral1563601862, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		GameObject_t1756533147 * L_5 = __this->get_CC_2();
		NullCheck(L_5);
		CharacterControl_t3164190352 * L_6 = GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381(L_5, /*hidden argument*/GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381_MethodInfo_var);
		NullCheck(L_6);
		L_6->set_dogSpace_32((bool)1);
	}

IL_003b:
	{
		Collider_t3497673348 * L_7 = ___other0;
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m2079638459(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral696030502, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0076;
		}
	}
	{
		String_t* L_10 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_10, _stringLiteral2275519968, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0076;
		}
	}
	{
		GameObject_t1756533147 * L_12 = __this->get_CC_2();
		NullCheck(L_12);
		CharacterControl_t3164190352 * L_13 = GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381(L_12, /*hidden argument*/GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381_MethodInfo_var);
		NullCheck(L_13);
		L_13->set_catSpace_33((bool)1);
	}

IL_0076:
	{
		Collider_t3497673348 * L_14 = ___other0;
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m2079638459(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_15, _stringLiteral1146369312, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00b1;
		}
	}
	{
		String_t* L_17 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_17, _stringLiteral4267164206, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00b1;
		}
	}
	{
		GameObject_t1756533147 * L_19 = __this->get_CC_2();
		NullCheck(L_19);
		CharacterControl_t3164190352 * L_20 = GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381(L_19, /*hidden argument*/GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381_MethodInfo_var);
		NullCheck(L_20);
		L_20->set_owlSpace_34((bool)1);
	}

IL_00b1:
	{
		Collider_t3497673348 * L_21 = ___other0;
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m2079638459(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_22, _stringLiteral696030967, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00ec;
		}
	}
	{
		String_t* L_24 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_24, _stringLiteral2276027343, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00ec;
		}
	}
	{
		GameObject_t1756533147 * L_26 = __this->get_CC_2();
		NullCheck(L_26);
		CharacterControl_t3164190352 * L_27 = GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381(L_26, /*hidden argument*/GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381_MethodInfo_var);
		NullCheck(L_27);
		L_27->set_ratSpace_35((bool)1);
	}

IL_00ec:
	{
		Collider_t3497673348 * L_28 = ___other0;
		NullCheck(L_28);
		String_t* L_29 = Object_get_name_m2079638459(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_30 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_29, _stringLiteral783931732, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0127;
		}
	}
	{
		String_t* L_31 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_32 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_31, _stringLiteral3711976978, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0127;
		}
	}
	{
		GameObject_t1756533147 * L_33 = __this->get_CC_2();
		NullCheck(L_33);
		CharacterControl_t3164190352 * L_34 = GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381(L_33, /*hidden argument*/GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381_MethodInfo_var);
		NullCheck(L_34);
		L_34->set_turtleSpace_36((bool)1);
	}

IL_0127:
	{
		return;
	}
}
// System.Void SpaceDetection::OnTriggerExit(UnityEngine.Collider)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1502599448;
extern Il2CppCodeGenString* _stringLiteral1563601862;
extern Il2CppCodeGenString* _stringLiteral696030502;
extern Il2CppCodeGenString* _stringLiteral2275519968;
extern Il2CppCodeGenString* _stringLiteral1146369312;
extern Il2CppCodeGenString* _stringLiteral4267164206;
extern Il2CppCodeGenString* _stringLiteral696030967;
extern Il2CppCodeGenString* _stringLiteral2276027343;
extern Il2CppCodeGenString* _stringLiteral783931732;
extern Il2CppCodeGenString* _stringLiteral3711976978;
extern const uint32_t SpaceDetection_OnTriggerExit_m3278577982_MetadataUsageId;
extern "C"  void SpaceDetection_OnTriggerExit_m3278577982 (SpaceDetection_t1772284253 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpaceDetection_OnTriggerExit_m3278577982_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t3497673348 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m2079638459(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral1502599448, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_3 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, _stringLiteral1563601862, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		GameObject_t1756533147 * L_5 = __this->get_CC_2();
		NullCheck(L_5);
		CharacterControl_t3164190352 * L_6 = GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381(L_5, /*hidden argument*/GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381_MethodInfo_var);
		NullCheck(L_6);
		L_6->set_dogSpace_32((bool)0);
	}

IL_003b:
	{
		Collider_t3497673348 * L_7 = ___other0;
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m2079638459(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral696030502, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0076;
		}
	}
	{
		String_t* L_10 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_10, _stringLiteral2275519968, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0076;
		}
	}
	{
		GameObject_t1756533147 * L_12 = __this->get_CC_2();
		NullCheck(L_12);
		CharacterControl_t3164190352 * L_13 = GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381(L_12, /*hidden argument*/GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381_MethodInfo_var);
		NullCheck(L_13);
		L_13->set_catSpace_33((bool)0);
	}

IL_0076:
	{
		Collider_t3497673348 * L_14 = ___other0;
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m2079638459(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_15, _stringLiteral1146369312, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00b1;
		}
	}
	{
		String_t* L_17 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_17, _stringLiteral4267164206, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00b1;
		}
	}
	{
		GameObject_t1756533147 * L_19 = __this->get_CC_2();
		NullCheck(L_19);
		CharacterControl_t3164190352 * L_20 = GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381(L_19, /*hidden argument*/GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381_MethodInfo_var);
		NullCheck(L_20);
		L_20->set_owlSpace_34((bool)0);
	}

IL_00b1:
	{
		Collider_t3497673348 * L_21 = ___other0;
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m2079638459(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_22, _stringLiteral696030967, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00ec;
		}
	}
	{
		String_t* L_24 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_24, _stringLiteral2276027343, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00ec;
		}
	}
	{
		GameObject_t1756533147 * L_26 = __this->get_CC_2();
		NullCheck(L_26);
		CharacterControl_t3164190352 * L_27 = GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381(L_26, /*hidden argument*/GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381_MethodInfo_var);
		NullCheck(L_27);
		L_27->set_ratSpace_35((bool)0);
	}

IL_00ec:
	{
		Collider_t3497673348 * L_28 = ___other0;
		NullCheck(L_28);
		String_t* L_29 = Object_get_name_m2079638459(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_30 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_29, _stringLiteral783931732, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0127;
		}
	}
	{
		String_t* L_31 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_32 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_31, _stringLiteral3711976978, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0127;
		}
	}
	{
		GameObject_t1756533147 * L_33 = __this->get_CC_2();
		NullCheck(L_33);
		CharacterControl_t3164190352 * L_34 = GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381(L_33, /*hidden argument*/GameObject_GetComponent_TisCharacterControl_t3164190352_m1629004381_MethodInfo_var);
		NullCheck(L_34);
		L_34->set_turtleSpace_36((bool)0);
	}

IL_0127:
	{
		return;
	}
}
// System.Void SpriteTrail::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Queue_1_t1505052203_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1__ctor_m2885645460_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3566263623;
extern const uint32_t SpriteTrail__ctor_m588023566_MetadataUsageId;
extern "C"  void SpriteTrail__ctor_m588023566 (SpriteTrail_t2284883325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteTrail__ctor_m588023566_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_m_TrailName_2(L_0);
		__this->set_m_LayerName_6(_stringLiteral3566263623);
		__this->set_m_ZMoveStep_7((0.0001f));
		__this->set_m_ZMoveMax_8((0.9999f));
		__this->set_m_HideTrailOnDisabled_10((bool)1);
		Queue_1_t1505052203 * L_1 = (Queue_1_t1505052203 *)il2cpp_codegen_object_new(Queue_1_t1505052203_il2cpp_TypeInfo_var);
		Queue_1__ctor_m2885645460(L_1, /*hidden argument*/Queue_1__ctor_m2885645460_MethodInfo_var);
		__this->set_m_ElementsInTrail_20(L_1);
		__this->set_m_CanBeAutomaticallyActivated_36((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpriteTrail::SetTrailParent(UnityEngine.Transform)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3489514782;
extern Il2CppCodeGenString* _stringLiteral3053306054;
extern const uint32_t SpriteTrail_SetTrailParent_m3799627059_MetadataUsageId;
extern "C"  void SpriteTrail_SetTrailParent_m3799627059 (SpriteTrail_t2284883325 * __this, Transform_t3275118058 * ___trailParent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteTrail_SetTrailParent_m3799627059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Transform_t3275118058 * V_1 = NULL;
	{
		Transform_t3275118058 * L_0 = ___trailParent0;
		__this->set_m_TrailParent_4(L_0);
		Transform_t3275118058 * L_1 = __this->get_m_TrailParent_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_006b;
		}
	}
	{
		GameObject_t1756533147 * L_3 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3489514782, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t1756533147 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0055;
		}
	}
	{
		GameObject_t1756533147 * L_6 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_6, _stringLiteral3489514782, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		__this->set_m_GlobalTrailContainer_25(L_7);
		Transform_t3275118058 * L_8 = __this->get_m_GlobalTrailContainer_25();
		NullCheck(L_8);
		Object_set_hideFlags_m2204253440(L_8, 1, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_0055:
	{
		GameObject_t1756533147 * L_9 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3489514782, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
		__this->set_m_GlobalTrailContainer_25(L_10);
	}

IL_006a:
	{
		return;
	}

IL_006b:
	{
		Transform_t3275118058 * L_11 = ___trailParent0;
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = Transform_FindChild_m2677714886(L_11, _stringLiteral3053306054, /*hidden argument*/NULL);
		V_1 = L_12;
		Transform_t3275118058 * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_13, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00c0;
		}
	}
	{
		GameObject_t1756533147 * L_15 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_15, _stringLiteral3053306054, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = GameObject_get_transform_m909382139(L_15, /*hidden argument*/NULL);
		__this->set_m_LocalTrailContainer_24(L_16);
		Transform_t3275118058 * L_17 = __this->get_m_LocalTrailContainer_24();
		NullCheck(L_17);
		Object_set_hideFlags_m2204253440(L_17, 1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_18 = __this->get_m_LocalTrailContainer_24();
		NullCheck(L_18);
		Transform_t3275118058 * L_19 = Component_get_transform_m2697483695(L_18, /*hidden argument*/NULL);
		Transform_t3275118058 * L_20 = __this->get_m_TrailParent_4();
		NullCheck(L_19);
		Transform_SetParent_m1963830867(L_19, L_20, (bool)0, /*hidden argument*/NULL);
		goto IL_00c7;
	}

IL_00c0:
	{
		Transform_t3275118058 * L_21 = V_1;
		__this->set_m_LocalTrailContainer_24(L_21);
	}

IL_00c7:
	{
		return;
	}
}
// System.Void SpriteTrail::SetTrailPreset(TrailPreset)
extern "C"  void SpriteTrail_SetTrailPreset_m277816234 (SpriteTrail_t2284883325 * __this, TrailPreset_t465077881 * ___preset0, const MethodInfo* method)
{
	{
		TrailPreset_t465077881 * L_0 = __this->get_m_CurrentTrailPreset_3();
		__this->set_m_PreviousTrailPreset_22(L_0);
		TrailPreset_t465077881 * L_1 = ___preset0;
		__this->set_m_CurrentTrailPreset_3(L_1);
		return;
	}
}
// System.Void SpriteTrail::EnableTrail()
extern "C"  void SpriteTrail_EnableTrail_m3387428939 (SpriteTrail_t2284883325 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeInHierarchy_m4242915935(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		__this->set_m_CanBeAutomaticallyActivated_36((bool)1);
		SpriteRenderer_t1209076198 * L_2 = __this->get_m_SpriteToDuplicate_5();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_position_m1104419803(L_3, /*hidden argument*/NULL);
		Vector2_t2243707579  L_5 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_m_PreviousPosSpawned_30(L_5);
		int32_t L_6 = __this->get_m_TrailActivationCondition_11();
		V_0 = L_6;
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_8 = V_0;
		if (!L_8)
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) == ((int32_t)2)))
		{
			goto IL_0064;
		}
	}
	{
		goto IL_0069;
	}

IL_0053:
	{
		SpriteTrail_EnableTrailEffect_m1251733603(__this, (bool)1, /*hidden argument*/NULL);
		goto IL_0069;
	}

IL_005f:
	{
		goto IL_0069;
	}

IL_0064:
	{
		goto IL_0069;
	}

IL_0069:
	{
		return;
	}
}
// System.Void SpriteTrail::DisableTrail()
extern "C"  void SpriteTrail_DisableTrail_m2137670676 (SpriteTrail_t2284883325 * __this, const MethodInfo* method)
{
	{
		__this->set_m_WillBeActivatedOnEnable_38((bool)0);
		__this->set_m_CanBeAutomaticallyActivated_36((bool)0);
		SpriteTrail_DisableTrailEffect_m4082338629(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpriteTrail::EnableTrailEffect(System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t SpriteTrail_EnableTrailEffect_m1251733603_MetadataUsageId;
extern "C"  void SpriteTrail_EnableTrailEffect_m1251733603 (SpriteTrail_t2284883325 * __this, bool ___forceTrailCreation0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteTrail_EnableTrailEffect_m1251733603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpriteRenderer_t1209076198 * L_0 = __this->get_m_SpriteToDuplicate_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		TrailPreset_t465077881 * L_2 = __this->get_m_CurrentTrailPreset_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		return;
	}

IL_0024:
	{
		float L_4 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_TimeTrailStarted_27(L_4);
		__this->set_m_EffectEnabled_37((bool)1);
		float L_5 = __this->get_m_ZMoveMax_8();
		__this->set_m_CurrentDisplacement_26(L_5);
		SpriteRenderer_t1209076198 * L_6 = __this->get_m_SpriteToDuplicate_5();
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_position_m1104419803(L_7, /*hidden argument*/NULL);
		Vector2_t2243707579  L_9 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->set_m_PreviousPosSpawned_30(L_9);
		float L_10 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_PreviousTimeSpawned_28(L_10);
		int32_t L_11 = Time_get_frameCount_m1198768813(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_PreviousFrameSpawned_29(L_11);
		bool L_12 = ___forceTrailCreation0;
		if (!L_12)
		{
			goto IL_007f;
		}
	}
	{
		SpriteTrail_GenerateNewTrailElement_m447001791(__this, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void SpriteTrail::DisableTrailEffect()
extern "C"  void SpriteTrail_DisableTrailEffect_m4082338629 (SpriteTrail_t2284883325 * __this, const MethodInfo* method)
{
	{
		__this->set_m_EffectEnabled_37((bool)0);
		bool L_0 = __this->get_m_HideTrailOnDisabled_10();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		SpriteTrail_HideTrail_m2673586674(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void SpriteTrail::HideTrail()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m2757116194_MethodInfo_var;
extern const MethodInfo* Queue_1_get_Count_m353297782_MethodInfo_var;
extern const uint32_t SpriteTrail_HideTrail_m2673586674_MetadataUsageId;
extern "C"  void SpriteTrail_HideTrail_m2673586674 (SpriteTrail_t2284883325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteTrail_HideTrail_m2673586674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TrailElement_t1685395368 * V_0 = NULL;
	{
		goto IL_0024;
	}

IL_0005:
	{
		Queue_1_t1505052203 * L_0 = __this->get_m_ElementsInTrail_20();
		NullCheck(L_0);
		TrailElement_t1685395368 * L_1 = Queue_1_Dequeue_m2757116194(L_0, /*hidden argument*/Queue_1_Dequeue_m2757116194_MethodInfo_var);
		V_0 = L_1;
		TrailElement_t1685395368 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		TrailElement_t1685395368 * L_4 = V_0;
		NullCheck(L_4);
		TrailElement_Hide_m3076722378(L_4, (bool)1, /*hidden argument*/NULL);
	}

IL_0024:
	{
		Queue_1_t1505052203 * L_5 = __this->get_m_ElementsInTrail_20();
		NullCheck(L_5);
		int32_t L_6 = Queue_1_get_Count_m353297782(L_5, /*hidden argument*/Queue_1_get_Count_m353297782_MethodInfo_var);
		if ((((int32_t)L_6) > ((int32_t)0)))
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean SpriteTrail::IsEffectEnabled()
extern "C"  bool SpriteTrail_IsEffectEnabled_m236050918 (SpriteTrail_t2284883325 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_EffectEnabled_37();
		return L_0;
	}
}
// System.Void SpriteTrail::OnLevelWasLoaded()
extern Il2CppClass* TrailElement_t1685395368_il2cpp_TypeInfo_var;
extern Il2CppClass* SpriteTrail_t2284883325_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t SpriteTrail_OnLevelWasLoaded_m4154238041_MetadataUsageId;
extern "C"  void SpriteTrail_OnLevelWasLoaded_m4154238041 (SpriteTrail_t2284883325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteTrail_OnLevelWasLoaded_m4154238041_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrailElement_t1685395368_il2cpp_TypeInfo_var);
		TrailElement_ClearFreeElements_m65079503(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SpriteTrail_t2284883325_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_0 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->set_m_TrailElementPrefab_23((GameObject_t1756533147 *)NULL);
		return;
	}
}
// System.Void SpriteTrail::Awake()
extern Il2CppClass* SpriteTrail_t2284883325_il2cpp_TypeInfo_var;
extern Il2CppClass* TrailElement_t1685395368_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisSpriteRenderer_t1209076198_m3314035363_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisTrailElement_t1685395368_m2634836246_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTrailElement_t1685395368_m898602943_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral331242091;
extern Il2CppCodeGenString* _stringLiteral403805392;
extern const uint32_t SpriteTrail_Awake_m543085597_MetadataUsageId;
extern "C"  void SpriteTrail_Awake_m543085597 (SpriteTrail_t2284883325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteTrail_Awake_m543085597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteTrail_t2284883325_il2cpp_TypeInfo_var);
		int32_t L_0 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailCount_21();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrailElement_t1685395368_il2cpp_TypeInfo_var);
		TrailElement_ClearFreeElements_m65079503(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SpriteTrail_t2284883325_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_1 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->set_m_TrailElementPrefab_23((GameObject_t1756533147 *)NULL);
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteTrail_t2284883325_il2cpp_TypeInfo_var);
		int32_t L_2 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailCount_21();
		((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->set_m_TrailCount_21(((int32_t)((int32_t)L_2+(int32_t)1)));
		Transform_t3275118058 * L_3 = __this->get_m_TrailParent_4();
		SpriteTrail_SetTrailParent_m3799627059(__this, L_3, /*hidden argument*/NULL);
		float L_4 = __this->get_m_ZMoveMax_8();
		__this->set_m_CurrentDisplacement_26(L_4);
		GameObject_t1756533147 * L_5 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral331242091, /*hidden argument*/NULL);
		((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->set_m_TrailElementPrefab_23(L_5);
		GameObject_t1756533147 * L_6 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00ee;
		}
	}
	{
		GameObject_t1756533147 * L_8 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m498247354(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SpriteTrail_t2284883325_il2cpp_TypeInfo_var);
		((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->set_m_TrailElementPrefab_23(L_8);
		GameObject_t1756533147 * L_9 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		NullCheck(L_9);
		GameObject_AddComponent_TisSpriteRenderer_t1209076198_m3314035363(L_9, /*hidden argument*/GameObject_AddComponent_TisSpriteRenderer_t1209076198_m3314035363_MethodInfo_var);
		GameObject_t1756533147 * L_10 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		NullCheck(L_10);
		GameObject_AddComponent_TisTrailElement_t1685395368_m2634836246(L_10, /*hidden argument*/GameObject_AddComponent_TisTrailElement_t1685395368_m2634836246_MethodInfo_var);
		GameObject_t1756533147 * L_11 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		NullCheck(L_11);
		Object_set_name_m4157836998(L_11, _stringLiteral331242091, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		NullCheck(L_12);
		Object_set_hideFlags_m2204253440(L_12, 1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_13 = __this->get_m_TrailParent_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_13, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteTrail_t2284883325_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_15 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = GameObject_get_transform_m909382139(L_15, /*hidden argument*/NULL);
		Transform_t3275118058 * L_17 = __this->get_m_GlobalTrailContainer_25();
		NullCheck(L_16);
		Transform_SetParent_m1963830867(L_16, L_17, (bool)1, /*hidden argument*/NULL);
		goto IL_00de;
	}

IL_00c8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteTrail_t2284883325_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_18 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		NullCheck(L_18);
		Transform_t3275118058 * L_19 = GameObject_get_transform_m909382139(L_18, /*hidden argument*/NULL);
		Transform_t3275118058 * L_20 = __this->get_m_LocalTrailContainer_24();
		NullCheck(L_19);
		Transform_SetParent_m1963830867(L_19, L_20, (bool)1, /*hidden argument*/NULL);
	}

IL_00de:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteTrail_t2284883325_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_21 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		NullCheck(L_21);
		TrailElement_t1685395368 * L_22 = GameObject_GetComponent_TisTrailElement_t1685395368_m898602943(L_21, /*hidden argument*/GameObject_GetComponent_TisTrailElement_t1685395368_m898602943_MethodInfo_var);
		NullCheck(L_22);
		TrailElement_Hide_m3076722378(L_22, (bool)0, /*hidden argument*/NULL);
	}

IL_00ee:
	{
		SpriteRenderer_t1209076198 * L_23 = __this->get_m_SpriteToDuplicate_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_24 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_23, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_010b;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_25 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		__this->set_m_SpriteToDuplicate_5(L_25);
	}

IL_010b:
	{
		SpriteRenderer_t1209076198 * L_26 = __this->get_m_SpriteToDuplicate_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_27 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_26, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0127;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral403805392, /*hidden argument*/NULL);
		return;
	}

IL_0127:
	{
		SpriteRenderer_t1209076198 * L_28 = __this->get_m_SpriteToDuplicate_5();
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t2243707580  L_30 = Transform_get_position_m1104419803(L_29, /*hidden argument*/NULL);
		Vector2_t2243707579  L_31 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		__this->set_m_PreviousPosSpawned_30(L_31);
		return;
	}
}
// System.Void SpriteTrail::CalculateCurrentVelocity()
extern "C"  void SpriteTrail_CalculateCurrentVelocity_m2497564602 (SpriteTrail_t2284883325 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_FirstVelocityCheck_35();
		if (!L_0)
		{
			goto IL_006b;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_1 = __this->get_m_SpriteToDuplicate_5();
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_localPosition_m2533925116(L_2, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Vector2_t2243707579  L_5 = __this->get_m_PreviousFrameLocalPos_31();
		Vector2_t2243707579  L_6 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		float L_7 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = Vector2_op_Division_m96580069(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		__this->set_m_VelocityLocal_33(L_8);
		SpriteRenderer_t1209076198 * L_9 = __this->get_m_SpriteToDuplicate_5();
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_position_m1104419803(L_10, /*hidden argument*/NULL);
		Vector2_t2243707579  L_12 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Vector2_t2243707579  L_13 = __this->get_m_PreviousFrameWorldPos_32();
		Vector2_t2243707579  L_14 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		float L_15 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = Vector2_op_Division_m96580069(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		__this->set_m_VelocityWorld_34(L_16);
	}

IL_006b:
	{
		__this->set_m_FirstVelocityCheck_35((bool)1);
		SpriteRenderer_t1209076198 * L_17 = __this->get_m_SpriteToDuplicate_5();
		NullCheck(L_17);
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = Transform_get_localPosition_m2533925116(L_18, /*hidden argument*/NULL);
		Vector2_t2243707579  L_20 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		__this->set_m_PreviousFrameLocalPos_31(L_20);
		SpriteRenderer_t1209076198 * L_21 = __this->get_m_SpriteToDuplicate_5();
		NullCheck(L_21);
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t2243707580  L_23 = Transform_get_position_m1104419803(L_22, /*hidden argument*/NULL);
		Vector2_t2243707579  L_24 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		__this->set_m_PreviousFrameWorldPos_32(L_24);
		return;
	}
}
// System.Void SpriteTrail::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t SpriteTrail_Update_m2249259625_MetadataUsageId;
extern "C"  void SpriteTrail_Update_m2249259625 (SpriteTrail_t2284883325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteTrail_Update_m2249259625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2243707579  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	int32_t V_7 = 0;
	float V_8 = 0.0f;
	{
		TrailPreset_t465077881 * L_0 = __this->get_m_PreviousTrailPreset_22();
		TrailPreset_t465077881 * L_1 = __this->get_m_CurrentTrailPreset_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		bool L_3 = __this->get_m_HideTrailOnDisabled_10();
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		SpriteTrail_HideTrail_m2673586674(__this, /*hidden argument*/NULL);
	}

IL_0027:
	{
		TrailPreset_t465077881 * L_4 = __this->get_m_CurrentTrailPreset_3();
		__this->set_m_PreviousTrailPreset_22(L_4);
	}

IL_0033:
	{
		int32_t L_5 = __this->get_m_TrailActivationCondition_11();
		if ((((int32_t)L_5) == ((int32_t)2)))
		{
			goto IL_004b;
		}
	}
	{
		int32_t L_6 = __this->get_m_TrailDisactivationCondition_12();
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_0051;
		}
	}

IL_004b:
	{
		SpriteTrail_CalculateCurrentVelocity_m2497564602(__this, /*hidden argument*/NULL);
	}

IL_0051:
	{
		bool L_7 = __this->get_m_EffectEnabled_37();
		if (!L_7)
		{
			goto IL_0368;
		}
	}
	{
		TrailPreset_t465077881 * L_8 = __this->get_m_CurrentTrailPreset_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_8, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_006e;
		}
	}
	{
		return;
	}

IL_006e:
	{
		SpriteRenderer_t1209076198 * L_10 = __this->get_m_SpriteToDuplicate_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0080;
		}
	}
	{
		return;
	}

IL_0080:
	{
		int32_t L_12 = __this->get_m_TrailDisactivationCondition_12();
		V_0 = L_12;
		int32_t L_13 = V_0;
		if (!L_13)
		{
			goto IL_00a0;
		}
	}
	{
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) == ((int32_t)2)))
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_15 = V_0;
		if ((((int32_t)L_15) == ((int32_t)1)))
		{
			goto IL_00c8;
		}
	}
	{
		goto IL_013a;
	}

IL_00a0:
	{
		goto IL_013a;
	}

IL_00a5:
	{
		float L_16 = __this->get_m_TimeTrailStarted_27();
		float L_17 = __this->get_m_TrailActivationDuration_19();
		float L_18 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_16+(float)L_17))) <= ((float)L_18))))
		{
			goto IL_00c3;
		}
	}
	{
		SpriteTrail_DisableTrailEffect_m4082338629(__this, /*hidden argument*/NULL);
		return;
	}

IL_00c3:
	{
		goto IL_013a;
	}

IL_00c8:
	{
		V_1 = (0.0f);
		bool L_19 = __this->get_m_VelocityStopIsLocalSpace_17();
		if (!L_19)
		{
			goto IL_00ea;
		}
	}
	{
		Vector2_t2243707579 * L_20 = __this->get_address_of_m_VelocityLocal_33();
		float L_21 = Vector2_get_magnitude_m33802565(L_20, /*hidden argument*/NULL);
		V_1 = L_21;
		goto IL_00f6;
	}

IL_00ea:
	{
		Vector2_t2243707579 * L_22 = __this->get_address_of_m_VelocityWorld_34();
		float L_23 = Vector2_get_magnitude_m33802565(L_22, /*hidden argument*/NULL);
		V_1 = L_23;
	}

IL_00f6:
	{
		bool L_24 = __this->get_m_StopIfOverVelocity_16();
		if (L_24)
		{
			goto IL_0118;
		}
	}
	{
		float L_25 = V_1;
		float L_26 = __this->get_m_VelocityNeededToStop_18();
		if ((!(((float)L_25) >= ((float)L_26))))
		{
			goto IL_0118;
		}
	}
	{
		SpriteTrail_DisableTrailEffect_m4082338629(__this, /*hidden argument*/NULL);
		goto IL_0135;
	}

IL_0118:
	{
		bool L_27 = __this->get_m_StopIfOverVelocity_16();
		if (!L_27)
		{
			goto IL_0135;
		}
	}
	{
		float L_28 = V_1;
		float L_29 = __this->get_m_VelocityNeededToStop_18();
		if ((!(((float)L_28) <= ((float)L_29))))
		{
			goto IL_0135;
		}
	}
	{
		SpriteTrail_DisableTrailEffect_m4082338629(__this, /*hidden argument*/NULL);
	}

IL_0135:
	{
		goto IL_013a;
	}

IL_013a:
	{
		TrailPreset_t465077881 * L_30 = __this->get_m_CurrentTrailPreset_3();
		NullCheck(L_30);
		int32_t L_31 = L_30->get_m_TrailElementSpawnCondition_8();
		V_2 = L_31;
		int32_t L_32 = V_2;
		if (!L_32)
		{
			goto IL_015f;
		}
	}
	{
		int32_t L_33 = V_2;
		if ((((int32_t)L_33) == ((int32_t)1)))
		{
			goto IL_01fc;
		}
	}
	{
		int32_t L_34 = V_2;
		if ((((int32_t)L_34) == ((int32_t)2)))
		{
			goto IL_022e;
		}
	}
	{
		goto IL_0363;
	}

IL_015f:
	{
		float L_35 = __this->get_m_PreviousTimeSpawned_28();
		TrailPreset_t465077881 * L_36 = __this->get_m_CurrentTrailPreset_3();
		NullCheck(L_36);
		float L_37 = L_36->get_m_TimeBetweenSpawns_9();
		float L_38 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_35+(float)L_37))) <= ((float)L_38))))
		{
			goto IL_01f7;
		}
	}
	{
		float L_39 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_40 = __this->get_m_PreviousTimeSpawned_28();
		TrailPreset_t465077881 * L_41 = __this->get_m_CurrentTrailPreset_3();
		NullCheck(L_41);
		float L_42 = L_41->get_m_TimeBetweenSpawns_9();
		V_3 = ((float)((float)L_39-(float)((float)((float)L_40+(float)L_42))));
		TrailPreset_t465077881 * L_43 = __this->get_m_CurrentTrailPreset_3();
		NullCheck(L_43);
		float L_44 = L_43->get_m_TimeBetweenSpawns_9();
		if ((!(((float)L_44) > ((float)(0.0f)))))
		{
			goto IL_01cd;
		}
	}
	{
		goto IL_01bc;
	}

IL_01ae:
	{
		float L_45 = V_3;
		TrailPreset_t465077881 * L_46 = __this->get_m_CurrentTrailPreset_3();
		NullCheck(L_46);
		float L_47 = L_46->get_m_TimeBetweenSpawns_9();
		V_3 = ((float)((float)L_45-(float)L_47));
	}

IL_01bc:
	{
		float L_48 = V_3;
		TrailPreset_t465077881 * L_49 = __this->get_m_CurrentTrailPreset_3();
		NullCheck(L_49);
		float L_50 = L_49->get_m_TimeBetweenSpawns_9();
		if ((((float)L_48) >= ((float)L_50)))
		{
			goto IL_01ae;
		}
	}

IL_01cd:
	{
		float L_51 = V_3;
		TrailPreset_t465077881 * L_52 = __this->get_m_CurrentTrailPreset_3();
		NullCheck(L_52);
		float L_53 = L_52->get_m_TimeBetweenSpawns_9();
		if ((!(((float)L_51) > ((float)L_53))))
		{
			goto IL_01e4;
		}
	}
	{
		V_3 = (0.0f);
	}

IL_01e4:
	{
		float L_54 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_55 = V_3;
		__this->set_m_PreviousTimeSpawned_28(((float)((float)L_54-(float)L_55)));
		SpriteTrail_GenerateNewTrailElement_m447001791(__this, /*hidden argument*/NULL);
	}

IL_01f7:
	{
		goto IL_0363;
	}

IL_01fc:
	{
		int32_t L_56 = __this->get_m_PreviousFrameSpawned_29();
		TrailPreset_t465077881 * L_57 = __this->get_m_CurrentTrailPreset_3();
		NullCheck(L_57);
		int32_t L_58 = L_57->get_m_FramesBetweenSpawns_10();
		int32_t L_59 = Time_get_frameCount_m1198768813(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_56+(int32_t)L_58))) > ((int32_t)L_59)))
		{
			goto IL_0229;
		}
	}
	{
		int32_t L_60 = Time_get_frameCount_m1198768813(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_PreviousFrameSpawned_29(L_60);
		SpriteTrail_GenerateNewTrailElement_m447001791(__this, /*hidden argument*/NULL);
	}

IL_0229:
	{
		goto IL_0363;
	}

IL_022e:
	{
		TrailPreset_t465077881 * L_61 = __this->get_m_CurrentTrailPreset_3();
		NullCheck(L_61);
		bool L_62 = L_61->get_m_DistanceCorrection_12();
		if (!L_62)
		{
			goto IL_030d;
		}
	}
	{
		TrailPreset_t465077881 * L_63 = __this->get_m_CurrentTrailPreset_3();
		NullCheck(L_63);
		float L_64 = L_63->get_m_DistanceBetweenSpawns_11();
		if ((!(((float)L_64) > ((float)(0.0f)))))
		{
			goto IL_030d;
		}
	}
	{
		goto IL_02d8;
	}

IL_0258:
	{
		SpriteRenderer_t1209076198 * L_65 = __this->get_m_SpriteToDuplicate_5();
		NullCheck(L_65);
		Transform_t3275118058 * L_66 = Component_get_transform_m2697483695(L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		Vector3_t2243707580  L_67 = Transform_get_position_m1104419803(L_66, /*hidden argument*/NULL);
		Vector2_t2243707579  L_68 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		Vector2_t2243707579  L_69 = __this->get_m_PreviousPosSpawned_30();
		Vector2_t2243707579  L_70 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_68, L_69, /*hidden argument*/NULL);
		V_4 = L_70;
		Vector2_t2243707579  L_71 = __this->get_m_PreviousPosSpawned_30();
		Vector2_t2243707579  L_72 = Vector2_get_normalized_m2985402409((&V_4), /*hidden argument*/NULL);
		TrailPreset_t465077881 * L_73 = __this->get_m_CurrentTrailPreset_3();
		NullCheck(L_73);
		float L_74 = L_73->get_m_DistanceBetweenSpawns_11();
		Vector2_t2243707579  L_75 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_72, L_74, /*hidden argument*/NULL);
		Vector2_t2243707579  L_76 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_71, L_75, /*hidden argument*/NULL);
		V_5 = L_76;
		float L_77 = (&V_5)->get_x_0();
		float L_78 = (&V_5)->get_y_1();
		SpriteRenderer_t1209076198 * L_79 = __this->get_m_SpriteToDuplicate_5();
		NullCheck(L_79);
		Transform_t3275118058 * L_80 = Component_get_transform_m2697483695(L_79, /*hidden argument*/NULL);
		NullCheck(L_80);
		Vector3_t2243707580  L_81 = Transform_get_position_m1104419803(L_80, /*hidden argument*/NULL);
		V_6 = L_81;
		float L_82 = (&V_6)->get_z_3();
		Vector3_t2243707580  L_83;
		memset(&L_83, 0, sizeof(L_83));
		Vector3__ctor_m2638739322(&L_83, L_77, L_78, L_82, /*hidden argument*/NULL);
		SpriteTrail_GenerateNewTrailElement_m2721125526(__this, L_83, /*hidden argument*/NULL);
		Vector2_t2243707579  L_84 = V_5;
		__this->set_m_PreviousPosSpawned_30(L_84);
	}

IL_02d8:
	{
		Vector2_t2243707579  L_85 = __this->get_m_PreviousPosSpawned_30();
		SpriteRenderer_t1209076198 * L_86 = __this->get_m_SpriteToDuplicate_5();
		NullCheck(L_86);
		Transform_t3275118058 * L_87 = Component_get_transform_m2697483695(L_86, /*hidden argument*/NULL);
		NullCheck(L_87);
		Vector3_t2243707580  L_88 = Transform_get_position_m1104419803(L_87, /*hidden argument*/NULL);
		Vector2_t2243707579  L_89 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_88, /*hidden argument*/NULL);
		float L_90 = Vector2_Distance_m280750759(NULL /*static, unused*/, L_85, L_89, /*hidden argument*/NULL);
		TrailPreset_t465077881 * L_91 = __this->get_m_CurrentTrailPreset_3();
		NullCheck(L_91);
		float L_92 = L_91->get_m_DistanceBetweenSpawns_11();
		if ((((float)L_90) >= ((float)L_92)))
		{
			goto IL_0258;
		}
	}
	{
		goto IL_035e;
	}

IL_030d:
	{
		Vector2_t2243707579  L_93 = __this->get_m_PreviousPosSpawned_30();
		SpriteRenderer_t1209076198 * L_94 = __this->get_m_SpriteToDuplicate_5();
		NullCheck(L_94);
		Transform_t3275118058 * L_95 = Component_get_transform_m2697483695(L_94, /*hidden argument*/NULL);
		NullCheck(L_95);
		Vector3_t2243707580  L_96 = Transform_get_position_m1104419803(L_95, /*hidden argument*/NULL);
		Vector2_t2243707579  L_97 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_96, /*hidden argument*/NULL);
		float L_98 = Vector2_Distance_m280750759(NULL /*static, unused*/, L_93, L_97, /*hidden argument*/NULL);
		TrailPreset_t465077881 * L_99 = __this->get_m_CurrentTrailPreset_3();
		NullCheck(L_99);
		float L_100 = L_99->get_m_DistanceBetweenSpawns_11();
		if ((!(((float)L_98) >= ((float)L_100))))
		{
			goto IL_035e;
		}
	}
	{
		SpriteTrail_GenerateNewTrailElement_m447001791(__this, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_101 = __this->get_m_SpriteToDuplicate_5();
		NullCheck(L_101);
		Transform_t3275118058 * L_102 = Component_get_transform_m2697483695(L_101, /*hidden argument*/NULL);
		NullCheck(L_102);
		Vector3_t2243707580  L_103 = Transform_get_position_m1104419803(L_102, /*hidden argument*/NULL);
		Vector2_t2243707579  L_104 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_103, /*hidden argument*/NULL);
		__this->set_m_PreviousPosSpawned_30(L_104);
	}

IL_035e:
	{
		goto IL_0363;
	}

IL_0363:
	{
		goto IL_0421;
	}

IL_0368:
	{
		bool L_105 = __this->get_m_CanBeAutomaticallyActivated_36();
		if (!L_105)
		{
			goto IL_0421;
		}
	}
	{
		int32_t L_106 = __this->get_m_TrailActivationCondition_11();
		V_7 = L_106;
		int32_t L_107 = V_7;
		if (!L_107)
		{
			goto IL_0397;
		}
	}
	{
		int32_t L_108 = V_7;
		if ((((int32_t)L_108) == ((int32_t)1)))
		{
			goto IL_03a3;
		}
	}
	{
		int32_t L_109 = V_7;
		if ((((int32_t)L_109) == ((int32_t)2)))
		{
			goto IL_03a8;
		}
	}
	{
		goto IL_0421;
	}

IL_0397:
	{
		SpriteTrail_EnableTrailEffect_m1251733603(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_0421;
	}

IL_03a3:
	{
		goto IL_0421;
	}

IL_03a8:
	{
		V_8 = (0.0f);
		bool L_110 = __this->get_m_VelocityStopIsLocalSpace_17();
		if (!L_110)
		{
			goto IL_03cc;
		}
	}
	{
		Vector2_t2243707579 * L_111 = __this->get_address_of_m_VelocityLocal_33();
		float L_112 = Vector2_get_magnitude_m33802565(L_111, /*hidden argument*/NULL);
		V_8 = L_112;
		goto IL_03d9;
	}

IL_03cc:
	{
		Vector2_t2243707579 * L_113 = __this->get_address_of_m_VelocityWorld_34();
		float L_114 = Vector2_get_magnitude_m33802565(L_113, /*hidden argument*/NULL);
		V_8 = L_114;
	}

IL_03d9:
	{
		bool L_115 = __this->get_m_StartIfUnderVelocity_13();
		if (L_115)
		{
			goto IL_03fd;
		}
	}
	{
		float L_116 = V_8;
		float L_117 = __this->get_m_VelocityNeededToStart_15();
		if ((!(((float)L_116) >= ((float)L_117))))
		{
			goto IL_03fd;
		}
	}
	{
		SpriteTrail_EnableTrailEffect_m1251733603(__this, (bool)1, /*hidden argument*/NULL);
		goto IL_041c;
	}

IL_03fd:
	{
		bool L_118 = __this->get_m_StartIfUnderVelocity_13();
		if (!L_118)
		{
			goto IL_041c;
		}
	}
	{
		float L_119 = V_8;
		float L_120 = __this->get_m_VelocityNeededToStart_15();
		if ((!(((float)L_119) <= ((float)L_120))))
		{
			goto IL_041c;
		}
	}
	{
		SpriteTrail_EnableTrailEffect_m1251733603(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_041c:
	{
		goto IL_0421;
	}

IL_0421:
	{
		return;
	}
}
// System.Void SpriteTrail::GenerateNewTrailElement(UnityEngine.Vector3)
extern Il2CppClass* TrailElement_t1685395368_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTrailElement_t1685395368_m898602943_MethodInfo_var;
extern const uint32_t SpriteTrail_GenerateNewTrailElement_m2721125526_MetadataUsageId;
extern "C"  void SpriteTrail_GenerateNewTrailElement_m2721125526 (SpriteTrail_t2284883325 * __this, Vector3_t2243707580  ___pos0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteTrail_GenerateNewTrailElement_m2721125526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	TrailElement_t1685395368 * V_1 = NULL;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		float L_0 = __this->get_m_CurrentDisplacement_26();
		float L_1 = __this->get_m_ZMoveStep_7();
		__this->set_m_CurrentDisplacement_26(((float)((float)L_0-(float)L_1)));
		float L_2 = __this->get_m_CurrentDisplacement_26();
		float L_3 = __this->get_m_ZMoveStep_7();
		if ((!(((float)L_2) <= ((float)L_3))))
		{
			goto IL_0030;
		}
	}
	{
		float L_4 = __this->get_m_ZMoveMax_8();
		__this->set_m_CurrentDisplacement_26(L_4);
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrailElement_t1685395368_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_5 = TrailElement_GetFreeElement_m700256560(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_5;
		GameObject_t1756533147 * L_6 = V_0;
		NullCheck(L_6);
		TrailElement_t1685395368 * L_7 = GameObject_GetComponent_TisTrailElement_t1685395368_m898602943(L_6, /*hidden argument*/GameObject_GetComponent_TisTrailElement_t1685395368_m898602943_MethodInfo_var);
		V_1 = L_7;
		TrailElement_t1685395368 * L_8 = V_1;
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = L_8->get_m_Transform_15();
		SpriteRenderer_t1209076198 * L_10 = __this->get_m_SpriteToDuplicate_5();
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_SetParent_m1963830867(L_9, L_11, (bool)1, /*hidden argument*/NULL);
		TrailElement_t1685395368 * L_12 = V_1;
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = L_12->get_m_Transform_15();
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_localScale_m2325460848(L_13, L_14, /*hidden argument*/NULL);
		TrailElement_t1685395368 * L_15 = V_1;
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = L_15->get_m_Transform_15();
		Quaternion_t4030073918  L_17 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_localRotation_m2055111962(L_16, L_17, /*hidden argument*/NULL);
		TrailElement_t1685395368 * L_18 = V_1;
		NullCheck(L_18);
		Transform_t3275118058 * L_19 = L_18->get_m_Transform_15();
		Vector3_t2243707580  L_20 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_localPosition_m1026930133(L_19, L_20, /*hidden argument*/NULL);
		Transform_t3275118058 * L_21 = __this->get_m_TrailParent_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00bb;
		}
	}
	{
		TrailElement_t1685395368 * L_23 = V_1;
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = L_23->get_m_Transform_15();
		Transform_t3275118058 * L_25 = __this->get_m_GlobalTrailContainer_25();
		NullCheck(L_24);
		Transform_SetParent_m1963830867(L_24, L_25, (bool)1, /*hidden argument*/NULL);
		goto IL_00cd;
	}

IL_00bb:
	{
		TrailElement_t1685395368 * L_26 = V_1;
		NullCheck(L_26);
		Transform_t3275118058 * L_27 = L_26->get_m_Transform_15();
		Transform_t3275118058 * L_28 = __this->get_m_LocalTrailContainer_24();
		NullCheck(L_27);
		Transform_SetParent_m1963830867(L_27, L_28, (bool)1, /*hidden argument*/NULL);
	}

IL_00cd:
	{
		Vector3_t2243707580  L_29 = ___pos0;
		V_2 = L_29;
		Vector3_t2243707580 * L_30 = (&V_2);
		float L_31 = L_30->get_z_3();
		float L_32 = __this->get_m_CurrentDisplacement_26();
		L_30->set_z_3(((float)((float)L_31+(float)L_32)));
		TrailElement_t1685395368 * L_33 = V_1;
		NullCheck(L_33);
		Transform_t3275118058 * L_34 = L_33->get_m_Transform_15();
		Vector3_t2243707580  L_35 = V_2;
		NullCheck(L_34);
		Transform_set_position_m2469242620(L_34, L_35, /*hidden argument*/NULL);
		TrailElement_t1685395368 * L_36 = V_1;
		NullCheck(L_36);
		TrailElement_Initialise_m1205103621(L_36, __this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_37 = V_0;
		String_t* L_38 = __this->get_m_LayerName_6();
		int32_t L_39 = LayerMask_NameToLayer_m1506372053(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		GameObject_set_layer_m2712461877(L_37, L_39, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpriteTrail::GenerateNewTrailElement()
extern Il2CppClass* TrailElement_t1685395368_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTrailElement_t1685395368_m898602943_MethodInfo_var;
extern const uint32_t SpriteTrail_GenerateNewTrailElement_m447001791_MetadataUsageId;
extern "C"  void SpriteTrail_GenerateNewTrailElement_m447001791 (SpriteTrail_t2284883325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteTrail_GenerateNewTrailElement_m447001791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	TrailElement_t1685395368 * V_1 = NULL;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		float L_0 = __this->get_m_CurrentDisplacement_26();
		float L_1 = __this->get_m_ZMoveStep_7();
		__this->set_m_CurrentDisplacement_26(((float)((float)L_0-(float)L_1)));
		float L_2 = __this->get_m_CurrentDisplacement_26();
		float L_3 = __this->get_m_ZMoveStep_7();
		if ((!(((float)L_2) <= ((float)L_3))))
		{
			goto IL_0030;
		}
	}
	{
		float L_4 = __this->get_m_ZMoveMax_8();
		__this->set_m_CurrentDisplacement_26(L_4);
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrailElement_t1685395368_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_5 = TrailElement_GetFreeElement_m700256560(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_5;
		GameObject_t1756533147 * L_6 = V_0;
		NullCheck(L_6);
		TrailElement_t1685395368 * L_7 = GameObject_GetComponent_TisTrailElement_t1685395368_m898602943(L_6, /*hidden argument*/GameObject_GetComponent_TisTrailElement_t1685395368_m898602943_MethodInfo_var);
		V_1 = L_7;
		TrailElement_t1685395368 * L_8 = V_1;
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = L_8->get_m_Transform_15();
		SpriteRenderer_t1209076198 * L_10 = __this->get_m_SpriteToDuplicate_5();
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_SetParent_m1963830867(L_9, L_11, (bool)1, /*hidden argument*/NULL);
		TrailElement_t1685395368 * L_12 = V_1;
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = L_12->get_m_Transform_15();
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_localScale_m2325460848(L_13, L_14, /*hidden argument*/NULL);
		TrailElement_t1685395368 * L_15 = V_1;
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = L_15->get_m_Transform_15();
		Quaternion_t4030073918  L_17 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_localRotation_m2055111962(L_16, L_17, /*hidden argument*/NULL);
		TrailElement_t1685395368 * L_18 = V_1;
		NullCheck(L_18);
		Transform_t3275118058 * L_19 = L_18->get_m_Transform_15();
		Vector3_t2243707580  L_20 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_localPosition_m1026930133(L_19, L_20, /*hidden argument*/NULL);
		Transform_t3275118058 * L_21 = __this->get_m_TrailParent_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00bb;
		}
	}
	{
		TrailElement_t1685395368 * L_23 = V_1;
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = L_23->get_m_Transform_15();
		Transform_t3275118058 * L_25 = __this->get_m_GlobalTrailContainer_25();
		NullCheck(L_24);
		Transform_SetParent_m1963830867(L_24, L_25, (bool)1, /*hidden argument*/NULL);
		goto IL_00cd;
	}

IL_00bb:
	{
		TrailElement_t1685395368 * L_26 = V_1;
		NullCheck(L_26);
		Transform_t3275118058 * L_27 = L_26->get_m_Transform_15();
		Transform_t3275118058 * L_28 = __this->get_m_LocalTrailContainer_24();
		NullCheck(L_27);
		Transform_SetParent_m1963830867(L_27, L_28, (bool)1, /*hidden argument*/NULL);
	}

IL_00cd:
	{
		GameObject_t1756533147 * L_29 = V_0;
		NullCheck(L_29);
		Transform_t3275118058 * L_30 = GameObject_get_transform_m909382139(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_t2243707580  L_31 = Transform_get_position_m1104419803(L_30, /*hidden argument*/NULL);
		V_2 = L_31;
		float L_32 = __this->get_m_CurrentDisplacement_26();
		(&V_2)->set_z_3(L_32);
		TrailElement_t1685395368 * L_33 = V_1;
		NullCheck(L_33);
		Transform_t3275118058 * L_34 = L_33->get_m_Transform_15();
		Vector3_t2243707580  L_35 = V_2;
		NullCheck(L_34);
		Transform_set_position_m2469242620(L_34, L_35, /*hidden argument*/NULL);
		TrailElement_t1685395368 * L_36 = V_1;
		NullCheck(L_36);
		TrailElement_Initialise_m1205103621(L_36, __this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_37 = V_0;
		String_t* L_38 = __this->get_m_LayerName_6();
		int32_t L_39 = LayerMask_NameToLayer_m1506372053(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		GameObject_set_layer_m2712461877(L_37, L_39, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpriteTrail::OnDisable()
extern "C"  void SpriteTrail_OnDisable_m318479869 (SpriteTrail_t2284883325 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SpriteTrail_DisableTrailEffect_m4082338629(__this, /*hidden argument*/NULL);
		int32_t L_0 = __this->get_m_TrailActivationCondition_11();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		goto IL_0025;
	}

IL_0019:
	{
		__this->set_m_WillBeActivatedOnEnable_38((bool)1);
		goto IL_0025;
	}

IL_0025:
	{
		return;
	}
}
// System.Void SpriteTrail::OnEnable()
extern "C"  void SpriteTrail_OnEnable_m1193871758 (SpriteTrail_t2284883325 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_TrailActivationCondition_11();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0013;
		}
	}
	{
		goto IL_0029;
	}

IL_0013:
	{
		bool L_2 = __this->get_m_WillBeActivatedOnEnable_38();
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		SpriteTrail_EnableTrail_m3387428939(__this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		goto IL_003f;
	}

IL_0029:
	{
		bool L_3 = __this->get_m_CanBeAutomaticallyActivated_36();
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		SpriteTrail_EnableTrail_m3387428939(__this, /*hidden argument*/NULL);
	}

IL_003a:
	{
		goto IL_003f;
	}

IL_003f:
	{
		return;
	}
}
// System.Void SpriteTrail::OnDestroy()
extern Il2CppClass* SpriteTrail_t2284883325_il2cpp_TypeInfo_var;
extern const uint32_t SpriteTrail_OnDestroy_m3464570531_MetadataUsageId;
extern "C"  void SpriteTrail_OnDestroy_m3464570531 (SpriteTrail_t2284883325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteTrail_OnDestroy_m3464570531_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteTrail_t2284883325_il2cpp_TypeInfo_var);
		int32_t L_0 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailCount_21();
		((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->set_m_TrailCount_21(((int32_t)((int32_t)L_0-(int32_t)1)));
		return;
	}
}
// UnityEngine.GameObject SpriteTrail::GetTrailElementPrefab()
extern Il2CppClass* SpriteTrail_t2284883325_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisSpriteRenderer_t1209076198_m3314035363_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisTrailElement_t1685395368_m2634836246_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral331242091;
extern const uint32_t SpriteTrail_GetTrailElementPrefab_m1242168547_MetadataUsageId;
extern "C"  GameObject_t1756533147 * SpriteTrail_GetTrailElementPrefab_m1242168547 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteTrail_GetTrailElementPrefab_m1242168547_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteTrail_t2284883325_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_0 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004a;
		}
	}
	{
		GameObject_t1756533147 * L_2 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m498247354(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SpriteTrail_t2284883325_il2cpp_TypeInfo_var);
		((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->set_m_TrailElementPrefab_23(L_2);
		GameObject_t1756533147 * L_3 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		NullCheck(L_3);
		GameObject_AddComponent_TisSpriteRenderer_t1209076198_m3314035363(L_3, /*hidden argument*/GameObject_AddComponent_TisSpriteRenderer_t1209076198_m3314035363_MethodInfo_var);
		GameObject_t1756533147 * L_4 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		NullCheck(L_4);
		GameObject_AddComponent_TisTrailElement_t1685395368_m2634836246(L_4, /*hidden argument*/GameObject_AddComponent_TisTrailElement_t1685395368_m2634836246_MethodInfo_var);
		GameObject_t1756533147 * L_5 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		NullCheck(L_5);
		Object_set_name_m4157836998(L_5, _stringLiteral331242091, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		NullCheck(L_6);
		Object_set_hideFlags_m2204253440(L_6, 1, /*hidden argument*/NULL);
	}

IL_004a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteTrail_t2284883325_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_7 = ((SpriteTrail_t2284883325_StaticFields*)SpriteTrail_t2284883325_il2cpp_TypeInfo_var->static_fields)->get_m_TrailElementPrefab_23();
		return L_7;
	}
}
// System.Void SpriteTrail::.cctor()
extern "C"  void SpriteTrail__cctor_m1651259743 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TabBase::.ctor()
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern const uint32_t TabBase__ctor_m2370470677_MetadataUsageId;
extern "C"  void TabBase__ctor_m2370470677 (TabBase_t319583306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabBase__ctor_m2370470677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_idSize_2(((int32_t)11));
		__this->set_addBaseCounter_3(5);
		__this->set_removeBaseCounter_4(5);
		__this->set_PosAverageCounter_5(2);
		GUIStyle_t1799908754 * L_0 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_0, /*hidden argument*/NULL);
		__this->set_myStyle_22(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TabBase::Start()
extern Il2CppClass* Dictionary_2_t728786214_il2cpp_TypeInfo_var;
extern Il2CppClass* TabBase_t319583306_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3873625228_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2980096784_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3880445430_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m916864576_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1884791100_MethodInfo_var;
extern const uint32_t TabBase_Start_m1508650917_MetadataUsageId;
extern "C"  void TabBase_Start_m1508650917 (TabBase_t319583306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabBase_Start_m1508650917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t728786214 * L_0 = (Dictionary_2_t728786214 *)il2cpp_codegen_object_new(Dictionary_2_t728786214_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3880445430(L_0, /*hidden argument*/Dictionary_2__ctor_m3880445430_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->set_tabData_6(L_0);
		Dictionary_2_t3873625228 * L_1 = (Dictionary_2_t3873625228 *)il2cpp_codegen_object_new(Dictionary_2_t3873625228_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m916864576(L_1, /*hidden argument*/Dictionary_2__ctor_m916864576_MethodInfo_var);
		__this->set_tabIdData_7(L_1);
		Dictionary_2_t2980096784 * L_2 = (Dictionary_2_t2980096784 *)il2cpp_codegen_object_new(Dictionary_2_t2980096784_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1884791100(L_2, /*hidden argument*/Dictionary_2__ctor_m1884791100_MethodInfo_var);
		__this->set_idDictionary_14(L_2);
		TabBase_CreateId_m2675960116(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TabBase::Update()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* testBle_t3025851879_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeoutException_t3246754798_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* tabID_t570832297_il2cpp_TypeInfo_var;
extern Il2CppClass* TabBase_t319583306_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* baseLegID_t3972271149_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m3590942114_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3707086780_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m2891962645_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3020128755_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3269019933_MethodInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m2556299403_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m89342720_MethodInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m484542103_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m1525488644_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3202490836_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m283828154_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2833412289;
extern Il2CppCodeGenString* _stringLiteral337138656;
extern Il2CppCodeGenString* _stringLiteral2492009378;
extern Il2CppCodeGenString* _stringLiteral2492009409;
extern Il2CppCodeGenString* _stringLiteral920280337;
extern Il2CppCodeGenString* _stringLiteral920280205;
extern const uint32_t TabBase_Update_m1909341032_MetadataUsageId;
extern "C"  void TabBase_Update_m1909341032 (TabBase_t319583306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabBase_Update_m1909341032_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t1642385972* V_1 = NULL;
	KeyValuePair_2_t1630970450  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t898682634  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	String_t* V_10 = NULL;
	StringU5BU5D_t1642385972* V_11 = NULL;
	int32_t V_12 = 0;
	tabID_t570832297 * V_13 = NULL;
	StringU5BU5D_t1642385972* V_14 = NULL;
	StringU5BU5D_t1642385972* V_15 = NULL;
	StringU5BU5D_t1642385972* V_16 = NULL;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	StringU5BU5D_t1642385972* V_19 = NULL;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	int32_t V_23 = 0;
	int32_t V_24 = 0;
	baseLegID_t3972271149 * V_25 = NULL;
	baseLegID_t3972271149 * V_26 = NULL;
	KeyValuePair_2_t1630970450  V_27;
	memset(&V_27, 0, sizeof(V_27));
	Enumerator_t898682634  V_28;
	memset(&V_28, 0, sizeof(V_28));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
			int32_t L_1 = testBle_get_uniqId_m3823478467(NULL /*static, unused*/, /*hidden argument*/NULL);
			int32_t L_2 = __this->get_testBleLastUniqId_16();
			if ((((int32_t)L_1) == ((int32_t)L_2)))
			{
				goto IL_0027;
			}
		}

IL_0016:
		{
			IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
			int32_t L_3 = testBle_get_uniqId_m3823478467(NULL /*static, unused*/, /*hidden argument*/NULL);
			__this->set_testBleLastUniqId_16(L_3);
			String_t* L_4 = testBle_getBleData_m3947189216(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_0 = L_4;
		}

IL_0027:
		{
			goto IL_0032;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (TimeoutException_t3246754798_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002c;
		throw e;
	}

CATCH_002c:
	{ // begin catch(System.TimeoutException)
		goto IL_0032;
	} // end catch (depth: 1)

IL_0032:
	{
		String_t* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0d4c;
		}
	}
	{
		String_t* L_7 = V_0;
		CharU5BU5D_t1328083999* L_8 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)35));
		NullCheck(L_7);
		StringU5BU5D_t1642385972* L_9 = String_Split_m3326265864(L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		String_t* L_10 = V_0;
		__this->set_testData_17(L_10);
		Dictionary_2_t3873625228 * L_11 = __this->get_tabIdData_7();
		NullCheck(L_11);
		Enumerator_t898682634  L_12 = Dictionary_2_GetEnumerator_m3590942114(L_11, /*hidden argument*/Dictionary_2_GetEnumerator_m3590942114_MethodInfo_var);
		V_3 = L_12;
	}

IL_0062:
	try
	{ // begin try (depth: 1)
		{
			goto IL_007c;
		}

IL_0067:
		{
			KeyValuePair_2_t1630970450  L_13 = Enumerator_get_Current_m3707086780((&V_3), /*hidden argument*/Enumerator_get_Current_m3707086780_MethodInfo_var);
			V_2 = L_13;
			tabID_t570832297 * L_14 = KeyValuePair_2_get_Value_m2891962645((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_14);
			L_14->set_changedData_2((bool)0);
		}

IL_007c:
		{
			bool L_15 = Enumerator_MoveNext_m3020128755((&V_3), /*hidden argument*/Enumerator_MoveNext_m3020128755_MethodInfo_var);
			if (L_15)
			{
				goto IL_0067;
			}
		}

IL_0088:
		{
			IL2CPP_LEAVE(0x9B, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3269019933((&V_3), /*hidden argument*/Enumerator_Dispose_m3269019933_MethodInfo_var);
		IL2CPP_END_FINALLY(141)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_009b:
	{
		StringU5BU5D_t1642385972* L_16 = V_1;
		V_11 = L_16;
		V_12 = 0;
		goto IL_0b24;
	}

IL_00a6:
	{
		StringU5BU5D_t1642385972* L_17 = V_11;
		int32_t L_18 = V_12;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		String_t* L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_10 = L_20;
		tabID_t570832297 * L_21 = (tabID_t570832297 *)il2cpp_codegen_object_new(tabID_t570832297_il2cpp_TypeInfo_var);
		tabID__ctor_m1405518218(L_21, /*hidden argument*/NULL);
		V_13 = L_21;
		String_t* L_22 = V_10;
		CharU5BU5D_t1328083999* L_23 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_23);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)45));
		NullCheck(L_22);
		StringU5BU5D_t1642385972* L_24 = String_Split_m3326265864(L_22, L_23, /*hidden argument*/NULL);
		V_14 = L_24;
		StringU5BU5D_t1642385972* L_25 = V_14;
		NullCheck(L_25);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_048f;
		}
	}
	{
		StringU5BU5D_t1642385972* L_26 = V_14;
		NullCheck(L_26);
		int32_t L_27 = 0;
		String_t* L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		CharU5BU5D_t1328083999* L_29 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_29);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)32));
		NullCheck(L_28);
		StringU5BU5D_t1642385972* L_30 = String_Split_m3326265864(L_28, L_29, /*hidden argument*/NULL);
		V_15 = L_30;
		StringU5BU5D_t1642385972* L_31 = V_14;
		NullCheck(L_31);
		int32_t L_32 = 1;
		String_t* L_33 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		CharU5BU5D_t1328083999* L_34 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_34);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)32));
		NullCheck(L_33);
		StringU5BU5D_t1642385972* L_35 = String_Split_m3326265864(L_33, L_34, /*hidden argument*/NULL);
		V_16 = L_35;
		StringU5BU5D_t1642385972* L_36 = V_15;
		NullCheck(L_36);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_36)->max_length))))) == ((uint32_t)3))))
		{
			goto IL_048a;
		}
	}
	{
		StringU5BU5D_t1642385972* L_37 = V_16;
		NullCheck(L_37);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_37)->max_length))))) == ((uint32_t)3))))
		{
			goto IL_048a;
		}
	}
	{
		StringU5BU5D_t1642385972* L_38 = V_15;
		NullCheck(L_38);
		int32_t L_39 = 0;
		String_t* L_40 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		bool L_41 = Int32_TryParse_m656840904(NULL /*static, unused*/, L_40, (&V_4), /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_048a;
		}
	}
	{
		StringU5BU5D_t1642385972* L_42 = V_15;
		NullCheck(L_42);
		int32_t L_43 = 1;
		String_t* L_44 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		bool L_45 = Int32_TryParse_m656840904(NULL /*static, unused*/, L_44, (&V_6), /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_048a;
		}
	}
	{
		StringU5BU5D_t1642385972* L_46 = V_15;
		NullCheck(L_46);
		int32_t L_47 = 2;
		String_t* L_48 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		bool L_49 = Int32_TryParse_m656840904(NULL /*static, unused*/, L_48, (&V_9), /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_048a;
		}
	}
	{
		StringU5BU5D_t1642385972* L_50 = V_16;
		NullCheck(L_50);
		int32_t L_51 = 0;
		String_t* L_52 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		bool L_53 = Int32_TryParse_m656840904(NULL /*static, unused*/, L_52, (&V_5), /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_048a;
		}
	}
	{
		StringU5BU5D_t1642385972* L_54 = V_16;
		NullCheck(L_54);
		int32_t L_55 = 1;
		String_t* L_56 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		bool L_57 = Int32_TryParse_m656840904(NULL /*static, unused*/, L_56, (&V_7), /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_048a;
		}
	}
	{
		StringU5BU5D_t1642385972* L_58 = V_16;
		NullCheck(L_58);
		int32_t L_59 = 2;
		String_t* L_60 = (L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_59));
		bool L_61 = Int32_TryParse_m656840904(NULL /*static, unused*/, L_60, (&V_8), /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_048a;
		}
	}
	{
		tabID_t570832297 * L_62 = V_13;
		int32_t L_63 = V_4;
		NullCheck(L_62);
		L_62->set_id_1(((int32_t)((int32_t)L_63/(int32_t)2)));
		Dictionary_2_t3873625228 * L_64 = __this->get_tabIdData_7();
		tabID_t570832297 * L_65 = V_13;
		NullCheck(L_65);
		int32_t L_66 = L_65->get_id_1();
		NullCheck(L_64);
		bool L_67 = Dictionary_2_ContainsKey_m2556299403(L_64, L_66, /*hidden argument*/Dictionary_2_ContainsKey_m2556299403_MethodInfo_var);
		if (!L_67)
		{
			goto IL_048a;
		}
	}
	{
		tabID_t570832297 * L_68 = V_13;
		NullCheck(L_68);
		int32_t L_69 = L_68->get_id_1();
		if ((!(((uint32_t)L_69) == ((uint32_t)1))))
		{
			goto IL_01ae;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->set_switch_state1_11(0);
		__this->set_buttonCounter_13(0);
	}

IL_01ae:
	{
		tabID_t570832297 * L_70 = V_13;
		NullCheck(L_70);
		int32_t L_71 = L_70->get_id_1();
		if ((!(((uint32_t)L_71) == ((uint32_t)5))))
		{
			goto IL_01c1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->set_switch_state5_12(0);
	}

IL_01c1:
	{
		tabID_t570832297 * L_72 = V_13;
		NullCheck(L_72);
		tBase_t1720960579 * L_73 = L_72->get_getPos_0();
		int32_t L_74 = V_6;
		int32_t L_75 = V_7;
		NullCheck(L_73);
		L_73->set_x_1(((int32_t)((int32_t)((int32_t)((int32_t)L_74+(int32_t)L_75))/(int32_t)2)));
		tabID_t570832297 * L_76 = V_13;
		NullCheck(L_76);
		tBase_t1720960579 * L_77 = L_76->get_getPos_0();
		int32_t L_78 = V_9;
		int32_t L_79 = V_8;
		NullCheck(L_77);
		L_77->set_y_2(((int32_t)((int32_t)((int32_t)((int32_t)L_78+(int32_t)L_79))/(int32_t)2)));
		int32_t L_80 = V_6;
		int32_t L_81 = V_7;
		V_17 = ((int32_t)((int32_t)L_80-(int32_t)L_81));
		int32_t L_82 = V_9;
		int32_t L_83 = V_8;
		V_18 = ((int32_t)((int32_t)L_82-(int32_t)L_83));
		tabID_t570832297 * L_84 = V_13;
		NullCheck(L_84);
		tBase_t1720960579 * L_85 = L_84->get_getPos_0();
		int32_t L_86 = V_17;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		float L_87 = Convert_ToSingle_m915696580(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
		float L_88 = fabsf(L_87);
		int32_t L_89 = V_18;
		float L_90 = Convert_ToSingle_m915696580(NULL /*static, unused*/, L_89, /*hidden argument*/NULL);
		float L_91 = fabsf(L_90);
		NullCheck(L_85);
		L_85->set_rotation_3(((float)((float)L_88/(float)L_91)));
		tabID_t570832297 * L_92 = V_13;
		NullCheck(L_92);
		tBase_t1720960579 * L_93 = L_92->get_getPos_0();
		tabID_t570832297 * L_94 = V_13;
		NullCheck(L_94);
		tBase_t1720960579 * L_95 = L_94->get_getPos_0();
		NullCheck(L_95);
		float L_96 = L_95->get_rotation_3();
		double L_97 = atan((((double)((double)L_96))));
		float L_98 = Convert_ToSingle_m690748777(NULL /*static, unused*/, L_97, /*hidden argument*/NULL);
		NullCheck(L_93);
		L_93->set_rotation_3(L_98);
		tabID_t570832297 * L_99 = V_13;
		NullCheck(L_99);
		tBase_t1720960579 * L_100 = L_99->get_getPos_0();
		tabID_t570832297 * L_101 = V_13;
		NullCheck(L_101);
		tBase_t1720960579 * L_102 = L_101->get_getPos_0();
		NullCheck(L_102);
		float L_103 = L_102->get_rotation_3();
		float L_104 = Convert_ToSingle_m690748777(NULL /*static, unused*/, ((double)((double)(((double)((double)L_103)))*(double)(57.295779513082323))), /*hidden argument*/NULL);
		NullCheck(L_100);
		L_100->set_rotation_3(L_104);
		int32_t L_105 = V_18;
		if ((((int32_t)L_105) <= ((int32_t)0)))
		{
			goto IL_0298;
		}
	}
	{
		int32_t L_106 = V_17;
		if ((((int32_t)L_106) < ((int32_t)0)))
		{
			goto IL_0298;
		}
	}
	{
		tabID_t570832297 * L_107 = V_13;
		NullCheck(L_107);
		tBase_t1720960579 * L_108 = L_107->get_getPos_0();
		tabID_t570832297 * L_109 = V_13;
		NullCheck(L_109);
		tBase_t1720960579 * L_110 = L_109->get_getPos_0();
		NullCheck(L_110);
		float L_111 = L_110->get_rotation_3();
		NullCheck(L_108);
		L_108->set_rotation_3(((float)((float)(360.0f)-(float)L_111)));
		goto IL_0320;
	}

IL_0298:
	{
		int32_t L_112 = V_18;
		if ((((int32_t)L_112) < ((int32_t)0)))
		{
			goto IL_02bf;
		}
	}
	{
		int32_t L_113 = V_17;
		if ((((int32_t)L_113) >= ((int32_t)0)))
		{
			goto IL_02bf;
		}
	}
	{
		tabID_t570832297 * L_114 = V_13;
		NullCheck(L_114);
		tBase_t1720960579 * L_115 = L_114->get_getPos_0();
		tBase_t1720960579 * L_116 = L_115;
		NullCheck(L_116);
		float L_117 = L_116->get_rotation_3();
		NullCheck(L_116);
		L_116->set_rotation_3(L_117);
		goto IL_0320;
	}

IL_02bf:
	{
		int32_t L_118 = V_18;
		if ((((int32_t)L_118) >= ((int32_t)0)))
		{
			goto IL_02f2;
		}
	}
	{
		int32_t L_119 = V_17;
		if ((((int32_t)L_119) >= ((int32_t)0)))
		{
			goto IL_02f2;
		}
	}
	{
		tabID_t570832297 * L_120 = V_13;
		NullCheck(L_120);
		tBase_t1720960579 * L_121 = L_120->get_getPos_0();
		tabID_t570832297 * L_122 = V_13;
		NullCheck(L_122);
		tBase_t1720960579 * L_123 = L_122->get_getPos_0();
		NullCheck(L_123);
		float L_124 = L_123->get_rotation_3();
		NullCheck(L_121);
		L_121->set_rotation_3(((float)((float)(180.0f)-(float)L_124)));
		goto IL_0320;
	}

IL_02f2:
	{
		int32_t L_125 = V_18;
		if ((((int32_t)L_125) > ((int32_t)0)))
		{
			goto IL_0320;
		}
	}
	{
		int32_t L_126 = V_17;
		if ((((int32_t)L_126) < ((int32_t)0)))
		{
			goto IL_0320;
		}
	}
	{
		tabID_t570832297 * L_127 = V_13;
		NullCheck(L_127);
		tBase_t1720960579 * L_128 = L_127->get_getPos_0();
		tabID_t570832297 * L_129 = V_13;
		NullCheck(L_129);
		tBase_t1720960579 * L_130 = L_129->get_getPos_0();
		NullCheck(L_130);
		float L_131 = L_130->get_rotation_3();
		NullCheck(L_128);
		L_128->set_rotation_3(((float)((float)(180.0f)+(float)L_131)));
	}

IL_0320:
	{
		tabID_t570832297 * L_132 = V_13;
		NullCheck(L_132);
		tBase_t1720960579 * L_133 = L_132->get_getPos_0();
		tabID_t570832297 * L_134 = V_13;
		NullCheck(L_134);
		tBase_t1720960579 * L_135 = L_134->get_getPos_0();
		NullCheck(L_135);
		float L_136 = L_135->get_rotation_3();
		NullCheck(L_133);
		L_133->set_rotation_3(((-L_136)));
		Dictionary_2_t3873625228 * L_137 = __this->get_tabIdData_7();
		tabID_t570832297 * L_138 = V_13;
		NullCheck(L_138);
		int32_t L_139 = L_138->get_id_1();
		NullCheck(L_137);
		bool L_140 = Dictionary_2_ContainsKey_m2556299403(L_137, L_139, /*hidden argument*/Dictionary_2_ContainsKey_m2556299403_MethodInfo_var);
		if (!L_140)
		{
			goto IL_048a;
		}
	}
	{
		Dictionary_2_t3873625228 * L_141 = __this->get_tabIdData_7();
		tabID_t570832297 * L_142 = V_13;
		NullCheck(L_142);
		int32_t L_143 = L_142->get_id_1();
		NullCheck(L_141);
		tabID_t570832297 * L_144 = Dictionary_2_get_Item_m89342720(L_141, L_143, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		tabID_t570832297 * L_145 = V_13;
		NullCheck(L_145);
		int32_t L_146 = L_145->get_id_1();
		NullCheck(L_144);
		L_144->set_id_1(L_146);
		Dictionary_2_t3873625228 * L_147 = __this->get_tabIdData_7();
		tabID_t570832297 * L_148 = V_13;
		NullCheck(L_148);
		int32_t L_149 = L_148->get_id_1();
		NullCheck(L_147);
		tabID_t570832297 * L_150 = Dictionary_2_get_Item_m89342720(L_147, L_149, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_150);
		tBase_t1720960579 * L_151 = L_150->get_getPos_0();
		tBase_t1720960579 * L_152 = L_151;
		NullCheck(L_152);
		int32_t L_153 = L_152->get_x_1();
		tabID_t570832297 * L_154 = V_13;
		NullCheck(L_154);
		tBase_t1720960579 * L_155 = L_154->get_getPos_0();
		NullCheck(L_155);
		int32_t L_156 = L_155->get_x_1();
		NullCheck(L_152);
		L_152->set_x_1(((int32_t)((int32_t)L_153+(int32_t)L_156)));
		Dictionary_2_t3873625228 * L_157 = __this->get_tabIdData_7();
		tabID_t570832297 * L_158 = V_13;
		NullCheck(L_158);
		int32_t L_159 = L_158->get_id_1();
		NullCheck(L_157);
		tabID_t570832297 * L_160 = Dictionary_2_get_Item_m89342720(L_157, L_159, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_160);
		tBase_t1720960579 * L_161 = L_160->get_getPos_0();
		tBase_t1720960579 * L_162 = L_161;
		NullCheck(L_162);
		int32_t L_163 = L_162->get_y_2();
		tabID_t570832297 * L_164 = V_13;
		NullCheck(L_164);
		tBase_t1720960579 * L_165 = L_164->get_getPos_0();
		NullCheck(L_165);
		int32_t L_166 = L_165->get_y_2();
		NullCheck(L_162);
		L_162->set_y_2(((int32_t)((int32_t)L_163+(int32_t)L_166)));
		Dictionary_2_t3873625228 * L_167 = __this->get_tabIdData_7();
		tabID_t570832297 * L_168 = V_13;
		NullCheck(L_168);
		int32_t L_169 = L_168->get_id_1();
		NullCheck(L_167);
		tabID_t570832297 * L_170 = Dictionary_2_get_Item_m89342720(L_167, L_169, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_170);
		tBase_t1720960579 * L_171 = L_170->get_getPos_0();
		tabID_t570832297 * L_172 = V_13;
		NullCheck(L_172);
		tBase_t1720960579 * L_173 = L_172->get_getPos_0();
		NullCheck(L_173);
		float L_174 = L_173->get_rotation_3();
		NullCheck(L_171);
		L_171->set_rotation_3(L_174);
		Dictionary_2_t3873625228 * L_175 = __this->get_tabIdData_7();
		tabID_t570832297 * L_176 = V_13;
		NullCheck(L_176);
		int32_t L_177 = L_176->get_id_1();
		NullCheck(L_175);
		tabID_t570832297 * L_178 = Dictionary_2_get_Item_m89342720(L_175, L_177, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_178);
		tBase_t1720960579 * L_179 = L_178->get_getPos_0();
		tabID_t570832297 * L_180 = V_13;
		NullCheck(L_180);
		tBase_t1720960579 * L_181 = L_180->get_getPos_0();
		NullCheck(L_181);
		bool L_182 = L_181->get_state_0();
		NullCheck(L_179);
		L_179->set_state_0(L_182);
		Dictionary_2_t3873625228 * L_183 = __this->get_tabIdData_7();
		tabID_t570832297 * L_184 = V_13;
		NullCheck(L_184);
		int32_t L_185 = L_184->get_id_1();
		NullCheck(L_183);
		tabID_t570832297 * L_186 = Dictionary_2_get_Item_m89342720(L_183, L_185, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		tabID_t570832297 * L_187 = L_186;
		NullCheck(L_187);
		int32_t L_188 = L_187->get_addCounter_3();
		NullCheck(L_187);
		L_187->set_addCounter_3(((int32_t)((int32_t)L_188+(int32_t)1)));
		Dictionary_2_t3873625228 * L_189 = __this->get_tabIdData_7();
		tabID_t570832297 * L_190 = V_13;
		NullCheck(L_190);
		int32_t L_191 = L_190->get_id_1();
		NullCheck(L_189);
		tabID_t570832297 * L_192 = Dictionary_2_get_Item_m89342720(L_189, L_191, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_192);
		L_192->set_removeCounter_4(0);
		Dictionary_2_t3873625228 * L_193 = __this->get_tabIdData_7();
		tabID_t570832297 * L_194 = V_13;
		NullCheck(L_194);
		int32_t L_195 = L_194->get_id_1();
		NullCheck(L_193);
		tabID_t570832297 * L_196 = Dictionary_2_get_Item_m89342720(L_193, L_195, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_196);
		L_196->set_changedData_2((bool)1);
		Dictionary_2_t3873625228 * L_197 = __this->get_tabIdData_7();
		tabID_t570832297 * L_198 = V_13;
		NullCheck(L_198);
		int32_t L_199 = L_198->get_id_1();
		NullCheck(L_197);
		tabID_t570832297 * L_200 = Dictionary_2_get_Item_m89342720(L_197, L_199, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		tabID_t570832297 * L_201 = L_200;
		NullCheck(L_201);
		int32_t L_202 = L_201->get_posAverageCounter_5();
		NullCheck(L_201);
		L_201->set_posAverageCounter_5(((int32_t)((int32_t)L_202+(int32_t)1)));
	}

IL_048a:
	{
		goto IL_0b1e;
	}

IL_048f:
	{
		StringU5BU5D_t1642385972* L_203 = V_14;
		NullCheck(L_203);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_203)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0b1e;
		}
	}
	{
		StringU5BU5D_t1642385972* L_204 = V_14;
		NullCheck(L_204);
		int32_t L_205 = 0;
		String_t* L_206 = (L_204)->GetAt(static_cast<il2cpp_array_size_t>(L_205));
		CharU5BU5D_t1328083999* L_207 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_207);
		(L_207)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)32));
		NullCheck(L_206);
		StringU5BU5D_t1642385972* L_208 = String_Split_m3326265864(L_206, L_207, /*hidden argument*/NULL);
		V_19 = L_208;
		StringU5BU5D_t1642385972* L_209 = V_19;
		NullCheck(L_209);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_209)->max_length))))) == ((uint32_t)3))))
		{
			goto IL_0b1e;
		}
	}
	{
		StringU5BU5D_t1642385972* L_210 = V_19;
		NullCheck(L_210);
		int32_t L_211 = 0;
		String_t* L_212 = (L_210)->GetAt(static_cast<il2cpp_array_size_t>(L_211));
		bool L_213 = Int32_TryParse_m656840904(NULL /*static, unused*/, L_212, (&V_4), /*hidden argument*/NULL);
		if (!L_213)
		{
			goto IL_0b1e;
		}
	}
	{
		StringU5BU5D_t1642385972* L_214 = V_19;
		NullCheck(L_214);
		int32_t L_215 = 1;
		String_t* L_216 = (L_214)->GetAt(static_cast<il2cpp_array_size_t>(L_215));
		bool L_217 = Int32_TryParse_m656840904(NULL /*static, unused*/, L_216, (&V_6), /*hidden argument*/NULL);
		if (!L_217)
		{
			goto IL_0b1e;
		}
	}
	{
		StringU5BU5D_t1642385972* L_218 = V_19;
		NullCheck(L_218);
		int32_t L_219 = 2;
		String_t* L_220 = (L_218)->GetAt(static_cast<il2cpp_array_size_t>(L_219));
		bool L_221 = Int32_TryParse_m656840904(NULL /*static, unused*/, L_220, (&V_9), /*hidden argument*/NULL);
		if (!L_221)
		{
			goto IL_0b1e;
		}
	}
	{
		int32_t L_222 = V_4;
		if ((((int32_t)L_222) == ((int32_t)((int32_t)25))))
		{
			goto IL_04fb;
		}
	}
	{
		int32_t L_223 = V_4;
		if ((!(((uint32_t)L_223) == ((uint32_t)((int32_t)23)))))
		{
			goto IL_0954;
		}
	}

IL_04fb:
	{
		int32_t L_224 = V_4;
		if ((!(((uint32_t)L_224) == ((uint32_t)((int32_t)25)))))
		{
			goto IL_050c;
		}
	}
	{
		V_20 = 3;
		goto IL_0510;
	}

IL_050c:
	{
		V_20 = ((int32_t)11);
	}

IL_0510:
	{
		Dictionary_2_t2980096784 * L_225 = __this->get_idDictionary_14();
		int32_t L_226 = V_20;
		NullCheck(L_225);
		bool L_227 = Dictionary_2_ContainsKey_m484542103(L_225, L_226, /*hidden argument*/Dictionary_2_ContainsKey_m484542103_MethodInfo_var);
		if (!L_227)
		{
			goto IL_08e5;
		}
	}
	{
		tabID_t570832297 * L_228 = V_13;
		Dictionary_2_t2980096784 * L_229 = __this->get_idDictionary_14();
		int32_t L_230 = V_20;
		NullCheck(L_229);
		baseLegID_t3972271149 * L_231 = Dictionary_2_get_Item_m1525488644(L_229, L_230, /*hidden argument*/Dictionary_2_get_Item_m1525488644_MethodInfo_var);
		NullCheck(L_231);
		int32_t L_232 = L_231->get_id_0();
		NullCheck(L_228);
		L_228->set_id_1(((int32_t)((int32_t)L_232/(int32_t)2)));
		Dictionary_2_t2980096784 * L_233 = __this->get_idDictionary_14();
		int32_t L_234 = V_20;
		NullCheck(L_233);
		baseLegID_t3972271149 * L_235 = Dictionary_2_get_Item_m1525488644(L_233, L_234, /*hidden argument*/Dictionary_2_get_Item_m1525488644_MethodInfo_var);
		NullCheck(L_235);
		int32_t L_236 = L_235->get_x_1();
		V_21 = L_236;
		Dictionary_2_t2980096784 * L_237 = __this->get_idDictionary_14();
		int32_t L_238 = V_20;
		NullCheck(L_237);
		baseLegID_t3972271149 * L_239 = Dictionary_2_get_Item_m1525488644(L_237, L_238, /*hidden argument*/Dictionary_2_get_Item_m1525488644_MethodInfo_var);
		NullCheck(L_239);
		int32_t L_240 = L_239->get_y_2();
		V_22 = L_240;
		Dictionary_2_t3873625228 * L_241 = __this->get_tabIdData_7();
		tabID_t570832297 * L_242 = V_13;
		NullCheck(L_242);
		int32_t L_243 = L_242->get_id_1();
		NullCheck(L_241);
		bool L_244 = Dictionary_2_ContainsKey_m2556299403(L_241, L_243, /*hidden argument*/Dictionary_2_ContainsKey_m2556299403_MethodInfo_var);
		if (!L_244)
		{
			goto IL_08e5;
		}
	}
	{
		tabID_t570832297 * L_245 = V_13;
		NullCheck(L_245);
		tBase_t1720960579 * L_246 = L_245->get_getPos_0();
		int32_t L_247 = V_6;
		int32_t L_248 = V_21;
		NullCheck(L_246);
		L_246->set_x_1(((int32_t)((int32_t)((int32_t)((int32_t)L_247+(int32_t)L_248))/(int32_t)2)));
		tabID_t570832297 * L_249 = V_13;
		NullCheck(L_249);
		tBase_t1720960579 * L_250 = L_249->get_getPos_0();
		int32_t L_251 = V_9;
		int32_t L_252 = V_22;
		NullCheck(L_250);
		L_250->set_y_2(((int32_t)((int32_t)((int32_t)((int32_t)L_251+(int32_t)L_252))/(int32_t)2)));
		ObjectU5BU5D_t3614634134* L_253 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_253);
		ArrayElementTypeCheck (L_253, _stringLiteral2833412289);
		(L_253)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2833412289);
		ObjectU5BU5D_t3614634134* L_254 = L_253;
		int32_t L_255 = V_6;
		int32_t L_256 = L_255;
		Il2CppObject * L_257 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_256);
		NullCheck(L_254);
		ArrayElementTypeCheck (L_254, L_257);
		(L_254)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_257);
		ObjectU5BU5D_t3614634134* L_258 = L_254;
		NullCheck(L_258);
		ArrayElementTypeCheck (L_258, _stringLiteral337138656);
		(L_258)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral337138656);
		ObjectU5BU5D_t3614634134* L_259 = L_258;
		int32_t L_260 = V_9;
		int32_t L_261 = L_260;
		Il2CppObject * L_262 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_261);
		NullCheck(L_259);
		ArrayElementTypeCheck (L_259, L_262);
		(L_259)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_262);
		ObjectU5BU5D_t3614634134* L_263 = L_259;
		NullCheck(L_263);
		ArrayElementTypeCheck (L_263, _stringLiteral2492009378);
		(L_263)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral2492009378);
		ObjectU5BU5D_t3614634134* L_264 = L_263;
		int32_t L_265 = V_21;
		int32_t L_266 = L_265;
		Il2CppObject * L_267 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_266);
		NullCheck(L_264);
		ArrayElementTypeCheck (L_264, L_267);
		(L_264)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_267);
		ObjectU5BU5D_t3614634134* L_268 = L_264;
		NullCheck(L_268);
		ArrayElementTypeCheck (L_268, _stringLiteral2492009409);
		(L_268)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral2492009409);
		ObjectU5BU5D_t3614634134* L_269 = L_268;
		int32_t L_270 = V_22;
		int32_t L_271 = L_270;
		Il2CppObject * L_272 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_271);
		NullCheck(L_269);
		ArrayElementTypeCheck (L_269, L_272);
		(L_269)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_272);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_273 = String_Concat_m3881798623(NULL /*static, unused*/, L_269, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_273, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_274 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_274);
		ArrayElementTypeCheck (L_274, _stringLiteral2833412289);
		(L_274)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2833412289);
		ObjectU5BU5D_t3614634134* L_275 = L_274;
		tabID_t570832297 * L_276 = V_13;
		NullCheck(L_276);
		tBase_t1720960579 * L_277 = L_276->get_getPos_0();
		NullCheck(L_277);
		int32_t L_278 = L_277->get_x_1();
		int32_t L_279 = L_278;
		Il2CppObject * L_280 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_279);
		NullCheck(L_275);
		ArrayElementTypeCheck (L_275, L_280);
		(L_275)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_280);
		ObjectU5BU5D_t3614634134* L_281 = L_275;
		NullCheck(L_281);
		ArrayElementTypeCheck (L_281, _stringLiteral337138656);
		(L_281)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral337138656);
		ObjectU5BU5D_t3614634134* L_282 = L_281;
		tabID_t570832297 * L_283 = V_13;
		NullCheck(L_283);
		tBase_t1720960579 * L_284 = L_283->get_getPos_0();
		NullCheck(L_284);
		int32_t L_285 = L_284->get_y_2();
		int32_t L_286 = L_285;
		Il2CppObject * L_287 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_286);
		NullCheck(L_282);
		ArrayElementTypeCheck (L_282, L_287);
		(L_282)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_287);
		String_t* L_288 = String_Concat_m3881798623(NULL /*static, unused*/, L_282, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_288, /*hidden argument*/NULL);
		int32_t L_289 = V_6;
		int32_t L_290 = V_21;
		V_23 = ((int32_t)((int32_t)L_289-(int32_t)L_290));
		int32_t L_291 = V_9;
		int32_t L_292 = V_22;
		V_24 = ((int32_t)((int32_t)L_291-(int32_t)L_292));
		tabID_t570832297 * L_293 = V_13;
		NullCheck(L_293);
		tBase_t1720960579 * L_294 = L_293->get_getPos_0();
		int32_t L_295 = V_23;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		float L_296 = Convert_ToSingle_m915696580(NULL /*static, unused*/, L_295, /*hidden argument*/NULL);
		float L_297 = fabsf(L_296);
		int32_t L_298 = V_24;
		float L_299 = Convert_ToSingle_m915696580(NULL /*static, unused*/, L_298, /*hidden argument*/NULL);
		float L_300 = fabsf(L_299);
		NullCheck(L_294);
		L_294->set_rotation_3(((float)((float)L_297/(float)L_300)));
		tabID_t570832297 * L_301 = V_13;
		NullCheck(L_301);
		tBase_t1720960579 * L_302 = L_301->get_getPos_0();
		tabID_t570832297 * L_303 = V_13;
		NullCheck(L_303);
		tBase_t1720960579 * L_304 = L_303->get_getPos_0();
		NullCheck(L_304);
		float L_305 = L_304->get_rotation_3();
		double L_306 = atan((((double)((double)L_305))));
		float L_307 = Convert_ToSingle_m690748777(NULL /*static, unused*/, L_306, /*hidden argument*/NULL);
		NullCheck(L_302);
		L_302->set_rotation_3(L_307);
		tabID_t570832297 * L_308 = V_13;
		NullCheck(L_308);
		tBase_t1720960579 * L_309 = L_308->get_getPos_0();
		tabID_t570832297 * L_310 = V_13;
		NullCheck(L_310);
		tBase_t1720960579 * L_311 = L_310->get_getPos_0();
		NullCheck(L_311);
		float L_312 = L_311->get_rotation_3();
		float L_313 = Convert_ToSingle_m690748777(NULL /*static, unused*/, ((double)((double)(((double)((double)L_312)))*(double)(57.295779513082323))), /*hidden argument*/NULL);
		NullCheck(L_309);
		L_309->set_rotation_3(L_313);
		int32_t L_314 = V_24;
		if ((((int32_t)L_314) <= ((int32_t)0)))
		{
			goto IL_06f3;
		}
	}
	{
		int32_t L_315 = V_23;
		if ((((int32_t)L_315) < ((int32_t)0)))
		{
			goto IL_06f3;
		}
	}
	{
		tabID_t570832297 * L_316 = V_13;
		NullCheck(L_316);
		tBase_t1720960579 * L_317 = L_316->get_getPos_0();
		tabID_t570832297 * L_318 = V_13;
		NullCheck(L_318);
		tBase_t1720960579 * L_319 = L_318->get_getPos_0();
		NullCheck(L_319);
		float L_320 = L_319->get_rotation_3();
		NullCheck(L_317);
		L_317->set_rotation_3(((float)((float)(360.0f)-(float)L_320)));
		goto IL_077b;
	}

IL_06f3:
	{
		int32_t L_321 = V_24;
		if ((((int32_t)L_321) < ((int32_t)0)))
		{
			goto IL_071a;
		}
	}
	{
		int32_t L_322 = V_23;
		if ((((int32_t)L_322) >= ((int32_t)0)))
		{
			goto IL_071a;
		}
	}
	{
		tabID_t570832297 * L_323 = V_13;
		NullCheck(L_323);
		tBase_t1720960579 * L_324 = L_323->get_getPos_0();
		tBase_t1720960579 * L_325 = L_324;
		NullCheck(L_325);
		float L_326 = L_325->get_rotation_3();
		NullCheck(L_325);
		L_325->set_rotation_3(L_326);
		goto IL_077b;
	}

IL_071a:
	{
		int32_t L_327 = V_24;
		if ((((int32_t)L_327) >= ((int32_t)0)))
		{
			goto IL_074d;
		}
	}
	{
		int32_t L_328 = V_23;
		if ((((int32_t)L_328) >= ((int32_t)0)))
		{
			goto IL_074d;
		}
	}
	{
		tabID_t570832297 * L_329 = V_13;
		NullCheck(L_329);
		tBase_t1720960579 * L_330 = L_329->get_getPos_0();
		tabID_t570832297 * L_331 = V_13;
		NullCheck(L_331);
		tBase_t1720960579 * L_332 = L_331->get_getPos_0();
		NullCheck(L_332);
		float L_333 = L_332->get_rotation_3();
		NullCheck(L_330);
		L_330->set_rotation_3(((float)((float)(180.0f)-(float)L_333)));
		goto IL_077b;
	}

IL_074d:
	{
		int32_t L_334 = V_24;
		if ((((int32_t)L_334) > ((int32_t)0)))
		{
			goto IL_077b;
		}
	}
	{
		int32_t L_335 = V_23;
		if ((((int32_t)L_335) < ((int32_t)0)))
		{
			goto IL_077b;
		}
	}
	{
		tabID_t570832297 * L_336 = V_13;
		NullCheck(L_336);
		tBase_t1720960579 * L_337 = L_336->get_getPos_0();
		tabID_t570832297 * L_338 = V_13;
		NullCheck(L_338);
		tBase_t1720960579 * L_339 = L_338->get_getPos_0();
		NullCheck(L_339);
		float L_340 = L_339->get_rotation_3();
		NullCheck(L_337);
		L_337->set_rotation_3(((float)((float)(180.0f)+(float)L_340)));
	}

IL_077b:
	{
		tabID_t570832297 * L_341 = V_13;
		NullCheck(L_341);
		tBase_t1720960579 * L_342 = L_341->get_getPos_0();
		tabID_t570832297 * L_343 = V_13;
		NullCheck(L_343);
		tBase_t1720960579 * L_344 = L_343->get_getPos_0();
		NullCheck(L_344);
		float L_345 = L_344->get_rotation_3();
		NullCheck(L_342);
		L_342->set_rotation_3(((-L_345)));
		Dictionary_2_t3873625228 * L_346 = __this->get_tabIdData_7();
		tabID_t570832297 * L_347 = V_13;
		NullCheck(L_347);
		int32_t L_348 = L_347->get_id_1();
		NullCheck(L_346);
		bool L_349 = Dictionary_2_ContainsKey_m2556299403(L_346, L_348, /*hidden argument*/Dictionary_2_ContainsKey_m2556299403_MethodInfo_var);
		if (!L_349)
		{
			goto IL_08e5;
		}
	}
	{
		Dictionary_2_t3873625228 * L_350 = __this->get_tabIdData_7();
		tabID_t570832297 * L_351 = V_13;
		NullCheck(L_351);
		int32_t L_352 = L_351->get_id_1();
		NullCheck(L_350);
		tabID_t570832297 * L_353 = Dictionary_2_get_Item_m89342720(L_350, L_352, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		tabID_t570832297 * L_354 = V_13;
		NullCheck(L_354);
		int32_t L_355 = L_354->get_id_1();
		NullCheck(L_353);
		L_353->set_id_1(L_355);
		Dictionary_2_t3873625228 * L_356 = __this->get_tabIdData_7();
		tabID_t570832297 * L_357 = V_13;
		NullCheck(L_357);
		int32_t L_358 = L_357->get_id_1();
		NullCheck(L_356);
		tabID_t570832297 * L_359 = Dictionary_2_get_Item_m89342720(L_356, L_358, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_359);
		tBase_t1720960579 * L_360 = L_359->get_getPos_0();
		tBase_t1720960579 * L_361 = L_360;
		NullCheck(L_361);
		int32_t L_362 = L_361->get_x_1();
		tabID_t570832297 * L_363 = V_13;
		NullCheck(L_363);
		tBase_t1720960579 * L_364 = L_363->get_getPos_0();
		NullCheck(L_364);
		int32_t L_365 = L_364->get_x_1();
		NullCheck(L_361);
		L_361->set_x_1(((int32_t)((int32_t)L_362+(int32_t)L_365)));
		Dictionary_2_t3873625228 * L_366 = __this->get_tabIdData_7();
		tabID_t570832297 * L_367 = V_13;
		NullCheck(L_367);
		int32_t L_368 = L_367->get_id_1();
		NullCheck(L_366);
		tabID_t570832297 * L_369 = Dictionary_2_get_Item_m89342720(L_366, L_368, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_369);
		tBase_t1720960579 * L_370 = L_369->get_getPos_0();
		tBase_t1720960579 * L_371 = L_370;
		NullCheck(L_371);
		int32_t L_372 = L_371->get_y_2();
		tabID_t570832297 * L_373 = V_13;
		NullCheck(L_373);
		tBase_t1720960579 * L_374 = L_373->get_getPos_0();
		NullCheck(L_374);
		int32_t L_375 = L_374->get_y_2();
		NullCheck(L_371);
		L_371->set_y_2(((int32_t)((int32_t)L_372+(int32_t)L_375)));
		Dictionary_2_t3873625228 * L_376 = __this->get_tabIdData_7();
		tabID_t570832297 * L_377 = V_13;
		NullCheck(L_377);
		int32_t L_378 = L_377->get_id_1();
		NullCheck(L_376);
		tabID_t570832297 * L_379 = Dictionary_2_get_Item_m89342720(L_376, L_378, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_379);
		tBase_t1720960579 * L_380 = L_379->get_getPos_0();
		tabID_t570832297 * L_381 = V_13;
		NullCheck(L_381);
		tBase_t1720960579 * L_382 = L_381->get_getPos_0();
		NullCheck(L_382);
		float L_383 = L_382->get_rotation_3();
		NullCheck(L_380);
		L_380->set_rotation_3(L_383);
		Dictionary_2_t3873625228 * L_384 = __this->get_tabIdData_7();
		tabID_t570832297 * L_385 = V_13;
		NullCheck(L_385);
		int32_t L_386 = L_385->get_id_1();
		NullCheck(L_384);
		tabID_t570832297 * L_387 = Dictionary_2_get_Item_m89342720(L_384, L_386, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_387);
		tBase_t1720960579 * L_388 = L_387->get_getPos_0();
		tabID_t570832297 * L_389 = V_13;
		NullCheck(L_389);
		tBase_t1720960579 * L_390 = L_389->get_getPos_0();
		NullCheck(L_390);
		bool L_391 = L_390->get_state_0();
		NullCheck(L_388);
		L_388->set_state_0(L_391);
		Dictionary_2_t3873625228 * L_392 = __this->get_tabIdData_7();
		tabID_t570832297 * L_393 = V_13;
		NullCheck(L_393);
		int32_t L_394 = L_393->get_id_1();
		NullCheck(L_392);
		tabID_t570832297 * L_395 = Dictionary_2_get_Item_m89342720(L_392, L_394, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		tabID_t570832297 * L_396 = L_395;
		NullCheck(L_396);
		int32_t L_397 = L_396->get_addCounter_3();
		NullCheck(L_396);
		L_396->set_addCounter_3(((int32_t)((int32_t)L_397+(int32_t)1)));
		Dictionary_2_t3873625228 * L_398 = __this->get_tabIdData_7();
		tabID_t570832297 * L_399 = V_13;
		NullCheck(L_399);
		int32_t L_400 = L_399->get_id_1();
		NullCheck(L_398);
		tabID_t570832297 * L_401 = Dictionary_2_get_Item_m89342720(L_398, L_400, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_401);
		L_401->set_removeCounter_4(0);
		Dictionary_2_t3873625228 * L_402 = __this->get_tabIdData_7();
		tabID_t570832297 * L_403 = V_13;
		NullCheck(L_403);
		int32_t L_404 = L_403->get_id_1();
		NullCheck(L_402);
		tabID_t570832297 * L_405 = Dictionary_2_get_Item_m89342720(L_402, L_404, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_405);
		L_405->set_changedData_2((bool)1);
		Dictionary_2_t3873625228 * L_406 = __this->get_tabIdData_7();
		tabID_t570832297 * L_407 = V_13;
		NullCheck(L_407);
		int32_t L_408 = L_407->get_id_1();
		NullCheck(L_406);
		tabID_t570832297 * L_409 = Dictionary_2_get_Item_m89342720(L_406, L_408, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		tabID_t570832297 * L_410 = L_409;
		NullCheck(L_410);
		int32_t L_411 = L_410->get_posAverageCounter_5();
		NullCheck(L_410);
		L_410->set_posAverageCounter_5(((int32_t)((int32_t)L_411+(int32_t)1)));
	}

IL_08e5:
	{
		int32_t L_412 = V_4;
		if ((!(((uint32_t)L_412) == ((uint32_t)((int32_t)25)))))
		{
			goto IL_0927;
		}
	}
	{
		int32_t L_413 = __this->get_buttonCounter_13();
		__this->set_buttonCounter_13(((int32_t)((int32_t)L_413+(int32_t)1)));
		int32_t L_414 = __this->get_buttonCounter_13();
		if ((!(((uint32_t)L_414) == ((uint32_t)2))))
		{
			goto IL_090e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->set_switch_state1_11(1);
	}

IL_090e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		int32_t L_415 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_switch_state1_11();
		int32_t L_416 = L_415;
		Il2CppObject * L_417 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_416);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_418 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral920280337, L_417, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_418, /*hidden argument*/NULL);
	}

IL_0927:
	{
		int32_t L_419 = V_4;
		if ((!(((uint32_t)L_419) == ((uint32_t)((int32_t)23)))))
		{
			goto IL_094f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->set_switch_state5_12(1);
		int32_t L_420 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_switch_state5_12();
		int32_t L_421 = L_420;
		Il2CppObject * L_422 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_421);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_423 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral920280205, L_422, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_423, /*hidden argument*/NULL);
	}

IL_094f:
	{
		goto IL_0b1e;
	}

IL_0954:
	{
		tabID_t570832297 * L_424 = V_13;
		int32_t L_425 = V_4;
		NullCheck(L_424);
		L_424->set_id_1(((int32_t)((int32_t)L_425/(int32_t)2)));
		Dictionary_2_t3873625228 * L_426 = __this->get_tabIdData_7();
		tabID_t570832297 * L_427 = V_13;
		NullCheck(L_427);
		int32_t L_428 = L_427->get_id_1();
		NullCheck(L_426);
		bool L_429 = Dictionary_2_ContainsKey_m2556299403(L_426, L_428, /*hidden argument*/Dictionary_2_ContainsKey_m2556299403_MethodInfo_var);
		if (!L_429)
		{
			goto IL_0b1e;
		}
	}
	{
		int32_t L_430 = V_4;
		if ((!(((uint32_t)L_430) == ((uint32_t)3))))
		{
			goto IL_09eb;
		}
	}
	{
		Dictionary_2_t2980096784 * L_431 = __this->get_idDictionary_14();
		NullCheck(L_431);
		bool L_432 = Dictionary_2_ContainsKey_m484542103(L_431, 3, /*hidden argument*/Dictionary_2_ContainsKey_m484542103_MethodInfo_var);
		if (!L_432)
		{
			goto IL_09ba;
		}
	}
	{
		Dictionary_2_t2980096784 * L_433 = __this->get_idDictionary_14();
		NullCheck(L_433);
		baseLegID_t3972271149 * L_434 = Dictionary_2_get_Item_m1525488644(L_433, 3, /*hidden argument*/Dictionary_2_get_Item_m1525488644_MethodInfo_var);
		int32_t L_435 = V_6;
		NullCheck(L_434);
		L_434->set_x_1(L_435);
		Dictionary_2_t2980096784 * L_436 = __this->get_idDictionary_14();
		NullCheck(L_436);
		baseLegID_t3972271149 * L_437 = Dictionary_2_get_Item_m1525488644(L_436, 3, /*hidden argument*/Dictionary_2_get_Item_m1525488644_MethodInfo_var);
		int32_t L_438 = V_9;
		NullCheck(L_437);
		L_437->set_y_2(L_438);
		goto IL_09eb;
	}

IL_09ba:
	{
		baseLegID_t3972271149 * L_439 = (baseLegID_t3972271149 *)il2cpp_codegen_object_new(baseLegID_t3972271149_il2cpp_TypeInfo_var);
		baseLegID__ctor_m693884990(L_439, /*hidden argument*/NULL);
		V_25 = L_439;
		baseLegID_t3972271149 * L_440 = V_25;
		int32_t L_441 = V_4;
		NullCheck(L_440);
		L_440->set_id_0(L_441);
		baseLegID_t3972271149 * L_442 = V_25;
		int32_t L_443 = V_6;
		NullCheck(L_442);
		L_442->set_x_1(L_443);
		baseLegID_t3972271149 * L_444 = V_25;
		int32_t L_445 = V_9;
		NullCheck(L_444);
		L_444->set_y_2(L_445);
		Dictionary_2_t2980096784 * L_446 = __this->get_idDictionary_14();
		int32_t L_447 = V_4;
		baseLegID_t3972271149 * L_448 = V_25;
		NullCheck(L_446);
		Dictionary_2_Add_m3202490836(L_446, L_447, L_448, /*hidden argument*/Dictionary_2_Add_m3202490836_MethodInfo_var);
	}

IL_09eb:
	{
		int32_t L_449 = V_4;
		if ((!(((uint32_t)L_449) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0a64;
		}
	}
	{
		Dictionary_2_t2980096784 * L_450 = __this->get_idDictionary_14();
		NullCheck(L_450);
		bool L_451 = Dictionary_2_ContainsKey_m484542103(L_450, ((int32_t)11), /*hidden argument*/Dictionary_2_ContainsKey_m484542103_MethodInfo_var);
		if (!L_451)
		{
			goto IL_0a33;
		}
	}
	{
		Dictionary_2_t2980096784 * L_452 = __this->get_idDictionary_14();
		NullCheck(L_452);
		baseLegID_t3972271149 * L_453 = Dictionary_2_get_Item_m1525488644(L_452, ((int32_t)11), /*hidden argument*/Dictionary_2_get_Item_m1525488644_MethodInfo_var);
		int32_t L_454 = V_6;
		NullCheck(L_453);
		L_453->set_x_1(L_454);
		Dictionary_2_t2980096784 * L_455 = __this->get_idDictionary_14();
		NullCheck(L_455);
		baseLegID_t3972271149 * L_456 = Dictionary_2_get_Item_m1525488644(L_455, ((int32_t)11), /*hidden argument*/Dictionary_2_get_Item_m1525488644_MethodInfo_var);
		int32_t L_457 = V_9;
		NullCheck(L_456);
		L_456->set_y_2(L_457);
		goto IL_0a64;
	}

IL_0a33:
	{
		baseLegID_t3972271149 * L_458 = (baseLegID_t3972271149 *)il2cpp_codegen_object_new(baseLegID_t3972271149_il2cpp_TypeInfo_var);
		baseLegID__ctor_m693884990(L_458, /*hidden argument*/NULL);
		V_26 = L_458;
		baseLegID_t3972271149 * L_459 = V_26;
		int32_t L_460 = V_4;
		NullCheck(L_459);
		L_459->set_id_0(L_460);
		baseLegID_t3972271149 * L_461 = V_26;
		int32_t L_462 = V_6;
		NullCheck(L_461);
		L_461->set_x_1(L_462);
		baseLegID_t3972271149 * L_463 = V_26;
		int32_t L_464 = V_9;
		NullCheck(L_463);
		L_463->set_y_2(L_464);
		Dictionary_2_t2980096784 * L_465 = __this->get_idDictionary_14();
		int32_t L_466 = V_4;
		baseLegID_t3972271149 * L_467 = V_26;
		NullCheck(L_465);
		Dictionary_2_Add_m3202490836(L_465, L_466, L_467, /*hidden argument*/Dictionary_2_Add_m3202490836_MethodInfo_var);
	}

IL_0a64:
	{
		Dictionary_2_t3873625228 * L_468 = __this->get_tabIdData_7();
		tabID_t570832297 * L_469 = V_13;
		NullCheck(L_469);
		int32_t L_470 = L_469->get_id_1();
		NullCheck(L_468);
		tabID_t570832297 * L_471 = Dictionary_2_get_Item_m89342720(L_468, L_470, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_471);
		tBase_t1720960579 * L_472 = L_471->get_getPos_0();
		NullCheck(L_472);
		bool L_473 = L_472->get_state_0();
		if (L_473)
		{
			goto IL_0acf;
		}
	}
	{
		Dictionary_2_t3873625228 * L_474 = __this->get_tabIdData_7();
		tabID_t570832297 * L_475 = V_13;
		NullCheck(L_475);
		int32_t L_476 = L_475->get_id_1();
		NullCheck(L_474);
		tabID_t570832297 * L_477 = Dictionary_2_get_Item_m89342720(L_474, L_476, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_477);
		tBase_t1720960579 * L_478 = L_477->get_getPos_0();
		tBase_t1720960579 * L_479 = L_478;
		NullCheck(L_479);
		int32_t L_480 = L_479->get_x_1();
		int32_t L_481 = V_6;
		NullCheck(L_479);
		L_479->set_x_1(((int32_t)((int32_t)L_480+(int32_t)L_481)));
		Dictionary_2_t3873625228 * L_482 = __this->get_tabIdData_7();
		tabID_t570832297 * L_483 = V_13;
		NullCheck(L_483);
		int32_t L_484 = L_483->get_id_1();
		NullCheck(L_482);
		tabID_t570832297 * L_485 = Dictionary_2_get_Item_m89342720(L_482, L_484, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_485);
		tBase_t1720960579 * L_486 = L_485->get_getPos_0();
		tBase_t1720960579 * L_487 = L_486;
		NullCheck(L_487);
		int32_t L_488 = L_487->get_y_2();
		int32_t L_489 = V_9;
		NullCheck(L_487);
		L_487->set_y_2(((int32_t)((int32_t)L_488+(int32_t)L_489)));
	}

IL_0acf:
	{
		Dictionary_2_t3873625228 * L_490 = __this->get_tabIdData_7();
		tabID_t570832297 * L_491 = V_13;
		NullCheck(L_491);
		int32_t L_492 = L_491->get_id_1();
		NullCheck(L_490);
		tabID_t570832297 * L_493 = Dictionary_2_get_Item_m89342720(L_490, L_492, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_493);
		L_493->set_changedData_2((bool)1);
		Dictionary_2_t3873625228 * L_494 = __this->get_tabIdData_7();
		tabID_t570832297 * L_495 = V_13;
		NullCheck(L_495);
		int32_t L_496 = L_495->get_id_1();
		NullCheck(L_494);
		tabID_t570832297 * L_497 = Dictionary_2_get_Item_m89342720(L_494, L_496, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		tabID_t570832297 * L_498 = L_497;
		NullCheck(L_498);
		int32_t L_499 = L_498->get_addCounter_3();
		NullCheck(L_498);
		L_498->set_addCounter_3(((int32_t)((int32_t)L_499+(int32_t)1)));
		Dictionary_2_t3873625228 * L_500 = __this->get_tabIdData_7();
		tabID_t570832297 * L_501 = V_13;
		NullCheck(L_501);
		int32_t L_502 = L_501->get_id_1();
		NullCheck(L_500);
		tabID_t570832297 * L_503 = Dictionary_2_get_Item_m89342720(L_500, L_502, /*hidden argument*/Dictionary_2_get_Item_m89342720_MethodInfo_var);
		NullCheck(L_503);
		L_503->set_removeCounter_4(0);
	}

IL_0b1e:
	{
		int32_t L_504 = V_12;
		V_12 = ((int32_t)((int32_t)L_504+(int32_t)1));
	}

IL_0b24:
	{
		int32_t L_505 = V_12;
		StringU5BU5D_t1642385972* L_506 = V_11;
		NullCheck(L_506);
		if ((((int32_t)L_505) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_506)->max_length)))))))
		{
			goto IL_00a6;
		}
	}
	{
		Dictionary_2_t3873625228 * L_507 = __this->get_tabIdData_7();
		NullCheck(L_507);
		Enumerator_t898682634  L_508 = Dictionary_2_GetEnumerator_m3590942114(L_507, /*hidden argument*/Dictionary_2_GetEnumerator_m3590942114_MethodInfo_var);
		V_28 = L_508;
	}

IL_0b3c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0d1b;
		}

IL_0b41:
		{
			KeyValuePair_2_t1630970450  L_509 = Enumerator_get_Current_m3707086780((&V_28), /*hidden argument*/Enumerator_get_Current_m3707086780_MethodInfo_var);
			V_27 = L_509;
			tabID_t570832297 * L_510 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_510);
			bool L_511 = L_510->get_changedData_2();
			if (L_511)
			{
				goto IL_0bd6;
			}
		}

IL_0b5b:
		{
			int32_t L_512 = __this->get_removeBaseCounter_4();
			tabID_t570832297 * L_513 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_513);
			int32_t L_514 = L_513->get_removeCounter_4();
			if ((((int32_t)L_512) >= ((int32_t)L_514)))
			{
				goto IL_0bb0;
			}
		}

IL_0b72:
		{
			tabID_t570832297 * L_515 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_515);
			tBase_t1720960579 * L_516 = L_515->get_getPos_0();
			NullCheck(L_516);
			L_516->set_state_0((bool)0);
			IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
			Dictionary_2_t728786214 * L_517 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_tabData_6();
			tabID_t570832297 * L_518 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_518);
			int32_t L_519 = L_518->get_id_1();
			NullCheck(L_517);
			tBase_t1720960579 * L_520 = Dictionary_2_get_Item_m283828154(L_517, L_519, /*hidden argument*/Dictionary_2_get_Item_m283828154_MethodInfo_var);
			tabID_t570832297 * L_521 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_521);
			tBase_t1720960579 * L_522 = L_521->get_getPos_0();
			NullCheck(L_522);
			bool L_523 = L_522->get_state_0();
			NullCheck(L_520);
			L_520->set_state_0(L_523);
		}

IL_0bb0:
		{
			tabID_t570832297 * L_524 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_524);
			L_524->set_addCounter_3(0);
			tabID_t570832297 * L_525 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			tabID_t570832297 * L_526 = L_525;
			NullCheck(L_526);
			int32_t L_527 = L_526->get_removeCounter_4();
			NullCheck(L_526);
			L_526->set_removeCounter_4(((int32_t)((int32_t)L_527+(int32_t)1)));
			goto IL_0c2b;
		}

IL_0bd6:
		{
			tabID_t570832297 * L_528 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_528);
			int32_t L_529 = L_528->get_addCounter_3();
			int32_t L_530 = __this->get_addBaseCounter_3();
			if ((((int32_t)L_529) < ((int32_t)L_530)))
			{
				goto IL_0c2b;
			}
		}

IL_0bed:
		{
			tabID_t570832297 * L_531 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_531);
			tBase_t1720960579 * L_532 = L_531->get_getPos_0();
			NullCheck(L_532);
			L_532->set_state_0((bool)1);
			IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
			Dictionary_2_t728786214 * L_533 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_tabData_6();
			tabID_t570832297 * L_534 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_534);
			int32_t L_535 = L_534->get_id_1();
			NullCheck(L_533);
			tBase_t1720960579 * L_536 = Dictionary_2_get_Item_m283828154(L_533, L_535, /*hidden argument*/Dictionary_2_get_Item_m283828154_MethodInfo_var);
			tabID_t570832297 * L_537 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_537);
			tBase_t1720960579 * L_538 = L_537->get_getPos_0();
			NullCheck(L_538);
			bool L_539 = L_538->get_state_0();
			NullCheck(L_536);
			L_536->set_state_0(L_539);
		}

IL_0c2b:
		{
			tabID_t570832297 * L_540 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_540);
			int32_t L_541 = L_540->get_posAverageCounter_5();
			int32_t L_542 = __this->get_PosAverageCounter_5();
			if ((((int32_t)L_541) < ((int32_t)L_542)))
			{
				goto IL_0d1b;
			}
		}

IL_0c42:
		{
			IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
			Dictionary_2_t728786214 * L_543 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_tabData_6();
			tabID_t570832297 * L_544 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_544);
			int32_t L_545 = L_544->get_id_1();
			NullCheck(L_543);
			tBase_t1720960579 * L_546 = Dictionary_2_get_Item_m283828154(L_543, L_545, /*hidden argument*/Dictionary_2_get_Item_m283828154_MethodInfo_var);
			tabID_t570832297 * L_547 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_547);
			tBase_t1720960579 * L_548 = L_547->get_getPos_0();
			NullCheck(L_548);
			int32_t L_549 = L_548->get_x_1();
			int32_t L_550 = __this->get_PosAverageCounter_5();
			NullCheck(L_546);
			L_546->set_x_1(((int32_t)((int32_t)L_549/(int32_t)L_550)));
			Dictionary_2_t728786214 * L_551 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_tabData_6();
			tabID_t570832297 * L_552 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_552);
			int32_t L_553 = L_552->get_id_1();
			NullCheck(L_551);
			tBase_t1720960579 * L_554 = Dictionary_2_get_Item_m283828154(L_551, L_553, /*hidden argument*/Dictionary_2_get_Item_m283828154_MethodInfo_var);
			tabID_t570832297 * L_555 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_555);
			tBase_t1720960579 * L_556 = L_555->get_getPos_0();
			NullCheck(L_556);
			int32_t L_557 = L_556->get_y_2();
			int32_t L_558 = __this->get_PosAverageCounter_5();
			NullCheck(L_554);
			L_554->set_y_2(((int32_t)((int32_t)L_557/(int32_t)L_558)));
			Dictionary_2_t728786214 * L_559 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_tabData_6();
			tabID_t570832297 * L_560 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_560);
			int32_t L_561 = L_560->get_id_1();
			NullCheck(L_559);
			tBase_t1720960579 * L_562 = Dictionary_2_get_Item_m283828154(L_559, L_561, /*hidden argument*/Dictionary_2_get_Item_m283828154_MethodInfo_var);
			tabID_t570832297 * L_563 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_563);
			tBase_t1720960579 * L_564 = L_563->get_getPos_0();
			NullCheck(L_564);
			float L_565 = L_564->get_rotation_3();
			NullCheck(L_562);
			L_562->set_rotation_3(L_565);
			tabID_t570832297 * L_566 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_566);
			tBase_t1720960579 * L_567 = L_566->get_getPos_0();
			NullCheck(L_567);
			L_567->set_x_1(0);
			tabID_t570832297 * L_568 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_568);
			tBase_t1720960579 * L_569 = L_568->get_getPos_0();
			NullCheck(L_569);
			L_569->set_y_2(0);
			tabID_t570832297 * L_570 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_570);
			tBase_t1720960579 * L_571 = L_570->get_getPos_0();
			NullCheck(L_571);
			L_571->set_rotation_3((0.0f));
			tabID_t570832297 * L_572 = KeyValuePair_2_get_Value_m2891962645((&V_27), /*hidden argument*/KeyValuePair_2_get_Value_m2891962645_MethodInfo_var);
			NullCheck(L_572);
			L_572->set_posAverageCounter_5(0);
		}

IL_0d1b:
		{
			bool L_573 = Enumerator_MoveNext_m3020128755((&V_28), /*hidden argument*/Enumerator_MoveNext_m3020128755_MethodInfo_var);
			if (L_573)
			{
				goto IL_0b41;
			}
		}

IL_0d27:
		{
			IL2CPP_LEAVE(0xD3A, FINALLY_0d2c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0d2c;
	}

FINALLY_0d2c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3269019933((&V_28), /*hidden argument*/Enumerator_Dispose_m3269019933_MethodInfo_var);
		IL2CPP_END_FINALLY(3372)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(3372)
	{
		IL2CPP_JUMP_TBL(0xD3A, IL_0d3a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0d3a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->set_dataState_8((bool)1);
		int32_t L_574 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get__uniqID_10();
		((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->set__uniqID_10(((int32_t)((int32_t)L_574+(int32_t)1)));
	}

IL_0d4c:
	{
		return;
	}
}
// System.Boolean TabBase::checkDataState()
extern Il2CppClass* TabBase_t319583306_il2cpp_TypeInfo_var;
extern const uint32_t TabBase_checkDataState_m3036812730_MetadataUsageId;
extern "C"  bool TabBase_checkDataState_m3036812730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabBase_checkDataState_m3036812730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		bool L_0 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_dataState_8();
		return L_0;
	}
}
// System.Boolean TabBase::getDataState()
extern Il2CppClass* TabBase_t319583306_il2cpp_TypeInfo_var;
extern const uint32_t TabBase_getDataState_m1438652662_MetadataUsageId;
extern "C"  bool TabBase_getDataState_m1438652662 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabBase_getDataState_m1438652662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		bool L_0 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_dataState_8();
		return L_0;
	}
}
// System.Int32 TabBase::get_uniqId()
extern Il2CppClass* TabBase_t319583306_il2cpp_TypeInfo_var;
extern const uint32_t TabBase_get_uniqId_m1739931730_MetadataUsageId;
extern "C"  int32_t TabBase_get_uniqId_m1739931730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabBase_get_uniqId_m1739931730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		int32_t L_0 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get__uniqID_10();
		return L_0;
	}
}
// System.Int32 TabBase::GetCount()
extern Il2CppClass* TabBase_t319583306_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t728786214_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3880445430_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m779849068_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m12816146_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m4253762491_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m276666470_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m420253381_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3027320499_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029311;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern Il2CppCodeGenString* _stringLiteral51790588;
extern const uint32_t TabBase_GetCount_m1111189072_MetadataUsageId;
extern "C"  int32_t TabBase_GetCount_m1111189072 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabBase_GetCount_m1111189072_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t728786214 * V_0 = NULL;
	int32_t V_1 = 0;
	KeyValuePair_2_t2781098732  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t2048810916  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		int32_t L_0 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_lastUniqId_18();
		int32_t L_1 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get__uniqID_10();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_011d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		int32_t L_2 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get__uniqID_10();
		((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->set_lastUniqId_18(L_2);
		Dictionary_2_t728786214 * L_3 = (Dictionary_2_t728786214 *)il2cpp_codegen_object_new(Dictionary_2_t728786214_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3880445430(L_3, /*hidden argument*/Dictionary_2__ctor_m3880445430_MethodInfo_var);
		V_0 = L_3;
		Dictionary_2_t728786214 * L_4 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_tabData_6();
		V_0 = L_4;
		V_1 = 0;
		((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->set_testID_9(_stringLiteral372029311);
		Dictionary_2_t728786214 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_010d;
		}
	}
	{
		Dictionary_2_t728786214 * L_6 = V_0;
		NullCheck(L_6);
		Enumerator_t2048810916  L_7 = Dictionary_2_GetEnumerator_m779849068(L_6, /*hidden argument*/Dictionary_2_GetEnumerator_m779849068_MethodInfo_var);
		V_3 = L_7;
	}

IL_003e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00ee;
		}

IL_0043:
		{
			KeyValuePair_2_t2781098732  L_8 = Enumerator_get_Current_m12816146((&V_3), /*hidden argument*/Enumerator_get_Current_m12816146_MethodInfo_var);
			V_2 = L_8;
			tBase_t1720960579 * L_9 = KeyValuePair_2_get_Value_m4253762491((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m4253762491_MethodInfo_var);
			NullCheck(L_9);
			bool L_10 = L_9->get_state_0();
			if (!L_10)
			{
				goto IL_00ee;
			}
		}

IL_005c:
		{
			int32_t L_11 = V_1;
			V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
			ObjectU5BU5D_t3614634134* L_12 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
			IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
			String_t* L_13 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_testID_9();
			NullCheck(L_12);
			ArrayElementTypeCheck (L_12, L_13);
			(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_13);
			ObjectU5BU5D_t3614634134* L_14 = L_12;
			NullCheck(L_14);
			ArrayElementTypeCheck (L_14, _stringLiteral372029310);
			(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral372029310);
			ObjectU5BU5D_t3614634134* L_15 = L_14;
			int32_t L_16 = KeyValuePair_2_get_Key_m276666470((&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m276666470_MethodInfo_var);
			int32_t L_17 = L_16;
			Il2CppObject * L_18 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_17);
			NullCheck(L_15);
			ArrayElementTypeCheck (L_15, L_18);
			(L_15)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_18);
			ObjectU5BU5D_t3614634134* L_19 = L_15;
			NullCheck(L_19);
			ArrayElementTypeCheck (L_19, _stringLiteral372029310);
			(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral372029310);
			ObjectU5BU5D_t3614634134* L_20 = L_19;
			tBase_t1720960579 * L_21 = KeyValuePair_2_get_Value_m4253762491((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m4253762491_MethodInfo_var);
			NullCheck(L_21);
			int32_t L_22 = L_21->get_x_1();
			int32_t L_23 = L_22;
			Il2CppObject * L_24 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_23);
			NullCheck(L_20);
			ArrayElementTypeCheck (L_20, L_24);
			(L_20)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_24);
			ObjectU5BU5D_t3614634134* L_25 = L_20;
			NullCheck(L_25);
			ArrayElementTypeCheck (L_25, _stringLiteral372029310);
			(L_25)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral372029310);
			ObjectU5BU5D_t3614634134* L_26 = L_25;
			tBase_t1720960579 * L_27 = KeyValuePair_2_get_Value_m4253762491((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m4253762491_MethodInfo_var);
			NullCheck(L_27);
			int32_t L_28 = L_27->get_y_2();
			int32_t L_29 = L_28;
			Il2CppObject * L_30 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_29);
			NullCheck(L_26);
			ArrayElementTypeCheck (L_26, L_30);
			(L_26)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_30);
			ObjectU5BU5D_t3614634134* L_31 = L_26;
			NullCheck(L_31);
			ArrayElementTypeCheck (L_31, _stringLiteral372029310);
			(L_31)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)_stringLiteral372029310);
			ObjectU5BU5D_t3614634134* L_32 = L_31;
			tBase_t1720960579 * L_33 = KeyValuePair_2_get_Value_m4253762491((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m4253762491_MethodInfo_var);
			NullCheck(L_33);
			float L_34 = L_33->get_rotation_3();
			int32_t L_35 = (((int32_t)((int32_t)L_34)));
			Il2CppObject * L_36 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_35);
			NullCheck(L_32);
			ArrayElementTypeCheck (L_32, L_36);
			(L_32)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_36);
			ObjectU5BU5D_t3614634134* L_37 = L_32;
			NullCheck(L_37);
			ArrayElementTypeCheck (L_37, _stringLiteral51790588);
			(L_37)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)_stringLiteral51790588);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_38 = String_Concat_m3881798623(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
			((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->set_testID_9(L_38);
		}

IL_00ee:
		{
			bool L_39 = Enumerator_MoveNext_m420253381((&V_3), /*hidden argument*/Enumerator_MoveNext_m420253381_MethodInfo_var);
			if (L_39)
			{
				goto IL_0043;
			}
		}

IL_00fa:
		{
			IL2CPP_LEAVE(0x10D, FINALLY_00ff);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00ff;
	}

FINALLY_00ff:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3027320499((&V_3), /*hidden argument*/Enumerator_Dispose_m3027320499_MethodInfo_var);
		IL2CPP_END_FINALLY(255)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(255)
	{
		IL2CPP_JUMP_TBL(0x10D, IL_010d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_010d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		String_t* L_40 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_testID_9();
		((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->set_lastTestId_20(L_40);
		int32_t L_41 = V_1;
		((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->set_lastCount_19(L_41);
	}

IL_011d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		int32_t L_42 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_lastCount_19();
		return L_42;
	}
}
// System.Collections.Generic.Dictionary`2<System.Int32,tBase> TabBase::GetBase()
extern Il2CppClass* TabBase_t319583306_il2cpp_TypeInfo_var;
extern const uint32_t TabBase_GetBase_m3096554348_MetadataUsageId;
extern "C"  Dictionary_2_t728786214 * TabBase_GetBase_m3096554348 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabBase_GetBase_m3096554348_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		Dictionary_2_t728786214 * L_0 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_tabData_6();
		return L_0;
	}
}
// System.Void TabBase::SendData(System.String)
extern Il2CppClass* testBle_t3025851879_il2cpp_TypeInfo_var;
extern const uint32_t TabBase_SendData_m1632484299_MetadataUsageId;
extern "C"  void TabBase_SendData_m1632484299 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabBase_SendData_m1632484299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		testBle_SendString_m3395385065(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TabBase::CloseTabToy()
extern Il2CppClass* TabBase_t319583306_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3033369902;
extern const uint32_t TabBase_CloseTabToy_m99423136_MetadataUsageId;
extern "C"  void TabBase_CloseTabToy_m99423136 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabBase_CloseTabToy_m99423136_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		TabBase_SendData_m1632484299(NULL /*static, unused*/, _stringLiteral3033369902, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TabBase::CreateId()
extern Il2CppClass* tBase_t1720960579_il2cpp_TypeInfo_var;
extern Il2CppClass* tabID_t570832297_il2cpp_TypeInfo_var;
extern Il2CppClass* TabBase_t319583306_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_Add_m4159519998_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m645565480_MethodInfo_var;
extern const uint32_t TabBase_CreateId_m2675960116_MetadataUsageId;
extern "C"  void TabBase_CreateId_m2675960116 (TabBase_t319583306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabBase_CreateId_m2675960116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	tBase_t1720960579 * V_1 = NULL;
	tabID_t570832297 * V_2 = NULL;
	{
		V_0 = 1;
		goto IL_00a0;
	}

IL_0007:
	{
		tBase_t1720960579 * L_0 = (tBase_t1720960579 *)il2cpp_codegen_object_new(tBase_t1720960579_il2cpp_TypeInfo_var);
		tBase__ctor_m4144850876(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		tabID_t570832297 * L_1 = (tabID_t570832297 *)il2cpp_codegen_object_new(tabID_t570832297_il2cpp_TypeInfo_var);
		tabID__ctor_m1405518218(L_1, /*hidden argument*/NULL);
		V_2 = L_1;
		tBase_t1720960579 * L_2 = V_1;
		NullCheck(L_2);
		L_2->set_state_0((bool)0);
		tBase_t1720960579 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_x_1(0);
		tBase_t1720960579 * L_4 = V_1;
		NullCheck(L_4);
		L_4->set_y_2(0);
		tBase_t1720960579 * L_5 = V_1;
		NullCheck(L_5);
		L_5->set_rotation_3((0.0f));
		tabID_t570832297 * L_6 = V_2;
		NullCheck(L_6);
		L_6->set_addCounter_3(0);
		tabID_t570832297 * L_7 = V_2;
		NullCheck(L_7);
		L_7->set_removeCounter_4(0);
		tabID_t570832297 * L_8 = V_2;
		NullCheck(L_8);
		tBase_t1720960579 * L_9 = L_8->get_getPos_0();
		NullCheck(L_9);
		L_9->set_rotation_3((0.0f));
		tabID_t570832297 * L_10 = V_2;
		NullCheck(L_10);
		tBase_t1720960579 * L_11 = L_10->get_getPos_0();
		NullCheck(L_11);
		L_11->set_y_2(0);
		tabID_t570832297 * L_12 = V_2;
		NullCheck(L_12);
		tBase_t1720960579 * L_13 = L_12->get_getPos_0();
		NullCheck(L_13);
		L_13->set_x_1(0);
		tabID_t570832297 * L_14 = V_2;
		NullCheck(L_14);
		tBase_t1720960579 * L_15 = L_14->get_getPos_0();
		NullCheck(L_15);
		L_15->set_state_0((bool)0);
		tabID_t570832297 * L_16 = V_2;
		NullCheck(L_16);
		L_16->set_changedData_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		Dictionary_2_t728786214 * L_17 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_tabData_6();
		int32_t L_18 = V_0;
		tBase_t1720960579 * L_19 = V_1;
		NullCheck(L_17);
		Dictionary_2_Add_m4159519998(L_17, L_18, L_19, /*hidden argument*/Dictionary_2_Add_m4159519998_MethodInfo_var);
		tabID_t570832297 * L_20 = V_2;
		int32_t L_21 = V_0;
		NullCheck(L_20);
		L_20->set_id_1(L_21);
		Dictionary_2_t3873625228 * L_22 = __this->get_tabIdData_7();
		int32_t L_23 = V_0;
		tabID_t570832297 * L_24 = V_2;
		NullCheck(L_22);
		Dictionary_2_Add_m645565480(L_22, L_23, L_24, /*hidden argument*/Dictionary_2_Add_m645565480_MethodInfo_var);
		int32_t L_25 = V_0;
		V_0 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_00a0:
	{
		int32_t L_26 = V_0;
		int32_t L_27 = __this->get_idSize_2();
		if ((((int32_t)L_26) <= ((int32_t)L_27)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TabBase::OnGUI()
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* TabBase_t319583306_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral816986750;
extern Il2CppCodeGenString* _stringLiteral3153430280;
extern const uint32_t TabBase_OnGUI_m2384831551_MetadataUsageId;
extern "C"  void TabBase_OnGUI_m2384831551 (TabBase_t319583306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabBase_OnGUI_m2384831551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Rect__ctor_m1220545469(&L_1, (((float)((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)200)))))), (0.0f), (200.0f), (200.0f), /*hidden argument*/NULL);
		GUILayout_BeginArea_m3297699023(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_2 = __this->get_myStyle_22();
		GUILayoutOptionU5BU5D_t2108882777* L_3 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		GUILayoutOption_t4183744904 * L_4 = GUILayout_Height_m607115982(NULL /*static, unused*/, (200.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_4);
		GUILayoutOptionU5BU5D_t2108882777* L_5 = L_3;
		GUILayoutOption_t4183744904 * L_6 = GUILayout_Width_m261136689(NULL /*static, unused*/, (200.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_6);
		bool L_7 = GUILayout_Button_m2061694122(NULL /*static, unused*/, _stringLiteral816986750, L_2, L_5, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0069;
		}
	}
	{
		bool L_8 = __this->get_button_state_21();
		__this->set_button_state_21((bool)((((int32_t)L_8) == ((int32_t)0))? 1 : 0));
	}

IL_0069:
	{
		GUILayout_EndArea_m1904221074(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_9 = __this->get_button_state_21();
		if (!L_9)
		{
			goto IL_00b2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		int32_t L_10 = TabBase_GetCount_m1111189072(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3153430280, L_12, /*hidden argument*/NULL);
		bool L_14 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_13, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_009d;
		}
	}

IL_009d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TabBase_t319583306_il2cpp_TypeInfo_var);
		String_t* L_15 = ((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->get_lastTestId_20();
		bool L_16 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_15, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00b2;
		}
	}

IL_00b2:
	{
		return;
	}
}
// System.Void TabBase::.cctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TabBase_t319583306_il2cpp_TypeInfo_var;
extern const uint32_t TabBase__cctor_m3860335284_MetadataUsageId;
extern "C"  void TabBase__cctor_m3860335284 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabBase__cctor_m3860335284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		((TabBase_t319583306_StaticFields*)TabBase_t319583306_il2cpp_TypeInfo_var->static_fields)->set_lastTestId_20(L_0);
		return;
	}
}
// System.Void TabBase/baseLegID::.ctor()
extern "C"  void baseLegID__ctor_m693884990 (baseLegID_t3972271149 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TabBase/tabID::.ctor()
extern Il2CppClass* tBase_t1720960579_il2cpp_TypeInfo_var;
extern const uint32_t tabID__ctor_m1405518218_MetadataUsageId;
extern "C"  void tabID__ctor_m1405518218 (tabID_t570832297 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tabID__ctor_m1405518218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		tBase_t1720960579 * L_0 = (tBase_t1720960579 *)il2cpp_codegen_object_new(tBase_t1720960579_il2cpp_TypeInfo_var);
		tBase__ctor_m4144850876(L_0, /*hidden argument*/NULL);
		__this->set_getPos_0(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void tBase::.ctor()
extern "C"  void tBase__ctor_m4144850876 (tBase_t1720960579 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testBle::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1979154176;
extern Il2CppCodeGenString* _stringLiteral2161142631;
extern const uint32_t testBle__ctor_m2665862840_MetadataUsageId;
extern "C"  void testBle__ctor_m2665862840 (testBle_t3025851879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle__ctor_m2665862840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set__readCharacteristicUUID_5(_stringLiteral1979154176);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__address_6(L_0);
		__this->set__name_7(_stringLiteral2161142631);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_BleDataNew_9(L_1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testBle::Start()
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern Il2CppClass* testBle_t3025851879_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1831019615_il2cpp_TypeInfo_var;
extern const MethodInfo* testBle_U3CStartU3Em__0_m1223780933_MethodInfo_var;
extern const MethodInfo* testBle_U3CStartU3Em__1_m3580770748_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m4012585251_MethodInfo_var;
extern const uint32_t testBle_Start_m1342812652_MetadataUsageId;
extern "C"  void testBle_Start_m1342812652 (testBle_t3025851879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_Start_m1342812652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_t3226471752 * G_B2_0 = NULL;
	int32_t G_B2_1 = 0;
	int32_t G_B2_2 = 0;
	Action_t3226471752 * G_B1_0 = NULL;
	int32_t G_B1_1 = 0;
	int32_t G_B1_2 = 0;
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)testBle_U3CStartU3Em__0_m1223780933_MethodInfo_var);
		Action_t3226471752 * L_1 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		Action_1_t1831019615 * L_2 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_13();
		G_B1_0 = L_1;
		G_B1_1 = 0;
		G_B1_2 = 1;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = 0;
			G_B2_2 = 1;
			goto IL_0026;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)testBle_U3CStartU3Em__1_m3580770748_MethodInfo_var);
		Action_1_t1831019615 * L_4 = (Action_1_t1831019615 *)il2cpp_codegen_object_new(Action_1_t1831019615_il2cpp_TypeInfo_var);
		Action_1__ctor_m4012585251(L_4, NULL, L_3, /*hidden argument*/Action_1__ctor_m4012585251_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_13(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		Action_1_t1831019615 * L_5 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_13();
		BluetoothLEHardwareInterface_Initialize_m3691300628(NULL /*static, unused*/, (bool)G_B2_2, (bool)G_B2_1, G_B2_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testBle::Connect()
extern Il2CppClass* testBle_t3025851879_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1831019615_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t4234541925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3256166369_il2cpp_TypeInfo_var;
extern const MethodInfo* testBle_U3CConnectU3Em__2_m586010835_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m4012585251_MethodInfo_var;
extern const MethodInfo* testBle_U3CConnectU3Em__3_m4101113984_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m2597838752_MethodInfo_var;
extern const MethodInfo* testBle_U3CConnectU3Em__4_m2263236401_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m458181345_MethodInfo_var;
extern const MethodInfo* testBle_U3CConnectU3Em__5_m978557238_MethodInfo_var;
extern const uint32_t testBle_Connect_m3205883302_MetadataUsageId;
extern "C"  void testBle_Connect_m3205883302 (testBle_t3025851879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_Connect_m3205883302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	Action_1_t1831019615 * G_B4_0 = NULL;
	String_t* G_B4_1 = NULL;
	Action_1_t1831019615 * G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	{
		String_t* L_0 = __this->get__address_6();
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		Action_1_t1831019615 * L_1 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_14();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)testBle_U3CConnectU3Em__2_m586010835_MethodInfo_var);
		Action_1_t1831019615 * L_3 = (Action_1_t1831019615 *)il2cpp_codegen_object_new(Action_1_t1831019615_il2cpp_TypeInfo_var);
		Action_1__ctor_m4012585251(L_3, NULL, L_2, /*hidden argument*/Action_1__ctor_m4012585251_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_14(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		Action_1_t1831019615 * L_4 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_14();
		Action_2_t4234541925 * L_5 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_15();
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
		if (L_5)
		{
			G_B4_0 = L_4;
			G_B4_1 = G_B2_0;
			goto IL_003b;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)testBle_U3CConnectU3Em__3_m4101113984_MethodInfo_var);
		Action_2_t4234541925 * L_7 = (Action_2_t4234541925 *)il2cpp_codegen_object_new(Action_2_t4234541925_il2cpp_TypeInfo_var);
		Action_2__ctor_m2597838752(L_7, NULL, L_6, /*hidden argument*/Action_2__ctor_m2597838752_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_15(L_7);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_003b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		Action_2_t4234541925 * L_8 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_15();
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)testBle_U3CConnectU3Em__4_m2263236401_MethodInfo_var);
		Action_3_t3256166369 * L_10 = (Action_3_t3256166369 *)il2cpp_codegen_object_new(Action_3_t3256166369_il2cpp_TypeInfo_var);
		Action_3__ctor_m458181345(L_10, __this, L_9, /*hidden argument*/Action_3__ctor_m458181345_MethodInfo_var);
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)testBle_U3CConnectU3Em__5_m978557238_MethodInfo_var);
		Action_1_t1831019615 * L_12 = (Action_1_t1831019615 *)il2cpp_codegen_object_new(Action_1_t1831019615_il2cpp_TypeInfo_var);
		Action_1__ctor_m4012585251(L_12, __this, L_11, /*hidden argument*/Action_1__ctor_m4012585251_MethodInfo_var);
		BluetoothLEHardwareInterface_ConnectToPeripheral_m2997112613(NULL /*static, unused*/, G_B4_1, G_B4_0, L_8, L_10, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean testBle::IsEqual(System.String,System.String)
extern "C"  bool testBle_IsEqual_m3590934016 (testBle_t3025851879 * __this, String_t* ___uuid10, String_t* ___uuid21, const MethodInfo* method)
{
	{
		String_t* L_0 = ___uuid10;
		NullCheck(L_0);
		String_t* L_1 = String_ToUpper_m3715743312(L_0, /*hidden argument*/NULL);
		String_t* L_2 = ___uuid21;
		NullCheck(L_2);
		String_t* L_3 = String_ToUpper_m3715743312(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_4 = String_CompareTo_m3879609894(L_1, L_3, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void testBle::SearchAndConnectDevice()
extern Il2CppClass* Action_2_t4234541925_il2cpp_TypeInfo_var;
extern Il2CppClass* testBle_t3025851879_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_4_t2314457268_il2cpp_TypeInfo_var;
extern const MethodInfo* testBle_U3CSearchAndConnectDeviceU3Em__6_m3612029114_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m2597838752_MethodInfo_var;
extern const MethodInfo* testBle_U3CSearchAndConnectDeviceU3Em__7_m1789553411_MethodInfo_var;
extern const MethodInfo* Action_4__ctor_m940517352_MethodInfo_var;
extern const uint32_t testBle_SearchAndConnectDevice_m66085037_MetadataUsageId;
extern "C"  void testBle_SearchAndConnectDevice_m66085037 (testBle_t3025851879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_SearchAndConnectDevice_m66085037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_t4234541925 * G_B2_0 = NULL;
	Il2CppObject * G_B2_1 = NULL;
	Action_2_t4234541925 * G_B1_0 = NULL;
	Il2CppObject * G_B1_1 = NULL;
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)testBle_U3CSearchAndConnectDeviceU3Em__6_m3612029114_MethodInfo_var);
		Action_2_t4234541925 * L_1 = (Action_2_t4234541925 *)il2cpp_codegen_object_new(Action_2_t4234541925_il2cpp_TypeInfo_var);
		Action_2__ctor_m2597838752(L_1, __this, L_0, /*hidden argument*/Action_2__ctor_m2597838752_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		Action_4_t2314457268 * L_2 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_16();
		G_B1_0 = L_1;
		G_B1_1 = NULL;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = NULL;
			goto IL_0025;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)testBle_U3CSearchAndConnectDeviceU3Em__7_m1789553411_MethodInfo_var);
		Action_4_t2314457268 * L_4 = (Action_4_t2314457268 *)il2cpp_codegen_object_new(Action_4_t2314457268_il2cpp_TypeInfo_var);
		Action_4__ctor_m940517352(L_4, NULL, L_3, /*hidden argument*/Action_4__ctor_m940517352_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_16(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		Action_4_t2314457268 * L_5 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_16();
		BluetoothLEHardwareInterface_ScanForPeripheralsWithServices_m3085173196(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)(StringU5BU5D_t1642385972*)G_B2_1, G_B2_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.String testBle::getBleData()
extern Il2CppClass* testBle_t3025851879_il2cpp_TypeInfo_var;
extern const uint32_t testBle_getBleData_m3947189216_MetadataUsageId;
extern "C"  String_t* testBle_getBleData_m3947189216 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_getBleData_m3947189216_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		String_t* L_0 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get_BleDataDone_8();
		return L_0;
	}
}
// System.Int32 testBle::get_uniqId()
extern Il2CppClass* testBle_t3025851879_il2cpp_TypeInfo_var;
extern const uint32_t testBle_get_uniqId_m3823478467_MetadataUsageId;
extern "C"  int32_t testBle_get_uniqId_m3823478467 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_get_uniqId_m3823478467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		int32_t L_0 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get__uniqID_12();
		return L_0;
	}
}
// System.String testBle::changeDataFormat(System.Byte[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t672740780_il2cpp_TypeInfo_var;
extern Il2CppClass* baseLegID_t1303619648_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3414161453_MethodInfo_var;
extern const MethodInfo* List_1_Add_m768752313_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3145487364_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1911082406_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2079664556_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1649105920_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern Il2CppCodeGenString* _stringLiteral372029311;
extern const uint32_t testBle_changeDataFormat_m2934963405_MetadataUsageId;
extern "C"  String_t* testBle_changeDataFormat_m2934963405 (testBle_t3025851879 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_changeDataFormat_m2934963405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	baseLegID_t1303619648 * V_2 = NULL;
	uint8_t V_3 = 0x0;
	ByteU5BU5D_t3397334013* V_4 = NULL;
	int32_t V_5 = 0;
	bool V_6 = false;
	baseLegID_t1303619648 * V_7 = NULL;
	Enumerator_t207470454  V_8;
	memset(&V_8, 0, sizeof(V_8));
	baseLegID_t1303619648 * V_9 = NULL;
	Enumerator_t207470454  V_10;
	memset(&V_10, 0, sizeof(V_10));
	String_t* V_11 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		V_1 = 0;
		List_1_t672740780 * L_1 = (List_1_t672740780 *)il2cpp_codegen_object_new(List_1_t672740780_il2cpp_TypeInfo_var);
		List_1__ctor_m3414161453(L_1, /*hidden argument*/List_1__ctor_m3414161453_MethodInfo_var);
		__this->set_idList_11(L_1);
		baseLegID_t1303619648 * L_2 = (baseLegID_t1303619648 *)il2cpp_codegen_object_new(baseLegID_t1303619648_il2cpp_TypeInfo_var);
		baseLegID__ctor_m525806059(L_2, /*hidden argument*/NULL);
		V_2 = L_2;
		ByteU5BU5D_t3397334013* L_3 = ___data0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_00a7;
	}

IL_0024:
	{
		ByteU5BU5D_t3397334013* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		uint8_t L_8 = V_3;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)255)))))
		{
			goto IL_0041;
		}
	}
	{
		__this->set_dataState_10((bool)1);
		goto IL_00a1;
	}

IL_0041:
	{
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
		int32_t L_10 = V_1;
		if (((int32_t)((int32_t)L_10-(int32_t)1)) == 0)
		{
			goto IL_005e;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)1)) == 1)
		{
			goto IL_006a;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)1)) == 2)
		{
			goto IL_0078;
		}
	}
	{
		goto IL_009a;
	}

IL_005e:
	{
		baseLegID_t1303619648 * L_11 = V_2;
		uint8_t L_12 = V_3;
		NullCheck(L_11);
		L_11->set_id_0(L_12);
		goto IL_00a1;
	}

IL_006a:
	{
		baseLegID_t1303619648 * L_13 = V_2;
		uint8_t L_14 = V_3;
		NullCheck(L_13);
		L_13->set_x_1(((int32_t)((int32_t)L_14*(int32_t)6)));
		goto IL_00a1;
	}

IL_0078:
	{
		baseLegID_t1303619648 * L_15 = V_2;
		uint8_t L_16 = V_3;
		NullCheck(L_15);
		L_15->set_y_2(((int32_t)((int32_t)L_16*(int32_t)4)));
		List_1_t672740780 * L_17 = __this->get_idList_11();
		baseLegID_t1303619648 * L_18 = V_2;
		NullCheck(L_17);
		List_1_Add_m768752313(L_17, L_18, /*hidden argument*/List_1_Add_m768752313_MethodInfo_var);
		V_1 = 0;
		baseLegID_t1303619648 * L_19 = (baseLegID_t1303619648 *)il2cpp_codegen_object_new(baseLegID_t1303619648_il2cpp_TypeInfo_var);
		baseLegID__ctor_m525806059(L_19, /*hidden argument*/NULL);
		V_2 = L_19;
		goto IL_00a1;
	}

IL_009a:
	{
		V_1 = 0;
		goto IL_00a1;
	}

IL_00a1:
	{
		int32_t L_20 = V_5;
		V_5 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00a7:
	{
		int32_t L_21 = V_5;
		ByteU5BU5D_t3397334013* L_22 = V_4;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		V_6 = (bool)0;
		List_1_t672740780 * L_23 = __this->get_idList_11();
		NullCheck(L_23);
		Enumerator_t207470454  L_24 = List_1_GetEnumerator_m3145487364(L_23, /*hidden argument*/List_1_GetEnumerator_m3145487364_MethodInfo_var);
		V_8 = L_24;
	}

IL_00c2:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0278;
		}

IL_00c7:
		{
			baseLegID_t1303619648 * L_25 = Enumerator_get_Current_m1911082406((&V_8), /*hidden argument*/Enumerator_get_Current_m1911082406_MethodInfo_var);
			V_7 = L_25;
			V_6 = (bool)0;
			baseLegID_t1303619648 * L_26 = V_7;
			NullCheck(L_26);
			int32_t L_27 = L_26->get_id_0();
			if (((int32_t)((int32_t)L_27%(int32_t)2)))
			{
				goto IL_020d;
			}
		}

IL_00e1:
		{
			List_1_t672740780 * L_28 = __this->get_idList_11();
			NullCheck(L_28);
			Enumerator_t207470454  L_29 = List_1_GetEnumerator_m3145487364(L_28, /*hidden argument*/List_1_GetEnumerator_m3145487364_MethodInfo_var);
			V_10 = L_29;
		}

IL_00ee:
		try
		{ // begin try (depth: 2)
			{
				goto IL_017f;
			}

IL_00f3:
			{
				baseLegID_t1303619648 * L_30 = Enumerator_get_Current_m1911082406((&V_10), /*hidden argument*/Enumerator_get_Current_m1911082406_MethodInfo_var);
				V_9 = L_30;
				baseLegID_t1303619648 * L_31 = V_7;
				NullCheck(L_31);
				int32_t L_32 = L_31->get_id_0();
				baseLegID_t1303619648 * L_33 = V_9;
				NullCheck(L_33);
				int32_t L_34 = L_33->get_id_0();
				if ((!(((uint32_t)((int32_t)((int32_t)L_32+(int32_t)1))) == ((uint32_t)L_34))))
				{
					goto IL_017f;
				}
			}

IL_0111:
			{
				String_t* L_35 = V_0;
				V_11 = L_35;
				StringU5BU5D_t1642385972* L_36 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)7));
				String_t* L_37 = V_11;
				NullCheck(L_36);
				ArrayElementTypeCheck (L_36, L_37);
				(L_36)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_37);
				StringU5BU5D_t1642385972* L_38 = L_36;
				baseLegID_t1303619648 * L_39 = V_7;
				NullCheck(L_39);
				int32_t* L_40 = L_39->get_address_of_id_0();
				String_t* L_41 = Int32_ToString_m2960866144(L_40, /*hidden argument*/NULL);
				NullCheck(L_38);
				ArrayElementTypeCheck (L_38, L_41);
				(L_38)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_41);
				StringU5BU5D_t1642385972* L_42 = L_38;
				NullCheck(L_42);
				ArrayElementTypeCheck (L_42, _stringLiteral372029310);
				(L_42)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029310);
				StringU5BU5D_t1642385972* L_43 = L_42;
				baseLegID_t1303619648 * L_44 = V_7;
				NullCheck(L_44);
				int32_t* L_45 = L_44->get_address_of_x_1();
				String_t* L_46 = Int32_ToString_m2960866144(L_45, /*hidden argument*/NULL);
				NullCheck(L_43);
				ArrayElementTypeCheck (L_43, L_46);
				(L_43)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_46);
				StringU5BU5D_t1642385972* L_47 = L_43;
				NullCheck(L_47);
				ArrayElementTypeCheck (L_47, _stringLiteral372029310);
				(L_47)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029310);
				StringU5BU5D_t1642385972* L_48 = L_47;
				baseLegID_t1303619648 * L_49 = V_7;
				NullCheck(L_49);
				int32_t* L_50 = L_49->get_address_of_y_2();
				String_t* L_51 = Int32_ToString_m2960866144(L_50, /*hidden argument*/NULL);
				NullCheck(L_48);
				ArrayElementTypeCheck (L_48, L_51);
				(L_48)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_51);
				StringU5BU5D_t1642385972* L_52 = L_48;
				NullCheck(L_52);
				ArrayElementTypeCheck (L_52, _stringLiteral372029313);
				(L_52)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral372029313);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_53 = String_Concat_m626692867(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
				V_0 = L_53;
				V_6 = (bool)1;
			}

IL_017f:
			{
				bool L_54 = Enumerator_MoveNext_m2079664556((&V_10), /*hidden argument*/Enumerator_MoveNext_m2079664556_MethodInfo_var);
				if (L_54)
				{
					goto IL_00f3;
				}
			}

IL_018b:
			{
				IL2CPP_LEAVE(0x19E, FINALLY_0190);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_0190;
		}

FINALLY_0190:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m1649105920((&V_10), /*hidden argument*/Enumerator_Dispose_m1649105920_MethodInfo_var);
			IL2CPP_END_FINALLY(400)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(400)
		{
			IL2CPP_JUMP_TBL(0x19E, IL_019e)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_019e:
		{
			bool L_55 = V_6;
			if (L_55)
			{
				goto IL_0208;
			}
		}

IL_01a5:
		{
			String_t* L_56 = V_0;
			V_11 = L_56;
			StringU5BU5D_t1642385972* L_57 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)6));
			String_t* L_58 = V_11;
			NullCheck(L_57);
			ArrayElementTypeCheck (L_57, L_58);
			(L_57)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_58);
			StringU5BU5D_t1642385972* L_59 = L_57;
			baseLegID_t1303619648 * L_60 = V_7;
			NullCheck(L_60);
			int32_t* L_61 = L_60->get_address_of_id_0();
			String_t* L_62 = Int32_ToString_m2960866144(L_61, /*hidden argument*/NULL);
			NullCheck(L_59);
			ArrayElementTypeCheck (L_59, L_62);
			(L_59)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_62);
			StringU5BU5D_t1642385972* L_63 = L_59;
			NullCheck(L_63);
			ArrayElementTypeCheck (L_63, _stringLiteral372029310);
			(L_63)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029310);
			StringU5BU5D_t1642385972* L_64 = L_63;
			baseLegID_t1303619648 * L_65 = V_7;
			NullCheck(L_65);
			int32_t* L_66 = L_65->get_address_of_x_1();
			String_t* L_67 = Int32_ToString_m2960866144(L_66, /*hidden argument*/NULL);
			NullCheck(L_64);
			ArrayElementTypeCheck (L_64, L_67);
			(L_64)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_67);
			StringU5BU5D_t1642385972* L_68 = L_64;
			NullCheck(L_68);
			ArrayElementTypeCheck (L_68, _stringLiteral372029310);
			(L_68)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029310);
			StringU5BU5D_t1642385972* L_69 = L_68;
			baseLegID_t1303619648 * L_70 = V_7;
			NullCheck(L_70);
			int32_t* L_71 = L_70->get_address_of_y_2();
			String_t* L_72 = Int32_ToString_m2960866144(L_71, /*hidden argument*/NULL);
			NullCheck(L_69);
			ArrayElementTypeCheck (L_69, L_72);
			(L_69)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_72);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_73 = String_Concat_m626692867(NULL /*static, unused*/, L_69, /*hidden argument*/NULL);
			V_0 = L_73;
		}

IL_0208:
		{
			goto IL_0278;
		}

IL_020d:
		{
			String_t* L_74 = V_0;
			V_11 = L_74;
			StringU5BU5D_t1642385972* L_75 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)7));
			String_t* L_76 = V_11;
			NullCheck(L_75);
			ArrayElementTypeCheck (L_75, L_76);
			(L_75)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_76);
			StringU5BU5D_t1642385972* L_77 = L_75;
			baseLegID_t1303619648 * L_78 = V_7;
			NullCheck(L_78);
			int32_t* L_79 = L_78->get_address_of_id_0();
			String_t* L_80 = Int32_ToString_m2960866144(L_79, /*hidden argument*/NULL);
			NullCheck(L_77);
			ArrayElementTypeCheck (L_77, L_80);
			(L_77)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_80);
			StringU5BU5D_t1642385972* L_81 = L_77;
			NullCheck(L_81);
			ArrayElementTypeCheck (L_81, _stringLiteral372029310);
			(L_81)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029310);
			StringU5BU5D_t1642385972* L_82 = L_81;
			baseLegID_t1303619648 * L_83 = V_7;
			NullCheck(L_83);
			int32_t* L_84 = L_83->get_address_of_x_1();
			String_t* L_85 = Int32_ToString_m2960866144(L_84, /*hidden argument*/NULL);
			NullCheck(L_82);
			ArrayElementTypeCheck (L_82, L_85);
			(L_82)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_85);
			StringU5BU5D_t1642385972* L_86 = L_82;
			NullCheck(L_86);
			ArrayElementTypeCheck (L_86, _stringLiteral372029310);
			(L_86)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029310);
			StringU5BU5D_t1642385972* L_87 = L_86;
			baseLegID_t1303619648 * L_88 = V_7;
			NullCheck(L_88);
			int32_t* L_89 = L_88->get_address_of_y_2();
			String_t* L_90 = Int32_ToString_m2960866144(L_89, /*hidden argument*/NULL);
			NullCheck(L_87);
			ArrayElementTypeCheck (L_87, L_90);
			(L_87)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_90);
			StringU5BU5D_t1642385972* L_91 = L_87;
			NullCheck(L_91);
			ArrayElementTypeCheck (L_91, _stringLiteral372029311);
			(L_91)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral372029311);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_92 = String_Concat_m626692867(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
			V_0 = L_92;
		}

IL_0278:
		{
			bool L_93 = Enumerator_MoveNext_m2079664556((&V_8), /*hidden argument*/Enumerator_MoveNext_m2079664556_MethodInfo_var);
			if (L_93)
			{
				goto IL_00c7;
			}
		}

IL_0284:
		{
			IL2CPP_LEAVE(0x297, FINALLY_0289);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0289;
	}

FINALLY_0289:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1649105920((&V_8), /*hidden argument*/Enumerator_Dispose_m1649105920_MethodInfo_var);
		IL2CPP_END_FINALLY(649)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(649)
	{
		IL2CPP_JUMP_TBL(0x297, IL_0297)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0297:
	{
		bool L_94 = __this->get_dataState_10();
		if (!L_94)
		{
			goto IL_02be;
		}
	}
	{
		String_t* L_95 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_96 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_97 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_95, L_96, /*hidden argument*/NULL);
		if (!L_97)
		{
			goto IL_02be;
		}
	}
	{
		String_t* L_98 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_99 = String_Concat_m2596409543(NULL /*static, unused*/, L_98, _stringLiteral372029311, /*hidden argument*/NULL);
		V_0 = L_99;
	}

IL_02be:
	{
		String_t* L_100 = V_0;
		return L_100;
	}
}
// System.Void testBle::SendBytes(System.Byte[])
extern Il2CppClass* testBle_t3025851879_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1831019615_il2cpp_TypeInfo_var;
extern const MethodInfo* testBle_U3CSendBytesU3Em__8_m1750528736_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m4012585251_MethodInfo_var;
extern const uint32_t testBle_SendBytes_m3832945432_MetadataUsageId;
extern "C"  void testBle_SendBytes_m3832945432 (testBle_t3025851879 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_SendBytes_m3832945432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	ByteU5BU5D_t3397334013* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	String_t* G_B2_4 = NULL;
	String_t* G_B2_5 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	ByteU5BU5D_t3397334013* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	String_t* G_B1_4 = NULL;
	String_t* G_B1_5 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		String_t* L_0 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get__connectedID_2();
		String_t* L_1 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get__serviceUUID_3();
		String_t* L_2 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get__writeCharacteristicUUID_4();
		ByteU5BU5D_t3397334013* L_3 = ___data0;
		ByteU5BU5D_t3397334013* L_4 = ___data0;
		NullCheck(L_4);
		Action_1_t1831019615 * L_5 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_17();
		G_B1_0 = 1;
		G_B1_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))));
		G_B1_2 = L_3;
		G_B1_3 = L_2;
		G_B1_4 = L_1;
		G_B1_5 = L_0;
		if (L_5)
		{
			G_B2_0 = 1;
			G_B2_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))));
			G_B2_2 = L_3;
			G_B2_3 = L_2;
			G_B2_4 = L_1;
			G_B2_5 = L_0;
			goto IL_002c;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)testBle_U3CSendBytesU3Em__8_m1750528736_MethodInfo_var);
		Action_1_t1831019615 * L_7 = (Action_1_t1831019615 *)il2cpp_codegen_object_new(Action_1_t1831019615_il2cpp_TypeInfo_var);
		Action_1__ctor_m4012585251(L_7, NULL, L_6, /*hidden argument*/Action_1__ctor_m4012585251_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache4_17(L_7);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
		G_B2_4 = G_B1_4;
		G_B2_5 = G_B1_5;
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		Action_1_t1831019615 * L_8 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_17();
		BluetoothLEHardwareInterface_WriteCharacteristic_m1354208428(NULL /*static, unused*/, G_B2_5, G_B2_4, G_B2_3, G_B2_2, G_B2_1, (bool)G_B2_0, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testBle::SendString(System.String)
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* testBle_t3025851879_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1831019615_il2cpp_TypeInfo_var;
extern const MethodInfo* testBle_U3CSendStringU3Em__9_m2680849211_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m4012585251_MethodInfo_var;
extern const uint32_t testBle_SendString_m3395385065_MetadataUsageId;
extern "C"  void testBle_SendString_m3395385065 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_SendString_m3395385065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	ByteU5BU5D_t3397334013* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	String_t* G_B2_4 = NULL;
	String_t* G_B2_5 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	ByteU5BU5D_t3397334013* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	String_t* G_B1_4 = NULL;
	String_t* G_B1_5 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_0 = Encoding_get_Default_m908538569(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___s0;
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_2 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		String_t* L_3 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get__connectedID_2();
		String_t* L_4 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get__serviceUUID_3();
		String_t* L_5 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get__writeCharacteristicUUID_4();
		ByteU5BU5D_t3397334013* L_6 = V_0;
		ByteU5BU5D_t3397334013* L_7 = V_0;
		NullCheck(L_7);
		Action_1_t1831019615 * L_8 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_18();
		G_B1_0 = 1;
		G_B1_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))));
		G_B1_2 = L_6;
		G_B1_3 = L_5;
		G_B1_4 = L_4;
		G_B1_5 = L_3;
		if (L_8)
		{
			G_B2_0 = 1;
			G_B2_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))));
			G_B2_2 = L_6;
			G_B2_3 = L_5;
			G_B2_4 = L_4;
			G_B2_5 = L_3;
			goto IL_0038;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)testBle_U3CSendStringU3Em__9_m2680849211_MethodInfo_var);
		Action_1_t1831019615 * L_10 = (Action_1_t1831019615 *)il2cpp_codegen_object_new(Action_1_t1831019615_il2cpp_TypeInfo_var);
		Action_1__ctor_m4012585251(L_10, NULL, L_9, /*hidden argument*/Action_1__ctor_m4012585251_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache5_18(L_10);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
		G_B2_4 = G_B1_4;
		G_B2_5 = G_B1_5;
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		Action_1_t1831019615 * L_11 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_18();
		BluetoothLEHardwareInterface_WriteCharacteristic_m1354208428(NULL /*static, unused*/, G_B2_5, G_B2_4, G_B2_3, G_B2_2, G_B2_1, (bool)G_B2_0, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testBle::Update()
extern "C"  void testBle_Update_m3165031635 (testBle_t3025851879 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void testBle::.cctor()
extern Il2CppClass* testBle_t3025851879_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3545238117;
extern Il2CppCodeGenString* _stringLiteral1979154176;
extern const uint32_t testBle__cctor_m889340189_MetadataUsageId;
extern "C"  void testBle__cctor_m889340189 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle__cctor_m889340189_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->set__serviceUUID_3(_stringLiteral3545238117);
		((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->set__writeCharacteristicUUID_4(_stringLiteral1979154176);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->set_BleDataDone_8(L_0);
		return;
	}
}
// System.Void testBle::<Start>m__0()
extern Il2CppCodeGenString* _stringLiteral445101128;
extern const uint32_t testBle_U3CStartU3Em__0_m1223780933_MetadataUsageId;
extern "C"  void testBle_U3CStartU3Em__0_m1223780933 (testBle_t3025851879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_U3CStartU3Em__0_m1223780933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BluetoothLEHardwareInterface_Log_m3949775483(NULL /*static, unused*/, _stringLiteral445101128, /*hidden argument*/NULL);
		testBle_SearchAndConnectDevice_m66085037(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testBle::<Start>m__1(System.String)
extern Il2CppCodeGenString* _stringLiteral2437095642;
extern const uint32_t testBle_U3CStartU3Em__1_m3580770748_MetadataUsageId;
extern "C"  void testBle_U3CStartU3Em__1_m3580770748 (Il2CppObject * __this /* static, unused */, String_t* ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_U3CStartU3Em__1_m3580770748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BluetoothLEHardwareInterface_Log_m3949775483(NULL /*static, unused*/, _stringLiteral2437095642, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testBle::<Connect>m__2(System.String)
extern Il2CppCodeGenString* _stringLiteral1605005719;
extern const uint32_t testBle_U3CConnectU3Em__2_m586010835_MetadataUsageId;
extern "C"  void testBle_U3CConnectU3Em__2_m586010835 (Il2CppObject * __this /* static, unused */, String_t* ___address0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_U3CConnectU3Em__2_m586010835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BluetoothLEHardwareInterface_Log_m3949775483(NULL /*static, unused*/, _stringLiteral1605005719, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testBle::<Connect>m__3(System.String,System.String)
extern "C"  void testBle_U3CConnectU3Em__3_m4101113984 (Il2CppObject * __this /* static, unused */, String_t* ___address0, String_t* ___serviceUUID1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void testBle::<Connect>m__4(System.String,System.String,System.String)
extern Il2CppClass* U3CConnectU3Ec__AnonStorey0_t350889135_il2cpp_TypeInfo_var;
extern Il2CppClass* testBle_t3025851879_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t4234541925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t329312853_il2cpp_TypeInfo_var;
extern const MethodInfo* testBle_U3CConnectU3Em__A_m2995414874_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m2597838752_MethodInfo_var;
extern const MethodInfo* U3CConnectU3Ec__AnonStorey0_U3CU3Em__0_m4203433414_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m2897725092_MethodInfo_var;
extern const uint32_t testBle_U3CConnectU3Em__4_m2263236401_MetadataUsageId;
extern "C"  void testBle_U3CConnectU3Em__4_m2263236401 (testBle_t3025851879 * __this, String_t* ___address0, String_t* ___serviceUUID1, String_t* ___characteristicUUID2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_U3CConnectU3Em__4_m2263236401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CConnectU3Ec__AnonStorey0_t350889135 * V_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B4_1 = NULL;
	String_t* G_B4_2 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	{
		U3CConnectU3Ec__AnonStorey0_t350889135 * L_0 = (U3CConnectU3Ec__AnonStorey0_t350889135 *)il2cpp_codegen_object_new(U3CConnectU3Ec__AnonStorey0_t350889135_il2cpp_TypeInfo_var);
		U3CConnectU3Ec__AnonStorey0__ctor_m3799893872(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CConnectU3Ec__AnonStorey0_t350889135 * L_1 = V_0;
		String_t* L_2 = ___characteristicUUID2;
		NullCheck(L_1);
		L_1->set_characteristicUUID_0(L_2);
		U3CConnectU3Ec__AnonStorey0_t350889135 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_1(__this);
		String_t* L_4 = ___serviceUUID1;
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		String_t* L_5 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get__serviceUUID_3();
		bool L_6 = testBle_IsEqual_m3590934016(__this, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0080;
		}
	}
	{
		String_t* L_7 = ___address0;
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->set__connectedID_2(L_7);
		U3CConnectU3Ec__AnonStorey0_t350889135 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = L_8->get_characteristicUUID_0();
		String_t* L_10 = __this->get__readCharacteristicUUID_5();
		bool L_11 = testBle_IsEqual_m3590934016(__this, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0080;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		String_t* L_12 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get__connectedID_2();
		String_t* L_13 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get__serviceUUID_3();
		String_t* L_14 = __this->get__readCharacteristicUUID_5();
		Action_2_t4234541925 * L_15 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_19();
		G_B3_0 = L_14;
		G_B3_1 = L_13;
		G_B3_2 = L_12;
		if (L_15)
		{
			G_B4_0 = L_14;
			G_B4_1 = L_13;
			G_B4_2 = L_12;
			goto IL_006a;
		}
	}
	{
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)testBle_U3CConnectU3Em__A_m2995414874_MethodInfo_var);
		Action_2_t4234541925 * L_17 = (Action_2_t4234541925 *)il2cpp_codegen_object_new(Action_2_t4234541925_il2cpp_TypeInfo_var);
		Action_2__ctor_m2597838752(L_17, NULL, L_16, /*hidden argument*/Action_2__ctor_m2597838752_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache6_19(L_17);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
	}

IL_006a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		Action_2_t4234541925 * L_18 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_19();
		U3CConnectU3Ec__AnonStorey0_t350889135 * L_19 = V_0;
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)U3CConnectU3Ec__AnonStorey0_U3CU3Em__0_m4203433414_MethodInfo_var);
		Action_3_t329312853 * L_21 = (Action_3_t329312853 *)il2cpp_codegen_object_new(Action_3_t329312853_il2cpp_TypeInfo_var);
		Action_3__ctor_m2897725092(L_21, L_19, L_20, /*hidden argument*/Action_3__ctor_m2897725092_MethodInfo_var);
		BluetoothLEHardwareInterface_SubscribeCharacteristicWithDeviceAddress_m3073680728(NULL /*static, unused*/, G_B4_2, G_B4_1, G_B4_0, L_18, L_21, /*hidden argument*/NULL);
	}

IL_0080:
	{
		return;
	}
}
// System.Void testBle::<Connect>m__5(System.String)
extern Il2CppCodeGenString* _stringLiteral1494954686;
extern const uint32_t testBle_U3CConnectU3Em__5_m978557238_MetadataUsageId;
extern "C"  void testBle_U3CConnectU3Em__5_m978557238 (testBle_t3025851879 * __this, String_t* ___address0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_U3CConnectU3Em__5_m978557238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BluetoothLEHardwareInterface_Log_m3949775483(NULL /*static, unused*/, _stringLiteral1494954686, /*hidden argument*/NULL);
		testBle_SearchAndConnectDevice_m66085037(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testBle::<SearchAndConnectDevice>m__6(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t testBle_U3CSearchAndConnectDeviceU3Em__6_m3612029114_MetadataUsageId;
extern "C"  void testBle_U3CSearchAndConnectDeviceU3Em__6_m3612029114 (testBle_t3025851879 * __this, String_t* ___address0, String_t* ___name1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_U3CSearchAndConnectDeviceU3Em__6_m3612029114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name1;
		String_t* L_1 = __this->get__name_7();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		String_t* L_3 = ___address0;
		__this->set__address_6(L_3);
		BluetoothLEHardwareInterface_StopScan_m1034974532(NULL /*static, unused*/, /*hidden argument*/NULL);
		testBle_Connect_m3205883302(__this, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void testBle::<SearchAndConnectDevice>m__7(System.String,System.String,System.Int32,System.Byte[])
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral245602644;
extern const uint32_t testBle_U3CSearchAndConnectDeviceU3Em__7_m1789553411_MetadataUsageId;
extern "C"  void testBle_U3CSearchAndConnectDeviceU3Em__7_m1789553411 (Il2CppObject * __this /* static, unused */, String_t* ___address0, String_t* ___name1, int32_t ___rssi2, ByteU5BU5D_t3397334013* ___advertisingInfo3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_U3CSearchAndConnectDeviceU3Em__7_m1789553411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___advertisingInfo3;
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_1 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		String_t* L_2 = ___name1;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		ObjectU5BU5D_t3614634134* L_3 = L_1;
		int32_t L_4 = ___rssi2;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		ObjectU5BU5D_t3614634134* L_7 = L_3;
		ByteU5BU5D_t3397334013* L_8 = ___advertisingInfo3;
		NullCheck(L_8);
		int32_t L_9 = (((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length))));
		Il2CppObject * L_10 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_10);
		ObjectU5BU5D_t3614634134* L_11 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_12 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_13 = ___advertisingInfo3;
		NullCheck(L_12);
		String_t* L_14 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_12, L_13);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral245602644, L_11, /*hidden argument*/NULL);
		BluetoothLEHardwareInterface_Log_m3949775483(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void testBle::<SendBytes>m__8(System.String)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3355301189;
extern const uint32_t testBle_U3CSendBytesU3Em__8_m1750528736_MetadataUsageId;
extern "C"  void testBle_U3CSendBytesU3Em__8_m1750528736 (Il2CppObject * __this /* static, unused */, String_t* ___characteristicUUID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_U3CSendBytesU3Em__8_m1750528736_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3355301189, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testBle::<SendString>m__9(System.String)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2529223109;
extern const uint32_t testBle_U3CSendStringU3Em__9_m2680849211_MetadataUsageId;
extern "C"  void testBle_U3CSendStringU3Em__9_m2680849211 (Il2CppObject * __this /* static, unused */, String_t* ___characteristicUUID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testBle_U3CSendStringU3Em__9_m2680849211_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2529223109, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testBle::<Connect>m__A(System.String,System.String)
extern "C"  void testBle_U3CConnectU3Em__A_m2995414874 (Il2CppObject * __this /* static, unused */, String_t* ___deviceAddress0, String_t* ___notification1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void testBle/<Connect>c__AnonStorey0::.ctor()
extern "C"  void U3CConnectU3Ec__AnonStorey0__ctor_m3799893872 (U3CConnectU3Ec__AnonStorey0_t350889135 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testBle/<Connect>c__AnonStorey0::<>m__0(System.String,System.String,System.Byte[])
extern Il2CppClass* testBle_t3025851879_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CConnectU3Ec__AnonStorey0_U3CU3Em__0_m4203433414_MetadataUsageId;
extern "C"  void U3CConnectU3Ec__AnonStorey0_U3CU3Em__0_m4203433414 (U3CConnectU3Ec__AnonStorey0_t350889135 * __this, String_t* ___deviceAddress20, String_t* ___characteristic1, ByteU5BU5D_t3397334013* ___data2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CConnectU3Ec__AnonStorey0_U3CU3Em__0_m4203433414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___deviceAddress20;
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		String_t* L_1 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get__connectedID_2();
		NullCheck(L_0);
		int32_t L_2 = String_CompareTo_m3879609894(L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_009b;
		}
	}
	{
		testBle_t3025851879 * L_3 = __this->get_U24this_1();
		String_t* L_4 = __this->get_characteristicUUID_0();
		testBle_t3025851879 * L_5 = __this->get_U24this_1();
		NullCheck(L_5);
		String_t* L_6 = L_5->get__readCharacteristicUUID_5();
		NullCheck(L_3);
		bool L_7 = testBle_IsEqual_m3590934016(L_3, L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_009b;
		}
	}
	{
		testBle_t3025851879 * L_8 = __this->get_U24this_1();
		testBle_t3025851879 * L_9 = L_8;
		NullCheck(L_9);
		String_t* L_10 = L_9->get_BleDataNew_9();
		testBle_t3025851879 * L_11 = __this->get_U24this_1();
		ByteU5BU5D_t3397334013* L_12 = ___data2;
		NullCheck(L_11);
		String_t* L_13 = testBle_changeDataFormat_m2934963405(L_11, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m2596409543(NULL /*static, unused*/, L_10, L_13, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_BleDataNew_9(L_14);
		testBle_t3025851879 * L_15 = __this->get_U24this_1();
		NullCheck(L_15);
		bool L_16 = L_15->get_dataState_10();
		if (!L_16)
		{
			goto IL_009b;
		}
	}
	{
		testBle_t3025851879 * L_17 = __this->get_U24this_1();
		NullCheck(L_17);
		String_t* L_18 = L_17->get_BleDataNew_9();
		IL2CPP_RUNTIME_CLASS_INIT(testBle_t3025851879_il2cpp_TypeInfo_var);
		((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->set_BleDataDone_8(L_18);
		testBle_t3025851879 * L_19 = __this->get_U24this_1();
		NullCheck(L_19);
		L_19->set_dataState_10((bool)0);
		testBle_t3025851879 * L_20 = __this->get_U24this_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_20);
		L_20->set_BleDataNew_9(L_21);
		int32_t L_22 = ((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->get__uniqID_12();
		((testBle_t3025851879_StaticFields*)testBle_t3025851879_il2cpp_TypeInfo_var->static_fields)->set__uniqID_12(((int32_t)((int32_t)L_22+(int32_t)1)));
	}

IL_009b:
	{
		return;
	}
}
// System.Void testBle/baseLegID::.ctor()
extern "C"  void baseLegID__ctor_m525806059 (baseLegID_t1303619648 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestSceneManager::.ctor()
extern "C"  void TestSceneManager__ctor_m2884217182 (TestSceneManager_t2345075657 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestSceneManager::Awake()
extern "C"  void TestSceneManager_Awake_m994355017 (TestSceneManager_t2345075657 * __this, const MethodInfo* method)
{
	GameObject_t1756533147 * V_0 = NULL;
	GameObjectU5BU5D_t3057952154* V_1 = NULL;
	int32_t V_2 = 0;
	{
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_m_Containers_2();
		V_1 = L_0;
		V_2 = 0;
		goto IL_001d;
	}

IL_000e:
	{
		GameObjectU5BU5D_t3057952154* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		GameObject_t1756533147 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		GameObject_t1756533147 * L_5 = V_0;
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)0, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_001d:
	{
		int32_t L_7 = V_2;
		GameObjectU5BU5D_t3057952154* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_9 = __this->get_m_CurrentIndex_3();
		TestSceneManager_ActivateContainer_m2050603347(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestSceneManager::ActivateContainer(System.Int32)
extern "C"  void TestSceneManager_ActivateContainer_m2050603347 (TestSceneManager_t2345075657 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		GameObjectU5BU5D_t3057952154* L_1 = __this->get_m_Containers_2();
		NullCheck(L_1);
		if ((((int32_t)L_0) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))))
		{
			goto IL_0011;
		}
	}
	{
		___index0 = 0;
	}

IL_0011:
	{
		int32_t L_2 = ___index0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_3 = __this->get_m_Containers_2();
		NullCheck(L_3);
		___index0 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))-(int32_t)1));
	}

IL_0024:
	{
		int32_t L_4 = ___index0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0058;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_5 = __this->get_m_Containers_2();
		int32_t L_6 = __this->get_m_CurrentIndex_3();
		NullCheck(L_5);
		int32_t L_7 = L_6;
		GameObject_t1756533147 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		GameObject_SetActive_m2887581199(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = ___index0;
		__this->set_m_CurrentIndex_3(L_9);
		GameObjectU5BU5D_t3057952154* L_10 = __this->get_m_Containers_2();
		int32_t L_11 = __this->get_m_CurrentIndex_3();
		NullCheck(L_10);
		int32_t L_12 = L_11;
		GameObject_t1756533147 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		GameObject_SetActive_m2887581199(L_13, (bool)1, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void TestSceneManager::GoToNext()
extern "C"  void TestSceneManager_GoToNext_m368671678 (TestSceneManager_t2345075657 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_CurrentIndex_3();
		TestSceneManager_ActivateContainer_m2050603347(__this, ((int32_t)((int32_t)L_0+(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestSceneManager::GoToPrevious()
extern "C"  void TestSceneManager_GoToPrevious_m2494976322 (TestSceneManager_t2345075657 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_CurrentIndex_3();
		TestSceneManager_ActivateContainer_m2050603347(__this, ((int32_t)((int32_t)L_0-(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrailElement::.ctor()
extern "C"  void TrailElement__ctor_m2219600159 (TrailElement_t1685395368 * __this, const MethodInfo* method)
{
	{
		__this->set_m_ItemId_4((-1));
		__this->set_m_TrailPos_13((-1));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrailElement::ClearFreeElements()
extern Il2CppClass* TrailElement_t1685395368_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Pop_m2665221936_MethodInfo_var;
extern const MethodInfo* Stack_1_get_Count_m335565545_MethodInfo_var;
extern const uint32_t TrailElement_ClearFreeElements_m65079503_MetadataUsageId;
extern "C"  void TrailElement_ClearFreeElements_m65079503 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrailElement_ClearFreeElements_m65079503_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		goto IL_0019;
	}

IL_0005:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrailElement_t1685395368_il2cpp_TypeInfo_var);
		Stack_1_t2844261301 * L_0 = ((TrailElement_t1685395368_StaticFields*)TrailElement_t1685395368_il2cpp_TypeInfo_var->static_fields)->get_m_FreeElements_2();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Stack_1_Pop_m2665221936(L_0, /*hidden argument*/Stack_1_Pop_m2665221936_MethodInfo_var);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = GameObject_get_gameObject_m3662236595(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrailElement_t1685395368_il2cpp_TypeInfo_var);
		Stack_1_t2844261301 * L_3 = ((TrailElement_t1685395368_StaticFields*)TrailElement_t1685395368_il2cpp_TypeInfo_var->static_fields)->get_m_FreeElements_2();
		NullCheck(L_3);
		int32_t L_4 = Stack_1_get_Count_m335565545(L_3, /*hidden argument*/Stack_1_get_Count_m335565545_MethodInfo_var);
		if ((((int32_t)L_4) > ((int32_t)0)))
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Void TrailElement::Initialise(SpriteTrail)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var;
extern const MethodInfo* Queue_1_Enqueue_m507665509_MethodInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m2757116194_MethodInfo_var;
extern const MethodInfo* Queue_1_get_Count_m353297782_MethodInfo_var;
extern const MethodInfo* Queue_1_GetEnumerator_m1731654386_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m640616911_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m481530403_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3669259067_MethodInfo_var;
extern const uint32_t TrailElement_Initialise_m1205103621_MetadataUsageId;
extern "C"  void TrailElement_Initialise_m1205103621 (TrailElement_t1685395368 * __this, SpriteTrail_t2284883325 * ___trail0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrailElement_Initialise_m1205103621_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	TrailElement_t1685395368 * V_1 = NULL;
	Enumerator_t2015115283  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		__this->set_m_NeedDequeue_14((bool)0);
		__this->set_m_TrailPos_13((-1));
		__this->set_m_NeedLateUpdate_12((bool)0);
		__this->set_m_TimeSinceCreation_6((0.0f));
		SpriteTrail_t2284883325 * L_0 = ___trail0;
		__this->set_m_MotherTrail_11(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		SpriteTrail_t2284883325 * L_2 = ___trail0;
		NullCheck(L_2);
		TrailPreset_t465077881 * L_3 = L_2->get_m_CurrentTrailPreset_3();
		__this->set_m_TrailSettings_5(L_3);
		SpriteRenderer_t1209076198 * L_4 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		__this->set_m_SpRenderer_7(L_4);
		TrailPreset_t465077881 * L_5 = __this->get_m_TrailSettings_5();
		NullCheck(L_5);
		Material_t193706927 * L_6 = L_5->get_m_SpecialMat_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007c;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_8 = __this->get_m_SpRenderer_7();
		TrailPreset_t465077881 * L_9 = __this->get_m_TrailSettings_5();
		NullCheck(L_9);
		Material_t193706927 * L_10 = L_9->get_m_SpecialMat_2();
		NullCheck(L_8);
		Renderer_set_material_m1053097112(L_8, L_10, /*hidden argument*/NULL);
		goto IL_0092;
	}

IL_007c:
	{
		SpriteRenderer_t1209076198 * L_11 = __this->get_m_SpRenderer_7();
		SpriteTrail_t2284883325 * L_12 = ___trail0;
		NullCheck(L_12);
		SpriteRenderer_t1209076198 * L_13 = L_12->get_m_SpriteToDuplicate_5();
		NullCheck(L_13);
		Material_t193706927 * L_14 = Renderer_get_material_m2553789785(L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		Renderer_set_material_m1053097112(L_11, L_14, /*hidden argument*/NULL);
	}

IL_0092:
	{
		SpriteRenderer_t1209076198 * L_15 = __this->get_m_SpRenderer_7();
		SpriteTrail_t2284883325 * L_16 = ___trail0;
		NullCheck(L_16);
		SpriteRenderer_t1209076198 * L_17 = L_16->get_m_SpriteToDuplicate_5();
		NullCheck(L_17);
		Color_t2020392075  L_18 = SpriteRenderer_get_color_m345525162(L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		SpriteRenderer_set_color_m2339931967(L_15, L_18, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_19 = __this->get_m_SpRenderer_7();
		SpriteTrail_t2284883325 * L_20 = ___trail0;
		NullCheck(L_20);
		int32_t L_21 = L_20->get_m_TrailOrderInLayer_9();
		NullCheck(L_19);
		Renderer_set_sortingOrder_m809829562(L_19, L_21, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_22 = __this->get_m_SpRenderer_7();
		SpriteTrail_t2284883325 * L_23 = ___trail0;
		NullCheck(L_23);
		SpriteRenderer_t1209076198 * L_24 = L_23->get_m_SpriteToDuplicate_5();
		NullCheck(L_24);
		Sprite_t309593783 * L_25 = SpriteRenderer_get_sprite_m3016837432(L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		SpriteRenderer_set_sprite_m617298623(L_22, L_25, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_26 = __this->get_m_SpRenderer_7();
		SpriteTrail_t2284883325 * L_27 = ___trail0;
		NullCheck(L_27);
		SpriteRenderer_t1209076198 * L_28 = L_27->get_m_SpriteToDuplicate_5();
		NullCheck(L_28);
		bool L_29 = SpriteRenderer_get_flipX_m922215015(L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		SpriteRenderer_set_flipX_m994174828(L_26, L_29, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_30 = __this->get_m_SpRenderer_7();
		SpriteTrail_t2284883325 * L_31 = ___trail0;
		NullCheck(L_31);
		SpriteRenderer_t1209076198 * L_32 = L_31->get_m_SpriteToDuplicate_5();
		NullCheck(L_32);
		bool L_33 = SpriteRenderer_get_flipY_m3671301346(L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		SpriteRenderer_set_flipY_m1303919149(L_30, L_33, /*hidden argument*/NULL);
		Transform_t3275118058 * L_34 = __this->get_m_Transform_15();
		NullCheck(L_34);
		Vector3_t2243707580  L_35 = Transform_get_localScale_m3074381503(L_34, /*hidden argument*/NULL);
		__this->set_m_InitSize_9(L_35);
		Transform_t3275118058 * L_36 = __this->get_m_Transform_15();
		NullCheck(L_36);
		Vector3_t2243707580  L_37 = Transform_get_localPosition_m2533925116(L_36, /*hidden argument*/NULL);
		__this->set_m_InitPos_10(L_37);
		__this->set_m_Init_8((bool)1);
		SpriteTrail_t2284883325 * L_38 = ___trail0;
		NullCheck(L_38);
		Queue_1_t1505052203 * L_39 = L_38->get_m_ElementsInTrail_20();
		NullCheck(L_39);
		Queue_1_Enqueue_m507665509(L_39, __this, /*hidden argument*/Queue_1_Enqueue_m507665509_MethodInfo_var);
		TrailElement_ApplyFrameEffect_m1827553553(__this, /*hidden argument*/NULL);
		TrailPreset_t465077881 * L_40 = __this->get_m_TrailSettings_5();
		NullCheck(L_40);
		int32_t L_41 = L_40->get_m_TrailElementDurationCondition_5();
		if ((!(((uint32_t)L_41) == ((uint32_t)1))))
		{
			goto IL_020d;
		}
	}
	{
		TrailPreset_t465077881 * L_42 = __this->get_m_TrailSettings_5();
		NullCheck(L_42);
		int32_t L_43 = L_42->get_m_TrailMaxLength_6();
		if ((((int32_t)L_43) <= ((int32_t)0)))
		{
			goto IL_018e;
		}
	}
	{
		goto IL_016e;
	}

IL_015d:
	{
		SpriteTrail_t2284883325 * L_44 = ___trail0;
		NullCheck(L_44);
		Queue_1_t1505052203 * L_45 = L_44->get_m_ElementsInTrail_20();
		NullCheck(L_45);
		TrailElement_t1685395368 * L_46 = Queue_1_Dequeue_m2757116194(L_45, /*hidden argument*/Queue_1_Dequeue_m2757116194_MethodInfo_var);
		NullCheck(L_46);
		TrailElement_Hide_m3076722378(L_46, (bool)1, /*hidden argument*/NULL);
	}

IL_016e:
	{
		SpriteTrail_t2284883325 * L_47 = ___trail0;
		NullCheck(L_47);
		Queue_1_t1505052203 * L_48 = L_47->get_m_ElementsInTrail_20();
		NullCheck(L_48);
		int32_t L_49 = Queue_1_get_Count_m353297782(L_48, /*hidden argument*/Queue_1_get_Count_m353297782_MethodInfo_var);
		TrailPreset_t465077881 * L_50 = __this->get_m_TrailSettings_5();
		NullCheck(L_50);
		int32_t L_51 = L_50->get_m_TrailMaxLength_6();
		if ((((int32_t)L_49) > ((int32_t)L_51)))
		{
			goto IL_015d;
		}
	}
	{
		goto IL_01b5;
	}

IL_018e:
	{
		goto IL_01a4;
	}

IL_0193:
	{
		SpriteTrail_t2284883325 * L_52 = ___trail0;
		NullCheck(L_52);
		Queue_1_t1505052203 * L_53 = L_52->get_m_ElementsInTrail_20();
		NullCheck(L_53);
		TrailElement_t1685395368 * L_54 = Queue_1_Dequeue_m2757116194(L_53, /*hidden argument*/Queue_1_Dequeue_m2757116194_MethodInfo_var);
		NullCheck(L_54);
		TrailElement_Hide_m3076722378(L_54, (bool)1, /*hidden argument*/NULL);
	}

IL_01a4:
	{
		SpriteTrail_t2284883325 * L_55 = ___trail0;
		NullCheck(L_55);
		Queue_1_t1505052203 * L_56 = L_55->get_m_ElementsInTrail_20();
		NullCheck(L_56);
		int32_t L_57 = Queue_1_get_Count_m353297782(L_56, /*hidden argument*/Queue_1_get_Count_m353297782_MethodInfo_var);
		if ((((int32_t)L_57) > ((int32_t)0)))
		{
			goto IL_0193;
		}
	}

IL_01b5:
	{
		V_0 = 0;
		SpriteTrail_t2284883325 * L_58 = ___trail0;
		NullCheck(L_58);
		Queue_1_t1505052203 * L_59 = L_58->get_m_ElementsInTrail_20();
		NullCheck(L_59);
		Enumerator_t2015115283  L_60 = Queue_1_GetEnumerator_m1731654386(L_59, /*hidden argument*/Queue_1_GetEnumerator_m1731654386_MethodInfo_var);
		V_2 = L_60;
	}

IL_01c3:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01ee;
		}

IL_01c8:
		{
			TrailElement_t1685395368 * L_61 = Enumerator_get_Current_m640616911((&V_2), /*hidden argument*/Enumerator_get_Current_m640616911_MethodInfo_var);
			V_1 = L_61;
			TrailElement_t1685395368 * L_62 = V_1;
			SpriteTrail_t2284883325 * L_63 = ___trail0;
			NullCheck(L_63);
			Queue_1_t1505052203 * L_64 = L_63->get_m_ElementsInTrail_20();
			NullCheck(L_64);
			int32_t L_65 = Queue_1_get_Count_m353297782(L_64, /*hidden argument*/Queue_1_get_Count_m353297782_MethodInfo_var);
			int32_t L_66 = V_0;
			NullCheck(L_62);
			L_62->set_m_TrailPos_13(((int32_t)((int32_t)L_65-(int32_t)L_66)));
			TrailElement_t1685395368 * L_67 = V_1;
			NullCheck(L_67);
			L_67->set_m_NeedLateUpdate_12((bool)1);
			int32_t L_68 = V_0;
			V_0 = ((int32_t)((int32_t)L_68+(int32_t)1));
		}

IL_01ee:
		{
			bool L_69 = Enumerator_MoveNext_m481530403((&V_2), /*hidden argument*/Enumerator_MoveNext_m481530403_MethodInfo_var);
			if (L_69)
			{
				goto IL_01c8;
			}
		}

IL_01fa:
		{
			IL2CPP_LEAVE(0x20D, FINALLY_01ff);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_01ff;
	}

FINALLY_01ff:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3669259067((&V_2), /*hidden argument*/Enumerator_Dispose_m3669259067_MethodInfo_var);
		IL2CPP_END_FINALLY(511)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(511)
	{
		IL2CPP_JUMP_TBL(0x20D, IL_020d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_020d:
	{
		return;
	}
}
// System.Void TrailElement::Awake()
extern Il2CppClass* TrailElement_t1685395368_il2cpp_TypeInfo_var;
extern const uint32_t TrailElement_Awake_m336711492_MetadataUsageId;
extern "C"  void TrailElement_Awake_m336711492 (TrailElement_t1685395368 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrailElement_Awake_m336711492_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_m_Transform_15(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(TrailElement_t1685395368_il2cpp_TypeInfo_var);
		int32_t L_1 = ((TrailElement_t1685395368_StaticFields*)TrailElement_t1685395368_il2cpp_TypeInfo_var->static_fields)->get_m_ItemCount_3();
		__this->set_m_ItemId_4(L_1);
		int32_t L_2 = ((TrailElement_t1685395368_StaticFields*)TrailElement_t1685395368_il2cpp_TypeInfo_var->static_fields)->get_m_ItemCount_3();
		((TrailElement_t1685395368_StaticFields*)TrailElement_t1685395368_il2cpp_TypeInfo_var->static_fields)->set_m_ItemCount_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Void TrailElement::Update()
extern "C"  void TrailElement_Update_m3641136998 (TrailElement_t1685395368 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Init_8();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		float L_1 = __this->get_m_TimeSinceCreation_6();
		float L_2 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_TimeSinceCreation_6(((float)((float)L_1+(float)L_2)));
		TrailElement_ApplyFrameEffect_m1827553553(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrailElement::LateUpdate()
extern "C"  void TrailElement_LateUpdate_m3561634826 (TrailElement_t1685395368 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_NeedLateUpdate_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		int32_t L_1 = __this->get_m_TrailPos_13();
		TrailElement_ApplyAddSpriteEffect_m2236787785(__this, L_1, /*hidden argument*/NULL);
		__this->set_m_NeedLateUpdate_12((bool)0);
		return;
	}
}
// System.Void TrailElement::ApplyAddSpriteEffect(System.Int32)
extern "C"  void TrailElement_ApplyAddSpriteEffect_m2236787785 (TrailElement_t1685395368 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		TrailPreset_t465077881 * L_0 = __this->get_m_TrailSettings_5();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_m_TrailMaxLength_6();
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_2 = ___index0;
		TrailPreset_t465077881 * L_3 = __this->get_m_TrailSettings_5();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_m_TrailMaxLength_6();
		TrailElement_ApplyModificationFromRatio_m4018682883(__this, ((float)((float)(((float)((float)L_2)))/(float)(((float)((float)L_4))))), /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void TrailElement::ApplyFrameEffect()
extern "C"  void TrailElement_ApplyFrameEffect_m1827553553 (TrailElement_t1685395368 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		TrailPreset_t465077881 * L_0 = __this->get_m_TrailSettings_5();
		NullCheck(L_0);
		float L_1 = L_0->get_m_TrailDuration_7();
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_002e;
		}
	}
	{
		float L_2 = __this->get_m_TimeSinceCreation_6();
		TrailPreset_t465077881 * L_3 = __this->get_m_TrailSettings_5();
		NullCheck(L_3);
		float L_4 = L_3->get_m_TrailDuration_7();
		V_0 = ((float)((float)L_2/(float)L_4));
	}

IL_002e:
	{
		float L_5 = V_0;
		if ((!(((float)L_5) >= ((float)(1.0f)))))
		{
			goto IL_0041;
		}
	}
	{
		TrailElement_Hide_m3076722378(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}

IL_0041:
	{
		TrailPreset_t465077881 * L_6 = __this->get_m_TrailSettings_5();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_m_TrailElementDurationCondition_5();
		if (L_7)
		{
			goto IL_0058;
		}
	}
	{
		float L_8 = V_0;
		TrailElement_ApplyModificationFromRatio_m4018682883(__this, L_8, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void TrailElement::ApplyModificationFromRatio(System.Single)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t TrailElement_ApplyModificationFromRatio_m4018682883_MetadataUsageId;
extern "C"  void TrailElement_ApplyModificationFromRatio_m4018682883 (TrailElement_t1685395368 * __this, float ___ratio0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrailElement_ApplyModificationFromRatio_m4018682883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		TrailPreset_t465077881 * L_0 = __this->get_m_TrailSettings_5();
		NullCheck(L_0);
		bool L_1 = L_0->get_m_UseOnlyAlpha_3();
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_2 = __this->get_m_SpRenderer_7();
		NullCheck(L_2);
		Color_t2020392075  L_3 = SpriteRenderer_get_color_m345525162(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		TrailPreset_t465077881 * L_4 = __this->get_m_TrailSettings_5();
		NullCheck(L_4);
		Gradient_t3600583008 * L_5 = L_4->get_m_TrailColor_4();
		float L_6 = ___ratio0;
		NullCheck(L_5);
		Color_t2020392075  L_7 = Gradient_Evaluate_m1322905850(L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = (&V_1)->get_a_3();
		(&V_0)->set_a_3(L_8);
		SpriteRenderer_t1209076198 * L_9 = __this->get_m_SpRenderer_7();
		Color_t2020392075  L_10 = V_0;
		NullCheck(L_9);
		SpriteRenderer_set_color_m2339931967(L_9, L_10, /*hidden argument*/NULL);
		goto IL_0069;
	}

IL_004d:
	{
		SpriteRenderer_t1209076198 * L_11 = __this->get_m_SpRenderer_7();
		TrailPreset_t465077881 * L_12 = __this->get_m_TrailSettings_5();
		NullCheck(L_12);
		Gradient_t3600583008 * L_13 = L_12->get_m_TrailColor_4();
		float L_14 = ___ratio0;
		NullCheck(L_13);
		Color_t2020392075  L_15 = Gradient_Evaluate_m1322905850(L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		SpriteRenderer_set_color_m2339931967(L_11, L_15, /*hidden argument*/NULL);
	}

IL_0069:
	{
		TrailPreset_t465077881 * L_16 = __this->get_m_TrailSettings_5();
		NullCheck(L_16);
		bool L_17 = L_16->get_m_UseSizeModifier_13();
		if (!L_17)
		{
			goto IL_00e1;
		}
	}
	{
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_2));
		Vector3_t2243707580 * L_18 = __this->get_address_of_m_InitSize_9();
		float L_19 = L_18->get_x_1();
		TrailPreset_t465077881 * L_20 = __this->get_m_TrailSettings_5();
		NullCheck(L_20);
		AnimationCurve_t3306541151 * L_21 = L_20->get_m_TrailSizeX_15();
		float L_22 = ___ratio0;
		NullCheck(L_21);
		float L_23 = AnimationCurve_Evaluate_m3698879322(L_21, L_22, /*hidden argument*/NULL);
		(&V_2)->set_x_1(((float)((float)L_19*(float)L_23)));
		Vector3_t2243707580 * L_24 = __this->get_address_of_m_InitSize_9();
		float L_25 = L_24->get_y_2();
		TrailPreset_t465077881 * L_26 = __this->get_m_TrailSettings_5();
		NullCheck(L_26);
		AnimationCurve_t3306541151 * L_27 = L_26->get_m_TrailSizeY_16();
		float L_28 = ___ratio0;
		NullCheck(L_27);
		float L_29 = AnimationCurve_Evaluate_m3698879322(L_27, L_28, /*hidden argument*/NULL);
		(&V_2)->set_y_2(((float)((float)L_25*(float)L_29)));
		(&V_2)->set_z_3((1.0f));
		Transform_t3275118058 * L_30 = __this->get_m_Transform_15();
		Vector3_t2243707580  L_31 = V_2;
		NullCheck(L_30);
		Transform_set_localScale_m2325460848(L_30, L_31, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		TrailPreset_t465077881 * L_32 = __this->get_m_TrailSettings_5();
		NullCheck(L_32);
		bool L_33 = L_32->get_m_UsePositionModifier_14();
		if (!L_33)
		{
			goto IL_0142;
		}
	}
	{
		Vector3_t2243707580  L_34 = __this->get_m_InitPos_10();
		V_3 = L_34;
		Vector3_t2243707580 * L_35 = (&V_3);
		float L_36 = L_35->get_x_1();
		TrailPreset_t465077881 * L_37 = __this->get_m_TrailSettings_5();
		NullCheck(L_37);
		AnimationCurve_t3306541151 * L_38 = L_37->get_m_TrailPositionX_17();
		float L_39 = ___ratio0;
		NullCheck(L_38);
		float L_40 = AnimationCurve_Evaluate_m3698879322(L_38, L_39, /*hidden argument*/NULL);
		L_35->set_x_1(((float)((float)L_36+(float)L_40)));
		Vector3_t2243707580 * L_41 = (&V_3);
		float L_42 = L_41->get_y_2();
		TrailPreset_t465077881 * L_43 = __this->get_m_TrailSettings_5();
		NullCheck(L_43);
		AnimationCurve_t3306541151 * L_44 = L_43->get_m_TrailPositionY_18();
		float L_45 = ___ratio0;
		NullCheck(L_44);
		float L_46 = AnimationCurve_Evaluate_m3698879322(L_44, L_45, /*hidden argument*/NULL);
		L_41->set_y_2(((float)((float)L_42+(float)L_46)));
		Transform_t3275118058 * L_47 = __this->get_m_Transform_15();
		Vector3_t2243707580  L_48 = V_3;
		NullCheck(L_47);
		Transform_set_localPosition_m1026930133(L_47, L_48, /*hidden argument*/NULL);
	}

IL_0142:
	{
		return;
	}
}
// UnityEngine.GameObject TrailElement::GetFreeElement()
extern Il2CppClass* TrailElement_t1685395368_il2cpp_TypeInfo_var;
extern Il2CppClass* SpriteTrail_t2284883325_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_get_Count_m335565545_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m2665221936_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const uint32_t TrailElement_GetFreeElement_m700256560_MetadataUsageId;
extern "C"  GameObject_t1756533147 * TrailElement_GetFreeElement_m700256560 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrailElement_GetFreeElement_m700256560_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrailElement_t1685395368_il2cpp_TypeInfo_var);
		Stack_1_t2844261301 * L_0 = ((TrailElement_t1685395368_StaticFields*)TrailElement_t1685395368_il2cpp_TypeInfo_var->static_fields)->get_m_FreeElements_2();
		NullCheck(L_0);
		int32_t L_1 = Stack_1_get_Count_m335565545(L_0, /*hidden argument*/Stack_1_get_Count_m335565545_MethodInfo_var);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrailElement_t1685395368_il2cpp_TypeInfo_var);
		Stack_1_t2844261301 * L_2 = ((TrailElement_t1685395368_StaticFields*)TrailElement_t1685395368_il2cpp_TypeInfo_var->static_fields)->get_m_FreeElements_2();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Stack_1_Pop_m2665221936(L_2, /*hidden argument*/Stack_1_Pop_m2665221936_MethodInfo_var);
		return L_3;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteTrail_t2284883325_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_4 = SpriteTrail_GetTrailElementPrefab_m1242168547(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_5 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_4, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		return L_5;
	}
}
// System.Void TrailElement::Hide(System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* TrailElement_t1685395368_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1_get_Count_m353297782_MethodInfo_var;
extern const MethodInfo* Queue_1_Peek_m2757710183_MethodInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m2757116194_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m3004012536_MethodInfo_var;
extern const uint32_t TrailElement_Hide_m3076722378_MetadataUsageId;
extern "C"  void TrailElement_Hide_m3076722378 (TrailElement_t1685395368 * __this, bool ___AddToFree0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrailElement_Hide_m3076722378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, __this, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}

IL_001d:
	{
		return;
	}

IL_001e:
	{
		SpriteTrail_t2284883325 * L_3 = __this->get_m_MotherTrail_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		__this->set_m_NeedDequeue_14((bool)1);
	}

IL_0036:
	{
		SpriteTrail_t2284883325 * L_5 = __this->get_m_MotherTrail_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0088;
		}
	}
	{
		SpriteTrail_t2284883325 * L_7 = __this->get_m_MotherTrail_11();
		NullCheck(L_7);
		Queue_1_t1505052203 * L_8 = L_7->get_m_ElementsInTrail_20();
		NullCheck(L_8);
		int32_t L_9 = Queue_1_get_Count_m353297782(L_8, /*hidden argument*/Queue_1_get_Count_m353297782_MethodInfo_var);
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0088;
		}
	}
	{
		SpriteTrail_t2284883325 * L_10 = __this->get_m_MotherTrail_11();
		NullCheck(L_10);
		Queue_1_t1505052203 * L_11 = L_10->get_m_ElementsInTrail_20();
		NullCheck(L_11);
		TrailElement_t1685395368 * L_12 = Queue_1_Peek_m2757710183(L_11, /*hidden argument*/Queue_1_Peek_m2757710183_MethodInfo_var);
		NullCheck(L_12);
		bool L_13 = L_12->get_m_NeedDequeue_14();
		if (!L_13)
		{
			goto IL_0088;
		}
	}
	{
		SpriteTrail_t2284883325 * L_14 = __this->get_m_MotherTrail_11();
		NullCheck(L_14);
		Queue_1_t1505052203 * L_15 = L_14->get_m_ElementsInTrail_20();
		NullCheck(L_15);
		Queue_1_Dequeue_m2757116194(L_15, /*hidden argument*/Queue_1_Dequeue_m2757116194_MethodInfo_var);
	}

IL_0088:
	{
		GameObject_t1756533147 * L_16 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		GameObject_SetActive_m2887581199(L_16, (bool)0, /*hidden argument*/NULL);
		bool L_17 = ___AddToFree0;
		if (!L_17)
		{
			goto IL_00aa;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrailElement_t1685395368_il2cpp_TypeInfo_var);
		Stack_1_t2844261301 * L_18 = ((TrailElement_t1685395368_StaticFields*)TrailElement_t1685395368_il2cpp_TypeInfo_var->static_fields)->get_m_FreeElements_2();
		GameObject_t1756533147 * L_19 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Stack_1_Push_m3004012536(L_18, L_19, /*hidden argument*/Stack_1_Push_m3004012536_MethodInfo_var);
	}

IL_00aa:
	{
		__this->set_m_Init_8((bool)0);
		return;
	}
}
// System.Void TrailElement::.cctor()
extern Il2CppClass* Stack_1_t2844261301_il2cpp_TypeInfo_var;
extern Il2CppClass* TrailElement_t1685395368_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m2347528477_MethodInfo_var;
extern const uint32_t TrailElement__cctor_m2464911458_MetadataUsageId;
extern "C"  void TrailElement__cctor_m2464911458 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrailElement__cctor_m2464911458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stack_1_t2844261301 * L_0 = (Stack_1_t2844261301 *)il2cpp_codegen_object_new(Stack_1_t2844261301_il2cpp_TypeInfo_var);
		Stack_1__ctor_m2347528477(L_0, /*hidden argument*/Stack_1__ctor_m2347528477_MethodInfo_var);
		((TrailElement_t1685395368_StaticFields*)TrailElement_t1685395368_il2cpp_TypeInfo_var->static_fields)->set_m_FreeElements_2(L_0);
		((TrailElement_t1685395368_StaticFields*)TrailElement_t1685395368_il2cpp_TypeInfo_var->static_fields)->set_m_ItemCount_3(0);
		return;
	}
}
// System.Void TrailManagerExample::.ctor()
extern "C"  void TrailManagerExample__ctor_m1756197966 (TrailManagerExample_t3762595367 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrailManagerExample::OnDisable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t TrailManagerExample_OnDisable_m1649692411_MetadataUsageId;
extern "C"  void TrailManagerExample_OnDisable_m1649692411 (TrailManagerExample_t3762595367 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrailManagerExample_OnDisable_m1649692411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_m_UI_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_m_UI_5();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void TrailManagerExample::OnEnable()
extern const MethodInfo* GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSimpleCharacterControler2D_t2053125211_m438366674_MethodInfo_var;
extern const uint32_t TrailManagerExample_OnEnable_m3487594818_MetadataUsageId;
extern "C"  void TrailManagerExample_OnEnable_m3487594818 (TrailManagerExample_t3762595367 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrailManagerExample_OnEnable_m3487594818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpriteTrail_t2284883325 * V_0 = NULL;
	SpriteTrailU5BU5D_t321135600* V_1 = NULL;
	int32_t V_2 = 0;
	{
		GameObject_t1756533147 * L_0 = __this->get_m_Character_4();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_m2469242620(L_1, L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_m_Character_4();
		NullCheck(L_4);
		Rigidbody2D_t502193897 * L_5 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_4, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var);
		Vector2_t2243707579  L_6 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Rigidbody2D_set_velocity_m3592751374(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = __this->get_m_Character_4();
		NullCheck(L_7);
		Rigidbody2D_t502193897 * L_8 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_7, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var);
		GameObject_t1756533147 * L_9 = __this->get_m_Character_4();
		NullCheck(L_9);
		SimpleCharacterControler2D_t2053125211 * L_10 = GameObject_GetComponent_TisSimpleCharacterControler2D_t2053125211_m438366674(L_9, /*hidden argument*/GameObject_GetComponent_TisSimpleCharacterControler2D_t2053125211_m438366674_MethodInfo_var);
		NullCheck(L_10);
		float L_11 = L_10->get_m_Force_3();
		Vector2_t2243707579  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector2__ctor_m3067419446(&L_12, L_11, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		Rigidbody2D_AddForce_m4245830473(L_8, L_12, 1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_13 = __this->get_m_UI_5();
		NullCheck(L_13);
		GameObject_SetActive_m2887581199(L_13, (bool)1, /*hidden argument*/NULL);
		SpriteTrailU5BU5D_t321135600* L_14 = __this->get_m_Trails_2();
		V_1 = L_14;
		V_2 = 0;
		goto IL_0082;
	}

IL_0074:
	{
		SpriteTrailU5BU5D_t321135600* L_15 = V_1;
		int32_t L_16 = V_2;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		SpriteTrail_t2284883325 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_0 = L_18;
		SpriteTrail_t2284883325 * L_19 = V_0;
		NullCheck(L_19);
		SpriteTrail_DisableTrail_m2137670676(L_19, /*hidden argument*/NULL);
		int32_t L_20 = V_2;
		V_2 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0082:
	{
		int32_t L_21 = V_2;
		SpriteTrailU5BU5D_t321135600* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_23 = __this->get_m_CurrentTrailIndex_3();
		TrailManagerExample_ActivateTrail_m690315824(__this, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrailManagerExample::ActivateTrail(System.Int32)
extern "C"  void TrailManagerExample_ActivateTrail_m690315824 (TrailManagerExample_t3762595367 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		SpriteTrailU5BU5D_t321135600* L_1 = __this->get_m_Trails_2();
		NullCheck(L_1);
		if ((((int32_t)L_0) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))))
		{
			goto IL_0011;
		}
	}
	{
		___index0 = 0;
	}

IL_0011:
	{
		int32_t L_2 = ___index0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		SpriteTrailU5BU5D_t321135600* L_3 = __this->get_m_Trails_2();
		NullCheck(L_3);
		___index0 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))-(int32_t)1));
	}

IL_0024:
	{
		int32_t L_4 = ___index0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0056;
		}
	}
	{
		SpriteTrailU5BU5D_t321135600* L_5 = __this->get_m_Trails_2();
		int32_t L_6 = __this->get_m_CurrentTrailIndex_3();
		NullCheck(L_5);
		int32_t L_7 = L_6;
		SpriteTrail_t2284883325 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		SpriteTrail_DisableTrail_m2137670676(L_8, /*hidden argument*/NULL);
		int32_t L_9 = ___index0;
		__this->set_m_CurrentTrailIndex_3(L_9);
		SpriteTrailU5BU5D_t321135600* L_10 = __this->get_m_Trails_2();
		int32_t L_11 = __this->get_m_CurrentTrailIndex_3();
		NullCheck(L_10);
		int32_t L_12 = L_11;
		SpriteTrail_t2284883325 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		SpriteTrail_EnableTrail_m3387428939(L_13, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
// System.Void TrailManagerExample::GoToNext()
extern "C"  void TrailManagerExample_GoToNext_m4020412622 (TrailManagerExample_t3762595367 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_CurrentTrailIndex_3();
		TrailManagerExample_ActivateTrail_m690315824(__this, ((int32_t)((int32_t)L_0+(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrailManagerExample::GoToPrevious()
extern "C"  void TrailManagerExample_GoToPrevious_m2105779650 (TrailManagerExample_t3762595367 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_CurrentTrailIndex_3();
		TrailManagerExample_ActivateTrail_m690315824(__this, ((int32_t)((int32_t)L_0-(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrailPreset::.ctor()
extern "C"  void TrailPreset__ctor_m2349883794 (TrailPreset_t465077881 * __this, const MethodInfo* method)
{
	{
		__this->set_m_TrailMaxLength_6(((int32_t)10));
		__this->set_m_TrailDuration_7((0.5f));
		__this->set_m_DistanceCorrection_12((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIActions::.ctor()
extern "C"  void UIActions__ctor_m1180179564 (UIActions_t467790635 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIActions::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral434527363;
extern const uint32_t UIActions_Awake_m113298475_MetadataUsageId;
extern "C"  void UIActions_Awake_m113298475 (UIActions_t467790635 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIActions_Awake_m113298475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral434527363, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIActions::Start()
extern Il2CppCodeGenString* _stringLiteral2660685848;
extern Il2CppCodeGenString* _stringLiteral2660682713;
extern Il2CppCodeGenString* _stringLiteral881793685;
extern const uint32_t UIActions_Start_m4073899760_MetadataUsageId;
extern "C"  void UIActions_Start_m4073899760 (UIActions_t467790635 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIActions_Start_m4073899760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_leftCurtainReverse_3();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_rightCurtainReverse_5();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		int32_t L_2 = Application_get_loadedLevel_m3768581785(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0048;
		}
	}
	{
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral2660685848, 0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral2660682713, 0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral881793685, 0, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0048:
	{
		int32_t L_3 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral881793685, ((int32_t)((int32_t)L_3+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_005e:
	{
		return;
	}
}
// System.Void UIActions::Update()
extern "C"  void UIActions_Update_m831686391 (UIActions_t467790635 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UIActions::TwoPlayerGame()
extern "C"  void UIActions_TwoPlayerGame_m3495032287 (UIActions_t467790635 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UIActions::closeCurtains()
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3308898148;
extern const uint32_t UIActions_closeCurtains_m4258359545_MetadataUsageId;
extern "C"  void UIActions_closeCurtains_m4258359545 (UIActions_t467790635 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIActions_closeCurtains_m4258359545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_curtainCloseSound_6();
		NullCheck(L_0);
		AudioSource_t1135106623 * L_1 = GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039(L_0, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039_MethodInfo_var);
		NullCheck(L_1);
		AudioSource_Play_m353744792(L_1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_leftCurtain_2();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_rightCurtain_4();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral3308898148, (2.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIActions::loadLevel()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral881793685;
extern Il2CppCodeGenString* _stringLiteral434527363;
extern const uint32_t UIActions_loadLevel_m322202434_MetadataUsageId;
extern "C"  void UIActions_loadLevel_m322202434 (UIActions_t467790635 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIActions_loadLevel_m322202434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Application_get_loadedLevel_m3768581785(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_0015:
	{
		int32_t L_1 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0030;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_0030:
	{
		int32_t L_2 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_004c;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)18), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_004c:
	{
		int32_t L_3 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)3))))
		{
			goto IL_0067;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_0067:
	{
		int32_t L_4 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)4))))
		{
			goto IL_0083;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)17), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_0083:
	{
		int32_t L_5 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)5))))
		{
			goto IL_009f;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)20), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_009f:
	{
		int32_t L_6 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)6))))
		{
			goto IL_00bb;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)16), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_00bb:
	{
		int32_t L_7 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)7))))
		{
			goto IL_00d7;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)23), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_00d7:
	{
		int32_t L_8 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)8))))
		{
			goto IL_00f2;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, 5, /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_00f2:
	{
		int32_t L_9 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_010e;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, 6, /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_010e:
	{
		int32_t L_10 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_012b;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)21), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_012b:
	{
		int32_t L_11 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0147;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_0147:
	{
		int32_t L_12 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0163;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, 8, /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_0163:
	{
		int32_t L_13 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0180;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_0180:
	{
		int32_t L_14 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_019d;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)24), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_019d:
	{
		int32_t L_15 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_01ba;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)22), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_01ba:
	{
		int32_t L_16 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_01d7;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_01d7:
	{
		int32_t L_17 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)17)))))
		{
			goto IL_01f4;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)11), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_01f4:
	{
		int32_t L_18 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)18)))))
		{
			goto IL_0211;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)12), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_0211:
	{
		int32_t L_19 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)19)))))
		{
			goto IL_022e;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)13), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_022e:
	{
		int32_t L_20 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)20)))))
		{
			goto IL_024b;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)19), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_024b:
	{
		int32_t L_21 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)21)))))
		{
			goto IL_0268;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)15), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_0268:
	{
		int32_t L_22 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)22)))))
		{
			goto IL_0285;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, ((int32_t)14), /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_0285:
	{
		int32_t L_23 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)23)))))
		{
			goto IL_02a1;
		}
	}
	{
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		goto IL_02c7;
	}

IL_02a1:
	{
		int32_t L_24 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral881793685, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)24)))))
		{
			goto IL_02c7;
		}
	}
	{
		GameObject_t1756533147 * L_25 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral434527363, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyObject_m2343493981(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_02c7:
	{
		return;
	}
}
// System.Void UIDetection::.ctor()
extern "C"  void UIDetection__ctor_m1583400304 (UIDetection_t3199971681 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIDetection::Start()
extern Il2CppCodeGenString* _stringLiteral40213808;
extern const uint32_t UIDetection_Start_m2145702376_MetadataUsageId;
extern "C"  void UIDetection_Start_m2145702376 (UIDetection_t3199971681 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDetection_Start_m2145702376_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral40213808, /*hidden argument*/NULL);
		__this->set_gameCo_2(L_0);
		return;
	}
}
// System.Void UIDetection::Update()
extern "C"  void UIDetection_Update_m2554543749 (UIDetection_t3199971681 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UIDetection::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisIconControl_t564227302_m435800415_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2933534756;
extern Il2CppCodeGenString* _stringLiteral68814504;
extern Il2CppCodeGenString* _stringLiteral1108177399;
extern const uint32_t UIDetection_OnTriggerEnter2D_m1613736096_MetadataUsageId;
extern "C"  void UIDetection_OnTriggerEnter2D_m1613736096 (UIDetection_t3199971681 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDetection_OnTriggerEnter2D_m1613736096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t646061738 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m2079638459(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2933534756, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		String_t* L_3 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		bool L_4 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, _stringLiteral68814504, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		GameObject_t1756533147 * L_5 = __this->get_gameCo_2();
		NullCheck(L_5);
		IconControl_t564227302 * L_6 = GameObject_GetComponent_TisIconControl_t564227302_m435800415(L_5, /*hidden argument*/GameObject_GetComponent_TisIconControl_t564227302_m435800415_MethodInfo_var);
		Collider2D_t646061738 * L_7 = ___other0;
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m2079638459(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->set_pl1collider_28(L_8);
	}

IL_0040:
	{
		String_t* L_9 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, _stringLiteral1108177399, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006b;
		}
	}
	{
		GameObject_t1756533147 * L_11 = __this->get_gameCo_2();
		NullCheck(L_11);
		IconControl_t564227302 * L_12 = GameObject_GetComponent_TisIconControl_t564227302_m435800415(L_11, /*hidden argument*/GameObject_GetComponent_TisIconControl_t564227302_m435800415_MethodInfo_var);
		Collider2D_t646061738 * L_13 = ___other0;
		NullCheck(L_13);
		String_t* L_14 = Object_get_name_m2079638459(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		L_12->set_pl2collider_29(L_14);
	}

IL_006b:
	{
		GameObject_t1756533147 * L_15 = __this->get_gameCo_2();
		NullCheck(L_15);
		IconControl_t564227302 * L_16 = GameObject_GetComponent_TisIconControl_t564227302_m435800415(L_15, /*hidden argument*/GameObject_GetComponent_TisIconControl_t564227302_m435800415_MethodInfo_var);
		NullCheck(L_16);
		IconControl_checkAnswer_m2014282341(L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIDetection::OnTriggerExit2D(UnityEngine.Collider2D)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisIconControl_t564227302_m435800415_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral68814504;
extern Il2CppCodeGenString* _stringLiteral1108177399;
extern const uint32_t UIDetection_OnTriggerExit2D_m2591984930_MetadataUsageId;
extern "C"  void UIDetection_OnTriggerExit2D_m2591984930 (UIDetection_t3199971681 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDetection_OnTriggerExit2D_m2591984930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, _stringLiteral68814504, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_gameCo_2();
		NullCheck(L_2);
		IconControl_t564227302 * L_3 = GameObject_GetComponent_TisIconControl_t564227302_m435800415(L_2, /*hidden argument*/GameObject_GetComponent_TisIconControl_t564227302_m435800415_MethodInfo_var);
		NullCheck(L_3);
		L_3->set_pl1collider_28((String_t*)NULL);
	}

IL_0026:
	{
		String_t* L_4 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, _stringLiteral1108177399, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004c;
		}
	}
	{
		GameObject_t1756533147 * L_6 = __this->get_gameCo_2();
		NullCheck(L_6);
		IconControl_t564227302 * L_7 = GameObject_GetComponent_TisIconControl_t564227302_m435800415(L_6, /*hidden argument*/GameObject_GetComponent_TisIconControl_t564227302_m435800415_MethodInfo_var);
		NullCheck(L_7);
		L_7->set_pl2collider_29((String_t*)NULL);
	}

IL_004c:
	{
		return;
	}
}
// System.Void UILineRenderer::.ctor()
extern Il2CppClass* Graphic_t2426225576_il2cpp_TypeInfo_var;
extern const uint32_t UILineRenderer__ctor_m1228311854_MetadataUsageId;
extern "C"  void UILineRenderer__ctor_m1228311854 (UILineRenderer_t1431191837 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UILineRenderer__ctor_m1228311854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rect_t3681755626  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Rect__ctor_m1220545469(&L_0, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_m_UVRect_20(L_0);
		__this->set_LineThickness_21((2.0f));
		IL2CPP_RUNTIME_CLASS_INIT(Graphic_t2426225576_il2cpp_TypeInfo_var);
		Graphic__ctor_m821539719(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture UILineRenderer::get_mainTexture()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Graphic_t2426225576_il2cpp_TypeInfo_var;
extern const uint32_t UILineRenderer_get_mainTexture_m1097622252_MetadataUsageId;
extern "C"  Texture_t2243626319 * UILineRenderer_get_mainTexture_m1097622252 (UILineRenderer_t1431191837 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UILineRenderer_get_mainTexture_m1097622252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_t3542995729 * G_B3_0 = NULL;
	{
		Texture_t2243626319 * L_0 = __this->get_m_Texture_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Graphic_t2426225576_il2cpp_TypeInfo_var);
		Texture2D_t3542995729 * L_2 = ((Graphic_t2426225576_StaticFields*)Graphic_t2426225576_il2cpp_TypeInfo_var->static_fields)->get_s_WhiteTexture_3();
		G_B3_0 = L_2;
		goto IL_0021;
	}

IL_001b:
	{
		Texture_t2243626319 * L_3 = __this->get_m_Texture_19();
		G_B3_0 = ((Texture2D_t3542995729 *)(L_3));
	}

IL_0021:
	{
		return G_B3_0;
	}
}
// UnityEngine.Texture UILineRenderer::get_texture()
extern "C"  Texture_t2243626319 * UILineRenderer_get_texture_m3250786741 (UILineRenderer_t1431191837 * __this, const MethodInfo* method)
{
	{
		Texture_t2243626319 * L_0 = __this->get_m_Texture_19();
		return L_0;
	}
}
// System.Void UILineRenderer::set_texture(UnityEngine.Texture)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UILineRenderer_set_texture_m1683784838_MetadataUsageId;
extern "C"  void UILineRenderer_set_texture_m1683784838 (UILineRenderer_t1431191837 * __this, Texture_t2243626319 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UILineRenderer_set_texture_m1683784838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture_t2243626319 * L_0 = __this->get_m_Texture_19();
		Texture_t2243626319 * L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Texture_t2243626319 * L_3 = ___value0;
		__this->set_m_Texture_19(L_3);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(29 /* System.Void UnityEngine.UI.Graphic::SetMaterialDirty() */, __this);
		return;
	}
}
// UnityEngine.Rect UILineRenderer::get_uvRect()
extern "C"  Rect_t3681755626  UILineRenderer_get_uvRect_m3166800612 (UILineRenderer_t1431191837 * __this, const MethodInfo* method)
{
	{
		Rect_t3681755626  L_0 = __this->get_m_UVRect_20();
		return L_0;
	}
}
// System.Void UILineRenderer::set_uvRect(UnityEngine.Rect)
extern "C"  void UILineRenderer_set_uvRect_m1994527789 (UILineRenderer_t1431191837 * __this, Rect_t3681755626  ___value0, const MethodInfo* method)
{
	{
		Rect_t3681755626  L_0 = __this->get_m_UVRect_20();
		Rect_t3681755626  L_1 = ___value0;
		bool L_2 = Rect_op_Equality_m2793663577(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Rect_t3681755626  L_3 = ___value0;
		__this->set_m_UVRect_20(L_3);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		return;
	}
}
// System.Void UILineRenderer::OnFillVBO(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern Il2CppClass* Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1612828711_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m310628129_MethodInfo_var;
extern const MethodInfo* List_1_Add_m148291600_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m4024240619_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m313976211_MethodInfo_var;
extern const uint32_t UILineRenderer_OnFillVBO_m1039925740_MetadataUsageId;
extern "C"  void UILineRenderer_OnFillVBO_m1039925740 (UILineRenderer_t1431191837 * __this, List_1_t573379950 * ___vbo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UILineRenderer_OnFillVBO_m1039925740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Rect_t3681755626  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	Vector2_t2243707579  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Rect_t3681755626  V_10;
	memset(&V_10, 0, sizeof(V_10));
	List_1_t1612828711 * V_11 = NULL;
	Vector2_t2243707579  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector2_t2243707579  V_13;
	memset(&V_13, 0, sizeof(V_13));
	int32_t V_14 = 0;
	Vector2_t2243707579  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector2U5BU5D_t686124026* V_16 = NULL;
	Vector2_t2243707579  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector2_t2243707579  V_18;
	memset(&V_18, 0, sizeof(V_18));
	int32_t V_19 = 0;
	Vector2_t2243707579  V_20;
	memset(&V_20, 0, sizeof(V_20));
	Vector2_t2243707579  V_21;
	memset(&V_21, 0, sizeof(V_21));
	float V_22 = 0.0f;
	Vector2_t2243707579  V_23;
	memset(&V_23, 0, sizeof(V_23));
	Vector2_t2243707579  V_24;
	memset(&V_24, 0, sizeof(V_24));
	Vector2_t2243707579  V_25;
	memset(&V_25, 0, sizeof(V_25));
	Vector2_t2243707579  V_26;
	memset(&V_26, 0, sizeof(V_26));
	Vector2_t2243707579  V_27;
	memset(&V_27, 0, sizeof(V_27));
	Vector2_t2243707579  V_28;
	memset(&V_28, 0, sizeof(V_28));
	Vector2_t2243707579  V_29;
	memset(&V_29, 0, sizeof(V_29));
	Vector2_t2243707579  V_30;
	memset(&V_30, 0, sizeof(V_30));
	Vector2_t2243707579  V_31;
	memset(&V_31, 0, sizeof(V_31));
	Vector2_t2243707579  V_32;
	memset(&V_32, 0, sizeof(V_32));
	Vector2U5BU5D_t686124026* V_33 = NULL;
	{
		Vector2U5BU5D_t686124026* L_0 = __this->get_Points_24();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Vector2U5BU5D_t686124026* L_1 = __this->get_Points_24();
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)2)))
		{
			goto IL_005b;
		}
	}

IL_0019:
	{
		Vector2U5BU5D_t686124026* L_2 = ((Vector2U5BU5D_t686124026*)SZArrayNew(Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_2);
		Vector2_t2243707579  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m3067419446(&L_3, (0.0f), (0.0f), /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_3;
		Vector2U5BU5D_t686124026* L_4 = L_2;
		NullCheck(L_4);
		Vector2_t2243707579  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2__ctor_m3067419446(&L_5, (1.0f), (1.0f), /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_5;
		__this->set_Points_24(L_4);
	}

IL_005b:
	{
		V_0 = ((int32_t)24);
		RectTransform_t3349966182 * L_6 = Graphic_get_rectTransform_m2697395074(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Rect_t3681755626  L_7 = RectTransform_get_rect_m73954734(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = Rect_get_width_m1138015702((&V_2), /*hidden argument*/NULL);
		V_1 = L_8;
		RectTransform_t3349966182 * L_9 = Graphic_get_rectTransform_m2697395074(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Rect_t3681755626  L_10 = RectTransform_get_rect_m73954734(L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = Rect_get_height_m3128694305((&V_4), /*hidden argument*/NULL);
		V_3 = L_11;
		RectTransform_t3349966182 * L_12 = Graphic_get_rectTransform_m2697395074(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector2_t2243707579  L_13 = RectTransform_get_pivot_m759087479(L_12, /*hidden argument*/NULL);
		V_6 = L_13;
		float L_14 = (&V_6)->get_x_0();
		RectTransform_t3349966182 * L_15 = Graphic_get_rectTransform_m2697395074(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Rect_t3681755626  L_16 = RectTransform_get_rect_m73954734(L_15, /*hidden argument*/NULL);
		V_7 = L_16;
		float L_17 = Rect_get_width_m1138015702((&V_7), /*hidden argument*/NULL);
		V_5 = ((float)((float)((-L_14))*(float)L_17));
		RectTransform_t3349966182 * L_18 = Graphic_get_rectTransform_m2697395074(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector2_t2243707579  L_19 = RectTransform_get_pivot_m759087479(L_18, /*hidden argument*/NULL);
		V_9 = L_19;
		float L_20 = (&V_9)->get_y_1();
		RectTransform_t3349966182 * L_21 = Graphic_get_rectTransform_m2697395074(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Rect_t3681755626  L_22 = RectTransform_get_rect_m73954734(L_21, /*hidden argument*/NULL);
		V_10 = L_22;
		float L_23 = Rect_get_height_m3128694305((&V_10), /*hidden argument*/NULL);
		V_8 = ((float)((float)((-L_20))*(float)L_23));
		bool L_24 = __this->get_relativeSize_25();
		if (L_24)
		{
			goto IL_00f6;
		}
	}
	{
		V_1 = (1.0f);
		V_3 = (1.0f);
	}

IL_00f6:
	{
		List_1_t1612828711 * L_25 = (List_1_t1612828711 *)il2cpp_codegen_object_new(List_1_t1612828711_il2cpp_TypeInfo_var);
		List_1__ctor_m310628129(L_25, /*hidden argument*/List_1__ctor_m310628129_MethodInfo_var);
		V_11 = L_25;
		List_1_t1612828711 * L_26 = V_11;
		Vector2U5BU5D_t686124026* L_27 = __this->get_Points_24();
		NullCheck(L_27);
		NullCheck(L_26);
		List_1_Add_m148291600(L_26, (*(Vector2_t2243707579 *)((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/List_1_Add_m148291600_MethodInfo_var);
		Vector2U5BU5D_t686124026* L_28 = __this->get_Points_24();
		NullCheck(L_28);
		Vector2U5BU5D_t686124026* L_29 = __this->get_Points_24();
		NullCheck(L_29);
		Vector2U5BU5D_t686124026* L_30 = __this->get_Points_24();
		NullCheck(L_30);
		Vector2_t2243707579  L_31 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))), (*(Vector2_t2243707579 *)((L_30)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		V_13 = L_31;
		Vector2_t2243707579  L_32 = Vector2_get_normalized_m2985402409((&V_13), /*hidden argument*/NULL);
		int32_t L_33 = V_0;
		Vector2_t2243707579  L_34 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_32, (((float)((float)L_33))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_35 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), L_34, /*hidden argument*/NULL);
		V_12 = L_35;
		List_1_t1612828711 * L_36 = V_11;
		Vector2_t2243707579  L_37 = V_12;
		NullCheck(L_36);
		List_1_Add_m148291600(L_36, L_37, /*hidden argument*/List_1_Add_m148291600_MethodInfo_var);
		V_14 = 1;
		goto IL_0194;
	}

IL_0175:
	{
		List_1_t1612828711 * L_38 = V_11;
		Vector2U5BU5D_t686124026* L_39 = __this->get_Points_24();
		int32_t L_40 = V_14;
		NullCheck(L_39);
		NullCheck(L_38);
		List_1_Add_m148291600(L_38, (*(Vector2_t2243707579 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40)))), /*hidden argument*/List_1_Add_m148291600_MethodInfo_var);
		int32_t L_41 = V_14;
		V_14 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_0194:
	{
		int32_t L_42 = V_14;
		Vector2U5BU5D_t686124026* L_43 = __this->get_Points_24();
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_43)->max_length))))-(int32_t)1)))))
		{
			goto IL_0175;
		}
	}
	{
		Vector2U5BU5D_t686124026* L_44 = __this->get_Points_24();
		Vector2U5BU5D_t686124026* L_45 = __this->get_Points_24();
		NullCheck(L_45);
		NullCheck(L_44);
		Vector2U5BU5D_t686124026* L_46 = __this->get_Points_24();
		Vector2U5BU5D_t686124026* L_47 = __this->get_Points_24();
		NullCheck(L_47);
		NullCheck(L_46);
		Vector2U5BU5D_t686124026* L_48 = __this->get_Points_24();
		Vector2U5BU5D_t686124026* L_49 = __this->get_Points_24();
		NullCheck(L_49);
		NullCheck(L_48);
		Vector2_t2243707579  L_50 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_46)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length))))-(int32_t)1)))))), (*(Vector2_t2243707579 *)((L_48)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_49)->max_length))))-(int32_t)2)))))), /*hidden argument*/NULL);
		V_15 = L_50;
		Vector2_t2243707579  L_51 = Vector2_get_normalized_m2985402409((&V_15), /*hidden argument*/NULL);
		int32_t L_52 = V_0;
		Vector2_t2243707579  L_53 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_51, (((float)((float)L_52))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_54 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length))))-(int32_t)1)))))), L_53, /*hidden argument*/NULL);
		V_12 = L_54;
		List_1_t1612828711 * L_55 = V_11;
		Vector2_t2243707579  L_56 = V_12;
		NullCheck(L_55);
		List_1_Add_m148291600(L_55, L_56, /*hidden argument*/List_1_Add_m148291600_MethodInfo_var);
		List_1_t1612828711 * L_57 = V_11;
		Vector2U5BU5D_t686124026* L_58 = __this->get_Points_24();
		Vector2U5BU5D_t686124026* L_59 = __this->get_Points_24();
		NullCheck(L_59);
		NullCheck(L_58);
		NullCheck(L_57);
		List_1_Add_m148291600(L_57, (*(Vector2_t2243707579 *)((L_58)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_59)->max_length))))-(int32_t)1)))))), /*hidden argument*/List_1_Add_m148291600_MethodInfo_var);
		List_1_t1612828711 * L_60 = V_11;
		NullCheck(L_60);
		Vector2U5BU5D_t686124026* L_61 = List_1_ToArray_m4024240619(L_60, /*hidden argument*/List_1_ToArray_m4024240619_MethodInfo_var);
		V_16 = L_61;
		bool L_62 = __this->get_UseMargins_22();
		if (!L_62)
		{
			goto IL_0295;
		}
	}
	{
		float L_63 = V_1;
		Vector2_t2243707579 * L_64 = __this->get_address_of_Margin_23();
		float L_65 = L_64->get_x_0();
		V_1 = ((float)((float)L_63-(float)L_65));
		float L_66 = V_3;
		Vector2_t2243707579 * L_67 = __this->get_address_of_Margin_23();
		float L_68 = L_67->get_y_1();
		V_3 = ((float)((float)L_66-(float)L_68));
		float L_69 = V_5;
		Vector2_t2243707579 * L_70 = __this->get_address_of_Margin_23();
		float L_71 = L_70->get_x_0();
		V_5 = ((float)((float)L_69+(float)((float)((float)L_71/(float)(2.0f)))));
		float L_72 = V_8;
		Vector2_t2243707579 * L_73 = __this->get_address_of_Margin_23();
		float L_74 = L_73->get_y_1();
		V_8 = ((float)((float)L_72+(float)((float)((float)L_74/(float)(2.0f)))));
	}

IL_0295:
	{
		List_1_t573379950 * L_75 = ___vbo0;
		NullCheck(L_75);
		List_1_Clear_m313976211(L_75, /*hidden argument*/List_1_Clear_m313976211_MethodInfo_var);
		Vector2_t2243707579  L_76 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_17 = L_76;
		Vector2_t2243707579  L_77 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_18 = L_77;
		V_19 = 1;
		goto IL_064a;
	}

IL_02b1:
	{
		Vector2U5BU5D_t686124026* L_78 = V_16;
		int32_t L_79 = V_19;
		NullCheck(L_78);
		V_20 = (*(Vector2_t2243707579 *)((L_78)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_79-(int32_t)1))))));
		Vector2U5BU5D_t686124026* L_80 = V_16;
		int32_t L_81 = V_19;
		NullCheck(L_80);
		V_21 = (*(Vector2_t2243707579 *)((L_80)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_81))));
		float L_82 = (&V_20)->get_x_0();
		float L_83 = V_1;
		float L_84 = V_5;
		float L_85 = (&V_20)->get_y_1();
		float L_86 = V_3;
		float L_87 = V_8;
		Vector2__ctor_m3067419446((&V_20), ((float)((float)((float)((float)L_82*(float)L_83))+(float)L_84)), ((float)((float)((float)((float)L_85*(float)L_86))+(float)L_87)), /*hidden argument*/NULL);
		float L_88 = (&V_21)->get_x_0();
		float L_89 = V_1;
		float L_90 = V_5;
		float L_91 = (&V_21)->get_y_1();
		float L_92 = V_3;
		float L_93 = V_8;
		Vector2__ctor_m3067419446((&V_21), ((float)((float)((float)((float)L_88*(float)L_89))+(float)L_90)), ((float)((float)((float)((float)L_91*(float)L_92))+(float)L_93)), /*hidden argument*/NULL);
		float L_94 = (&V_21)->get_y_1();
		float L_95 = (&V_20)->get_y_1();
		float L_96 = (&V_21)->get_x_0();
		float L_97 = (&V_20)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_98 = atan2f(((float)((float)L_94-(float)L_95)), ((float)((float)L_96-(float)L_97)));
		V_22 = ((float)((float)((float)((float)L_98*(float)(180.0f)))/(float)(3.14159274f)));
		Vector2_t2243707579  L_99 = V_20;
		float L_100 = __this->get_LineThickness_21();
		Vector2_t2243707579  L_101;
		memset(&L_101, 0, sizeof(L_101));
		Vector2__ctor_m3067419446(&L_101, (0.0f), ((float)((float)((-L_100))/(float)(2.0f))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_102 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_99, L_101, /*hidden argument*/NULL);
		V_23 = L_102;
		Vector2_t2243707579  L_103 = V_20;
		float L_104 = __this->get_LineThickness_21();
		Vector2_t2243707579  L_105;
		memset(&L_105, 0, sizeof(L_105));
		Vector2__ctor_m3067419446(&L_105, (0.0f), ((float)((float)L_104/(float)(2.0f))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_106 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_103, L_105, /*hidden argument*/NULL);
		V_24 = L_106;
		Vector2_t2243707579  L_107 = V_21;
		float L_108 = __this->get_LineThickness_21();
		Vector2_t2243707579  L_109;
		memset(&L_109, 0, sizeof(L_109));
		Vector2__ctor_m3067419446(&L_109, (0.0f), ((float)((float)L_108/(float)(2.0f))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_110 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_107, L_109, /*hidden argument*/NULL);
		V_25 = L_110;
		Vector2_t2243707579  L_111 = V_21;
		float L_112 = __this->get_LineThickness_21();
		Vector2_t2243707579  L_113;
		memset(&L_113, 0, sizeof(L_113));
		Vector2__ctor_m3067419446(&L_113, (0.0f), ((float)((float)((-L_112))/(float)(2.0f))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_114 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_111, L_113, /*hidden argument*/NULL);
		V_26 = L_114;
		Vector2_t2243707579  L_115 = V_23;
		Vector3_t2243707580  L_116 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_115, /*hidden argument*/NULL);
		Vector2_t2243707579  L_117 = V_20;
		Vector3_t2243707580  L_118 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_117, /*hidden argument*/NULL);
		float L_119 = V_22;
		Vector3_t2243707580  L_120;
		memset(&L_120, 0, sizeof(L_120));
		Vector3__ctor_m2638739322(&L_120, (0.0f), (0.0f), L_119, /*hidden argument*/NULL);
		Vector3_t2243707580  L_121 = UILineRenderer_RotatePointAroundPivot_m1756234985(__this, L_116, L_118, L_120, /*hidden argument*/NULL);
		Vector2_t2243707579  L_122 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_121, /*hidden argument*/NULL);
		V_23 = L_122;
		Vector2_t2243707579  L_123 = V_24;
		Vector3_t2243707580  L_124 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_123, /*hidden argument*/NULL);
		Vector2_t2243707579  L_125 = V_20;
		Vector3_t2243707580  L_126 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_125, /*hidden argument*/NULL);
		float L_127 = V_22;
		Vector3_t2243707580  L_128;
		memset(&L_128, 0, sizeof(L_128));
		Vector3__ctor_m2638739322(&L_128, (0.0f), (0.0f), L_127, /*hidden argument*/NULL);
		Vector3_t2243707580  L_129 = UILineRenderer_RotatePointAroundPivot_m1756234985(__this, L_124, L_126, L_128, /*hidden argument*/NULL);
		Vector2_t2243707579  L_130 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_129, /*hidden argument*/NULL);
		V_24 = L_130;
		Vector2_t2243707579  L_131 = V_25;
		Vector3_t2243707580  L_132 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_131, /*hidden argument*/NULL);
		Vector2_t2243707579  L_133 = V_21;
		Vector3_t2243707580  L_134 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_133, /*hidden argument*/NULL);
		float L_135 = V_22;
		Vector3_t2243707580  L_136;
		memset(&L_136, 0, sizeof(L_136));
		Vector3__ctor_m2638739322(&L_136, (0.0f), (0.0f), L_135, /*hidden argument*/NULL);
		Vector3_t2243707580  L_137 = UILineRenderer_RotatePointAroundPivot_m1756234985(__this, L_132, L_134, L_136, /*hidden argument*/NULL);
		Vector2_t2243707579  L_138 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_137, /*hidden argument*/NULL);
		V_25 = L_138;
		Vector2_t2243707579  L_139 = V_26;
		Vector3_t2243707580  L_140 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_139, /*hidden argument*/NULL);
		Vector2_t2243707579  L_141 = V_21;
		Vector3_t2243707580  L_142 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_141, /*hidden argument*/NULL);
		float L_143 = V_22;
		Vector3_t2243707580  L_144;
		memset(&L_144, 0, sizeof(L_144));
		Vector3__ctor_m2638739322(&L_144, (0.0f), (0.0f), L_143, /*hidden argument*/NULL);
		Vector3_t2243707580  L_145 = UILineRenderer_RotatePointAroundPivot_m1756234985(__this, L_140, L_142, L_144, /*hidden argument*/NULL);
		Vector2_t2243707579  L_146 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_145, /*hidden argument*/NULL);
		V_26 = L_146;
		Vector2_t2243707579  L_147 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_27 = L_147;
		Vector2__ctor_m3067419446((&V_28), (0.0f), (1.0f), /*hidden argument*/NULL);
		Vector2__ctor_m3067419446((&V_29), (0.5f), (0.0f), /*hidden argument*/NULL);
		Vector2__ctor_m3067419446((&V_30), (0.5f), (1.0f), /*hidden argument*/NULL);
		Vector2__ctor_m3067419446((&V_31), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector2__ctor_m3067419446((&V_32), (1.0f), (1.0f), /*hidden argument*/NULL);
		Vector2U5BU5D_t686124026* L_148 = ((Vector2U5BU5D_t686124026*)SZArrayNew(Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_148);
		Vector2_t2243707579  L_149 = V_29;
		(*(Vector2_t2243707579 *)((L_148)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_149;
		Vector2U5BU5D_t686124026* L_150 = L_148;
		NullCheck(L_150);
		Vector2_t2243707579  L_151 = V_30;
		(*(Vector2_t2243707579 *)((L_150)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_151;
		Vector2U5BU5D_t686124026* L_152 = L_150;
		NullCheck(L_152);
		Vector2_t2243707579  L_153 = V_30;
		(*(Vector2_t2243707579 *)((L_152)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_153;
		Vector2U5BU5D_t686124026* L_154 = L_152;
		NullCheck(L_154);
		Vector2_t2243707579  L_155 = V_29;
		(*(Vector2_t2243707579 *)((L_154)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_155;
		V_33 = L_154;
		int32_t L_156 = V_19;
		if ((((int32_t)L_156) <= ((int32_t)1)))
		{
			goto IL_055b;
		}
	}
	{
		List_1_t573379950 * L_157 = ___vbo0;
		Vector2U5BU5D_t686124026* L_158 = ((Vector2U5BU5D_t686124026*)SZArrayNew(Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_158);
		Vector2_t2243707579  L_159 = V_17;
		(*(Vector2_t2243707579 *)((L_158)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_159;
		Vector2U5BU5D_t686124026* L_160 = L_158;
		NullCheck(L_160);
		Vector2_t2243707579  L_161 = V_18;
		(*(Vector2_t2243707579 *)((L_160)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_161;
		Vector2U5BU5D_t686124026* L_162 = L_160;
		NullCheck(L_162);
		Vector2_t2243707579  L_163 = V_23;
		(*(Vector2_t2243707579 *)((L_162)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_163;
		Vector2U5BU5D_t686124026* L_164 = L_162;
		NullCheck(L_164);
		Vector2_t2243707579  L_165 = V_24;
		(*(Vector2_t2243707579 *)((L_164)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_165;
		Vector2U5BU5D_t686124026* L_166 = V_33;
		UILineRenderer_SetVbo_m858010382(__this, L_157, L_164, L_166, /*hidden argument*/NULL);
	}

IL_055b:
	{
		int32_t L_167 = V_19;
		if ((!(((uint32_t)L_167) == ((uint32_t)1))))
		{
			goto IL_05a8;
		}
	}
	{
		Vector2U5BU5D_t686124026* L_168 = ((Vector2U5BU5D_t686124026*)SZArrayNew(Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_168);
		Vector2_t2243707579  L_169 = V_27;
		(*(Vector2_t2243707579 *)((L_168)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_169;
		Vector2U5BU5D_t686124026* L_170 = L_168;
		NullCheck(L_170);
		Vector2_t2243707579  L_171 = V_28;
		(*(Vector2_t2243707579 *)((L_170)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_171;
		Vector2U5BU5D_t686124026* L_172 = L_170;
		NullCheck(L_172);
		Vector2_t2243707579  L_173 = V_30;
		(*(Vector2_t2243707579 *)((L_172)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_173;
		Vector2U5BU5D_t686124026* L_174 = L_172;
		NullCheck(L_174);
		Vector2_t2243707579  L_175 = V_29;
		(*(Vector2_t2243707579 *)((L_174)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_175;
		V_33 = L_174;
		goto IL_05f5;
	}

IL_05a8:
	{
		int32_t L_176 = V_19;
		Vector2U5BU5D_t686124026* L_177 = V_16;
		NullCheck(L_177);
		if ((!(((uint32_t)L_176) == ((uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_177)->max_length))))-(int32_t)1))))))
		{
			goto IL_05f5;
		}
	}
	{
		Vector2U5BU5D_t686124026* L_178 = ((Vector2U5BU5D_t686124026*)SZArrayNew(Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_178);
		Vector2_t2243707579  L_179 = V_29;
		(*(Vector2_t2243707579 *)((L_178)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_179;
		Vector2U5BU5D_t686124026* L_180 = L_178;
		NullCheck(L_180);
		Vector2_t2243707579  L_181 = V_30;
		(*(Vector2_t2243707579 *)((L_180)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_181;
		Vector2U5BU5D_t686124026* L_182 = L_180;
		NullCheck(L_182);
		Vector2_t2243707579  L_183 = V_32;
		(*(Vector2_t2243707579 *)((L_182)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_183;
		Vector2U5BU5D_t686124026* L_184 = L_182;
		NullCheck(L_184);
		Vector2_t2243707579  L_185 = V_31;
		(*(Vector2_t2243707579 *)((L_184)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_185;
		V_33 = L_184;
	}

IL_05f5:
	{
		List_1_t573379950 * L_186 = ___vbo0;
		Vector2U5BU5D_t686124026* L_187 = ((Vector2U5BU5D_t686124026*)SZArrayNew(Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_187);
		Vector2_t2243707579  L_188 = V_23;
		(*(Vector2_t2243707579 *)((L_187)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_188;
		Vector2U5BU5D_t686124026* L_189 = L_187;
		NullCheck(L_189);
		Vector2_t2243707579  L_190 = V_24;
		(*(Vector2_t2243707579 *)((L_189)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_190;
		Vector2U5BU5D_t686124026* L_191 = L_189;
		NullCheck(L_191);
		Vector2_t2243707579  L_192 = V_25;
		(*(Vector2_t2243707579 *)((L_191)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_192;
		Vector2U5BU5D_t686124026* L_193 = L_191;
		NullCheck(L_193);
		Vector2_t2243707579  L_194 = V_26;
		(*(Vector2_t2243707579 *)((L_193)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_194;
		Vector2U5BU5D_t686124026* L_195 = V_33;
		UILineRenderer_SetVbo_m858010382(__this, L_186, L_193, L_195, /*hidden argument*/NULL);
		Vector2_t2243707579  L_196 = V_25;
		V_17 = L_196;
		Vector2_t2243707579  L_197 = V_26;
		V_18 = L_197;
		int32_t L_198 = V_19;
		V_19 = ((int32_t)((int32_t)L_198+(int32_t)1));
	}

IL_064a:
	{
		int32_t L_199 = V_19;
		Vector2U5BU5D_t686124026* L_200 = V_16;
		NullCheck(L_200);
		if ((((int32_t)L_199) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_200)->max_length)))))))
		{
			goto IL_02b1;
		}
	}
	{
		return;
	}
}
// System.Void UILineRenderer::SetVbo(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Vector2[],UnityEngine.Vector2[])
extern Il2CppClass* UIVertex_t1204258818_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m3591975577_MethodInfo_var;
extern const uint32_t UILineRenderer_SetVbo_m858010382_MetadataUsageId;
extern "C"  void UILineRenderer_SetVbo_m858010382 (UILineRenderer_t1431191837 * __this, List_1_t573379950 * ___vbo0, Vector2U5BU5D_t686124026* ___vertices1, Vector2U5BU5D_t686124026* ___uvs2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UILineRenderer_SetVbo_m858010382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	UIVertex_t1204258818  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		V_0 = 0;
		goto IL_0055;
	}

IL_0007:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIVertex_t1204258818_il2cpp_TypeInfo_var);
		UIVertex_t1204258818  L_0 = ((UIVertex_t1204258818_StaticFields*)UIVertex_t1204258818_il2cpp_TypeInfo_var->static_fields)->get_simpleVert_8();
		V_1 = L_0;
		Color_t2020392075  L_1 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, __this);
		Color32_t874517518  L_2 = Color32_op_Implicit_m624191464(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		(&V_1)->set_color_2(L_2);
		Vector2U5BU5D_t686124026* L_3 = ___vertices1;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Vector3_t2243707580  L_5 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, (*(Vector2_t2243707579 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))), /*hidden argument*/NULL);
		(&V_1)->set_position_0(L_5);
		Vector2U5BU5D_t686124026* L_6 = ___uvs2;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		(&V_1)->set_uv0_3((*(Vector2_t2243707579 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)))));
		List_1_t573379950 * L_8 = ___vbo0;
		UIVertex_t1204258818  L_9 = V_1;
		NullCheck(L_8);
		List_1_Add_m3591975577(L_8, L_9, /*hidden argument*/List_1_Add_m3591975577_MethodInfo_var);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0055:
	{
		int32_t L_11 = V_0;
		Vector2U5BU5D_t686124026* L_12 = ___vertices1;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// UnityEngine.Vector3 UILineRenderer::RotatePointAroundPivot(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  UILineRenderer_RotatePointAroundPivot_m1756234985 (UILineRenderer_t1431191837 * __this, Vector3_t2243707580  ___point0, Vector3_t2243707580  ___pivot1, Vector3_t2243707580  ___angles2, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = ___point0;
		Vector3_t2243707580  L_1 = ___pivot1;
		Vector3_t2243707580  L_2 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = ___angles2;
		Quaternion_t4030073918  L_4 = Quaternion_Euler_m3586339259(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = V_0;
		Vector3_t2243707580  L_6 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector3_t2243707580  L_7 = V_0;
		Vector3_t2243707580  L_8 = ___pivot1;
		Vector3_t2243707580  L_9 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		___point0 = L_9;
		Vector3_t2243707580  L_10 = ___point0;
		return L_10;
	}
}
// System.Void winner::.ctor()
extern "C"  void winner__ctor_m1117566382 (winner_t1142905907 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void winner::Awake()
extern const MethodInfo* Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2660685848;
extern Il2CppCodeGenString* _stringLiteral2660682713;
extern Il2CppCodeGenString* _stringLiteral1745960738;
extern Il2CppCodeGenString* _stringLiteral732917139;
extern const uint32_t winner_Awake_m3002997699_MetadataUsageId;
extern "C"  void winner_Awake_m3002997699 (winner_t1142905907 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (winner_Awake_m3002997699_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral2660685848, /*hidden argument*/NULL);
		int32_t L_1 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral2660682713, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_002e;
		}
	}
	{
		Text_t356221433 * L_2 = Component_GetComponent_TisText_t356221433_m1342661039(__this, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, _stringLiteral1745960738);
		goto IL_0057;
	}

IL_002e:
	{
		int32_t L_3 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral2660682713, /*hidden argument*/NULL);
		int32_t L_4 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral2660685848, /*hidden argument*/NULL);
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_0057;
		}
	}
	{
		Text_t356221433 * L_5 = Component_GetComponent_TisText_t356221433_m1342661039(__this, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteral732917139);
	}

IL_0057:
	{
		return;
	}
}
// System.Void winner::Start()
extern "C"  void winner_Start_m2311782962 (winner_t1142905907 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void winner::Update()
extern "C"  void winner_Update_m1183961471 (winner_t1142905907 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
