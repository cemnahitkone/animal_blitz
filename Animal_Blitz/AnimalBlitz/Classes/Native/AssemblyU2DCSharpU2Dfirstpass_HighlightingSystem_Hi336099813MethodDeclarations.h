﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HighlightingSystem.HighlightingBase
struct HighlightingBase_t336099813;
// HighlightingSystem.HighlightingBlitter
struct HighlightingBlitter_t2949110578;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Camera
struct Camera_t189460977;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H2949110578.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"

// System.Void HighlightingSystem.HighlightingBase::.ctor()
extern "C"  void HighlightingBase__ctor_m1751304723 (HighlightingBase_t336099813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HighlightingSystem.HighlightingBase::get_uvStartsAtTop()
extern "C"  bool HighlightingBase_get_uvStartsAtTop_m1786397426 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HighlightingSystem.HighlightingBase::get_isSupported()
extern "C"  bool HighlightingBase_get_isSupported_m1060771174 (HighlightingBase_t336099813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HighlightingSystem.HighlightingBase::get_downsampleFactor()
extern "C"  int32_t HighlightingBase_get_downsampleFactor_m3447051165 (HighlightingBase_t336099813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBase::set_downsampleFactor(System.Int32)
extern "C"  void HighlightingBase_set_downsampleFactor_m4056539428 (HighlightingBase_t336099813 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HighlightingSystem.HighlightingBase::get_iterations()
extern "C"  int32_t HighlightingBase_get_iterations_m553206698 (HighlightingBase_t336099813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBase::set_iterations(System.Int32)
extern "C"  void HighlightingBase_set_iterations_m1116507547 (HighlightingBase_t336099813 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HighlightingSystem.HighlightingBase::get_blurMinSpread()
extern "C"  float HighlightingBase_get_blurMinSpread_m1916237220 (HighlightingBase_t336099813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBase::set_blurMinSpread(System.Single)
extern "C"  void HighlightingBase_set_blurMinSpread_m1361799121 (HighlightingBase_t336099813 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HighlightingSystem.HighlightingBase::get_blurSpread()
extern "C"  float HighlightingBase_get_blurSpread_m3339261904 (HighlightingBase_t336099813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBase::set_blurSpread(System.Single)
extern "C"  void HighlightingBase_set_blurSpread_m175801855 (HighlightingBase_t336099813 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HighlightingSystem.HighlightingBase::get_blurIntensity()
extern "C"  float HighlightingBase_get_blurIntensity_m4144433266 (HighlightingBase_t336099813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBase::set_blurIntensity(System.Single)
extern "C"  void HighlightingBase_set_blurIntensity_m4010025897 (HighlightingBase_t336099813 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HighlightingSystem.HighlightingBlitter HighlightingSystem.HighlightingBase::get_blitter()
extern "C"  HighlightingBlitter_t2949110578 * HighlightingBase_get_blitter_m2498396404 (HighlightingBase_t336099813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBase::set_blitter(HighlightingSystem.HighlightingBlitter)
extern "C"  void HighlightingBase_set_blitter_m288229937 (HighlightingBase_t336099813 * __this, HighlightingBlitter_t2949110578 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBase::OnEnable()
extern "C"  void HighlightingBase_OnEnable_m21076391 (HighlightingBase_t336099813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBase::OnDisable()
extern "C"  void HighlightingBase_OnDisable_m3519437558 (HighlightingBase_t336099813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBase::OnPreRender()
extern "C"  void HighlightingBase_OnPreRender_m4139244023 (HighlightingBase_t336099813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBase::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void HighlightingBase_OnRenderImage_m2221789855 (HighlightingBase_t336099813 * __this, RenderTexture_t2666733923 * ___src0, RenderTexture_t2666733923 * ___dst1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HighlightingSystem.HighlightingBase::IsHighlightingCamera(UnityEngine.Camera)
extern "C"  bool HighlightingBase_IsHighlightingCamera_m2166075236 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBase::Initialize()
extern "C"  void HighlightingBase_Initialize_m1961791279 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBase::CreateQuad()
extern "C"  void HighlightingBase_CreateQuad_m1421589878 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HighlightingSystem.HighlightingBase::GetAA()
extern "C"  int32_t HighlightingBase_GetAA_m3391488427 (HighlightingBase_t336099813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HighlightingSystem.HighlightingBase::CheckSupported(System.Boolean)
extern "C"  bool HighlightingBase_CheckSupported_m3712654012 (HighlightingBase_t336099813 * __this, bool ___verbose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBase::RebuildCommandBuffer()
extern "C"  void HighlightingBase_RebuildCommandBuffer_m3517084141 (HighlightingBase_t336099813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBase::Blit(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void HighlightingBase_Blit_m2022560864 (HighlightingBase_t336099813 * __this, RenderTexture_t2666733923 * ___src0, RenderTexture_t2666733923 * ___dst1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBase::.cctor()
extern "C"  void HighlightingBase__cctor_m2249983320 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
