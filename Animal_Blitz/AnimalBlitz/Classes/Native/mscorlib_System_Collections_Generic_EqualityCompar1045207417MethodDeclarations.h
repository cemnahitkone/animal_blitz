﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<HighlightingSystem.HighlighterRenderer/Data>
struct DefaultComparer_t1045207417;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H3718244552.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<HighlightingSystem.HighlighterRenderer/Data>::.ctor()
extern "C"  void DefaultComparer__ctor_m2158527414_gshared (DefaultComparer_t1045207417 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2158527414(__this, method) ((  void (*) (DefaultComparer_t1045207417 *, const MethodInfo*))DefaultComparer__ctor_m2158527414_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<HighlightingSystem.HighlighterRenderer/Data>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m338643181_gshared (DefaultComparer_t1045207417 * __this, Data_t3718244552  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m338643181(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1045207417 *, Data_t3718244552 , const MethodInfo*))DefaultComparer_GetHashCode_m338643181_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<HighlightingSystem.HighlighterRenderer/Data>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3556149453_gshared (DefaultComparer_t1045207417 * __this, Data_t3718244552  ___x0, Data_t3718244552  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3556149453(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1045207417 *, Data_t3718244552 , Data_t3718244552 , const MethodInfo*))DefaultComparer_Equals_m3556149453_gshared)(__this, ___x0, ___y1, method)
