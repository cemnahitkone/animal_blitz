﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.DotParams.SkyParam
struct SkyParam_t4272832432;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.DotParams.SkyParam::.ctor()
extern "C"  void SkyParam__ctor_m148478908 (SkyParam_t4272832432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
