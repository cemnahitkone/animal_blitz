﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2509106130MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<TrailElement>::.ctor()
#define Queue_1__ctor_m2885645460(__this, method) ((  void (*) (Queue_1_t1505052203 *, const MethodInfo*))Queue_1__ctor_m1845245813_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<TrailElement>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m2674034755(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t1505052203 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m772787461_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<TrailElement>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m1517500075(__this, method) ((  bool (*) (Queue_1_t1505052203 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m307125669_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<TrailElement>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m2636315299(__this, method) ((  Il2CppObject * (*) (Queue_1_t1505052203 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m311152041_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<TrailElement>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1398359311(__this, method) ((  Il2CppObject* (*) (Queue_1_t1505052203 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4179169157_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<TrailElement>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m250169574(__this, method) ((  Il2CppObject * (*) (Queue_1_t1505052203 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m251608368_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<TrailElement>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m3137045060(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t1505052203 *, TrailElementU5BU5D_t2201193657*, int32_t, const MethodInfo*))Queue_1_CopyTo_m1419617898_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<TrailElement>::Dequeue()
#define Queue_1_Dequeue_m2757116194(__this, method) ((  TrailElement_t1685395368 * (*) (Queue_1_t1505052203 *, const MethodInfo*))Queue_1_Dequeue_m4118160228_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<TrailElement>::Peek()
#define Queue_1_Peek_m2757710183(__this, method) ((  TrailElement_t1685395368 * (*) (Queue_1_t1505052203 *, const MethodInfo*))Queue_1_Peek_m1463479953_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<TrailElement>::Enqueue(T)
#define Queue_1_Enqueue_m507665509(__this, ___item0, method) ((  void (*) (Queue_1_t1505052203 *, TrailElement_t1685395368 *, const MethodInfo*))Queue_1_Enqueue_m2424099095_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<TrailElement>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m3983042760(__this, ___new_size0, method) ((  void (*) (Queue_1_t1505052203 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3858927842_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<TrailElement>::get_Count()
#define Queue_1_get_Count_m353297782(__this, method) ((  int32_t (*) (Queue_1_t1505052203 *, const MethodInfo*))Queue_1_get_Count_m3795587777_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<TrailElement>::GetEnumerator()
#define Queue_1_GetEnumerator_m1731654386(__this, method) ((  Enumerator_t2015115283  (*) (Queue_1_t1505052203 *, const MethodInfo*))Queue_1_GetEnumerator_m2842671368_gshared)(__this, method)
