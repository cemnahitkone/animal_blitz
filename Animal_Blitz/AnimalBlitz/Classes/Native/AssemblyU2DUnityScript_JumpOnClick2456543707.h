﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t189460977;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JumpOnClick
struct  JumpOnClick_t2456543707  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera JumpOnClick::sceneCamera
	Camera_t189460977 * ___sceneCamera_2;
	// System.Single JumpOnClick::groundY
	float ___groundY_3;
	// System.Single JumpOnClick::ySpeed
	float ___ySpeed_4;

public:
	inline static int32_t get_offset_of_sceneCamera_2() { return static_cast<int32_t>(offsetof(JumpOnClick_t2456543707, ___sceneCamera_2)); }
	inline Camera_t189460977 * get_sceneCamera_2() const { return ___sceneCamera_2; }
	inline Camera_t189460977 ** get_address_of_sceneCamera_2() { return &___sceneCamera_2; }
	inline void set_sceneCamera_2(Camera_t189460977 * value)
	{
		___sceneCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___sceneCamera_2, value);
	}

	inline static int32_t get_offset_of_groundY_3() { return static_cast<int32_t>(offsetof(JumpOnClick_t2456543707, ___groundY_3)); }
	inline float get_groundY_3() const { return ___groundY_3; }
	inline float* get_address_of_groundY_3() { return &___groundY_3; }
	inline void set_groundY_3(float value)
	{
		___groundY_3 = value;
	}

	inline static int32_t get_offset_of_ySpeed_4() { return static_cast<int32_t>(offsetof(JumpOnClick_t2456543707, ___ySpeed_4)); }
	inline float get_ySpeed_4() const { return ___ySpeed_4; }
	inline float* get_address_of_ySpeed_4() { return &___ySpeed_4; }
	inline void set_ySpeed_4(float value)
	{
		___ySpeed_4 = value;
	}
};

struct JumpOnClick_t2456543707_StaticFields
{
public:
	// System.Single JumpOnClick::gravity
	float ___gravity_5;
	// System.Single JumpOnClick::jumpSpeed
	float ___jumpSpeed_6;

public:
	inline static int32_t get_offset_of_gravity_5() { return static_cast<int32_t>(offsetof(JumpOnClick_t2456543707_StaticFields, ___gravity_5)); }
	inline float get_gravity_5() const { return ___gravity_5; }
	inline float* get_address_of_gravity_5() { return &___gravity_5; }
	inline void set_gravity_5(float value)
	{
		___gravity_5 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_6() { return static_cast<int32_t>(offsetof(JumpOnClick_t2456543707_StaticFields, ___jumpSpeed_6)); }
	inline float get_jumpSpeed_6() const { return ___jumpSpeed_6; }
	inline float* get_address_of_jumpSpeed_6() { return &___jumpSpeed_6; }
	inline void set_jumpSpeed_6(float value)
	{
		___jumpSpeed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
