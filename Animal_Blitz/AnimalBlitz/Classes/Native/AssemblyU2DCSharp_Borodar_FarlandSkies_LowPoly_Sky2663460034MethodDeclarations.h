﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle
struct SkyboxDayNightCycle_t2663460034;
// Borodar.FarlandSkies.LowPoly.DotParams.SkyParam
struct SkyParam_t4272832432;
// Borodar.FarlandSkies.LowPoly.DotParams.StarsParam
struct StarsParam_t907828490;
// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam
struct CelestialParam_t4052651973;
// Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam
struct CloudsParam_t3937665775;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot4272832432.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_DotP907828490.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot4052651973.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot3937665775.h"

// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::.ctor()
extern "C"  void SkyboxDayNightCycle__ctor_m2751430261 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::get_TimeOfDay()
extern "C"  float SkyboxDayNightCycle_get_TimeOfDay_m956768922 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::set_TimeOfDay(System.Single)
extern "C"  void SkyboxDayNightCycle_set_TimeOfDay_m3892010369 (SkyboxDayNightCycle_t2663460034 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Borodar.FarlandSkies.LowPoly.DotParams.SkyParam Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::get_CurrentSkyParam()
extern "C"  SkyParam_t4272832432 * SkyboxDayNightCycle_get_CurrentSkyParam_m820157601 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::set_CurrentSkyParam(Borodar.FarlandSkies.LowPoly.DotParams.SkyParam)
extern "C"  void SkyboxDayNightCycle_set_CurrentSkyParam_m4035644430 (SkyboxDayNightCycle_t2663460034 * __this, SkyParam_t4272832432 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Borodar.FarlandSkies.LowPoly.DotParams.StarsParam Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::get_CurrentStarsParam()
extern "C"  StarsParam_t907828490 * SkyboxDayNightCycle_get_CurrentStarsParam_m1974840097 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::set_CurrentStarsParam(Borodar.FarlandSkies.LowPoly.DotParams.StarsParam)
extern "C"  void SkyboxDayNightCycle_set_CurrentStarsParam_m1815760878 (SkyboxDayNightCycle_t2663460034 * __this, StarsParam_t907828490 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::get_CurrentSunParam()
extern "C"  CelestialParam_t4052651973 * SkyboxDayNightCycle_get_CurrentSunParam_m2758135285 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::set_CurrentSunParam(Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam)
extern "C"  void SkyboxDayNightCycle_set_CurrentSunParam_m2519911578 (SkyboxDayNightCycle_t2663460034 * __this, CelestialParam_t4052651973 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::get_CurrentMoonParam()
extern "C"  CelestialParam_t4052651973 * SkyboxDayNightCycle_get_CurrentMoonParam_m2638489792 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::set_CurrentMoonParam(Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam)
extern "C"  void SkyboxDayNightCycle_set_CurrentMoonParam_m2985111357 (SkyboxDayNightCycle_t2663460034 * __this, CelestialParam_t4052651973 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::get_CurrentCloudsParam()
extern "C"  CloudsParam_t3937665775 * SkyboxDayNightCycle_get_CurrentCloudsParam_m2368674121 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::set_CurrentCloudsParam(Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam)
extern "C"  void SkyboxDayNightCycle_set_CurrentCloudsParam_m2382747670 (SkyboxDayNightCycle_t2663460034 * __this, CloudsParam_t3937665775 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::Awake()
extern "C"  void SkyboxDayNightCycle_Awake_m339890626 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::Start()
extern "C"  void SkyboxDayNightCycle_Start_m3499042805 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::Update()
extern "C"  void SkyboxDayNightCycle_Update_m407040532 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::OnValidate()
extern "C"  void SkyboxDayNightCycle_OnValidate_m3860465794 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::UpdateSky()
extern "C"  void SkyboxDayNightCycle_UpdateSky_m605364337 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::UpdateStars()
extern "C"  void SkyboxDayNightCycle_UpdateStars_m4139585559 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::UpdateSun()
extern "C"  void SkyboxDayNightCycle_UpdateSun_m693082028 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::UpdateMoon()
extern "C"  void SkyboxDayNightCycle_UpdateMoon_m1396469115 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::UpdateClouds()
extern "C"  void SkyboxDayNightCycle_UpdateClouds_m3440957838 (SkyboxDayNightCycle_t2663460034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
