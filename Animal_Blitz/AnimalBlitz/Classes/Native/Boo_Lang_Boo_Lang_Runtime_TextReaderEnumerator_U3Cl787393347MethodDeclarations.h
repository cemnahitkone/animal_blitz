﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD
struct U3ClinesU3Ec__IteratorD_t787393347;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t3799711356;

#include "codegen/il2cpp-codegen.h"

// System.Void Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::.ctor()
extern "C"  void U3ClinesU3Ec__IteratorD__ctor_m3937265136 (U3ClinesU3Ec__IteratorD_t787393347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::System.Collections.Generic.IEnumerator<string>.get_Current()
extern "C"  String_t* U3ClinesU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CstringU3E_get_Current_m2700997798 (U3ClinesU3Ec__IteratorD_t787393347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3ClinesU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1412468734 (U3ClinesU3Ec__IteratorD_t787393347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3ClinesU3Ec__IteratorD_System_Collections_IEnumerable_GetEnumerator_m339387867 (U3ClinesU3Ec__IteratorD_t787393347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.String> Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::System.Collections.Generic.IEnumerable<string>.GetEnumerator()
extern "C"  Il2CppObject* U3ClinesU3Ec__IteratorD_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m96088709 (U3ClinesU3Ec__IteratorD_t787393347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::MoveNext()
extern "C"  bool U3ClinesU3Ec__IteratorD_MoveNext_m4023992000 (U3ClinesU3Ec__IteratorD_t787393347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::Dispose()
extern "C"  void U3ClinesU3Ec__IteratorD_Dispose_m705472977 (U3ClinesU3Ec__IteratorD_t787393347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::Reset()
extern "C"  void U3ClinesU3Ec__IteratorD_Reset_m1083540087 (U3ClinesU3Ec__IteratorD_t787393347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
