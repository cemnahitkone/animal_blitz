﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BluetoothLEHardwareInterface
struct BluetoothLEHardwareInterface_t4023789856;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// BluetoothDeviceScript
struct BluetoothDeviceScript_t810775987;
// System.Action
struct Action_t3226471752;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Action`2<System.String,System.String>
struct Action_2_t4234541925;
// System.Action`4<System.String,System.String,System.Int32,System.Byte[]>
struct Action_4_t2314457268;
// System.Action`3<System.String,System.String,System.String>
struct Action_3_t3256166369;
// System.Action`2<System.String,System.Byte[]>
struct Action_2_t1307688409;
// System.Action`3<System.String,System.String,System.Byte[]>
struct Action_3_t329312853;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothLEHardwareI1474803024.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothLEHardwareI1741997128.h"

// System.Void BluetoothLEHardwareInterface::.ctor()
extern "C"  void BluetoothLEHardwareInterface__ctor_m1277440725 (BluetoothLEHardwareInterface_t4023789856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLELog(System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLELog_m3965866644 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEInitialize(System.Boolean,System.Boolean)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEInitialize_m4214605180 (Il2CppObject * __this /* static, unused */, bool ___asCentral0, bool ___asPeripheral1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEDeInitialize()
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEDeInitialize_m1183275517 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEPauseMessages(System.Boolean)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEPauseMessages_m12473301 (Il2CppObject * __this /* static, unused */, bool ___isPaused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEScanForPeripheralsWithServices(System.String,System.Boolean)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEScanForPeripheralsWithServices_m1764614798 (Il2CppObject * __this /* static, unused */, String_t* ___serviceUUIDsString0, bool ___allowDuplicates1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERetrieveListOfPeripheralsWithServices(System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLERetrieveListOfPeripheralsWithServices_m143887212 (Il2CppObject * __this /* static, unused */, String_t* ___serviceUUIDsString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEStopScan()
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEStopScan_m107073007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEConnectToPeripheral(System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEConnectToPeripheral_m1210203199 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEDisconnectPeripheral(System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEDisconnectPeripheral_m2753876142 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEReadCharacteristic(System.String,System.String,System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEReadCharacteristic_m1988194505 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEWriteCharacteristic(System.String,System.String,System.String,System.Byte[],System.Int32,System.Boolean)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEWriteCharacteristic_m2337396763 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, ByteU5BU5D_t3397334013* ___data3, int32_t ___length4, bool ___withResponse5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLESubscribeCharacteristic(System.String,System.String,System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLESubscribeCharacteristic_m4162793143 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEUnSubscribeCharacteristic(System.String,System.String,System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEUnSubscribeCharacteristic_m69770214 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEPeripheralName(System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEPeripheralName_m3800903089 (Il2CppObject * __this /* static, unused */, String_t* ___newName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLECreateService(System.String,System.Boolean)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLECreateService_m1860289310 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, bool ___primary1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveService(System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLERemoveService_m3534109365 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveServices()
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLERemoveServices_m3994765694 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLECreateCharacteristic(System.String,System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLECreateCharacteristic_m1441784201 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, int32_t ___properties1, int32_t ___permissions2, ByteU5BU5D_t3397334013* ___data3, int32_t ___length4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveCharacteristic(System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLERemoveCharacteristic_m4251113393 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveCharacteristics()
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLERemoveCharacteristics_m3371340286 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEStartAdvertising()
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEStartAdvertising_m2391874196 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEStopAdvertising()
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEStopAdvertising_m2372726 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEUpdateCharacteristicValue(System.String,System.Byte[],System.Int32)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEUpdateCharacteristicValue_m85558683 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, ByteU5BU5D_t3397334013* ___data1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::Log(System.String)
extern "C"  void BluetoothLEHardwareInterface_Log_m3949775483 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BluetoothDeviceScript BluetoothLEHardwareInterface::Initialize(System.Boolean,System.Boolean,System.Action,System.Action`1<System.String>)
extern "C"  BluetoothDeviceScript_t810775987 * BluetoothLEHardwareInterface_Initialize_m3691300628 (Il2CppObject * __this /* static, unused */, bool ___asCentral0, bool ___asPeripheral1, Action_t3226471752 * ___action2, Action_1_t1831019615 * ___errorAction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::DeInitialize(System.Action)
extern "C"  void BluetoothLEHardwareInterface_DeInitialize_m2251043401 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::FinishDeInitialize()
extern "C"  void BluetoothLEHardwareInterface_FinishDeInitialize_m3772842739 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::PauseMessages(System.Boolean)
extern "C"  void BluetoothLEHardwareInterface_PauseMessages_m4020506122 (Il2CppObject * __this /* static, unused */, bool ___isPaused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::ScanForPeripheralsWithServices(System.String[],System.Action`2<System.String,System.String>,System.Action`4<System.String,System.String,System.Int32,System.Byte[]>)
extern "C"  void BluetoothLEHardwareInterface_ScanForPeripheralsWithServices_m3085173196 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___serviceUUIDs0, Action_2_t4234541925 * ___action1, Action_4_t2314457268 * ___actionAdvertisingInfo2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::RetrieveListOfPeripheralsWithServices(System.String[],System.Action`2<System.String,System.String>)
extern "C"  void BluetoothLEHardwareInterface_RetrieveListOfPeripheralsWithServices_m3880900408 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___serviceUUIDs0, Action_2_t4234541925 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::StopScan()
extern "C"  void BluetoothLEHardwareInterface_StopScan_m1034974532 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::ConnectToPeripheral(System.String,System.Action`1<System.String>,System.Action`2<System.String,System.String>,System.Action`3<System.String,System.String,System.String>,System.Action`1<System.String>)
extern "C"  void BluetoothLEHardwareInterface_ConnectToPeripheral_m2997112613 (Il2CppObject * __this /* static, unused */, String_t* ___name0, Action_1_t1831019615 * ___connectAction1, Action_2_t4234541925 * ___serviceAction2, Action_3_t3256166369 * ___characteristicAction3, Action_1_t1831019615 * ___disconnectAction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::DisconnectPeripheral(System.String,System.Action`1<System.String>)
extern "C"  void BluetoothLEHardwareInterface_DisconnectPeripheral_m3085347731 (Il2CppObject * __this /* static, unused */, String_t* ___name0, Action_1_t1831019615 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::ReadCharacteristic(System.String,System.String,System.String,System.Action`2<System.String,System.Byte[]>)
extern "C"  void BluetoothLEHardwareInterface_ReadCharacteristic_m2865661240 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, Action_2_t1307688409 * ___action3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::WriteCharacteristic(System.String,System.String,System.String,System.Byte[],System.Int32,System.Boolean,System.Action`1<System.String>)
extern "C"  void BluetoothLEHardwareInterface_WriteCharacteristic_m1354208428 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, ByteU5BU5D_t3397334013* ___data3, int32_t ___length4, bool ___withResponse5, Action_1_t1831019615 * ___action6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::SubscribeCharacteristic(System.String,System.String,System.String,System.Action`1<System.String>,System.Action`2<System.String,System.Byte[]>)
extern "C"  void BluetoothLEHardwareInterface_SubscribeCharacteristic_m3873716912 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, Action_1_t1831019615 * ___notificationAction3, Action_2_t1307688409 * ___action4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::SubscribeCharacteristicWithDeviceAddress(System.String,System.String,System.String,System.Action`2<System.String,System.String>,System.Action`3<System.String,System.String,System.Byte[]>)
extern "C"  void BluetoothLEHardwareInterface_SubscribeCharacteristicWithDeviceAddress_m3073680728 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, Action_2_t4234541925 * ___notificationAction3, Action_3_t329312853 * ___action4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::UnSubscribeCharacteristic(System.String,System.String,System.String,System.Action`1<System.String>)
extern "C"  void BluetoothLEHardwareInterface_UnSubscribeCharacteristic_m614746437 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, Action_1_t1831019615 * ___action3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::PeripheralName(System.String)
extern "C"  void BluetoothLEHardwareInterface_PeripheralName_m4089551076 (Il2CppObject * __this /* static, unused */, String_t* ___newName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::CreateService(System.String,System.Boolean,System.Action`1<System.String>)
extern "C"  void BluetoothLEHardwareInterface_CreateService_m4006704813 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, bool ___primary1, Action_1_t1831019615 * ___action2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::RemoveService(System.String)
extern "C"  void BluetoothLEHardwareInterface_RemoveService_m3808888640 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::RemoveServices()
extern "C"  void BluetoothLEHardwareInterface_RemoveServices_m399109141 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::CreateCharacteristic(System.String,BluetoothLEHardwareInterface/CBCharacteristicProperties,BluetoothLEHardwareInterface/CBAttributePermissions,System.Byte[],System.Int32,System.Action`2<System.String,System.Byte[]>)
extern "C"  void BluetoothLEHardwareInterface_CreateCharacteristic_m1598497468 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, int32_t ___properties1, int32_t ___permissions2, ByteU5BU5D_t3397334013* ___data3, int32_t ___length4, Action_2_t1307688409 * ___action5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::RemoveCharacteristic(System.String)
extern "C"  void BluetoothLEHardwareInterface_RemoveCharacteristic_m3689193062 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::RemoveCharacteristics()
extern "C"  void BluetoothLEHardwareInterface_RemoveCharacteristics_m2071299125 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::StartAdvertising(System.Action)
extern "C"  void BluetoothLEHardwareInterface_StartAdvertising_m2707254606 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::StopAdvertising(System.Action)
extern "C"  void BluetoothLEHardwareInterface_StopAdvertising_m1625577478 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothLEHardwareInterface::UpdateCharacteristicValue(System.String,System.Byte[],System.Int32)
extern "C"  void BluetoothLEHardwareInterface_UpdateCharacteristicValue_m1021036518 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, ByteU5BU5D_t3397334013* ___data1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
