﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Ricimi.AnimatedButton/ButtonClickedEvent
struct ButtonClickedEvent_t1021215292;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.AnimatedButton
struct  AnimatedButton_t4037967263  : public UIBehaviour_t3960014691
{
public:
	// System.Boolean Ricimi.AnimatedButton::interactable
	bool ___interactable_2;
	// Ricimi.AnimatedButton/ButtonClickedEvent Ricimi.AnimatedButton::m_OnClick
	ButtonClickedEvent_t1021215292 * ___m_OnClick_3;
	// UnityEngine.Animator Ricimi.AnimatedButton::m_animator
	Animator_t69676727 * ___m_animator_4;

public:
	inline static int32_t get_offset_of_interactable_2() { return static_cast<int32_t>(offsetof(AnimatedButton_t4037967263, ___interactable_2)); }
	inline bool get_interactable_2() const { return ___interactable_2; }
	inline bool* get_address_of_interactable_2() { return &___interactable_2; }
	inline void set_interactable_2(bool value)
	{
		___interactable_2 = value;
	}

	inline static int32_t get_offset_of_m_OnClick_3() { return static_cast<int32_t>(offsetof(AnimatedButton_t4037967263, ___m_OnClick_3)); }
	inline ButtonClickedEvent_t1021215292 * get_m_OnClick_3() const { return ___m_OnClick_3; }
	inline ButtonClickedEvent_t1021215292 ** get_address_of_m_OnClick_3() { return &___m_OnClick_3; }
	inline void set_m_OnClick_3(ButtonClickedEvent_t1021215292 * value)
	{
		___m_OnClick_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnClick_3, value);
	}

	inline static int32_t get_offset_of_m_animator_4() { return static_cast<int32_t>(offsetof(AnimatedButton_t4037967263, ___m_animator_4)); }
	inline Animator_t69676727 * get_m_animator_4() const { return ___m_animator_4; }
	inline Animator_t69676727 ** get_address_of_m_animator_4() { return &___m_animator_4; }
	inline void set_m_animator_4(Animator_t69676727 * value)
	{
		___m_animator_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_animator_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
