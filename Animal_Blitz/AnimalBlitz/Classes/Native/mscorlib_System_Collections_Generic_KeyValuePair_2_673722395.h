﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Borodar.FarlandSkies.LowPoly.DotParams.MoonParam
struct MoonParam_t2066856486;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.MoonParam>
struct  KeyValuePair_2_t673722395 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	float ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	MoonParam_t2066856486 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t673722395, ___key_0)); }
	inline float get_key_0() const { return ___key_0; }
	inline float* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(float value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t673722395, ___value_1)); }
	inline MoonParam_t2066856486 * get_value_1() const { return ___value_1; }
	inline MoonParam_t2066856486 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(MoonParam_t2066856486 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
