﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.Popup
struct Popup_t1187979376;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.Popup::.ctor()
extern "C"  void Popup__ctor_m3235964294 (Popup_t1187979376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.Popup::Open()
extern "C"  void Popup_Open_m2847985796 (Popup_t1187979376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.Popup::Close()
extern "C"  void Popup_Close_m2952563078 (Popup_t1187979376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Ricimi.Popup::RunPopupDestroy()
extern "C"  Il2CppObject * Popup_RunPopupDestroy_m2461139149 (Popup_t1187979376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.Popup::AddBackground()
extern "C"  void Popup_AddBackground_m1648822605 (Popup_t1187979376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.Popup::RemoveBackground()
extern "C"  void Popup_RemoveBackground_m4200900992 (Popup_t1187979376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
