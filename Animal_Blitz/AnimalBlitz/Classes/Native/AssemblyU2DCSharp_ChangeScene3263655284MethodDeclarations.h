﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangeScene
struct ChangeScene_t3263655284;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChangeScene::.ctor()
extern "C"  void ChangeScene__ctor_m4267203671 (ChangeScene_t3263655284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeScene::GoToScene(System.String)
extern "C"  void ChangeScene_GoToScene_m1587160876 (ChangeScene_t3263655284 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
