﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// tBase
struct tBase_t1720960579;

#include "codegen/il2cpp-codegen.h"

// System.Void tBase::.ctor()
extern "C"  void tBase__ctor_m4144850876 (tBase_t1720960579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
