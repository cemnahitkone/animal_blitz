﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator0
struct U3CRandomSpawnsCoroutineU3Ec__Iterator0_t4111098856;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator0::.ctor()
extern "C"  void U3CRandomSpawnsCoroutineU3Ec__Iterator0__ctor_m1737243277 (U3CRandomSpawnsCoroutineU3Ec__Iterator0_t4111098856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator0::MoveNext()
extern "C"  bool U3CRandomSpawnsCoroutineU3Ec__Iterator0_MoveNext_m1465278695 (U3CRandomSpawnsCoroutineU3Ec__Iterator0_t4111098856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRandomSpawnsCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1374486691 (U3CRandomSpawnsCoroutineU3Ec__Iterator0_t4111098856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRandomSpawnsCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1903938107 (U3CRandomSpawnsCoroutineU3Ec__Iterator0_t4111098856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator0::Dispose()
extern "C"  void U3CRandomSpawnsCoroutineU3Ec__Iterator0_Dispose_m2623684114 (U3CRandomSpawnsCoroutineU3Ec__Iterator0_t4111098856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX2_Demo/<RandomSpawnsCoroutine>c__Iterator0::Reset()
extern "C"  void U3CRandomSpawnsCoroutineU3Ec__Iterator0_Reset_m2335936464 (U3CRandomSpawnsCoroutineU3Ec__Iterator0_t4111098856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
