﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.MusicManager
struct  MusicManager_t1668724344  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Slider Ricimi.MusicManager::m_musicSlider
	Slider_t297367283 * ___m_musicSlider_2;
	// UnityEngine.GameObject Ricimi.MusicManager::m_musicButton
	GameObject_t1756533147 * ___m_musicButton_3;

public:
	inline static int32_t get_offset_of_m_musicSlider_2() { return static_cast<int32_t>(offsetof(MusicManager_t1668724344, ___m_musicSlider_2)); }
	inline Slider_t297367283 * get_m_musicSlider_2() const { return ___m_musicSlider_2; }
	inline Slider_t297367283 ** get_address_of_m_musicSlider_2() { return &___m_musicSlider_2; }
	inline void set_m_musicSlider_2(Slider_t297367283 * value)
	{
		___m_musicSlider_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_musicSlider_2, value);
	}

	inline static int32_t get_offset_of_m_musicButton_3() { return static_cast<int32_t>(offsetof(MusicManager_t1668724344, ___m_musicButton_3)); }
	inline GameObject_t1756533147 * get_m_musicButton_3() const { return ___m_musicButton_3; }
	inline GameObject_t1756533147 ** get_address_of_m_musicButton_3() { return &___m_musicButton_3; }
	inline void set_m_musicButton_3(GameObject_t1756533147 * value)
	{
		___m_musicButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_musicButton_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
