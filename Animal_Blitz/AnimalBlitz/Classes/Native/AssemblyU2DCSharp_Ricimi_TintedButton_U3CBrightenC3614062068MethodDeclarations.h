﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.TintedButton/<BrightenColor>c__Iterator1
struct U3CBrightenColorU3Ec__Iterator1_t3614062068;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.TintedButton/<BrightenColor>c__Iterator1::.ctor()
extern "C"  void U3CBrightenColorU3Ec__Iterator1__ctor_m2734970453 (U3CBrightenColorU3Ec__Iterator1_t3614062068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ricimi.TintedButton/<BrightenColor>c__Iterator1::MoveNext()
extern "C"  bool U3CBrightenColorU3Ec__Iterator1_MoveNext_m3272917731 (U3CBrightenColorU3Ec__Iterator1_t3614062068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Ricimi.TintedButton/<BrightenColor>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBrightenColorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2455852111 (U3CBrightenColorU3Ec__Iterator1_t3614062068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Ricimi.TintedButton/<BrightenColor>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBrightenColorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2386766583 (U3CBrightenColorU3Ec__Iterator1_t3614062068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.TintedButton/<BrightenColor>c__Iterator1::Dispose()
extern "C"  void U3CBrightenColorU3Ec__Iterator1_Dispose_m4183605614 (U3CBrightenColorU3Ec__Iterator1_t3614062068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.TintedButton/<BrightenColor>c__Iterator1::Reset()
extern "C"  void U3CBrightenColorU3Ec__Iterator1_Reset_m3398001632 (U3CBrightenColorU3Ec__Iterator1_t3614062068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
