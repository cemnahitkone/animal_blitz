﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// testBle
struct testBle_t3025851879;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void testBle::.ctor()
extern "C"  void testBle__ctor_m2665862840 (testBle_t3025851879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::Start()
extern "C"  void testBle_Start_m1342812652 (testBle_t3025851879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::Connect()
extern "C"  void testBle_Connect_m3205883302 (testBle_t3025851879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean testBle::IsEqual(System.String,System.String)
extern "C"  bool testBle_IsEqual_m3590934016 (testBle_t3025851879 * __this, String_t* ___uuid10, String_t* ___uuid21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::SearchAndConnectDevice()
extern "C"  void testBle_SearchAndConnectDevice_m66085037 (testBle_t3025851879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String testBle::getBleData()
extern "C"  String_t* testBle_getBleData_m3947189216 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 testBle::get_uniqId()
extern "C"  int32_t testBle_get_uniqId_m3823478467 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String testBle::changeDataFormat(System.Byte[])
extern "C"  String_t* testBle_changeDataFormat_m2934963405 (testBle_t3025851879 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::SendBytes(System.Byte[])
extern "C"  void testBle_SendBytes_m3832945432 (testBle_t3025851879 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::SendString(System.String)
extern "C"  void testBle_SendString_m3395385065 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::Update()
extern "C"  void testBle_Update_m3165031635 (testBle_t3025851879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::.cctor()
extern "C"  void testBle__cctor_m889340189 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::<Start>m__0()
extern "C"  void testBle_U3CStartU3Em__0_m1223780933 (testBle_t3025851879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::<Start>m__1(System.String)
extern "C"  void testBle_U3CStartU3Em__1_m3580770748 (Il2CppObject * __this /* static, unused */, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::<Connect>m__2(System.String)
extern "C"  void testBle_U3CConnectU3Em__2_m586010835 (Il2CppObject * __this /* static, unused */, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::<Connect>m__3(System.String,System.String)
extern "C"  void testBle_U3CConnectU3Em__3_m4101113984 (Il2CppObject * __this /* static, unused */, String_t* ___address0, String_t* ___serviceUUID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::<Connect>m__4(System.String,System.String,System.String)
extern "C"  void testBle_U3CConnectU3Em__4_m2263236401 (testBle_t3025851879 * __this, String_t* ___address0, String_t* ___serviceUUID1, String_t* ___characteristicUUID2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::<Connect>m__5(System.String)
extern "C"  void testBle_U3CConnectU3Em__5_m978557238 (testBle_t3025851879 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::<SearchAndConnectDevice>m__6(System.String,System.String)
extern "C"  void testBle_U3CSearchAndConnectDeviceU3Em__6_m3612029114 (testBle_t3025851879 * __this, String_t* ___address0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::<SearchAndConnectDevice>m__7(System.String,System.String,System.Int32,System.Byte[])
extern "C"  void testBle_U3CSearchAndConnectDeviceU3Em__7_m1789553411 (Il2CppObject * __this /* static, unused */, String_t* ___address0, String_t* ___name1, int32_t ___rssi2, ByteU5BU5D_t3397334013* ___advertisingInfo3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::<SendBytes>m__8(System.String)
extern "C"  void testBle_U3CSendBytesU3Em__8_m1750528736 (Il2CppObject * __this /* static, unused */, String_t* ___characteristicUUID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::<SendString>m__9(System.String)
extern "C"  void testBle_U3CSendStringU3Em__9_m2680849211 (Il2CppObject * __this /* static, unused */, String_t* ___characteristicUUID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle::<Connect>m__A(System.String,System.String)
extern "C"  void testBle_U3CConnectU3Em__A_m2995414874 (Il2CppObject * __this /* static, unused */, String_t* ___deviceAddress0, String_t* ___notification1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
