﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Rotate
struct Rotate_t4255939432;

#include "codegen/il2cpp-codegen.h"

// System.Void Rotate::.ctor()
extern "C"  void Rotate__ctor_m1201418695 (Rotate_t4255939432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Rotate::Start()
extern "C"  void Rotate_Start_m1763714195 (Rotate_t4255939432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Rotate::Update()
extern "C"  void Rotate_Update_m3953539786 (Rotate_t4255939432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Rotate::Main()
extern "C"  void Rotate_Main_m236250746 (Rotate_t4255939432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
