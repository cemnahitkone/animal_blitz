﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator0
struct U3CCheckIfAliveU3Ec__Iterator0_t3107901010;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator0::.ctor()
extern "C"  void U3CCheckIfAliveU3Ec__Iterator0__ctor_m1674228791 (U3CCheckIfAliveU3Ec__Iterator0_t3107901010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator0::MoveNext()
extern "C"  bool U3CCheckIfAliveU3Ec__Iterator0_MoveNext_m3959115781 (U3CCheckIfAliveU3Ec__Iterator0_t3107901010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckIfAliveU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3584371309 (U3CCheckIfAliveU3Ec__Iterator0_t3107901010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckIfAliveU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2417949397 (U3CCheckIfAliveU3Ec__Iterator0_t3107901010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator0::Dispose()
extern "C"  void U3CCheckIfAliveU3Ec__Iterator0_Dispose_m3933216764 (U3CCheckIfAliveU3Ec__Iterator0_t3107901010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator0::Reset()
extern "C"  void U3CCheckIfAliveU3Ec__Iterator0_Reset_m1725830842 (U3CCheckIfAliveU3Ec__Iterator0_t3107901010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
