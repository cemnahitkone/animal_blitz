﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule
struct LimitVelocityOverLifetimeModule_t1878180938;
struct LimitVelocityOverLifetimeModule_t1878180938_marshaled_pinvoke;
struct LimitVelocityOverLifetimeModule_t1878180938_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_LimitVeloci1878180938.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void LimitVelocityOverLifetimeModule__ctor_m3669423446 (LimitVelocityOverLifetimeModule_t1878180938 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct LimitVelocityOverLifetimeModule_t1878180938;
struct LimitVelocityOverLifetimeModule_t1878180938_marshaled_pinvoke;

extern "C" void LimitVelocityOverLifetimeModule_t1878180938_marshal_pinvoke(const LimitVelocityOverLifetimeModule_t1878180938& unmarshaled, LimitVelocityOverLifetimeModule_t1878180938_marshaled_pinvoke& marshaled);
extern "C" void LimitVelocityOverLifetimeModule_t1878180938_marshal_pinvoke_back(const LimitVelocityOverLifetimeModule_t1878180938_marshaled_pinvoke& marshaled, LimitVelocityOverLifetimeModule_t1878180938& unmarshaled);
extern "C" void LimitVelocityOverLifetimeModule_t1878180938_marshal_pinvoke_cleanup(LimitVelocityOverLifetimeModule_t1878180938_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct LimitVelocityOverLifetimeModule_t1878180938;
struct LimitVelocityOverLifetimeModule_t1878180938_marshaled_com;

extern "C" void LimitVelocityOverLifetimeModule_t1878180938_marshal_com(const LimitVelocityOverLifetimeModule_t1878180938& unmarshaled, LimitVelocityOverLifetimeModule_t1878180938_marshaled_com& marshaled);
extern "C" void LimitVelocityOverLifetimeModule_t1878180938_marshal_com_back(const LimitVelocityOverLifetimeModule_t1878180938_marshaled_com& marshaled, LimitVelocityOverLifetimeModule_t1878180938& unmarshaled);
extern "C" void LimitVelocityOverLifetimeModule_t1878180938_marshal_com_cleanup(LimitVelocityOverLifetimeModule_t1878180938_marshaled_com& marshaled);
