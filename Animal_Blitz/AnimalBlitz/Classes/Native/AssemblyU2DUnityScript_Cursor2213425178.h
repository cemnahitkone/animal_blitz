﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cursor
struct  Cursor_t2213425178  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera Cursor::sceneCamera
	Camera_t189460977 * ___sceneCamera_2;
	// UnityEngine.Vector2 Cursor::offset
	Vector2_t2243707579  ___offset_3;
	// UnityEngine.Texture Cursor::defaultTexture
	Texture_t2243626319 * ___defaultTexture_4;
	// UnityEngine.Texture Cursor::clickTexture
	Texture_t2243626319 * ___clickTexture_5;
	// System.Single Cursor::clickImageDuration
	float ___clickImageDuration_6;
	// System.Single Cursor::lastClickTime
	float ___lastClickTime_7;

public:
	inline static int32_t get_offset_of_sceneCamera_2() { return static_cast<int32_t>(offsetof(Cursor_t2213425178, ___sceneCamera_2)); }
	inline Camera_t189460977 * get_sceneCamera_2() const { return ___sceneCamera_2; }
	inline Camera_t189460977 ** get_address_of_sceneCamera_2() { return &___sceneCamera_2; }
	inline void set_sceneCamera_2(Camera_t189460977 * value)
	{
		___sceneCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___sceneCamera_2, value);
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(Cursor_t2213425178, ___offset_3)); }
	inline Vector2_t2243707579  get_offset_3() const { return ___offset_3; }
	inline Vector2_t2243707579 * get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(Vector2_t2243707579  value)
	{
		___offset_3 = value;
	}

	inline static int32_t get_offset_of_defaultTexture_4() { return static_cast<int32_t>(offsetof(Cursor_t2213425178, ___defaultTexture_4)); }
	inline Texture_t2243626319 * get_defaultTexture_4() const { return ___defaultTexture_4; }
	inline Texture_t2243626319 ** get_address_of_defaultTexture_4() { return &___defaultTexture_4; }
	inline void set_defaultTexture_4(Texture_t2243626319 * value)
	{
		___defaultTexture_4 = value;
		Il2CppCodeGenWriteBarrier(&___defaultTexture_4, value);
	}

	inline static int32_t get_offset_of_clickTexture_5() { return static_cast<int32_t>(offsetof(Cursor_t2213425178, ___clickTexture_5)); }
	inline Texture_t2243626319 * get_clickTexture_5() const { return ___clickTexture_5; }
	inline Texture_t2243626319 ** get_address_of_clickTexture_5() { return &___clickTexture_5; }
	inline void set_clickTexture_5(Texture_t2243626319 * value)
	{
		___clickTexture_5 = value;
		Il2CppCodeGenWriteBarrier(&___clickTexture_5, value);
	}

	inline static int32_t get_offset_of_clickImageDuration_6() { return static_cast<int32_t>(offsetof(Cursor_t2213425178, ___clickImageDuration_6)); }
	inline float get_clickImageDuration_6() const { return ___clickImageDuration_6; }
	inline float* get_address_of_clickImageDuration_6() { return &___clickImageDuration_6; }
	inline void set_clickImageDuration_6(float value)
	{
		___clickImageDuration_6 = value;
	}

	inline static int32_t get_offset_of_lastClickTime_7() { return static_cast<int32_t>(offsetof(Cursor_t2213425178, ___lastClickTime_7)); }
	inline float get_lastClickTime_7() const { return ___lastClickTime_7; }
	inline float* get_address_of_lastClickTime_7() { return &___lastClickTime_7; }
	inline void set_lastClickTime_7(float value)
	{
		___lastClickTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
