﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.TimeSlider
struct TimeSlider_t3354239974;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.TimeSlider::.ctor()
extern "C"  void TimeSlider__ctor_m1013972337 (TimeSlider_t3354239974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.TimeSlider::Awake()
extern "C"  void TimeSlider_Awake_m4091261946 (TimeSlider_t3354239974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.TimeSlider::Start()
extern "C"  void TimeSlider_Start_m1401702817 (TimeSlider_t3354239974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.TimeSlider::Update()
extern "C"  void TimeSlider_Update_m3448986556 (TimeSlider_t3354239974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.TimeSlider::OnValueChanged(System.Single)
extern "C"  void TimeSlider_OnValueChanged_m2261487840 (TimeSlider_t3354239974 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
