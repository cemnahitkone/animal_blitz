﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.TintedButton/<DarkenColor>c__Iterator0
struct  U3CDarkenColorU3Ec__Iterator0_t1184020839  : public Il2CppObject
{
public:
	// System.Int32 Ricimi.TintedButton/<DarkenColor>c__Iterator0::<i>__0
	int32_t ___U3CiU3E__0_0;
	// UnityEngine.UI.Image Ricimi.TintedButton/<DarkenColor>c__Iterator0::image
	Image_t2042527209 * ___image_1;
	// UnityEngine.Color Ricimi.TintedButton/<DarkenColor>c__Iterator0::<newColor>__1
	Color_t2020392075  ___U3CnewColorU3E__1_2;
	// System.Object Ricimi.TintedButton/<DarkenColor>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean Ricimi.TintedButton/<DarkenColor>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Ricimi.TintedButton/<DarkenColor>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDarkenColorU3Ec__Iterator0_t1184020839, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_image_1() { return static_cast<int32_t>(offsetof(U3CDarkenColorU3Ec__Iterator0_t1184020839, ___image_1)); }
	inline Image_t2042527209 * get_image_1() const { return ___image_1; }
	inline Image_t2042527209 ** get_address_of_image_1() { return &___image_1; }
	inline void set_image_1(Image_t2042527209 * value)
	{
		___image_1 = value;
		Il2CppCodeGenWriteBarrier(&___image_1, value);
	}

	inline static int32_t get_offset_of_U3CnewColorU3E__1_2() { return static_cast<int32_t>(offsetof(U3CDarkenColorU3Ec__Iterator0_t1184020839, ___U3CnewColorU3E__1_2)); }
	inline Color_t2020392075  get_U3CnewColorU3E__1_2() const { return ___U3CnewColorU3E__1_2; }
	inline Color_t2020392075 * get_address_of_U3CnewColorU3E__1_2() { return &___U3CnewColorU3E__1_2; }
	inline void set_U3CnewColorU3E__1_2(Color_t2020392075  value)
	{
		___U3CnewColorU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CDarkenColorU3Ec__Iterator0_t1184020839, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CDarkenColorU3Ec__Iterator0_t1184020839, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CDarkenColorU3Ec__Iterator0_t1184020839, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
