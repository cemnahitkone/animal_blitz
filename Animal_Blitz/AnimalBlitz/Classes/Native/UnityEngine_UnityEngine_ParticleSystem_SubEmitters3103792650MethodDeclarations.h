﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/SubEmittersModule
struct SubEmittersModule_t3103792650;
struct SubEmittersModule_t3103792650_marshaled_pinvoke;
struct SubEmittersModule_t3103792650_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_SubEmitters3103792650.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/SubEmittersModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void SubEmittersModule__ctor_m28843786 (SubEmittersModule_t3103792650 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct SubEmittersModule_t3103792650;
struct SubEmittersModule_t3103792650_marshaled_pinvoke;

extern "C" void SubEmittersModule_t3103792650_marshal_pinvoke(const SubEmittersModule_t3103792650& unmarshaled, SubEmittersModule_t3103792650_marshaled_pinvoke& marshaled);
extern "C" void SubEmittersModule_t3103792650_marshal_pinvoke_back(const SubEmittersModule_t3103792650_marshaled_pinvoke& marshaled, SubEmittersModule_t3103792650& unmarshaled);
extern "C" void SubEmittersModule_t3103792650_marshal_pinvoke_cleanup(SubEmittersModule_t3103792650_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SubEmittersModule_t3103792650;
struct SubEmittersModule_t3103792650_marshaled_com;

extern "C" void SubEmittersModule_t3103792650_marshal_com(const SubEmittersModule_t3103792650& unmarshaled, SubEmittersModule_t3103792650_marshaled_com& marshaled);
extern "C" void SubEmittersModule_t3103792650_marshal_com_back(const SubEmittersModule_t3103792650_marshaled_com& marshaled, SubEmittersModule_t3103792650& unmarshaled);
extern "C" void SubEmittersModule_t3103792650_marshal_com_cleanup(SubEmittersModule_t3103792650_marshaled_com& marshaled);
