﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// BluetoothDeviceScript
struct BluetoothDeviceScript_t810775987;
// System.String
struct String_t;
// BluetoothLEHardwareInterface
struct BluetoothLEHardwareInterface_t4023789856;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Action
struct Action_t3226471752;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Object
struct Il2CppObject;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Action`2<System.String,System.String>
struct Action_2_t4234541925;
// System.Action`4<System.String,System.String,System.Int32,System.Byte[]>
struct Action_4_t2314457268;
// System.Action`3<System.String,System.String,System.String>
struct Action_3_t3256166369;
// System.Action`2<System.String,System.Byte[]>
struct Action_2_t1307688409;
// System.Action`3<System.String,System.String,System.Byte[]>
struct Action_3_t329312853;
// HighlightingSystem.Highlighter
struct Highlighter_t958778585;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Transform
struct Transform_t3275118058;
// HighlightingSystem.HighlighterRenderer
struct HighlighterRenderer_t4217245640;
// System.Collections.Generic.List`1<UnityEngine.Renderer>
struct List_1_t3921398993;
// HighlightingSystem.HighlighterBlocker
struct HighlighterBlocker_t896209287;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t1204166949;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Renderer
struct Renderer_t257310565;
// HighlightingSystem.HighlighterRenderer/<EndOfFrame>c__Iterator0
struct U3CEndOfFrameU3Ec__Iterator0_t1938462382;
// HighlightingSystem.HighlightingBase
struct HighlightingBase_t336099813;
// HighlightingSystem.HighlightingBlitter
struct HighlightingBlitter_t2949110578;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// HighlightingSystem.HighlightingRenderer
struct HighlightingRenderer_t3709628099;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1486305137.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1486305137MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa3894236545.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa3894236545MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementat762068664.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementat762068664MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothDeviceScript810775987.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothDeviceScript810775987MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothLEHardwareI4023789856MethodDeclarations.h"
#include "System_Core_System_Action3226471752MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1831019615MethodDeclarations.h"
#include "System_Core_System_Action_2_gen4234541925MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "System_Core_System_Action_4_gen2314457268MethodDeclarations.h"
#include "System_Core_System_Action_3_gen3256166369MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_Action_1_gen1831019615.h"
#include "System_Core_System_Action_2_gen4234541925.h"
#include "System_Core_System_Action_4_gen2314457268.h"
#include "System_Core_System_Action_3_gen3256166369.h"
#include "System_Core_System_Action_2_gen1307688409MethodDeclarations.h"
#include "System_Core_System_Action_3_gen329312853MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1307688409.h"
#include "System_Core_System_Action_3_gen329312853.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothLEHardwareI4023789856.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothLEHardwareI1474803024.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothLEHardwareI1741997128.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothLEHardwareI1741997128MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothLEHardwareI1474803024MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BtConnection3662742114.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BtConnection3662742114MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi958778585.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi958778585MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3586366772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3586366772.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader2430389951.h"
#include "UnityEngine_UnityEngine_Shader2430389951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_S1950236385MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H1615946202.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g3587206735MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g3587206735.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H4217245640MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H4217245640.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3921398993MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3921398993.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "mscorlib_System_Collections_Generic_List_1_gen672924358MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3188497603MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi896209287.h"
#include "mscorlib_System_Collections_Generic_List_1_gen672924358.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3188497603.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_CommandBuffer1204166949.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E2075522577.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E2075522577MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H1615946202MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi896209287MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi336099813MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H1938462382MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H1938462382.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3087365684MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H3718244552.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3087365684.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Rendering_CommandBuffer1204166949MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H3718244552MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi336099813.h"
#include "UnityEngine_UnityEngine_Rendering_GraphicsDeviceTyp872267891.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H2949110578.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H2949110578MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g2817889127MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g2817889127.h"
#include "UnityEngine_UnityEngine_Rendering_CameraEvent2007842675.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"
#include "UnityEngine_UnityEngine_Texture2243626319MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_RenderingPath1538007819.h"
#include "UnityEngine_UnityEngine_CameraClearFlags452084705.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat3360518468.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite2842868372.h"
#include "UnityEngine_UnityEngine_FilterMode10814199.h"
#include "UnityEngine_UnityEngine_TextureWrapMode3683976566.h"
#include "UnityEngine_UnityEngine_Rendering_RenderTargetIdent772440638.h"
#include "UnityEngine_UnityEngine_Rendering_RenderTargetIdent772440638MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Graphics2412809155MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemInfo2353426895MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_BuiltinRenderText195473098.h"
#include "UnityEngine_UnityEngine_Mesh1356156583MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QualitySettings3238033062MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4000188241MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4000188241.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H3709628099.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H3709628099MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_S1950236385.h"

// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3813873105(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<BluetoothDeviceScript>()
#define GameObject_AddComponent_TisBluetoothDeviceScript_t810775987_m1605496783(__this, method) ((  BluetoothDeviceScript_t810775987 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
#define Component_GetComponent_TisTransform_t3275118058_m235623703(__this, method) ((  Transform_t3275118058 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2650145732(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<HighlightingSystem.HighlighterRenderer>()
#define GameObject_GetComponent_TisHighlighterRenderer_t4217245640_m4110411296(__this, method) ((  HighlighterRenderer_t4217245640 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<HighlightingSystem.HighlighterRenderer>()
#define GameObject_AddComponent_TisHighlighterRenderer_t4217245640_m2107265519(__this, method) ((  HighlighterRenderer_t4217245640 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<HighlightingSystem.Highlighter>()
#define Component_GetComponent_TisHighlighter_t958778585_m2365035773(__this, method) ((  Highlighter_t958778585 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<HighlightingSystem.HighlighterBlocker>()
#define Component_GetComponent_TisHighlighterBlocker_t896209287_m572960287(__this, method) ((  HighlighterBlocker_t896209287 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t257310565_m141370815(__this, method) ((  Renderer_t257310565 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t189460977_m3276577584(__this, method) ((  Camera_t189460977 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BluetoothDeviceScript::.ctor()
extern "C"  void BluetoothDeviceScript__ctor_m3422952948 (BluetoothDeviceScript_t810775987 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BluetoothDeviceScript::Start()
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const uint32_t BluetoothDeviceScript_Start_m3834806660_MetadataUsageId;
extern "C"  void BluetoothDeviceScript_Start_m3834806660 (BluetoothDeviceScript_t810775987 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothDeviceScript_Start_m3834806660_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_DiscoveredDeviceList_2(L_0);
		return;
	}
}
// System.Void BluetoothDeviceScript::Update()
extern "C"  void BluetoothDeviceScript_Update_m4257449185 (BluetoothDeviceScript_t810775987 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void BluetoothDeviceScript::OnBluetoothMessage(System.String)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3306437720_MethodInfo_var;
extern const MethodInfo* List_1_Contains_m3447151919_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* Action_2_Invoke_m3317412533_MethodInfo_var;
extern const MethodInfo* Action_4_Invoke_m808000806_MethodInfo_var;
extern const MethodInfo* Action_3_Invoke_m3478341225_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral6994405;
extern Il2CppCodeGenString* _stringLiteral1338385592;
extern Il2CppCodeGenString* _stringLiteral1410547565;
extern Il2CppCodeGenString* _stringLiteral22442200;
extern Il2CppCodeGenString* _stringLiteral36802029;
extern Il2CppCodeGenString* _stringLiteral2826090563;
extern Il2CppCodeGenString* _stringLiteral1747715499;
extern Il2CppCodeGenString* _stringLiteral2387802627;
extern Il2CppCodeGenString* _stringLiteral3371807147;
extern Il2CppCodeGenString* _stringLiteral3042613876;
extern Il2CppCodeGenString* _stringLiteral1890259001;
extern Il2CppCodeGenString* _stringLiteral967114908;
extern Il2CppCodeGenString* _stringLiteral1446214219;
extern Il2CppCodeGenString* _stringLiteral1534366217;
extern Il2CppCodeGenString* _stringLiteral366648875;
extern Il2CppCodeGenString* _stringLiteral1875650275;
extern Il2CppCodeGenString* _stringLiteral3998213925;
extern Il2CppCodeGenString* _stringLiteral2337682656;
extern Il2CppCodeGenString* _stringLiteral1125681393;
extern const uint32_t BluetoothDeviceScript_OnBluetoothMessage_m3706554852_MetadataUsageId;
extern "C"  void BluetoothDeviceScript_OnBluetoothMessage_m3706554852 (BluetoothDeviceScript_t810775987 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothDeviceScript_OnBluetoothMessage_m3706554852_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t1328083999* V_0 = NULL;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	int32_t V_4 = 0;
	ByteU5BU5D_t3397334013* V_5 = NULL;
	{
		String_t* L_0 = ___message0;
		if (!L_0)
		{
			goto IL_06a4;
		}
	}
	{
		CharU5BU5D_t1328083999* L_1 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)126));
		V_0 = L_1;
		String_t* L_2 = ___message0;
		CharU5BU5D_t1328083999* L_3 = V_0;
		NullCheck(L_2);
		StringU5BU5D_t1642385972* L_4 = String_Split_m3326265864(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		V_2 = 0;
		goto IL_003d;
	}

IL_0021:
	{
		int32_t L_5 = V_2;
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_6);
		StringU5BU5D_t1642385972* L_8 = V_1;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		String_t* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral6994405, L_7, L_11, /*hidden argument*/NULL);
		BluetoothLEHardwareInterface_Log_m3949775483(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_14 = V_2;
		StringU5BU5D_t1642385972* L_15 = V_1;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_16 = ___message0;
		NullCheck(L_16);
		int32_t L_17 = String_get_Length_m1606060069(L_16, /*hidden argument*/NULL);
		NullCheck(_stringLiteral1338385592);
		int32_t L_18 = String_get_Length_m1606060069(_stringLiteral1338385592, /*hidden argument*/NULL);
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_009d;
		}
	}
	{
		String_t* L_19 = ___message0;
		NullCheck(_stringLiteral1338385592);
		int32_t L_20 = String_get_Length_m1606060069(_stringLiteral1338385592, /*hidden argument*/NULL);
		NullCheck(L_19);
		String_t* L_21 = String_Substring_m12482732(L_19, 0, L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_21, _stringLiteral1338385592, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_009d;
		}
	}
	{
		__this->set_Initialized_23((bool)1);
		Action_t3226471752 * L_23 = __this->get_InitializedAction_3();
		if (!L_23)
		{
			goto IL_0098;
		}
	}
	{
		Action_t3226471752 * L_24 = __this->get_InitializedAction_3();
		NullCheck(L_24);
		Action_Invoke_m3801112262(L_24, /*hidden argument*/NULL);
	}

IL_0098:
	{
		goto IL_06a4;
	}

IL_009d:
	{
		String_t* L_25 = ___message0;
		NullCheck(L_25);
		int32_t L_26 = String_get_Length_m1606060069(L_25, /*hidden argument*/NULL);
		NullCheck(_stringLiteral1410547565);
		int32_t L_27 = String_get_Length_m1606060069(_stringLiteral1410547565, /*hidden argument*/NULL);
		if ((((int32_t)L_26) < ((int32_t)L_27)))
		{
			goto IL_00f9;
		}
	}
	{
		String_t* L_28 = ___message0;
		NullCheck(_stringLiteral1410547565);
		int32_t L_29 = String_get_Length_m1606060069(_stringLiteral1410547565, /*hidden argument*/NULL);
		NullCheck(L_28);
		String_t* L_30 = String_Substring_m12482732(L_28, 0, L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_31 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_30, _stringLiteral1410547565, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00f9;
		}
	}
	{
		BluetoothLEHardwareInterface_FinishDeInitialize_m3772842739(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_Initialized_23((bool)0);
		Action_t3226471752 * L_32 = __this->get_DeinitializedAction_4();
		if (!L_32)
		{
			goto IL_00f4;
		}
	}
	{
		Action_t3226471752 * L_33 = __this->get_DeinitializedAction_4();
		NullCheck(L_33);
		Action_Invoke_m3801112262(L_33, /*hidden argument*/NULL);
	}

IL_00f4:
	{
		goto IL_06a4;
	}

IL_00f9:
	{
		String_t* L_34 = ___message0;
		NullCheck(L_34);
		int32_t L_35 = String_get_Length_m1606060069(L_34, /*hidden argument*/NULL);
		NullCheck(_stringLiteral22442200);
		int32_t L_36 = String_get_Length_m1606060069(_stringLiteral22442200, /*hidden argument*/NULL);
		if ((((int32_t)L_35) < ((int32_t)L_36)))
		{
			goto IL_015d;
		}
	}
	{
		String_t* L_37 = ___message0;
		NullCheck(_stringLiteral22442200);
		int32_t L_38 = String_get_Length_m1606060069(_stringLiteral22442200, /*hidden argument*/NULL);
		NullCheck(L_37);
		String_t* L_39 = String_Substring_m12482732(L_37, 0, L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_40 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_39, _stringLiteral22442200, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_015d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_41;
		StringU5BU5D_t1642385972* L_42 = V_1;
		NullCheck(L_42);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_42)->max_length))))) < ((int32_t)2)))
		{
			goto IL_0141;
		}
	}
	{
		StringU5BU5D_t1642385972* L_43 = V_1;
		NullCheck(L_43);
		int32_t L_44 = 1;
		String_t* L_45 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		V_3 = L_45;
	}

IL_0141:
	{
		Action_1_t1831019615 * L_46 = __this->get_ErrorAction_5();
		if (!L_46)
		{
			goto IL_0158;
		}
	}
	{
		Action_1_t1831019615 * L_47 = __this->get_ErrorAction_5();
		String_t* L_48 = V_3;
		NullCheck(L_47);
		Action_1_Invoke_m3306437720(L_47, L_48, /*hidden argument*/Action_1_Invoke_m3306437720_MethodInfo_var);
	}

IL_0158:
	{
		goto IL_06a4;
	}

IL_015d:
	{
		String_t* L_49 = ___message0;
		NullCheck(L_49);
		int32_t L_50 = String_get_Length_m1606060069(L_49, /*hidden argument*/NULL);
		NullCheck(_stringLiteral36802029);
		int32_t L_51 = String_get_Length_m1606060069(_stringLiteral36802029, /*hidden argument*/NULL);
		if ((((int32_t)L_50) < ((int32_t)L_51)))
		{
			goto IL_01b9;
		}
	}
	{
		String_t* L_52 = ___message0;
		NullCheck(_stringLiteral36802029);
		int32_t L_53 = String_get_Length_m1606060069(_stringLiteral36802029, /*hidden argument*/NULL);
		NullCheck(L_52);
		String_t* L_54 = String_Substring_m12482732(L_52, 0, L_53, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_55 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_54, _stringLiteral36802029, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_01b9;
		}
	}
	{
		StringU5BU5D_t1642385972* L_56 = V_1;
		NullCheck(L_56);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_56)->max_length))))) < ((int32_t)2)))
		{
			goto IL_01b4;
		}
	}
	{
		Action_1_t1831019615 * L_57 = __this->get_ServiceAddedAction_6();
		if (!L_57)
		{
			goto IL_01b4;
		}
	}
	{
		Action_1_t1831019615 * L_58 = __this->get_ServiceAddedAction_6();
		StringU5BU5D_t1642385972* L_59 = V_1;
		NullCheck(L_59);
		int32_t L_60 = 1;
		String_t* L_61 = (L_59)->GetAt(static_cast<il2cpp_array_size_t>(L_60));
		NullCheck(L_58);
		Action_1_Invoke_m3306437720(L_58, L_61, /*hidden argument*/Action_1_Invoke_m3306437720_MethodInfo_var);
	}

IL_01b4:
	{
		goto IL_06a4;
	}

IL_01b9:
	{
		String_t* L_62 = ___message0;
		NullCheck(L_62);
		int32_t L_63 = String_get_Length_m1606060069(L_62, /*hidden argument*/NULL);
		NullCheck(_stringLiteral2826090563);
		int32_t L_64 = String_get_Length_m1606060069(_stringLiteral2826090563, /*hidden argument*/NULL);
		if ((((int32_t)L_63) < ((int32_t)L_64)))
		{
			goto IL_0213;
		}
	}
	{
		String_t* L_65 = ___message0;
		NullCheck(_stringLiteral2826090563);
		int32_t L_66 = String_get_Length_m1606060069(_stringLiteral2826090563, /*hidden argument*/NULL);
		NullCheck(L_65);
		String_t* L_67 = String_Substring_m12482732(L_65, 0, L_66, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_68 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_67, _stringLiteral2826090563, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_0213;
		}
	}
	{
		BluetoothLEHardwareInterface_Log_m3949775483(NULL /*static, unused*/, _stringLiteral1747715499, /*hidden argument*/NULL);
		Action_t3226471752 * L_69 = __this->get_StartedAdvertisingAction_7();
		if (!L_69)
		{
			goto IL_020e;
		}
	}
	{
		Action_t3226471752 * L_70 = __this->get_StartedAdvertisingAction_7();
		NullCheck(L_70);
		Action_Invoke_m3801112262(L_70, /*hidden argument*/NULL);
	}

IL_020e:
	{
		goto IL_06a4;
	}

IL_0213:
	{
		String_t* L_71 = ___message0;
		NullCheck(L_71);
		int32_t L_72 = String_get_Length_m1606060069(L_71, /*hidden argument*/NULL);
		NullCheck(_stringLiteral2387802627);
		int32_t L_73 = String_get_Length_m1606060069(_stringLiteral2387802627, /*hidden argument*/NULL);
		if ((((int32_t)L_72) < ((int32_t)L_73)))
		{
			goto IL_026d;
		}
	}
	{
		String_t* L_74 = ___message0;
		NullCheck(_stringLiteral2387802627);
		int32_t L_75 = String_get_Length_m1606060069(_stringLiteral2387802627, /*hidden argument*/NULL);
		NullCheck(L_74);
		String_t* L_76 = String_Substring_m12482732(L_74, 0, L_75, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_77 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_76, _stringLiteral2387802627, /*hidden argument*/NULL);
		if (!L_77)
		{
			goto IL_026d;
		}
	}
	{
		BluetoothLEHardwareInterface_Log_m3949775483(NULL /*static, unused*/, _stringLiteral3371807147, /*hidden argument*/NULL);
		Action_t3226471752 * L_78 = __this->get_StoppedAdvertisingAction_8();
		if (!L_78)
		{
			goto IL_0268;
		}
	}
	{
		Action_t3226471752 * L_79 = __this->get_StoppedAdvertisingAction_8();
		NullCheck(L_79);
		Action_Invoke_m3801112262(L_79, /*hidden argument*/NULL);
	}

IL_0268:
	{
		goto IL_06a4;
	}

IL_026d:
	{
		String_t* L_80 = ___message0;
		NullCheck(L_80);
		int32_t L_81 = String_get_Length_m1606060069(L_80, /*hidden argument*/NULL);
		NullCheck(_stringLiteral3042613876);
		int32_t L_82 = String_get_Length_m1606060069(_stringLiteral3042613876, /*hidden argument*/NULL);
		if ((((int32_t)L_81) < ((int32_t)L_82)))
		{
			goto IL_0335;
		}
	}
	{
		String_t* L_83 = ___message0;
		NullCheck(_stringLiteral3042613876);
		int32_t L_84 = String_get_Length_m1606060069(_stringLiteral3042613876, /*hidden argument*/NULL);
		NullCheck(L_83);
		String_t* L_85 = String_Substring_m12482732(L_83, 0, L_84, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_86 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_85, _stringLiteral3042613876, /*hidden argument*/NULL);
		if (!L_86)
		{
			goto IL_0335;
		}
	}
	{
		StringU5BU5D_t1642385972* L_87 = V_1;
		NullCheck(L_87);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_87)->max_length))))) < ((int32_t)3)))
		{
			goto IL_0330;
		}
	}
	{
		List_1_t1398341365 * L_88 = __this->get_DiscoveredDeviceList_2();
		StringU5BU5D_t1642385972* L_89 = V_1;
		NullCheck(L_89);
		int32_t L_90 = 1;
		String_t* L_91 = (L_89)->GetAt(static_cast<il2cpp_array_size_t>(L_90));
		NullCheck(L_88);
		bool L_92 = List_1_Contains_m3447151919(L_88, L_91, /*hidden argument*/List_1_Contains_m3447151919_MethodInfo_var);
		if (L_92)
		{
			goto IL_02e8;
		}
	}
	{
		List_1_t1398341365 * L_93 = __this->get_DiscoveredDeviceList_2();
		StringU5BU5D_t1642385972* L_94 = V_1;
		NullCheck(L_94);
		int32_t L_95 = 1;
		String_t* L_96 = (L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_95));
		NullCheck(L_93);
		List_1_Add_m4061286785(L_93, L_96, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		Action_2_t4234541925 * L_97 = __this->get_DiscoveredPeripheralAction_9();
		if (!L_97)
		{
			goto IL_02e8;
		}
	}
	{
		Action_2_t4234541925 * L_98 = __this->get_DiscoveredPeripheralAction_9();
		StringU5BU5D_t1642385972* L_99 = V_1;
		NullCheck(L_99);
		int32_t L_100 = 1;
		String_t* L_101 = (L_99)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		StringU5BU5D_t1642385972* L_102 = V_1;
		NullCheck(L_102);
		int32_t L_103 = 2;
		String_t* L_104 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_103));
		NullCheck(L_98);
		Action_2_Invoke_m3317412533(L_98, L_101, L_104, /*hidden argument*/Action_2_Invoke_m3317412533_MethodInfo_var);
	}

IL_02e8:
	{
		StringU5BU5D_t1642385972* L_105 = V_1;
		NullCheck(L_105);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_105)->max_length))))) < ((int32_t)5)))
		{
			goto IL_0330;
		}
	}
	{
		Action_4_t2314457268 * L_106 = __this->get_DiscoveredPeripheralWithAdvertisingInfoAction_10();
		if (!L_106)
		{
			goto IL_0330;
		}
	}
	{
		V_4 = 0;
		StringU5BU5D_t1642385972* L_107 = V_1;
		NullCheck(L_107);
		int32_t L_108 = 3;
		String_t* L_109 = (L_107)->GetAt(static_cast<il2cpp_array_size_t>(L_108));
		bool L_110 = Int32_TryParse_m656840904(NULL /*static, unused*/, L_109, (&V_4), /*hidden argument*/NULL);
		if (L_110)
		{
			goto IL_0311;
		}
	}
	{
		V_4 = 0;
	}

IL_0311:
	{
		StringU5BU5D_t1642385972* L_111 = V_1;
		NullCheck(L_111);
		int32_t L_112 = 4;
		String_t* L_113 = (L_111)->GetAt(static_cast<il2cpp_array_size_t>(L_112));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_114 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, L_113, /*hidden argument*/NULL);
		V_5 = L_114;
		Action_4_t2314457268 * L_115 = __this->get_DiscoveredPeripheralWithAdvertisingInfoAction_10();
		StringU5BU5D_t1642385972* L_116 = V_1;
		NullCheck(L_116);
		int32_t L_117 = 1;
		String_t* L_118 = (L_116)->GetAt(static_cast<il2cpp_array_size_t>(L_117));
		StringU5BU5D_t1642385972* L_119 = V_1;
		NullCheck(L_119);
		int32_t L_120 = 2;
		String_t* L_121 = (L_119)->GetAt(static_cast<il2cpp_array_size_t>(L_120));
		int32_t L_122 = V_4;
		ByteU5BU5D_t3397334013* L_123 = V_5;
		NullCheck(L_115);
		Action_4_Invoke_m808000806(L_115, L_118, L_121, L_122, L_123, /*hidden argument*/Action_4_Invoke_m808000806_MethodInfo_var);
	}

IL_0330:
	{
		goto IL_06a4;
	}

IL_0335:
	{
		String_t* L_124 = ___message0;
		NullCheck(L_124);
		int32_t L_125 = String_get_Length_m1606060069(L_124, /*hidden argument*/NULL);
		NullCheck(_stringLiteral1890259001);
		int32_t L_126 = String_get_Length_m1606060069(_stringLiteral1890259001, /*hidden argument*/NULL);
		if ((((int32_t)L_125) < ((int32_t)L_126)))
		{
			goto IL_03a2;
		}
	}
	{
		String_t* L_127 = ___message0;
		NullCheck(_stringLiteral1890259001);
		int32_t L_128 = String_get_Length_m1606060069(_stringLiteral1890259001, /*hidden argument*/NULL);
		NullCheck(L_127);
		String_t* L_129 = String_Substring_m12482732(L_127, 0, L_128, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_130 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_129, _stringLiteral1890259001, /*hidden argument*/NULL);
		if (!L_130)
		{
			goto IL_03a2;
		}
	}
	{
		StringU5BU5D_t1642385972* L_131 = V_1;
		NullCheck(L_131);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_131)->max_length))))) < ((int32_t)3)))
		{
			goto IL_039d;
		}
	}
	{
		List_1_t1398341365 * L_132 = __this->get_DiscoveredDeviceList_2();
		StringU5BU5D_t1642385972* L_133 = V_1;
		NullCheck(L_133);
		int32_t L_134 = 1;
		String_t* L_135 = (L_133)->GetAt(static_cast<il2cpp_array_size_t>(L_134));
		NullCheck(L_132);
		List_1_Add_m4061286785(L_132, L_135, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		Action_2_t4234541925 * L_136 = __this->get_RetrievedConnectedPeripheralAction_11();
		if (!L_136)
		{
			goto IL_039d;
		}
	}
	{
		Action_2_t4234541925 * L_137 = __this->get_RetrievedConnectedPeripheralAction_11();
		StringU5BU5D_t1642385972* L_138 = V_1;
		NullCheck(L_138);
		int32_t L_139 = 1;
		String_t* L_140 = (L_138)->GetAt(static_cast<il2cpp_array_size_t>(L_139));
		StringU5BU5D_t1642385972* L_141 = V_1;
		NullCheck(L_141);
		int32_t L_142 = 2;
		String_t* L_143 = (L_141)->GetAt(static_cast<il2cpp_array_size_t>(L_142));
		NullCheck(L_137);
		Action_2_Invoke_m3317412533(L_137, L_140, L_143, /*hidden argument*/Action_2_Invoke_m3317412533_MethodInfo_var);
	}

IL_039d:
	{
		goto IL_06a4;
	}

IL_03a2:
	{
		String_t* L_144 = ___message0;
		NullCheck(L_144);
		int32_t L_145 = String_get_Length_m1606060069(L_144, /*hidden argument*/NULL);
		NullCheck(_stringLiteral967114908);
		int32_t L_146 = String_get_Length_m1606060069(_stringLiteral967114908, /*hidden argument*/NULL);
		if ((((int32_t)L_145) < ((int32_t)L_146)))
		{
			goto IL_03f1;
		}
	}
	{
		String_t* L_147 = ___message0;
		NullCheck(_stringLiteral967114908);
		int32_t L_148 = String_get_Length_m1606060069(_stringLiteral967114908, /*hidden argument*/NULL);
		NullCheck(L_147);
		String_t* L_149 = String_Substring_m12482732(L_147, 0, L_148, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_150 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_149, _stringLiteral967114908, /*hidden argument*/NULL);
		if (!L_150)
		{
			goto IL_03f1;
		}
	}
	{
		StringU5BU5D_t1642385972* L_151 = V_1;
		NullCheck(L_151);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_151)->max_length))))) < ((int32_t)3)))
		{
			goto IL_03ec;
		}
	}
	{
		StringU5BU5D_t1642385972* L_152 = V_1;
		NullCheck(L_152);
		int32_t L_153 = 1;
		String_t* L_154 = (L_152)->GetAt(static_cast<il2cpp_array_size_t>(L_153));
		StringU5BU5D_t1642385972* L_155 = V_1;
		NullCheck(L_155);
		int32_t L_156 = 2;
		String_t* L_157 = (L_155)->GetAt(static_cast<il2cpp_array_size_t>(L_156));
		BluetoothDeviceScript_OnPeripheralData_m1475850261(__this, L_154, L_157, /*hidden argument*/NULL);
	}

IL_03ec:
	{
		goto IL_06a4;
	}

IL_03f1:
	{
		String_t* L_158 = ___message0;
		NullCheck(L_158);
		int32_t L_159 = String_get_Length_m1606060069(L_158, /*hidden argument*/NULL);
		NullCheck(_stringLiteral1446214219);
		int32_t L_160 = String_get_Length_m1606060069(_stringLiteral1446214219, /*hidden argument*/NULL);
		if ((((int32_t)L_159) < ((int32_t)L_160)))
		{
			goto IL_044d;
		}
	}
	{
		String_t* L_161 = ___message0;
		NullCheck(_stringLiteral1446214219);
		int32_t L_162 = String_get_Length_m1606060069(_stringLiteral1446214219, /*hidden argument*/NULL);
		NullCheck(L_161);
		String_t* L_163 = String_Substring_m12482732(L_161, 0, L_162, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_164 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_163, _stringLiteral1446214219, /*hidden argument*/NULL);
		if (!L_164)
		{
			goto IL_044d;
		}
	}
	{
		StringU5BU5D_t1642385972* L_165 = V_1;
		NullCheck(L_165);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_165)->max_length))))) < ((int32_t)2)))
		{
			goto IL_0448;
		}
	}
	{
		Action_1_t1831019615 * L_166 = __this->get_ConnectedPeripheralAction_13();
		if (!L_166)
		{
			goto IL_0448;
		}
	}
	{
		Action_1_t1831019615 * L_167 = __this->get_ConnectedPeripheralAction_13();
		StringU5BU5D_t1642385972* L_168 = V_1;
		NullCheck(L_168);
		int32_t L_169 = 1;
		String_t* L_170 = (L_168)->GetAt(static_cast<il2cpp_array_size_t>(L_169));
		NullCheck(L_167);
		Action_1_Invoke_m3306437720(L_167, L_170, /*hidden argument*/Action_1_Invoke_m3306437720_MethodInfo_var);
	}

IL_0448:
	{
		goto IL_06a4;
	}

IL_044d:
	{
		String_t* L_171 = ___message0;
		NullCheck(L_171);
		int32_t L_172 = String_get_Length_m1606060069(L_171, /*hidden argument*/NULL);
		NullCheck(_stringLiteral1534366217);
		int32_t L_173 = String_get_Length_m1606060069(_stringLiteral1534366217, /*hidden argument*/NULL);
		if ((((int32_t)L_172) < ((int32_t)L_173)))
		{
			goto IL_04c2;
		}
	}
	{
		String_t* L_174 = ___message0;
		NullCheck(_stringLiteral1534366217);
		int32_t L_175 = String_get_Length_m1606060069(_stringLiteral1534366217, /*hidden argument*/NULL);
		NullCheck(L_174);
		String_t* L_176 = String_Substring_m12482732(L_174, 0, L_175, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_177 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_176, _stringLiteral1534366217, /*hidden argument*/NULL);
		if (!L_177)
		{
			goto IL_04c2;
		}
	}
	{
		StringU5BU5D_t1642385972* L_178 = V_1;
		NullCheck(L_178);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_178)->max_length))))) < ((int32_t)2)))
		{
			goto IL_04bd;
		}
	}
	{
		Action_1_t1831019615 * L_179 = __this->get_ConnectedDisconnectPeripheralAction_14();
		if (!L_179)
		{
			goto IL_04a4;
		}
	}
	{
		Action_1_t1831019615 * L_180 = __this->get_ConnectedDisconnectPeripheralAction_14();
		StringU5BU5D_t1642385972* L_181 = V_1;
		NullCheck(L_181);
		int32_t L_182 = 1;
		String_t* L_183 = (L_181)->GetAt(static_cast<il2cpp_array_size_t>(L_182));
		NullCheck(L_180);
		Action_1_Invoke_m3306437720(L_180, L_183, /*hidden argument*/Action_1_Invoke_m3306437720_MethodInfo_var);
	}

IL_04a4:
	{
		Action_1_t1831019615 * L_184 = __this->get_DisconnectedPeripheralAction_15();
		if (!L_184)
		{
			goto IL_04bd;
		}
	}
	{
		Action_1_t1831019615 * L_185 = __this->get_DisconnectedPeripheralAction_15();
		StringU5BU5D_t1642385972* L_186 = V_1;
		NullCheck(L_186);
		int32_t L_187 = 1;
		String_t* L_188 = (L_186)->GetAt(static_cast<il2cpp_array_size_t>(L_187));
		NullCheck(L_185);
		Action_1_Invoke_m3306437720(L_185, L_188, /*hidden argument*/Action_1_Invoke_m3306437720_MethodInfo_var);
	}

IL_04bd:
	{
		goto IL_06a4;
	}

IL_04c2:
	{
		String_t* L_189 = ___message0;
		NullCheck(L_189);
		int32_t L_190 = String_get_Length_m1606060069(L_189, /*hidden argument*/NULL);
		NullCheck(_stringLiteral366648875);
		int32_t L_191 = String_get_Length_m1606060069(_stringLiteral366648875, /*hidden argument*/NULL);
		if ((((int32_t)L_190) < ((int32_t)L_191)))
		{
			goto IL_0521;
		}
	}
	{
		String_t* L_192 = ___message0;
		NullCheck(_stringLiteral366648875);
		int32_t L_193 = String_get_Length_m1606060069(_stringLiteral366648875, /*hidden argument*/NULL);
		NullCheck(L_192);
		String_t* L_194 = String_Substring_m12482732(L_192, 0, L_193, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_195 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_194, _stringLiteral366648875, /*hidden argument*/NULL);
		if (!L_195)
		{
			goto IL_0521;
		}
	}
	{
		StringU5BU5D_t1642385972* L_196 = V_1;
		NullCheck(L_196);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_196)->max_length))))) < ((int32_t)3)))
		{
			goto IL_051c;
		}
	}
	{
		Action_2_t4234541925 * L_197 = __this->get_DiscoveredServiceAction_16();
		if (!L_197)
		{
			goto IL_051c;
		}
	}
	{
		Action_2_t4234541925 * L_198 = __this->get_DiscoveredServiceAction_16();
		StringU5BU5D_t1642385972* L_199 = V_1;
		NullCheck(L_199);
		int32_t L_200 = 1;
		String_t* L_201 = (L_199)->GetAt(static_cast<il2cpp_array_size_t>(L_200));
		StringU5BU5D_t1642385972* L_202 = V_1;
		NullCheck(L_202);
		int32_t L_203 = 2;
		String_t* L_204 = (L_202)->GetAt(static_cast<il2cpp_array_size_t>(L_203));
		NullCheck(L_198);
		Action_2_Invoke_m3317412533(L_198, L_201, L_204, /*hidden argument*/Action_2_Invoke_m3317412533_MethodInfo_var);
	}

IL_051c:
	{
		goto IL_06a4;
	}

IL_0521:
	{
		String_t* L_205 = ___message0;
		NullCheck(L_205);
		int32_t L_206 = String_get_Length_m1606060069(L_205, /*hidden argument*/NULL);
		NullCheck(_stringLiteral1875650275);
		int32_t L_207 = String_get_Length_m1606060069(_stringLiteral1875650275, /*hidden argument*/NULL);
		if ((((int32_t)L_206) < ((int32_t)L_207)))
		{
			goto IL_0583;
		}
	}
	{
		String_t* L_208 = ___message0;
		NullCheck(_stringLiteral1875650275);
		int32_t L_209 = String_get_Length_m1606060069(_stringLiteral1875650275, /*hidden argument*/NULL);
		NullCheck(L_208);
		String_t* L_210 = String_Substring_m12482732(L_208, 0, L_209, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_211 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_210, _stringLiteral1875650275, /*hidden argument*/NULL);
		if (!L_211)
		{
			goto IL_0583;
		}
	}
	{
		StringU5BU5D_t1642385972* L_212 = V_1;
		NullCheck(L_212);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_212)->max_length))))) < ((int32_t)4)))
		{
			goto IL_057e;
		}
	}
	{
		Action_3_t3256166369 * L_213 = __this->get_DiscoveredCharacteristicAction_17();
		if (!L_213)
		{
			goto IL_057e;
		}
	}
	{
		Action_3_t3256166369 * L_214 = __this->get_DiscoveredCharacteristicAction_17();
		StringU5BU5D_t1642385972* L_215 = V_1;
		NullCheck(L_215);
		int32_t L_216 = 1;
		String_t* L_217 = (L_215)->GetAt(static_cast<il2cpp_array_size_t>(L_216));
		StringU5BU5D_t1642385972* L_218 = V_1;
		NullCheck(L_218);
		int32_t L_219 = 2;
		String_t* L_220 = (L_218)->GetAt(static_cast<il2cpp_array_size_t>(L_219));
		StringU5BU5D_t1642385972* L_221 = V_1;
		NullCheck(L_221);
		int32_t L_222 = 3;
		String_t* L_223 = (L_221)->GetAt(static_cast<il2cpp_array_size_t>(L_222));
		NullCheck(L_214);
		Action_3_Invoke_m3478341225(L_214, L_217, L_220, L_223, /*hidden argument*/Action_3_Invoke_m3478341225_MethodInfo_var);
	}

IL_057e:
	{
		goto IL_06a4;
	}

IL_0583:
	{
		String_t* L_224 = ___message0;
		NullCheck(L_224);
		int32_t L_225 = String_get_Length_m1606060069(L_224, /*hidden argument*/NULL);
		NullCheck(_stringLiteral3998213925);
		int32_t L_226 = String_get_Length_m1606060069(_stringLiteral3998213925, /*hidden argument*/NULL);
		if ((((int32_t)L_225) < ((int32_t)L_226)))
		{
			goto IL_05df;
		}
	}
	{
		String_t* L_227 = ___message0;
		NullCheck(_stringLiteral3998213925);
		int32_t L_228 = String_get_Length_m1606060069(_stringLiteral3998213925, /*hidden argument*/NULL);
		NullCheck(L_227);
		String_t* L_229 = String_Substring_m12482732(L_227, 0, L_228, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_230 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_229, _stringLiteral3998213925, /*hidden argument*/NULL);
		if (!L_230)
		{
			goto IL_05df;
		}
	}
	{
		StringU5BU5D_t1642385972* L_231 = V_1;
		NullCheck(L_231);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_231)->max_length))))) < ((int32_t)2)))
		{
			goto IL_05da;
		}
	}
	{
		Action_1_t1831019615 * L_232 = __this->get_DidWriteCharacteristicAction_18();
		if (!L_232)
		{
			goto IL_05da;
		}
	}
	{
		Action_1_t1831019615 * L_233 = __this->get_DidWriteCharacteristicAction_18();
		StringU5BU5D_t1642385972* L_234 = V_1;
		NullCheck(L_234);
		int32_t L_235 = 1;
		String_t* L_236 = (L_234)->GetAt(static_cast<il2cpp_array_size_t>(L_235));
		NullCheck(L_233);
		Action_1_Invoke_m3306437720(L_233, L_236, /*hidden argument*/Action_1_Invoke_m3306437720_MethodInfo_var);
	}

IL_05da:
	{
		goto IL_06a4;
	}

IL_05df:
	{
		String_t* L_237 = ___message0;
		NullCheck(L_237);
		int32_t L_238 = String_get_Length_m1606060069(L_237, /*hidden argument*/NULL);
		NullCheck(_stringLiteral2337682656);
		int32_t L_239 = String_get_Length_m1606060069(_stringLiteral2337682656, /*hidden argument*/NULL);
		if ((((int32_t)L_238) < ((int32_t)L_239)))
		{
			goto IL_0657;
		}
	}
	{
		String_t* L_240 = ___message0;
		NullCheck(_stringLiteral2337682656);
		int32_t L_241 = String_get_Length_m1606060069(_stringLiteral2337682656, /*hidden argument*/NULL);
		NullCheck(L_240);
		String_t* L_242 = String_Substring_m12482732(L_240, 0, L_241, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_243 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_242, _stringLiteral2337682656, /*hidden argument*/NULL);
		if (!L_243)
		{
			goto IL_0657;
		}
	}
	{
		StringU5BU5D_t1642385972* L_244 = V_1;
		NullCheck(L_244);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_244)->max_length))))) < ((int32_t)3)))
		{
			goto IL_0652;
		}
	}
	{
		Action_1_t1831019615 * L_245 = __this->get_DidUpdateNotificationStateForCharacteristicAction_19();
		if (!L_245)
		{
			goto IL_0636;
		}
	}
	{
		Action_1_t1831019615 * L_246 = __this->get_DidUpdateNotificationStateForCharacteristicAction_19();
		StringU5BU5D_t1642385972* L_247 = V_1;
		NullCheck(L_247);
		int32_t L_248 = 2;
		String_t* L_249 = (L_247)->GetAt(static_cast<il2cpp_array_size_t>(L_248));
		NullCheck(L_246);
		Action_1_Invoke_m3306437720(L_246, L_249, /*hidden argument*/Action_1_Invoke_m3306437720_MethodInfo_var);
	}

IL_0636:
	{
		Action_2_t4234541925 * L_250 = __this->get_DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_20();
		if (!L_250)
		{
			goto IL_0652;
		}
	}
	{
		Action_2_t4234541925 * L_251 = __this->get_DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_20();
		StringU5BU5D_t1642385972* L_252 = V_1;
		NullCheck(L_252);
		int32_t L_253 = 1;
		String_t* L_254 = (L_252)->GetAt(static_cast<il2cpp_array_size_t>(L_253));
		StringU5BU5D_t1642385972* L_255 = V_1;
		NullCheck(L_255);
		int32_t L_256 = 2;
		String_t* L_257 = (L_255)->GetAt(static_cast<il2cpp_array_size_t>(L_256));
		NullCheck(L_251);
		Action_2_Invoke_m3317412533(L_251, L_254, L_257, /*hidden argument*/Action_2_Invoke_m3317412533_MethodInfo_var);
	}

IL_0652:
	{
		goto IL_06a4;
	}

IL_0657:
	{
		String_t* L_258 = ___message0;
		NullCheck(L_258);
		int32_t L_259 = String_get_Length_m1606060069(L_258, /*hidden argument*/NULL);
		NullCheck(_stringLiteral1125681393);
		int32_t L_260 = String_get_Length_m1606060069(_stringLiteral1125681393, /*hidden argument*/NULL);
		if ((((int32_t)L_259) < ((int32_t)L_260)))
		{
			goto IL_06a4;
		}
	}
	{
		String_t* L_261 = ___message0;
		NullCheck(_stringLiteral1125681393);
		int32_t L_262 = String_get_Length_m1606060069(_stringLiteral1125681393, /*hidden argument*/NULL);
		NullCheck(L_261);
		String_t* L_263 = String_Substring_m12482732(L_261, 0, L_262, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_264 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_263, _stringLiteral1125681393, /*hidden argument*/NULL);
		if (!L_264)
		{
			goto IL_06a4;
		}
	}
	{
		StringU5BU5D_t1642385972* L_265 = V_1;
		NullCheck(L_265);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_265)->max_length))))) < ((int32_t)4)))
		{
			goto IL_06a4;
		}
	}
	{
		StringU5BU5D_t1642385972* L_266 = V_1;
		NullCheck(L_266);
		int32_t L_267 = 1;
		String_t* L_268 = (L_266)->GetAt(static_cast<il2cpp_array_size_t>(L_267));
		StringU5BU5D_t1642385972* L_269 = V_1;
		NullCheck(L_269);
		int32_t L_270 = 2;
		String_t* L_271 = (L_269)->GetAt(static_cast<il2cpp_array_size_t>(L_270));
		StringU5BU5D_t1642385972* L_272 = V_1;
		NullCheck(L_272);
		int32_t L_273 = 3;
		String_t* L_274 = (L_272)->GetAt(static_cast<il2cpp_array_size_t>(L_273));
		BluetoothDeviceScript_OnBluetoothData_m1904684625(__this, L_268, L_271, L_274, /*hidden argument*/NULL);
	}

IL_06a4:
	{
		return;
	}
}
// System.Void BluetoothDeviceScript::OnBluetoothData(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t BluetoothDeviceScript_OnBluetoothData_m649667473_MetadataUsageId;
extern "C"  void BluetoothDeviceScript_OnBluetoothData_m649667473 (BluetoothDeviceScript_t810775987 * __this, String_t* ___base64Data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothDeviceScript_OnBluetoothData_m649667473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_2 = ___base64Data0;
		BluetoothDeviceScript_OnBluetoothData_m1904684625(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BluetoothDeviceScript::OnBluetoothData(System.String,System.String,System.String)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m3715468698_MethodInfo_var;
extern const MethodInfo* Action_3_Invoke_m825426516_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2369046602;
extern Il2CppCodeGenString* _stringLiteral2795205480;
extern Il2CppCodeGenString* _stringLiteral6500422;
extern const uint32_t BluetoothDeviceScript_OnBluetoothData_m1904684625_MetadataUsageId;
extern "C"  void BluetoothDeviceScript_OnBluetoothData_m1904684625 (BluetoothDeviceScript_t810775987 * __this, String_t* ___deviceAddress0, String_t* ___characteristic1, String_t* ___base64Data2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothDeviceScript_OnBluetoothData_m1904684625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	String_t* V_1 = NULL;
	uint8_t V_2 = 0x0;
	ByteU5BU5D_t3397334013* V_3 = NULL;
	int32_t V_4 = 0;
	{
		String_t* L_0 = ___base64Data2;
		if (!L_0)
		{
			goto IL_009f;
		}
	}
	{
		String_t* L_1 = ___base64Data2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_2 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ByteU5BU5D_t3397334013* L_3 = V_0;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_009f;
		}
	}
	{
		String_t* L_4 = ___deviceAddress0;
		String_t* L_5 = ___characteristic1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral2369046602, L_4, _stringLiteral2795205480, L_5, /*hidden argument*/NULL);
		BluetoothLEHardwareInterface_Log_m3949775483(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_1 = L_7;
		ByteU5BU5D_t3397334013* L_8 = V_0;
		V_3 = L_8;
		V_4 = 0;
		goto IL_005e;
	}

IL_003c:
	{
		ByteU5BU5D_t3397334013* L_9 = V_3;
		int32_t L_10 = V_4;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		uint8_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_2 = L_12;
		String_t* L_13 = V_1;
		uint8_t L_14 = V_2;
		uint8_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral6500422, L_16, /*hidden argument*/NULL);
		String_t* L_18 = String_Concat_m2596409543(NULL /*static, unused*/, L_13, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		int32_t L_19 = V_4;
		V_4 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_20 = V_4;
		ByteU5BU5D_t3397334013* L_21 = V_3;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_003c;
		}
	}
	{
		String_t* L_22 = V_1;
		BluetoothLEHardwareInterface_Log_m3949775483(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		Action_2_t1307688409 * L_23 = __this->get_DidUpdateCharacteristicValueAction_21();
		if (!L_23)
		{
			goto IL_0086;
		}
	}
	{
		Action_2_t1307688409 * L_24 = __this->get_DidUpdateCharacteristicValueAction_21();
		String_t* L_25 = ___characteristic1;
		ByteU5BU5D_t3397334013* L_26 = V_0;
		NullCheck(L_24);
		Action_2_Invoke_m3715468698(L_24, L_25, L_26, /*hidden argument*/Action_2_Invoke_m3715468698_MethodInfo_var);
	}

IL_0086:
	{
		Action_3_t329312853 * L_27 = __this->get_DidUpdateCharacteristicValueWithDeviceAddressAction_22();
		if (!L_27)
		{
			goto IL_009f;
		}
	}
	{
		Action_3_t329312853 * L_28 = __this->get_DidUpdateCharacteristicValueWithDeviceAddressAction_22();
		String_t* L_29 = ___deviceAddress0;
		String_t* L_30 = ___characteristic1;
		ByteU5BU5D_t3397334013* L_31 = V_0;
		NullCheck(L_28);
		Action_3_Invoke_m825426516(L_28, L_29, L_30, L_31, /*hidden argument*/Action_3_Invoke_m825426516_MethodInfo_var);
	}

IL_009f:
	{
		return;
	}
}
// System.Void BluetoothDeviceScript::OnPeripheralData(System.String,System.String)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m3715468698_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3262054489;
extern Il2CppCodeGenString* _stringLiteral6500422;
extern const uint32_t BluetoothDeviceScript_OnPeripheralData_m1475850261_MetadataUsageId;
extern "C"  void BluetoothDeviceScript_OnPeripheralData_m1475850261 (BluetoothDeviceScript_t810775987 * __this, String_t* ___characteristic0, String_t* ___base64Data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothDeviceScript_OnPeripheralData_m1475850261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	String_t* V_1 = NULL;
	uint8_t V_2 = 0x0;
	ByteU5BU5D_t3397334013* V_3 = NULL;
	int32_t V_4 = 0;
	{
		String_t* L_0 = ___base64Data1;
		if (!L_0)
		{
			goto IL_0080;
		}
	}
	{
		String_t* L_1 = ___base64Data1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_2 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ByteU5BU5D_t3397334013* L_3 = V_0;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		String_t* L_4 = ___characteristic0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3262054489, L_4, /*hidden argument*/NULL);
		BluetoothLEHardwareInterface_Log_m3949775483(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_1 = L_6;
		ByteU5BU5D_t3397334013* L_7 = V_0;
		V_3 = L_7;
		V_4 = 0;
		goto IL_0058;
	}

IL_0036:
	{
		ByteU5BU5D_t3397334013* L_8 = V_3;
		int32_t L_9 = V_4;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		uint8_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_2 = L_11;
		String_t* L_12 = V_1;
		uint8_t L_13 = V_2;
		uint8_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral6500422, L_15, /*hidden argument*/NULL);
		String_t* L_17 = String_Concat_m2596409543(NULL /*static, unused*/, L_12, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		int32_t L_18 = V_4;
		V_4 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0058:
	{
		int32_t L_19 = V_4;
		ByteU5BU5D_t3397334013* L_20 = V_3;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_21 = V_1;
		BluetoothLEHardwareInterface_Log_m3949775483(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		Action_2_t1307688409 * L_22 = __this->get_PeripheralReceivedWriteDataAction_12();
		if (!L_22)
		{
			goto IL_0080;
		}
	}
	{
		Action_2_t1307688409 * L_23 = __this->get_PeripheralReceivedWriteDataAction_12();
		String_t* L_24 = ___characteristic0;
		ByteU5BU5D_t3397334013* L_25 = V_0;
		NullCheck(L_23);
		Action_2_Invoke_m3715468698(L_23, L_24, L_25, /*hidden argument*/Action_2_Invoke_m3715468698_MethodInfo_var);
	}

IL_0080:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::.ctor()
extern "C"  void BluetoothLEHardwareInterface__ctor_m1277440725 (BluetoothLEHardwareInterface_t4023789856 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _iOSBluetoothLELog(char*);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLELog(System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLELog_m3965866644 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLELog)(____message0_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _iOSBluetoothLEInitialize(int32_t, int32_t);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEInitialize(System.Boolean,System.Boolean)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEInitialize_m4214605180 (Il2CppObject * __this /* static, unused */, bool ___asCentral0, bool ___asPeripheral1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEInitialize)(static_cast<int32_t>(___asCentral0), static_cast<int32_t>(___asPeripheral1));

}
extern "C" void DEFAULT_CALL _iOSBluetoothLEDeInitialize();
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEDeInitialize()
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEDeInitialize_m1183275517 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEDeInitialize)();

}
extern "C" void DEFAULT_CALL _iOSBluetoothLEPauseMessages(int32_t);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEPauseMessages(System.Boolean)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEPauseMessages_m12473301 (Il2CppObject * __this /* static, unused */, bool ___isPaused0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEPauseMessages)(static_cast<int32_t>(___isPaused0));

}
extern "C" void DEFAULT_CALL _iOSBluetoothLEScanForPeripheralsWithServices(char*, int32_t);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEScanForPeripheralsWithServices(System.String,System.Boolean)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEScanForPeripheralsWithServices_m1764614798 (Il2CppObject * __this /* static, unused */, String_t* ___serviceUUIDsString0, bool ___allowDuplicates1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t);

	// Marshaling of parameter '___serviceUUIDsString0' to native representation
	char* ____serviceUUIDsString0_marshaled = NULL;
	____serviceUUIDsString0_marshaled = il2cpp_codegen_marshal_string(___serviceUUIDsString0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEScanForPeripheralsWithServices)(____serviceUUIDsString0_marshaled, static_cast<int32_t>(___allowDuplicates1));

	// Marshaling cleanup of parameter '___serviceUUIDsString0' native representation
	il2cpp_codegen_marshal_free(____serviceUUIDsString0_marshaled);
	____serviceUUIDsString0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _iOSBluetoothLERetrieveListOfPeripheralsWithServices(char*);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERetrieveListOfPeripheralsWithServices(System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLERetrieveListOfPeripheralsWithServices_m143887212 (Il2CppObject * __this /* static, unused */, String_t* ___serviceUUIDsString0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___serviceUUIDsString0' to native representation
	char* ____serviceUUIDsString0_marshaled = NULL;
	____serviceUUIDsString0_marshaled = il2cpp_codegen_marshal_string(___serviceUUIDsString0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLERetrieveListOfPeripheralsWithServices)(____serviceUUIDsString0_marshaled);

	// Marshaling cleanup of parameter '___serviceUUIDsString0' native representation
	il2cpp_codegen_marshal_free(____serviceUUIDsString0_marshaled);
	____serviceUUIDsString0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _iOSBluetoothLEStopScan();
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEStopScan()
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEStopScan_m107073007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEStopScan)();

}
extern "C" void DEFAULT_CALL _iOSBluetoothLEConnectToPeripheral(char*);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEConnectToPeripheral(System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEConnectToPeripheral_m1210203199 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEConnectToPeripheral)(____name0_marshaled);

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _iOSBluetoothLEDisconnectPeripheral(char*);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEDisconnectPeripheral(System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEDisconnectPeripheral_m2753876142 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEDisconnectPeripheral)(____name0_marshaled);

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _iOSBluetoothLEReadCharacteristic(char*, char*, char*);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEReadCharacteristic(System.String,System.String,System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEReadCharacteristic_m1988194505 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Marshaling of parameter '___service1' to native representation
	char* ____service1_marshaled = NULL;
	____service1_marshaled = il2cpp_codegen_marshal_string(___service1);

	// Marshaling of parameter '___characteristic2' to native representation
	char* ____characteristic2_marshaled = NULL;
	____characteristic2_marshaled = il2cpp_codegen_marshal_string(___characteristic2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEReadCharacteristic)(____name0_marshaled, ____service1_marshaled, ____characteristic2_marshaled);

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

	// Marshaling cleanup of parameter '___service1' native representation
	il2cpp_codegen_marshal_free(____service1_marshaled);
	____service1_marshaled = NULL;

	// Marshaling cleanup of parameter '___characteristic2' native representation
	il2cpp_codegen_marshal_free(____characteristic2_marshaled);
	____characteristic2_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _iOSBluetoothLEWriteCharacteristic(char*, char*, char*, uint8_t*, int32_t, int32_t);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEWriteCharacteristic(System.String,System.String,System.String,System.Byte[],System.Int32,System.Boolean)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEWriteCharacteristic_m2337396763 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, ByteU5BU5D_t3397334013* ___data3, int32_t ___length4, bool ___withResponse5, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*, uint8_t*, int32_t, int32_t);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Marshaling of parameter '___service1' to native representation
	char* ____service1_marshaled = NULL;
	____service1_marshaled = il2cpp_codegen_marshal_string(___service1);

	// Marshaling of parameter '___characteristic2' to native representation
	char* ____characteristic2_marshaled = NULL;
	____characteristic2_marshaled = il2cpp_codegen_marshal_string(___characteristic2);

	// Marshaling of parameter '___data3' to native representation
	uint8_t* ____data3_marshaled = NULL;
	if (___data3 != NULL)
	{
		____data3_marshaled = reinterpret_cast<uint8_t*>((___data3)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEWriteCharacteristic)(____name0_marshaled, ____service1_marshaled, ____characteristic2_marshaled, ____data3_marshaled, ___length4, static_cast<int32_t>(___withResponse5));

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

	// Marshaling cleanup of parameter '___service1' native representation
	il2cpp_codegen_marshal_free(____service1_marshaled);
	____service1_marshaled = NULL;

	// Marshaling cleanup of parameter '___characteristic2' native representation
	il2cpp_codegen_marshal_free(____characteristic2_marshaled);
	____characteristic2_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _iOSBluetoothLESubscribeCharacteristic(char*, char*, char*);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLESubscribeCharacteristic(System.String,System.String,System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLESubscribeCharacteristic_m4162793143 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Marshaling of parameter '___service1' to native representation
	char* ____service1_marshaled = NULL;
	____service1_marshaled = il2cpp_codegen_marshal_string(___service1);

	// Marshaling of parameter '___characteristic2' to native representation
	char* ____characteristic2_marshaled = NULL;
	____characteristic2_marshaled = il2cpp_codegen_marshal_string(___characteristic2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLESubscribeCharacteristic)(____name0_marshaled, ____service1_marshaled, ____characteristic2_marshaled);

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

	// Marshaling cleanup of parameter '___service1' native representation
	il2cpp_codegen_marshal_free(____service1_marshaled);
	____service1_marshaled = NULL;

	// Marshaling cleanup of parameter '___characteristic2' native representation
	il2cpp_codegen_marshal_free(____characteristic2_marshaled);
	____characteristic2_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _iOSBluetoothLEUnSubscribeCharacteristic(char*, char*, char*);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEUnSubscribeCharacteristic(System.String,System.String,System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEUnSubscribeCharacteristic_m69770214 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Marshaling of parameter '___service1' to native representation
	char* ____service1_marshaled = NULL;
	____service1_marshaled = il2cpp_codegen_marshal_string(___service1);

	// Marshaling of parameter '___characteristic2' to native representation
	char* ____characteristic2_marshaled = NULL;
	____characteristic2_marshaled = il2cpp_codegen_marshal_string(___characteristic2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEUnSubscribeCharacteristic)(____name0_marshaled, ____service1_marshaled, ____characteristic2_marshaled);

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

	// Marshaling cleanup of parameter '___service1' native representation
	il2cpp_codegen_marshal_free(____service1_marshaled);
	____service1_marshaled = NULL;

	// Marshaling cleanup of parameter '___characteristic2' native representation
	il2cpp_codegen_marshal_free(____characteristic2_marshaled);
	____characteristic2_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _iOSBluetoothLEPeripheralName(char*);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEPeripheralName(System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEPeripheralName_m3800903089 (Il2CppObject * __this /* static, unused */, String_t* ___newName0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___newName0' to native representation
	char* ____newName0_marshaled = NULL;
	____newName0_marshaled = il2cpp_codegen_marshal_string(___newName0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEPeripheralName)(____newName0_marshaled);

	// Marshaling cleanup of parameter '___newName0' native representation
	il2cpp_codegen_marshal_free(____newName0_marshaled);
	____newName0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _iOSBluetoothLECreateService(char*, int32_t);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLECreateService(System.String,System.Boolean)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLECreateService_m1860289310 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, bool ___primary1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t);

	// Marshaling of parameter '___uuid0' to native representation
	char* ____uuid0_marshaled = NULL;
	____uuid0_marshaled = il2cpp_codegen_marshal_string(___uuid0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLECreateService)(____uuid0_marshaled, static_cast<int32_t>(___primary1));

	// Marshaling cleanup of parameter '___uuid0' native representation
	il2cpp_codegen_marshal_free(____uuid0_marshaled);
	____uuid0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _iOSBluetoothLERemoveService(char*);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveService(System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLERemoveService_m3534109365 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___uuid0' to native representation
	char* ____uuid0_marshaled = NULL;
	____uuid0_marshaled = il2cpp_codegen_marshal_string(___uuid0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLERemoveService)(____uuid0_marshaled);

	// Marshaling cleanup of parameter '___uuid0' native representation
	il2cpp_codegen_marshal_free(____uuid0_marshaled);
	____uuid0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _iOSBluetoothLERemoveServices();
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveServices()
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLERemoveServices_m3994765694 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLERemoveServices)();

}
extern "C" void DEFAULT_CALL _iOSBluetoothLECreateCharacteristic(char*, int32_t, int32_t, uint8_t*, int32_t);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLECreateCharacteristic(System.String,System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLECreateCharacteristic_m1441784201 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, int32_t ___properties1, int32_t ___permissions2, ByteU5BU5D_t3397334013* ___data3, int32_t ___length4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, int32_t, uint8_t*, int32_t);

	// Marshaling of parameter '___uuid0' to native representation
	char* ____uuid0_marshaled = NULL;
	____uuid0_marshaled = il2cpp_codegen_marshal_string(___uuid0);

	// Marshaling of parameter '___data3' to native representation
	uint8_t* ____data3_marshaled = NULL;
	if (___data3 != NULL)
	{
		____data3_marshaled = reinterpret_cast<uint8_t*>((___data3)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLECreateCharacteristic)(____uuid0_marshaled, ___properties1, ___permissions2, ____data3_marshaled, ___length4);

	// Marshaling cleanup of parameter '___uuid0' native representation
	il2cpp_codegen_marshal_free(____uuid0_marshaled);
	____uuid0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _iOSBluetoothLERemoveCharacteristic(char*);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveCharacteristic(System.String)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLERemoveCharacteristic_m4251113393 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___uuid0' to native representation
	char* ____uuid0_marshaled = NULL;
	____uuid0_marshaled = il2cpp_codegen_marshal_string(___uuid0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLERemoveCharacteristic)(____uuid0_marshaled);

	// Marshaling cleanup of parameter '___uuid0' native representation
	il2cpp_codegen_marshal_free(____uuid0_marshaled);
	____uuid0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _iOSBluetoothLERemoveCharacteristics();
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLERemoveCharacteristics()
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLERemoveCharacteristics_m3371340286 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLERemoveCharacteristics)();

}
extern "C" void DEFAULT_CALL _iOSBluetoothLEStartAdvertising();
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEStartAdvertising()
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEStartAdvertising_m2391874196 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEStartAdvertising)();

}
extern "C" void DEFAULT_CALL _iOSBluetoothLEStopAdvertising();
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEStopAdvertising()
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEStopAdvertising_m2372726 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEStopAdvertising)();

}
extern "C" void DEFAULT_CALL _iOSBluetoothLEUpdateCharacteristicValue(char*, uint8_t*, int32_t);
// System.Void BluetoothLEHardwareInterface::_iOSBluetoothLEUpdateCharacteristicValue(System.String,System.Byte[],System.Int32)
extern "C"  void BluetoothLEHardwareInterface__iOSBluetoothLEUpdateCharacteristicValue_m85558683 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, ByteU5BU5D_t3397334013* ___data1, int32_t ___length2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, uint8_t*, int32_t);

	// Marshaling of parameter '___uuid0' to native representation
	char* ____uuid0_marshaled = NULL;
	____uuid0_marshaled = il2cpp_codegen_marshal_string(___uuid0);

	// Marshaling of parameter '___data1' to native representation
	uint8_t* ____data1_marshaled = NULL;
	if (___data1 != NULL)
	{
		____data1_marshaled = reinterpret_cast<uint8_t*>((___data1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_iOSBluetoothLEUpdateCharacteristicValue)(____uuid0_marshaled, ____data1_marshaled, ___length2);

	// Marshaling cleanup of parameter '___uuid0' native representation
	il2cpp_codegen_marshal_free(____uuid0_marshaled);
	____uuid0_marshaled = NULL;

}
// System.Void BluetoothLEHardwareInterface::Log(System.String)
extern "C"  void BluetoothLEHardwareInterface_Log_m3949775483 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		String_t* L_1 = ___message0;
		BluetoothLEHardwareInterface__iOSBluetoothLELog_m3965866644(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0010:
	{
		return;
	}
}
// BluetoothDeviceScript BluetoothLEHardwareInterface::Initialize(System.Boolean,System.Boolean,System.Action,System.Action`1<System.String>)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisBluetoothDeviceScript_t810775987_m1605496783_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral430393278;
extern Il2CppCodeGenString* _stringLiteral3215533806;
extern Il2CppCodeGenString* _stringLiteral1338385592;
extern const uint32_t BluetoothLEHardwareInterface_Initialize_m3691300628_MetadataUsageId;
extern "C"  BluetoothDeviceScript_t810775987 * BluetoothLEHardwareInterface_Initialize_m3691300628 (Il2CppObject * __this /* static, unused */, bool ___asCentral0, bool ___asPeripheral1, Action_t3226471752 * ___action2, Action_1_t1831019615 * ___errorAction3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_Initialize_m3691300628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->set_bluetoothDeviceScript_0((BluetoothDeviceScript_t810775987 *)NULL);
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral430393278, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0059;
		}
	}
	{
		GameObject_t1756533147 * L_3 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_3, _stringLiteral430393278, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t1756533147 * L_4 = V_0;
		NullCheck(L_4);
		BluetoothDeviceScript_t810775987 * L_5 = GameObject_AddComponent_TisBluetoothDeviceScript_t810775987_m1605496783(L_4, /*hidden argument*/GameObject_AddComponent_TisBluetoothDeviceScript_t810775987_m1605496783_MethodInfo_var);
		((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->set_bluetoothDeviceScript_0(L_5);
		BluetoothDeviceScript_t810775987 * L_6 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0059;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_8 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_t3226471752 * L_9 = ___action2;
		NullCheck(L_8);
		L_8->set_InitializedAction_3(L_9);
		BluetoothDeviceScript_t810775987 * L_10 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_1_t1831019615 * L_11 = ___errorAction3;
		NullCheck(L_10);
		L_10->set_ErrorAction_5(L_11);
	}

IL_0059:
	{
		GameObject_t1756533147 * L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		bool L_13 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0092;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_14 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_14, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_008d;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_16 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		NullCheck(L_16);
		Component_SendMessage_m913946877(L_16, _stringLiteral3215533806, _stringLiteral1338385592, /*hidden argument*/NULL);
	}

IL_008d:
	{
		goto IL_0099;
	}

IL_0092:
	{
		bool L_17 = ___asCentral0;
		bool L_18 = ___asPeripheral1;
		BluetoothLEHardwareInterface__iOSBluetoothLEInitialize_m4214605180(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
	}

IL_0099:
	{
		BluetoothDeviceScript_t810775987 * L_19 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		return L_19;
	}
}
// System.Void BluetoothLEHardwareInterface::DeInitialize(System.Action)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3215533806;
extern Il2CppCodeGenString* _stringLiteral1410547565;
extern const uint32_t BluetoothLEHardwareInterface_DeInitialize_m2251043401_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_DeInitialize_m2251043401 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___action0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_DeInitialize_m2251043401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BluetoothDeviceScript_t810775987 * L_0 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_2 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_t3226471752 * L_3 = ___action0;
		NullCheck(L_2);
		L_2->set_DeinitializedAction_4(L_3);
	}

IL_001b:
	{
		bool L_4 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004e;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_5 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0049;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_7 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		NullCheck(L_7);
		Component_SendMessage_m913946877(L_7, _stringLiteral3215533806, _stringLiteral1410547565, /*hidden argument*/NULL);
	}

IL_0049:
	{
		goto IL_0053;
	}

IL_004e:
	{
		BluetoothLEHardwareInterface__iOSBluetoothLEDeInitialize_m1183275517(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0053:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::FinishDeInitialize()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral430393278;
extern const uint32_t BluetoothLEHardwareInterface_FinishDeInitialize_m3772842739_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_FinishDeInitialize_m3772842739 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_FinishDeInitialize_m3772842739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral430393278, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		GameObject_t1756533147 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::PauseMessages(System.Boolean)
extern "C"  void BluetoothLEHardwareInterface_PauseMessages_m4020506122 (Il2CppObject * __this /* static, unused */, bool ___isPaused0, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = ___isPaused0;
		BluetoothLEHardwareInterface__iOSBluetoothLEPauseMessages_m12473301(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0010:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::ScanForPeripheralsWithServices(System.String[],System.Action`2<System.String,System.String>,System.Action`4<System.String,System.String,System.Int32,System.Byte[]>)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2045495756_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029394;
extern const uint32_t BluetoothLEHardwareInterface_ScanForPeripheralsWithServices_m3085173196_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_ScanForPeripheralsWithServices_m3085173196 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___serviceUUIDs0, Action_2_t4234541925 * ___action1, Action_4_t2314457268 * ___actionAdvertisingInfo2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_ScanForPeripheralsWithServices_m3085173196_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	StringU5BU5D_t1642385972* V_2 = NULL;
	int32_t V_3 = 0;
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_00a9;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_1 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004e;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_3 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_2_t4234541925 * L_4 = ___action1;
		NullCheck(L_3);
		L_3->set_DiscoveredPeripheralAction_9(L_4);
		BluetoothDeviceScript_t810775987 * L_5 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_4_t2314457268 * L_6 = ___actionAdvertisingInfo2;
		NullCheck(L_5);
		L_5->set_DiscoveredPeripheralWithAdvertisingInfoAction_10(L_6);
		BluetoothDeviceScript_t810775987 * L_7 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		NullCheck(L_7);
		List_1_t1398341365 * L_8 = L_7->get_DiscoveredDeviceList_2();
		if (!L_8)
		{
			goto IL_004e;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_9 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		NullCheck(L_9);
		List_1_t1398341365 * L_10 = L_9->get_DiscoveredDeviceList_2();
		NullCheck(L_10);
		List_1_Clear_m2045495756(L_10, /*hidden argument*/List_1_Clear_m2045495756_MethodInfo_var);
	}

IL_004e:
	{
		V_0 = (String_t*)NULL;
		StringU5BU5D_t1642385972* L_11 = ___serviceUUIDs0;
		if (!L_11)
		{
			goto IL_009c;
		}
	}
	{
		StringU5BU5D_t1642385972* L_12 = ___serviceUUIDs0;
		NullCheck(L_12);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_009c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_13;
		StringU5BU5D_t1642385972* L_14 = ___serviceUUIDs0;
		V_2 = L_14;
		V_3 = 0;
		goto IL_0083;
	}

IL_006e:
	{
		StringU5BU5D_t1642385972* L_15 = V_2;
		int32_t L_16 = V_3;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		String_t* L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_1 = L_18;
		String_t* L_19 = V_0;
		String_t* L_20 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m612901809(NULL /*static, unused*/, L_19, L_20, _stringLiteral372029394, /*hidden argument*/NULL);
		V_0 = L_21;
		int32_t L_22 = V_3;
		V_3 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_23 = V_3;
		StringU5BU5D_t1642385972* L_24 = V_2;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length)))))))
		{
			goto IL_006e;
		}
	}
	{
		String_t* L_25 = V_0;
		String_t* L_26 = V_0;
		NullCheck(L_26);
		int32_t L_27 = String_get_Length_m1606060069(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		String_t* L_28 = String_Substring_m12482732(L_25, 0, ((int32_t)((int32_t)L_27-(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_28;
	}

IL_009c:
	{
		String_t* L_29 = V_0;
		Action_4_t2314457268 * L_30 = ___actionAdvertisingInfo2;
		BluetoothLEHardwareInterface__iOSBluetoothLEScanForPeripheralsWithServices_m1764614798(NULL /*static, unused*/, L_29, (bool)((((int32_t)((((Il2CppObject*)(Action_4_t2314457268 *)L_30) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_00a9:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::RetrieveListOfPeripheralsWithServices(System.String[],System.Action`2<System.String,System.String>)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2045495756_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029394;
extern const uint32_t BluetoothLEHardwareInterface_RetrieveListOfPeripheralsWithServices_m3880900408_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_RetrieveListOfPeripheralsWithServices_m3880900408 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___serviceUUIDs0, Action_2_t4234541925 * ___action1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_RetrieveListOfPeripheralsWithServices_m3880900408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	StringU5BU5D_t1642385972* V_2 = NULL;
	int32_t V_3 = 0;
	String_t* G_B7_0 = NULL;
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0095;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_1 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0043;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_3 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_2_t4234541925 * L_4 = ___action1;
		NullCheck(L_3);
		L_3->set_RetrievedConnectedPeripheralAction_11(L_4);
		BluetoothDeviceScript_t810775987 * L_5 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		NullCheck(L_5);
		List_1_t1398341365 * L_6 = L_5->get_DiscoveredDeviceList_2();
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_7 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		NullCheck(L_7);
		List_1_t1398341365 * L_8 = L_7->get_DiscoveredDeviceList_2();
		NullCheck(L_8);
		List_1_Clear_m2045495756(L_8, /*hidden argument*/List_1_Clear_m2045495756_MethodInfo_var);
	}

IL_0043:
	{
		StringU5BU5D_t1642385972* L_9 = ___serviceUUIDs0;
		NullCheck(L_9);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B7_0 = L_10;
		goto IL_0057;
	}

IL_0056:
	{
		G_B7_0 = ((String_t*)(NULL));
	}

IL_0057:
	{
		V_0 = G_B7_0;
		StringU5BU5D_t1642385972* L_11 = ___serviceUUIDs0;
		V_2 = L_11;
		V_3 = 0;
		goto IL_0076;
	}

IL_0061:
	{
		StringU5BU5D_t1642385972* L_12 = V_2;
		int32_t L_13 = V_3;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		String_t* L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_1 = L_15;
		String_t* L_16 = V_0;
		String_t* L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m612901809(NULL /*static, unused*/, L_16, L_17, _stringLiteral372029394, /*hidden argument*/NULL);
		V_0 = L_18;
		int32_t L_19 = V_3;
		V_3 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0076:
	{
		int32_t L_20 = V_3;
		StringU5BU5D_t1642385972* L_21 = V_2;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0061;
		}
	}
	{
		String_t* L_22 = V_0;
		String_t* L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = String_get_Length_m1606060069(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_25 = String_Substring_m12482732(L_22, 0, ((int32_t)((int32_t)L_24-(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_25;
		String_t* L_26 = V_0;
		BluetoothLEHardwareInterface__iOSBluetoothLERetrieveListOfPeripheralsWithServices_m143887212(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
	}

IL_0095:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::StopScan()
extern "C"  void BluetoothLEHardwareInterface_StopScan_m1034974532 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		BluetoothLEHardwareInterface__iOSBluetoothLEStopScan_m107073007(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000f:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::ConnectToPeripheral(System.String,System.Action`1<System.String>,System.Action`2<System.String,System.String>,System.Action`3<System.String,System.String,System.String>,System.Action`1<System.String>)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t BluetoothLEHardwareInterface_ConnectToPeripheral_m2997112613_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_ConnectToPeripheral_m2997112613 (Il2CppObject * __this /* static, unused */, String_t* ___name0, Action_1_t1831019615 * ___connectAction1, Action_2_t4234541925 * ___serviceAction2, Action_3_t3256166369 * ___characteristicAction3, Action_1_t1831019615 * ___disconnectAction4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_ConnectToPeripheral_m2997112613_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_004d;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_1 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0047;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_3 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_1_t1831019615 * L_4 = ___connectAction1;
		NullCheck(L_3);
		L_3->set_ConnectedPeripheralAction_13(L_4);
		BluetoothDeviceScript_t810775987 * L_5 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_2_t4234541925 * L_6 = ___serviceAction2;
		NullCheck(L_5);
		L_5->set_DiscoveredServiceAction_16(L_6);
		BluetoothDeviceScript_t810775987 * L_7 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_3_t3256166369 * L_8 = ___characteristicAction3;
		NullCheck(L_7);
		L_7->set_DiscoveredCharacteristicAction_17(L_8);
		BluetoothDeviceScript_t810775987 * L_9 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_1_t1831019615 * L_10 = ___disconnectAction4;
		NullCheck(L_9);
		L_9->set_ConnectedDisconnectPeripheralAction_14(L_10);
	}

IL_0047:
	{
		String_t* L_11 = ___name0;
		BluetoothLEHardwareInterface__iOSBluetoothLEConnectToPeripheral_m1210203199(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_004d:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::DisconnectPeripheral(System.String,System.Action`1<System.String>)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t BluetoothLEHardwareInterface_DisconnectPeripheral_m3085347731_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_DisconnectPeripheral_m3085347731 (Il2CppObject * __this /* static, unused */, String_t* ___name0, Action_1_t1831019615 * ___action1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_DisconnectPeripheral_m3085347731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_1 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_3 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_1_t1831019615 * L_4 = ___action1;
		NullCheck(L_3);
		L_3->set_DisconnectedPeripheralAction_15(L_4);
	}

IL_0025:
	{
		String_t* L_5 = ___name0;
		BluetoothLEHardwareInterface__iOSBluetoothLEDisconnectPeripheral_m2753876142(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::ReadCharacteristic(System.String,System.String,System.String,System.Action`2<System.String,System.Byte[]>)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t BluetoothLEHardwareInterface_ReadCharacteristic_m2865661240_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_ReadCharacteristic_m2865661240 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, Action_2_t1307688409 * ___action3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_ReadCharacteristic_m2865661240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002d;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_1 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_3 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_2_t1307688409 * L_4 = ___action3;
		NullCheck(L_3);
		L_3->set_DidUpdateCharacteristicValueAction_21(L_4);
	}

IL_0025:
	{
		String_t* L_5 = ___name0;
		String_t* L_6 = ___service1;
		String_t* L_7 = ___characteristic2;
		BluetoothLEHardwareInterface__iOSBluetoothLEReadCharacteristic_m1988194505(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::WriteCharacteristic(System.String,System.String,System.String,System.Byte[],System.Int32,System.Boolean,System.Action`1<System.String>)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t BluetoothLEHardwareInterface_WriteCharacteristic_m1354208428_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_WriteCharacteristic_m1354208428 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, ByteU5BU5D_t3397334013* ___data3, int32_t ___length4, bool ___withResponse5, Action_1_t1831019615 * ___action6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_WriteCharacteristic_m1354208428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0033;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_1 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_3 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_1_t1831019615 * L_4 = ___action6;
		NullCheck(L_3);
		L_3->set_DidWriteCharacteristicAction_18(L_4);
	}

IL_0026:
	{
		String_t* L_5 = ___name0;
		String_t* L_6 = ___service1;
		String_t* L_7 = ___characteristic2;
		ByteU5BU5D_t3397334013* L_8 = ___data3;
		int32_t L_9 = ___length4;
		bool L_10 = ___withResponse5;
		BluetoothLEHardwareInterface__iOSBluetoothLEWriteCharacteristic_m2337396763(NULL /*static, unused*/, L_5, L_6, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::SubscribeCharacteristic(System.String,System.String,System.String,System.Action`1<System.String>,System.Action`2<System.String,System.Byte[]>)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t BluetoothLEHardwareInterface_SubscribeCharacteristic_m3873716912_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_SubscribeCharacteristic_m3873716912 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, Action_1_t1831019615 * ___notificationAction3, Action_2_t1307688409 * ___action4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_SubscribeCharacteristic_m3873716912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_1 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_3 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_1_t1831019615 * L_4 = ___notificationAction3;
		NullCheck(L_3);
		L_3->set_DidUpdateNotificationStateForCharacteristicAction_19(L_4);
		BluetoothDeviceScript_t810775987 * L_5 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_2_t1307688409 * L_6 = ___action4;
		NullCheck(L_5);
		L_5->set_DidUpdateCharacteristicValueAction_21(L_6);
	}

IL_0031:
	{
		String_t* L_7 = ___name0;
		String_t* L_8 = ___service1;
		String_t* L_9 = ___characteristic2;
		BluetoothLEHardwareInterface__iOSBluetoothLESubscribeCharacteristic_m4162793143(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::SubscribeCharacteristicWithDeviceAddress(System.String,System.String,System.String,System.Action`2<System.String,System.String>,System.Action`3<System.String,System.String,System.Byte[]>)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t BluetoothLEHardwareInterface_SubscribeCharacteristicWithDeviceAddress_m3073680728_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_SubscribeCharacteristicWithDeviceAddress_m3073680728 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, Action_2_t4234541925 * ___notificationAction3, Action_3_t329312853 * ___action4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_SubscribeCharacteristicWithDeviceAddress_m3073680728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_1 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_3 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_2_t4234541925 * L_4 = ___notificationAction3;
		NullCheck(L_3);
		L_3->set_DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_20(L_4);
		BluetoothDeviceScript_t810775987 * L_5 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_3_t329312853 * L_6 = ___action4;
		NullCheck(L_5);
		L_5->set_DidUpdateCharacteristicValueWithDeviceAddressAction_22(L_6);
	}

IL_0031:
	{
		String_t* L_7 = ___name0;
		String_t* L_8 = ___service1;
		String_t* L_9 = ___characteristic2;
		BluetoothLEHardwareInterface__iOSBluetoothLESubscribeCharacteristic_m4162793143(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::UnSubscribeCharacteristic(System.String,System.String,System.String,System.Action`1<System.String>)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t BluetoothLEHardwareInterface_UnSubscribeCharacteristic_m614746437_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_UnSubscribeCharacteristic_m614746437 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___service1, String_t* ___characteristic2, Action_1_t1831019615 * ___action3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_UnSubscribeCharacteristic_m614746437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002d;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_1 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_3 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_1_t1831019615 * L_4 = ___action3;
		NullCheck(L_3);
		L_3->set_DidUpdateNotificationStateForCharacteristicAction_19(L_4);
	}

IL_0025:
	{
		String_t* L_5 = ___name0;
		String_t* L_6 = ___service1;
		String_t* L_7 = ___characteristic2;
		BluetoothLEHardwareInterface__iOSBluetoothLEUnSubscribeCharacteristic_m69770214(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::PeripheralName(System.String)
extern "C"  void BluetoothLEHardwareInterface_PeripheralName_m4089551076 (Il2CppObject * __this /* static, unused */, String_t* ___newName0, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		String_t* L_1 = ___newName0;
		BluetoothLEHardwareInterface__iOSBluetoothLEPeripheralName_m3800903089(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0010:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::CreateService(System.String,System.Boolean,System.Action`1<System.String>)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t BluetoothLEHardwareInterface_CreateService_m4006704813_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_CreateService_m4006704813 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, bool ___primary1, Action_1_t1831019615 * ___action2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_CreateService_m4006704813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002c;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_1 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_3 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_1_t1831019615 * L_4 = ___action2;
		NullCheck(L_3);
		L_3->set_ServiceAddedAction_6(L_4);
	}

IL_0025:
	{
		String_t* L_5 = ___uuid0;
		bool L_6 = ___primary1;
		BluetoothLEHardwareInterface__iOSBluetoothLECreateService_m1860289310(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::RemoveService(System.String)
extern "C"  void BluetoothLEHardwareInterface_RemoveService_m3808888640 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		String_t* L_1 = ___uuid0;
		BluetoothLEHardwareInterface__iOSBluetoothLERemoveService_m3534109365(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0010:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::RemoveServices()
extern "C"  void BluetoothLEHardwareInterface_RemoveServices_m399109141 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		BluetoothLEHardwareInterface__iOSBluetoothLERemoveServices_m3994765694(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000f:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::CreateCharacteristic(System.String,BluetoothLEHardwareInterface/CBCharacteristicProperties,BluetoothLEHardwareInterface/CBAttributePermissions,System.Byte[],System.Int32,System.Action`2<System.String,System.Byte[]>)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t BluetoothLEHardwareInterface_CreateCharacteristic_m1598497468_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_CreateCharacteristic_m1598497468 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, int32_t ___properties1, int32_t ___permissions2, ByteU5BU5D_t3397334013* ___data3, int32_t ___length4, Action_2_t1307688409 * ___action5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_CreateCharacteristic_m1598497468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0031;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_1 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_3 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_2_t1307688409 * L_4 = ___action5;
		NullCheck(L_3);
		L_3->set_PeripheralReceivedWriteDataAction_12(L_4);
	}

IL_0026:
	{
		String_t* L_5 = ___uuid0;
		int32_t L_6 = ___properties1;
		int32_t L_7 = ___permissions2;
		ByteU5BU5D_t3397334013* L_8 = ___data3;
		int32_t L_9 = ___length4;
		BluetoothLEHardwareInterface__iOSBluetoothLECreateCharacteristic_m1441784201(NULL /*static, unused*/, L_5, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::RemoveCharacteristic(System.String)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t BluetoothLEHardwareInterface_RemoveCharacteristic_m3689193062_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_RemoveCharacteristic_m3689193062 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_RemoveCharacteristic_m3689193062_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_1 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_3 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		NullCheck(L_3);
		L_3->set_PeripheralReceivedWriteDataAction_12((Action_2_t1307688409 *)NULL);
	}

IL_0025:
	{
		String_t* L_4 = ___uuid0;
		BluetoothLEHardwareInterface__iOSBluetoothLERemoveCharacteristic_m4251113393(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::RemoveCharacteristics()
extern "C"  void BluetoothLEHardwareInterface_RemoveCharacteristics_m2071299125 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		BluetoothLEHardwareInterface__iOSBluetoothLERemoveCharacteristics_m3371340286(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000f:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::StartAdvertising(System.Action)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t BluetoothLEHardwareInterface_StartAdvertising_m2707254606_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_StartAdvertising_m2707254606 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___action0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_StartAdvertising_m2707254606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002a;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_1 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_3 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_t3226471752 * L_4 = ___action0;
		NullCheck(L_3);
		L_3->set_StartedAdvertisingAction_7(L_4);
	}

IL_0025:
	{
		BluetoothLEHardwareInterface__iOSBluetoothLEStartAdvertising_m2391874196(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::StopAdvertising(System.Action)
extern Il2CppClass* BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t BluetoothLEHardwareInterface_StopAdvertising_m1625577478_MetadataUsageId;
extern "C"  void BluetoothLEHardwareInterface_StopAdvertising_m1625577478 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___action0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BluetoothLEHardwareInterface_StopAdvertising_m1625577478_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002a;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_1 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		BluetoothDeviceScript_t810775987 * L_3 = ((BluetoothLEHardwareInterface_t4023789856_StaticFields*)BluetoothLEHardwareInterface_t4023789856_il2cpp_TypeInfo_var->static_fields)->get_bluetoothDeviceScript_0();
		Action_t3226471752 * L_4 = ___action0;
		NullCheck(L_3);
		L_3->set_StoppedAdvertisingAction_8(L_4);
	}

IL_0025:
	{
		BluetoothLEHardwareInterface__iOSBluetoothLEStopAdvertising_m2372726(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void BluetoothLEHardwareInterface::UpdateCharacteristicValue(System.String,System.Byte[],System.Int32)
extern "C"  void BluetoothLEHardwareInterface_UpdateCharacteristicValue_m1021036518 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, ByteU5BU5D_t3397334013* ___data1, int32_t ___length2, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = ___uuid0;
		ByteU5BU5D_t3397334013* L_2 = ___data1;
		int32_t L_3 = ___length2;
		BluetoothLEHardwareInterface__iOSBluetoothLEUpdateCharacteristicValue_m85558683(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::.ctor()
extern Il2CppClass* List_1_t3586366772_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2357124814_MethodInfo_var;
extern const uint32_t Highlighter__ctor_m1870385233_MetadataUsageId;
extern "C"  void Highlighter__ctor_m1870385233 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter__ctor_m1870385233_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m1909920690(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_occluderColor_4(L_0);
		List_1_t3586366772 * L_1 = (List_1_t3586366772 *)il2cpp_codegen_object_new(List_1_t3586366772_il2cpp_TypeInfo_var);
		List_1__ctor_m2357124814(L_1, /*hidden argument*/List_1__ctor_m2357124814_MethodInfo_var);
		__this->set_highlightableRenderers_15(L_1);
		__this->set__once_21((-1));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::ReinitMaterials()
extern "C"  void Highlighter_ReinitMaterials_m2811231566 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	{
		__this->set_renderersDirty_16((bool)1);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::OnParams(UnityEngine.Color)
extern "C"  void Highlighter_OnParams_m3546271198 (Highlighter_t958778585 * __this, Color_t2020392075  ___color0, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = ___color0;
		__this->set_onceColor_27(L_0);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::On()
extern "C"  void Highlighter_On_m3369021276 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	{
		Highlighter_set_once_m951767820(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::On(UnityEngine.Color)
extern "C"  void Highlighter_On_m2605899832 (Highlighter_t958778585 * __this, Color_t2020392075  ___color0, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = ___color0;
		__this->set_onceColor_27(L_0);
		Highlighter_set_once_m951767820(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::FlashingParams(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  void Highlighter_FlashingParams_m3382565974 (Highlighter_t958778585 * __this, Color_t2020392075  ___color10, Color_t2020392075  ___color21, float ___freq2, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = ___color10;
		__this->set_flashingColorMin_29(L_0);
		Color_t2020392075  L_1 = ___color21;
		__this->set_flashingColorMax_30(L_1);
		float L_2 = ___freq2;
		__this->set_flashingFreq_28(L_2);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::FlashingOn()
extern "C"  void Highlighter_FlashingOn_m194070706 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	{
		__this->set_flashing_22((bool)1);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::FlashingOn(UnityEngine.Color,UnityEngine.Color)
extern "C"  void Highlighter_FlashingOn_m1390834640 (Highlighter_t958778585 * __this, Color_t2020392075  ___color10, Color_t2020392075  ___color21, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = ___color10;
		__this->set_flashingColorMin_29(L_0);
		Color_t2020392075  L_1 = ___color21;
		__this->set_flashingColorMax_30(L_1);
		__this->set_flashing_22((bool)1);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::FlashingOn(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  void Highlighter_FlashingOn_m2927979613 (Highlighter_t958778585 * __this, Color_t2020392075  ___color10, Color_t2020392075  ___color21, float ___freq2, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = ___color10;
		__this->set_flashingColorMin_29(L_0);
		Color_t2020392075  L_1 = ___color21;
		__this->set_flashingColorMax_30(L_1);
		float L_2 = ___freq2;
		__this->set_flashingFreq_28(L_2);
		__this->set_flashing_22((bool)1);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::FlashingOn(System.Single)
extern "C"  void Highlighter_FlashingOn_m1790714077 (Highlighter_t958778585 * __this, float ___freq0, const MethodInfo* method)
{
	{
		float L_0 = ___freq0;
		__this->set_flashingFreq_28(L_0);
		__this->set_flashing_22((bool)1);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::FlashingOff()
extern "C"  void Highlighter_FlashingOff_m1115705326 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	{
		__this->set_flashing_22((bool)0);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::FlashingSwitch()
extern "C"  void Highlighter_FlashingSwitch_m2852614083 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_flashing_22();
		__this->set_flashing_22((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::ConstantParams(UnityEngine.Color)
extern "C"  void Highlighter_ConstantParams_m3662601853 (Highlighter_t958778585 * __this, Color_t2020392075  ___color0, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = ___color0;
		__this->set_constantColor_31(L_0);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::ConstantOn(System.Single)
extern "C"  void Highlighter_ConstantOn_m933978095 (Highlighter_t958778585 * __this, float ___time0, const MethodInfo* method)
{
	Highlighter_t958778585 * G_B2_0 = NULL;
	Highlighter_t958778585 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	Highlighter_t958778585 * G_B3_1 = NULL;
	{
		float L_0 = ___time0;
		G_B1_0 = __this;
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			G_B2_0 = __this;
			goto IL_0012;
		}
	}
	{
		float L_1 = ___time0;
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_transitionTime_26(G_B3_0);
		__this->set_transitionTarget_25((1.0f));
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::ConstantOn(UnityEngine.Color,System.Single)
extern "C"  void Highlighter_ConstantOn_m2367712529 (Highlighter_t958778585 * __this, Color_t2020392075  ___color0, float ___time1, const MethodInfo* method)
{
	Highlighter_t958778585 * G_B2_0 = NULL;
	Highlighter_t958778585 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	Highlighter_t958778585 * G_B3_1 = NULL;
	{
		Color_t2020392075  L_0 = ___color0;
		__this->set_constantColor_31(L_0);
		float L_1 = ___time1;
		G_B1_0 = __this;
		if ((!(((float)L_1) >= ((float)(0.0f)))))
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		float L_2 = ___time1;
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_001e;
	}

IL_0019:
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B2_0;
	}

IL_001e:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_transitionTime_26(G_B3_0);
		__this->set_transitionTarget_25((1.0f));
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::ConstantOff(System.Single)
extern "C"  void Highlighter_ConstantOff_m9553853 (Highlighter_t958778585 * __this, float ___time0, const MethodInfo* method)
{
	Highlighter_t958778585 * G_B2_0 = NULL;
	Highlighter_t958778585 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	Highlighter_t958778585 * G_B3_1 = NULL;
	{
		float L_0 = ___time0;
		G_B1_0 = __this;
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			G_B2_0 = __this;
			goto IL_0012;
		}
	}
	{
		float L_1 = ___time0;
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_transitionTime_26(G_B3_0);
		__this->set_transitionTarget_25((0.0f));
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::ConstantSwitch(System.Single)
extern "C"  void Highlighter_ConstantSwitch_m879137366 (Highlighter_t958778585 * __this, float ___time0, const MethodInfo* method)
{
	Highlighter_t958778585 * G_B2_0 = NULL;
	Highlighter_t958778585 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	Highlighter_t958778585 * G_B3_1 = NULL;
	Highlighter_t958778585 * G_B5_0 = NULL;
	Highlighter_t958778585 * G_B4_0 = NULL;
	float G_B6_0 = 0.0f;
	Highlighter_t958778585 * G_B6_1 = NULL;
	{
		float L_0 = ___time0;
		G_B1_0 = __this;
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			G_B2_0 = __this;
			goto IL_0012;
		}
	}
	{
		float L_1 = ___time0;
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_transitionTime_26(G_B3_0);
		float L_2 = __this->get_transitionTarget_25();
		G_B4_0 = __this;
		if ((!(((float)L_2) > ((float)(0.0f)))))
		{
			G_B5_0 = __this;
			goto IL_0037;
		}
	}
	{
		G_B6_0 = (0.0f);
		G_B6_1 = G_B4_0;
		goto IL_003c;
	}

IL_0037:
	{
		G_B6_0 = (1.0f);
		G_B6_1 = G_B5_0;
	}

IL_003c:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_transitionTarget_25(G_B6_0);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::ConstantOnImmediate()
extern "C"  void Highlighter_ConstantOnImmediate_m2971091603 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (1.0f);
		V_0 = L_0;
		__this->set_transitionTarget_25(L_0);
		float L_1 = V_0;
		__this->set_transitionValue_24(L_1);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::ConstantOnImmediate(UnityEngine.Color)
extern "C"  void Highlighter_ConstantOnImmediate_m704471569 (Highlighter_t958778585 * __this, Color_t2020392075  ___color0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Color_t2020392075  L_0 = ___color0;
		__this->set_constantColor_31(L_0);
		float L_1 = (1.0f);
		V_0 = L_1;
		__this->set_transitionTarget_25(L_1);
		float L_2 = V_0;
		__this->set_transitionValue_24(L_2);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::ConstantOffImmediate()
extern "C"  void Highlighter_ConstantOffImmediate_m4107465673 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (0.0f);
		V_0 = L_0;
		__this->set_transitionTarget_25(L_0);
		float L_1 = V_0;
		__this->set_transitionValue_24(L_1);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::ConstantSwitchImmediate()
extern "C"  void Highlighter_ConstantSwitchImmediate_m2315164720 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Highlighter_t958778585 * G_B2_0 = NULL;
	Highlighter_t958778585 * G_B2_1 = NULL;
	Highlighter_t958778585 * G_B1_0 = NULL;
	Highlighter_t958778585 * G_B1_1 = NULL;
	float G_B3_0 = 0.0f;
	Highlighter_t958778585 * G_B3_1 = NULL;
	Highlighter_t958778585 * G_B3_2 = NULL;
	{
		float L_0 = __this->get_transitionTarget_25();
		G_B1_0 = __this;
		G_B1_1 = __this;
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			G_B2_0 = __this;
			G_B2_1 = __this;
			goto IL_001c;
		}
	}
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0021;
	}

IL_001c:
	{
		G_B3_0 = (1.0f);
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0021:
	{
		float L_1 = G_B3_0;
		V_0 = L_1;
		NullCheck(G_B3_1);
		G_B3_1->set_transitionTarget_25(L_1);
		float L_2 = V_0;
		NullCheck(G_B3_2);
		G_B3_2->set_transitionValue_24(L_2);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::Off()
extern "C"  void Highlighter_Off_m2257449156 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Highlighter_set_once_m951767820(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_flashing_22((bool)0);
		float L_0 = (0.0f);
		V_0 = L_0;
		__this->set_transitionTarget_25(L_0);
		float L_1 = V_0;
		__this->set_transitionValue_24(L_1);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::Die()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Highlighter_Die_m1665957649_MetadataUsageId;
extern "C"  void Highlighter_Die_m1665957649 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_Die_m1665957649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::SeeThrough(System.Boolean)
extern "C"  void Highlighter_SeeThrough_m68342876 (Highlighter_t958778585 * __this, bool ___state0, const MethodInfo* method)
{
	{
		bool L_0 = ___state0;
		__this->set_seeThrough_12(L_0);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::SeeThroughOn()
extern "C"  void Highlighter_SeeThroughOn_m1888285312 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	{
		__this->set_seeThrough_12((bool)1);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::SeeThroughOff()
extern "C"  void Highlighter_SeeThroughOff_m3980756644 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	{
		__this->set_seeThrough_12((bool)0);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::SeeThroughSwitch()
extern "C"  void Highlighter_SeeThroughSwitch_m285041647 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_seeThrough_12();
		__this->set_seeThrough_12((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::OccluderOn()
extern "C"  void Highlighter_OccluderOn_m1893637277 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	{
		__this->set_occluder_13((bool)1);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::OccluderOff()
extern "C"  void Highlighter_OccluderOff_m843899443 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	{
		__this->set_occluder_13((bool)0);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::OccluderSwitch()
extern "C"  void Highlighter_OccluderSwitch_m951874390 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_occluder_13();
		__this->set_occluder_13((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		return;
	}
}
// System.Boolean HighlightingSystem.Highlighter::get_once()
extern "C"  bool Highlighter_get_once_m104830995 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__once_21();
		int32_t L_1 = Time_get_frameCount_m1198768813(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
// System.Void HighlightingSystem.Highlighter::set_once(System.Boolean)
extern "C"  void Highlighter_set_once_m951767820 (Highlighter_t958778585 * __this, bool ___value0, const MethodInfo* method)
{
	Highlighter_t958778585 * G_B2_0 = NULL;
	Highlighter_t958778585 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	Highlighter_t958778585 * G_B3_1 = NULL;
	{
		bool L_0 = ___value0;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = Time_get_frameCount_m1198768813(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = (-1);
		G_B3_1 = G_B2_0;
	}

IL_0012:
	{
		NullCheck(G_B3_1);
		G_B3_1->set__once_21(G_B3_0);
		return;
	}
}
// UnityEngine.Shader HighlightingSystem.Highlighter::get_opaqueShader()
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1439859418;
extern const uint32_t Highlighter_get_opaqueShader_m1430768539_MetadataUsageId;
extern "C"  Shader_t2430389951 * Highlighter_get_opaqueShader_m1430768539 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_get_opaqueShader_m1430768539_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		Shader_t2430389951 * L_0 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get__opaqueShader_32();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Shader_t2430389951 * L_2 = Shader_Find_m4179408078(NULL /*static, unused*/, _stringLiteral1439859418, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->set__opaqueShader_32(L_2);
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		Shader_t2430389951 * L_3 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get__opaqueShader_32();
		return L_3;
	}
}
// UnityEngine.Shader HighlightingSystem.Highlighter::get_transparentShader()
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2110246157;
extern const uint32_t Highlighter_get_transparentShader_m3015025486_MetadataUsageId;
extern "C"  Shader_t2430389951 * Highlighter_get_transparentShader_m3015025486 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_get_transparentShader_m3015025486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		Shader_t2430389951 * L_0 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get__transparentShader_33();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Shader_t2430389951 * L_2 = Shader_Find_m4179408078(NULL /*static, unused*/, _stringLiteral2110246157, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->set__transparentShader_33(L_2);
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		Shader_t2430389951 * L_3 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get__transparentShader_33();
		return L_3;
	}
}
// UnityEngine.Material HighlightingSystem.Highlighter::get_opaqueMaterial()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t193706927_il2cpp_TypeInfo_var;
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t Highlighter_get_opaqueMaterial_m1991552851_MetadataUsageId;
extern "C"  Material_t193706927 * Highlighter_get_opaqueMaterial_m1991552851 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_get_opaqueMaterial_m1991552851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get__opaqueMaterial_34();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		Shader_t2430389951 * L_2 = Highlighter_get_opaqueShader_m1430768539(NULL /*static, unused*/, /*hidden argument*/NULL);
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1897560860(L_3, L_2, /*hidden argument*/NULL);
		__this->set__opaqueMaterial_34(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		ShaderPropertyID_Initialize_m2760510245(NULL /*static, unused*/, /*hidden argument*/NULL);
		Material_t193706927 * L_4 = __this->get__opaqueMaterial_34();
		int32_t L_5 = ShaderPropertyID_get__ZTest_m2113714673(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_6 = __this->get_zTest_19();
		int32_t L_7 = Highlighter_GetZTest_m1622142992(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		Material_SetInt_m977568583(L_4, L_5, L_7, /*hidden argument*/NULL);
		Material_t193706927 * L_8 = __this->get__opaqueMaterial_34();
		int32_t L_9 = ShaderPropertyID_get__StencilRef_m1989750604(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_10 = __this->get_stencilRef_20();
		int32_t L_11 = Highlighter_GetStencilRef_m1953084609(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		Material_SetInt_m977568583(L_8, L_9, L_11, /*hidden argument*/NULL);
	}

IL_005c:
	{
		Material_t193706927 * L_12 = __this->get__opaqueMaterial_34();
		return L_12;
	}
}
// System.Void HighlightingSystem.Highlighter::Awake()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTransform_t3275118058_m235623703_MethodInfo_var;
extern const uint32_t Highlighter_Awake_m1238830174_MetadataUsageId;
extern "C"  void Highlighter_Awake_m1238830174 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_Awake_m1238830174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		ShaderPropertyID_Initialize_m2760510245(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_0 = Component_GetComponent_TisTransform_t3275118058_m235623703(__this, /*hidden argument*/Component_GetComponent_TisTransform_t3275118058_m235623703_MethodInfo_var);
		__this->set_tr_14(L_0);
		__this->set_renderersDirty_16((bool)1);
		int32_t L_1 = 1;
		V_0 = (bool)L_1;
		__this->set_zTest_19((bool)L_1);
		bool L_2 = V_0;
		__this->set_seeThrough_12(L_2);
		__this->set_mode_18(0);
		__this->set_stencilRef_20((bool)1);
		Highlighter_set_once_m951767820(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_flashing_22((bool)0);
		__this->set_occluder_13((bool)0);
		float L_3 = (0.0f);
		V_1 = L_3;
		__this->set_transitionTarget_25(L_3);
		float L_4 = V_1;
		__this->set_transitionValue_24(L_4);
		Color_t2020392075  L_5 = Color_get_red_m2410286591(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_onceColor_27(L_5);
		__this->set_flashingFreq_28((2.0f));
		Color_t2020392075  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_m1909920690(&L_6, (0.0f), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_flashingColorMin_29(L_6);
		Color_t2020392075  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m1909920690(&L_7, (0.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_flashingColorMax_30(L_7);
		Color_t2020392075  L_8 = Color_get_yellow_m3741935494(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_constantColor_31(L_8);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::OnEnable()
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1_Add_m2581154149_MethodInfo_var;
extern const uint32_t Highlighter_OnEnable_m278852705_MetadataUsageId;
extern "C"  void Highlighter_OnEnable_m278852705 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_OnEnable_m278852705_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		HashSet_1_t3587206735 * L_0 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_highlighters_8();
		NullCheck(L_0);
		HashSet_1_Add_m2581154149(L_0, __this, /*hidden argument*/HashSet_1_Add_m2581154149_MethodInfo_var);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::OnDisable()
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1_Remove_m3414687274_MethodInfo_var;
extern const uint32_t Highlighter_OnDisable_m3975849982_MetadataUsageId;
extern "C"  void Highlighter_OnDisable_m3975849982 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_OnDisable_m3975849982_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		HashSet_1_t3587206735 * L_0 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_highlighters_8();
		NullCheck(L_0);
		HashSet_1_Remove_m3414687274(L_0, __this, /*hidden argument*/HashSet_1_Remove_m3414687274_MethodInfo_var);
		Highlighter_ClearRenderers_m2537697750(__this, /*hidden argument*/NULL);
		__this->set_renderersDirty_16((bool)1);
		Highlighter_set_once_m951767820(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_flashing_22((bool)0);
		float L_1 = (0.0f);
		V_0 = L_1;
		__this->set_transitionTarget_25(L_1);
		float L_2 = V_0;
		__this->set_transitionValue_24(L_2);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::Update()
extern "C"  void Highlighter_Update_m1915287488 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	{
		Highlighter_UpdateTransition_m3266769167(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::ClearRenderers()
extern const MethodInfo* List_1_get_Count_m3417646876_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2912931613_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m712299851_MethodInfo_var;
extern const uint32_t Highlighter_ClearRenderers_m2537697750_MetadataUsageId;
extern "C"  void Highlighter_ClearRenderers_m2537697750 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_ClearRenderers_m2537697750_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	HighlighterRenderer_t4217245640 * V_1 = NULL;
	{
		List_1_t3586366772 * L_0 = __this->get_highlightableRenderers_15();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m3417646876(L_0, /*hidden argument*/List_1_get_Count_m3417646876_MethodInfo_var);
		V_0 = ((int32_t)((int32_t)L_1-(int32_t)1));
		goto IL_002b;
	}

IL_0013:
	{
		List_1_t3586366772 * L_2 = __this->get_highlightableRenderers_15();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		HighlighterRenderer_t4217245640 * L_4 = List_1_get_Item_m2912931613(L_2, L_3, /*hidden argument*/List_1_get_Item_m2912931613_MethodInfo_var);
		V_1 = L_4;
		HighlighterRenderer_t4217245640 * L_5 = V_1;
		NullCheck(L_5);
		HighlighterRenderer_SetState_m2777729094(L_5, (bool)0, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6-(int32_t)1));
	}

IL_002b:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) >= ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		List_1_t3586366772 * L_8 = __this->get_highlightableRenderers_15();
		NullCheck(L_8);
		List_1_Clear_m712299851(L_8, /*hidden argument*/List_1_Clear_m712299851_MethodInfo_var);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::UpdateRenderers()
extern Il2CppClass* List_1_t3921398993_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3577252413_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m949074065_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2524923786_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisHighlighterRenderer_t4217245640_m4110411296_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisHighlighterRenderer_t4217245640_m2107265519_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1903505042_MethodInfo_var;
extern const uint32_t Highlighter_UpdateRenderers_m4242998278_MetadataUsageId;
extern "C"  void Highlighter_UpdateRenderers_m4242998278 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_UpdateRenderers_m4242998278_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3921398993 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	GameObject_t1756533147 * V_3 = NULL;
	HighlighterRenderer_t4217245640 * V_4 = NULL;
	{
		bool L_0 = __this->get_renderersDirty_16();
		if (!L_0)
		{
			goto IL_0095;
		}
	}
	{
		Highlighter_ClearRenderers_m2537697750(__this, /*hidden argument*/NULL);
		List_1_t3921398993 * L_1 = (List_1_t3921398993 *)il2cpp_codegen_object_new(List_1_t3921398993_il2cpp_TypeInfo_var);
		List_1__ctor_m3577252413(L_1, /*hidden argument*/List_1__ctor_m3577252413_MethodInfo_var);
		V_0 = L_1;
		Transform_t3275118058 * L_2 = __this->get_tr_14();
		List_1_t3921398993 * L_3 = V_0;
		Highlighter_GrabRenderers_m3766673824(__this, L_2, L_3, /*hidden argument*/NULL);
		V_1 = 0;
		List_1_t3921398993 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m949074065(L_4, /*hidden argument*/List_1_get_Count_m949074065_MethodInfo_var);
		V_2 = L_5;
		goto IL_0087;
	}

IL_0032:
	{
		List_1_t3921398993 * L_6 = V_0;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		Renderer_t257310565 * L_8 = List_1_get_Item_m2524923786(L_6, L_7, /*hidden argument*/List_1_get_Item_m2524923786_MethodInfo_var);
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		GameObject_t1756533147 * L_10 = V_3;
		NullCheck(L_10);
		HighlighterRenderer_t4217245640 * L_11 = GameObject_GetComponent_TisHighlighterRenderer_t4217245640_m4110411296(L_10, /*hidden argument*/GameObject_GetComponent_TisHighlighterRenderer_t4217245640_m4110411296_MethodInfo_var);
		V_4 = L_11;
		HighlighterRenderer_t4217245640 * L_12 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_12, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_005c;
		}
	}
	{
		GameObject_t1756533147 * L_14 = V_3;
		NullCheck(L_14);
		HighlighterRenderer_t4217245640 * L_15 = GameObject_AddComponent_TisHighlighterRenderer_t4217245640_m2107265519(L_14, /*hidden argument*/GameObject_AddComponent_TisHighlighterRenderer_t4217245640_m2107265519_MethodInfo_var);
		V_4 = L_15;
	}

IL_005c:
	{
		HighlighterRenderer_t4217245640 * L_16 = V_4;
		NullCheck(L_16);
		HighlighterRenderer_SetState_m2777729094(L_16, (bool)1, /*hidden argument*/NULL);
		HighlighterRenderer_t4217245640 * L_17 = V_4;
		Material_t193706927 * L_18 = Highlighter_get_opaqueMaterial_m1991552851(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		Shader_t2430389951 * L_19 = Highlighter_get_transparentShader_m3015025486(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		HighlighterRenderer_Initialize_m306395362(L_17, L_18, L_19, /*hidden argument*/NULL);
		List_1_t3586366772 * L_20 = __this->get_highlightableRenderers_15();
		HighlighterRenderer_t4217245640 * L_21 = V_4;
		NullCheck(L_20);
		List_1_Add_m1903505042(L_20, L_21, /*hidden argument*/List_1_Add_m1903505042_MethodInfo_var);
		int32_t L_22 = V_1;
		V_1 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0087:
	{
		int32_t L_23 = V_1;
		int32_t L_24 = V_2;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0032;
		}
	}
	{
		__this->set_renderersDirty_16((bool)0);
	}

IL_0095:
	{
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::GrabRenderers(UnityEngine.Transform,System.Collections.Generic.List`1<UnityEngine.Renderer>)
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern Il2CppClass* Renderer_t257310565_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m2491121942_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m4203309067_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m688539176_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2919783149_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2666983737_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m3982085842_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisHighlighter_t958778585_m2365035773_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisHighlighterBlocker_t896209287_m572960287_MethodInfo_var;
extern const uint32_t Highlighter_GrabRenderers_m3766673824_MetadataUsageId;
extern "C"  void Highlighter_GrabRenderers_m3766673824 (Highlighter_t958778585 * __this, Transform_t3275118058 * ___t0, List_1_t3921398993 * ___renderers1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_GrabRenderers_m3766673824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Transform_t3275118058 * V_7 = NULL;
	Highlighter_t958778585 * V_8 = NULL;
	HighlighterBlocker_t896209287 * V_9 = NULL;
	{
		Transform_t3275118058 * L_0 = ___t0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		List_1_t672924358 * L_2 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_types_2();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m2491121942(L_2, /*hidden argument*/List_1_get_Count_m2491121942_MethodInfo_var);
		V_2 = L_3;
		goto IL_0068;
	}

IL_0019:
	{
		GameObject_t1756533147 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		List_1_t672924358 * L_5 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_types_2();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Type_t * L_7 = List_1_get_Item_m4203309067(L_5, L_6, /*hidden argument*/List_1_get_Item_m4203309067_MethodInfo_var);
		List_1_t3188497603 * L_8 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_sComponents_17();
		NullCheck(L_4);
		GameObject_GetComponents_m239772857(L_4, L_7, L_8, /*hidden argument*/NULL);
		V_3 = 0;
		List_1_t3188497603 * L_9 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_sComponents_17();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m688539176(L_9, /*hidden argument*/List_1_get_Count_m688539176_MethodInfo_var);
		V_4 = L_10;
		goto IL_005c;
	}

IL_0042:
	{
		List_1_t3921398993 * L_11 = ___renderers1;
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		List_1_t3188497603 * L_12 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_sComponents_17();
		int32_t L_13 = V_3;
		NullCheck(L_12);
		Component_t3819376471 * L_14 = List_1_get_Item_m2919783149(L_12, L_13, /*hidden argument*/List_1_get_Item_m2919783149_MethodInfo_var);
		NullCheck(L_11);
		List_1_Add_m2666983737(L_11, ((Renderer_t257310565 *)IsInstClass(L_14, Renderer_t257310565_il2cpp_TypeInfo_var)), /*hidden argument*/List_1_Add_m2666983737_MethodInfo_var);
		int32_t L_15 = V_3;
		V_3 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_16 = V_3;
		int32_t L_17 = V_4;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_18 = V_1;
		V_1 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0068:
	{
		int32_t L_19 = V_1;
		int32_t L_20 = V_2;
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		List_1_t3188497603 * L_21 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_sComponents_17();
		NullCheck(L_21);
		List_1_Clear_m3982085842(L_21, /*hidden argument*/List_1_Clear_m3982085842_MethodInfo_var);
		Transform_t3275118058 * L_22 = ___t0;
		NullCheck(L_22);
		int32_t L_23 = Transform_get_childCount_m881385315(L_22, /*hidden argument*/NULL);
		V_5 = L_23;
		int32_t L_24 = V_5;
		if (L_24)
		{
			goto IL_0089;
		}
	}
	{
		return;
	}

IL_0089:
	{
		V_6 = 0;
		goto IL_00e0;
	}

IL_0091:
	{
		Transform_t3275118058 * L_25 = ___t0;
		int32_t L_26 = V_6;
		NullCheck(L_25);
		Transform_t3275118058 * L_27 = Transform_GetChild_m3838588184(L_25, L_26, /*hidden argument*/NULL);
		V_7 = L_27;
		Transform_t3275118058 * L_28 = V_7;
		NullCheck(L_28);
		Highlighter_t958778585 * L_29 = Component_GetComponent_TisHighlighter_t958778585_m2365035773(L_28, /*hidden argument*/Component_GetComponent_TisHighlighter_t958778585_m2365035773_MethodInfo_var);
		V_8 = L_29;
		Highlighter_t958778585 * L_30 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_31 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_30, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00b6;
		}
	}
	{
		goto IL_00da;
	}

IL_00b6:
	{
		Transform_t3275118058 * L_32 = V_7;
		NullCheck(L_32);
		HighlighterBlocker_t896209287 * L_33 = Component_GetComponent_TisHighlighterBlocker_t896209287_m572960287(L_32, /*hidden argument*/Component_GetComponent_TisHighlighterBlocker_t896209287_m572960287_MethodInfo_var);
		V_9 = L_33;
		HighlighterBlocker_t896209287 * L_34 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_35 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_34, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_00d1;
		}
	}
	{
		goto IL_00da;
	}

IL_00d1:
	{
		Transform_t3275118058 * L_36 = V_7;
		List_1_t3921398993 * L_37 = ___renderers1;
		Highlighter_GrabRenderers_m3766673824(__this, L_36, L_37, /*hidden argument*/NULL);
	}

IL_00da:
	{
		int32_t L_38 = V_6;
		V_6 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00e0:
	{
		int32_t L_39 = V_6;
		int32_t L_40 = V_5;
		if ((((int32_t)L_39) < ((int32_t)L_40)))
		{
			goto IL_0091;
		}
	}
	{
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::UpdateShaderParams(System.Boolean,System.Boolean)
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2912931613_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3417646876_MethodInfo_var;
extern const uint32_t Highlighter_UpdateShaderParams_m3938890225_MetadataUsageId;
extern "C"  void Highlighter_UpdateShaderParams_m3938890225 (Highlighter_t958778585 * __this, bool ___zt0, bool ___sr1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_UpdateShaderParams_m3938890225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		bool L_0 = __this->get_zTest_19();
		bool L_1 = ___zt0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_005e;
		}
	}
	{
		bool L_2 = ___zt0;
		__this->set_zTest_19(L_2);
		bool L_3 = __this->get_zTest_19();
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		int32_t L_4 = Highlighter_GetZTest_m1622142992(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Material_t193706927 * L_5 = Highlighter_get_opaqueMaterial_m1991552851(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_6 = ShaderPropertyID_get__ZTest_m2113714673(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		NullCheck(L_5);
		Material_SetInt_m977568583(L_5, L_6, L_7, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_004d;
	}

IL_0037:
	{
		List_1_t3586366772 * L_8 = __this->get_highlightableRenderers_15();
		int32_t L_9 = V_1;
		NullCheck(L_8);
		HighlighterRenderer_t4217245640 * L_10 = List_1_get_Item_m2912931613(L_8, L_9, /*hidden argument*/List_1_get_Item_m2912931613_MethodInfo_var);
		int32_t L_11 = V_0;
		NullCheck(L_10);
		HighlighterRenderer_SetZTestForTransparent_m3290452558(L_10, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_13 = V_1;
		List_1_t3586366772 * L_14 = __this->get_highlightableRenderers_15();
		NullCheck(L_14);
		int32_t L_15 = List_1_get_Count_m3417646876(L_14, /*hidden argument*/List_1_get_Count_m3417646876_MethodInfo_var);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0037;
		}
	}

IL_005e:
	{
		bool L_16 = __this->get_stencilRef_20();
		bool L_17 = ___sr1;
		if ((((int32_t)L_16) == ((int32_t)L_17)))
		{
			goto IL_00bc;
		}
	}
	{
		bool L_18 = ___sr1;
		__this->set_stencilRef_20(L_18);
		bool L_19 = __this->get_stencilRef_20();
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		int32_t L_20 = Highlighter_GetStencilRef_m1953084609(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		Material_t193706927 * L_21 = Highlighter_get_opaqueMaterial_m1991552851(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_22 = ShaderPropertyID_get__StencilRef_m1989750604(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_23 = V_2;
		NullCheck(L_21);
		Material_SetInt_m977568583(L_21, L_22, L_23, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_00ab;
	}

IL_0095:
	{
		List_1_t3586366772 * L_24 = __this->get_highlightableRenderers_15();
		int32_t L_25 = V_3;
		NullCheck(L_24);
		HighlighterRenderer_t4217245640 * L_26 = List_1_get_Item_m2912931613(L_24, L_25, /*hidden argument*/List_1_get_Item_m2912931613_MethodInfo_var);
		int32_t L_27 = V_2;
		NullCheck(L_26);
		HighlighterRenderer_SetStencilRefForTransparent_m1560370405(L_26, L_27, /*hidden argument*/NULL);
		int32_t L_28 = V_3;
		V_3 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00ab:
	{
		int32_t L_29 = V_3;
		List_1_t3586366772 * L_30 = __this->get_highlightableRenderers_15();
		NullCheck(L_30);
		int32_t L_31 = List_1_get_Count_m3417646876(L_30, /*hidden argument*/List_1_get_Count_m3417646876_MethodInfo_var);
		if ((((int32_t)L_29) < ((int32_t)L_31)))
		{
			goto IL_0095;
		}
	}

IL_00bc:
	{
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::UpdateColors()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2912931613_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3417646876_MethodInfo_var;
extern const uint32_t Highlighter_UpdateColors_m3432148212_MetadataUsageId;
extern "C"  void Highlighter_UpdateColors_m3432148212 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_UpdateColors_m3432148212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = Highlighter_get_once_m104830995(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Color_t2020392075  L_1 = __this->get_onceColor_27();
		__this->set_currentColor_23(L_1);
		goto IL_00bc;
	}

IL_001c:
	{
		bool L_2 = __this->get_flashing_22();
		if (!L_2)
		{
			goto IL_0066;
		}
	}
	{
		Color_t2020392075  L_3 = __this->get_flashingColorMin_29();
		Color_t2020392075  L_4 = __this->get_flashingColorMax_30();
		float L_5 = Time_get_realtimeSinceStartup_m357614587(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = __this->get_flashingFreq_28();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = sinf(((float)((float)((float)((float)L_5*(float)L_6))*(float)(6.28318548f))));
		Color_t2020392075  L_8 = Color_Lerp_m3323752807(NULL /*static, unused*/, L_3, L_4, ((float)((float)((float)((float)(0.5f)*(float)L_7))+(float)(0.5f))), /*hidden argument*/NULL);
		__this->set_currentColor_23(L_8);
		goto IL_00bc;
	}

IL_0066:
	{
		float L_9 = __this->get_transitionValue_24();
		if ((!(((float)L_9) > ((float)(0.0f)))))
		{
			goto IL_009f;
		}
	}
	{
		Color_t2020392075  L_10 = __this->get_constantColor_31();
		__this->set_currentColor_23(L_10);
		Color_t2020392075 * L_11 = __this->get_address_of_currentColor_23();
		Color_t2020392075 * L_12 = L_11;
		float L_13 = L_12->get_a_3();
		float L_14 = __this->get_transitionValue_24();
		L_12->set_a_3(((float)((float)L_13*(float)L_14)));
		goto IL_00bc;
	}

IL_009f:
	{
		bool L_15 = __this->get_occluder_13();
		if (!L_15)
		{
			goto IL_00bb;
		}
	}
	{
		Color_t2020392075  L_16 = __this->get_occluderColor_4();
		__this->set_currentColor_23(L_16);
		goto IL_00bc;
	}

IL_00bb:
	{
		return;
	}

IL_00bc:
	{
		Material_t193706927 * L_17 = Highlighter_get_opaqueMaterial_m1991552851(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_18 = ShaderPropertyID_get__Color_m865056110(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t2020392075  L_19 = __this->get_currentColor_23();
		NullCheck(L_17);
		Material_SetColor_m1191533068(L_17, L_18, L_19, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_00f4;
	}

IL_00d9:
	{
		List_1_t3586366772 * L_20 = __this->get_highlightableRenderers_15();
		int32_t L_21 = V_0;
		NullCheck(L_20);
		HighlighterRenderer_t4217245640 * L_22 = List_1_get_Item_m2912931613(L_20, L_21, /*hidden argument*/List_1_get_Item_m2912931613_MethodInfo_var);
		Color_t2020392075  L_23 = __this->get_currentColor_23();
		NullCheck(L_22);
		HighlighterRenderer_SetColorForTransparent_m4078721542(L_22, L_23, /*hidden argument*/NULL);
		int32_t L_24 = V_0;
		V_0 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00f4:
	{
		int32_t L_25 = V_0;
		List_1_t3586366772 * L_26 = __this->get_highlightableRenderers_15();
		NullCheck(L_26);
		int32_t L_27 = List_1_get_Count_m3417646876(L_26, /*hidden argument*/List_1_get_Count_m3417646876_MethodInfo_var);
		if ((((int32_t)L_25) < ((int32_t)L_27)))
		{
			goto IL_00d9;
		}
	}
	{
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::UpdateTransition()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Highlighter_UpdateTransition_m3266769167_MetadataUsageId;
extern "C"  void Highlighter_UpdateTransition_m3266769167 (Highlighter_t958778585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_UpdateTransition_m3266769167_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float G_B6_0 = 0.0f;
	{
		float L_0 = __this->get_transitionValue_24();
		float L_1 = __this->get_transitionTarget_25();
		if ((((float)L_0) == ((float)L_1)))
		{
			goto IL_0072;
		}
	}
	{
		float L_2 = __this->get_transitionTime_26();
		if ((!(((float)L_2) <= ((float)(0.0f)))))
		{
			goto IL_0032;
		}
	}
	{
		float L_3 = __this->get_transitionTarget_25();
		__this->set_transitionValue_24(L_3);
		goto IL_0072;
	}

IL_0032:
	{
		float L_4 = __this->get_transitionTarget_25();
		if ((!(((float)L_4) > ((float)(0.0f)))))
		{
			goto IL_004c;
		}
	}
	{
		G_B6_0 = (1.0f);
		goto IL_0051;
	}

IL_004c:
	{
		G_B6_0 = (-1.0f);
	}

IL_0051:
	{
		V_0 = G_B6_0;
		float L_5 = __this->get_transitionValue_24();
		float L_6 = V_0;
		float L_7 = Time_get_unscaledDeltaTime_m4281640537(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = __this->get_transitionTime_26();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_9 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, ((float)((float)L_5+(float)((float)((float)((float)((float)L_6*(float)L_7))/(float)L_8)))), /*hidden argument*/NULL);
		__this->set_transitionValue_24(L_9);
	}

IL_0072:
	{
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::FillBufferInternal(UnityEngine.Rendering.CommandBuffer,HighlightingSystem.Highlighter/Mode,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m3417646876_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2912931613_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m3323336196_MethodInfo_var;
extern const uint32_t Highlighter_FillBufferInternal_m3761726880_MetadataUsageId;
extern "C"  void Highlighter_FillBufferInternal_m3761726880 (Highlighter_t958778585 * __this, CommandBuffer_t1204166949 * ___buffer0, int32_t ___m1, bool ___depthAvailable2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_FillBufferInternal_m3761726880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	HighlighterRenderer_t4217245640 * V_3 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B10_0 = 0;
	Highlighter_t958778585 * G_B13_0 = NULL;
	Highlighter_t958778585 * G_B12_0 = NULL;
	int32_t G_B14_0 = 0;
	Highlighter_t958778585 * G_B14_1 = NULL;
	Highlighter_t958778585 * G_B18_0 = NULL;
	Highlighter_t958778585 * G_B17_0 = NULL;
	int32_t G_B19_0 = 0;
	Highlighter_t958778585 * G_B19_1 = NULL;
	{
		Highlighter_UpdateRenderers_m4242998278(__this, /*hidden argument*/NULL);
		bool L_0 = Highlighter_get_once_m104830995(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		bool L_1 = __this->get_flashing_22();
		if (L_1)
		{
			goto IL_002b;
		}
	}
	{
		float L_2 = __this->get_transitionValue_24();
		G_B4_0 = ((((float)L_2) > ((float)(0.0f)))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B4_0 = 1;
	}

IL_002c:
	{
		V_0 = (bool)G_B4_0;
		bool L_3 = __this->get_occluder_13();
		if (!L_3)
		{
			goto IL_004c;
		}
	}
	{
		bool L_4 = __this->get_seeThrough_12();
		if (L_4)
		{
			goto IL_0049;
		}
	}
	{
		bool L_5 = ___depthAvailable2;
		G_B8_0 = ((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
		goto IL_004a;
	}

IL_0049:
	{
		G_B8_0 = 1;
	}

IL_004a:
	{
		G_B10_0 = G_B8_0;
		goto IL_004d;
	}

IL_004c:
	{
		G_B10_0 = 0;
	}

IL_004d:
	{
		V_1 = (bool)G_B10_0;
		__this->set_mode_18(0);
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0078;
		}
	}
	{
		bool L_7 = __this->get_seeThrough_12();
		G_B12_0 = __this;
		if (!L_7)
		{
			G_B13_0 = __this;
			goto IL_006d;
		}
	}
	{
		G_B14_0 = 3;
		G_B14_1 = G_B12_0;
		goto IL_006e;
	}

IL_006d:
	{
		G_B14_0 = 1;
		G_B14_1 = G_B13_0;
	}

IL_006e:
	{
		NullCheck(G_B14_1);
		G_B14_1->set_mode_18(G_B14_0);
		goto IL_0096;
	}

IL_0078:
	{
		bool L_8 = V_1;
		if (!L_8)
		{
			goto IL_0096;
		}
	}
	{
		bool L_9 = __this->get_seeThrough_12();
		G_B17_0 = __this;
		if (!L_9)
		{
			G_B18_0 = __this;
			goto IL_0090;
		}
	}
	{
		G_B19_0 = 4;
		G_B19_1 = G_B17_0;
		goto IL_0091;
	}

IL_0090:
	{
		G_B19_0 = 2;
		G_B19_1 = G_B18_0;
	}

IL_0091:
	{
		NullCheck(G_B19_1);
		G_B19_1->set_mode_18(G_B19_0);
	}

IL_0096:
	{
		int32_t L_10 = __this->get_mode_18();
		if (!L_10)
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_11 = __this->get_mode_18();
		int32_t L_12 = ___m1;
		if ((((int32_t)L_11) == ((int32_t)L_12)))
		{
			goto IL_00ae;
		}
	}

IL_00ad:
	{
		return;
	}

IL_00ae:
	{
		bool L_13 = V_0;
		if (!L_13)
		{
			goto IL_00c6;
		}
	}
	{
		bool L_14 = __this->get_seeThrough_12();
		Highlighter_UpdateShaderParams_m3938890225(__this, L_14, (bool)1, /*hidden argument*/NULL);
		goto IL_00d9;
	}

IL_00c6:
	{
		bool L_15 = V_1;
		if (!L_15)
		{
			goto IL_00d9;
		}
	}
	{
		bool L_16 = __this->get_seeThrough_12();
		Highlighter_UpdateShaderParams_m3938890225(__this, (bool)0, L_16, /*hidden argument*/NULL);
	}

IL_00d9:
	{
		Highlighter_UpdateColors_m3432148212(__this, /*hidden argument*/NULL);
		List_1_t3586366772 * L_17 = __this->get_highlightableRenderers_15();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m3417646876(L_17, /*hidden argument*/List_1_get_Count_m3417646876_MethodInfo_var);
		V_2 = ((int32_t)((int32_t)L_18-(int32_t)1));
		goto IL_013f;
	}

IL_00f2:
	{
		List_1_t3586366772 * L_19 = __this->get_highlightableRenderers_15();
		int32_t L_20 = V_2;
		NullCheck(L_19);
		HighlighterRenderer_t4217245640 * L_21 = List_1_get_Item_m2912931613(L_19, L_20, /*hidden argument*/List_1_get_Item_m2912931613_MethodInfo_var);
		V_3 = L_21;
		HighlighterRenderer_t4217245640 * L_22 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_23 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_22, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_011c;
		}
	}
	{
		List_1_t3586366772 * L_24 = __this->get_highlightableRenderers_15();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		List_1_RemoveAt_m3323336196(L_24, L_25, /*hidden argument*/List_1_RemoveAt_m3323336196_MethodInfo_var);
		goto IL_013b;
	}

IL_011c:
	{
		HighlighterRenderer_t4217245640 * L_26 = V_3;
		CommandBuffer_t1204166949 * L_27 = ___buffer0;
		NullCheck(L_26);
		bool L_28 = HighlighterRenderer_FillBuffer_m392175031(L_26, L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_013b;
		}
	}
	{
		List_1_t3586366772 * L_29 = __this->get_highlightableRenderers_15();
		int32_t L_30 = V_2;
		NullCheck(L_29);
		List_1_RemoveAt_m3323336196(L_29, L_30, /*hidden argument*/List_1_RemoveAt_m3323336196_MethodInfo_var);
		HighlighterRenderer_t4217245640 * L_31 = V_3;
		NullCheck(L_31);
		HighlighterRenderer_SetState_m2777729094(L_31, (bool)0, /*hidden argument*/NULL);
	}

IL_013b:
	{
		int32_t L_32 = V_2;
		V_2 = ((int32_t)((int32_t)L_32-(int32_t)1));
	}

IL_013f:
	{
		int32_t L_33 = V_2;
		if ((((int32_t)L_33) >= ((int32_t)0)))
		{
			goto IL_00f2;
		}
	}
	{
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::FillBuffer(UnityEngine.Rendering.CommandBuffer,System.Boolean)
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1_GetEnumerator_m274437764_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2186121612_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4214120950_MethodInfo_var;
extern const uint32_t Highlighter_FillBuffer_m3838603729_MetadataUsageId;
extern "C"  void Highlighter_FillBuffer_m3838603729 (Il2CppObject * __this /* static, unused */, CommandBuffer_t1204166949 * ___buffer0, bool ___depthAvailable1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_FillBuffer_m3838603729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Enumerator_t2075522577  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Highlighter_t958778585 * V_3 = NULL;
	{
		V_0 = 0;
		goto IL_0040;
	}

IL_0007:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		ModeU5BU5D_t1282320767* L_0 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_renderingOrder_7();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		HashSet_1_t3587206735 * L_4 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_highlighters_8();
		NullCheck(L_4);
		Enumerator_t2075522577  L_5 = HashSet_1_GetEnumerator_m274437764(L_4, /*hidden argument*/HashSet_1_GetEnumerator_m274437764_MethodInfo_var);
		V_2 = L_5;
		goto IL_0030;
	}

IL_001f:
	{
		Highlighter_t958778585 * L_6 = Enumerator_get_Current_m2186121612((&V_2), /*hidden argument*/Enumerator_get_Current_m2186121612_MethodInfo_var);
		V_3 = L_6;
		Highlighter_t958778585 * L_7 = V_3;
		CommandBuffer_t1204166949 * L_8 = ___buffer0;
		int32_t L_9 = V_1;
		bool L_10 = ___depthAvailable1;
		NullCheck(L_7);
		Highlighter_FillBufferInternal_m3761726880(L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
	}

IL_0030:
	{
		bool L_11 = Enumerator_MoveNext_m4214120950((&V_2), /*hidden argument*/Enumerator_MoveNext_m4214120950_MethodInfo_var);
		if (L_11)
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		ModeU5BU5D_t1282320767* L_14 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_renderingOrder_7();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Int32 HighlightingSystem.Highlighter::GetZTest(System.Boolean)
extern "C"  int32_t Highlighter_GetZTest_m1622142992 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = ___enabled0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		G_B3_0 = 8;
		goto IL_000d;
	}

IL_000c:
	{
		G_B3_0 = 4;
	}

IL_000d:
	{
		return G_B3_0;
	}
}
// System.Int32 HighlightingSystem.Highlighter::GetStencilRef(System.Boolean)
extern "C"  int32_t Highlighter_GetStencilRef_m1953084609 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = ___enabled0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_000d;
	}

IL_000c:
	{
		G_B3_0 = 0;
	}

IL_000d:
	{
		return G_B3_0;
	}
}
// System.Void HighlightingSystem.Highlighter::SetZWrite(System.Int32)
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t Highlighter_SetZWrite_m2014013653_MetadataUsageId;
extern "C"  void Highlighter_SetZWrite_m2014013653 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_SetZWrite_m2014013653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		int32_t L_0 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_zWrite_9();
		int32_t L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		int32_t L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->set_zWrite_9(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_3 = ShaderPropertyID_get__HighlightingZWrite_m3382839208(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_zWrite_9();
		Shader_SetGlobalInt_m4173630976(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::SetOffsetFactor(System.Single)
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t Highlighter_SetOffsetFactor_m1804810322_MetadataUsageId;
extern "C"  void Highlighter_SetOffsetFactor_m1804810322 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_SetOffsetFactor_m1804810322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		float L_0 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_offsetFactor_10();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		float L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->set_offsetFactor_10(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_3 = ShaderPropertyID_get__HighlightingOffsetFactor_m808876897(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_offsetFactor_10();
		Shader_SetGlobalFloat_m3515256261(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::SetOffsetUnits(System.Single)
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t Highlighter_SetOffsetUnits_m623061432_MetadataUsageId;
extern "C"  void Highlighter_SetOffsetUnits_m623061432 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter_SetOffsetUnits_m623061432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		float L_0 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_offsetUnits_11();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		float L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->set_offsetUnits_11(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_3 = ShaderPropertyID_get__HighlightingOffsetUnits_m2985827065(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = ((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->get_offsetUnits_11();
		Shader_SetGlobalFloat_m3515256261(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightingSystem.Highlighter::.cctor()
extern const Il2CppType* MeshRenderer_t1268241104_0_0_0_var;
extern const Il2CppType* SkinnedMeshRenderer_t4220419316_0_0_0_var;
extern const Il2CppType* SpriteRenderer_t1209076198_0_0_0_var;
extern const Il2CppType* ParticleRenderer_t1780124863_0_0_0_var;
extern const Il2CppType* ParticleSystemRenderer_t892265570_0_0_0_var;
extern Il2CppClass* List_1_t672924358_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern Il2CppClass* ModeU5BU5D_t1282320767_il2cpp_TypeInfo_var;
extern Il2CppClass* HashSet_1_t3587206735_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3188497603_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4047179467_MethodInfo_var;
extern const MethodInfo* List_1_Add_m176071399_MethodInfo_var;
extern const MethodInfo* HashSet_1__ctor_m777319201_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1533259582_MethodInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305143____U24fieldU2D1456763F890A84558F99AFA687C36B9037697848_0_FieldInfo_var;
extern const uint32_t Highlighter__cctor_m3873170132_MetadataUsageId;
extern "C"  void Highlighter__cctor_m3873170132 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlighter__cctor_m3873170132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t672924358 * V_0 = NULL;
	{
		List_1_t672924358 * L_0 = (List_1_t672924358 *)il2cpp_codegen_object_new(List_1_t672924358_il2cpp_TypeInfo_var);
		List_1__ctor_m4047179467(L_0, /*hidden argument*/List_1__ctor_m4047179467_MethodInfo_var);
		V_0 = L_0;
		List_1_t672924358 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(MeshRenderer_t1268241104_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_1);
		List_1_Add_m176071399(L_1, L_2, /*hidden argument*/List_1_Add_m176071399_MethodInfo_var);
		List_1_t672924358 * L_3 = V_0;
		Type_t * L_4 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(SkinnedMeshRenderer_t4220419316_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_3);
		List_1_Add_m176071399(L_3, L_4, /*hidden argument*/List_1_Add_m176071399_MethodInfo_var);
		List_1_t672924358 * L_5 = V_0;
		Type_t * L_6 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(SpriteRenderer_t1209076198_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_5);
		List_1_Add_m176071399(L_5, L_6, /*hidden argument*/List_1_Add_m176071399_MethodInfo_var);
		List_1_t672924358 * L_7 = V_0;
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ParticleRenderer_t1780124863_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_7);
		List_1_Add_m176071399(L_7, L_8, /*hidden argument*/List_1_Add_m176071399_MethodInfo_var);
		List_1_t672924358 * L_9 = V_0;
		Type_t * L_10 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ParticleSystemRenderer_t892265570_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_9);
		List_1_Add_m176071399(L_9, L_10, /*hidden argument*/List_1_Add_m176071399_MethodInfo_var);
		List_1_t672924358 * L_11 = V_0;
		((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->set_types_2(L_11);
		ModeU5BU5D_t1282320767* L_12 = ((ModeU5BU5D_t1282320767*)SZArrayNew(ModeU5BU5D_t1282320767_il2cpp_TypeInfo_var, (uint32_t)4));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_12, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305143____U24fieldU2D1456763F890A84558F99AFA687C36B9037697848_0_FieldInfo_var), /*hidden argument*/NULL);
		((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->set_renderingOrder_7(L_12);
		HashSet_1_t3587206735 * L_13 = (HashSet_1_t3587206735 *)il2cpp_codegen_object_new(HashSet_1_t3587206735_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m777319201(L_13, /*hidden argument*/HashSet_1__ctor_m777319201_MethodInfo_var);
		((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->set_highlighters_8(L_13);
		((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->set_zWrite_9((-1));
		((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->set_offsetFactor_10((std::numeric_limits<float>::quiet_NaN()));
		((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->set_offsetUnits_11((std::numeric_limits<float>::quiet_NaN()));
		List_1_t3188497603 * L_14 = (List_1_t3188497603 *)il2cpp_codegen_object_new(List_1_t3188497603_il2cpp_TypeInfo_var);
		List_1__ctor_m1533259582(L_14, 4, /*hidden argument*/List_1__ctor_m1533259582_MethodInfo_var);
		((Highlighter_t958778585_StaticFields*)Highlighter_t958778585_il2cpp_TypeInfo_var->static_fields)->set_sComponents_17(L_14);
		return;
	}
}
// System.Void HighlightingSystem.HighlighterBlocker::.ctor()
extern "C"  void HighlighterBlocker__ctor_m526022523 (HighlighterBlocker_t896209287 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightingSystem.HighlighterRenderer::.ctor()
extern "C"  void HighlighterRenderer__ctor_m3750554968 (HighlighterRenderer_t4217245640 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightingSystem.HighlighterRenderer::OnEnable()
extern "C"  void HighlighterRenderer_OnEnable_m1144232048 (HighlighterRenderer_t4217245640 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = HighlighterRenderer_EndOfFrame_m3876382929(__this, /*hidden argument*/NULL);
		Coroutine_t2299508840 * L_1 = MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		__this->set_endOfFrame_14(L_1);
		return;
	}
}
// System.Void HighlightingSystem.HighlighterRenderer::OnDisable()
extern "C"  void HighlighterRenderer_OnDisable_m1468183261 (HighlighterRenderer_t4217245640 * __this, const MethodInfo* method)
{
	{
		__this->set_lastCamera_12((Camera_t189460977 *)NULL);
		Coroutine_t2299508840 * L_0 = __this->get_endOfFrame_14();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Coroutine_t2299508840 * L_1 = __this->get_endOfFrame_14();
		MonoBehaviour_StopCoroutine_m1668572632(__this, L_1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Void HighlightingSystem.HighlighterRenderer::OnWillRenderObject()
extern Il2CppClass* HighlightingBase_t336099813_il2cpp_TypeInfo_var;
extern const uint32_t HighlighterRenderer_OnWillRenderObject_m1534154096_MetadataUsageId;
extern "C"  void HighlighterRenderer_OnWillRenderObject_m1534154096 (HighlighterRenderer_t4217245640 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlighterRenderer_OnWillRenderObject_m1534154096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Camera_t189460977 * V_0 = NULL;
	{
		Camera_t189460977 * L_0 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Camera_t189460977 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		bool L_2 = HighlightingBase_IsHighlightingCamera_m2166075236(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		Camera_t189460977 * L_3 = V_0;
		__this->set_lastCamera_12(L_3);
	}

IL_0018:
	{
		return;
	}
}
// System.Collections.IEnumerator HighlightingSystem.HighlighterRenderer::EndOfFrame()
extern Il2CppClass* U3CEndOfFrameU3Ec__Iterator0_t1938462382_il2cpp_TypeInfo_var;
extern const uint32_t HighlighterRenderer_EndOfFrame_m3876382929_MetadataUsageId;
extern "C"  Il2CppObject * HighlighterRenderer_EndOfFrame_m3876382929 (HighlighterRenderer_t4217245640 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlighterRenderer_EndOfFrame_m3876382929_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CEndOfFrameU3Ec__Iterator0_t1938462382 * V_0 = NULL;
	{
		U3CEndOfFrameU3Ec__Iterator0_t1938462382 * L_0 = (U3CEndOfFrameU3Ec__Iterator0_t1938462382 *)il2cpp_codegen_object_new(U3CEndOfFrameU3Ec__Iterator0_t1938462382_il2cpp_TypeInfo_var);
		U3CEndOfFrameU3Ec__Iterator0__ctor_m1881942031(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEndOfFrameU3Ec__Iterator0_t1938462382 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CEndOfFrameU3Ec__Iterator0_t1938462382 * L_2 = V_0;
		return L_2;
	}
}
// System.Void HighlightingSystem.HighlighterRenderer::Initialize(UnityEngine.Material,UnityEngine.Shader)
extern Il2CppClass* List_1_t3087365684_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Data_t3718244552_il2cpp_TypeInfo_var;
extern Il2CppClass* HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t193706927_il2cpp_TypeInfo_var;
extern Il2CppClass* SpriteRenderer_t1209076198_il2cpp_TypeInfo_var;
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3100466721_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4144517861_MethodInfo_var;
extern const uint32_t HighlighterRenderer_Initialize_m306395362_MetadataUsageId;
extern "C"  void HighlighterRenderer_Initialize_m306395362 (HighlighterRenderer_t4217245640 * __this, Material_t193706927 * ___sharedOpaqueMaterial0, Shader_t2430389951 * ___transparentShader1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlighterRenderer_Initialize_m306395362_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MaterialU5BU5D_t3123989686* V_0 = NULL;
	int32_t V_1 = 0;
	Material_t193706927 * V_2 = NULL;
	Data_t3718244552  V_3;
	memset(&V_3, 0, sizeof(V_3));
	String_t* V_4 = NULL;
	Material_t193706927 * V_5 = NULL;
	int32_t V_6 = 0;
	int32_t G_B12_0 = 0;
	Material_t193706927 * G_B12_1 = NULL;
	int32_t G_B11_0 = 0;
	Material_t193706927 * G_B11_1 = NULL;
	float G_B13_0 = 0.0f;
	int32_t G_B13_1 = 0;
	Material_t193706927 * G_B13_2 = NULL;
	{
		List_1_t3087365684 * L_0 = (List_1_t3087365684 *)il2cpp_codegen_object_new(List_1_t3087365684_il2cpp_TypeInfo_var);
		List_1__ctor_m3100466721(L_0, /*hidden argument*/List_1__ctor_m3100466721_MethodInfo_var);
		__this->set_data_11(L_0);
		Renderer_t257310565 * L_1 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var);
		__this->set_r_10(L_1);
		Object_set_hideFlags_m2204253440(__this, ((int32_t)30), /*hidden argument*/NULL);
		Renderer_t257310565 * L_2 = __this->get_r_10();
		NullCheck(L_2);
		MaterialU5BU5D_t3123989686* L_3 = Renderer_get_sharedMaterials_m4026934221(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		MaterialU5BU5D_t3123989686* L_4 = V_0;
		if (!L_4)
		{
			goto IL_016b;
		}
	}
	{
		V_1 = 0;
		goto IL_0162;
	}

IL_0038:
	{
		MaterialU5BU5D_t3123989686* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Material_t193706927 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_2 = L_8;
		Material_t193706927 * L_9 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_004d;
		}
	}
	{
		goto IL_015e;
	}

IL_004d:
	{
		Initobj (Data_t3718244552_il2cpp_TypeInfo_var, (&V_3));
		Material_t193706927 * L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var);
		String_t* L_12 = ((HighlighterRenderer_t4217245640_StaticFields*)HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var->static_fields)->get_sRenderType_5();
		String_t* L_13 = ((HighlighterRenderer_t4217245640_StaticFields*)HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var->static_fields)->get_sOpaque_6();
		NullCheck(L_11);
		String_t* L_14 = Material_GetTag_m3319424960(L_11, L_12, (bool)1, L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		String_t* L_15 = V_4;
		String_t* L_16 = ((HighlighterRenderer_t4217245640_StaticFields*)HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var->static_fields)->get_sTransparent_7();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_008a;
		}
	}
	{
		String_t* L_18 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var);
		String_t* L_19 = ((HighlighterRenderer_t4217245640_StaticFields*)HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var->static_fields)->get_sTransparentCutout_8();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_013a;
		}
	}

IL_008a:
	{
		Shader_t2430389951 * L_21 = ___transparentShader1;
		Material_t193706927 * L_22 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1897560860(L_22, L_21, /*hidden argument*/NULL);
		V_5 = L_22;
		Renderer_t257310565 * L_23 = __this->get_r_10();
		if (!((SpriteRenderer_t1209076198 *)IsInstSealed(L_23, SpriteRenderer_t1209076198_il2cpp_TypeInfo_var)))
		{
			goto IL_00af;
		}
	}
	{
		Material_t193706927 * L_24 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_25 = ShaderPropertyID_get__Cull_m2367453659(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		Material_SetInt_m977568583(L_24, L_25, 0, /*hidden argument*/NULL);
	}

IL_00af:
	{
		Material_t193706927 * L_26 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_27 = ShaderPropertyID_get__MainTex_m1457479119(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		bool L_28 = Material_HasProperty_m3175512802(L_26, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00f5;
		}
	}
	{
		Material_t193706927 * L_29 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_30 = ShaderPropertyID_get__MainTex_m1457479119(NULL /*static, unused*/, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = V_2;
		NullCheck(L_31);
		Texture_t2243626319 * L_32 = Material_get_mainTexture_m432794412(L_31, /*hidden argument*/NULL);
		NullCheck(L_29);
		Material_SetTexture_m58378708(L_29, L_30, L_32, /*hidden argument*/NULL);
		Material_t193706927 * L_33 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var);
		String_t* L_34 = ((HighlighterRenderer_t4217245640_StaticFields*)HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var->static_fields)->get_sMainTex_9();
		Material_t193706927 * L_35 = V_2;
		NullCheck(L_35);
		Vector2_t2243707579  L_36 = Material_get_mainTextureOffset_m786294629(L_35, /*hidden argument*/NULL);
		NullCheck(L_33);
		Material_SetTextureOffset_m3084369360(L_33, L_34, L_36, /*hidden argument*/NULL);
		Material_t193706927 * L_37 = V_5;
		String_t* L_38 = ((HighlighterRenderer_t4217245640_StaticFields*)HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var->static_fields)->get_sMainTex_9();
		Material_t193706927 * L_39 = V_2;
		NullCheck(L_39);
		Vector2_t2243707579  L_40 = Material_get_mainTextureScale_m2123078452(L_39, /*hidden argument*/NULL);
		NullCheck(L_37);
		Material_SetTextureScale_m1622979841(L_37, L_38, L_40, /*hidden argument*/NULL);
	}

IL_00f5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_41 = ShaderPropertyID_get__Cutoff_m2480496912(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_41;
		Material_t193706927 * L_42 = V_5;
		int32_t L_43 = V_6;
		Material_t193706927 * L_44 = V_2;
		int32_t L_45 = V_6;
		NullCheck(L_44);
		bool L_46 = Material_HasProperty_m3175512802(L_44, L_45, /*hidden argument*/NULL);
		G_B11_0 = L_43;
		G_B11_1 = L_42;
		if (!L_46)
		{
			G_B12_0 = L_43;
			G_B12_1 = L_42;
			goto IL_011a;
		}
	}
	{
		Material_t193706927 * L_47 = V_2;
		int32_t L_48 = V_6;
		NullCheck(L_47);
		float L_49 = Material_GetFloat_m4250722315(L_47, L_48, /*hidden argument*/NULL);
		G_B13_0 = L_49;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_011f;
	}

IL_011a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var);
		float L_50 = ((HighlighterRenderer_t4217245640_StaticFields*)HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var->static_fields)->get_transparentCutoff_2();
		G_B13_0 = L_50;
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_011f:
	{
		NullCheck(G_B13_2);
		Material_SetFloat_m953675160(G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		Material_t193706927 * L_51 = V_5;
		(&V_3)->set_material_0(L_51);
		(&V_3)->set_transparent_2((bool)1);
		goto IL_014a;
	}

IL_013a:
	{
		Material_t193706927 * L_52 = ___sharedOpaqueMaterial0;
		(&V_3)->set_material_0(L_52);
		(&V_3)->set_transparent_2((bool)0);
	}

IL_014a:
	{
		int32_t L_53 = V_1;
		(&V_3)->set_submeshIndex_1(L_53);
		List_1_t3087365684 * L_54 = __this->get_data_11();
		Data_t3718244552  L_55 = V_3;
		NullCheck(L_54);
		List_1_Add_m4144517861(L_54, L_55, /*hidden argument*/List_1_Add_m4144517861_MethodInfo_var);
	}

IL_015e:
	{
		int32_t L_56 = V_1;
		V_1 = ((int32_t)((int32_t)L_56+(int32_t)1));
	}

IL_0162:
	{
		int32_t L_57 = V_1;
		MaterialU5BU5D_t3123989686* L_58 = V_0;
		NullCheck(L_58);
		if ((((int32_t)L_57) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_58)->max_length)))))))
		{
			goto IL_0038;
		}
	}

IL_016b:
	{
		return;
	}
}
// System.Boolean HighlightingSystem.HighlighterRenderer::FillBuffer(UnityEngine.Rendering.CommandBuffer)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m343165665_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1623153598_MethodInfo_var;
extern const uint32_t HighlighterRenderer_FillBuffer_m392175031_MetadataUsageId;
extern "C"  bool HighlighterRenderer_FillBuffer_m392175031 (HighlighterRenderer_t4217245640 * __this, CommandBuffer_t1204166949 * ___buffer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlighterRenderer_FillBuffer_m392175031_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Data_t3718244552  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Renderer_t257310565 * L_0 = __this->get_r_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return (bool)0;
	}

IL_0013:
	{
		Camera_t189460977 * L_2 = __this->get_lastCamera_12();
		Camera_t189460977 * L_3 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		V_0 = 0;
		List_1_t3087365684 * L_5 = __this->get_data_11();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m343165665(L_5, /*hidden argument*/List_1_get_Count_m343165665_MethodInfo_var);
		V_1 = L_6;
		goto IL_0066;
	}

IL_003b:
	{
		List_1_t3087365684 * L_7 = __this->get_data_11();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		Data_t3718244552  L_9 = List_1_get_Item_m1623153598(L_7, L_8, /*hidden argument*/List_1_get_Item_m1623153598_MethodInfo_var);
		V_2 = L_9;
		CommandBuffer_t1204166949 * L_10 = ___buffer0;
		Renderer_t257310565 * L_11 = __this->get_r_10();
		Material_t193706927 * L_12 = (&V_2)->get_material_0();
		int32_t L_13 = (&V_2)->get_submeshIndex_1();
		NullCheck(L_10);
		CommandBuffer_DrawRenderer_m878488214(L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = V_1;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_003b;
		}
	}

IL_006d:
	{
		return (bool)1;
	}
}
// System.Void HighlightingSystem.HighlighterRenderer::SetColorForTransparent(UnityEngine.Color)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m343165665_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1623153598_MethodInfo_var;
extern const uint32_t HighlighterRenderer_SetColorForTransparent_m4078721542_MetadataUsageId;
extern "C"  void HighlighterRenderer_SetColorForTransparent_m4078721542 (HighlighterRenderer_t4217245640 * __this, Color_t2020392075  ___clr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlighterRenderer_SetColorForTransparent_m4078721542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Data_t3718244552  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		V_0 = 0;
		List_1_t3087365684 * L_0 = __this->get_data_11();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m343165665(L_0, /*hidden argument*/List_1_get_Count_m343165665_MethodInfo_var);
		V_1 = L_1;
		goto IL_0042;
	}

IL_0013:
	{
		List_1_t3087365684 * L_2 = __this->get_data_11();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Data_t3718244552  L_4 = List_1_get_Item_m1623153598(L_2, L_3, /*hidden argument*/List_1_get_Item_m1623153598_MethodInfo_var);
		V_2 = L_4;
		bool L_5 = (&V_2)->get_transparent_2();
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		Material_t193706927 * L_6 = (&V_2)->get_material_0();
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_7 = ShaderPropertyID_get__Color_m865056110(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t2020392075  L_8 = ___clr0;
		NullCheck(L_6);
		Material_SetColor_m1191533068(L_6, L_7, L_8, /*hidden argument*/NULL);
	}

IL_003e:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void HighlightingSystem.HighlighterRenderer::SetZTestForTransparent(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m343165665_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1623153598_MethodInfo_var;
extern const uint32_t HighlighterRenderer_SetZTestForTransparent_m3290452558_MetadataUsageId;
extern "C"  void HighlighterRenderer_SetZTestForTransparent_m3290452558 (HighlighterRenderer_t4217245640 * __this, int32_t ___zTest0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlighterRenderer_SetZTestForTransparent_m3290452558_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Data_t3718244552  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		V_0 = 0;
		List_1_t3087365684 * L_0 = __this->get_data_11();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m343165665(L_0, /*hidden argument*/List_1_get_Count_m343165665_MethodInfo_var);
		V_1 = L_1;
		goto IL_0042;
	}

IL_0013:
	{
		List_1_t3087365684 * L_2 = __this->get_data_11();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Data_t3718244552  L_4 = List_1_get_Item_m1623153598(L_2, L_3, /*hidden argument*/List_1_get_Item_m1623153598_MethodInfo_var);
		V_2 = L_4;
		bool L_5 = (&V_2)->get_transparent_2();
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		Material_t193706927 * L_6 = (&V_2)->get_material_0();
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_7 = ShaderPropertyID_get__ZTest_m2113714673(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_8 = ___zTest0;
		NullCheck(L_6);
		Material_SetInt_m977568583(L_6, L_7, L_8, /*hidden argument*/NULL);
	}

IL_003e:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void HighlightingSystem.HighlighterRenderer::SetStencilRefForTransparent(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m343165665_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1623153598_MethodInfo_var;
extern const uint32_t HighlighterRenderer_SetStencilRefForTransparent_m1560370405_MetadataUsageId;
extern "C"  void HighlighterRenderer_SetStencilRefForTransparent_m1560370405 (HighlighterRenderer_t4217245640 * __this, int32_t ___stencilRef0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlighterRenderer_SetStencilRefForTransparent_m1560370405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Data_t3718244552  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		V_0 = 0;
		List_1_t3087365684 * L_0 = __this->get_data_11();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m343165665(L_0, /*hidden argument*/List_1_get_Count_m343165665_MethodInfo_var);
		V_1 = L_1;
		goto IL_0042;
	}

IL_0013:
	{
		List_1_t3087365684 * L_2 = __this->get_data_11();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Data_t3718244552  L_4 = List_1_get_Item_m1623153598(L_2, L_3, /*hidden argument*/List_1_get_Item_m1623153598_MethodInfo_var);
		V_2 = L_4;
		bool L_5 = (&V_2)->get_transparent_2();
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		Material_t193706927 * L_6 = (&V_2)->get_material_0();
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_7 = ShaderPropertyID_get__StencilRef_m1989750604(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_8 = ___stencilRef0;
		NullCheck(L_6);
		Material_SetInt_m977568583(L_6, L_7, L_8, /*hidden argument*/NULL);
	}

IL_003e:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void HighlightingSystem.HighlighterRenderer::SetState(System.Boolean)
extern "C"  void HighlighterRenderer_SetState_m2777729094 (HighlighterRenderer_t4217245640 * __this, bool ___alive0, const MethodInfo* method)
{
	{
		bool L_0 = ___alive0;
		__this->set_isAlive_13(L_0);
		return;
	}
}
// System.Void HighlightingSystem.HighlighterRenderer::.cctor()
extern Il2CppClass* HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1787917738;
extern Il2CppCodeGenString* _stringLiteral2895745187;
extern Il2CppCodeGenString* _stringLiteral1807943676;
extern Il2CppCodeGenString* _stringLiteral1440447248;
extern Il2CppCodeGenString* _stringLiteral4026354833;
extern const uint32_t HighlighterRenderer__cctor_m3550548299_MetadataUsageId;
extern "C"  void HighlighterRenderer__cctor_m3550548299 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlighterRenderer__cctor_m3550548299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((HighlighterRenderer_t4217245640_StaticFields*)HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var->static_fields)->set_transparentCutoff_2((0.5f));
		((HighlighterRenderer_t4217245640_StaticFields*)HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var->static_fields)->set_sRenderType_5(_stringLiteral1787917738);
		((HighlighterRenderer_t4217245640_StaticFields*)HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var->static_fields)->set_sOpaque_6(_stringLiteral2895745187);
		((HighlighterRenderer_t4217245640_StaticFields*)HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var->static_fields)->set_sTransparent_7(_stringLiteral1807943676);
		((HighlighterRenderer_t4217245640_StaticFields*)HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var->static_fields)->set_sTransparentCutout_8(_stringLiteral1440447248);
		((HighlighterRenderer_t4217245640_StaticFields*)HighlighterRenderer_t4217245640_il2cpp_TypeInfo_var->static_fields)->set_sMainTex_9(_stringLiteral4026354833);
		return;
	}
}
// System.Void HighlightingSystem.HighlighterRenderer/<EndOfFrame>c__Iterator0::.ctor()
extern "C"  void U3CEndOfFrameU3Ec__Iterator0__ctor_m1881942031 (U3CEndOfFrameU3Ec__Iterator0_t1938462382 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean HighlightingSystem.HighlighterRenderer/<EndOfFrame>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t U3CEndOfFrameU3Ec__Iterator0_MoveNext_m3823359153_MetadataUsageId;
extern "C"  bool U3CEndOfFrameU3Ec__Iterator0_MoveNext_m3823359153 (U3CEndOfFrameU3Ec__Iterator0_t1938462382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEndOfFrameU3Ec__Iterator0_MoveNext_m3823359153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0040;
		}
	}
	{
		goto IL_0073;
	}

IL_0021:
	{
		WaitForEndOfFrame_t1785723201 * L_2 = (WaitForEndOfFrame_t1785723201 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m3062480170(L_2, /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_003b;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_003b:
	{
		goto IL_0075;
	}

IL_0040:
	{
		HighlighterRenderer_t4217245640 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		L_4->set_lastCamera_12((Camera_t189460977 *)NULL);
		HighlighterRenderer_t4217245640 * L_5 = __this->get_U24this_0();
		NullCheck(L_5);
		bool L_6 = L_5->get_isAlive_13();
		if (L_6)
		{
			goto IL_0067;
		}
	}
	{
		HighlighterRenderer_t4217245640 * L_7 = __this->get_U24this_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0067:
	{
		goto IL_0021;
	}
	// Dead block : IL_006c: ldarg.0

IL_0073:
	{
		return (bool)0;
	}

IL_0075:
	{
		return (bool)1;
	}
}
// System.Object HighlightingSystem.HighlighterRenderer/<EndOfFrame>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEndOfFrameU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1319366005 (U3CEndOfFrameU3Ec__Iterator0_t1938462382 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object HighlightingSystem.HighlighterRenderer/<EndOfFrame>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEndOfFrameU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m374171213 (U3CEndOfFrameU3Ec__Iterator0_t1938462382 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void HighlightingSystem.HighlighterRenderer/<EndOfFrame>c__Iterator0::Dispose()
extern "C"  void U3CEndOfFrameU3Ec__Iterator0_Dispose_m2796909686 (U3CEndOfFrameU3Ec__Iterator0_t1938462382 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void HighlightingSystem.HighlighterRenderer/<EndOfFrame>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CEndOfFrameU3Ec__Iterator0_Reset_m4159133188_MetadataUsageId;
extern "C"  void U3CEndOfFrameU3Ec__Iterator0_Reset_m4159133188 (U3CEndOfFrameU3Ec__Iterator0_t1938462382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEndOfFrameU3Ec__Iterator0_Reset_m4159133188_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// Conversion methods for marshalling of: HighlightingSystem.HighlighterRenderer/Data
extern "C" void Data_t3718244552_marshal_pinvoke(const Data_t3718244552& unmarshaled, Data_t3718244552_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___material_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'material' of type 'Data': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___material_0Exception);
}
extern "C" void Data_t3718244552_marshal_pinvoke_back(const Data_t3718244552_marshaled_pinvoke& marshaled, Data_t3718244552& unmarshaled)
{
	Il2CppCodeGenException* ___material_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'material' of type 'Data': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___material_0Exception);
}
// Conversion method for clean up from marshalling of: HighlightingSystem.HighlighterRenderer/Data
extern "C" void Data_t3718244552_marshal_pinvoke_cleanup(Data_t3718244552_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: HighlightingSystem.HighlighterRenderer/Data
extern "C" void Data_t3718244552_marshal_com(const Data_t3718244552& unmarshaled, Data_t3718244552_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___material_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'material' of type 'Data': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___material_0Exception);
}
extern "C" void Data_t3718244552_marshal_com_back(const Data_t3718244552_marshaled_com& marshaled, Data_t3718244552& unmarshaled)
{
	Il2CppCodeGenException* ___material_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'material' of type 'Data': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___material_0Exception);
}
// Conversion method for clean up from marshalling of: HighlightingSystem.HighlighterRenderer/Data
extern "C" void Data_t3718244552_marshal_com_cleanup(Data_t3718244552_marshaled_com& marshaled)
{
}
// System.Void HighlightingSystem.HighlightingBase::.ctor()
extern "C"  void HighlightingBase__ctor_m1751304723 (HighlightingBase_t336099813 * __this, const MethodInfo* method)
{
	{
		__this->set_cachedWidth_12((-1));
		__this->set_cachedHeight_13((-1));
		__this->set_cachedAA_14((-1));
		__this->set__downsampleFactor_15(4);
		__this->set__iterations_16(2);
		__this->set__blurMinSpread_17((0.65f));
		__this->set__blurSpread_18((0.25f));
		__this->set__blurIntensity_19((0.3f));
		__this->set_isDepthAvailable_24((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean HighlightingSystem.HighlightingBase::get_uvStartsAtTop()
extern Il2CppClass* HighlightingBase_t336099813_il2cpp_TypeInfo_var;
extern const uint32_t HighlightingBase_get_uvStartsAtTop_m1786397426_MetadataUsageId;
extern "C"  bool HighlightingBase_get_uvStartsAtTop_m1786397426 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBase_get_uvStartsAtTop_m1786397426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B8_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		int32_t L_0 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_device_8();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		int32_t L_1 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_device_8();
		if ((((int32_t)L_1) == ((int32_t)6)))
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		int32_t L_2 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_device_8();
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		int32_t L_3 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_device_8();
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		int32_t L_4 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_device_8();
		if ((((int32_t)L_4) == ((int32_t)((int32_t)12))))
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		int32_t L_5 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_device_8();
		if ((((int32_t)L_5) == ((int32_t)((int32_t)13))))
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		int32_t L_6 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_device_8();
		G_B8_0 = ((((int32_t)L_6) == ((int32_t)((int32_t)16)))? 1 : 0);
		goto IL_0050;
	}

IL_004f:
	{
		G_B8_0 = 1;
	}

IL_0050:
	{
		return (bool)G_B8_0;
	}
}
// System.Boolean HighlightingSystem.HighlightingBase::get_isSupported()
extern "C"  bool HighlightingBase_get_isSupported_m1060771174 (HighlightingBase_t336099813 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker1< bool, bool >::Invoke(9 /* System.Boolean HighlightingSystem.HighlightingBase::CheckSupported(System.Boolean) */, __this, (bool)0);
		return L_0;
	}
}
// System.Int32 HighlightingSystem.HighlightingBase::get_downsampleFactor()
extern "C"  int32_t HighlightingBase_get_downsampleFactor_m3447051165 (HighlightingBase_t336099813 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__downsampleFactor_15();
		return L_0;
	}
}
// System.Void HighlightingSystem.HighlightingBase::set_downsampleFactor(System.Int32)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral960267877;
extern const uint32_t HighlightingBase_set_downsampleFactor_m4056539428_MetadataUsageId;
extern "C"  void HighlightingBase_set_downsampleFactor_m4056539428 (HighlightingBase_t336099813 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBase_set_downsampleFactor_m4056539428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get__downsampleFactor_15();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_2 = ___value0;
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_3 = ___value0;
		int32_t L_4 = ___value0;
		if (((int32_t)((int32_t)L_3&(int32_t)((int32_t)((int32_t)L_4-(int32_t)1)))))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_5 = ___value0;
		__this->set__downsampleFactor_15(L_5);
		goto IL_0032;
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral960267877, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Int32 HighlightingSystem.HighlightingBase::get_iterations()
extern "C"  int32_t HighlightingBase_get_iterations_m553206698 (HighlightingBase_t336099813 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__iterations_16();
		return L_0;
	}
}
// System.Void HighlightingSystem.HighlightingBase::set_iterations(System.Int32)
extern "C"  void HighlightingBase_set_iterations_m1116507547 (HighlightingBase_t336099813 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__iterations_16();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_2 = ___value0;
		__this->set__iterations_16(L_2);
	}

IL_0013:
	{
		return;
	}
}
// System.Single HighlightingSystem.HighlightingBase::get_blurMinSpread()
extern "C"  float HighlightingBase_get_blurMinSpread_m1916237220 (HighlightingBase_t336099813 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__blurMinSpread_17();
		return L_0;
	}
}
// System.Void HighlightingSystem.HighlightingBase::set_blurMinSpread(System.Single)
extern "C"  void HighlightingBase_set_blurMinSpread_m1361799121 (HighlightingBase_t336099813 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = __this->get__blurMinSpread_17();
		float L_1 = ___value0;
		if ((((float)L_0) == ((float)L_1)))
		{
			goto IL_0013;
		}
	}
	{
		float L_2 = ___value0;
		__this->set__blurMinSpread_17(L_2);
	}

IL_0013:
	{
		return;
	}
}
// System.Single HighlightingSystem.HighlightingBase::get_blurSpread()
extern "C"  float HighlightingBase_get_blurSpread_m3339261904 (HighlightingBase_t336099813 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__blurSpread_18();
		return L_0;
	}
}
// System.Void HighlightingSystem.HighlightingBase::set_blurSpread(System.Single)
extern "C"  void HighlightingBase_set_blurSpread_m175801855 (HighlightingBase_t336099813 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = __this->get__blurSpread_18();
		float L_1 = ___value0;
		if ((((float)L_0) == ((float)L_1)))
		{
			goto IL_0013;
		}
	}
	{
		float L_2 = ___value0;
		__this->set__blurSpread_18(L_2);
	}

IL_0013:
	{
		return;
	}
}
// System.Single HighlightingSystem.HighlightingBase::get_blurIntensity()
extern "C"  float HighlightingBase_get_blurIntensity_m4144433266 (HighlightingBase_t336099813 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__blurIntensity_19();
		return L_0;
	}
}
// System.Void HighlightingSystem.HighlightingBase::set_blurIntensity(System.Single)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t HighlightingBase_set_blurIntensity_m4010025897_MetadataUsageId;
extern "C"  void HighlightingBase_set_blurIntensity_m4010025897 (HighlightingBase_t336099813 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBase_set_blurIntensity_m4010025897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get__blurIntensity_19();
		float L_1 = ___value0;
		if ((((float)L_0) == ((float)L_1)))
		{
			goto IL_0033;
		}
	}
	{
		float L_2 = ___value0;
		__this->set__blurIntensity_19(L_2);
		bool L_3 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0033;
		}
	}
	{
		Material_t193706927 * L_4 = __this->get_blurMaterial_31();
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_5 = ShaderPropertyID_get__Intensity_m450774784(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = __this->get__blurIntensity_19();
		NullCheck(L_4);
		Material_SetFloat_m953675160(L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// HighlightingSystem.HighlightingBlitter HighlightingSystem.HighlightingBase::get_blitter()
extern "C"  HighlightingBlitter_t2949110578 * HighlightingBase_get_blitter_m2498396404 (HighlightingBase_t336099813 * __this, const MethodInfo* method)
{
	{
		HighlightingBlitter_t2949110578 * L_0 = __this->get__blitter_20();
		return L_0;
	}
}
// System.Void HighlightingSystem.HighlightingBase::set_blitter(HighlightingSystem.HighlightingBlitter)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t HighlightingBase_set_blitter_m288229937_MetadataUsageId;
extern "C"  void HighlightingBase_set_blitter_m288229937 (HighlightingBase_t336099813 * __this, HighlightingBlitter_t2949110578 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBase_set_blitter_m288229937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HighlightingBlitter_t2949110578 * L_0 = __this->get__blitter_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		HighlightingBlitter_t2949110578 * L_2 = __this->get__blitter_20();
		NullCheck(L_2);
		VirtActionInvoker1< HighlightingBase_t336099813 * >::Invoke(6 /* System.Void HighlightingSystem.HighlightingBlitter::Unregister(HighlightingSystem.HighlightingBase) */, L_2, __this);
	}

IL_001d:
	{
		HighlightingBlitter_t2949110578 * L_3 = ___value0;
		__this->set__blitter_20(L_3);
		HighlightingBlitter_t2949110578 * L_4 = __this->get__blitter_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0041;
		}
	}
	{
		HighlightingBlitter_t2949110578 * L_6 = __this->get__blitter_20();
		NullCheck(L_6);
		VirtActionInvoker1< HighlightingBase_t336099813 * >::Invoke(5 /* System.Void HighlightingSystem.HighlightingBlitter::Register(HighlightingSystem.HighlightingBase) */, L_6, __this);
	}

IL_0041:
	{
		return;
	}
}
// System.Void HighlightingSystem.HighlightingBase::OnEnable()
extern Il2CppClass* HighlightingBase_t336099813_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t193706927_il2cpp_TypeInfo_var;
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern Il2CppClass* CommandBuffer_t1204166949_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var;
extern const MethodInfo* HashSet_1_Add_m1841985763_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral851818281;
extern const uint32_t HighlightingBase_OnEnable_m21076391_MetadataUsageId;
extern "C"  void HighlightingBase_OnEnable_m21076391 (HighlightingBase_t336099813 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBase_OnEnable_m21076391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		HighlightingBase_Initialize_m1961791279(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_0 = VirtFuncInvoker1< bool, bool >::Invoke(9 /* System.Boolean HighlightingSystem.HighlightingBase::CheckSupported(System.Boolean) */, __this, (bool)1);
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral851818281, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		MaterialU5BU5D_t3123989686* L_1 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_materials_30();
		NullCheck(L_1);
		int32_t L_2 = 0;
		Material_t193706927 * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		Material_t193706927 * L_4 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_4, L_3, /*hidden argument*/NULL);
		__this->set_blurMaterial_31(L_4);
		MaterialU5BU5D_t3123989686* L_5 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_materials_30();
		NullCheck(L_5);
		int32_t L_6 = 1;
		Material_t193706927 * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		Material_t193706927 * L_8 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_8, L_7, /*hidden argument*/NULL);
		__this->set_cutMaterial_32(L_8);
		MaterialU5BU5D_t3123989686* L_9 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_materials_30();
		NullCheck(L_9);
		int32_t L_10 = 2;
		Material_t193706927 * L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		Material_t193706927 * L_12 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_12, L_11, /*hidden argument*/NULL);
		__this->set_compMaterial_33(L_12);
		Material_t193706927 * L_13 = __this->get_blurMaterial_31();
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_14 = ShaderPropertyID_get__Intensity_m450774784(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_15 = __this->get__blurIntensity_19();
		NullCheck(L_13);
		Material_SetFloat_m953675160(L_13, L_14, L_15, /*hidden argument*/NULL);
		CommandBuffer_t1204166949 * L_16 = (CommandBuffer_t1204166949 *)il2cpp_codegen_object_new(CommandBuffer_t1204166949_il2cpp_TypeInfo_var);
		CommandBuffer__ctor_m3893953450(L_16, /*hidden argument*/NULL);
		__this->set_renderBuffer_11(L_16);
		CommandBuffer_t1204166949 * L_17 = __this->get_renderBuffer_11();
		String_t* L_18 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_renderBufferName_3();
		NullCheck(L_17);
		CommandBuffer_set_name_m4179675112(L_17, L_18, /*hidden argument*/NULL);
		Camera_t189460977 * L_19 = Component_GetComponent_TisCamera_t189460977_m3276577584(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var);
		__this->set_cam_23(L_19);
		HashSet_1_t2817889127 * L_20 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_cameras_35();
		Camera_t189460977 * L_21 = __this->get_cam_23();
		NullCheck(L_20);
		HashSet_1_Add_m1841985763(L_20, L_21, /*hidden argument*/HashSet_1_Add_m1841985763_MethodInfo_var);
		Camera_t189460977 * L_22 = __this->get_cam_23();
		CommandBuffer_t1204166949 * L_23 = __this->get_renderBuffer_11();
		NullCheck(L_22);
		Camera_AddCommandBuffer_m2569587168(L_22, ((int32_t)12), L_23, /*hidden argument*/NULL);
		HighlightingBlitter_t2949110578 * L_24 = __this->get__blitter_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_24, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d7;
		}
	}
	{
		HighlightingBlitter_t2949110578 * L_26 = __this->get__blitter_20();
		NullCheck(L_26);
		VirtActionInvoker1< HighlightingBase_t336099813 * >::Invoke(5 /* System.Void HighlightingSystem.HighlightingBlitter::Register(HighlightingSystem.HighlightingBase) */, L_26, __this);
	}

IL_00d7:
	{
		return;
	}
}
// System.Void HighlightingSystem.HighlightingBase::OnDisable()
extern Il2CppClass* HighlightingBase_t336099813_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1_Remove_m1727832982_MethodInfo_var;
extern const uint32_t HighlightingBase_OnDisable_m3519437558_MetadataUsageId;
extern "C"  void HighlightingBase_OnDisable_m3519437558 (HighlightingBase_t336099813 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBase_OnDisable_m3519437558_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		HashSet_1_t2817889127 * L_0 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_cameras_35();
		Camera_t189460977 * L_1 = __this->get_cam_23();
		NullCheck(L_0);
		HashSet_1_Remove_m1727832982(L_0, L_1, /*hidden argument*/HashSet_1_Remove_m1727832982_MethodInfo_var);
		CommandBuffer_t1204166949 * L_2 = __this->get_renderBuffer_11();
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		Camera_t189460977 * L_3 = __this->get_cam_23();
		CommandBuffer_t1204166949 * L_4 = __this->get_renderBuffer_11();
		NullCheck(L_3);
		Camera_RemoveCommandBuffer_m2103408695(L_3, ((int32_t)12), L_4, /*hidden argument*/NULL);
		__this->set_renderBuffer_11((CommandBuffer_t1204166949 *)NULL);
	}

IL_0036:
	{
		RenderTexture_t2666733923 * L_5 = __this->get_highlightingBuffer_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0069;
		}
	}
	{
		RenderTexture_t2666733923 * L_7 = __this->get_highlightingBuffer_22();
		NullCheck(L_7);
		bool L_8 = RenderTexture_IsCreated_m375737400(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0069;
		}
	}
	{
		RenderTexture_t2666733923 * L_9 = __this->get_highlightingBuffer_22();
		NullCheck(L_9);
		RenderTexture_Release_m1476285593(L_9, /*hidden argument*/NULL);
		__this->set_highlightingBuffer_22((RenderTexture_t2666733923 *)NULL);
	}

IL_0069:
	{
		HighlightingBlitter_t2949110578 * L_10 = __this->get__blitter_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_10, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0086;
		}
	}
	{
		HighlightingBlitter_t2949110578 * L_12 = __this->get__blitter_20();
		NullCheck(L_12);
		VirtActionInvoker1< HighlightingBase_t336099813 * >::Invoke(6 /* System.Void HighlightingSystem.HighlightingBlitter::Unregister(HighlightingSystem.HighlightingBase) */, L_12, __this);
	}

IL_0086:
	{
		return;
	}
}
// System.Void HighlightingSystem.HighlightingBase::OnPreRender()
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* RenderTexture_t2666733923_il2cpp_TypeInfo_var;
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral326427641;
extern Il2CppCodeGenString* _stringLiteral504229039;
extern Il2CppCodeGenString* _stringLiteral2793851401;
extern const uint32_t HighlightingBase_OnPreRender_m4139244023_MetadataUsageId;
extern "C"  void HighlightingBase_OnPreRender_m4139244023 (HighlightingBase_t336099813 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBase_OnPreRender_m4139244023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	bool V_2 = false;
	Vector4_t2243707581  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t G_B11_0 = 0;
	bool G_B18_0 = false;
	bool G_B15_0 = false;
	bool G_B16_0 = false;
	bool G_B17_0 = false;
	int32_t G_B19_0 = 0;
	bool G_B19_1 = false;
	int32_t G_B25_0 = 0;
	int32_t G_B25_1 = 0;
	HighlightingBase_t336099813 * G_B25_2 = NULL;
	int32_t G_B24_0 = 0;
	int32_t G_B24_1 = 0;
	HighlightingBase_t336099813 * G_B24_2 = NULL;
	int32_t G_B26_0 = 0;
	int32_t G_B26_1 = 0;
	int32_t G_B26_2 = 0;
	HighlightingBase_t336099813 * G_B26_3 = NULL;
	{
		V_0 = (bool)0;
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 HighlightingSystem.HighlightingBase::GetAA() */, __this);
		V_1 = L_0;
		int32_t L_1 = V_1;
		V_2 = (bool)((((int32_t)L_1) == ((int32_t)1))? 1 : 0);
		Camera_t189460977 * L_2 = __this->get_cam_23();
		NullCheck(L_2);
		int32_t L_3 = Camera_get_actualRenderingPath_m1894280031(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_002f;
		}
	}
	{
		Camera_t189460977 * L_4 = __this->get_cam_23();
		NullCheck(L_4);
		int32_t L_5 = Camera_get_actualRenderingPath_m1894280031(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_005c;
		}
	}

IL_002f:
	{
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) <= ((int32_t)1)))
		{
			goto IL_0038;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_0038:
	{
		Camera_t189460977 * L_7 = __this->get_cam_23();
		NullCheck(L_7);
		int32_t L_8 = Camera_get_clearFlags_m1743144302(L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)3)))
		{
			goto IL_005a;
		}
	}
	{
		Camera_t189460977 * L_9 = __this->get_cam_23();
		NullCheck(L_9);
		int32_t L_10 = Camera_get_clearFlags_m1743144302(L_9, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)4))))
		{
			goto IL_005c;
		}
	}

IL_005a:
	{
		V_2 = (bool)0;
	}

IL_005c:
	{
		bool L_11 = __this->get_isDepthAvailable_24();
		bool L_12 = V_2;
		if ((((int32_t)L_11) == ((int32_t)L_12)))
		{
			goto IL_00ac;
		}
	}
	{
		V_0 = (bool)1;
		bool L_13 = V_2;
		__this->set_isDepthAvailable_24(L_13);
		bool L_14 = __this->get_isDepthAvailable_24();
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		G_B11_0 = 0;
		goto IL_0083;
	}

IL_0082:
	{
		G_B11_0 = 1;
	}

IL_0083:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		Highlighter_SetZWrite_m2014013653(NULL /*static, unused*/, G_B11_0, /*hidden argument*/NULL);
		bool L_15 = __this->get_isDepthAvailable_24();
		if (!L_15)
		{
			goto IL_00a2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral326427641, /*hidden argument*/NULL);
		goto IL_00ac;
	}

IL_00a2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral504229039, /*hidden argument*/NULL);
	}

IL_00ac:
	{
		bool L_16 = V_0;
		RenderTexture_t2666733923 * L_17 = __this->get_highlightingBuffer_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_17, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		G_B15_0 = L_16;
		if (L_18)
		{
			G_B18_0 = L_16;
			goto IL_00f8;
		}
	}
	{
		Camera_t189460977 * L_19 = __this->get_cam_23();
		NullCheck(L_19);
		int32_t L_20 = Camera_get_pixelWidth_m2325481193(L_19, /*hidden argument*/NULL);
		int32_t L_21 = __this->get_cachedWidth_12();
		G_B16_0 = G_B15_0;
		if ((!(((uint32_t)L_20) == ((uint32_t)L_21))))
		{
			G_B18_0 = G_B15_0;
			goto IL_00f8;
		}
	}
	{
		Camera_t189460977 * L_22 = __this->get_cam_23();
		NullCheck(L_22);
		int32_t L_23 = Camera_get_pixelHeight_m2527046964(L_22, /*hidden argument*/NULL);
		int32_t L_24 = __this->get_cachedHeight_13();
		G_B17_0 = G_B16_0;
		if ((!(((uint32_t)L_23) == ((uint32_t)L_24))))
		{
			G_B18_0 = G_B16_0;
			goto IL_00f8;
		}
	}
	{
		int32_t L_25 = V_1;
		int32_t L_26 = __this->get_cachedAA_14();
		G_B19_0 = ((((int32_t)((((int32_t)L_25) == ((int32_t)L_26))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		G_B19_1 = G_B17_0;
		goto IL_00f9;
	}

IL_00f8:
	{
		G_B19_0 = 1;
		G_B19_1 = G_B18_0;
	}

IL_00f9:
	{
		V_0 = (bool)((int32_t)((int32_t)G_B19_1|(int32_t)G_B19_0));
		bool L_27 = V_0;
		if (!L_27)
		{
			goto IL_0254;
		}
	}
	{
		RenderTexture_t2666733923 * L_28 = __this->get_highlightingBuffer_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_29 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_28, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_012d;
		}
	}
	{
		RenderTexture_t2666733923 * L_30 = __this->get_highlightingBuffer_22();
		NullCheck(L_30);
		bool L_31 = RenderTexture_IsCreated_m375737400(L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_012d;
		}
	}
	{
		RenderTexture_t2666733923 * L_32 = __this->get_highlightingBuffer_22();
		NullCheck(L_32);
		RenderTexture_Release_m1476285593(L_32, /*hidden argument*/NULL);
	}

IL_012d:
	{
		Camera_t189460977 * L_33 = __this->get_cam_23();
		NullCheck(L_33);
		int32_t L_34 = Camera_get_pixelWidth_m2325481193(L_33, /*hidden argument*/NULL);
		__this->set_cachedWidth_12(L_34);
		Camera_t189460977 * L_35 = __this->get_cam_23();
		NullCheck(L_35);
		int32_t L_36 = Camera_get_pixelHeight_m2527046964(L_35, /*hidden argument*/NULL);
		__this->set_cachedHeight_13(L_36);
		int32_t L_37 = V_1;
		__this->set_cachedAA_14(L_37);
		int32_t L_38 = __this->get_cachedWidth_12();
		int32_t L_39 = __this->get_cachedHeight_13();
		bool L_40 = __this->get_isDepthAvailable_24();
		G_B24_0 = L_39;
		G_B24_1 = L_38;
		G_B24_2 = __this;
		if (!L_40)
		{
			G_B25_0 = L_39;
			G_B25_1 = L_38;
			G_B25_2 = __this;
			goto IL_0174;
		}
	}
	{
		G_B26_0 = 0;
		G_B26_1 = G_B24_0;
		G_B26_2 = G_B24_1;
		G_B26_3 = G_B24_2;
		goto IL_0176;
	}

IL_0174:
	{
		G_B26_0 = ((int32_t)24);
		G_B26_1 = G_B25_0;
		G_B26_2 = G_B25_1;
		G_B26_3 = G_B25_2;
	}

IL_0176:
	{
		RenderTexture_t2666733923 * L_41 = (RenderTexture_t2666733923 *)il2cpp_codegen_object_new(RenderTexture_t2666733923_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m1475925789(L_41, G_B26_2, G_B26_1, G_B26_0, 0, 0, /*hidden argument*/NULL);
		NullCheck(G_B26_3);
		G_B26_3->set_highlightingBuffer_22(L_41);
		RenderTexture_t2666733923 * L_42 = __this->get_highlightingBuffer_22();
		int32_t L_43 = __this->get_cachedAA_14();
		NullCheck(L_42);
		RenderTexture_set_antiAliasing_m1838970818(L_42, L_43, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_44 = __this->get_highlightingBuffer_22();
		NullCheck(L_44);
		Texture_set_filterMode_m3838996656(L_44, 0, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_45 = __this->get_highlightingBuffer_22();
		NullCheck(L_45);
		RenderTexture_set_useMipMap_m627913929(L_45, (bool)0, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_46 = __this->get_highlightingBuffer_22();
		NullCheck(L_46);
		Texture_set_wrapMode_m333956747(L_46, 1, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_47 = __this->get_highlightingBuffer_22();
		NullCheck(L_47);
		bool L_48 = RenderTexture_Create_m1259588374(L_47, /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_01d1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral2793851401, /*hidden argument*/NULL);
	}

IL_01d1:
	{
		RenderTexture_t2666733923 * L_49 = __this->get_highlightingBuffer_22();
		RenderTargetIdentifier_t772440638  L_50;
		memset(&L_50, 0, sizeof(L_50));
		RenderTargetIdentifier__ctor_m957202749(&L_50, L_49, /*hidden argument*/NULL);
		__this->set_highlightingBufferID_21(L_50);
		Material_t193706927 * L_51 = __this->get_cutMaterial_32();
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_52 = ShaderPropertyID_get__HighlightingBuffer_m2889951321(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_53 = __this->get_highlightingBuffer_22();
		NullCheck(L_51);
		Material_SetTexture_m58378708(L_51, L_52, L_53, /*hidden argument*/NULL);
		Material_t193706927 * L_54 = __this->get_compMaterial_33();
		int32_t L_55 = ShaderPropertyID_get__HighlightingBuffer_m2889951321(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_56 = __this->get_highlightingBuffer_22();
		NullCheck(L_54);
		Material_SetTexture_m58378708(L_54, L_55, L_56, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_57 = __this->get_highlightingBuffer_22();
		NullCheck(L_57);
		int32_t L_58 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_57);
		RenderTexture_t2666733923 * L_59 = __this->get_highlightingBuffer_22();
		NullCheck(L_59);
		int32_t L_60 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_59);
		Vector4__ctor_m1222289168((&V_3), ((float)((float)(1.0f)/(float)(((float)((float)L_58))))), ((float)((float)(1.0f)/(float)(((float)((float)L_60))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		Material_t193706927 * L_61 = __this->get_cutMaterial_32();
		int32_t L_62 = ShaderPropertyID_get__HighlightingBufferTexelSize_m3370777770(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector4_t2243707581  L_63 = V_3;
		NullCheck(L_61);
		Material_SetVector_m1395004512(L_61, L_62, L_63, /*hidden argument*/NULL);
	}

IL_0254:
	{
		float L_64 = __this->get_offsetFactor_9();
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		Highlighter_SetOffsetFactor_m1804810322(NULL /*static, unused*/, L_64, /*hidden argument*/NULL);
		float L_65 = __this->get_offsetUnits_10();
		Highlighter_SetOffsetUnits_m623061432(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(10 /* System.Void HighlightingSystem.HighlightingBase::RebuildCommandBuffer() */, __this);
		return;
	}
}
// System.Void HighlightingSystem.HighlightingBase::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Graphics_t2412809155_il2cpp_TypeInfo_var;
extern const uint32_t HighlightingBase_OnRenderImage_m2221789855_MetadataUsageId;
extern "C"  void HighlightingBase_OnRenderImage_m2221789855 (HighlightingBase_t336099813 * __this, RenderTexture_t2666733923 * ___src0, RenderTexture_t2666733923 * ___dst1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBase_OnRenderImage_m2221789855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HighlightingBlitter_t2949110578 * L_0 = HighlightingBase_get_blitter_m2498396404(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		RenderTexture_t2666733923 * L_2 = ___src0;
		RenderTexture_t2666733923 * L_3 = ___dst1;
		VirtActionInvoker2< RenderTexture_t2666733923 *, RenderTexture_t2666733923 * >::Invoke(11 /* System.Void HighlightingSystem.HighlightingBase::Blit(UnityEngine.RenderTexture,UnityEngine.RenderTexture) */, __this, L_2, L_3);
		goto IL_0025;
	}

IL_001e:
	{
		RenderTexture_t2666733923 * L_4 = ___src0;
		RenderTexture_t2666733923 * L_5 = ___dst1;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t2412809155_il2cpp_TypeInfo_var);
		Graphics_Blit_m2123328641(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Boolean HighlightingSystem.HighlightingBase::IsHighlightingCamera(UnityEngine.Camera)
extern Il2CppClass* HighlightingBase_t336099813_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1_Contains_m3473640089_MethodInfo_var;
extern const uint32_t HighlightingBase_IsHighlightingCamera_m2166075236_MetadataUsageId;
extern "C"  bool HighlightingBase_IsHighlightingCamera_m2166075236 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBase_IsHighlightingCamera_m2166075236_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		HashSet_1_t2817889127 * L_0 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_cameras_35();
		Camera_t189460977 * L_1 = ___cam0;
		NullCheck(L_0);
		bool L_2 = HashSet_1_Contains_m3473640089(L_0, L_1, /*hidden argument*/HashSet_1_Contains_m3473640089_MethodInfo_var);
		return L_2;
	}
}
// System.Void HighlightingSystem.HighlightingBase::Initialize()
extern Il2CppClass* HighlightingBase_t336099813_il2cpp_TypeInfo_var;
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern Il2CppClass* ShaderU5BU5D_t1916779366_il2cpp_TypeInfo_var;
extern Il2CppClass* MaterialU5BU5D_t3123989686_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t193706927_il2cpp_TypeInfo_var;
extern const uint32_t HighlightingBase_Initialize_m1961791279_MetadataUsageId;
extern "C"  void HighlightingBase_Initialize_m1961791279 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBase_Initialize_m1961791279_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Shader_t2430389951 * V_2 = NULL;
	Material_t193706927 * V_3 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		bool L_0 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_initialized_34();
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		int32_t L_1 = SystemInfo_get_graphicsDeviceType_m2764442219(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->set_device_8(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		ShaderPropertyID_Initialize_m2760510245(NULL /*static, unused*/, /*hidden argument*/NULL);
		StringU5BU5D_t1642385972* L_2 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_shaderPaths_28();
		NullCheck(L_2);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))));
		int32_t L_3 = V_0;
		((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->set_shaders_29(((ShaderU5BU5D_t1916779366*)SZArrayNew(ShaderU5BU5D_t1916779366_il2cpp_TypeInfo_var, (uint32_t)L_3)));
		int32_t L_4 = V_0;
		((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->set_materials_30(((MaterialU5BU5D_t3123989686*)SZArrayNew(MaterialU5BU5D_t3123989686_il2cpp_TypeInfo_var, (uint32_t)L_4)));
		V_1 = 0;
		goto IL_0067;
	}

IL_003f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		StringU5BU5D_t1642385972* L_5 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_shaderPaths_28();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		String_t* L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		Shader_t2430389951 * L_9 = Shader_Find_m4179408078(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		ShaderU5BU5D_t1916779366* L_10 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_shaders_29();
		int32_t L_11 = V_1;
		Shader_t2430389951 * L_12 = V_2;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_12);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (Shader_t2430389951 *)L_12);
		Shader_t2430389951 * L_13 = V_2;
		Material_t193706927 * L_14 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1897560860(L_14, L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		MaterialU5BU5D_t3123989686* L_15 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_materials_30();
		int32_t L_16 = V_1;
		Material_t193706927 * L_17 = V_3;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (Material_t193706927 *)L_17);
		int32_t L_18 = V_1;
		V_1 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0067:
	{
		int32_t L_19 = V_1;
		int32_t L_20 = V_0;
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_003f;
		}
	}
	{
		RenderTargetIdentifier_t772440638  L_21;
		memset(&L_21, 0, sizeof(L_21));
		RenderTargetIdentifier__ctor_m607511440(&L_21, 2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->set_cameraTargetID_6(L_21);
		HighlightingBase_CreateQuad_m1421589878(NULL /*static, unused*/, /*hidden argument*/NULL);
		((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->set_initialized_34((bool)1);
		return;
	}
}
// System.Void HighlightingSystem.HighlightingBase::CreateQuad()
extern Il2CppClass* HighlightingBase_t336099813_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Mesh_t1356156583_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var;
extern Il2CppClass* ColorU5BU5D_t672350442_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305143____U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1_FieldInfo_var;
extern const uint32_t HighlightingBase_CreateQuad_m1421589878_MetadataUsageId;
extern "C"  void HighlightingBase_CreateQuad_m1421589878 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBase_CreateQuad_m1421589878_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		Mesh_t1356156583 * L_0 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_quad_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Mesh_t1356156583 * L_2 = (Mesh_t1356156583 *)il2cpp_codegen_object_new(Mesh_t1356156583_il2cpp_TypeInfo_var);
		Mesh__ctor_m2975981674(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->set_quad_7(L_2);
		goto IL_0029;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		Mesh_t1356156583 * L_3 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_quad_7();
		NullCheck(L_3);
		Mesh_Clear_m231813403(L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		V_0 = (-1.0f);
		V_1 = (1.0f);
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		bool L_4 = HighlightingBase_get_uvStartsAtTop_m1786397426(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = (1.0f);
		V_1 = (-1.0f);
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		Mesh_t1356156583 * L_5 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_quad_7();
		Vector3U5BU5D_t1172311765* L_6 = ((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_6);
		float L_7 = V_0;
		Vector3_t2243707580  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m2638739322(&L_8, (-1.0f), L_7, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_8;
		Vector3U5BU5D_t1172311765* L_9 = L_6;
		NullCheck(L_9);
		float L_10 = V_1;
		Vector3_t2243707580  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector3__ctor_m2638739322(&L_11, (-1.0f), L_10, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_11;
		Vector3U5BU5D_t1172311765* L_12 = L_9;
		NullCheck(L_12);
		float L_13 = V_1;
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, (1.0f), L_13, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_14;
		Vector3U5BU5D_t1172311765* L_15 = L_12;
		NullCheck(L_15);
		float L_16 = V_0;
		Vector3_t2243707580  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m2638739322(&L_17, (1.0f), L_16, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_17;
		NullCheck(L_5);
		Mesh_set_vertices_m2936804213(L_5, L_15, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_18 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_quad_7();
		Vector2U5BU5D_t686124026* L_19 = ((Vector2U5BU5D_t686124026*)SZArrayNew(Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_19);
		Vector2_t2243707579  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector2__ctor_m3067419446(&L_20, (0.0f), (0.0f), /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_20;
		Vector2U5BU5D_t686124026* L_21 = L_19;
		NullCheck(L_21);
		Vector2_t2243707579  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector2__ctor_m3067419446(&L_22, (0.0f), (1.0f), /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_22;
		Vector2U5BU5D_t686124026* L_23 = L_21;
		NullCheck(L_23);
		Vector2_t2243707579  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector2__ctor_m3067419446(&L_24, (1.0f), (1.0f), /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_24;
		Vector2U5BU5D_t686124026* L_25 = L_23;
		NullCheck(L_25);
		Vector2_t2243707579  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Vector2__ctor_m3067419446(&L_26, (1.0f), (0.0f), /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_26;
		NullCheck(L_18);
		Mesh_set_uv_m1497318906(L_18, L_25, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_27 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_quad_7();
		ColorU5BU5D_t672350442* L_28 = ((ColorU5BU5D_t672350442*)SZArrayNew(ColorU5BU5D_t672350442_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_28);
		Color_t2020392075  L_29 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_colorClear_2();
		(*(Color_t2020392075 *)((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_29;
		ColorU5BU5D_t672350442* L_30 = L_28;
		NullCheck(L_30);
		Color_t2020392075  L_31 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_colorClear_2();
		(*(Color_t2020392075 *)((L_30)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_31;
		ColorU5BU5D_t672350442* L_32 = L_30;
		NullCheck(L_32);
		Color_t2020392075  L_33 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_colorClear_2();
		(*(Color_t2020392075 *)((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_33;
		ColorU5BU5D_t672350442* L_34 = L_32;
		NullCheck(L_34);
		Color_t2020392075  L_35 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_colorClear_2();
		(*(Color_t2020392075 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_35;
		NullCheck(L_27);
		Mesh_set_colors_m864557505(L_27, L_34, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_36 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_quad_7();
		Int32U5BU5D_t3030399641* L_37 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)6));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_37, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305143____U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1_FieldInfo_var), /*hidden argument*/NULL);
		NullCheck(L_36);
		Mesh_set_triangles_m3244966865(L_36, L_37, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 HighlightingSystem.HighlightingBase::GetAA()
extern "C"  int32_t HighlightingBase_GetAA_m3391488427 (HighlightingBase_t336099813 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = QualitySettings_get_antiAliasing_m1084683812(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		V_0 = 1;
	}

IL_000e:
	{
		Camera_t189460977 * L_2 = __this->get_cam_23();
		NullCheck(L_2);
		int32_t L_3 = Camera_get_actualRenderingPath_m1894280031(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_0030;
		}
	}
	{
		Camera_t189460977 * L_4 = __this->get_cam_23();
		NullCheck(L_4);
		int32_t L_5 = Camera_get_actualRenderingPath_m1894280031(L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)3))))
		{
			goto IL_0032;
		}
	}

IL_0030:
	{
		V_0 = 1;
	}

IL_0032:
	{
		int32_t L_6 = V_0;
		return L_6;
	}
}
// System.Boolean HighlightingSystem.HighlightingBase::CheckSupported(System.Boolean)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern Il2CppClass* HighlightingBase_t336099813_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral639331264;
extern Il2CppCodeGenString* _stringLiteral1201975157;
extern Il2CppCodeGenString* _stringLiteral3277743172;
extern Il2CppCodeGenString* _stringLiteral3770351213;
extern Il2CppCodeGenString* _stringLiteral1794362073;
extern Il2CppCodeGenString* _stringLiteral1549327674;
extern Il2CppCodeGenString* _stringLiteral1351620165;
extern Il2CppCodeGenString* _stringLiteral2971277279;
extern const uint32_t HighlightingBase_CheckSupported_m3712654012_MetadataUsageId;
extern "C"  bool HighlightingBase_CheckSupported_m3712654012 (HighlightingBase_t336099813 * __this, bool ___verbose0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBase_CheckSupported_m3712654012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Shader_t2430389951 * V_2 = NULL;
	{
		V_0 = (bool)1;
		bool L_0 = SystemInfo_get_supportsImageEffects_m406299852(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		bool L_1 = ___verbose0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral639331264, /*hidden argument*/NULL);
	}

IL_001c:
	{
		V_0 = (bool)0;
	}

IL_001e:
	{
		bool L_2 = SystemInfo_get_supportsRenderTextures_m2715598897(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_003a;
		}
	}
	{
		bool L_3 = ___verbose0;
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1201975157, /*hidden argument*/NULL);
	}

IL_0038:
	{
		V_0 = (bool)0;
	}

IL_003a:
	{
		bool L_4 = SystemInfo_SupportsRenderTextureFormat_m3185388893(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0057;
		}
	}
	{
		bool L_5 = ___verbose0;
		if (!L_5)
		{
			goto IL_0055;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3277743172, /*hidden argument*/NULL);
	}

IL_0055:
	{
		V_0 = (bool)0;
	}

IL_0057:
	{
		int32_t L_6 = SystemInfo_get_supportsStencil_m3808368315(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_6) >= ((int32_t)1)))
		{
			goto IL_0074;
		}
	}
	{
		bool L_7 = ___verbose0;
		if (!L_7)
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3770351213, /*hidden argument*/NULL);
	}

IL_0072:
	{
		V_0 = (bool)0;
	}

IL_0074:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		Shader_t2430389951 * L_8 = Highlighter_get_opaqueShader_m1430768539(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = Shader_get_isSupported_m344486701(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0095;
		}
	}
	{
		bool L_10 = ___verbose0;
		if (!L_10)
		{
			goto IL_0093;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1794362073, /*hidden argument*/NULL);
	}

IL_0093:
	{
		V_0 = (bool)0;
	}

IL_0095:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		Shader_t2430389951 * L_11 = Highlighter_get_transparentShader_m3015025486(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		bool L_12 = Shader_get_isSupported_m344486701(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_00b6;
		}
	}
	{
		bool L_13 = ___verbose0;
		if (!L_13)
		{
			goto IL_00b4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1549327674, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		V_0 = (bool)0;
	}

IL_00b6:
	{
		V_1 = 0;
		goto IL_00f6;
	}

IL_00bd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		ShaderU5BU5D_t1916779366* L_14 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_shaders_29();
		int32_t L_15 = V_1;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Shader_t2430389951 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_2 = L_17;
		Shader_t2430389951 * L_18 = V_2;
		NullCheck(L_18);
		bool L_19 = Shader_get_isSupported_m344486701(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00f2;
		}
	}
	{
		bool L_20 = ___verbose0;
		if (!L_20)
		{
			goto IL_00f0;
		}
	}
	{
		Shader_t2430389951 * L_21 = V_2;
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m2079638459(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1351620165, L_22, _stringLiteral2971277279, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
	}

IL_00f0:
	{
		V_0 = (bool)0;
	}

IL_00f2:
	{
		int32_t L_24 = V_1;
		V_1 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00f6:
	{
		int32_t L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		ShaderU5BU5D_t1916779366* L_26 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_shaders_29();
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length)))))))
		{
			goto IL_00bd;
		}
	}
	{
		bool L_27 = V_0;
		return L_27;
	}
}
// System.Void HighlightingSystem.HighlightingBase::RebuildCommandBuffer()
extern Il2CppClass* HighlightingBase_t336099813_il2cpp_TypeInfo_var;
extern Il2CppClass* Highlighter_t958778585_il2cpp_TypeInfo_var;
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t HighlightingBase_RebuildCommandBuffer_m3517084141_MetadataUsageId;
extern "C"  void HighlightingBase_RebuildCommandBuffer_m3517084141 (HighlightingBase_t336099813 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBase_RebuildCommandBuffer_m3517084141_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RenderTargetIdentifier_t772440638  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RenderTargetIdentifier_t772440638  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RenderTargetIdentifier_t772440638  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	bool V_5 = false;
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	RenderTargetIdentifier_t772440638  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	int32_t G_B11_0 = 0;
	CommandBuffer_t1204166949 * G_B11_1 = NULL;
	int32_t G_B10_0 = 0;
	CommandBuffer_t1204166949 * G_B10_1 = NULL;
	RenderTargetIdentifier_t772440638  G_B12_0;
	memset(&G_B12_0, 0, sizeof(G_B12_0));
	int32_t G_B12_1 = 0;
	CommandBuffer_t1204166949 * G_B12_2 = NULL;
	{
		CommandBuffer_t1204166949 * L_0 = __this->get_renderBuffer_11();
		NullCheck(L_0);
		CommandBuffer_Clear_m3644095167(L_0, /*hidden argument*/NULL);
		bool L_1 = __this->get_isDepthAvailable_24();
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		RenderTargetIdentifier_t772440638  L_2 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_cameraTargetID_6();
		G_B3_0 = L_2;
		goto IL_0026;
	}

IL_0020:
	{
		RenderTargetIdentifier_t772440638  L_3 = __this->get_highlightingBufferID_21();
		G_B3_0 = L_3;
	}

IL_0026:
	{
		V_0 = G_B3_0;
		CommandBuffer_t1204166949 * L_4 = __this->get_renderBuffer_11();
		RenderTargetIdentifier_t772440638  L_5 = __this->get_highlightingBufferID_21();
		RenderTargetIdentifier_t772440638  L_6 = V_0;
		NullCheck(L_4);
		CommandBuffer_SetRenderTarget_m3943293835(L_4, L_5, L_6, /*hidden argument*/NULL);
		CommandBuffer_t1204166949 * L_7 = __this->get_renderBuffer_11();
		bool L_8 = __this->get_isDepthAvailable_24();
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		Color_t2020392075  L_9 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_colorClear_2();
		NullCheck(L_7);
		CommandBuffer_ClearRenderTarget_m4246011180(L_7, (bool)((((int32_t)L_8) == ((int32_t)0))? 1 : 0), (bool)1, L_9, /*hidden argument*/NULL);
		CommandBuffer_t1204166949 * L_10 = __this->get_renderBuffer_11();
		bool L_11 = __this->get_isDepthAvailable_24();
		IL2CPP_RUNTIME_CLASS_INIT(Highlighter_t958778585_il2cpp_TypeInfo_var);
		Highlighter_FillBuffer_m3838603729(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_12 = ShaderPropertyID_get__HighlightingBlur1_m3786131333(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTargetIdentifier__ctor_m1216379536((&V_1), L_12, /*hidden argument*/NULL);
		int32_t L_13 = ShaderPropertyID_get__HighlightingBlur2_m1319370004(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTargetIdentifier__ctor_m1216379536((&V_2), L_13, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_14 = __this->get_highlightingBuffer_22();
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_14);
		int32_t L_16 = __this->get__downsampleFactor_15();
		V_3 = ((int32_t)((int32_t)L_15/(int32_t)L_16));
		RenderTexture_t2666733923 * L_17 = __this->get_highlightingBuffer_22();
		NullCheck(L_17);
		int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_17);
		int32_t L_19 = __this->get__downsampleFactor_15();
		V_4 = ((int32_t)((int32_t)L_18/(int32_t)L_19));
		CommandBuffer_t1204166949 * L_20 = __this->get_renderBuffer_11();
		int32_t L_21 = ShaderPropertyID_get__HighlightingBlur1_m3786131333(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_22 = V_3;
		int32_t L_23 = V_4;
		NullCheck(L_20);
		CommandBuffer_GetTemporaryRT_m435236685(L_20, L_21, L_22, L_23, 0, 1, 0, 0, /*hidden argument*/NULL);
		CommandBuffer_t1204166949 * L_24 = __this->get_renderBuffer_11();
		int32_t L_25 = ShaderPropertyID_get__HighlightingBlur2_m1319370004(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_26 = V_3;
		int32_t L_27 = V_4;
		NullCheck(L_24);
		CommandBuffer_GetTemporaryRT_m435236685(L_24, L_25, L_26, L_27, 0, 1, 0, 0, /*hidden argument*/NULL);
		CommandBuffer_t1204166949 * L_28 = __this->get_renderBuffer_11();
		RenderTargetIdentifier_t772440638  L_29 = __this->get_highlightingBufferID_21();
		RenderTargetIdentifier_t772440638  L_30 = V_1;
		NullCheck(L_28);
		CommandBuffer_Blit_m2244621585(L_28, L_29, L_30, /*hidden argument*/NULL);
		V_5 = (bool)1;
		V_6 = 0;
		goto IL_0152;
	}

IL_00ee:
	{
		float L_31 = __this->get__blurMinSpread_17();
		float L_32 = __this->get__blurSpread_18();
		int32_t L_33 = V_6;
		V_7 = ((float)((float)L_31+(float)((float)((float)L_32*(float)(((float)((float)L_33)))))));
		CommandBuffer_t1204166949 * L_34 = __this->get_renderBuffer_11();
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_35 = ShaderPropertyID_get__HighlightingBlurOffset_m2757156113(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_36 = V_7;
		NullCheck(L_34);
		CommandBuffer_SetGlobalFloat_m2538455203(L_34, L_35, L_36, /*hidden argument*/NULL);
		bool L_37 = V_5;
		if (!L_37)
		{
			goto IL_0132;
		}
	}
	{
		CommandBuffer_t1204166949 * L_38 = __this->get_renderBuffer_11();
		RenderTargetIdentifier_t772440638  L_39 = V_1;
		RenderTargetIdentifier_t772440638  L_40 = V_2;
		Material_t193706927 * L_41 = __this->get_blurMaterial_31();
		NullCheck(L_38);
		CommandBuffer_Blit_m3411770257(L_38, L_39, L_40, L_41, /*hidden argument*/NULL);
		goto IL_0145;
	}

IL_0132:
	{
		CommandBuffer_t1204166949 * L_42 = __this->get_renderBuffer_11();
		RenderTargetIdentifier_t772440638  L_43 = V_2;
		RenderTargetIdentifier_t772440638  L_44 = V_1;
		Material_t193706927 * L_45 = __this->get_blurMaterial_31();
		NullCheck(L_42);
		CommandBuffer_Blit_m3411770257(L_42, L_43, L_44, L_45, /*hidden argument*/NULL);
	}

IL_0145:
	{
		bool L_46 = V_5;
		V_5 = (bool)((((int32_t)L_46) == ((int32_t)0))? 1 : 0);
		int32_t L_47 = V_6;
		V_6 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_0152:
	{
		int32_t L_48 = V_6;
		int32_t L_49 = __this->get__iterations_16();
		if ((((int32_t)L_48) < ((int32_t)L_49)))
		{
			goto IL_00ee;
		}
	}
	{
		CommandBuffer_t1204166949 * L_50 = __this->get_renderBuffer_11();
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_51 = ShaderPropertyID_get__HighlightingBlurred_m412791331(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_52 = V_5;
		G_B10_0 = L_51;
		G_B10_1 = L_50;
		if (!L_52)
		{
			G_B11_0 = L_51;
			G_B11_1 = L_50;
			goto IL_0177;
		}
	}
	{
		RenderTargetIdentifier_t772440638  L_53 = V_1;
		G_B12_0 = L_53;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		goto IL_0178;
	}

IL_0177:
	{
		RenderTargetIdentifier_t772440638  L_54 = V_2;
		G_B12_0 = L_54;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
	}

IL_0178:
	{
		NullCheck(G_B12_2);
		CommandBuffer_SetGlobalTexture_m3541048502(G_B12_2, G_B12_1, G_B12_0, /*hidden argument*/NULL);
		CommandBuffer_t1204166949 * L_55 = __this->get_renderBuffer_11();
		RenderTargetIdentifier_t772440638  L_56 = __this->get_highlightingBufferID_21();
		RenderTargetIdentifier_t772440638  L_57 = V_0;
		NullCheck(L_55);
		CommandBuffer_SetRenderTarget_m3943293835(L_55, L_56, L_57, /*hidden argument*/NULL);
		CommandBuffer_t1204166949 * L_58 = __this->get_renderBuffer_11();
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		Mesh_t1356156583 * L_59 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_quad_7();
		Matrix4x4_t2933234003  L_60 = ((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->get_identityMatrix_4();
		Material_t193706927 * L_61 = __this->get_cutMaterial_32();
		NullCheck(L_58);
		CommandBuffer_DrawMesh_m1921916513(L_58, L_59, L_60, L_61, /*hidden argument*/NULL);
		CommandBuffer_t1204166949 * L_62 = __this->get_renderBuffer_11();
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_63 = ShaderPropertyID_get__HighlightingBlur1_m3786131333(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_62);
		CommandBuffer_ReleaseTemporaryRT_m2210944847(L_62, L_63, /*hidden argument*/NULL);
		CommandBuffer_t1204166949 * L_64 = __this->get_renderBuffer_11();
		int32_t L_65 = ShaderPropertyID_get__HighlightingBlur2_m1319370004(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_64);
		CommandBuffer_ReleaseTemporaryRT_m2210944847(L_64, L_65, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightingSystem.HighlightingBase::Blit(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Graphics_t2412809155_il2cpp_TypeInfo_var;
extern const uint32_t HighlightingBase_Blit_m2022560864_MetadataUsageId;
extern "C"  void HighlightingBase_Blit_m2022560864 (HighlightingBase_t336099813 * __this, RenderTexture_t2666733923 * ___src0, RenderTexture_t2666733923 * ___dst1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBase_Blit_m2022560864_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RenderTexture_t2666733923 * L_0 = ___src0;
		RenderTexture_t2666733923 * L_1 = ___dst1;
		Material_t193706927 * L_2 = __this->get_compMaterial_33();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t2412809155_il2cpp_TypeInfo_var);
		Graphics_Blit_m4129959705(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightingSystem.HighlightingBase::.cctor()
extern Il2CppClass* HighlightingBase_t336099813_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* HashSet_1_t2817889127_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1__ctor_m1263110375_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3114507509;
extern Il2CppCodeGenString* _stringLiteral1500493360;
extern Il2CppCodeGenString* _stringLiteral3759933973;
extern Il2CppCodeGenString* _stringLiteral3499305740;
extern const uint32_t HighlightingBase__cctor_m2249983320_MetadataUsageId;
extern "C"  void HighlightingBase__cctor_m2249983320 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBase__cctor_m2249983320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m1909920690(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->set_colorClear_2(L_0);
		((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->set_renderBufferName_3(_stringLiteral3114507509);
		Matrix4x4_t2933234003  L_1 = Matrix4x4_get_identity_m3039560904(NULL /*static, unused*/, /*hidden argument*/NULL);
		((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->set_identityMatrix_4(L_1);
		((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->set_device_8(4);
		StringU5BU5D_t1642385972* L_2 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral1500493360);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1500493360);
		StringU5BU5D_t1642385972* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral3759933973);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3759933973);
		StringU5BU5D_t1642385972* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral3499305740);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3499305740);
		((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->set_shaderPaths_28(L_4);
		((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->set_initialized_34((bool)0);
		HashSet_1_t2817889127 * L_5 = (HashSet_1_t2817889127 *)il2cpp_codegen_object_new(HashSet_1_t2817889127_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m1263110375(L_5, /*hidden argument*/HashSet_1__ctor_m1263110375_MethodInfo_var);
		((HighlightingBase_t336099813_StaticFields*)HighlightingBase_t336099813_il2cpp_TypeInfo_var->static_fields)->set_cameras_35(L_5);
		return;
	}
}
// System.Void HighlightingSystem.HighlightingBlitter::.ctor()
extern Il2CppClass* List_1_t4000188241_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2322819785_MethodInfo_var;
extern const uint32_t HighlightingBlitter__ctor_m1718539186_MetadataUsageId;
extern "C"  void HighlightingBlitter__ctor_m1718539186 (HighlightingBlitter_t2949110578 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBlitter__ctor_m1718539186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4000188241 * L_0 = (List_1_t4000188241 *)il2cpp_codegen_object_new(List_1_t4000188241_il2cpp_TypeInfo_var);
		List_1__ctor_m2322819785(L_0, /*hidden argument*/List_1__ctor_m2322819785_MethodInfo_var);
		__this->set_renderers_2(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightingSystem.HighlightingBlitter::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Graphics_t2412809155_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m630595918_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2091795081_MethodInfo_var;
extern const uint32_t HighlightingBlitter_OnRenderImage_m132934086_MetadataUsageId;
extern "C"  void HighlightingBlitter_OnRenderImage_m132934086 (HighlightingBlitter_t2949110578 * __this, RenderTexture_t2666733923 * ___src0, RenderTexture_t2666733923 * ___dst1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBlitter_OnRenderImage_m132934086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	HighlightingBase_t336099813 * V_2 = NULL;
	{
		V_0 = (bool)1;
		V_1 = 0;
		goto IL_003a;
	}

IL_0009:
	{
		List_1_t4000188241 * L_0 = __this->get_renderers_2();
		int32_t L_1 = V_1;
		NullCheck(L_0);
		HighlightingBase_t336099813 * L_2 = List_1_get_Item_m630595918(L_0, L_1, /*hidden argument*/List_1_get_Item_m630595918_MethodInfo_var);
		V_2 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		HighlightingBase_t336099813 * L_4 = V_2;
		RenderTexture_t2666733923 * L_5 = ___src0;
		RenderTexture_t2666733923 * L_6 = ___dst1;
		NullCheck(L_4);
		VirtActionInvoker2< RenderTexture_t2666733923 *, RenderTexture_t2666733923 * >::Invoke(11 /* System.Void HighlightingSystem.HighlightingBase::Blit(UnityEngine.RenderTexture,UnityEngine.RenderTexture) */, L_4, L_5, L_6);
		goto IL_0031;
	}

IL_0029:
	{
		HighlightingBase_t336099813 * L_7 = V_2;
		RenderTexture_t2666733923 * L_8 = ___dst1;
		RenderTexture_t2666733923 * L_9 = ___src0;
		NullCheck(L_7);
		VirtActionInvoker2< RenderTexture_t2666733923 *, RenderTexture_t2666733923 * >::Invoke(11 /* System.Void HighlightingSystem.HighlightingBase::Blit(UnityEngine.RenderTexture,UnityEngine.RenderTexture) */, L_7, L_8, L_9);
	}

IL_0031:
	{
		bool L_10 = V_0;
		V_0 = (bool)((((int32_t)L_10) == ((int32_t)0))? 1 : 0);
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_12 = V_1;
		List_1_t4000188241 * L_13 = __this->get_renderers_2();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m2091795081(L_13, /*hidden argument*/List_1_get_Count_m2091795081_MethodInfo_var);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_0009;
		}
	}
	{
		bool L_15 = V_0;
		if (!L_15)
		{
			goto IL_0058;
		}
	}
	{
		RenderTexture_t2666733923 * L_16 = ___src0;
		RenderTexture_t2666733923 * L_17 = ___dst1;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t2412809155_il2cpp_TypeInfo_var);
		Graphics_Blit_m2123328641(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void HighlightingSystem.HighlightingBlitter::Register(HighlightingSystem.HighlightingBase)
extern const MethodInfo* List_1_Contains_m674258087_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3498863493_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2091795081_MethodInfo_var;
extern const uint32_t HighlightingBlitter_Register_m2013352037_MetadataUsageId;
extern "C"  void HighlightingBlitter_Register_m2013352037 (HighlightingBlitter_t2949110578 * __this, HighlightingBase_t336099813 * ___renderer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBlitter_Register_m2013352037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4000188241 * L_0 = __this->get_renderers_2();
		HighlightingBase_t336099813 * L_1 = ___renderer0;
		NullCheck(L_0);
		bool L_2 = List_1_Contains_m674258087(L_0, L_1, /*hidden argument*/List_1_Contains_m674258087_MethodInfo_var);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		List_1_t4000188241 * L_3 = __this->get_renderers_2();
		HighlightingBase_t336099813 * L_4 = ___renderer0;
		NullCheck(L_3);
		List_1_Add_m3498863493(L_3, L_4, /*hidden argument*/List_1_Add_m3498863493_MethodInfo_var);
	}

IL_001d:
	{
		List_1_t4000188241 * L_5 = __this->get_renderers_2();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m2091795081(L_5, /*hidden argument*/List_1_get_Count_m2091795081_MethodInfo_var);
		Behaviour_set_enabled_m1796096907(__this, (bool)((((int32_t)L_6) > ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightingSystem.HighlightingBlitter::Unregister(HighlightingSystem.HighlightingBase)
extern const MethodInfo* List_1_IndexOf_m3124325973_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m4217141627_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2091795081_MethodInfo_var;
extern const uint32_t HighlightingBlitter_Unregister_m3454378352_MetadataUsageId;
extern "C"  void HighlightingBlitter_Unregister_m3454378352 (HighlightingBlitter_t2949110578 * __this, HighlightingBase_t336099813 * ___renderer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingBlitter_Unregister_m3454378352_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t4000188241 * L_0 = __this->get_renderers_2();
		HighlightingBase_t336099813 * L_1 = ___renderer0;
		NullCheck(L_0);
		int32_t L_2 = List_1_IndexOf_m3124325973(L_0, L_1, /*hidden argument*/List_1_IndexOf_m3124325973_MethodInfo_var);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0020;
		}
	}
	{
		List_1_t4000188241 * L_4 = __this->get_renderers_2();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		List_1_RemoveAt_m4217141627(L_4, L_5, /*hidden argument*/List_1_RemoveAt_m4217141627_MethodInfo_var);
	}

IL_0020:
	{
		List_1_t4000188241 * L_6 = __this->get_renderers_2();
		NullCheck(L_6);
		int32_t L_7 = List_1_get_Count_m2091795081(L_6, /*hidden argument*/List_1_get_Count_m2091795081_MethodInfo_var);
		Behaviour_set_enabled_m1796096907(__this, (bool)((((int32_t)L_7) > ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighlightingSystem.HighlightingRenderer::.ctor()
extern Il2CppClass* HighlightingBase_t336099813_il2cpp_TypeInfo_var;
extern const uint32_t HighlightingRenderer__ctor_m1125277111_MetadataUsageId;
extern "C"  void HighlightingRenderer__ctor_m1125277111 (HighlightingRenderer_t3709628099 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighlightingRenderer__ctor_m1125277111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighlightingBase_t336099813_il2cpp_TypeInfo_var);
		HighlightingBase__ctor_m1751304723(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__MainTex()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__MainTex_m1457479119_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__MainTex_m1457479119 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__MainTex_m1457479119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_MainTexU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__MainTex(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__MainTex_m1532611808_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__MainTex_m1532611808 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__MainTex_m1532611808_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_MainTexU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__Color()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__Color_m865056110_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__Color_m865056110 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__Color_m865056110_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_ColorU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__Color(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__Color_m3471565491_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__Color_m3471565491 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__Color_m3471565491_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_ColorU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__Cutoff()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__Cutoff_m2480496912_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__Cutoff_m2480496912 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__Cutoff_m2480496912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_CutoffU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__Cutoff(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__Cutoff_m1758519467_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__Cutoff_m1758519467 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__Cutoff_m1758519467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_CutoffU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__Intensity()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__Intensity_m450774784_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__Intensity_m450774784 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__Intensity_m450774784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_IntensityU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__Intensity(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__Intensity_m313481257_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__Intensity_m313481257 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__Intensity_m313481257_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_IntensityU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__ZTest()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__ZTest_m2113714673_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__ZTest_m2113714673 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__ZTest_m2113714673_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_ZTestU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__ZTest(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__ZTest_m1602890520_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__ZTest_m1602890520 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__ZTest_m1602890520_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_ZTestU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__StencilRef()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__StencilRef_m1989750604_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__StencilRef_m1989750604 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__StencilRef_m1989750604_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_StencilRefU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__StencilRef(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__StencilRef_m3138321741_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__StencilRef_m3138321741 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__StencilRef_m3138321741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_StencilRefU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__Cull()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__Cull_m2367453659_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__Cull_m2367453659 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__Cull_m2367453659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_CullU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__Cull(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__Cull_m774099102_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__Cull_m774099102 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__Cull_m774099102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_CullU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__HighlightingBlur1()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__HighlightingBlur1_m3786131333_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__HighlightingBlur1_m3786131333 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__HighlightingBlur1_m3786131333_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_HighlightingBlur1U3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__HighlightingBlur1(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__HighlightingBlur1_m1484745726_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__HighlightingBlur1_m1484745726 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__HighlightingBlur1_m1484745726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_HighlightingBlur1U3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__HighlightingBlur2()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__HighlightingBlur2_m1319370004_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__HighlightingBlur2_m1319370004 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__HighlightingBlur2_m1319370004_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_HighlightingBlur2U3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__HighlightingBlur2(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__HighlightingBlur2_m1074813213_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__HighlightingBlur2_m1074813213 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__HighlightingBlur2_m1074813213_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_HighlightingBlur2U3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__HighlightingBuffer()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__HighlightingBuffer_m2889951321_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__HighlightingBuffer_m2889951321 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__HighlightingBuffer_m2889951321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_HighlightingBufferU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__HighlightingBuffer(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__HighlightingBuffer_m2174509396_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__HighlightingBuffer_m2174509396 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__HighlightingBuffer_m2174509396_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_HighlightingBufferU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__HighlightingBufferTexelSize()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__HighlightingBufferTexelSize_m3370777770_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__HighlightingBufferTexelSize_m3370777770 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__HighlightingBufferTexelSize_m3370777770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_HighlightingBufferTexelSizeU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__HighlightingBufferTexelSize(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__HighlightingBufferTexelSize_m172160907_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__HighlightingBufferTexelSize_m172160907 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__HighlightingBufferTexelSize_m172160907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_HighlightingBufferTexelSizeU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__HighlightingBlurred()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__HighlightingBlurred_m412791331_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__HighlightingBlurred_m412791331 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__HighlightingBlurred_m412791331_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_HighlightingBlurredU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__HighlightingBlurred(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__HighlightingBlurred_m4038023858_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__HighlightingBlurred_m4038023858 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__HighlightingBlurred_m4038023858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_HighlightingBlurredU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__HighlightingBlurOffset()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__HighlightingBlurOffset_m2757156113_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__HighlightingBlurOffset_m2757156113 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__HighlightingBlurOffset_m2757156113_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_HighlightingBlurOffsetU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__HighlightingBlurOffset(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__HighlightingBlurOffset_m3225589328_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__HighlightingBlurOffset_m3225589328 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__HighlightingBlurOffset_m3225589328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_HighlightingBlurOffsetU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__HighlightingZWrite()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__HighlightingZWrite_m3382839208_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__HighlightingZWrite_m3382839208 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__HighlightingZWrite_m3382839208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_HighlightingZWriteU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__HighlightingZWrite(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__HighlightingZWrite_m1255954357_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__HighlightingZWrite_m1255954357 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__HighlightingZWrite_m1255954357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_HighlightingZWriteU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__HighlightingOffsetFactor()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__HighlightingOffsetFactor_m808876897_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__HighlightingOffsetFactor_m808876897 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__HighlightingOffsetFactor_m808876897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_HighlightingOffsetFactorU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__HighlightingOffsetFactor(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__HighlightingOffsetFactor_m2941807670_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__HighlightingOffsetFactor_m2941807670 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__HighlightingOffsetFactor_m2941807670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_HighlightingOffsetFactorU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.Int32 HighlightingSystem.ShaderPropertyID::get__HighlightingOffsetUnits()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_get__HighlightingOffsetUnits_m2985827065_MetadataUsageId;
extern "C"  int32_t ShaderPropertyID_get__HighlightingOffsetUnits_m2985827065 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_get__HighlightingOffsetUnits_m2985827065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_U3C_HighlightingOffsetUnitsU3Ek__BackingField_15();
		return L_0;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::set__HighlightingOffsetUnits(System.Int32)
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern const uint32_t ShaderPropertyID_set__HighlightingOffsetUnits_m1811511252_MetadataUsageId;
extern "C"  void ShaderPropertyID_set__HighlightingOffsetUnits_m1811511252 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_set__HighlightingOffsetUnits_m1811511252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_U3C_HighlightingOffsetUnitsU3Ek__BackingField_15(L_0);
		return;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::Initialize()
extern Il2CppClass* ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4026354833;
extern Il2CppCodeGenString* _stringLiteral895546098;
extern Il2CppCodeGenString* _stringLiteral2307789290;
extern Il2CppCodeGenString* _stringLiteral1714609204;
extern Il2CppCodeGenString* _stringLiteral1866649955;
extern Il2CppCodeGenString* _stringLiteral2491597914;
extern Il2CppCodeGenString* _stringLiteral2543195893;
extern Il2CppCodeGenString* _stringLiteral3351955739;
extern Il2CppCodeGenString* _stringLiteral623072384;
extern Il2CppCodeGenString* _stringLiteral579991799;
extern Il2CppCodeGenString* _stringLiteral514036650;
extern Il2CppCodeGenString* _stringLiteral947482673;
extern Il2CppCodeGenString* _stringLiteral1329573579;
extern Il2CppCodeGenString* _stringLiteral4064670130;
extern Il2CppCodeGenString* _stringLiteral2215850691;
extern Il2CppCodeGenString* _stringLiteral3887975239;
extern const uint32_t ShaderPropertyID_Initialize_m2760510245_MetadataUsageId;
extern "C"  void ShaderPropertyID_Initialize_m2760510245 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShaderPropertyID_Initialize_m2760510245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		bool L_0 = ((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->get_initialized_16();
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		int32_t L_1 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral4026354833, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var);
		ShaderPropertyID_set__MainTex_m1532611808(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral895546098, /*hidden argument*/NULL);
		ShaderPropertyID_set__Color_m3471565491(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		int32_t L_3 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral2307789290, /*hidden argument*/NULL);
		ShaderPropertyID_set__Cutoff_m1758519467(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral1714609204, /*hidden argument*/NULL);
		ShaderPropertyID_set__Intensity_m313481257(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		int32_t L_5 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral1866649955, /*hidden argument*/NULL);
		ShaderPropertyID_set__ZTest_m1602890520(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		int32_t L_6 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral2491597914, /*hidden argument*/NULL);
		ShaderPropertyID_set__StencilRef_m3138321741(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_7 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral2543195893, /*hidden argument*/NULL);
		ShaderPropertyID_set__Cull_m774099102(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		int32_t L_8 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral3351955739, /*hidden argument*/NULL);
		ShaderPropertyID_set__HighlightingBlur1_m1484745726(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		int32_t L_9 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral623072384, /*hidden argument*/NULL);
		ShaderPropertyID_set__HighlightingBlur2_m1074813213(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		int32_t L_10 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral579991799, /*hidden argument*/NULL);
		ShaderPropertyID_set__HighlightingBuffer_m2174509396(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_11 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral514036650, /*hidden argument*/NULL);
		ShaderPropertyID_set__HighlightingBufferTexelSize_m172160907(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		int32_t L_12 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral947482673, /*hidden argument*/NULL);
		ShaderPropertyID_set__HighlightingBlurred_m4038023858(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		int32_t L_13 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral1329573579, /*hidden argument*/NULL);
		ShaderPropertyID_set__HighlightingBlurOffset_m3225589328(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		int32_t L_14 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral4064670130, /*hidden argument*/NULL);
		ShaderPropertyID_set__HighlightingZWrite_m1255954357(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		int32_t L_15 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral2215850691, /*hidden argument*/NULL);
		ShaderPropertyID_set__HighlightingOffsetFactor_m2941807670(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		int32_t L_16 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, _stringLiteral3887975239, /*hidden argument*/NULL);
		ShaderPropertyID_set__HighlightingOffsetUnits_m1811511252(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		((ShaderPropertyID_t1950236385_StaticFields*)ShaderPropertyID_t1950236385_il2cpp_TypeInfo_var->static_fields)->set_initialized_16((bool)1);
		return;
	}
}
// System.Void HighlightingSystem.ShaderPropertyID::.cctor()
extern "C"  void ShaderPropertyID__cctor_m3374834112 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
