﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIActions
struct  UIActions_t467790635  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject UIActions::leftCurtain
	GameObject_t1756533147 * ___leftCurtain_2;
	// UnityEngine.GameObject UIActions::leftCurtainReverse
	GameObject_t1756533147 * ___leftCurtainReverse_3;
	// UnityEngine.GameObject UIActions::rightCurtain
	GameObject_t1756533147 * ___rightCurtain_4;
	// UnityEngine.GameObject UIActions::rightCurtainReverse
	GameObject_t1756533147 * ___rightCurtainReverse_5;
	// UnityEngine.GameObject UIActions::curtainCloseSound
	GameObject_t1756533147 * ___curtainCloseSound_6;
	// UnityEngine.Animator UIActions::anim
	Animator_t69676727 * ___anim_7;

public:
	inline static int32_t get_offset_of_leftCurtain_2() { return static_cast<int32_t>(offsetof(UIActions_t467790635, ___leftCurtain_2)); }
	inline GameObject_t1756533147 * get_leftCurtain_2() const { return ___leftCurtain_2; }
	inline GameObject_t1756533147 ** get_address_of_leftCurtain_2() { return &___leftCurtain_2; }
	inline void set_leftCurtain_2(GameObject_t1756533147 * value)
	{
		___leftCurtain_2 = value;
		Il2CppCodeGenWriteBarrier(&___leftCurtain_2, value);
	}

	inline static int32_t get_offset_of_leftCurtainReverse_3() { return static_cast<int32_t>(offsetof(UIActions_t467790635, ___leftCurtainReverse_3)); }
	inline GameObject_t1756533147 * get_leftCurtainReverse_3() const { return ___leftCurtainReverse_3; }
	inline GameObject_t1756533147 ** get_address_of_leftCurtainReverse_3() { return &___leftCurtainReverse_3; }
	inline void set_leftCurtainReverse_3(GameObject_t1756533147 * value)
	{
		___leftCurtainReverse_3 = value;
		Il2CppCodeGenWriteBarrier(&___leftCurtainReverse_3, value);
	}

	inline static int32_t get_offset_of_rightCurtain_4() { return static_cast<int32_t>(offsetof(UIActions_t467790635, ___rightCurtain_4)); }
	inline GameObject_t1756533147 * get_rightCurtain_4() const { return ___rightCurtain_4; }
	inline GameObject_t1756533147 ** get_address_of_rightCurtain_4() { return &___rightCurtain_4; }
	inline void set_rightCurtain_4(GameObject_t1756533147 * value)
	{
		___rightCurtain_4 = value;
		Il2CppCodeGenWriteBarrier(&___rightCurtain_4, value);
	}

	inline static int32_t get_offset_of_rightCurtainReverse_5() { return static_cast<int32_t>(offsetof(UIActions_t467790635, ___rightCurtainReverse_5)); }
	inline GameObject_t1756533147 * get_rightCurtainReverse_5() const { return ___rightCurtainReverse_5; }
	inline GameObject_t1756533147 ** get_address_of_rightCurtainReverse_5() { return &___rightCurtainReverse_5; }
	inline void set_rightCurtainReverse_5(GameObject_t1756533147 * value)
	{
		___rightCurtainReverse_5 = value;
		Il2CppCodeGenWriteBarrier(&___rightCurtainReverse_5, value);
	}

	inline static int32_t get_offset_of_curtainCloseSound_6() { return static_cast<int32_t>(offsetof(UIActions_t467790635, ___curtainCloseSound_6)); }
	inline GameObject_t1756533147 * get_curtainCloseSound_6() const { return ___curtainCloseSound_6; }
	inline GameObject_t1756533147 ** get_address_of_curtainCloseSound_6() { return &___curtainCloseSound_6; }
	inline void set_curtainCloseSound_6(GameObject_t1756533147 * value)
	{
		___curtainCloseSound_6 = value;
		Il2CppCodeGenWriteBarrier(&___curtainCloseSound_6, value);
	}

	inline static int32_t get_offset_of_anim_7() { return static_cast<int32_t>(offsetof(UIActions_t467790635, ___anim_7)); }
	inline Animator_t69676727 * get_anim_7() const { return ___anim_7; }
	inline Animator_t69676727 ** get_address_of_anim_7() { return &___anim_7; }
	inline void set_anim_7(Animator_t69676727 * value)
	{
		___anim_7 = value;
		Il2CppCodeGenWriteBarrier(&___anim_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
