﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.Core.Demo.PanelsDropdown
struct PanelsDropdown_t24103620;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.Core.Demo.PanelsDropdown::.ctor()
extern "C"  void PanelsDropdown__ctor_m2375458779 (PanelsDropdown_t24103620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.Core.Demo.PanelsDropdown::Awake()
extern "C"  void PanelsDropdown_Awake_m2013534700 (PanelsDropdown_t24103620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.Core.Demo.PanelsDropdown::OnValueChanged()
extern "C"  void PanelsDropdown_OnValueChanged_m3202423231 (PanelsDropdown_t24103620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
