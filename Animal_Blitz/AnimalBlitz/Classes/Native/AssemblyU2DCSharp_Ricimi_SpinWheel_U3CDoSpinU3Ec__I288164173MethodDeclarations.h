﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.SpinWheel/<DoSpin>c__Iterator0
struct U3CDoSpinU3Ec__Iterator0_t288164173;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.SpinWheel/<DoSpin>c__Iterator0::.ctor()
extern "C"  void U3CDoSpinU3Ec__Iterator0__ctor_m1176735664 (U3CDoSpinU3Ec__Iterator0_t288164173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ricimi.SpinWheel/<DoSpin>c__Iterator0::MoveNext()
extern "C"  bool U3CDoSpinU3Ec__Iterator0_MoveNext_m348110688 (U3CDoSpinU3Ec__Iterator0_t288164173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Ricimi.SpinWheel/<DoSpin>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoSpinU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2061675838 (U3CDoSpinU3Ec__Iterator0_t288164173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Ricimi.SpinWheel/<DoSpin>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoSpinU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3825300694 (U3CDoSpinU3Ec__Iterator0_t288164173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.SpinWheel/<DoSpin>c__Iterator0::Dispose()
extern "C"  void U3CDoSpinU3Ec__Iterator0_Dispose_m1567853263 (U3CDoSpinU3Ec__Iterator0_t288164173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.SpinWheel/<DoSpin>c__Iterator0::Reset()
extern "C"  void U3CDoSpinU3Ec__Iterator0_Reset_m814553965 (U3CDoSpinU3Ec__Iterator0_t288164173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
