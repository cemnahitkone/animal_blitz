﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_DotPar2548494618MethodDeclarations.h"

// System.Void Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1<Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam>::.ctor()
#define SortedParamsList_1__ctor_m4113031997(__this, method) ((  void (*) (SortedParamsList_1_t3796711098 *, const MethodInfo*))SortedParamsList_1__ctor_m4109281813_gshared)(__this, method)
// System.Void Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1<Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam>::Init()
#define SortedParamsList_1_Init_m4130644079(__this, method) ((  void (*) (SortedParamsList_1_t3796711098 *, const MethodInfo*))SortedParamsList_1_Init_m2929646359_gshared)(__this, method)
// System.Void Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1<Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam>::Update()
#define SortedParamsList_1_Update_m4253241036(__this, method) ((  void (*) (SortedParamsList_1_t3796711098 *, const MethodInfo*))SortedParamsList_1_Update_m4026540864_gshared)(__this, method)
