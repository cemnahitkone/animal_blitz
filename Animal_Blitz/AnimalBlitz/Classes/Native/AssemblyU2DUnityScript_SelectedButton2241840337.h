﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectedButton
struct  SelectedButton_t2241840337  : public MonoBehaviour_t1158329972
{
public:
	// System.Single SelectedButton::animationStart
	float ___animationStart_2;
	// System.Int32 SelectedButton::currentFrame
	int32_t ___currentFrame_3;
	// System.Single SelectedButton::animationTime
	float ___animationTime_4;
	// System.Single SelectedButton::textureOffset
	float ___textureOffset_5;
	// System.Single SelectedButton::animationSpeed
	float ___animationSpeed_6;

public:
	inline static int32_t get_offset_of_animationStart_2() { return static_cast<int32_t>(offsetof(SelectedButton_t2241840337, ___animationStart_2)); }
	inline float get_animationStart_2() const { return ___animationStart_2; }
	inline float* get_address_of_animationStart_2() { return &___animationStart_2; }
	inline void set_animationStart_2(float value)
	{
		___animationStart_2 = value;
	}

	inline static int32_t get_offset_of_currentFrame_3() { return static_cast<int32_t>(offsetof(SelectedButton_t2241840337, ___currentFrame_3)); }
	inline int32_t get_currentFrame_3() const { return ___currentFrame_3; }
	inline int32_t* get_address_of_currentFrame_3() { return &___currentFrame_3; }
	inline void set_currentFrame_3(int32_t value)
	{
		___currentFrame_3 = value;
	}

	inline static int32_t get_offset_of_animationTime_4() { return static_cast<int32_t>(offsetof(SelectedButton_t2241840337, ___animationTime_4)); }
	inline float get_animationTime_4() const { return ___animationTime_4; }
	inline float* get_address_of_animationTime_4() { return &___animationTime_4; }
	inline void set_animationTime_4(float value)
	{
		___animationTime_4 = value;
	}

	inline static int32_t get_offset_of_textureOffset_5() { return static_cast<int32_t>(offsetof(SelectedButton_t2241840337, ___textureOffset_5)); }
	inline float get_textureOffset_5() const { return ___textureOffset_5; }
	inline float* get_address_of_textureOffset_5() { return &___textureOffset_5; }
	inline void set_textureOffset_5(float value)
	{
		___textureOffset_5 = value;
	}

	inline static int32_t get_offset_of_animationSpeed_6() { return static_cast<int32_t>(offsetof(SelectedButton_t2241840337, ___animationSpeed_6)); }
	inline float get_animationSpeed_6() const { return ___animationSpeed_6; }
	inline float* get_address_of_animationSpeed_6() { return &___animationSpeed_6; }
	inline void set_animationSpeed_6(float value)
	{
		___animationSpeed_6 = value;
	}
};

struct SelectedButton_t2241840337_StaticFields
{
public:
	// System.Int32 SelectedButton::maxFrame
	int32_t ___maxFrame_7;

public:
	inline static int32_t get_offset_of_maxFrame_7() { return static_cast<int32_t>(offsetof(SelectedButton_t2241840337_StaticFields, ___maxFrame_7)); }
	inline int32_t get_maxFrame_7() const { return ___maxFrame_7; }
	inline int32_t* get_address_of_maxFrame_7() { return &___maxFrame_7; }
	inline void set_maxFrame_7(int32_t value)
	{
		___maxFrame_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
