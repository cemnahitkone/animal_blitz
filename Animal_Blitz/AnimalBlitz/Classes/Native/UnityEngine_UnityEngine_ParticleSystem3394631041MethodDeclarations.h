﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t574222242;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1612828713;
// System.Object
struct Il2CppObject;
// UnityEngine.ParticleSystem/IteratorDelegate
struct IteratorDelegate_t2419492168;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_ParticleSystemSimulationSp3554150940.h"
#include "UnityEngine_UnityEngine_ParticleSystemScalingMode366697231.h"
#include "UnityEngine_UnityEngine_ParticleSystem_MainModule6751348.h"
#include "UnityEngine_UnityEngine_ParticleSystem_EmissionMod2748003162.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ShapeModule2888832346.h"
#include "UnityEngine_UnityEngine_ParticleSystem_VelocityOve2506587731.h"
#include "UnityEngine_UnityEngine_ParticleSystem_LimitVeloci1878180938.h"
#include "UnityEngine_UnityEngine_ParticleSystem_InheritVelo1475438411.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ForceOverLi3255358877.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ColorOverLif474557797.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ColorBySpee3282895500.h"
#include "UnityEngine_UnityEngine_ParticleSystem_SizeOverLif4234313961.h"
#include "UnityEngine_UnityEngine_ParticleSystem_SizeBySpeed1187519164.h"
#include "UnityEngine_UnityEngine_ParticleSystem_RotationOve2204422354.h"
#include "UnityEngine_UnityEngine_ParticleSystem_RotationBySp538896421.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ExternalFor2596349650.h"
#include "UnityEngine_UnityEngine_ParticleSystem_NoiseModule1635569407.h"
#include "UnityEngine_UnityEngine_ParticleSystem_CollisionMo2408302831.h"
#include "UnityEngine_UnityEngine_ParticleSystem_TriggerModu2774521729.h"
#include "UnityEngine_UnityEngine_ParticleSystem_SubEmitters3103792650.h"
#include "UnityEngine_UnityEngine_ParticleSystem_TextureShee4262561859.h"
#include "UnityEngine_UnityEngine_ParticleSystem_LightsModul3164805776.h"
#include "UnityEngine_UnityEngine_ParticleSystem_TrailModule3782588419.h"
#include "UnityEngine_UnityEngine_ParticleSystemCustomData2750547156.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"
#include "UnityEngine_UnityEngine_ParticleSystemStopBehavior3921148531.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "UnityEngine_UnityEngine_ParticleSystem_Particle250075699.h"
#include "UnityEngine_UnityEngine_ParticleSystem_EmitParams1656680080.h"
#include "UnityEngine_UnityEngine_ParticleSystem_IteratorDel2419492168.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void UnityEngine.ParticleSystem::.ctor()
extern "C"  void ParticleSystem__ctor_m1373939944 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem::get_startDelay()
extern "C"  float ParticleSystem_get_startDelay_m3593315006 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_startDelay(System.Single)
extern "C"  void ParticleSystem_set_startDelay_m2886959681 (ParticleSystem_t3394631041 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::get_isPlaying()
extern "C"  bool ParticleSystem_get_isPlaying_m1575756079 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::get_isEmitting()
extern "C"  bool ParticleSystem_get_isEmitting_m460483652 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::get_isStopped()
extern "C"  bool ParticleSystem_get_isStopped_m700577998 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::get_isPaused()
extern "C"  bool ParticleSystem_get_isPaused_m3736903421 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::get_loop()
extern "C"  bool ParticleSystem_get_loop_m2479798537 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_loop(System.Boolean)
extern "C"  void ParticleSystem_set_loop_m2338499320 (ParticleSystem_t3394631041 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::get_playOnAwake()
extern "C"  bool ParticleSystem_get_playOnAwake_m2809574389 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_playOnAwake(System.Boolean)
extern "C"  void ParticleSystem_set_playOnAwake_m2176286056 (ParticleSystem_t3394631041 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem::get_time()
extern "C"  float ParticleSystem_get_time_m2334824140 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_time(System.Single)
extern "C"  void ParticleSystem_set_time_m1769247049 (ParticleSystem_t3394631041 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem::get_duration()
extern "C"  float ParticleSystem_get_duration_m2930894537 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem::get_playbackSpeed()
extern "C"  float ParticleSystem_get_playbackSpeed_m2164888763 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_playbackSpeed(System.Single)
extern "C"  void ParticleSystem_set_playbackSpeed_m3784435056 (ParticleSystem_t3394631041 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem::get_particleCount()
extern "C"  int32_t ParticleSystem_get_particleCount_m3737254194 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::get_enableEmission()
extern "C"  bool ParticleSystem_get_enableEmission_m3389363989 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_enableEmission(System.Boolean)
extern "C"  void ParticleSystem_set_enableEmission_m1222323446 (ParticleSystem_t3394631041 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem::get_emissionRate()
extern "C"  float ParticleSystem_get_emissionRate_m1144353272 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_emissionRate(System.Single)
extern "C"  void ParticleSystem_set_emissionRate_m2482228887 (ParticleSystem_t3394631041 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem::get_startSpeed()
extern "C"  float ParticleSystem_get_startSpeed_m1416474346 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_startSpeed(System.Single)
extern "C"  void ParticleSystem_set_startSpeed_m4170689111 (ParticleSystem_t3394631041 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem::get_startSize()
extern "C"  float ParticleSystem_get_startSize_m3327114244 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_startSize(System.Single)
extern "C"  void ParticleSystem_set_startSize_m657274193 (ParticleSystem_t3394631041 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.ParticleSystem::get_startColor()
extern "C"  Color_t2020392075  ParticleSystem_get_startColor_m3109096765 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_startColor(UnityEngine.Color)
extern "C"  void ParticleSystem_set_startColor_m4138687636 (ParticleSystem_t3394631041 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::INTERNAL_get_startColor(UnityEngine.Color&)
extern "C"  void ParticleSystem_INTERNAL_get_startColor_m175059240 (ParticleSystem_t3394631041 * __this, Color_t2020392075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::INTERNAL_set_startColor(UnityEngine.Color&)
extern "C"  void ParticleSystem_INTERNAL_set_startColor_m3832470796 (ParticleSystem_t3394631041 * __this, Color_t2020392075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem::get_startRotation()
extern "C"  float ParticleSystem_get_startRotation_m4268836835 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_startRotation(System.Single)
extern "C"  void ParticleSystem_set_startRotation_m669746994 (ParticleSystem_t3394631041 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ParticleSystem::get_startRotation3D()
extern "C"  Vector3_t2243707580  ParticleSystem_get_startRotation3D_m1707426368 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_startRotation3D(UnityEngine.Vector3)
extern "C"  void ParticleSystem_set_startRotation3D_m1497716127 (ParticleSystem_t3394631041 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::INTERNAL_get_startRotation3D(UnityEngine.Vector3&)
extern "C"  void ParticleSystem_INTERNAL_get_startRotation3D_m1381512529 (ParticleSystem_t3394631041 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::INTERNAL_set_startRotation3D(UnityEngine.Vector3&)
extern "C"  void ParticleSystem_INTERNAL_set_startRotation3D_m1056222829 (ParticleSystem_t3394631041 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem::get_startLifetime()
extern "C"  float ParticleSystem_get_startLifetime_m70433814 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_startLifetime(System.Single)
extern "C"  void ParticleSystem_set_startLifetime_m1743610511 (ParticleSystem_t3394631041 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem::get_gravityModifier()
extern "C"  float ParticleSystem_get_gravityModifier_m204082082 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_gravityModifier(System.Single)
extern "C"  void ParticleSystem_set_gravityModifier_m3543138761 (ParticleSystem_t3394631041 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem::get_maxParticles()
extern "C"  int32_t ParticleSystem_get_maxParticles_m3216658554 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_maxParticles(System.Int32)
extern "C"  void ParticleSystem_set_maxParticles_m1689482563 (ParticleSystem_t3394631041 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystemSimulationSpace UnityEngine.ParticleSystem::get_simulationSpace()
extern "C"  int32_t ParticleSystem_get_simulationSpace_m3869591304 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_simulationSpace(UnityEngine.ParticleSystemSimulationSpace)
extern "C"  void ParticleSystem_set_simulationSpace_m3350576231 (ParticleSystem_t3394631041 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystemScalingMode UnityEngine.ParticleSystem::get_scalingMode()
extern "C"  int32_t ParticleSystem_get_scalingMode_m1820638792 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_scalingMode(UnityEngine.ParticleSystemScalingMode)
extern "C"  void ParticleSystem_set_scalingMode_m4252548423 (ParticleSystem_t3394631041 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UnityEngine.ParticleSystem::get_randomSeed()
extern "C"  uint32_t ParticleSystem_get_randomSeed_m1645200496 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_randomSeed(System.UInt32)
extern "C"  void ParticleSystem_set_randomSeed_m426458673 (ParticleSystem_t3394631041 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::get_useAutoRandomSeed()
extern "C"  bool ParticleSystem_get_useAutoRandomSeed_m149101129 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::set_useAutoRandomSeed(System.Boolean)
extern "C"  void ParticleSystem_set_useAutoRandomSeed_m2795079044 (ParticleSystem_t3394631041 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
extern "C"  MainModule_t6751348  ParticleSystem_get_main_m1338387869 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
extern "C"  EmissionModule_t2748003162  ParticleSystem_get_emission_m3968992617 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/ShapeModule UnityEngine.ParticleSystem::get_shape()
extern "C"  ShapeModule_t2888832346  ParticleSystem_get_shape_m1960653537 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/VelocityOverLifetimeModule UnityEngine.ParticleSystem::get_velocityOverLifetime()
extern "C"  VelocityOverLifetimeModule_t2506587731  ParticleSystem_get_velocityOverLifetime_m3832874265 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule UnityEngine.ParticleSystem::get_limitVelocityOverLifetime()
extern "C"  LimitVelocityOverLifetimeModule_t1878180938  ParticleSystem_get_limitVelocityOverLifetime_m2424509281 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/InheritVelocityModule UnityEngine.ParticleSystem::get_inheritVelocity()
extern "C"  InheritVelocityModule_t1475438411  ParticleSystem_get_inheritVelocity_m274222913 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/ForceOverLifetimeModule UnityEngine.ParticleSystem::get_forceOverLifetime()
extern "C"  ForceOverLifetimeModule_t3255358877  ParticleSystem_get_forceOverLifetime_m3878039969 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/ColorOverLifetimeModule UnityEngine.ParticleSystem::get_colorOverLifetime()
extern "C"  ColorOverLifetimeModule_t474557797  ParticleSystem_get_colorOverLifetime_m1614521377 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/ColorBySpeedModule UnityEngine.ParticleSystem::get_colorBySpeed()
extern "C"  ColorBySpeedModule_t3282895500  ParticleSystem_get_colorBySpeed_m2717246953 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/SizeOverLifetimeModule UnityEngine.ParticleSystem::get_sizeOverLifetime()
extern "C"  SizeOverLifetimeModule_t4234313961  ParticleSystem_get_sizeOverLifetime_m1820862565 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/SizeBySpeedModule UnityEngine.ParticleSystem::get_sizeBySpeed()
extern "C"  SizeBySpeedModule_t1187519164  ParticleSystem_get_sizeBySpeed_m541399137 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/RotationOverLifetimeModule UnityEngine.ParticleSystem::get_rotationOverLifetime()
extern "C"  RotationOverLifetimeModule_t2204422354  ParticleSystem_get_rotationOverLifetime_m2510938025 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/RotationBySpeedModule UnityEngine.ParticleSystem::get_rotationBySpeed()
extern "C"  RotationBySpeedModule_t538896421  ParticleSystem_get_rotationBySpeed_m3007790657 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/ExternalForcesModule UnityEngine.ParticleSystem::get_externalForces()
extern "C"  ExternalForcesModule_t2596349650  ParticleSystem_get_externalForces_m816712989 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/NoiseModule UnityEngine.ParticleSystem::get_noise()
extern "C"  NoiseModule_t1635569407  ParticleSystem_get_noise_m332202977 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/CollisionModule UnityEngine.ParticleSystem::get_collision()
extern "C"  CollisionModule_t2408302831  ParticleSystem_get_collision_m3197647009 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/TriggerModule UnityEngine.ParticleSystem::get_trigger()
extern "C"  TriggerModule_t2774521729  ParticleSystem_get_trigger_m2684565121 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/SubEmittersModule UnityEngine.ParticleSystem::get_subEmitters()
extern "C"  SubEmittersModule_t3103792650  ParticleSystem_get_subEmitters_m3716512033 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/TextureSheetAnimationModule UnityEngine.ParticleSystem::get_textureSheetAnimation()
extern "C"  TextureSheetAnimationModule_t4262561859  ParticleSystem_get_textureSheetAnimation_m1891398433 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/LightsModule UnityEngine.ParticleSystem::get_lights()
extern "C"  LightsModule_t3164805776  ParticleSystem_get_lights_m2029104929 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/TrailModule UnityEngine.ParticleSystem::get_trails()
extern "C"  TrailModule_t3782588419  ParticleSystem_get_trails_m3982407818 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)
extern "C"  void ParticleSystem_SetParticles_m3035584975 (ParticleSystem_t3394631041 * __this, ParticleU5BU5D_t574222242* ___particles0, int32_t ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[])
extern "C"  int32_t ParticleSystem_GetParticles_m1903763264 (ParticleSystem_t3394631041 * __this, ParticleU5BU5D_t574222242* ___particles0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::SetCustomParticleData(System.Collections.Generic.List`1<UnityEngine.Vector4>,UnityEngine.ParticleSystemCustomData)
extern "C"  void ParticleSystem_SetCustomParticleData_m1736703144 (ParticleSystem_t3394631041 * __this, List_1_t1612828713 * ___customData0, int32_t ___streamIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem::GetCustomParticleData(System.Collections.Generic.List`1<UnityEngine.Vector4>,UnityEngine.ParticleSystemCustomData)
extern "C"  int32_t ParticleSystem_GetCustomParticleData_m3251377140 (ParticleSystem_t3394631041 * __this, List_1_t1612828713 * ___customData0, int32_t ___streamIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::SetCustomParticleDataInternal(System.Object,System.Int32)
extern "C"  void ParticleSystem_SetCustomParticleDataInternal_m1200705033 (ParticleSystem_t3394631041 * __this, Il2CppObject * ___customData0, int32_t ___streamIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem::GetCustomParticleDataInternal(System.Object,System.Int32)
extern "C"  int32_t ParticleSystem_GetCustomParticleDataInternal_m2619849551 (ParticleSystem_t3394631041 * __this, Il2CppObject * ___customData0, int32_t ___streamIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::Internal_Simulate(UnityEngine.ParticleSystem,System.Single,System.Boolean,System.Boolean)
extern "C"  bool ParticleSystem_Internal_Simulate_m1424460889 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___self0, float ___t1, bool ___restart2, bool ___fixedTimeStep3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::Internal_Play(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_Play_m2372735108 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::Internal_Stop(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemStopBehavior)
extern "C"  bool ParticleSystem_Internal_Stop_m3380666254 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___self0, int32_t ___stopBehavior1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::Internal_Pause(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_Pause_m1429799530 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::Internal_Clear(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_Clear_m661664011 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::Internal_IsAlive(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_IsAlive_m1667610959 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean)
extern "C"  void ParticleSystem_Simulate_m1157362631 (ParticleSystem_t3394631041 * __this, float ___t0, bool ___withChildren1, bool ___restart2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean)
extern "C"  void ParticleSystem_Simulate_m520229624 (ParticleSystem_t3394631041 * __this, float ___t0, bool ___withChildren1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single)
extern "C"  void ParticleSystem_Simulate_m645369211 (ParticleSystem_t3394631041 * __this, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void ParticleSystem_Simulate_m3186727082 (ParticleSystem_t3394631041 * __this, float ___t0, bool ___withChildren1, bool ___restart2, bool ___fixedTimeStep3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Play()
extern "C"  void ParticleSystem_Play_m4171585816 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern "C"  void ParticleSystem_Play_m1705837075 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean)
extern "C"  void ParticleSystem_Stop_m1901765691 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Stop()
extern "C"  void ParticleSystem_Stop_m941760450 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern "C"  void ParticleSystem_Stop_m3810114649 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, int32_t ___stopBehavior1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Pause()
extern "C"  void ParticleSystem_Pause_m1948769374 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Pause(System.Boolean)
extern "C"  void ParticleSystem_Pause_m2341216801 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Clear()
extern "C"  void ParticleSystem_Clear_m550005313 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Clear(System.Boolean)
extern "C"  void ParticleSystem_Clear_m4048064080 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::IsAlive()
extern "C"  bool ParticleSystem_IsAlive_m2418268213 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
extern "C"  bool ParticleSystem_IsAlive_m2793794644 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Emit(System.Int32)
extern "C"  void ParticleSystem_Emit_m649892508 (ParticleSystem_t3394631041 * __this, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::INTERNAL_CALL_Emit(UnityEngine.ParticleSystem,System.Int32)
extern "C"  void ParticleSystem_INTERNAL_CALL_Emit_m3734159159 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___self0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.Color32)
extern "C"  void ParticleSystem_Emit_m3918560584 (ParticleSystem_t3394631041 * __this, Vector3_t2243707580  ___position0, Vector3_t2243707580  ___velocity1, float ___size2, float ___lifetime3, Color32_t874517518  ___color4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/Particle)
extern "C"  void ParticleSystem_Emit_m2157207796 (ParticleSystem_t3394631041 * __this, Particle_t250075699  ___particle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Internal_EmitOld(UnityEngine.ParticleSystem/Particle&)
extern "C"  void ParticleSystem_Internal_EmitOld_m2905194281 (ParticleSystem_t3394631041 * __this, Particle_t250075699 * ___particle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/EmitParams,System.Int32)
extern "C"  void ParticleSystem_Emit_m2916854584 (ParticleSystem_t3394631041 * __this, EmitParams_t1656680080  ___emitParams0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Internal_Emit(UnityEngine.ParticleSystem/EmitParams&,System.Int32)
extern "C"  void ParticleSystem_Internal_Emit_m2721642954 (ParticleSystem_t3394631041 * __this, EmitParams_t1656680080 * ___emitParams0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::IterateParticleSystems(System.Boolean,UnityEngine.ParticleSystem/IteratorDelegate)
extern "C"  bool ParticleSystem_IterateParticleSystems_m1240416587 (ParticleSystem_t3394631041 * __this, bool ___recurse0, IteratorDelegate_t2419492168 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::IterateParticleSystemsRecursive(UnityEngine.Transform,UnityEngine.ParticleSystem/IteratorDelegate)
extern "C"  bool ParticleSystem_IterateParticleSystemsRecursive_m3260878897 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___transform0, IteratorDelegate_t2419492168 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::<Play>m__0(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_U3CPlayU3Em__0_m4250446697 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::<Pause>m__1(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_U3CPauseU3Em__1_m2033888636 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::<Clear>m__2(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_U3CClearU3Em__2_m1781329972 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::<IsAlive>m__3(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_U3CIsAliveU3Em__3_m4195117235 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
