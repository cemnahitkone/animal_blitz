﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.PopupOpener
struct PopupOpener_t3649718601;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.PopupOpener::.ctor()
extern "C"  void PopupOpener__ctor_m3198610533 (PopupOpener_t3649718601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.PopupOpener::Start()
extern "C"  void PopupOpener_Start_m2619033565 (PopupOpener_t3649718601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.PopupOpener::OpenPopup()
extern "C"  void PopupOpener_OpenPopup_m2343845615 (PopupOpener_t3649718601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
