﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_SmoothMouseLook_RotationAxes1370738632.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmoothMouseLook
struct  SmoothMouseLook_t128928814  : public MonoBehaviour_t1158329972
{
public:
	// SmoothMouseLook/RotationAxes SmoothMouseLook::axes
	int32_t ___axes_2;
	// System.Single SmoothMouseLook::SensitivityX
	float ___SensitivityX_3;
	// System.Single SmoothMouseLook::SensitivityY
	float ___SensitivityY_4;
	// System.Single SmoothMouseLook::MinimumX
	float ___MinimumX_5;
	// System.Single SmoothMouseLook::MaximumX
	float ___MaximumX_6;
	// System.Single SmoothMouseLook::MinimumY
	float ___MinimumY_7;
	// System.Single SmoothMouseLook::MaximumY
	float ___MaximumY_8;
	// System.Single SmoothMouseLook::FrameCounter
	float ___FrameCounter_9;
	// System.Single SmoothMouseLook::_rotationX
	float ____rotationX_10;
	// System.Single SmoothMouseLook::_rotationY
	float ____rotationY_11;
	// System.Collections.Generic.List`1<System.Single> SmoothMouseLook::_rotArrayX
	List_1_t1445631064 * ____rotArrayX_12;
	// System.Collections.Generic.List`1<System.Single> SmoothMouseLook::_rotArrayY
	List_1_t1445631064 * ____rotArrayY_13;
	// System.Single SmoothMouseLook::_rotAverageX
	float ____rotAverageX_14;
	// System.Single SmoothMouseLook::_rotAverageY
	float ____rotAverageY_15;
	// UnityEngine.Quaternion SmoothMouseLook::_originalRotation
	Quaternion_t4030073918  ____originalRotation_16;
	// UnityEngine.Quaternion SmoothMouseLook::_parentOriginalRotation
	Quaternion_t4030073918  ____parentOriginalRotation_17;

public:
	inline static int32_t get_offset_of_axes_2() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ___axes_2)); }
	inline int32_t get_axes_2() const { return ___axes_2; }
	inline int32_t* get_address_of_axes_2() { return &___axes_2; }
	inline void set_axes_2(int32_t value)
	{
		___axes_2 = value;
	}

	inline static int32_t get_offset_of_SensitivityX_3() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ___SensitivityX_3)); }
	inline float get_SensitivityX_3() const { return ___SensitivityX_3; }
	inline float* get_address_of_SensitivityX_3() { return &___SensitivityX_3; }
	inline void set_SensitivityX_3(float value)
	{
		___SensitivityX_3 = value;
	}

	inline static int32_t get_offset_of_SensitivityY_4() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ___SensitivityY_4)); }
	inline float get_SensitivityY_4() const { return ___SensitivityY_4; }
	inline float* get_address_of_SensitivityY_4() { return &___SensitivityY_4; }
	inline void set_SensitivityY_4(float value)
	{
		___SensitivityY_4 = value;
	}

	inline static int32_t get_offset_of_MinimumX_5() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ___MinimumX_5)); }
	inline float get_MinimumX_5() const { return ___MinimumX_5; }
	inline float* get_address_of_MinimumX_5() { return &___MinimumX_5; }
	inline void set_MinimumX_5(float value)
	{
		___MinimumX_5 = value;
	}

	inline static int32_t get_offset_of_MaximumX_6() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ___MaximumX_6)); }
	inline float get_MaximumX_6() const { return ___MaximumX_6; }
	inline float* get_address_of_MaximumX_6() { return &___MaximumX_6; }
	inline void set_MaximumX_6(float value)
	{
		___MaximumX_6 = value;
	}

	inline static int32_t get_offset_of_MinimumY_7() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ___MinimumY_7)); }
	inline float get_MinimumY_7() const { return ___MinimumY_7; }
	inline float* get_address_of_MinimumY_7() { return &___MinimumY_7; }
	inline void set_MinimumY_7(float value)
	{
		___MinimumY_7 = value;
	}

	inline static int32_t get_offset_of_MaximumY_8() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ___MaximumY_8)); }
	inline float get_MaximumY_8() const { return ___MaximumY_8; }
	inline float* get_address_of_MaximumY_8() { return &___MaximumY_8; }
	inline void set_MaximumY_8(float value)
	{
		___MaximumY_8 = value;
	}

	inline static int32_t get_offset_of_FrameCounter_9() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ___FrameCounter_9)); }
	inline float get_FrameCounter_9() const { return ___FrameCounter_9; }
	inline float* get_address_of_FrameCounter_9() { return &___FrameCounter_9; }
	inline void set_FrameCounter_9(float value)
	{
		___FrameCounter_9 = value;
	}

	inline static int32_t get_offset_of__rotationX_10() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ____rotationX_10)); }
	inline float get__rotationX_10() const { return ____rotationX_10; }
	inline float* get_address_of__rotationX_10() { return &____rotationX_10; }
	inline void set__rotationX_10(float value)
	{
		____rotationX_10 = value;
	}

	inline static int32_t get_offset_of__rotationY_11() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ____rotationY_11)); }
	inline float get__rotationY_11() const { return ____rotationY_11; }
	inline float* get_address_of__rotationY_11() { return &____rotationY_11; }
	inline void set__rotationY_11(float value)
	{
		____rotationY_11 = value;
	}

	inline static int32_t get_offset_of__rotArrayX_12() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ____rotArrayX_12)); }
	inline List_1_t1445631064 * get__rotArrayX_12() const { return ____rotArrayX_12; }
	inline List_1_t1445631064 ** get_address_of__rotArrayX_12() { return &____rotArrayX_12; }
	inline void set__rotArrayX_12(List_1_t1445631064 * value)
	{
		____rotArrayX_12 = value;
		Il2CppCodeGenWriteBarrier(&____rotArrayX_12, value);
	}

	inline static int32_t get_offset_of__rotArrayY_13() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ____rotArrayY_13)); }
	inline List_1_t1445631064 * get__rotArrayY_13() const { return ____rotArrayY_13; }
	inline List_1_t1445631064 ** get_address_of__rotArrayY_13() { return &____rotArrayY_13; }
	inline void set__rotArrayY_13(List_1_t1445631064 * value)
	{
		____rotArrayY_13 = value;
		Il2CppCodeGenWriteBarrier(&____rotArrayY_13, value);
	}

	inline static int32_t get_offset_of__rotAverageX_14() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ____rotAverageX_14)); }
	inline float get__rotAverageX_14() const { return ____rotAverageX_14; }
	inline float* get_address_of__rotAverageX_14() { return &____rotAverageX_14; }
	inline void set__rotAverageX_14(float value)
	{
		____rotAverageX_14 = value;
	}

	inline static int32_t get_offset_of__rotAverageY_15() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ____rotAverageY_15)); }
	inline float get__rotAverageY_15() const { return ____rotAverageY_15; }
	inline float* get_address_of__rotAverageY_15() { return &____rotAverageY_15; }
	inline void set__rotAverageY_15(float value)
	{
		____rotAverageY_15 = value;
	}

	inline static int32_t get_offset_of__originalRotation_16() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ____originalRotation_16)); }
	inline Quaternion_t4030073918  get__originalRotation_16() const { return ____originalRotation_16; }
	inline Quaternion_t4030073918 * get_address_of__originalRotation_16() { return &____originalRotation_16; }
	inline void set__originalRotation_16(Quaternion_t4030073918  value)
	{
		____originalRotation_16 = value;
	}

	inline static int32_t get_offset_of__parentOriginalRotation_17() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t128928814, ____parentOriginalRotation_17)); }
	inline Quaternion_t4030073918  get__parentOriginalRotation_17() const { return ____parentOriginalRotation_17; }
	inline Quaternion_t4030073918 * get_address_of__parentOriginalRotation_17() { return &____parentOriginalRotation_17; }
	inline void set__parentOriginalRotation_17(Quaternion_t4030073918  value)
	{
		____parentOriginalRotation_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
