﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.DotParams.MoonParam
struct MoonParam_t2066856486;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.DotParams.MoonParam::.ctor()
extern "C"  void MoonParam__ctor_m342178342 (MoonParam_t2066856486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
