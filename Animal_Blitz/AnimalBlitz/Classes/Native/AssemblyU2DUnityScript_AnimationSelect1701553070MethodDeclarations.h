﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationSelect
struct AnimationSelect_t1701553070;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void AnimationSelect::.ctor()
extern "C"  void AnimationSelect__ctor_m2218473122 (AnimationSelect_t1701553070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationSelect::Start()
extern "C"  void AnimationSelect_Start_m4276554258 (AnimationSelect_t1701553070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationSelect::Update()
extern "C"  void AnimationSelect_Update_m304034889 (AnimationSelect_t1701553070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationSelect::BlendAllCharactersToZero()
extern "C"  void AnimationSelect_BlendAllCharactersToZero_m2146318859 (AnimationSelect_t1701553070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationSelect::BlendAllToZero(UnityEngine.GameObject)
extern "C"  void AnimationSelect_BlendAllToZero_m2855194785 (AnimationSelect_t1701553070 * __this, GameObject_t1756533147 * ___character0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationSelect::Main()
extern "C"  void AnimationSelect_Main_m3430688715 (AnimationSelect_t1701553070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
