﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.Core.Demo.ForegroundToggle
struct ForegroundToggle_t2374938513;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.Core.Demo.ForegroundToggle::.ctor()
extern "C"  void ForegroundToggle__ctor_m3316207162 (ForegroundToggle_t2374938513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.Core.Demo.ForegroundToggle::OnValueChanged(System.Boolean)
extern "C"  void ForegroundToggle_OnValueChanged_m1086374395 (ForegroundToggle_t2374938513 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
