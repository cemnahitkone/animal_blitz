﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Action
struct Action_t3226471752;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`2<System.String,System.String>
struct Action_2_t4234541925;
// System.Action`4<System.String,System.String,System.Int32,System.Byte[]>
struct Action_4_t2314457268;
// System.Action`2<System.String,System.Byte[]>
struct Action_2_t1307688409;
// System.Action`3<System.String,System.String,System.String>
struct Action_3_t3256166369;
// System.Action`3<System.String,System.String,System.Byte[]>
struct Action_3_t329312853;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BluetoothDeviceScript
struct  BluetoothDeviceScript_t810775987  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<System.String> BluetoothDeviceScript::DiscoveredDeviceList
	List_1_t1398341365 * ___DiscoveredDeviceList_2;
	// System.Action BluetoothDeviceScript::InitializedAction
	Action_t3226471752 * ___InitializedAction_3;
	// System.Action BluetoothDeviceScript::DeinitializedAction
	Action_t3226471752 * ___DeinitializedAction_4;
	// System.Action`1<System.String> BluetoothDeviceScript::ErrorAction
	Action_1_t1831019615 * ___ErrorAction_5;
	// System.Action`1<System.String> BluetoothDeviceScript::ServiceAddedAction
	Action_1_t1831019615 * ___ServiceAddedAction_6;
	// System.Action BluetoothDeviceScript::StartedAdvertisingAction
	Action_t3226471752 * ___StartedAdvertisingAction_7;
	// System.Action BluetoothDeviceScript::StoppedAdvertisingAction
	Action_t3226471752 * ___StoppedAdvertisingAction_8;
	// System.Action`2<System.String,System.String> BluetoothDeviceScript::DiscoveredPeripheralAction
	Action_2_t4234541925 * ___DiscoveredPeripheralAction_9;
	// System.Action`4<System.String,System.String,System.Int32,System.Byte[]> BluetoothDeviceScript::DiscoveredPeripheralWithAdvertisingInfoAction
	Action_4_t2314457268 * ___DiscoveredPeripheralWithAdvertisingInfoAction_10;
	// System.Action`2<System.String,System.String> BluetoothDeviceScript::RetrievedConnectedPeripheralAction
	Action_2_t4234541925 * ___RetrievedConnectedPeripheralAction_11;
	// System.Action`2<System.String,System.Byte[]> BluetoothDeviceScript::PeripheralReceivedWriteDataAction
	Action_2_t1307688409 * ___PeripheralReceivedWriteDataAction_12;
	// System.Action`1<System.String> BluetoothDeviceScript::ConnectedPeripheralAction
	Action_1_t1831019615 * ___ConnectedPeripheralAction_13;
	// System.Action`1<System.String> BluetoothDeviceScript::ConnectedDisconnectPeripheralAction
	Action_1_t1831019615 * ___ConnectedDisconnectPeripheralAction_14;
	// System.Action`1<System.String> BluetoothDeviceScript::DisconnectedPeripheralAction
	Action_1_t1831019615 * ___DisconnectedPeripheralAction_15;
	// System.Action`2<System.String,System.String> BluetoothDeviceScript::DiscoveredServiceAction
	Action_2_t4234541925 * ___DiscoveredServiceAction_16;
	// System.Action`3<System.String,System.String,System.String> BluetoothDeviceScript::DiscoveredCharacteristicAction
	Action_3_t3256166369 * ___DiscoveredCharacteristicAction_17;
	// System.Action`1<System.String> BluetoothDeviceScript::DidWriteCharacteristicAction
	Action_1_t1831019615 * ___DidWriteCharacteristicAction_18;
	// System.Action`1<System.String> BluetoothDeviceScript::DidUpdateNotificationStateForCharacteristicAction
	Action_1_t1831019615 * ___DidUpdateNotificationStateForCharacteristicAction_19;
	// System.Action`2<System.String,System.String> BluetoothDeviceScript::DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction
	Action_2_t4234541925 * ___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_20;
	// System.Action`2<System.String,System.Byte[]> BluetoothDeviceScript::DidUpdateCharacteristicValueAction
	Action_2_t1307688409 * ___DidUpdateCharacteristicValueAction_21;
	// System.Action`3<System.String,System.String,System.Byte[]> BluetoothDeviceScript::DidUpdateCharacteristicValueWithDeviceAddressAction
	Action_3_t329312853 * ___DidUpdateCharacteristicValueWithDeviceAddressAction_22;
	// System.Boolean BluetoothDeviceScript::Initialized
	bool ___Initialized_23;

public:
	inline static int32_t get_offset_of_DiscoveredDeviceList_2() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___DiscoveredDeviceList_2)); }
	inline List_1_t1398341365 * get_DiscoveredDeviceList_2() const { return ___DiscoveredDeviceList_2; }
	inline List_1_t1398341365 ** get_address_of_DiscoveredDeviceList_2() { return &___DiscoveredDeviceList_2; }
	inline void set_DiscoveredDeviceList_2(List_1_t1398341365 * value)
	{
		___DiscoveredDeviceList_2 = value;
		Il2CppCodeGenWriteBarrier(&___DiscoveredDeviceList_2, value);
	}

	inline static int32_t get_offset_of_InitializedAction_3() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___InitializedAction_3)); }
	inline Action_t3226471752 * get_InitializedAction_3() const { return ___InitializedAction_3; }
	inline Action_t3226471752 ** get_address_of_InitializedAction_3() { return &___InitializedAction_3; }
	inline void set_InitializedAction_3(Action_t3226471752 * value)
	{
		___InitializedAction_3 = value;
		Il2CppCodeGenWriteBarrier(&___InitializedAction_3, value);
	}

	inline static int32_t get_offset_of_DeinitializedAction_4() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___DeinitializedAction_4)); }
	inline Action_t3226471752 * get_DeinitializedAction_4() const { return ___DeinitializedAction_4; }
	inline Action_t3226471752 ** get_address_of_DeinitializedAction_4() { return &___DeinitializedAction_4; }
	inline void set_DeinitializedAction_4(Action_t3226471752 * value)
	{
		___DeinitializedAction_4 = value;
		Il2CppCodeGenWriteBarrier(&___DeinitializedAction_4, value);
	}

	inline static int32_t get_offset_of_ErrorAction_5() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___ErrorAction_5)); }
	inline Action_1_t1831019615 * get_ErrorAction_5() const { return ___ErrorAction_5; }
	inline Action_1_t1831019615 ** get_address_of_ErrorAction_5() { return &___ErrorAction_5; }
	inline void set_ErrorAction_5(Action_1_t1831019615 * value)
	{
		___ErrorAction_5 = value;
		Il2CppCodeGenWriteBarrier(&___ErrorAction_5, value);
	}

	inline static int32_t get_offset_of_ServiceAddedAction_6() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___ServiceAddedAction_6)); }
	inline Action_1_t1831019615 * get_ServiceAddedAction_6() const { return ___ServiceAddedAction_6; }
	inline Action_1_t1831019615 ** get_address_of_ServiceAddedAction_6() { return &___ServiceAddedAction_6; }
	inline void set_ServiceAddedAction_6(Action_1_t1831019615 * value)
	{
		___ServiceAddedAction_6 = value;
		Il2CppCodeGenWriteBarrier(&___ServiceAddedAction_6, value);
	}

	inline static int32_t get_offset_of_StartedAdvertisingAction_7() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___StartedAdvertisingAction_7)); }
	inline Action_t3226471752 * get_StartedAdvertisingAction_7() const { return ___StartedAdvertisingAction_7; }
	inline Action_t3226471752 ** get_address_of_StartedAdvertisingAction_7() { return &___StartedAdvertisingAction_7; }
	inline void set_StartedAdvertisingAction_7(Action_t3226471752 * value)
	{
		___StartedAdvertisingAction_7 = value;
		Il2CppCodeGenWriteBarrier(&___StartedAdvertisingAction_7, value);
	}

	inline static int32_t get_offset_of_StoppedAdvertisingAction_8() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___StoppedAdvertisingAction_8)); }
	inline Action_t3226471752 * get_StoppedAdvertisingAction_8() const { return ___StoppedAdvertisingAction_8; }
	inline Action_t3226471752 ** get_address_of_StoppedAdvertisingAction_8() { return &___StoppedAdvertisingAction_8; }
	inline void set_StoppedAdvertisingAction_8(Action_t3226471752 * value)
	{
		___StoppedAdvertisingAction_8 = value;
		Il2CppCodeGenWriteBarrier(&___StoppedAdvertisingAction_8, value);
	}

	inline static int32_t get_offset_of_DiscoveredPeripheralAction_9() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___DiscoveredPeripheralAction_9)); }
	inline Action_2_t4234541925 * get_DiscoveredPeripheralAction_9() const { return ___DiscoveredPeripheralAction_9; }
	inline Action_2_t4234541925 ** get_address_of_DiscoveredPeripheralAction_9() { return &___DiscoveredPeripheralAction_9; }
	inline void set_DiscoveredPeripheralAction_9(Action_2_t4234541925 * value)
	{
		___DiscoveredPeripheralAction_9 = value;
		Il2CppCodeGenWriteBarrier(&___DiscoveredPeripheralAction_9, value);
	}

	inline static int32_t get_offset_of_DiscoveredPeripheralWithAdvertisingInfoAction_10() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___DiscoveredPeripheralWithAdvertisingInfoAction_10)); }
	inline Action_4_t2314457268 * get_DiscoveredPeripheralWithAdvertisingInfoAction_10() const { return ___DiscoveredPeripheralWithAdvertisingInfoAction_10; }
	inline Action_4_t2314457268 ** get_address_of_DiscoveredPeripheralWithAdvertisingInfoAction_10() { return &___DiscoveredPeripheralWithAdvertisingInfoAction_10; }
	inline void set_DiscoveredPeripheralWithAdvertisingInfoAction_10(Action_4_t2314457268 * value)
	{
		___DiscoveredPeripheralWithAdvertisingInfoAction_10 = value;
		Il2CppCodeGenWriteBarrier(&___DiscoveredPeripheralWithAdvertisingInfoAction_10, value);
	}

	inline static int32_t get_offset_of_RetrievedConnectedPeripheralAction_11() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___RetrievedConnectedPeripheralAction_11)); }
	inline Action_2_t4234541925 * get_RetrievedConnectedPeripheralAction_11() const { return ___RetrievedConnectedPeripheralAction_11; }
	inline Action_2_t4234541925 ** get_address_of_RetrievedConnectedPeripheralAction_11() { return &___RetrievedConnectedPeripheralAction_11; }
	inline void set_RetrievedConnectedPeripheralAction_11(Action_2_t4234541925 * value)
	{
		___RetrievedConnectedPeripheralAction_11 = value;
		Il2CppCodeGenWriteBarrier(&___RetrievedConnectedPeripheralAction_11, value);
	}

	inline static int32_t get_offset_of_PeripheralReceivedWriteDataAction_12() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___PeripheralReceivedWriteDataAction_12)); }
	inline Action_2_t1307688409 * get_PeripheralReceivedWriteDataAction_12() const { return ___PeripheralReceivedWriteDataAction_12; }
	inline Action_2_t1307688409 ** get_address_of_PeripheralReceivedWriteDataAction_12() { return &___PeripheralReceivedWriteDataAction_12; }
	inline void set_PeripheralReceivedWriteDataAction_12(Action_2_t1307688409 * value)
	{
		___PeripheralReceivedWriteDataAction_12 = value;
		Il2CppCodeGenWriteBarrier(&___PeripheralReceivedWriteDataAction_12, value);
	}

	inline static int32_t get_offset_of_ConnectedPeripheralAction_13() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___ConnectedPeripheralAction_13)); }
	inline Action_1_t1831019615 * get_ConnectedPeripheralAction_13() const { return ___ConnectedPeripheralAction_13; }
	inline Action_1_t1831019615 ** get_address_of_ConnectedPeripheralAction_13() { return &___ConnectedPeripheralAction_13; }
	inline void set_ConnectedPeripheralAction_13(Action_1_t1831019615 * value)
	{
		___ConnectedPeripheralAction_13 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectedPeripheralAction_13, value);
	}

	inline static int32_t get_offset_of_ConnectedDisconnectPeripheralAction_14() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___ConnectedDisconnectPeripheralAction_14)); }
	inline Action_1_t1831019615 * get_ConnectedDisconnectPeripheralAction_14() const { return ___ConnectedDisconnectPeripheralAction_14; }
	inline Action_1_t1831019615 ** get_address_of_ConnectedDisconnectPeripheralAction_14() { return &___ConnectedDisconnectPeripheralAction_14; }
	inline void set_ConnectedDisconnectPeripheralAction_14(Action_1_t1831019615 * value)
	{
		___ConnectedDisconnectPeripheralAction_14 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectedDisconnectPeripheralAction_14, value);
	}

	inline static int32_t get_offset_of_DisconnectedPeripheralAction_15() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___DisconnectedPeripheralAction_15)); }
	inline Action_1_t1831019615 * get_DisconnectedPeripheralAction_15() const { return ___DisconnectedPeripheralAction_15; }
	inline Action_1_t1831019615 ** get_address_of_DisconnectedPeripheralAction_15() { return &___DisconnectedPeripheralAction_15; }
	inline void set_DisconnectedPeripheralAction_15(Action_1_t1831019615 * value)
	{
		___DisconnectedPeripheralAction_15 = value;
		Il2CppCodeGenWriteBarrier(&___DisconnectedPeripheralAction_15, value);
	}

	inline static int32_t get_offset_of_DiscoveredServiceAction_16() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___DiscoveredServiceAction_16)); }
	inline Action_2_t4234541925 * get_DiscoveredServiceAction_16() const { return ___DiscoveredServiceAction_16; }
	inline Action_2_t4234541925 ** get_address_of_DiscoveredServiceAction_16() { return &___DiscoveredServiceAction_16; }
	inline void set_DiscoveredServiceAction_16(Action_2_t4234541925 * value)
	{
		___DiscoveredServiceAction_16 = value;
		Il2CppCodeGenWriteBarrier(&___DiscoveredServiceAction_16, value);
	}

	inline static int32_t get_offset_of_DiscoveredCharacteristicAction_17() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___DiscoveredCharacteristicAction_17)); }
	inline Action_3_t3256166369 * get_DiscoveredCharacteristicAction_17() const { return ___DiscoveredCharacteristicAction_17; }
	inline Action_3_t3256166369 ** get_address_of_DiscoveredCharacteristicAction_17() { return &___DiscoveredCharacteristicAction_17; }
	inline void set_DiscoveredCharacteristicAction_17(Action_3_t3256166369 * value)
	{
		___DiscoveredCharacteristicAction_17 = value;
		Il2CppCodeGenWriteBarrier(&___DiscoveredCharacteristicAction_17, value);
	}

	inline static int32_t get_offset_of_DidWriteCharacteristicAction_18() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___DidWriteCharacteristicAction_18)); }
	inline Action_1_t1831019615 * get_DidWriteCharacteristicAction_18() const { return ___DidWriteCharacteristicAction_18; }
	inline Action_1_t1831019615 ** get_address_of_DidWriteCharacteristicAction_18() { return &___DidWriteCharacteristicAction_18; }
	inline void set_DidWriteCharacteristicAction_18(Action_1_t1831019615 * value)
	{
		___DidWriteCharacteristicAction_18 = value;
		Il2CppCodeGenWriteBarrier(&___DidWriteCharacteristicAction_18, value);
	}

	inline static int32_t get_offset_of_DidUpdateNotificationStateForCharacteristicAction_19() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___DidUpdateNotificationStateForCharacteristicAction_19)); }
	inline Action_1_t1831019615 * get_DidUpdateNotificationStateForCharacteristicAction_19() const { return ___DidUpdateNotificationStateForCharacteristicAction_19; }
	inline Action_1_t1831019615 ** get_address_of_DidUpdateNotificationStateForCharacteristicAction_19() { return &___DidUpdateNotificationStateForCharacteristicAction_19; }
	inline void set_DidUpdateNotificationStateForCharacteristicAction_19(Action_1_t1831019615 * value)
	{
		___DidUpdateNotificationStateForCharacteristicAction_19 = value;
		Il2CppCodeGenWriteBarrier(&___DidUpdateNotificationStateForCharacteristicAction_19, value);
	}

	inline static int32_t get_offset_of_DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_20() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_20)); }
	inline Action_2_t4234541925 * get_DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_20() const { return ___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_20; }
	inline Action_2_t4234541925 ** get_address_of_DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_20() { return &___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_20; }
	inline void set_DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_20(Action_2_t4234541925 * value)
	{
		___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_20 = value;
		Il2CppCodeGenWriteBarrier(&___DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_20, value);
	}

	inline static int32_t get_offset_of_DidUpdateCharacteristicValueAction_21() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___DidUpdateCharacteristicValueAction_21)); }
	inline Action_2_t1307688409 * get_DidUpdateCharacteristicValueAction_21() const { return ___DidUpdateCharacteristicValueAction_21; }
	inline Action_2_t1307688409 ** get_address_of_DidUpdateCharacteristicValueAction_21() { return &___DidUpdateCharacteristicValueAction_21; }
	inline void set_DidUpdateCharacteristicValueAction_21(Action_2_t1307688409 * value)
	{
		___DidUpdateCharacteristicValueAction_21 = value;
		Il2CppCodeGenWriteBarrier(&___DidUpdateCharacteristicValueAction_21, value);
	}

	inline static int32_t get_offset_of_DidUpdateCharacteristicValueWithDeviceAddressAction_22() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___DidUpdateCharacteristicValueWithDeviceAddressAction_22)); }
	inline Action_3_t329312853 * get_DidUpdateCharacteristicValueWithDeviceAddressAction_22() const { return ___DidUpdateCharacteristicValueWithDeviceAddressAction_22; }
	inline Action_3_t329312853 ** get_address_of_DidUpdateCharacteristicValueWithDeviceAddressAction_22() { return &___DidUpdateCharacteristicValueWithDeviceAddressAction_22; }
	inline void set_DidUpdateCharacteristicValueWithDeviceAddressAction_22(Action_3_t329312853 * value)
	{
		___DidUpdateCharacteristicValueWithDeviceAddressAction_22 = value;
		Il2CppCodeGenWriteBarrier(&___DidUpdateCharacteristicValueWithDeviceAddressAction_22, value);
	}

	inline static int32_t get_offset_of_Initialized_23() { return static_cast<int32_t>(offsetof(BluetoothDeviceScript_t810775987, ___Initialized_23)); }
	inline bool get_Initialized_23() const { return ___Initialized_23; }
	inline bool* get_address_of_Initialized_23() { return &___Initialized_23; }
	inline void set_Initialized_23(bool value)
	{
		___Initialized_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
