﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.Transition
struct  Transition_t2490031921  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Ricimi.Transition::m_overlay
	GameObject_t1756533147 * ___m_overlay_3;

public:
	inline static int32_t get_offset_of_m_overlay_3() { return static_cast<int32_t>(offsetof(Transition_t2490031921, ___m_overlay_3)); }
	inline GameObject_t1756533147 * get_m_overlay_3() const { return ___m_overlay_3; }
	inline GameObject_t1756533147 ** get_address_of_m_overlay_3() { return &___m_overlay_3; }
	inline void set_m_overlay_3(GameObject_t1756533147 * value)
	{
		___m_overlay_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_overlay_3, value);
	}
};

struct Transition_t2490031921_StaticFields
{
public:
	// UnityEngine.GameObject Ricimi.Transition::m_canvas
	GameObject_t1756533147 * ___m_canvas_2;

public:
	inline static int32_t get_offset_of_m_canvas_2() { return static_cast<int32_t>(offsetof(Transition_t2490031921_StaticFields, ___m_canvas_2)); }
	inline GameObject_t1756533147 * get_m_canvas_2() const { return ___m_canvas_2; }
	inline GameObject_t1756533147 ** get_address_of_m_canvas_2() { return &___m_canvas_2; }
	inline void set_m_canvas_2(GameObject_t1756533147 * value)
	{
		___m_canvas_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_canvas_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
