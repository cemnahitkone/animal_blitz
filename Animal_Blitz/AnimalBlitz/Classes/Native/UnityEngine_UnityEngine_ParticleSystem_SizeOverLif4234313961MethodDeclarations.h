﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/SizeOverLifetimeModule
struct SizeOverLifetimeModule_t4234313961;
struct SizeOverLifetimeModule_t4234313961_marshaled_pinvoke;
struct SizeOverLifetimeModule_t4234313961_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_SizeOverLif4234313961.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/SizeOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void SizeOverLifetimeModule__ctor_m3921922615 (SizeOverLifetimeModule_t4234313961 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct SizeOverLifetimeModule_t4234313961;
struct SizeOverLifetimeModule_t4234313961_marshaled_pinvoke;

extern "C" void SizeOverLifetimeModule_t4234313961_marshal_pinvoke(const SizeOverLifetimeModule_t4234313961& unmarshaled, SizeOverLifetimeModule_t4234313961_marshaled_pinvoke& marshaled);
extern "C" void SizeOverLifetimeModule_t4234313961_marshal_pinvoke_back(const SizeOverLifetimeModule_t4234313961_marshaled_pinvoke& marshaled, SizeOverLifetimeModule_t4234313961& unmarshaled);
extern "C" void SizeOverLifetimeModule_t4234313961_marshal_pinvoke_cleanup(SizeOverLifetimeModule_t4234313961_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SizeOverLifetimeModule_t4234313961;
struct SizeOverLifetimeModule_t4234313961_marshaled_com;

extern "C" void SizeOverLifetimeModule_t4234313961_marshal_com(const SizeOverLifetimeModule_t4234313961& unmarshaled, SizeOverLifetimeModule_t4234313961_marshaled_com& marshaled);
extern "C" void SizeOverLifetimeModule_t4234313961_marshal_com_back(const SizeOverLifetimeModule_t4234313961_marshaled_com& marshaled, SizeOverLifetimeModule_t4234313961& unmarshaled);
extern "C" void SizeOverLifetimeModule_t4234313961_marshal_com_cleanup(SizeOverLifetimeModule_t4234313961_marshaled_com& marshaled);
