﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IconControl
struct IconControl_t564227302;

#include "codegen/il2cpp-codegen.h"

// System.Void IconControl::.ctor()
extern "C"  void IconControl__ctor_m132043599 (IconControl_t564227302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconControl::Start()
extern "C"  void IconControl_Start_m4015155435 (IconControl_t564227302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconControl::calculateSpeeds()
extern "C"  void IconControl_calculateSpeeds_m4115591427 (IconControl_t564227302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconControl::Update()
extern "C"  void IconControl_Update_m1339975508 (IconControl_t564227302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconControl::checkAnswer()
extern "C"  void IconControl_checkAnswer_m2014282341 (IconControl_t564227302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconControl::finishLevel()
extern "C"  void IconControl_finishLevel_m1676629442 (IconControl_t564227302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconControl::destroyDog()
extern "C"  void IconControl_destroyDog_m1968663041 (IconControl_t564227302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconControl::destroyCat()
extern "C"  void IconControl_destroyCat_m4092750423 (IconControl_t564227302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconControl::destroyOwl()
extern "C"  void IconControl_destroyOwl_m1774070153 (IconControl_t564227302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconControl::destroyRat()
extern "C"  void IconControl_destroyRat_m4092767816 (IconControl_t564227302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconControl::destroyTurtle()
extern "C"  void IconControl_destroyTurtle_m712327719 (IconControl_t564227302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
