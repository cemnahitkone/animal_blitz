﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<testBle/baseLegID>
struct List_1_t672740780;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`2<System.String,System.String>
struct Action_2_t4234541925;
// System.Action`4<System.String,System.String,System.Int32,System.Byte[]>
struct Action_4_t2314457268;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// testBle
struct  testBle_t3025851879  : public MonoBehaviour_t1158329972
{
public:
	// System.String testBle::_readCharacteristicUUID
	String_t* ____readCharacteristicUUID_5;
	// System.String testBle::_address
	String_t* ____address_6;
	// System.String testBle::_name
	String_t* ____name_7;
	// System.String testBle::BleDataNew
	String_t* ___BleDataNew_9;
	// System.Boolean testBle::dataState
	bool ___dataState_10;
	// System.Collections.Generic.List`1<testBle/baseLegID> testBle::idList
	List_1_t672740780 * ___idList_11;

public:
	inline static int32_t get_offset_of__readCharacteristicUUID_5() { return static_cast<int32_t>(offsetof(testBle_t3025851879, ____readCharacteristicUUID_5)); }
	inline String_t* get__readCharacteristicUUID_5() const { return ____readCharacteristicUUID_5; }
	inline String_t** get_address_of__readCharacteristicUUID_5() { return &____readCharacteristicUUID_5; }
	inline void set__readCharacteristicUUID_5(String_t* value)
	{
		____readCharacteristicUUID_5 = value;
		Il2CppCodeGenWriteBarrier(&____readCharacteristicUUID_5, value);
	}

	inline static int32_t get_offset_of__address_6() { return static_cast<int32_t>(offsetof(testBle_t3025851879, ____address_6)); }
	inline String_t* get__address_6() const { return ____address_6; }
	inline String_t** get_address_of__address_6() { return &____address_6; }
	inline void set__address_6(String_t* value)
	{
		____address_6 = value;
		Il2CppCodeGenWriteBarrier(&____address_6, value);
	}

	inline static int32_t get_offset_of__name_7() { return static_cast<int32_t>(offsetof(testBle_t3025851879, ____name_7)); }
	inline String_t* get__name_7() const { return ____name_7; }
	inline String_t** get_address_of__name_7() { return &____name_7; }
	inline void set__name_7(String_t* value)
	{
		____name_7 = value;
		Il2CppCodeGenWriteBarrier(&____name_7, value);
	}

	inline static int32_t get_offset_of_BleDataNew_9() { return static_cast<int32_t>(offsetof(testBle_t3025851879, ___BleDataNew_9)); }
	inline String_t* get_BleDataNew_9() const { return ___BleDataNew_9; }
	inline String_t** get_address_of_BleDataNew_9() { return &___BleDataNew_9; }
	inline void set_BleDataNew_9(String_t* value)
	{
		___BleDataNew_9 = value;
		Il2CppCodeGenWriteBarrier(&___BleDataNew_9, value);
	}

	inline static int32_t get_offset_of_dataState_10() { return static_cast<int32_t>(offsetof(testBle_t3025851879, ___dataState_10)); }
	inline bool get_dataState_10() const { return ___dataState_10; }
	inline bool* get_address_of_dataState_10() { return &___dataState_10; }
	inline void set_dataState_10(bool value)
	{
		___dataState_10 = value;
	}

	inline static int32_t get_offset_of_idList_11() { return static_cast<int32_t>(offsetof(testBle_t3025851879, ___idList_11)); }
	inline List_1_t672740780 * get_idList_11() const { return ___idList_11; }
	inline List_1_t672740780 ** get_address_of_idList_11() { return &___idList_11; }
	inline void set_idList_11(List_1_t672740780 * value)
	{
		___idList_11 = value;
		Il2CppCodeGenWriteBarrier(&___idList_11, value);
	}
};

struct testBle_t3025851879_StaticFields
{
public:
	// System.String testBle::_connectedID
	String_t* ____connectedID_2;
	// System.String testBle::_serviceUUID
	String_t* ____serviceUUID_3;
	// System.String testBle::_writeCharacteristicUUID
	String_t* ____writeCharacteristicUUID_4;
	// System.String testBle::BleDataDone
	String_t* ___BleDataDone_8;
	// System.Int32 testBle::_uniqID
	int32_t ____uniqID_12;
	// System.Action`1<System.String> testBle::<>f__am$cache0
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache0_13;
	// System.Action`1<System.String> testBle::<>f__am$cache1
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache1_14;
	// System.Action`2<System.String,System.String> testBle::<>f__am$cache2
	Action_2_t4234541925 * ___U3CU3Ef__amU24cache2_15;
	// System.Action`4<System.String,System.String,System.Int32,System.Byte[]> testBle::<>f__am$cache3
	Action_4_t2314457268 * ___U3CU3Ef__amU24cache3_16;
	// System.Action`1<System.String> testBle::<>f__am$cache4
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache4_17;
	// System.Action`1<System.String> testBle::<>f__am$cache5
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache5_18;
	// System.Action`2<System.String,System.String> testBle::<>f__am$cache6
	Action_2_t4234541925 * ___U3CU3Ef__amU24cache6_19;

public:
	inline static int32_t get_offset_of__connectedID_2() { return static_cast<int32_t>(offsetof(testBle_t3025851879_StaticFields, ____connectedID_2)); }
	inline String_t* get__connectedID_2() const { return ____connectedID_2; }
	inline String_t** get_address_of__connectedID_2() { return &____connectedID_2; }
	inline void set__connectedID_2(String_t* value)
	{
		____connectedID_2 = value;
		Il2CppCodeGenWriteBarrier(&____connectedID_2, value);
	}

	inline static int32_t get_offset_of__serviceUUID_3() { return static_cast<int32_t>(offsetof(testBle_t3025851879_StaticFields, ____serviceUUID_3)); }
	inline String_t* get__serviceUUID_3() const { return ____serviceUUID_3; }
	inline String_t** get_address_of__serviceUUID_3() { return &____serviceUUID_3; }
	inline void set__serviceUUID_3(String_t* value)
	{
		____serviceUUID_3 = value;
		Il2CppCodeGenWriteBarrier(&____serviceUUID_3, value);
	}

	inline static int32_t get_offset_of__writeCharacteristicUUID_4() { return static_cast<int32_t>(offsetof(testBle_t3025851879_StaticFields, ____writeCharacteristicUUID_4)); }
	inline String_t* get__writeCharacteristicUUID_4() const { return ____writeCharacteristicUUID_4; }
	inline String_t** get_address_of__writeCharacteristicUUID_4() { return &____writeCharacteristicUUID_4; }
	inline void set__writeCharacteristicUUID_4(String_t* value)
	{
		____writeCharacteristicUUID_4 = value;
		Il2CppCodeGenWriteBarrier(&____writeCharacteristicUUID_4, value);
	}

	inline static int32_t get_offset_of_BleDataDone_8() { return static_cast<int32_t>(offsetof(testBle_t3025851879_StaticFields, ___BleDataDone_8)); }
	inline String_t* get_BleDataDone_8() const { return ___BleDataDone_8; }
	inline String_t** get_address_of_BleDataDone_8() { return &___BleDataDone_8; }
	inline void set_BleDataDone_8(String_t* value)
	{
		___BleDataDone_8 = value;
		Il2CppCodeGenWriteBarrier(&___BleDataDone_8, value);
	}

	inline static int32_t get_offset_of__uniqID_12() { return static_cast<int32_t>(offsetof(testBle_t3025851879_StaticFields, ____uniqID_12)); }
	inline int32_t get__uniqID_12() const { return ____uniqID_12; }
	inline int32_t* get_address_of__uniqID_12() { return &____uniqID_12; }
	inline void set__uniqID_12(int32_t value)
	{
		____uniqID_12 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_13() { return static_cast<int32_t>(offsetof(testBle_t3025851879_StaticFields, ___U3CU3Ef__amU24cache0_13)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache0_13() const { return ___U3CU3Ef__amU24cache0_13; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache0_13() { return &___U3CU3Ef__amU24cache0_13; }
	inline void set_U3CU3Ef__amU24cache0_13(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_14() { return static_cast<int32_t>(offsetof(testBle_t3025851879_StaticFields, ___U3CU3Ef__amU24cache1_14)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache1_14() const { return ___U3CU3Ef__amU24cache1_14; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache1_14() { return &___U3CU3Ef__amU24cache1_14; }
	inline void set_U3CU3Ef__amU24cache1_14(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache1_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_15() { return static_cast<int32_t>(offsetof(testBle_t3025851879_StaticFields, ___U3CU3Ef__amU24cache2_15)); }
	inline Action_2_t4234541925 * get_U3CU3Ef__amU24cache2_15() const { return ___U3CU3Ef__amU24cache2_15; }
	inline Action_2_t4234541925 ** get_address_of_U3CU3Ef__amU24cache2_15() { return &___U3CU3Ef__amU24cache2_15; }
	inline void set_U3CU3Ef__amU24cache2_15(Action_2_t4234541925 * value)
	{
		___U3CU3Ef__amU24cache2_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_16() { return static_cast<int32_t>(offsetof(testBle_t3025851879_StaticFields, ___U3CU3Ef__amU24cache3_16)); }
	inline Action_4_t2314457268 * get_U3CU3Ef__amU24cache3_16() const { return ___U3CU3Ef__amU24cache3_16; }
	inline Action_4_t2314457268 ** get_address_of_U3CU3Ef__amU24cache3_16() { return &___U3CU3Ef__amU24cache3_16; }
	inline void set_U3CU3Ef__amU24cache3_16(Action_4_t2314457268 * value)
	{
		___U3CU3Ef__amU24cache3_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_17() { return static_cast<int32_t>(offsetof(testBle_t3025851879_StaticFields, ___U3CU3Ef__amU24cache4_17)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache4_17() const { return ___U3CU3Ef__amU24cache4_17; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache4_17() { return &___U3CU3Ef__amU24cache4_17; }
	inline void set_U3CU3Ef__amU24cache4_17(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache4_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_18() { return static_cast<int32_t>(offsetof(testBle_t3025851879_StaticFields, ___U3CU3Ef__amU24cache5_18)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache5_18() const { return ___U3CU3Ef__amU24cache5_18; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache5_18() { return &___U3CU3Ef__amU24cache5_18; }
	inline void set_U3CU3Ef__amU24cache5_18(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache5_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_19() { return static_cast<int32_t>(offsetof(testBle_t3025851879_StaticFields, ___U3CU3Ef__amU24cache6_19)); }
	inline Action_2_t4234541925 * get_U3CU3Ef__amU24cache6_19() const { return ___U3CU3Ef__amU24cache6_19; }
	inline Action_2_t4234541925 ** get_address_of_U3CU3Ef__amU24cache6_19() { return &___U3CU3Ef__amU24cache6_19; }
	inline void set_U3CU3Ef__amU24cache6_19(Action_2_t4234541925 * value)
	{
		___U3CU3Ef__amU24cache6_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
