﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BackgroundScroll
struct BackgroundScroll_t1444628405;

#include "codegen/il2cpp-codegen.h"

// System.Void BackgroundScroll::.ctor()
extern "C"  void BackgroundScroll__ctor_m2467004305 (BackgroundScroll_t1444628405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroundScroll::Start()
extern "C"  void BackgroundScroll_Start_m838642729 (BackgroundScroll_t1444628405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroundScroll::Update()
extern "C"  void BackgroundScroll_Update_m3270540836 (BackgroundScroll_t1444628405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroundScroll::Main()
extern "C"  void BackgroundScroll_Main_m2678384016 (BackgroundScroll_t1444628405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
