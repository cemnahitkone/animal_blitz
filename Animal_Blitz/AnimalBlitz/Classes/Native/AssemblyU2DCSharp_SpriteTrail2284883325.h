﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// TrailPreset
struct TrailPreset_t465077881;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// System.Collections.Generic.Queue`1<TrailElement>
struct Queue_1_t1505052203;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_TrailActivationCondition372043875.h"
#include "AssemblyU2DCSharp_TrailDisactivationCondition1222614977.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpriteTrail
struct  SpriteTrail_t2284883325  : public MonoBehaviour_t1158329972
{
public:
	// System.String SpriteTrail::m_TrailName
	String_t* ___m_TrailName_2;
	// TrailPreset SpriteTrail::m_CurrentTrailPreset
	TrailPreset_t465077881 * ___m_CurrentTrailPreset_3;
	// UnityEngine.Transform SpriteTrail::m_TrailParent
	Transform_t3275118058 * ___m_TrailParent_4;
	// UnityEngine.SpriteRenderer SpriteTrail::m_SpriteToDuplicate
	SpriteRenderer_t1209076198 * ___m_SpriteToDuplicate_5;
	// System.String SpriteTrail::m_LayerName
	String_t* ___m_LayerName_6;
	// System.Single SpriteTrail::m_ZMoveStep
	float ___m_ZMoveStep_7;
	// System.Single SpriteTrail::m_ZMoveMax
	float ___m_ZMoveMax_8;
	// System.Int32 SpriteTrail::m_TrailOrderInLayer
	int32_t ___m_TrailOrderInLayer_9;
	// System.Boolean SpriteTrail::m_HideTrailOnDisabled
	bool ___m_HideTrailOnDisabled_10;
	// TrailActivationCondition SpriteTrail::m_TrailActivationCondition
	int32_t ___m_TrailActivationCondition_11;
	// TrailDisactivationCondition SpriteTrail::m_TrailDisactivationCondition
	int32_t ___m_TrailDisactivationCondition_12;
	// System.Boolean SpriteTrail::m_StartIfUnderVelocity
	bool ___m_StartIfUnderVelocity_13;
	// System.Boolean SpriteTrail::m_VelocityStartIsLocalSpace
	bool ___m_VelocityStartIsLocalSpace_14;
	// System.Single SpriteTrail::m_VelocityNeededToStart
	float ___m_VelocityNeededToStart_15;
	// System.Boolean SpriteTrail::m_StopIfOverVelocity
	bool ___m_StopIfOverVelocity_16;
	// System.Boolean SpriteTrail::m_VelocityStopIsLocalSpace
	bool ___m_VelocityStopIsLocalSpace_17;
	// System.Single SpriteTrail::m_VelocityNeededToStop
	float ___m_VelocityNeededToStop_18;
	// System.Single SpriteTrail::m_TrailActivationDuration
	float ___m_TrailActivationDuration_19;
	// System.Collections.Generic.Queue`1<TrailElement> SpriteTrail::m_ElementsInTrail
	Queue_1_t1505052203 * ___m_ElementsInTrail_20;
	// TrailPreset SpriteTrail::m_PreviousTrailPreset
	TrailPreset_t465077881 * ___m_PreviousTrailPreset_22;
	// UnityEngine.Transform SpriteTrail::m_LocalTrailContainer
	Transform_t3275118058 * ___m_LocalTrailContainer_24;
	// UnityEngine.Transform SpriteTrail::m_GlobalTrailContainer
	Transform_t3275118058 * ___m_GlobalTrailContainer_25;
	// System.Single SpriteTrail::m_CurrentDisplacement
	float ___m_CurrentDisplacement_26;
	// System.Single SpriteTrail::m_TimeTrailStarted
	float ___m_TimeTrailStarted_27;
	// System.Single SpriteTrail::m_PreviousTimeSpawned
	float ___m_PreviousTimeSpawned_28;
	// System.Int32 SpriteTrail::m_PreviousFrameSpawned
	int32_t ___m_PreviousFrameSpawned_29;
	// UnityEngine.Vector2 SpriteTrail::m_PreviousPosSpawned
	Vector2_t2243707579  ___m_PreviousPosSpawned_30;
	// UnityEngine.Vector2 SpriteTrail::m_PreviousFrameLocalPos
	Vector2_t2243707579  ___m_PreviousFrameLocalPos_31;
	// UnityEngine.Vector2 SpriteTrail::m_PreviousFrameWorldPos
	Vector2_t2243707579  ___m_PreviousFrameWorldPos_32;
	// UnityEngine.Vector2 SpriteTrail::m_VelocityLocal
	Vector2_t2243707579  ___m_VelocityLocal_33;
	// UnityEngine.Vector2 SpriteTrail::m_VelocityWorld
	Vector2_t2243707579  ___m_VelocityWorld_34;
	// System.Boolean SpriteTrail::m_FirstVelocityCheck
	bool ___m_FirstVelocityCheck_35;
	// System.Boolean SpriteTrail::m_CanBeAutomaticallyActivated
	bool ___m_CanBeAutomaticallyActivated_36;
	// System.Boolean SpriteTrail::m_EffectEnabled
	bool ___m_EffectEnabled_37;
	// System.Boolean SpriteTrail::m_WillBeActivatedOnEnable
	bool ___m_WillBeActivatedOnEnable_38;

public:
	inline static int32_t get_offset_of_m_TrailName_2() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_TrailName_2)); }
	inline String_t* get_m_TrailName_2() const { return ___m_TrailName_2; }
	inline String_t** get_address_of_m_TrailName_2() { return &___m_TrailName_2; }
	inline void set_m_TrailName_2(String_t* value)
	{
		___m_TrailName_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_TrailName_2, value);
	}

	inline static int32_t get_offset_of_m_CurrentTrailPreset_3() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_CurrentTrailPreset_3)); }
	inline TrailPreset_t465077881 * get_m_CurrentTrailPreset_3() const { return ___m_CurrentTrailPreset_3; }
	inline TrailPreset_t465077881 ** get_address_of_m_CurrentTrailPreset_3() { return &___m_CurrentTrailPreset_3; }
	inline void set_m_CurrentTrailPreset_3(TrailPreset_t465077881 * value)
	{
		___m_CurrentTrailPreset_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_CurrentTrailPreset_3, value);
	}

	inline static int32_t get_offset_of_m_TrailParent_4() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_TrailParent_4)); }
	inline Transform_t3275118058 * get_m_TrailParent_4() const { return ___m_TrailParent_4; }
	inline Transform_t3275118058 ** get_address_of_m_TrailParent_4() { return &___m_TrailParent_4; }
	inline void set_m_TrailParent_4(Transform_t3275118058 * value)
	{
		___m_TrailParent_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_TrailParent_4, value);
	}

	inline static int32_t get_offset_of_m_SpriteToDuplicate_5() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_SpriteToDuplicate_5)); }
	inline SpriteRenderer_t1209076198 * get_m_SpriteToDuplicate_5() const { return ___m_SpriteToDuplicate_5; }
	inline SpriteRenderer_t1209076198 ** get_address_of_m_SpriteToDuplicate_5() { return &___m_SpriteToDuplicate_5; }
	inline void set_m_SpriteToDuplicate_5(SpriteRenderer_t1209076198 * value)
	{
		___m_SpriteToDuplicate_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_SpriteToDuplicate_5, value);
	}

	inline static int32_t get_offset_of_m_LayerName_6() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_LayerName_6)); }
	inline String_t* get_m_LayerName_6() const { return ___m_LayerName_6; }
	inline String_t** get_address_of_m_LayerName_6() { return &___m_LayerName_6; }
	inline void set_m_LayerName_6(String_t* value)
	{
		___m_LayerName_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_LayerName_6, value);
	}

	inline static int32_t get_offset_of_m_ZMoveStep_7() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_ZMoveStep_7)); }
	inline float get_m_ZMoveStep_7() const { return ___m_ZMoveStep_7; }
	inline float* get_address_of_m_ZMoveStep_7() { return &___m_ZMoveStep_7; }
	inline void set_m_ZMoveStep_7(float value)
	{
		___m_ZMoveStep_7 = value;
	}

	inline static int32_t get_offset_of_m_ZMoveMax_8() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_ZMoveMax_8)); }
	inline float get_m_ZMoveMax_8() const { return ___m_ZMoveMax_8; }
	inline float* get_address_of_m_ZMoveMax_8() { return &___m_ZMoveMax_8; }
	inline void set_m_ZMoveMax_8(float value)
	{
		___m_ZMoveMax_8 = value;
	}

	inline static int32_t get_offset_of_m_TrailOrderInLayer_9() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_TrailOrderInLayer_9)); }
	inline int32_t get_m_TrailOrderInLayer_9() const { return ___m_TrailOrderInLayer_9; }
	inline int32_t* get_address_of_m_TrailOrderInLayer_9() { return &___m_TrailOrderInLayer_9; }
	inline void set_m_TrailOrderInLayer_9(int32_t value)
	{
		___m_TrailOrderInLayer_9 = value;
	}

	inline static int32_t get_offset_of_m_HideTrailOnDisabled_10() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_HideTrailOnDisabled_10)); }
	inline bool get_m_HideTrailOnDisabled_10() const { return ___m_HideTrailOnDisabled_10; }
	inline bool* get_address_of_m_HideTrailOnDisabled_10() { return &___m_HideTrailOnDisabled_10; }
	inline void set_m_HideTrailOnDisabled_10(bool value)
	{
		___m_HideTrailOnDisabled_10 = value;
	}

	inline static int32_t get_offset_of_m_TrailActivationCondition_11() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_TrailActivationCondition_11)); }
	inline int32_t get_m_TrailActivationCondition_11() const { return ___m_TrailActivationCondition_11; }
	inline int32_t* get_address_of_m_TrailActivationCondition_11() { return &___m_TrailActivationCondition_11; }
	inline void set_m_TrailActivationCondition_11(int32_t value)
	{
		___m_TrailActivationCondition_11 = value;
	}

	inline static int32_t get_offset_of_m_TrailDisactivationCondition_12() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_TrailDisactivationCondition_12)); }
	inline int32_t get_m_TrailDisactivationCondition_12() const { return ___m_TrailDisactivationCondition_12; }
	inline int32_t* get_address_of_m_TrailDisactivationCondition_12() { return &___m_TrailDisactivationCondition_12; }
	inline void set_m_TrailDisactivationCondition_12(int32_t value)
	{
		___m_TrailDisactivationCondition_12 = value;
	}

	inline static int32_t get_offset_of_m_StartIfUnderVelocity_13() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_StartIfUnderVelocity_13)); }
	inline bool get_m_StartIfUnderVelocity_13() const { return ___m_StartIfUnderVelocity_13; }
	inline bool* get_address_of_m_StartIfUnderVelocity_13() { return &___m_StartIfUnderVelocity_13; }
	inline void set_m_StartIfUnderVelocity_13(bool value)
	{
		___m_StartIfUnderVelocity_13 = value;
	}

	inline static int32_t get_offset_of_m_VelocityStartIsLocalSpace_14() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_VelocityStartIsLocalSpace_14)); }
	inline bool get_m_VelocityStartIsLocalSpace_14() const { return ___m_VelocityStartIsLocalSpace_14; }
	inline bool* get_address_of_m_VelocityStartIsLocalSpace_14() { return &___m_VelocityStartIsLocalSpace_14; }
	inline void set_m_VelocityStartIsLocalSpace_14(bool value)
	{
		___m_VelocityStartIsLocalSpace_14 = value;
	}

	inline static int32_t get_offset_of_m_VelocityNeededToStart_15() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_VelocityNeededToStart_15)); }
	inline float get_m_VelocityNeededToStart_15() const { return ___m_VelocityNeededToStart_15; }
	inline float* get_address_of_m_VelocityNeededToStart_15() { return &___m_VelocityNeededToStart_15; }
	inline void set_m_VelocityNeededToStart_15(float value)
	{
		___m_VelocityNeededToStart_15 = value;
	}

	inline static int32_t get_offset_of_m_StopIfOverVelocity_16() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_StopIfOverVelocity_16)); }
	inline bool get_m_StopIfOverVelocity_16() const { return ___m_StopIfOverVelocity_16; }
	inline bool* get_address_of_m_StopIfOverVelocity_16() { return &___m_StopIfOverVelocity_16; }
	inline void set_m_StopIfOverVelocity_16(bool value)
	{
		___m_StopIfOverVelocity_16 = value;
	}

	inline static int32_t get_offset_of_m_VelocityStopIsLocalSpace_17() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_VelocityStopIsLocalSpace_17)); }
	inline bool get_m_VelocityStopIsLocalSpace_17() const { return ___m_VelocityStopIsLocalSpace_17; }
	inline bool* get_address_of_m_VelocityStopIsLocalSpace_17() { return &___m_VelocityStopIsLocalSpace_17; }
	inline void set_m_VelocityStopIsLocalSpace_17(bool value)
	{
		___m_VelocityStopIsLocalSpace_17 = value;
	}

	inline static int32_t get_offset_of_m_VelocityNeededToStop_18() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_VelocityNeededToStop_18)); }
	inline float get_m_VelocityNeededToStop_18() const { return ___m_VelocityNeededToStop_18; }
	inline float* get_address_of_m_VelocityNeededToStop_18() { return &___m_VelocityNeededToStop_18; }
	inline void set_m_VelocityNeededToStop_18(float value)
	{
		___m_VelocityNeededToStop_18 = value;
	}

	inline static int32_t get_offset_of_m_TrailActivationDuration_19() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_TrailActivationDuration_19)); }
	inline float get_m_TrailActivationDuration_19() const { return ___m_TrailActivationDuration_19; }
	inline float* get_address_of_m_TrailActivationDuration_19() { return &___m_TrailActivationDuration_19; }
	inline void set_m_TrailActivationDuration_19(float value)
	{
		___m_TrailActivationDuration_19 = value;
	}

	inline static int32_t get_offset_of_m_ElementsInTrail_20() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_ElementsInTrail_20)); }
	inline Queue_1_t1505052203 * get_m_ElementsInTrail_20() const { return ___m_ElementsInTrail_20; }
	inline Queue_1_t1505052203 ** get_address_of_m_ElementsInTrail_20() { return &___m_ElementsInTrail_20; }
	inline void set_m_ElementsInTrail_20(Queue_1_t1505052203 * value)
	{
		___m_ElementsInTrail_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_ElementsInTrail_20, value);
	}

	inline static int32_t get_offset_of_m_PreviousTrailPreset_22() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_PreviousTrailPreset_22)); }
	inline TrailPreset_t465077881 * get_m_PreviousTrailPreset_22() const { return ___m_PreviousTrailPreset_22; }
	inline TrailPreset_t465077881 ** get_address_of_m_PreviousTrailPreset_22() { return &___m_PreviousTrailPreset_22; }
	inline void set_m_PreviousTrailPreset_22(TrailPreset_t465077881 * value)
	{
		___m_PreviousTrailPreset_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_PreviousTrailPreset_22, value);
	}

	inline static int32_t get_offset_of_m_LocalTrailContainer_24() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_LocalTrailContainer_24)); }
	inline Transform_t3275118058 * get_m_LocalTrailContainer_24() const { return ___m_LocalTrailContainer_24; }
	inline Transform_t3275118058 ** get_address_of_m_LocalTrailContainer_24() { return &___m_LocalTrailContainer_24; }
	inline void set_m_LocalTrailContainer_24(Transform_t3275118058 * value)
	{
		___m_LocalTrailContainer_24 = value;
		Il2CppCodeGenWriteBarrier(&___m_LocalTrailContainer_24, value);
	}

	inline static int32_t get_offset_of_m_GlobalTrailContainer_25() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_GlobalTrailContainer_25)); }
	inline Transform_t3275118058 * get_m_GlobalTrailContainer_25() const { return ___m_GlobalTrailContainer_25; }
	inline Transform_t3275118058 ** get_address_of_m_GlobalTrailContainer_25() { return &___m_GlobalTrailContainer_25; }
	inline void set_m_GlobalTrailContainer_25(Transform_t3275118058 * value)
	{
		___m_GlobalTrailContainer_25 = value;
		Il2CppCodeGenWriteBarrier(&___m_GlobalTrailContainer_25, value);
	}

	inline static int32_t get_offset_of_m_CurrentDisplacement_26() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_CurrentDisplacement_26)); }
	inline float get_m_CurrentDisplacement_26() const { return ___m_CurrentDisplacement_26; }
	inline float* get_address_of_m_CurrentDisplacement_26() { return &___m_CurrentDisplacement_26; }
	inline void set_m_CurrentDisplacement_26(float value)
	{
		___m_CurrentDisplacement_26 = value;
	}

	inline static int32_t get_offset_of_m_TimeTrailStarted_27() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_TimeTrailStarted_27)); }
	inline float get_m_TimeTrailStarted_27() const { return ___m_TimeTrailStarted_27; }
	inline float* get_address_of_m_TimeTrailStarted_27() { return &___m_TimeTrailStarted_27; }
	inline void set_m_TimeTrailStarted_27(float value)
	{
		___m_TimeTrailStarted_27 = value;
	}

	inline static int32_t get_offset_of_m_PreviousTimeSpawned_28() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_PreviousTimeSpawned_28)); }
	inline float get_m_PreviousTimeSpawned_28() const { return ___m_PreviousTimeSpawned_28; }
	inline float* get_address_of_m_PreviousTimeSpawned_28() { return &___m_PreviousTimeSpawned_28; }
	inline void set_m_PreviousTimeSpawned_28(float value)
	{
		___m_PreviousTimeSpawned_28 = value;
	}

	inline static int32_t get_offset_of_m_PreviousFrameSpawned_29() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_PreviousFrameSpawned_29)); }
	inline int32_t get_m_PreviousFrameSpawned_29() const { return ___m_PreviousFrameSpawned_29; }
	inline int32_t* get_address_of_m_PreviousFrameSpawned_29() { return &___m_PreviousFrameSpawned_29; }
	inline void set_m_PreviousFrameSpawned_29(int32_t value)
	{
		___m_PreviousFrameSpawned_29 = value;
	}

	inline static int32_t get_offset_of_m_PreviousPosSpawned_30() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_PreviousPosSpawned_30)); }
	inline Vector2_t2243707579  get_m_PreviousPosSpawned_30() const { return ___m_PreviousPosSpawned_30; }
	inline Vector2_t2243707579 * get_address_of_m_PreviousPosSpawned_30() { return &___m_PreviousPosSpawned_30; }
	inline void set_m_PreviousPosSpawned_30(Vector2_t2243707579  value)
	{
		___m_PreviousPosSpawned_30 = value;
	}

	inline static int32_t get_offset_of_m_PreviousFrameLocalPos_31() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_PreviousFrameLocalPos_31)); }
	inline Vector2_t2243707579  get_m_PreviousFrameLocalPos_31() const { return ___m_PreviousFrameLocalPos_31; }
	inline Vector2_t2243707579 * get_address_of_m_PreviousFrameLocalPos_31() { return &___m_PreviousFrameLocalPos_31; }
	inline void set_m_PreviousFrameLocalPos_31(Vector2_t2243707579  value)
	{
		___m_PreviousFrameLocalPos_31 = value;
	}

	inline static int32_t get_offset_of_m_PreviousFrameWorldPos_32() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_PreviousFrameWorldPos_32)); }
	inline Vector2_t2243707579  get_m_PreviousFrameWorldPos_32() const { return ___m_PreviousFrameWorldPos_32; }
	inline Vector2_t2243707579 * get_address_of_m_PreviousFrameWorldPos_32() { return &___m_PreviousFrameWorldPos_32; }
	inline void set_m_PreviousFrameWorldPos_32(Vector2_t2243707579  value)
	{
		___m_PreviousFrameWorldPos_32 = value;
	}

	inline static int32_t get_offset_of_m_VelocityLocal_33() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_VelocityLocal_33)); }
	inline Vector2_t2243707579  get_m_VelocityLocal_33() const { return ___m_VelocityLocal_33; }
	inline Vector2_t2243707579 * get_address_of_m_VelocityLocal_33() { return &___m_VelocityLocal_33; }
	inline void set_m_VelocityLocal_33(Vector2_t2243707579  value)
	{
		___m_VelocityLocal_33 = value;
	}

	inline static int32_t get_offset_of_m_VelocityWorld_34() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_VelocityWorld_34)); }
	inline Vector2_t2243707579  get_m_VelocityWorld_34() const { return ___m_VelocityWorld_34; }
	inline Vector2_t2243707579 * get_address_of_m_VelocityWorld_34() { return &___m_VelocityWorld_34; }
	inline void set_m_VelocityWorld_34(Vector2_t2243707579  value)
	{
		___m_VelocityWorld_34 = value;
	}

	inline static int32_t get_offset_of_m_FirstVelocityCheck_35() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_FirstVelocityCheck_35)); }
	inline bool get_m_FirstVelocityCheck_35() const { return ___m_FirstVelocityCheck_35; }
	inline bool* get_address_of_m_FirstVelocityCheck_35() { return &___m_FirstVelocityCheck_35; }
	inline void set_m_FirstVelocityCheck_35(bool value)
	{
		___m_FirstVelocityCheck_35 = value;
	}

	inline static int32_t get_offset_of_m_CanBeAutomaticallyActivated_36() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_CanBeAutomaticallyActivated_36)); }
	inline bool get_m_CanBeAutomaticallyActivated_36() const { return ___m_CanBeAutomaticallyActivated_36; }
	inline bool* get_address_of_m_CanBeAutomaticallyActivated_36() { return &___m_CanBeAutomaticallyActivated_36; }
	inline void set_m_CanBeAutomaticallyActivated_36(bool value)
	{
		___m_CanBeAutomaticallyActivated_36 = value;
	}

	inline static int32_t get_offset_of_m_EffectEnabled_37() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_EffectEnabled_37)); }
	inline bool get_m_EffectEnabled_37() const { return ___m_EffectEnabled_37; }
	inline bool* get_address_of_m_EffectEnabled_37() { return &___m_EffectEnabled_37; }
	inline void set_m_EffectEnabled_37(bool value)
	{
		___m_EffectEnabled_37 = value;
	}

	inline static int32_t get_offset_of_m_WillBeActivatedOnEnable_38() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325, ___m_WillBeActivatedOnEnable_38)); }
	inline bool get_m_WillBeActivatedOnEnable_38() const { return ___m_WillBeActivatedOnEnable_38; }
	inline bool* get_address_of_m_WillBeActivatedOnEnable_38() { return &___m_WillBeActivatedOnEnable_38; }
	inline void set_m_WillBeActivatedOnEnable_38(bool value)
	{
		___m_WillBeActivatedOnEnable_38 = value;
	}
};

struct SpriteTrail_t2284883325_StaticFields
{
public:
	// System.Int32 SpriteTrail::m_TrailCount
	int32_t ___m_TrailCount_21;
	// UnityEngine.GameObject SpriteTrail::m_TrailElementPrefab
	GameObject_t1756533147 * ___m_TrailElementPrefab_23;
	// System.Boolean SpriteTrail::m_LevelRefreshDone
	bool ___m_LevelRefreshDone_39;

public:
	inline static int32_t get_offset_of_m_TrailCount_21() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325_StaticFields, ___m_TrailCount_21)); }
	inline int32_t get_m_TrailCount_21() const { return ___m_TrailCount_21; }
	inline int32_t* get_address_of_m_TrailCount_21() { return &___m_TrailCount_21; }
	inline void set_m_TrailCount_21(int32_t value)
	{
		___m_TrailCount_21 = value;
	}

	inline static int32_t get_offset_of_m_TrailElementPrefab_23() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325_StaticFields, ___m_TrailElementPrefab_23)); }
	inline GameObject_t1756533147 * get_m_TrailElementPrefab_23() const { return ___m_TrailElementPrefab_23; }
	inline GameObject_t1756533147 ** get_address_of_m_TrailElementPrefab_23() { return &___m_TrailElementPrefab_23; }
	inline void set_m_TrailElementPrefab_23(GameObject_t1756533147 * value)
	{
		___m_TrailElementPrefab_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_TrailElementPrefab_23, value);
	}

	inline static int32_t get_offset_of_m_LevelRefreshDone_39() { return static_cast<int32_t>(offsetof(SpriteTrail_t2284883325_StaticFields, ___m_LevelRefreshDone_39)); }
	inline bool get_m_LevelRefreshDone_39() const { return ___m_LevelRefreshDone_39; }
	inline bool* get_address_of_m_LevelRefreshDone_39() { return &___m_LevelRefreshDone_39; }
	inline void set_m_LevelRefreshDone_39(bool value)
	{
		___m_LevelRefreshDone_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
