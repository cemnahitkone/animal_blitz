﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.LevelScene
struct LevelScene_t288242702;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.LevelScene::.ctor()
extern "C"  void LevelScene__ctor_m2006586192 (LevelScene_t288242702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.LevelScene::Awake()
extern "C"  void LevelScene_Awake_m1668748817 (LevelScene_t288242702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.LevelScene::ShowPreviousLevels()
extern "C"  void LevelScene_ShowPreviousLevels_m2832432871 (LevelScene_t288242702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.LevelScene::ShowNextLevels()
extern "C"  void LevelScene_ShowNextLevels_m791446505 (LevelScene_t288242702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.LevelScene::EnablePrevLevelButton()
extern "C"  void LevelScene_EnablePrevLevelButton_m2018851332 (LevelScene_t288242702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.LevelScene::DisablePrevLevelButton()
extern "C"  void LevelScene_DisablePrevLevelButton_m1342890689 (LevelScene_t288242702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.LevelScene::EnableNextLevelButton()
extern "C"  void LevelScene_EnableNextLevelButton_m2320969804 (LevelScene_t288242702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.LevelScene::DisableNextLevelButton()
extern "C"  void LevelScene_DisableNextLevelButton_m1805227839 (LevelScene_t288242702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.LevelScene::SetLevelText(System.Int32)
extern "C"  void LevelScene_SetLevelText_m2929383242 (LevelScene_t288242702 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
