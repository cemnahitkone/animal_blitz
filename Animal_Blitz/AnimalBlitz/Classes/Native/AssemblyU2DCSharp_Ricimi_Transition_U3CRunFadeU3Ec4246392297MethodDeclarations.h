﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.Transition/<RunFade>c__Iterator0
struct U3CRunFadeU3Ec__Iterator0_t4246392297;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.Transition/<RunFade>c__Iterator0::.ctor()
extern "C"  void U3CRunFadeU3Ec__Iterator0__ctor_m617716714 (U3CRunFadeU3Ec__Iterator0_t4246392297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ricimi.Transition/<RunFade>c__Iterator0::MoveNext()
extern "C"  bool U3CRunFadeU3Ec__Iterator0_MoveNext_m603888794 (U3CRunFadeU3Ec__Iterator0_t4246392297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Ricimi.Transition/<RunFade>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRunFadeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1503239844 (U3CRunFadeU3Ec__Iterator0_t4246392297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Ricimi.Transition/<RunFade>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRunFadeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1326117884 (U3CRunFadeU3Ec__Iterator0_t4246392297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.Transition/<RunFade>c__Iterator0::Dispose()
extern "C"  void U3CRunFadeU3Ec__Iterator0_Dispose_m4194118907 (U3CRunFadeU3Ec__Iterator0_t4246392297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.Transition/<RunFade>c__Iterator0::Reset()
extern "C"  void U3CRunFadeU3Ec__Iterator0_Reset_m2195541101 (U3CRunFadeU3Ec__Iterator0_t4246392297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
