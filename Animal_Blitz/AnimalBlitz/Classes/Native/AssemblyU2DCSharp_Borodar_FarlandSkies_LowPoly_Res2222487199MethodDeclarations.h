﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.ResetButton
struct ResetButton_t2222487199;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.ResetButton::.ctor()
extern "C"  void ResetButton__ctor_m1721695732 (ResetButton_t2222487199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.ResetButton::Start()
extern "C"  void ResetButton_Start_m2793515296 (ResetButton_t2222487199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.ResetButton::OnClick()
extern "C"  void ResetButton_OnClick_m633140217 (ResetButton_t2222487199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
