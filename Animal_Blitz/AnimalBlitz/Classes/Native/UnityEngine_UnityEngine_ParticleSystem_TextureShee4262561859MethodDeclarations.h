﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct TextureSheetAnimationModule_t4262561859;
struct TextureSheetAnimationModule_t4262561859_marshaled_pinvoke;
struct TextureSheetAnimationModule_t4262561859_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_TextureShee4262561859.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/TextureSheetAnimationModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void TextureSheetAnimationModule__ctor_m1021096461 (TextureSheetAnimationModule_t4262561859 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct TextureSheetAnimationModule_t4262561859;
struct TextureSheetAnimationModule_t4262561859_marshaled_pinvoke;

extern "C" void TextureSheetAnimationModule_t4262561859_marshal_pinvoke(const TextureSheetAnimationModule_t4262561859& unmarshaled, TextureSheetAnimationModule_t4262561859_marshaled_pinvoke& marshaled);
extern "C" void TextureSheetAnimationModule_t4262561859_marshal_pinvoke_back(const TextureSheetAnimationModule_t4262561859_marshaled_pinvoke& marshaled, TextureSheetAnimationModule_t4262561859& unmarshaled);
extern "C" void TextureSheetAnimationModule_t4262561859_marshal_pinvoke_cleanup(TextureSheetAnimationModule_t4262561859_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct TextureSheetAnimationModule_t4262561859;
struct TextureSheetAnimationModule_t4262561859_marshaled_com;

extern "C" void TextureSheetAnimationModule_t4262561859_marshal_com(const TextureSheetAnimationModule_t4262561859& unmarshaled, TextureSheetAnimationModule_t4262561859_marshaled_com& marshaled);
extern "C" void TextureSheetAnimationModule_t4262561859_marshal_com_back(const TextureSheetAnimationModule_t4262561859_marshaled_com& marshaled, TextureSheetAnimationModule_t4262561859& unmarshaled);
extern "C" void TextureSheetAnimationModule_t4262561859_marshal_com_cleanup(TextureSheetAnimationModule_t4262561859_marshaled_com& marshaled);
