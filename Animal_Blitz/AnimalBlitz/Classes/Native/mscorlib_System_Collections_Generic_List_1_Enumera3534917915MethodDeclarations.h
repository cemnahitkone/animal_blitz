﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingBase>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3300969097(__this, ___l0, method) ((  void (*) (Enumerator_t3534917915 *, List_1_t4000188241 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingBase>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2784680729(__this, method) ((  void (*) (Enumerator_t3534917915 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingBase>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3177901553(__this, method) ((  Il2CppObject * (*) (Enumerator_t3534917915 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingBase>::Dispose()
#define Enumerator_Dispose_m872122252(__this, method) ((  void (*) (Enumerator_t3534917915 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingBase>::VerifyState()
#define Enumerator_VerifyState_m1377181763(__this, method) ((  void (*) (Enumerator_t3534917915 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingBase>::MoveNext()
#define Enumerator_MoveNext_m2290930537(__this, method) ((  bool (*) (Enumerator_t3534917915 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingBase>::get_Current()
#define Enumerator_get_Current_m465102326(__this, method) ((  HighlightingBase_t336099813 * (*) (Enumerator_t3534917915 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
