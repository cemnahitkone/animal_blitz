﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// Borodar.FarlandSkies.LowPoly.DotParams.SkyParam
struct SkyParam_t4272832432;
// Borodar.FarlandSkies.LowPoly.DotParams.StarsParam
struct StarsParam_t907828490;
// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam
struct CelestialParam_t4052651973;
// Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam
struct CloudsParam_t3937665775;
// CFX_AutoDestructShuriken
struct CFX_AutoDestructShuriken_t1500114366;
// CFX_LightIntensityFade
struct CFX_LightIntensityFade_t4221734619;
// tBase
struct tBase_t1720960579;
// TabBase/tabID
struct tabID_t570832297;
// TabBase/baseLegID
struct baseLegID_t3972271149;
// testBle/baseLegID
struct baseLegID_t1303619648;
// TrailElement
struct TrailElement_t1685395368;
// TrailPreset
struct TrailPreset_t465077881;
// SpriteTrail
struct SpriteTrail_t2284883325;

#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot4272832432.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_DotP907828490.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot4052651973.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot3937665775.h"
#include "AssemblyU2DCSharp_CFX_AutoDestructShuriken1500114366.h"
#include "AssemblyU2DCSharp_CFX_LightIntensityFade4221734619.h"
#include "AssemblyU2DCSharp_tBase1720960579.h"
#include "AssemblyU2DCSharp_TabBase_tabID570832297.h"
#include "AssemblyU2DCSharp_TabBase_baseLegID3972271149.h"
#include "AssemblyU2DCSharp_testBle_baseLegID1303619648.h"
#include "AssemblyU2DCSharp_TrailElement1685395368.h"
#include "AssemblyU2DCSharp_TrailPreset465077881.h"
#include "AssemblyU2DCSharp_SpriteTrail2284883325.h"

#pragma once
// Borodar.FarlandSkies.LowPoly.DotParams.SkyParam[]
struct SkyParamU5BU5D_t2503859089  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SkyParam_t4272832432 * m_Items[1];

public:
	inline SkyParam_t4272832432 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SkyParam_t4272832432 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SkyParam_t4272832432 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SkyParam_t4272832432 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SkyParam_t4272832432 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SkyParam_t4272832432 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Borodar.FarlandSkies.LowPoly.DotParams.StarsParam[]
struct StarsParamU5BU5D_t820505487  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StarsParam_t907828490 * m_Items[1];

public:
	inline StarsParam_t907828490 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline StarsParam_t907828490 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, StarsParam_t907828490 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline StarsParam_t907828490 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline StarsParam_t907828490 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, StarsParam_t907828490 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam[]
struct CelestialParamU5BU5D_t3461457800  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CelestialParam_t4052651973 * m_Items[1];

public:
	inline CelestialParam_t4052651973 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CelestialParam_t4052651973 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CelestialParam_t4052651973 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline CelestialParam_t4052651973 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CelestialParam_t4052651973 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CelestialParam_t4052651973 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam[]
struct CloudsParamU5BU5D_t3007299702  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CloudsParam_t3937665775 * m_Items[1];

public:
	inline CloudsParam_t3937665775 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CloudsParam_t3937665775 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CloudsParam_t3937665775 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline CloudsParam_t3937665775 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CloudsParam_t3937665775 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CloudsParam_t3937665775 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CFX_AutoDestructShuriken[]
struct CFX_AutoDestructShurikenU5BU5D_t2040307083  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CFX_AutoDestructShuriken_t1500114366 * m_Items[1];

public:
	inline CFX_AutoDestructShuriken_t1500114366 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CFX_AutoDestructShuriken_t1500114366 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CFX_AutoDestructShuriken_t1500114366 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline CFX_AutoDestructShuriken_t1500114366 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CFX_AutoDestructShuriken_t1500114366 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CFX_AutoDestructShuriken_t1500114366 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CFX_LightIntensityFade[]
struct CFX_LightIntensityFadeU5BU5D_t2555693914  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CFX_LightIntensityFade_t4221734619 * m_Items[1];

public:
	inline CFX_LightIntensityFade_t4221734619 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CFX_LightIntensityFade_t4221734619 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CFX_LightIntensityFade_t4221734619 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline CFX_LightIntensityFade_t4221734619 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CFX_LightIntensityFade_t4221734619 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CFX_LightIntensityFade_t4221734619 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// tBase[]
struct tBaseU5BU5D_t894370130  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) tBase_t1720960579 * m_Items[1];

public:
	inline tBase_t1720960579 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline tBase_t1720960579 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, tBase_t1720960579 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline tBase_t1720960579 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline tBase_t1720960579 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, tBase_t1720960579 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TabBase/tabID[]
struct tabIDU5BU5D_t3964590484  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) tabID_t570832297 * m_Items[1];

public:
	inline tabID_t570832297 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline tabID_t570832297 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, tabID_t570832297 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline tabID_t570832297 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline tabID_t570832297 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, tabID_t570832297 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TabBase/baseLegID[]
struct baseLegIDU5BU5D_t1801249920  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) baseLegID_t3972271149 * m_Items[1];

public:
	inline baseLegID_t3972271149 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline baseLegID_t3972271149 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, baseLegID_t3972271149 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline baseLegID_t3972271149 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline baseLegID_t3972271149 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, baseLegID_t3972271149 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// testBle/baseLegID[]
struct baseLegIDU5BU5D_t2007592641  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) baseLegID_t1303619648 * m_Items[1];

public:
	inline baseLegID_t1303619648 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline baseLegID_t1303619648 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, baseLegID_t1303619648 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline baseLegID_t1303619648 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline baseLegID_t1303619648 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, baseLegID_t1303619648 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TrailElement[]
struct TrailElementU5BU5D_t2201193657  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TrailElement_t1685395368 * m_Items[1];

public:
	inline TrailElement_t1685395368 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TrailElement_t1685395368 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TrailElement_t1685395368 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline TrailElement_t1685395368 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TrailElement_t1685395368 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TrailElement_t1685395368 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TrailPreset[]
struct TrailPresetU5BU5D_t169712516  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TrailPreset_t465077881 * m_Items[1];

public:
	inline TrailPreset_t465077881 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TrailPreset_t465077881 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TrailPreset_t465077881 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline TrailPreset_t465077881 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TrailPreset_t465077881 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TrailPreset_t465077881 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SpriteTrail[]
struct SpriteTrailU5BU5D_t321135600  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SpriteTrail_t2284883325 * m_Items[1];

public:
	inline SpriteTrail_t2284883325 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SpriteTrail_t2284883325 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SpriteTrail_t2284883325 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SpriteTrail_t2284883325 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SpriteTrail_t2284883325 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SpriteTrail_t2284883325 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
