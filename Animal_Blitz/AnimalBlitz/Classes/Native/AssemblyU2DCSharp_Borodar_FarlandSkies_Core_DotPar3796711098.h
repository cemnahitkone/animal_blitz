﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam[]
struct CloudsParamU5BU5D_t3007299702;
// Borodar.FarlandSkies.Core.DotParams.DotParamsList`1<Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam>
struct DotParamsList_1_t3530821536;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1<Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam>
struct  SortedParamsList_1_t3796711098  : public Il2CppObject
{
public:
	// T[] Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1::Params
	CloudsParamU5BU5D_t3007299702* ___Params_0;
	// Borodar.FarlandSkies.Core.DotParams.DotParamsList`1<T> Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1::SortedParams
	DotParamsList_1_t3530821536 * ___SortedParams_1;

public:
	inline static int32_t get_offset_of_Params_0() { return static_cast<int32_t>(offsetof(SortedParamsList_1_t3796711098, ___Params_0)); }
	inline CloudsParamU5BU5D_t3007299702* get_Params_0() const { return ___Params_0; }
	inline CloudsParamU5BU5D_t3007299702** get_address_of_Params_0() { return &___Params_0; }
	inline void set_Params_0(CloudsParamU5BU5D_t3007299702* value)
	{
		___Params_0 = value;
		Il2CppCodeGenWriteBarrier(&___Params_0, value);
	}

	inline static int32_t get_offset_of_SortedParams_1() { return static_cast<int32_t>(offsetof(SortedParamsList_1_t3796711098, ___SortedParams_1)); }
	inline DotParamsList_1_t3530821536 * get_SortedParams_1() const { return ___SortedParams_1; }
	inline DotParamsList_1_t3530821536 ** get_address_of_SortedParams_1() { return &___SortedParams_1; }
	inline void set_SortedParams_1(DotParamsList_1_t3530821536 * value)
	{
		___SortedParams_1 = value;
		Il2CppCodeGenWriteBarrier(&___SortedParams_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
