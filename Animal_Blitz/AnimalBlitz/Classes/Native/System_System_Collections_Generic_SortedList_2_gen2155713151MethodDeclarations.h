﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2<System.Single,System.Object>
struct SortedList_2_t2155713151;
// System.Collections.Generic.IComparer`1<System.Single>
struct IComparer_1_t30973054;
// System.Object
struct Il2CppObject;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>[]
struct KeyValuePair_2U5BU5D_t3257983789;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct IEnumerator_1_t3066806327;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IList`1<System.Single>
struct IList_1_t2617450533;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_System_Collections_Generic_SortedList_2_Enum494119784.h"

// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::.ctor()
extern "C"  void SortedList_2__ctor_m1729224555_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2__ctor_m1729224555(__this, method) ((  void (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2__ctor_m1729224555_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::.ctor(System.Int32)
extern "C"  void SortedList_2__ctor_m2711763692_gshared (SortedList_2_t2155713151 * __this, int32_t ___capacity0, const MethodInfo* method);
#define SortedList_2__ctor_m2711763692(__this, ___capacity0, method) ((  void (*) (SortedList_2_t2155713151 *, int32_t, const MethodInfo*))SortedList_2__ctor_m2711763692_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::.ctor(System.Int32,System.Collections.Generic.IComparer`1<TKey>)
extern "C"  void SortedList_2__ctor_m3946252741_gshared (SortedList_2_t2155713151 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define SortedList_2__ctor_m3946252741(__this, ___capacity0, ___comparer1, method) ((  void (*) (SortedList_2_t2155713151 *, int32_t, Il2CppObject*, const MethodInfo*))SortedList_2__ctor_m3946252741_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::.cctor()
extern "C"  void SortedList_2__cctor_m123454876_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define SortedList_2__cctor_m123454876(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SortedList_2__cctor_m123454876_gshared)(__this /* static, unused */, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool SortedList_2_System_Collections_ICollection_get_IsSynchronized_m2883733903_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_ICollection_get_IsSynchronized_m2883733903(__this, method) ((  bool (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2_System_Collections_ICollection_get_IsSynchronized_m2883733903_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * SortedList_2_System_Collections_ICollection_get_SyncRoot_m189958735_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_ICollection_get_SyncRoot_m189958735(__this, method) ((  Il2CppObject * (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2_System_Collections_ICollection_get_SyncRoot_m189958735_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * SortedList_2_System_Collections_IDictionary_get_Item_m183026243_gshared (SortedList_2_t2155713151 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_Item_m183026243(__this, ___key0, method) ((  Il2CppObject * (*) (SortedList_2_t2155713151 *, Il2CppObject *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_Item_m183026243_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void SortedList_2_System_Collections_IDictionary_set_Item_m1650836330_gshared (SortedList_2_t2155713151 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_set_Item_m1650836330(__this, ___key0, ___value1, method) ((  void (*) (SortedList_2_t2155713151 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_set_Item_m1650836330_gshared)(__this, ___key0, ___value1, method)
// System.Collections.ICollection System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * SortedList_2_System_Collections_IDictionary_get_Keys_m4034771553_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_Keys_m4034771553(__this, method) ((  Il2CppObject * (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_Keys_m4034771553_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * SortedList_2_System_Collections_IDictionary_get_Values_m2053295209_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_Values_m2053295209(__this, method) ((  Il2CppObject * (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_Values_m2053295209_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3659435785_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3659435785(__this, method) ((  bool (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3659435785_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Clear()
extern "C"  void SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Clear_m3183542805_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Clear_m3183542805(__this, method) ((  void (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Clear_m3183542805_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2333964160_gshared (SortedList_2_t2155713151 * __this, KeyValuePair_2U5BU5D_t3257983789* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2333964160(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedList_2_t2155713151 *, KeyValuePair_2U5BU5D_t3257983789*, int32_t, const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2333964160_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1313454404_gshared (SortedList_2_t2155713151 * __this, KeyValuePair_2_t1296315204  ___keyValuePair0, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1313454404(__this, ___keyValuePair0, method) ((  void (*) (SortedList_2_t2155713151 *, KeyValuePair_2_t1296315204 , const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1313454404_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2234949788_gshared (SortedList_2_t2155713151 * __this, KeyValuePair_2_t1296315204  ___keyValuePair0, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2234949788(__this, ___keyValuePair0, method) ((  bool (*) (SortedList_2_t2155713151 *, KeyValuePair_2_t1296315204 , const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2234949788_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m29089843_gshared (SortedList_2_t2155713151 * __this, KeyValuePair_2_t1296315204  ___keyValuePair0, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m29089843(__this, ___keyValuePair0, method) ((  bool (*) (SortedList_2_t2155713151 *, KeyValuePair_2_t1296315204 , const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m29089843_gshared)(__this, ___keyValuePair0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* SortedList_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2346275313_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2346275313(__this, method) ((  Il2CppObject* (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2346275313_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SortedList_2_System_Collections_IEnumerable_GetEnumerator_m1880108690_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IEnumerable_GetEnumerator_m1880108690(__this, method) ((  Il2CppObject * (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2_System_Collections_IEnumerable_GetEnumerator_m1880108690_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void SortedList_2_System_Collections_IDictionary_Add_m3124956037_gshared (SortedList_2_t2155713151 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_Add_m3124956037(__this, ___key0, ___value1, method) ((  void (*) (SortedList_2_t2155713151 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_Add_m3124956037_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool SortedList_2_System_Collections_IDictionary_Contains_m3320572661_gshared (SortedList_2_t2155713151 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_Contains_m3320572661(__this, ___key0, method) ((  bool (*) (SortedList_2_t2155713151 *, Il2CppObject *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_Contains_m3320572661_gshared)(__this, ___key0, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * SortedList_2_System_Collections_IDictionary_GetEnumerator_m1020211388_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_GetEnumerator_m1020211388(__this, method) ((  Il2CppObject * (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_GetEnumerator_m1020211388_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void SortedList_2_System_Collections_IDictionary_Remove_m3985280494_gshared (SortedList_2_t2155713151 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_Remove_m3985280494(__this, ___key0, method) ((  void (*) (SortedList_2_t2155713151 *, Il2CppObject *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_Remove_m3985280494_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void SortedList_2_System_Collections_ICollection_CopyTo_m2678002639_gshared (SortedList_2_t2155713151 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define SortedList_2_System_Collections_ICollection_CopyTo_m2678002639(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedList_2_t2155713151 *, Il2CppArray *, int32_t, const MethodInfo*))SortedList_2_System_Collections_ICollection_CopyTo_m2678002639_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Single,System.Object>::get_Count()
extern "C"  int32_t SortedList_2_get_Count_m3953460655_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2_get_Count_m3953460655(__this, method) ((  int32_t (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2_get_Count_m3953460655_gshared)(__this, method)
// TValue System.Collections.Generic.SortedList`2<System.Single,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * SortedList_2_get_Item_m1048824628_gshared (SortedList_2_t2155713151 * __this, float ___key0, const MethodInfo* method);
#define SortedList_2_get_Item_m1048824628(__this, ___key0, method) ((  Il2CppObject * (*) (SortedList_2_t2155713151 *, float, const MethodInfo*))SortedList_2_get_Item_m1048824628_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::set_Item(TKey,TValue)
extern "C"  void SortedList_2_set_Item_m156738219_gshared (SortedList_2_t2155713151 * __this, float ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedList_2_set_Item_m156738219(__this, ___key0, ___value1, method) ((  void (*) (SortedList_2_t2155713151 *, float, Il2CppObject *, const MethodInfo*))SortedList_2_set_Item_m156738219_gshared)(__this, ___key0, ___value1, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Single,System.Object>::get_Capacity()
extern "C"  int32_t SortedList_2_get_Capacity_m1664848572_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2_get_Capacity_m1664848572(__this, method) ((  int32_t (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2_get_Capacity_m1664848572_gshared)(__this, method)
// System.Collections.Generic.IList`1<TKey> System.Collections.Generic.SortedList`2<System.Single,System.Object>::get_Keys()
extern "C"  Il2CppObject* SortedList_2_get_Keys_m446487849_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2_get_Keys_m446487849(__this, method) ((  Il2CppObject* (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2_get_Keys_m446487849_gshared)(__this, method)
// System.Collections.Generic.IList`1<TValue> System.Collections.Generic.SortedList`2<System.Single,System.Object>::get_Values()
extern "C"  Il2CppObject* SortedList_2_get_Values_m2693547929_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2_get_Values_m2693547929(__this, method) ((  Il2CppObject* (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2_get_Values_m2693547929_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::Add(TKey,TValue)
extern "C"  void SortedList_2_Add_m3259317576_gshared (SortedList_2_t2155713151 * __this, float ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedList_2_Add_m3259317576(__this, ___key0, ___value1, method) ((  void (*) (SortedList_2_t2155713151 *, float, Il2CppObject *, const MethodInfo*))SortedList_2_Add_m3259317576_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2<System.Single,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* SortedList_2_GetEnumerator_m1595810358_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2_GetEnumerator_m1595810358(__this, method) ((  Il2CppObject* (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2_GetEnumerator_m1595810358_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::Clear()
extern "C"  void SortedList_2_Clear_m325015304_gshared (SortedList_2_t2155713151 * __this, const MethodInfo* method);
#define SortedList_2_Clear_m325015304(__this, method) ((  void (*) (SortedList_2_t2155713151 *, const MethodInfo*))SortedList_2_Clear_m325015304_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::RemoveAt(System.Int32)
extern "C"  void SortedList_2_RemoveAt_m3066111341_gshared (SortedList_2_t2155713151 * __this, int32_t ___index0, const MethodInfo* method);
#define SortedList_2_RemoveAt_m3066111341(__this, ___index0, method) ((  void (*) (SortedList_2_t2155713151 *, int32_t, const MethodInfo*))SortedList_2_RemoveAt_m3066111341_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Single,System.Object>::IndexOfKey(TKey)
extern "C"  int32_t SortedList_2_IndexOfKey_m1414598972_gshared (SortedList_2_t2155713151 * __this, float ___key0, const MethodInfo* method);
#define SortedList_2_IndexOfKey_m1414598972(__this, ___key0, method) ((  int32_t (*) (SortedList_2_t2155713151 *, float, const MethodInfo*))SortedList_2_IndexOfKey_m1414598972_gshared)(__this, ___key0, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Single,System.Object>::IndexOfValue(TValue)
extern "C"  int32_t SortedList_2_IndexOfValue_m2882021324_gshared (SortedList_2_t2155713151 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SortedList_2_IndexOfValue_m2882021324(__this, ___value0, method) ((  int32_t (*) (SortedList_2_t2155713151 *, Il2CppObject *, const MethodInfo*))SortedList_2_IndexOfValue_m2882021324_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::EnsureCapacity(System.Int32,System.Int32)
extern "C"  void SortedList_2_EnsureCapacity_m807368997_gshared (SortedList_2_t2155713151 * __this, int32_t ___n0, int32_t ___free1, const MethodInfo* method);
#define SortedList_2_EnsureCapacity_m807368997(__this, ___n0, ___free1, method) ((  void (*) (SortedList_2_t2155713151 *, int32_t, int32_t, const MethodInfo*))SortedList_2_EnsureCapacity_m807368997_gshared)(__this, ___n0, ___free1, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::PutImpl(TKey,TValue,System.Boolean)
extern "C"  void SortedList_2_PutImpl_m924608781_gshared (SortedList_2_t2155713151 * __this, float ___key0, Il2CppObject * ___value1, bool ___overwrite2, const MethodInfo* method);
#define SortedList_2_PutImpl_m924608781(__this, ___key0, ___value1, ___overwrite2, method) ((  void (*) (SortedList_2_t2155713151 *, float, Il2CppObject *, bool, const MethodInfo*))SortedList_2_PutImpl_m924608781_gshared)(__this, ___key0, ___value1, ___overwrite2, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::Init(System.Collections.Generic.IComparer`1<TKey>,System.Int32,System.Boolean)
extern "C"  void SortedList_2_Init_m2434447812_gshared (SortedList_2_t2155713151 * __this, Il2CppObject* ___comparer0, int32_t ___capacity1, bool ___forceSize2, const MethodInfo* method);
#define SortedList_2_Init_m2434447812(__this, ___comparer0, ___capacity1, ___forceSize2, method) ((  void (*) (SortedList_2_t2155713151 *, Il2CppObject*, int32_t, bool, const MethodInfo*))SortedList_2_Init_m2434447812_gshared)(__this, ___comparer0, ___capacity1, ___forceSize2, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,System.Object>::CopyToArray(System.Array,System.Int32,System.Collections.Generic.SortedList`2/EnumeratorMode<TKey,TValue>)
extern "C"  void SortedList_2_CopyToArray_m798904113_gshared (SortedList_2_t2155713151 * __this, Il2CppArray * ___arr0, int32_t ___i1, int32_t ___mode2, const MethodInfo* method);
#define SortedList_2_CopyToArray_m798904113(__this, ___arr0, ___i1, ___mode2, method) ((  void (*) (SortedList_2_t2155713151 *, Il2CppArray *, int32_t, int32_t, const MethodInfo*))SortedList_2_CopyToArray_m798904113_gshared)(__this, ___arr0, ___i1, ___mode2, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Single,System.Object>::Find(TKey)
extern "C"  int32_t SortedList_2_Find_m2937918771_gshared (SortedList_2_t2155713151 * __this, float ___key0, const MethodInfo* method);
#define SortedList_2_Find_m2937918771(__this, ___key0, method) ((  int32_t (*) (SortedList_2_t2155713151 *, float, const MethodInfo*))SortedList_2_Find_m2937918771_gshared)(__this, ___key0, method)
// TKey System.Collections.Generic.SortedList`2<System.Single,System.Object>::ToKey(System.Object)
extern "C"  float SortedList_2_ToKey_m1801778245_gshared (SortedList_2_t2155713151 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedList_2_ToKey_m1801778245(__this, ___key0, method) ((  float (*) (SortedList_2_t2155713151 *, Il2CppObject *, const MethodInfo*))SortedList_2_ToKey_m1801778245_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.SortedList`2<System.Single,System.Object>::ToValue(System.Object)
extern "C"  Il2CppObject * SortedList_2_ToValue_m335286821_gshared (SortedList_2_t2155713151 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SortedList_2_ToValue_m335286821(__this, ___value0, method) ((  Il2CppObject * (*) (SortedList_2_t2155713151 *, Il2CppObject *, const MethodInfo*))SortedList_2_ToValue_m335286821_gshared)(__this, ___value0, method)
// TKey System.Collections.Generic.SortedList`2<System.Single,System.Object>::KeyAt(System.Int32)
extern "C"  float SortedList_2_KeyAt_m300098002_gshared (SortedList_2_t2155713151 * __this, int32_t ___index0, const MethodInfo* method);
#define SortedList_2_KeyAt_m300098002(__this, ___index0, method) ((  float (*) (SortedList_2_t2155713151 *, int32_t, const MethodInfo*))SortedList_2_KeyAt_m300098002_gshared)(__this, ___index0, method)
// TValue System.Collections.Generic.SortedList`2<System.Single,System.Object>::ValueAt(System.Int32)
extern "C"  Il2CppObject * SortedList_2_ValueAt_m2566210962_gshared (SortedList_2_t2155713151 * __this, int32_t ___index0, const MethodInfo* method);
#define SortedList_2_ValueAt_m2566210962(__this, ___index0, method) ((  Il2CppObject * (*) (SortedList_2_t2155713151 *, int32_t, const MethodInfo*))SortedList_2_ValueAt_m2566210962_gshared)(__this, ___index0, method)
