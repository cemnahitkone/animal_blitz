﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.SkyboxCycleManager
struct SkyboxCycleManager_t61875227;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.SkyboxCycleManager::.ctor()
extern "C"  void SkyboxCycleManager__ctor_m2222043216 (SkyboxCycleManager_t61875227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxCycleManager::Start()
extern "C"  void SkyboxCycleManager_Start_m820813140 (SkyboxCycleManager_t61875227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxCycleManager::Update()
extern "C"  void SkyboxCycleManager_Update_m3450361627 (SkyboxCycleManager_t61875227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
