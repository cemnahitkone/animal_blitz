﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1920593194(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2659517882 *, float, CelestialParam_t4052651973 *, const MethodInfo*))KeyValuePair_2__ctor_m303434200_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam>::get_Key()
#define KeyValuePair_2_get_Key_m1296067364(__this, method) ((  float (*) (KeyValuePair_2_t2659517882 *, const MethodInfo*))KeyValuePair_2_get_Key_m4249127398_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3592388845(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2659517882 *, float, const MethodInfo*))KeyValuePair_2_set_Key_m448823159_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam>::get_Value()
#define KeyValuePair_2_get_Value_m4275759332(__this, method) ((  CelestialParam_t4052651973 * (*) (KeyValuePair_2_t2659517882 *, const MethodInfo*))KeyValuePair_2_get_Value_m1014938790_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2040235005(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2659517882 *, CelestialParam_t4052651973 *, const MethodInfo*))KeyValuePair_2_set_Value_m1070856647_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam>::ToString()
#define KeyValuePair_2_ToString_m838709455(__this, method) ((  String_t* (*) (KeyValuePair_2_t2659517882 *, const MethodInfo*))KeyValuePair_2_ToString_m4220912861_gshared)(__this, method)
