﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Borodar.FarlandSkies.LowPoly.DotParams.SkyParamsList
struct SkyParamsList_t3054173379;
// Borodar.FarlandSkies.LowPoly.DotParams.StarsParamsList
struct StarsParamsList_t1887883603;
// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParamsList
struct CelestialParamsList_t3739272824;
// Borodar.FarlandSkies.LowPoly.DotParams.CloudsParamsList
struct CloudsParamsList_t4198706186;
// Borodar.FarlandSkies.LowPoly.SkyboxController
struct SkyboxController_t2759006432;
// Borodar.FarlandSkies.LowPoly.DotParams.SkyParam
struct SkyParam_t4272832432;
// Borodar.FarlandSkies.LowPoly.DotParams.StarsParam
struct StarsParam_t907828490;
// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam
struct CelestialParam_t4052651973;
// Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam
struct CloudsParam_t3937665775;

#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_Helper2804708263.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle
struct  SkyboxDayNightCycle_t2663460034  : public Singleton_1_t2804708263
{
public:
	// Borodar.FarlandSkies.LowPoly.DotParams.SkyParamsList Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_skyParamsList
	SkyParamsList_t3054173379 * ____skyParamsList_3;
	// Borodar.FarlandSkies.LowPoly.DotParams.StarsParamsList Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_starsParamsList
	StarsParamsList_t1887883603 * ____starsParamsList_4;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_sunrise
	float ____sunrise_5;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_sunset
	float ____sunset_6;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_sunAltitude
	float ____sunAltitude_7;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_sunLongitude
	float ____sunLongitude_8;
	// UnityEngine.Vector2 Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_sunOrbit
	Vector2_t2243707579  ____sunOrbit_9;
	// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParamsList Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_sunParamsList
	CelestialParamsList_t3739272824 * ____sunParamsList_10;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_moonrise
	float ____moonrise_11;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_moonset
	float ____moonset_12;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_moonAltitude
	float ____moonAltitude_13;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_moonLongitude
	float ____moonLongitude_14;
	// UnityEngine.Vector2 Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_moonOrbit
	Vector2_t2243707579  ____moonOrbit_15;
	// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParamsList Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_moonParamsList
	CelestialParamsList_t3739272824 * ____moonParamsList_16;
	// Borodar.FarlandSkies.LowPoly.DotParams.CloudsParamsList Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_cloudsParamsList
	CloudsParamsList_t4198706186 * ____cloudsParamsList_17;
	// Borodar.FarlandSkies.LowPoly.SkyboxController Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_skyboxController
	SkyboxController_t2759006432 * ____skyboxController_18;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_sunDuration
	float ____sunDuration_19;
	// UnityEngine.Vector3 Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_sunAttitudeVector
	Vector3_t2243707580  ____sunAttitudeVector_20;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_moonDuration
	float ____moonDuration_21;
	// UnityEngine.Vector3 Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_moonAttitudeVector
	Vector3_t2243707580  ____moonAttitudeVector_22;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::_timeOfDay
	float ____timeOfDay_23;
	// Borodar.FarlandSkies.LowPoly.DotParams.SkyParam Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::<CurrentSkyParam>k__BackingField
	SkyParam_t4272832432 * ___U3CCurrentSkyParamU3Ek__BackingField_24;
	// Borodar.FarlandSkies.LowPoly.DotParams.StarsParam Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::<CurrentStarsParam>k__BackingField
	StarsParam_t907828490 * ___U3CCurrentStarsParamU3Ek__BackingField_25;
	// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::<CurrentSunParam>k__BackingField
	CelestialParam_t4052651973 * ___U3CCurrentSunParamU3Ek__BackingField_26;
	// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::<CurrentMoonParam>k__BackingField
	CelestialParam_t4052651973 * ___U3CCurrentMoonParamU3Ek__BackingField_27;
	// Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle::<CurrentCloudsParam>k__BackingField
	CloudsParam_t3937665775 * ___U3CCurrentCloudsParamU3Ek__BackingField_28;

public:
	inline static int32_t get_offset_of__skyParamsList_3() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____skyParamsList_3)); }
	inline SkyParamsList_t3054173379 * get__skyParamsList_3() const { return ____skyParamsList_3; }
	inline SkyParamsList_t3054173379 ** get_address_of__skyParamsList_3() { return &____skyParamsList_3; }
	inline void set__skyParamsList_3(SkyParamsList_t3054173379 * value)
	{
		____skyParamsList_3 = value;
		Il2CppCodeGenWriteBarrier(&____skyParamsList_3, value);
	}

	inline static int32_t get_offset_of__starsParamsList_4() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____starsParamsList_4)); }
	inline StarsParamsList_t1887883603 * get__starsParamsList_4() const { return ____starsParamsList_4; }
	inline StarsParamsList_t1887883603 ** get_address_of__starsParamsList_4() { return &____starsParamsList_4; }
	inline void set__starsParamsList_4(StarsParamsList_t1887883603 * value)
	{
		____starsParamsList_4 = value;
		Il2CppCodeGenWriteBarrier(&____starsParamsList_4, value);
	}

	inline static int32_t get_offset_of__sunrise_5() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____sunrise_5)); }
	inline float get__sunrise_5() const { return ____sunrise_5; }
	inline float* get_address_of__sunrise_5() { return &____sunrise_5; }
	inline void set__sunrise_5(float value)
	{
		____sunrise_5 = value;
	}

	inline static int32_t get_offset_of__sunset_6() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____sunset_6)); }
	inline float get__sunset_6() const { return ____sunset_6; }
	inline float* get_address_of__sunset_6() { return &____sunset_6; }
	inline void set__sunset_6(float value)
	{
		____sunset_6 = value;
	}

	inline static int32_t get_offset_of__sunAltitude_7() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____sunAltitude_7)); }
	inline float get__sunAltitude_7() const { return ____sunAltitude_7; }
	inline float* get_address_of__sunAltitude_7() { return &____sunAltitude_7; }
	inline void set__sunAltitude_7(float value)
	{
		____sunAltitude_7 = value;
	}

	inline static int32_t get_offset_of__sunLongitude_8() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____sunLongitude_8)); }
	inline float get__sunLongitude_8() const { return ____sunLongitude_8; }
	inline float* get_address_of__sunLongitude_8() { return &____sunLongitude_8; }
	inline void set__sunLongitude_8(float value)
	{
		____sunLongitude_8 = value;
	}

	inline static int32_t get_offset_of__sunOrbit_9() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____sunOrbit_9)); }
	inline Vector2_t2243707579  get__sunOrbit_9() const { return ____sunOrbit_9; }
	inline Vector2_t2243707579 * get_address_of__sunOrbit_9() { return &____sunOrbit_9; }
	inline void set__sunOrbit_9(Vector2_t2243707579  value)
	{
		____sunOrbit_9 = value;
	}

	inline static int32_t get_offset_of__sunParamsList_10() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____sunParamsList_10)); }
	inline CelestialParamsList_t3739272824 * get__sunParamsList_10() const { return ____sunParamsList_10; }
	inline CelestialParamsList_t3739272824 ** get_address_of__sunParamsList_10() { return &____sunParamsList_10; }
	inline void set__sunParamsList_10(CelestialParamsList_t3739272824 * value)
	{
		____sunParamsList_10 = value;
		Il2CppCodeGenWriteBarrier(&____sunParamsList_10, value);
	}

	inline static int32_t get_offset_of__moonrise_11() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____moonrise_11)); }
	inline float get__moonrise_11() const { return ____moonrise_11; }
	inline float* get_address_of__moonrise_11() { return &____moonrise_11; }
	inline void set__moonrise_11(float value)
	{
		____moonrise_11 = value;
	}

	inline static int32_t get_offset_of__moonset_12() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____moonset_12)); }
	inline float get__moonset_12() const { return ____moonset_12; }
	inline float* get_address_of__moonset_12() { return &____moonset_12; }
	inline void set__moonset_12(float value)
	{
		____moonset_12 = value;
	}

	inline static int32_t get_offset_of__moonAltitude_13() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____moonAltitude_13)); }
	inline float get__moonAltitude_13() const { return ____moonAltitude_13; }
	inline float* get_address_of__moonAltitude_13() { return &____moonAltitude_13; }
	inline void set__moonAltitude_13(float value)
	{
		____moonAltitude_13 = value;
	}

	inline static int32_t get_offset_of__moonLongitude_14() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____moonLongitude_14)); }
	inline float get__moonLongitude_14() const { return ____moonLongitude_14; }
	inline float* get_address_of__moonLongitude_14() { return &____moonLongitude_14; }
	inline void set__moonLongitude_14(float value)
	{
		____moonLongitude_14 = value;
	}

	inline static int32_t get_offset_of__moonOrbit_15() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____moonOrbit_15)); }
	inline Vector2_t2243707579  get__moonOrbit_15() const { return ____moonOrbit_15; }
	inline Vector2_t2243707579 * get_address_of__moonOrbit_15() { return &____moonOrbit_15; }
	inline void set__moonOrbit_15(Vector2_t2243707579  value)
	{
		____moonOrbit_15 = value;
	}

	inline static int32_t get_offset_of__moonParamsList_16() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____moonParamsList_16)); }
	inline CelestialParamsList_t3739272824 * get__moonParamsList_16() const { return ____moonParamsList_16; }
	inline CelestialParamsList_t3739272824 ** get_address_of__moonParamsList_16() { return &____moonParamsList_16; }
	inline void set__moonParamsList_16(CelestialParamsList_t3739272824 * value)
	{
		____moonParamsList_16 = value;
		Il2CppCodeGenWriteBarrier(&____moonParamsList_16, value);
	}

	inline static int32_t get_offset_of__cloudsParamsList_17() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____cloudsParamsList_17)); }
	inline CloudsParamsList_t4198706186 * get__cloudsParamsList_17() const { return ____cloudsParamsList_17; }
	inline CloudsParamsList_t4198706186 ** get_address_of__cloudsParamsList_17() { return &____cloudsParamsList_17; }
	inline void set__cloudsParamsList_17(CloudsParamsList_t4198706186 * value)
	{
		____cloudsParamsList_17 = value;
		Il2CppCodeGenWriteBarrier(&____cloudsParamsList_17, value);
	}

	inline static int32_t get_offset_of__skyboxController_18() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____skyboxController_18)); }
	inline SkyboxController_t2759006432 * get__skyboxController_18() const { return ____skyboxController_18; }
	inline SkyboxController_t2759006432 ** get_address_of__skyboxController_18() { return &____skyboxController_18; }
	inline void set__skyboxController_18(SkyboxController_t2759006432 * value)
	{
		____skyboxController_18 = value;
		Il2CppCodeGenWriteBarrier(&____skyboxController_18, value);
	}

	inline static int32_t get_offset_of__sunDuration_19() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____sunDuration_19)); }
	inline float get__sunDuration_19() const { return ____sunDuration_19; }
	inline float* get_address_of__sunDuration_19() { return &____sunDuration_19; }
	inline void set__sunDuration_19(float value)
	{
		____sunDuration_19 = value;
	}

	inline static int32_t get_offset_of__sunAttitudeVector_20() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____sunAttitudeVector_20)); }
	inline Vector3_t2243707580  get__sunAttitudeVector_20() const { return ____sunAttitudeVector_20; }
	inline Vector3_t2243707580 * get_address_of__sunAttitudeVector_20() { return &____sunAttitudeVector_20; }
	inline void set__sunAttitudeVector_20(Vector3_t2243707580  value)
	{
		____sunAttitudeVector_20 = value;
	}

	inline static int32_t get_offset_of__moonDuration_21() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____moonDuration_21)); }
	inline float get__moonDuration_21() const { return ____moonDuration_21; }
	inline float* get_address_of__moonDuration_21() { return &____moonDuration_21; }
	inline void set__moonDuration_21(float value)
	{
		____moonDuration_21 = value;
	}

	inline static int32_t get_offset_of__moonAttitudeVector_22() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____moonAttitudeVector_22)); }
	inline Vector3_t2243707580  get__moonAttitudeVector_22() const { return ____moonAttitudeVector_22; }
	inline Vector3_t2243707580 * get_address_of__moonAttitudeVector_22() { return &____moonAttitudeVector_22; }
	inline void set__moonAttitudeVector_22(Vector3_t2243707580  value)
	{
		____moonAttitudeVector_22 = value;
	}

	inline static int32_t get_offset_of__timeOfDay_23() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ____timeOfDay_23)); }
	inline float get__timeOfDay_23() const { return ____timeOfDay_23; }
	inline float* get_address_of__timeOfDay_23() { return &____timeOfDay_23; }
	inline void set__timeOfDay_23(float value)
	{
		____timeOfDay_23 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentSkyParamU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ___U3CCurrentSkyParamU3Ek__BackingField_24)); }
	inline SkyParam_t4272832432 * get_U3CCurrentSkyParamU3Ek__BackingField_24() const { return ___U3CCurrentSkyParamU3Ek__BackingField_24; }
	inline SkyParam_t4272832432 ** get_address_of_U3CCurrentSkyParamU3Ek__BackingField_24() { return &___U3CCurrentSkyParamU3Ek__BackingField_24; }
	inline void set_U3CCurrentSkyParamU3Ek__BackingField_24(SkyParam_t4272832432 * value)
	{
		___U3CCurrentSkyParamU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentSkyParamU3Ek__BackingField_24, value);
	}

	inline static int32_t get_offset_of_U3CCurrentStarsParamU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ___U3CCurrentStarsParamU3Ek__BackingField_25)); }
	inline StarsParam_t907828490 * get_U3CCurrentStarsParamU3Ek__BackingField_25() const { return ___U3CCurrentStarsParamU3Ek__BackingField_25; }
	inline StarsParam_t907828490 ** get_address_of_U3CCurrentStarsParamU3Ek__BackingField_25() { return &___U3CCurrentStarsParamU3Ek__BackingField_25; }
	inline void set_U3CCurrentStarsParamU3Ek__BackingField_25(StarsParam_t907828490 * value)
	{
		___U3CCurrentStarsParamU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentStarsParamU3Ek__BackingField_25, value);
	}

	inline static int32_t get_offset_of_U3CCurrentSunParamU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ___U3CCurrentSunParamU3Ek__BackingField_26)); }
	inline CelestialParam_t4052651973 * get_U3CCurrentSunParamU3Ek__BackingField_26() const { return ___U3CCurrentSunParamU3Ek__BackingField_26; }
	inline CelestialParam_t4052651973 ** get_address_of_U3CCurrentSunParamU3Ek__BackingField_26() { return &___U3CCurrentSunParamU3Ek__BackingField_26; }
	inline void set_U3CCurrentSunParamU3Ek__BackingField_26(CelestialParam_t4052651973 * value)
	{
		___U3CCurrentSunParamU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentSunParamU3Ek__BackingField_26, value);
	}

	inline static int32_t get_offset_of_U3CCurrentMoonParamU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ___U3CCurrentMoonParamU3Ek__BackingField_27)); }
	inline CelestialParam_t4052651973 * get_U3CCurrentMoonParamU3Ek__BackingField_27() const { return ___U3CCurrentMoonParamU3Ek__BackingField_27; }
	inline CelestialParam_t4052651973 ** get_address_of_U3CCurrentMoonParamU3Ek__BackingField_27() { return &___U3CCurrentMoonParamU3Ek__BackingField_27; }
	inline void set_U3CCurrentMoonParamU3Ek__BackingField_27(CelestialParam_t4052651973 * value)
	{
		___U3CCurrentMoonParamU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentMoonParamU3Ek__BackingField_27, value);
	}

	inline static int32_t get_offset_of_U3CCurrentCloudsParamU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(SkyboxDayNightCycle_t2663460034, ___U3CCurrentCloudsParamU3Ek__BackingField_28)); }
	inline CloudsParam_t3937665775 * get_U3CCurrentCloudsParamU3Ek__BackingField_28() const { return ___U3CCurrentCloudsParamU3Ek__BackingField_28; }
	inline CloudsParam_t3937665775 ** get_address_of_U3CCurrentCloudsParamU3Ek__BackingField_28() { return &___U3CCurrentCloudsParamU3Ek__BackingField_28; }
	inline void set_U3CCurrentCloudsParamU3Ek__BackingField_28(CloudsParam_t3937665775 * value)
	{
		___U3CCurrentCloudsParamU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentCloudsParamU3Ek__BackingField_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
