﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1022910149MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Camera>::.ctor()
#define HashSet_1__ctor_m1263110375(__this, method) ((  void (*) (HashSet_1_t2817889127 *, const MethodInfo*))HashSet_1__ctor_m2858247305_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Camera>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1__ctor_m605735704(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t2817889127 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1__ctor_m3582855242_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<UnityEngine.Camera>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m835005437(__this, method) ((  Il2CppObject* (*) (HashSet_1_t2817889127 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m788997721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Camera>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m792323266(__this, method) ((  bool (*) (HashSet_1_t2817889127 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2633171492_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Camera>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m2686340954(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t2817889127 *, CameraU5BU5D_t3079764780*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m1933244740_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Camera>::System.Collections.Generic.ICollection<T>.Add(T)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3092008290(__this, ___item0, method) ((  void (*) (HashSet_1_t2817889127 *, Camera_t189460977 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3632050820_gshared)(__this, ___item0, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<UnityEngine.Camera>::System.Collections.IEnumerable.GetEnumerator()
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m619555142(__this, method) ((  Il2CppObject * (*) (HashSet_1_t2817889127 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m2498631708_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.Camera>::get_Count()
#define HashSet_1_get_Count_m2124927997(__this, method) ((  int32_t (*) (HashSet_1_t2817889127 *, const MethodInfo*))HashSet_1_get_Count_m4103055329_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Camera>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1_Init_m1652757234(__this, ___capacity0, ___comparer1, method) ((  void (*) (HashSet_1_t2817889127 *, int32_t, Il2CppObject*, const MethodInfo*))HashSet_1_Init_m1258286688_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Camera>::InitArrays(System.Int32)
#define HashSet_1_InitArrays_m430793114(__this, ___size0, method) ((  void (*) (HashSet_1_t2817889127 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m1536879844_gshared)(__this, ___size0, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Camera>::SlotsContainsAt(System.Int32,System.Int32,T)
#define HashSet_1_SlotsContainsAt_m1997307520(__this, ___index0, ___hash1, ___item2, method) ((  bool (*) (HashSet_1_t2817889127 *, int32_t, int32_t, Camera_t189460977 *, const MethodInfo*))HashSet_1_SlotsContainsAt_m219342270_gshared)(__this, ___index0, ___hash1, ___item2, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Camera>::CopyTo(T[],System.Int32)
#define HashSet_1_CopyTo_m2728273630(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t2817889127 *, CameraU5BU5D_t3079764780*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m1750586488_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Camera>::CopyTo(T[],System.Int32,System.Int32)
#define HashSet_1_CopyTo_m2637822173(__this, ___array0, ___index1, ___count2, method) ((  void (*) (HashSet_1_t2817889127 *, CameraU5BU5D_t3079764780*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m4175866709_gshared)(__this, ___array0, ___index1, ___count2, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Camera>::Resize()
#define HashSet_1_Resize_m2401236643(__this, method) ((  void (*) (HashSet_1_t2817889127 *, const MethodInfo*))HashSet_1_Resize_m1435308491_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.Camera>::GetLinkHashCode(System.Int32)
#define HashSet_1_GetLinkHashCode_m3404682379(__this, ___index0, method) ((  int32_t (*) (HashSet_1_t2817889127 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m3972670595_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.Camera>::GetItemHashCode(T)
#define HashSet_1_GetItemHashCode_m403756915(__this, ___item0, method) ((  int32_t (*) (HashSet_1_t2817889127 *, Camera_t189460977 *, const MethodInfo*))HashSet_1_GetItemHashCode_m433445195_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Camera>::Add(T)
#define HashSet_1_Add_m1841985763(__this, ___item0, method) ((  bool (*) (HashSet_1_t2817889127 *, Camera_t189460977 *, const MethodInfo*))HashSet_1_Add_m2918921714_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Camera>::Clear()
#define HashSet_1_Clear_m647573130(__this, method) ((  void (*) (HashSet_1_t2817889127 *, const MethodInfo*))HashSet_1_Clear_m350367572_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Camera>::Contains(T)
#define HashSet_1_Contains_m3473640089(__this, ___item0, method) ((  bool (*) (HashSet_1_t2817889127 *, Camera_t189460977 *, const MethodInfo*))HashSet_1_Contains_m1075264948_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Camera>::Remove(T)
#define HashSet_1_Remove_m1727832982(__this, ___item0, method) ((  bool (*) (HashSet_1_t2817889127 *, Camera_t189460977 *, const MethodInfo*))HashSet_1_Remove_m4157587527_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Camera>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1_GetObjectData_m3252019885(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t2817889127 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1_GetObjectData_m2935317189_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Camera>::OnDeserialization(System.Object)
#define HashSet_1_OnDeserialization_m4102907265(__this, ___sender0, method) ((  void (*) (HashSet_1_t2817889127 *, Il2CppObject *, const MethodInfo*))HashSet_1_OnDeserialization_m1222146673_gshared)(__this, ___sender0, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<UnityEngine.Camera>::GetEnumerator()
#define HashSet_1_GetEnumerator_m3777231887(__this, method) ((  Enumerator_t1306204969  (*) (HashSet_1_t2817889127 *, const MethodInfo*))HashSet_1_GetEnumerator_m623886159_gshared)(__this, method)
