﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.DotParams.StarsParam
struct StarsParam_t907828490;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.DotParams.StarsParam::.ctor()
extern "C"  void StarsParam__ctor_m1990074316 (StarsParam_t907828490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
