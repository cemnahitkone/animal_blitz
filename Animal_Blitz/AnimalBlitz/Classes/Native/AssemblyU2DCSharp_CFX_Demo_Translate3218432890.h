﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_Demo_Translate
struct  CFX_Demo_Translate_t3218432890  : public MonoBehaviour_t1158329972
{
public:
	// System.Single CFX_Demo_Translate::speed
	float ___speed_2;
	// UnityEngine.Vector3 CFX_Demo_Translate::rotation
	Vector3_t2243707580  ___rotation_3;
	// UnityEngine.Vector3 CFX_Demo_Translate::axis
	Vector3_t2243707580  ___axis_4;
	// System.Boolean CFX_Demo_Translate::gravity
	bool ___gravity_5;
	// UnityEngine.Vector3 CFX_Demo_Translate::dir
	Vector3_t2243707580  ___dir_6;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(CFX_Demo_Translate_t3218432890, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_rotation_3() { return static_cast<int32_t>(offsetof(CFX_Demo_Translate_t3218432890, ___rotation_3)); }
	inline Vector3_t2243707580  get_rotation_3() const { return ___rotation_3; }
	inline Vector3_t2243707580 * get_address_of_rotation_3() { return &___rotation_3; }
	inline void set_rotation_3(Vector3_t2243707580  value)
	{
		___rotation_3 = value;
	}

	inline static int32_t get_offset_of_axis_4() { return static_cast<int32_t>(offsetof(CFX_Demo_Translate_t3218432890, ___axis_4)); }
	inline Vector3_t2243707580  get_axis_4() const { return ___axis_4; }
	inline Vector3_t2243707580 * get_address_of_axis_4() { return &___axis_4; }
	inline void set_axis_4(Vector3_t2243707580  value)
	{
		___axis_4 = value;
	}

	inline static int32_t get_offset_of_gravity_5() { return static_cast<int32_t>(offsetof(CFX_Demo_Translate_t3218432890, ___gravity_5)); }
	inline bool get_gravity_5() const { return ___gravity_5; }
	inline bool* get_address_of_gravity_5() { return &___gravity_5; }
	inline void set_gravity_5(bool value)
	{
		___gravity_5 = value;
	}

	inline static int32_t get_offset_of_dir_6() { return static_cast<int32_t>(offsetof(CFX_Demo_Translate_t3218432890, ___dir_6)); }
	inline Vector3_t2243707580  get_dir_6() const { return ___dir_6; }
	inline Vector3_t2243707580 * get_address_of_dir_6() { return &___dir_6; }
	inline void set_dir_6(Vector3_t2243707580  value)
	{
		___dir_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
