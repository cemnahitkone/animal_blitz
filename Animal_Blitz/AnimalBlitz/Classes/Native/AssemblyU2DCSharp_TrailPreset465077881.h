﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Gradient
struct Gradient_t3600583008;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_TrailElementDurationCondition3016874467.h"
#include "AssemblyU2DCSharp_TrailElementSpawnCondition2660531040.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrailPreset
struct  TrailPreset_t465077881  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material TrailPreset::m_SpecialMat
	Material_t193706927 * ___m_SpecialMat_2;
	// System.Boolean TrailPreset::m_UseOnlyAlpha
	bool ___m_UseOnlyAlpha_3;
	// UnityEngine.Gradient TrailPreset::m_TrailColor
	Gradient_t3600583008 * ___m_TrailColor_4;
	// TrailElementDurationCondition TrailPreset::m_TrailElementDurationCondition
	int32_t ___m_TrailElementDurationCondition_5;
	// System.Int32 TrailPreset::m_TrailMaxLength
	int32_t ___m_TrailMaxLength_6;
	// System.Single TrailPreset::m_TrailDuration
	float ___m_TrailDuration_7;
	// TrailElementSpawnCondition TrailPreset::m_TrailElementSpawnCondition
	int32_t ___m_TrailElementSpawnCondition_8;
	// System.Single TrailPreset::m_TimeBetweenSpawns
	float ___m_TimeBetweenSpawns_9;
	// System.Int32 TrailPreset::m_FramesBetweenSpawns
	int32_t ___m_FramesBetweenSpawns_10;
	// System.Single TrailPreset::m_DistanceBetweenSpawns
	float ___m_DistanceBetweenSpawns_11;
	// System.Boolean TrailPreset::m_DistanceCorrection
	bool ___m_DistanceCorrection_12;
	// System.Boolean TrailPreset::m_UseSizeModifier
	bool ___m_UseSizeModifier_13;
	// System.Boolean TrailPreset::m_UsePositionModifier
	bool ___m_UsePositionModifier_14;
	// UnityEngine.AnimationCurve TrailPreset::m_TrailSizeX
	AnimationCurve_t3306541151 * ___m_TrailSizeX_15;
	// UnityEngine.AnimationCurve TrailPreset::m_TrailSizeY
	AnimationCurve_t3306541151 * ___m_TrailSizeY_16;
	// UnityEngine.AnimationCurve TrailPreset::m_TrailPositionX
	AnimationCurve_t3306541151 * ___m_TrailPositionX_17;
	// UnityEngine.AnimationCurve TrailPreset::m_TrailPositionY
	AnimationCurve_t3306541151 * ___m_TrailPositionY_18;

public:
	inline static int32_t get_offset_of_m_SpecialMat_2() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_SpecialMat_2)); }
	inline Material_t193706927 * get_m_SpecialMat_2() const { return ___m_SpecialMat_2; }
	inline Material_t193706927 ** get_address_of_m_SpecialMat_2() { return &___m_SpecialMat_2; }
	inline void set_m_SpecialMat_2(Material_t193706927 * value)
	{
		___m_SpecialMat_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_SpecialMat_2, value);
	}

	inline static int32_t get_offset_of_m_UseOnlyAlpha_3() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_UseOnlyAlpha_3)); }
	inline bool get_m_UseOnlyAlpha_3() const { return ___m_UseOnlyAlpha_3; }
	inline bool* get_address_of_m_UseOnlyAlpha_3() { return &___m_UseOnlyAlpha_3; }
	inline void set_m_UseOnlyAlpha_3(bool value)
	{
		___m_UseOnlyAlpha_3 = value;
	}

	inline static int32_t get_offset_of_m_TrailColor_4() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_TrailColor_4)); }
	inline Gradient_t3600583008 * get_m_TrailColor_4() const { return ___m_TrailColor_4; }
	inline Gradient_t3600583008 ** get_address_of_m_TrailColor_4() { return &___m_TrailColor_4; }
	inline void set_m_TrailColor_4(Gradient_t3600583008 * value)
	{
		___m_TrailColor_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_TrailColor_4, value);
	}

	inline static int32_t get_offset_of_m_TrailElementDurationCondition_5() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_TrailElementDurationCondition_5)); }
	inline int32_t get_m_TrailElementDurationCondition_5() const { return ___m_TrailElementDurationCondition_5; }
	inline int32_t* get_address_of_m_TrailElementDurationCondition_5() { return &___m_TrailElementDurationCondition_5; }
	inline void set_m_TrailElementDurationCondition_5(int32_t value)
	{
		___m_TrailElementDurationCondition_5 = value;
	}

	inline static int32_t get_offset_of_m_TrailMaxLength_6() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_TrailMaxLength_6)); }
	inline int32_t get_m_TrailMaxLength_6() const { return ___m_TrailMaxLength_6; }
	inline int32_t* get_address_of_m_TrailMaxLength_6() { return &___m_TrailMaxLength_6; }
	inline void set_m_TrailMaxLength_6(int32_t value)
	{
		___m_TrailMaxLength_6 = value;
	}

	inline static int32_t get_offset_of_m_TrailDuration_7() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_TrailDuration_7)); }
	inline float get_m_TrailDuration_7() const { return ___m_TrailDuration_7; }
	inline float* get_address_of_m_TrailDuration_7() { return &___m_TrailDuration_7; }
	inline void set_m_TrailDuration_7(float value)
	{
		___m_TrailDuration_7 = value;
	}

	inline static int32_t get_offset_of_m_TrailElementSpawnCondition_8() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_TrailElementSpawnCondition_8)); }
	inline int32_t get_m_TrailElementSpawnCondition_8() const { return ___m_TrailElementSpawnCondition_8; }
	inline int32_t* get_address_of_m_TrailElementSpawnCondition_8() { return &___m_TrailElementSpawnCondition_8; }
	inline void set_m_TrailElementSpawnCondition_8(int32_t value)
	{
		___m_TrailElementSpawnCondition_8 = value;
	}

	inline static int32_t get_offset_of_m_TimeBetweenSpawns_9() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_TimeBetweenSpawns_9)); }
	inline float get_m_TimeBetweenSpawns_9() const { return ___m_TimeBetweenSpawns_9; }
	inline float* get_address_of_m_TimeBetweenSpawns_9() { return &___m_TimeBetweenSpawns_9; }
	inline void set_m_TimeBetweenSpawns_9(float value)
	{
		___m_TimeBetweenSpawns_9 = value;
	}

	inline static int32_t get_offset_of_m_FramesBetweenSpawns_10() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_FramesBetweenSpawns_10)); }
	inline int32_t get_m_FramesBetweenSpawns_10() const { return ___m_FramesBetweenSpawns_10; }
	inline int32_t* get_address_of_m_FramesBetweenSpawns_10() { return &___m_FramesBetweenSpawns_10; }
	inline void set_m_FramesBetweenSpawns_10(int32_t value)
	{
		___m_FramesBetweenSpawns_10 = value;
	}

	inline static int32_t get_offset_of_m_DistanceBetweenSpawns_11() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_DistanceBetweenSpawns_11)); }
	inline float get_m_DistanceBetweenSpawns_11() const { return ___m_DistanceBetweenSpawns_11; }
	inline float* get_address_of_m_DistanceBetweenSpawns_11() { return &___m_DistanceBetweenSpawns_11; }
	inline void set_m_DistanceBetweenSpawns_11(float value)
	{
		___m_DistanceBetweenSpawns_11 = value;
	}

	inline static int32_t get_offset_of_m_DistanceCorrection_12() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_DistanceCorrection_12)); }
	inline bool get_m_DistanceCorrection_12() const { return ___m_DistanceCorrection_12; }
	inline bool* get_address_of_m_DistanceCorrection_12() { return &___m_DistanceCorrection_12; }
	inline void set_m_DistanceCorrection_12(bool value)
	{
		___m_DistanceCorrection_12 = value;
	}

	inline static int32_t get_offset_of_m_UseSizeModifier_13() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_UseSizeModifier_13)); }
	inline bool get_m_UseSizeModifier_13() const { return ___m_UseSizeModifier_13; }
	inline bool* get_address_of_m_UseSizeModifier_13() { return &___m_UseSizeModifier_13; }
	inline void set_m_UseSizeModifier_13(bool value)
	{
		___m_UseSizeModifier_13 = value;
	}

	inline static int32_t get_offset_of_m_UsePositionModifier_14() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_UsePositionModifier_14)); }
	inline bool get_m_UsePositionModifier_14() const { return ___m_UsePositionModifier_14; }
	inline bool* get_address_of_m_UsePositionModifier_14() { return &___m_UsePositionModifier_14; }
	inline void set_m_UsePositionModifier_14(bool value)
	{
		___m_UsePositionModifier_14 = value;
	}

	inline static int32_t get_offset_of_m_TrailSizeX_15() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_TrailSizeX_15)); }
	inline AnimationCurve_t3306541151 * get_m_TrailSizeX_15() const { return ___m_TrailSizeX_15; }
	inline AnimationCurve_t3306541151 ** get_address_of_m_TrailSizeX_15() { return &___m_TrailSizeX_15; }
	inline void set_m_TrailSizeX_15(AnimationCurve_t3306541151 * value)
	{
		___m_TrailSizeX_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_TrailSizeX_15, value);
	}

	inline static int32_t get_offset_of_m_TrailSizeY_16() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_TrailSizeY_16)); }
	inline AnimationCurve_t3306541151 * get_m_TrailSizeY_16() const { return ___m_TrailSizeY_16; }
	inline AnimationCurve_t3306541151 ** get_address_of_m_TrailSizeY_16() { return &___m_TrailSizeY_16; }
	inline void set_m_TrailSizeY_16(AnimationCurve_t3306541151 * value)
	{
		___m_TrailSizeY_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_TrailSizeY_16, value);
	}

	inline static int32_t get_offset_of_m_TrailPositionX_17() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_TrailPositionX_17)); }
	inline AnimationCurve_t3306541151 * get_m_TrailPositionX_17() const { return ___m_TrailPositionX_17; }
	inline AnimationCurve_t3306541151 ** get_address_of_m_TrailPositionX_17() { return &___m_TrailPositionX_17; }
	inline void set_m_TrailPositionX_17(AnimationCurve_t3306541151 * value)
	{
		___m_TrailPositionX_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_TrailPositionX_17, value);
	}

	inline static int32_t get_offset_of_m_TrailPositionY_18() { return static_cast<int32_t>(offsetof(TrailPreset_t465077881, ___m_TrailPositionY_18)); }
	inline AnimationCurve_t3306541151 * get_m_TrailPositionY_18() const { return ___m_TrailPositionY_18; }
	inline AnimationCurve_t3306541151 ** get_address_of_m_TrailPositionY_18() { return &___m_TrailPositionY_18; }
	inline void set_m_TrailPositionY_18(AnimationCurve_t3306541151 * value)
	{
		___m_TrailPositionY_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_TrailPositionY_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
