﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.ColorSwapper
struct  ColorSwapper_t1578650717  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color Ricimi.ColorSwapper::enabledColor
	Color_t2020392075  ___enabledColor_2;
	// UnityEngine.Color Ricimi.ColorSwapper::disabledColor
	Color_t2020392075  ___disabledColor_3;
	// System.Boolean Ricimi.ColorSwapper::m_swapped
	bool ___m_swapped_4;
	// UnityEngine.UI.Image Ricimi.ColorSwapper::m_image
	Image_t2042527209 * ___m_image_5;

public:
	inline static int32_t get_offset_of_enabledColor_2() { return static_cast<int32_t>(offsetof(ColorSwapper_t1578650717, ___enabledColor_2)); }
	inline Color_t2020392075  get_enabledColor_2() const { return ___enabledColor_2; }
	inline Color_t2020392075 * get_address_of_enabledColor_2() { return &___enabledColor_2; }
	inline void set_enabledColor_2(Color_t2020392075  value)
	{
		___enabledColor_2 = value;
	}

	inline static int32_t get_offset_of_disabledColor_3() { return static_cast<int32_t>(offsetof(ColorSwapper_t1578650717, ___disabledColor_3)); }
	inline Color_t2020392075  get_disabledColor_3() const { return ___disabledColor_3; }
	inline Color_t2020392075 * get_address_of_disabledColor_3() { return &___disabledColor_3; }
	inline void set_disabledColor_3(Color_t2020392075  value)
	{
		___disabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_swapped_4() { return static_cast<int32_t>(offsetof(ColorSwapper_t1578650717, ___m_swapped_4)); }
	inline bool get_m_swapped_4() const { return ___m_swapped_4; }
	inline bool* get_address_of_m_swapped_4() { return &___m_swapped_4; }
	inline void set_m_swapped_4(bool value)
	{
		___m_swapped_4 = value;
	}

	inline static int32_t get_offset_of_m_image_5() { return static_cast<int32_t>(offsetof(ColorSwapper_t1578650717, ___m_image_5)); }
	inline Image_t2042527209 * get_m_image_5() const { return ___m_image_5; }
	inline Image_t2042527209 ** get_address_of_m_image_5() { return &___m_image_5; }
	inline void set_m_image_5(Image_t2042527209 * value)
	{
		___m_image_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_image_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
