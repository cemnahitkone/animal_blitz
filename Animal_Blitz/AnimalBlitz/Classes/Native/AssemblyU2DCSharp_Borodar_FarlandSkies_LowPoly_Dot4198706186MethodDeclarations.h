﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.DotParams.CloudsParamsList
struct CloudsParamsList_t4198706186;
// Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam
struct CloudsParam_t3937665775;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.DotParams.CloudsParamsList::.ctor()
extern "C"  void CloudsParamsList__ctor_m459366806 (CloudsParamsList_t4198706186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam Borodar.FarlandSkies.LowPoly.DotParams.CloudsParamsList::GetParamPerTime(System.Single)
extern "C"  CloudsParam_t3937665775 * CloudsParamsList_GetParamPerTime_m1750339255 (CloudsParamsList_t4198706186 * __this, float ___currentTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
