﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>
struct ListKeys_t3081387503;
// System.Collections.Generic.SortedList`2<System.Single,System.Object>
struct SortedList_2_t2155713151;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t3847001055;
// System.Object
struct Il2CppObject;
// System.Array
struct Il2CppArray;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"

// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C"  void ListKeys__ctor_m1969616994_gshared (ListKeys_t3081387503 * __this, SortedList_2_t2155713151 * ___host0, const MethodInfo* method);
#define ListKeys__ctor_m1969616994(__this, ___host0, method) ((  void (*) (ListKeys_t3081387503 *, SortedList_2_t2155713151 *, const MethodInfo*))ListKeys__ctor_m1969616994_gshared)(__this, ___host0, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ListKeys_System_Collections_IEnumerable_GetEnumerator_m2901817983_gshared (ListKeys_t3081387503 * __this, const MethodInfo* method);
#define ListKeys_System_Collections_IEnumerable_GetEnumerator_m2901817983(__this, method) ((  Il2CppObject * (*) (ListKeys_t3081387503 *, const MethodInfo*))ListKeys_System_Collections_IEnumerable_GetEnumerator_m2901817983_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::Add(TKey)
extern "C"  void ListKeys_Add_m2637790048_gshared (ListKeys_t3081387503 * __this, float ___item0, const MethodInfo* method);
#define ListKeys_Add_m2637790048(__this, ___item0, method) ((  void (*) (ListKeys_t3081387503 *, float, const MethodInfo*))ListKeys_Add_m2637790048_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::Remove(TKey)
extern "C"  bool ListKeys_Remove_m2119955111_gshared (ListKeys_t3081387503 * __this, float ___key0, const MethodInfo* method);
#define ListKeys_Remove_m2119955111(__this, ___key0, method) ((  bool (*) (ListKeys_t3081387503 *, float, const MethodInfo*))ListKeys_Remove_m2119955111_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::Clear()
extern "C"  void ListKeys_Clear_m1315472215_gshared (ListKeys_t3081387503 * __this, const MethodInfo* method);
#define ListKeys_Clear_m1315472215(__this, method) ((  void (*) (ListKeys_t3081387503 *, const MethodInfo*))ListKeys_Clear_m1315472215_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void ListKeys_CopyTo_m1938063160_gshared (ListKeys_t3081387503 * __this, SingleU5BU5D_t577127397* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ListKeys_CopyTo_m1938063160(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ListKeys_t3081387503 *, SingleU5BU5D_t577127397*, int32_t, const MethodInfo*))ListKeys_CopyTo_m1938063160_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::Contains(TKey)
extern "C"  bool ListKeys_Contains_m469907648_gshared (ListKeys_t3081387503 * __this, float ___item0, const MethodInfo* method);
#define ListKeys_Contains_m469907648(__this, ___item0, method) ((  bool (*) (ListKeys_t3081387503 *, float, const MethodInfo*))ListKeys_Contains_m469907648_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::IndexOf(TKey)
extern "C"  int32_t ListKeys_IndexOf_m1381910410_gshared (ListKeys_t3081387503 * __this, float ___item0, const MethodInfo* method);
#define ListKeys_IndexOf_m1381910410(__this, ___item0, method) ((  int32_t (*) (ListKeys_t3081387503 *, float, const MethodInfo*))ListKeys_IndexOf_m1381910410_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::Insert(System.Int32,TKey)
extern "C"  void ListKeys_Insert_m3056694359_gshared (ListKeys_t3081387503 * __this, int32_t ___index0, float ___item1, const MethodInfo* method);
#define ListKeys_Insert_m3056694359(__this, ___index0, ___item1, method) ((  void (*) (ListKeys_t3081387503 *, int32_t, float, const MethodInfo*))ListKeys_Insert_m3056694359_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::RemoveAt(System.Int32)
extern "C"  void ListKeys_RemoveAt_m750189028_gshared (ListKeys_t3081387503 * __this, int32_t ___index0, const MethodInfo* method);
#define ListKeys_RemoveAt_m750189028(__this, ___index0, method) ((  void (*) (ListKeys_t3081387503 *, int32_t, const MethodInfo*))ListKeys_RemoveAt_m750189028_gshared)(__this, ___index0, method)
// TKey System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::get_Item(System.Int32)
extern "C"  float ListKeys_get_Item_m2835859319_gshared (ListKeys_t3081387503 * __this, int32_t ___index0, const MethodInfo* method);
#define ListKeys_get_Item_m2835859319(__this, ___index0, method) ((  float (*) (ListKeys_t3081387503 *, int32_t, const MethodInfo*))ListKeys_get_Item_m2835859319_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::set_Item(System.Int32,TKey)
extern "C"  void ListKeys_set_Item_m3451925762_gshared (ListKeys_t3081387503 * __this, int32_t ___index0, float ___value1, const MethodInfo* method);
#define ListKeys_set_Item_m3451925762(__this, ___index0, ___value1, method) ((  void (*) (ListKeys_t3081387503 *, int32_t, float, const MethodInfo*))ListKeys_set_Item_m3451925762_gshared)(__this, ___index0, ___value1, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ListKeys_GetEnumerator_m2085824701_gshared (ListKeys_t3081387503 * __this, const MethodInfo* method);
#define ListKeys_GetEnumerator_m2085824701(__this, method) ((  Il2CppObject* (*) (ListKeys_t3081387503 *, const MethodInfo*))ListKeys_GetEnumerator_m2085824701_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::get_Count()
extern "C"  int32_t ListKeys_get_Count_m205995502_gshared (ListKeys_t3081387503 * __this, const MethodInfo* method);
#define ListKeys_get_Count_m205995502(__this, method) ((  int32_t (*) (ListKeys_t3081387503 *, const MethodInfo*))ListKeys_get_Count_m205995502_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::get_IsSynchronized()
extern "C"  bool ListKeys_get_IsSynchronized_m301381051_gshared (ListKeys_t3081387503 * __this, const MethodInfo* method);
#define ListKeys_get_IsSynchronized_m301381051(__this, method) ((  bool (*) (ListKeys_t3081387503 *, const MethodInfo*))ListKeys_get_IsSynchronized_m301381051_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::get_IsReadOnly()
extern "C"  bool ListKeys_get_IsReadOnly_m2715951835_gshared (ListKeys_t3081387503 * __this, const MethodInfo* method);
#define ListKeys_get_IsReadOnly_m2715951835(__this, method) ((  bool (*) (ListKeys_t3081387503 *, const MethodInfo*))ListKeys_get_IsReadOnly_m2715951835_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::get_SyncRoot()
extern "C"  Il2CppObject * ListKeys_get_SyncRoot_m1158751027_gshared (ListKeys_t3081387503 * __this, const MethodInfo* method);
#define ListKeys_get_SyncRoot_m1158751027(__this, method) ((  Il2CppObject * (*) (ListKeys_t3081387503 *, const MethodInfo*))ListKeys_get_SyncRoot_m1158751027_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Single,System.Object>::CopyTo(System.Array,System.Int32)
extern "C"  void ListKeys_CopyTo_m4027473475_gshared (ListKeys_t3081387503 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ListKeys_CopyTo_m4027473475(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ListKeys_t3081387503 *, Il2CppArray *, int32_t, const MethodInfo*))ListKeys_CopyTo_m4027473475_gshared)(__this, ___array0, ___arrayIndex1, method)
