﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/NoiseModule
struct NoiseModule_t1635569407;
struct NoiseModule_t1635569407_marshaled_pinvoke;
struct NoiseModule_t1635569407_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_NoiseModule1635569407.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/NoiseModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void NoiseModule__ctor_m1547624097 (NoiseModule_t1635569407 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct NoiseModule_t1635569407;
struct NoiseModule_t1635569407_marshaled_pinvoke;

extern "C" void NoiseModule_t1635569407_marshal_pinvoke(const NoiseModule_t1635569407& unmarshaled, NoiseModule_t1635569407_marshaled_pinvoke& marshaled);
extern "C" void NoiseModule_t1635569407_marshal_pinvoke_back(const NoiseModule_t1635569407_marshaled_pinvoke& marshaled, NoiseModule_t1635569407& unmarshaled);
extern "C" void NoiseModule_t1635569407_marshal_pinvoke_cleanup(NoiseModule_t1635569407_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct NoiseModule_t1635569407;
struct NoiseModule_t1635569407_marshaled_com;

extern "C" void NoiseModule_t1635569407_marshal_com(const NoiseModule_t1635569407& unmarshaled, NoiseModule_t1635569407_marshaled_com& marshaled);
extern "C" void NoiseModule_t1635569407_marshal_com_back(const NoiseModule_t1635569407_marshaled_com& marshaled, NoiseModule_t1635569407& unmarshaled);
extern "C" void NoiseModule_t1635569407_marshal_com_cleanup(NoiseModule_t1635569407_marshaled_com& marshaled);
