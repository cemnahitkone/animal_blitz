﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDetection
struct UIDetection_t3199971681;
// UnityEngine.Collider2D
struct Collider2D_t646061738;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"

// System.Void UIDetection::.ctor()
extern "C"  void UIDetection__ctor_m1583400304 (UIDetection_t3199971681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDetection::Start()
extern "C"  void UIDetection_Start_m2145702376 (UIDetection_t3199971681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDetection::Update()
extern "C"  void UIDetection_Update_m2554543749 (UIDetection_t3199971681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDetection::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void UIDetection_OnTriggerEnter2D_m1613736096 (UIDetection_t3199971681 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDetection::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void UIDetection_OnTriggerExit2D_m2591984930 (UIDetection_t3199971681 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
