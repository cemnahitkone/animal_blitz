﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectedButton
struct SelectedButton_t2241840337;

#include "codegen/il2cpp-codegen.h"

// System.Void SelectedButton::.ctor()
extern "C"  void SelectedButton__ctor_m1911760377 (SelectedButton_t2241840337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectedButton::.cctor()
extern "C"  void SelectedButton__cctor_m3332729448 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectedButton::Start()
extern "C"  void SelectedButton_Start_m33131737 (SelectedButton_t2241840337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectedButton::Update()
extern "C"  void SelectedButton_Update_m1751266396 (SelectedButton_t2241840337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectedButton::SetAnimationStart(System.Single)
extern "C"  void SelectedButton_SetAnimationStart_m4141702936 (SelectedButton_t2241840337 * __this, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectedButton::Main()
extern "C"  void SelectedButton_Main_m3514946796 (SelectedButton_t2241840337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
