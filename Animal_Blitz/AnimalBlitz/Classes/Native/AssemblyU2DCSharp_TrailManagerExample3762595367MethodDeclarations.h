﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TrailManagerExample
struct TrailManagerExample_t3762595367;

#include "codegen/il2cpp-codegen.h"

// System.Void TrailManagerExample::.ctor()
extern "C"  void TrailManagerExample__ctor_m1756197966 (TrailManagerExample_t3762595367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrailManagerExample::OnDisable()
extern "C"  void TrailManagerExample_OnDisable_m1649692411 (TrailManagerExample_t3762595367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrailManagerExample::OnEnable()
extern "C"  void TrailManagerExample_OnEnable_m3487594818 (TrailManagerExample_t3762595367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrailManagerExample::ActivateTrail(System.Int32)
extern "C"  void TrailManagerExample_ActivateTrail_m690315824 (TrailManagerExample_t3762595367 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrailManagerExample::GoToNext()
extern "C"  void TrailManagerExample_GoToNext_m4020412622 (TrailManagerExample_t3762595367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrailManagerExample::GoToPrevious()
extern "C"  void TrailManagerExample_GoToPrevious_m2105779650 (TrailManagerExample_t3762595367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
