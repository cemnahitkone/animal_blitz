﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_DotPar2548494618MethodDeclarations.h"

// System.Void Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1<Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::.ctor()
#define SortedParamsList_1__ctor_m3666564434(__this, method) ((  void (*) (SortedParamsList_1_t4131877755 *, const MethodInfo*))SortedParamsList_1__ctor_m4109281813_gshared)(__this, method)
// System.Void Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1<Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::Init()
#define SortedParamsList_1_Init_m3642400698(__this, method) ((  void (*) (SortedParamsList_1_t4131877755 *, const MethodInfo*))SortedParamsList_1_Init_m2929646359_gshared)(__this, method)
// System.Void Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1<Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::Update()
#define SortedParamsList_1_Update_m557314703(__this, method) ((  void (*) (SortedParamsList_1_t4131877755 *, const MethodInfo*))SortedParamsList_1_Update_m4026540864_gshared)(__this, method)
