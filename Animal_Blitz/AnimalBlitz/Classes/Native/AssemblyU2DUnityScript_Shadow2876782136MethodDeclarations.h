﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Shadow
struct Shadow_t2876782136;

#include "codegen/il2cpp-codegen.h"

// System.Void Shadow::.ctor()
extern "C"  void Shadow__ctor_m2668981182 (Shadow_t2876782136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shadow::Start()
extern "C"  void Shadow_Start_m817252038 (Shadow_t2876782136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shadow::LateUpdate()
extern "C"  void Shadow_LateUpdate_m2850280077 (Shadow_t2876782136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shadow::Main()
extern "C"  void Shadow_Main_m726902191 (Shadow_t2876782136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
