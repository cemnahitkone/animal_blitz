﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Canvas
struct Canvas_t209405766;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.PopupOpener
struct  PopupOpener_t3649718601  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Ricimi.PopupOpener::popupPrefab
	GameObject_t1756533147 * ___popupPrefab_2;
	// UnityEngine.Canvas Ricimi.PopupOpener::m_canvas
	Canvas_t209405766 * ___m_canvas_3;

public:
	inline static int32_t get_offset_of_popupPrefab_2() { return static_cast<int32_t>(offsetof(PopupOpener_t3649718601, ___popupPrefab_2)); }
	inline GameObject_t1756533147 * get_popupPrefab_2() const { return ___popupPrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_popupPrefab_2() { return &___popupPrefab_2; }
	inline void set_popupPrefab_2(GameObject_t1756533147 * value)
	{
		___popupPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___popupPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_canvas_3() { return static_cast<int32_t>(offsetof(PopupOpener_t3649718601, ___m_canvas_3)); }
	inline Canvas_t209405766 * get_m_canvas_3() const { return ___m_canvas_3; }
	inline Canvas_t209405766 ** get_address_of_m_canvas_3() { return &___m_canvas_3; }
	inline void set_m_canvas_3(Canvas_t209405766 * value)
	{
		___m_canvas_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_canvas_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
