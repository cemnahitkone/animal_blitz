﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Translate
struct Translate_t3008136214;

#include "codegen/il2cpp-codegen.h"

// System.Void Translate::.ctor()
extern "C"  void Translate__ctor_m2672653978 (Translate_t3008136214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Translate::Start()
extern "C"  void Translate_Start_m2271988410 (Translate_t3008136214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Translate::Update()
extern "C"  void Translate_Update_m2682358481 (Translate_t3008136214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Translate::Main()
extern "C"  void Translate_Main_m3370282563 (Translate_t3008136214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
