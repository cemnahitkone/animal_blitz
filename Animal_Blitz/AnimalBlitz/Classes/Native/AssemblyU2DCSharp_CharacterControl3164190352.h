﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.Dictionary`2<System.Int32,tBase>
struct Dictionary_2_t728786214;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterControl
struct  CharacterControl_t3164190352  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject CharacterControl::EventSys
	GameObject_t1756533147 * ___EventSys_2;
	// System.Boolean CharacterControl::success
	bool ___success_3;
	// UnityEngine.GameObject CharacterControl::successSound
	GameObject_t1756533147 * ___successSound_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,tBase> CharacterControl::_base
	Dictionary_2_t728786214 * ____base_5;
	// UnityEngine.GameObject CharacterControl::DogCharacter
	GameObject_t1756533147 * ___DogCharacter_6;
	// System.Boolean CharacterControl::dogDestroyed
	bool ___dogDestroyed_7;
	// System.Boolean CharacterControl::dogLanded
	bool ___dogLanded_8;
	// System.Single CharacterControl::dogSpeed
	float ___dogSpeed_9;
	// UnityEngine.GameObject CharacterControl::CatCharacter
	GameObject_t1756533147 * ___CatCharacter_10;
	// System.Boolean CharacterControl::catDestroyed
	bool ___catDestroyed_11;
	// System.Boolean CharacterControl::catLanded
	bool ___catLanded_12;
	// System.Single CharacterControl::catSpeed
	float ___catSpeed_13;
	// UnityEngine.GameObject CharacterControl::OwlCharacter
	GameObject_t1756533147 * ___OwlCharacter_14;
	// System.Boolean CharacterControl::owlDestroyed
	bool ___owlDestroyed_15;
	// System.Boolean CharacterControl::owlLanded
	bool ___owlLanded_16;
	// System.Single CharacterControl::owlSpeed
	float ___owlSpeed_17;
	// UnityEngine.GameObject CharacterControl::TurtleCharacter
	GameObject_t1756533147 * ___TurtleCharacter_18;
	// System.Boolean CharacterControl::turtleDestroyed
	bool ___turtleDestroyed_19;
	// System.Boolean CharacterControl::turtleLanded
	bool ___turtleLanded_20;
	// System.Single CharacterControl::turtleSpeed
	float ___turtleSpeed_21;
	// UnityEngine.GameObject CharacterControl::RatCharacter
	GameObject_t1756533147 * ___RatCharacter_22;
	// System.Boolean CharacterControl::ratDestroyed
	bool ___ratDestroyed_23;
	// System.Boolean CharacterControl::ratLanded
	bool ___ratLanded_24;
	// System.Single CharacterControl::ratSpeed
	float ___ratSpeed_25;
	// UnityEngine.GameObject CharacterControl::puff
	GameObject_t1756533147 * ___puff_26;
	// UnityEngine.Vector3 CharacterControl::dogLastPosition
	Vector3_t2243707580  ___dogLastPosition_27;
	// UnityEngine.Vector3 CharacterControl::catLastPosition
	Vector3_t2243707580  ___catLastPosition_28;
	// UnityEngine.Vector3 CharacterControl::owlLastPosition
	Vector3_t2243707580  ___owlLastPosition_29;
	// UnityEngine.Vector3 CharacterControl::ratLastPosition
	Vector3_t2243707580  ___ratLastPosition_30;
	// UnityEngine.Vector3 CharacterControl::turtleLastPosition
	Vector3_t2243707580  ___turtleLastPosition_31;
	// System.Boolean CharacterControl::dogSpace
	bool ___dogSpace_32;
	// System.Boolean CharacterControl::catSpace
	bool ___catSpace_33;
	// System.Boolean CharacterControl::owlSpace
	bool ___owlSpace_34;
	// System.Boolean CharacterControl::ratSpace
	bool ___ratSpace_35;
	// System.Boolean CharacterControl::turtleSpace
	bool ___turtleSpace_36;
	// System.Boolean CharacterControl::freeze
	bool ___freeze_37;
	// System.Boolean CharacterControl::talkDog
	bool ___talkDog_38;

public:
	inline static int32_t get_offset_of_EventSys_2() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___EventSys_2)); }
	inline GameObject_t1756533147 * get_EventSys_2() const { return ___EventSys_2; }
	inline GameObject_t1756533147 ** get_address_of_EventSys_2() { return &___EventSys_2; }
	inline void set_EventSys_2(GameObject_t1756533147 * value)
	{
		___EventSys_2 = value;
		Il2CppCodeGenWriteBarrier(&___EventSys_2, value);
	}

	inline static int32_t get_offset_of_success_3() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___success_3)); }
	inline bool get_success_3() const { return ___success_3; }
	inline bool* get_address_of_success_3() { return &___success_3; }
	inline void set_success_3(bool value)
	{
		___success_3 = value;
	}

	inline static int32_t get_offset_of_successSound_4() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___successSound_4)); }
	inline GameObject_t1756533147 * get_successSound_4() const { return ___successSound_4; }
	inline GameObject_t1756533147 ** get_address_of_successSound_4() { return &___successSound_4; }
	inline void set_successSound_4(GameObject_t1756533147 * value)
	{
		___successSound_4 = value;
		Il2CppCodeGenWriteBarrier(&___successSound_4, value);
	}

	inline static int32_t get_offset_of__base_5() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ____base_5)); }
	inline Dictionary_2_t728786214 * get__base_5() const { return ____base_5; }
	inline Dictionary_2_t728786214 ** get_address_of__base_5() { return &____base_5; }
	inline void set__base_5(Dictionary_2_t728786214 * value)
	{
		____base_5 = value;
		Il2CppCodeGenWriteBarrier(&____base_5, value);
	}

	inline static int32_t get_offset_of_DogCharacter_6() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___DogCharacter_6)); }
	inline GameObject_t1756533147 * get_DogCharacter_6() const { return ___DogCharacter_6; }
	inline GameObject_t1756533147 ** get_address_of_DogCharacter_6() { return &___DogCharacter_6; }
	inline void set_DogCharacter_6(GameObject_t1756533147 * value)
	{
		___DogCharacter_6 = value;
		Il2CppCodeGenWriteBarrier(&___DogCharacter_6, value);
	}

	inline static int32_t get_offset_of_dogDestroyed_7() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___dogDestroyed_7)); }
	inline bool get_dogDestroyed_7() const { return ___dogDestroyed_7; }
	inline bool* get_address_of_dogDestroyed_7() { return &___dogDestroyed_7; }
	inline void set_dogDestroyed_7(bool value)
	{
		___dogDestroyed_7 = value;
	}

	inline static int32_t get_offset_of_dogLanded_8() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___dogLanded_8)); }
	inline bool get_dogLanded_8() const { return ___dogLanded_8; }
	inline bool* get_address_of_dogLanded_8() { return &___dogLanded_8; }
	inline void set_dogLanded_8(bool value)
	{
		___dogLanded_8 = value;
	}

	inline static int32_t get_offset_of_dogSpeed_9() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___dogSpeed_9)); }
	inline float get_dogSpeed_9() const { return ___dogSpeed_9; }
	inline float* get_address_of_dogSpeed_9() { return &___dogSpeed_9; }
	inline void set_dogSpeed_9(float value)
	{
		___dogSpeed_9 = value;
	}

	inline static int32_t get_offset_of_CatCharacter_10() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___CatCharacter_10)); }
	inline GameObject_t1756533147 * get_CatCharacter_10() const { return ___CatCharacter_10; }
	inline GameObject_t1756533147 ** get_address_of_CatCharacter_10() { return &___CatCharacter_10; }
	inline void set_CatCharacter_10(GameObject_t1756533147 * value)
	{
		___CatCharacter_10 = value;
		Il2CppCodeGenWriteBarrier(&___CatCharacter_10, value);
	}

	inline static int32_t get_offset_of_catDestroyed_11() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___catDestroyed_11)); }
	inline bool get_catDestroyed_11() const { return ___catDestroyed_11; }
	inline bool* get_address_of_catDestroyed_11() { return &___catDestroyed_11; }
	inline void set_catDestroyed_11(bool value)
	{
		___catDestroyed_11 = value;
	}

	inline static int32_t get_offset_of_catLanded_12() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___catLanded_12)); }
	inline bool get_catLanded_12() const { return ___catLanded_12; }
	inline bool* get_address_of_catLanded_12() { return &___catLanded_12; }
	inline void set_catLanded_12(bool value)
	{
		___catLanded_12 = value;
	}

	inline static int32_t get_offset_of_catSpeed_13() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___catSpeed_13)); }
	inline float get_catSpeed_13() const { return ___catSpeed_13; }
	inline float* get_address_of_catSpeed_13() { return &___catSpeed_13; }
	inline void set_catSpeed_13(float value)
	{
		___catSpeed_13 = value;
	}

	inline static int32_t get_offset_of_OwlCharacter_14() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___OwlCharacter_14)); }
	inline GameObject_t1756533147 * get_OwlCharacter_14() const { return ___OwlCharacter_14; }
	inline GameObject_t1756533147 ** get_address_of_OwlCharacter_14() { return &___OwlCharacter_14; }
	inline void set_OwlCharacter_14(GameObject_t1756533147 * value)
	{
		___OwlCharacter_14 = value;
		Il2CppCodeGenWriteBarrier(&___OwlCharacter_14, value);
	}

	inline static int32_t get_offset_of_owlDestroyed_15() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___owlDestroyed_15)); }
	inline bool get_owlDestroyed_15() const { return ___owlDestroyed_15; }
	inline bool* get_address_of_owlDestroyed_15() { return &___owlDestroyed_15; }
	inline void set_owlDestroyed_15(bool value)
	{
		___owlDestroyed_15 = value;
	}

	inline static int32_t get_offset_of_owlLanded_16() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___owlLanded_16)); }
	inline bool get_owlLanded_16() const { return ___owlLanded_16; }
	inline bool* get_address_of_owlLanded_16() { return &___owlLanded_16; }
	inline void set_owlLanded_16(bool value)
	{
		___owlLanded_16 = value;
	}

	inline static int32_t get_offset_of_owlSpeed_17() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___owlSpeed_17)); }
	inline float get_owlSpeed_17() const { return ___owlSpeed_17; }
	inline float* get_address_of_owlSpeed_17() { return &___owlSpeed_17; }
	inline void set_owlSpeed_17(float value)
	{
		___owlSpeed_17 = value;
	}

	inline static int32_t get_offset_of_TurtleCharacter_18() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___TurtleCharacter_18)); }
	inline GameObject_t1756533147 * get_TurtleCharacter_18() const { return ___TurtleCharacter_18; }
	inline GameObject_t1756533147 ** get_address_of_TurtleCharacter_18() { return &___TurtleCharacter_18; }
	inline void set_TurtleCharacter_18(GameObject_t1756533147 * value)
	{
		___TurtleCharacter_18 = value;
		Il2CppCodeGenWriteBarrier(&___TurtleCharacter_18, value);
	}

	inline static int32_t get_offset_of_turtleDestroyed_19() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___turtleDestroyed_19)); }
	inline bool get_turtleDestroyed_19() const { return ___turtleDestroyed_19; }
	inline bool* get_address_of_turtleDestroyed_19() { return &___turtleDestroyed_19; }
	inline void set_turtleDestroyed_19(bool value)
	{
		___turtleDestroyed_19 = value;
	}

	inline static int32_t get_offset_of_turtleLanded_20() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___turtleLanded_20)); }
	inline bool get_turtleLanded_20() const { return ___turtleLanded_20; }
	inline bool* get_address_of_turtleLanded_20() { return &___turtleLanded_20; }
	inline void set_turtleLanded_20(bool value)
	{
		___turtleLanded_20 = value;
	}

	inline static int32_t get_offset_of_turtleSpeed_21() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___turtleSpeed_21)); }
	inline float get_turtleSpeed_21() const { return ___turtleSpeed_21; }
	inline float* get_address_of_turtleSpeed_21() { return &___turtleSpeed_21; }
	inline void set_turtleSpeed_21(float value)
	{
		___turtleSpeed_21 = value;
	}

	inline static int32_t get_offset_of_RatCharacter_22() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___RatCharacter_22)); }
	inline GameObject_t1756533147 * get_RatCharacter_22() const { return ___RatCharacter_22; }
	inline GameObject_t1756533147 ** get_address_of_RatCharacter_22() { return &___RatCharacter_22; }
	inline void set_RatCharacter_22(GameObject_t1756533147 * value)
	{
		___RatCharacter_22 = value;
		Il2CppCodeGenWriteBarrier(&___RatCharacter_22, value);
	}

	inline static int32_t get_offset_of_ratDestroyed_23() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___ratDestroyed_23)); }
	inline bool get_ratDestroyed_23() const { return ___ratDestroyed_23; }
	inline bool* get_address_of_ratDestroyed_23() { return &___ratDestroyed_23; }
	inline void set_ratDestroyed_23(bool value)
	{
		___ratDestroyed_23 = value;
	}

	inline static int32_t get_offset_of_ratLanded_24() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___ratLanded_24)); }
	inline bool get_ratLanded_24() const { return ___ratLanded_24; }
	inline bool* get_address_of_ratLanded_24() { return &___ratLanded_24; }
	inline void set_ratLanded_24(bool value)
	{
		___ratLanded_24 = value;
	}

	inline static int32_t get_offset_of_ratSpeed_25() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___ratSpeed_25)); }
	inline float get_ratSpeed_25() const { return ___ratSpeed_25; }
	inline float* get_address_of_ratSpeed_25() { return &___ratSpeed_25; }
	inline void set_ratSpeed_25(float value)
	{
		___ratSpeed_25 = value;
	}

	inline static int32_t get_offset_of_puff_26() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___puff_26)); }
	inline GameObject_t1756533147 * get_puff_26() const { return ___puff_26; }
	inline GameObject_t1756533147 ** get_address_of_puff_26() { return &___puff_26; }
	inline void set_puff_26(GameObject_t1756533147 * value)
	{
		___puff_26 = value;
		Il2CppCodeGenWriteBarrier(&___puff_26, value);
	}

	inline static int32_t get_offset_of_dogLastPosition_27() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___dogLastPosition_27)); }
	inline Vector3_t2243707580  get_dogLastPosition_27() const { return ___dogLastPosition_27; }
	inline Vector3_t2243707580 * get_address_of_dogLastPosition_27() { return &___dogLastPosition_27; }
	inline void set_dogLastPosition_27(Vector3_t2243707580  value)
	{
		___dogLastPosition_27 = value;
	}

	inline static int32_t get_offset_of_catLastPosition_28() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___catLastPosition_28)); }
	inline Vector3_t2243707580  get_catLastPosition_28() const { return ___catLastPosition_28; }
	inline Vector3_t2243707580 * get_address_of_catLastPosition_28() { return &___catLastPosition_28; }
	inline void set_catLastPosition_28(Vector3_t2243707580  value)
	{
		___catLastPosition_28 = value;
	}

	inline static int32_t get_offset_of_owlLastPosition_29() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___owlLastPosition_29)); }
	inline Vector3_t2243707580  get_owlLastPosition_29() const { return ___owlLastPosition_29; }
	inline Vector3_t2243707580 * get_address_of_owlLastPosition_29() { return &___owlLastPosition_29; }
	inline void set_owlLastPosition_29(Vector3_t2243707580  value)
	{
		___owlLastPosition_29 = value;
	}

	inline static int32_t get_offset_of_ratLastPosition_30() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___ratLastPosition_30)); }
	inline Vector3_t2243707580  get_ratLastPosition_30() const { return ___ratLastPosition_30; }
	inline Vector3_t2243707580 * get_address_of_ratLastPosition_30() { return &___ratLastPosition_30; }
	inline void set_ratLastPosition_30(Vector3_t2243707580  value)
	{
		___ratLastPosition_30 = value;
	}

	inline static int32_t get_offset_of_turtleLastPosition_31() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___turtleLastPosition_31)); }
	inline Vector3_t2243707580  get_turtleLastPosition_31() const { return ___turtleLastPosition_31; }
	inline Vector3_t2243707580 * get_address_of_turtleLastPosition_31() { return &___turtleLastPosition_31; }
	inline void set_turtleLastPosition_31(Vector3_t2243707580  value)
	{
		___turtleLastPosition_31 = value;
	}

	inline static int32_t get_offset_of_dogSpace_32() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___dogSpace_32)); }
	inline bool get_dogSpace_32() const { return ___dogSpace_32; }
	inline bool* get_address_of_dogSpace_32() { return &___dogSpace_32; }
	inline void set_dogSpace_32(bool value)
	{
		___dogSpace_32 = value;
	}

	inline static int32_t get_offset_of_catSpace_33() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___catSpace_33)); }
	inline bool get_catSpace_33() const { return ___catSpace_33; }
	inline bool* get_address_of_catSpace_33() { return &___catSpace_33; }
	inline void set_catSpace_33(bool value)
	{
		___catSpace_33 = value;
	}

	inline static int32_t get_offset_of_owlSpace_34() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___owlSpace_34)); }
	inline bool get_owlSpace_34() const { return ___owlSpace_34; }
	inline bool* get_address_of_owlSpace_34() { return &___owlSpace_34; }
	inline void set_owlSpace_34(bool value)
	{
		___owlSpace_34 = value;
	}

	inline static int32_t get_offset_of_ratSpace_35() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___ratSpace_35)); }
	inline bool get_ratSpace_35() const { return ___ratSpace_35; }
	inline bool* get_address_of_ratSpace_35() { return &___ratSpace_35; }
	inline void set_ratSpace_35(bool value)
	{
		___ratSpace_35 = value;
	}

	inline static int32_t get_offset_of_turtleSpace_36() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___turtleSpace_36)); }
	inline bool get_turtleSpace_36() const { return ___turtleSpace_36; }
	inline bool* get_address_of_turtleSpace_36() { return &___turtleSpace_36; }
	inline void set_turtleSpace_36(bool value)
	{
		___turtleSpace_36 = value;
	}

	inline static int32_t get_offset_of_freeze_37() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___freeze_37)); }
	inline bool get_freeze_37() const { return ___freeze_37; }
	inline bool* get_address_of_freeze_37() { return &___freeze_37; }
	inline void set_freeze_37(bool value)
	{
		___freeze_37 = value;
	}

	inline static int32_t get_offset_of_talkDog_38() { return static_cast<int32_t>(offsetof(CharacterControl_t3164190352, ___talkDog_38)); }
	inline bool get_talkDog_38() const { return ___talkDog_38; }
	inline bool* get_address_of_talkDog_38() { return &___talkDog_38; }
	inline void set_talkDog_38(bool value)
	{
		___talkDog_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
