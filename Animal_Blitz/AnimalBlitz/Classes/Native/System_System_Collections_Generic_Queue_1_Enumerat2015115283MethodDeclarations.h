﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat3019169210MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<TrailElement>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m425297381(__this, ___q0, method) ((  void (*) (Enumerator_t2015115283 *, Queue_1_t1505052203 *, const MethodInfo*))Enumerator__ctor_m677001007_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<TrailElement>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4270337242(__this, method) ((  void (*) (Enumerator_t2015115283 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m373072478_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<TrailElement>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m233405542(__this, method) ((  Il2CppObject * (*) (Enumerator_t2015115283 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2167685344_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<TrailElement>::Dispose()
#define Enumerator_Dispose_m3669259067(__this, method) ((  void (*) (Enumerator_t2015115283 *, const MethodInfo*))Enumerator_Dispose_m575349149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<TrailElement>::MoveNext()
#define Enumerator_MoveNext_m481530403(__this, method) ((  bool (*) (Enumerator_t2015115283 *, const MethodInfo*))Enumerator_MoveNext_m742418190_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<TrailElement>::get_Current()
#define Enumerator_get_Current_m640616911(__this, method) ((  TrailElement_t1685395368 * (*) (Enumerator_t2015115283 *, const MethodInfo*))Enumerator_get_Current_m1613610405_gshared)(__this, method)
