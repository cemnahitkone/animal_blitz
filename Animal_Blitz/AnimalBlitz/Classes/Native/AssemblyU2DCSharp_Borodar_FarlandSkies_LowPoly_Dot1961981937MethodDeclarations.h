﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.DotParams.MoonParamsList
struct MoonParamsList_t1961981937;
// Borodar.FarlandSkies.LowPoly.DotParams.MoonParam
struct MoonParam_t2066856486;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.DotParams.MoonParamsList::.ctor(System.Int32)
extern "C"  void MoonParamsList__ctor_m2720588654 (MoonParamsList_t1961981937 * __this, int32_t ___capacity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Borodar.FarlandSkies.LowPoly.DotParams.MoonParam Borodar.FarlandSkies.LowPoly.DotParams.MoonParamsList::GetParamPerTime(System.Single)
extern "C"  MoonParam_t2066856486 * MoonParamsList_GetParamPerTime_m3611856987 (MoonParamsList_t1961981937 * __this, float ___currentTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
