﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue
struct  DefaultValue_t3484749624  : public Il2CppObject
{
public:

public:
};

struct DefaultValue_t3484749624_StaticFields
{
public:
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::TopColor
	Color_t2020392075  ___TopColor_0;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::MiddleColor
	Color_t2020392075  ___MiddleColor_1;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::BottomColor
	Color_t2020392075  ___BottomColor_2;
	// System.Single Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::TopExponent
	float ___TopExponent_3;
	// System.Single Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::BottomExponent
	float ___BottomExponent_4;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::StarsTint
	Color_t2020392075  ___StarsTint_5;
	// System.Single Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::StarsExtinction
	float ___StarsExtinction_6;
	// System.Single Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::StarsTwinklingSpeed
	float ___StarsTwinklingSpeed_7;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::SunTint
	Color_t2020392075  ___SunTint_8;
	// System.Single Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::SunSize
	float ___SunSize_9;
	// System.Boolean Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::SunFlare
	bool ___SunFlare_10;
	// System.Single Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::SunFlareBrightness
	float ___SunFlareBrightness_11;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::MoonTint
	Color_t2020392075  ___MoonTint_12;
	// System.Single Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::MoonSize
	float ___MoonSize_13;
	// System.Boolean Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::MoonFlare
	bool ___MoonFlare_14;
	// System.Single Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::MoonFlareBrightness
	float ___MoonFlareBrightness_15;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::CloudsTint
	Color_t2020392075  ___CloudsTint_16;
	// System.Single Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::CloudsRotation
	float ___CloudsRotation_17;
	// System.Single Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::CloudsHeight
	float ___CloudsHeight_18;
	// System.Single Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::Exposure
	float ___Exposure_19;
	// System.Boolean Borodar.FarlandSkies.LowPoly.ResetButton/DefaultValue::AdjustFog
	bool ___AdjustFog_20;

public:
	inline static int32_t get_offset_of_TopColor_0() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___TopColor_0)); }
	inline Color_t2020392075  get_TopColor_0() const { return ___TopColor_0; }
	inline Color_t2020392075 * get_address_of_TopColor_0() { return &___TopColor_0; }
	inline void set_TopColor_0(Color_t2020392075  value)
	{
		___TopColor_0 = value;
	}

	inline static int32_t get_offset_of_MiddleColor_1() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___MiddleColor_1)); }
	inline Color_t2020392075  get_MiddleColor_1() const { return ___MiddleColor_1; }
	inline Color_t2020392075 * get_address_of_MiddleColor_1() { return &___MiddleColor_1; }
	inline void set_MiddleColor_1(Color_t2020392075  value)
	{
		___MiddleColor_1 = value;
	}

	inline static int32_t get_offset_of_BottomColor_2() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___BottomColor_2)); }
	inline Color_t2020392075  get_BottomColor_2() const { return ___BottomColor_2; }
	inline Color_t2020392075 * get_address_of_BottomColor_2() { return &___BottomColor_2; }
	inline void set_BottomColor_2(Color_t2020392075  value)
	{
		___BottomColor_2 = value;
	}

	inline static int32_t get_offset_of_TopExponent_3() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___TopExponent_3)); }
	inline float get_TopExponent_3() const { return ___TopExponent_3; }
	inline float* get_address_of_TopExponent_3() { return &___TopExponent_3; }
	inline void set_TopExponent_3(float value)
	{
		___TopExponent_3 = value;
	}

	inline static int32_t get_offset_of_BottomExponent_4() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___BottomExponent_4)); }
	inline float get_BottomExponent_4() const { return ___BottomExponent_4; }
	inline float* get_address_of_BottomExponent_4() { return &___BottomExponent_4; }
	inline void set_BottomExponent_4(float value)
	{
		___BottomExponent_4 = value;
	}

	inline static int32_t get_offset_of_StarsTint_5() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___StarsTint_5)); }
	inline Color_t2020392075  get_StarsTint_5() const { return ___StarsTint_5; }
	inline Color_t2020392075 * get_address_of_StarsTint_5() { return &___StarsTint_5; }
	inline void set_StarsTint_5(Color_t2020392075  value)
	{
		___StarsTint_5 = value;
	}

	inline static int32_t get_offset_of_StarsExtinction_6() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___StarsExtinction_6)); }
	inline float get_StarsExtinction_6() const { return ___StarsExtinction_6; }
	inline float* get_address_of_StarsExtinction_6() { return &___StarsExtinction_6; }
	inline void set_StarsExtinction_6(float value)
	{
		___StarsExtinction_6 = value;
	}

	inline static int32_t get_offset_of_StarsTwinklingSpeed_7() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___StarsTwinklingSpeed_7)); }
	inline float get_StarsTwinklingSpeed_7() const { return ___StarsTwinklingSpeed_7; }
	inline float* get_address_of_StarsTwinklingSpeed_7() { return &___StarsTwinklingSpeed_7; }
	inline void set_StarsTwinklingSpeed_7(float value)
	{
		___StarsTwinklingSpeed_7 = value;
	}

	inline static int32_t get_offset_of_SunTint_8() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___SunTint_8)); }
	inline Color_t2020392075  get_SunTint_8() const { return ___SunTint_8; }
	inline Color_t2020392075 * get_address_of_SunTint_8() { return &___SunTint_8; }
	inline void set_SunTint_8(Color_t2020392075  value)
	{
		___SunTint_8 = value;
	}

	inline static int32_t get_offset_of_SunSize_9() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___SunSize_9)); }
	inline float get_SunSize_9() const { return ___SunSize_9; }
	inline float* get_address_of_SunSize_9() { return &___SunSize_9; }
	inline void set_SunSize_9(float value)
	{
		___SunSize_9 = value;
	}

	inline static int32_t get_offset_of_SunFlare_10() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___SunFlare_10)); }
	inline bool get_SunFlare_10() const { return ___SunFlare_10; }
	inline bool* get_address_of_SunFlare_10() { return &___SunFlare_10; }
	inline void set_SunFlare_10(bool value)
	{
		___SunFlare_10 = value;
	}

	inline static int32_t get_offset_of_SunFlareBrightness_11() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___SunFlareBrightness_11)); }
	inline float get_SunFlareBrightness_11() const { return ___SunFlareBrightness_11; }
	inline float* get_address_of_SunFlareBrightness_11() { return &___SunFlareBrightness_11; }
	inline void set_SunFlareBrightness_11(float value)
	{
		___SunFlareBrightness_11 = value;
	}

	inline static int32_t get_offset_of_MoonTint_12() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___MoonTint_12)); }
	inline Color_t2020392075  get_MoonTint_12() const { return ___MoonTint_12; }
	inline Color_t2020392075 * get_address_of_MoonTint_12() { return &___MoonTint_12; }
	inline void set_MoonTint_12(Color_t2020392075  value)
	{
		___MoonTint_12 = value;
	}

	inline static int32_t get_offset_of_MoonSize_13() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___MoonSize_13)); }
	inline float get_MoonSize_13() const { return ___MoonSize_13; }
	inline float* get_address_of_MoonSize_13() { return &___MoonSize_13; }
	inline void set_MoonSize_13(float value)
	{
		___MoonSize_13 = value;
	}

	inline static int32_t get_offset_of_MoonFlare_14() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___MoonFlare_14)); }
	inline bool get_MoonFlare_14() const { return ___MoonFlare_14; }
	inline bool* get_address_of_MoonFlare_14() { return &___MoonFlare_14; }
	inline void set_MoonFlare_14(bool value)
	{
		___MoonFlare_14 = value;
	}

	inline static int32_t get_offset_of_MoonFlareBrightness_15() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___MoonFlareBrightness_15)); }
	inline float get_MoonFlareBrightness_15() const { return ___MoonFlareBrightness_15; }
	inline float* get_address_of_MoonFlareBrightness_15() { return &___MoonFlareBrightness_15; }
	inline void set_MoonFlareBrightness_15(float value)
	{
		___MoonFlareBrightness_15 = value;
	}

	inline static int32_t get_offset_of_CloudsTint_16() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___CloudsTint_16)); }
	inline Color_t2020392075  get_CloudsTint_16() const { return ___CloudsTint_16; }
	inline Color_t2020392075 * get_address_of_CloudsTint_16() { return &___CloudsTint_16; }
	inline void set_CloudsTint_16(Color_t2020392075  value)
	{
		___CloudsTint_16 = value;
	}

	inline static int32_t get_offset_of_CloudsRotation_17() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___CloudsRotation_17)); }
	inline float get_CloudsRotation_17() const { return ___CloudsRotation_17; }
	inline float* get_address_of_CloudsRotation_17() { return &___CloudsRotation_17; }
	inline void set_CloudsRotation_17(float value)
	{
		___CloudsRotation_17 = value;
	}

	inline static int32_t get_offset_of_CloudsHeight_18() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___CloudsHeight_18)); }
	inline float get_CloudsHeight_18() const { return ___CloudsHeight_18; }
	inline float* get_address_of_CloudsHeight_18() { return &___CloudsHeight_18; }
	inline void set_CloudsHeight_18(float value)
	{
		___CloudsHeight_18 = value;
	}

	inline static int32_t get_offset_of_Exposure_19() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___Exposure_19)); }
	inline float get_Exposure_19() const { return ___Exposure_19; }
	inline float* get_address_of_Exposure_19() { return &___Exposure_19; }
	inline void set_Exposure_19(float value)
	{
		___Exposure_19 = value;
	}

	inline static int32_t get_offset_of_AdjustFog_20() { return static_cast<int32_t>(offsetof(DefaultValue_t3484749624_StaticFields, ___AdjustFog_20)); }
	inline bool get_AdjustFog_20() const { return ___AdjustFog_20; }
	inline bool* get_address_of_AdjustFog_20() { return &___AdjustFog_20; }
	inline void set_AdjustFog_20(bool value)
	{
		___AdjustFog_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
