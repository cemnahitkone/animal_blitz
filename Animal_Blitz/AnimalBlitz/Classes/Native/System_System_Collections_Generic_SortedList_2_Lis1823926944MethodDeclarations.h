﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>
struct ListKeys_t1823926944;
// System.Collections.Generic.SortedList`2<System.Object,System.Object>
struct SortedList_2_t898252592;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"

// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C"  void ListKeys__ctor_m2319640143_gshared (ListKeys_t1823926944 * __this, SortedList_2_t898252592 * ___host0, const MethodInfo* method);
#define ListKeys__ctor_m2319640143(__this, ___host0, method) ((  void (*) (ListKeys_t1823926944 *, SortedList_2_t898252592 *, const MethodInfo*))ListKeys__ctor_m2319640143_gshared)(__this, ___host0, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ListKeys_System_Collections_IEnumerable_GetEnumerator_m2293917290_gshared (ListKeys_t1823926944 * __this, const MethodInfo* method);
#define ListKeys_System_Collections_IEnumerable_GetEnumerator_m2293917290(__this, method) ((  Il2CppObject * (*) (ListKeys_t1823926944 *, const MethodInfo*))ListKeys_System_Collections_IEnumerable_GetEnumerator_m2293917290_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::Add(TKey)
extern "C"  void ListKeys_Add_m1351494523_gshared (ListKeys_t1823926944 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListKeys_Add_m1351494523(__this, ___item0, method) ((  void (*) (ListKeys_t1823926944 *, Il2CppObject *, const MethodInfo*))ListKeys_Add_m1351494523_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::Remove(TKey)
extern "C"  bool ListKeys_Remove_m3338713928_gshared (ListKeys_t1823926944 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ListKeys_Remove_m3338713928(__this, ___key0, method) ((  bool (*) (ListKeys_t1823926944 *, Il2CppObject *, const MethodInfo*))ListKeys_Remove_m3338713928_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::Clear()
extern "C"  void ListKeys_Clear_m4148664532_gshared (ListKeys_t1823926944 * __this, const MethodInfo* method);
#define ListKeys_Clear_m4148664532(__this, method) ((  void (*) (ListKeys_t1823926944 *, const MethodInfo*))ListKeys_Clear_m4148664532_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void ListKeys_CopyTo_m2097607607_gshared (ListKeys_t1823926944 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ListKeys_CopyTo_m2097607607(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ListKeys_t1823926944 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ListKeys_CopyTo_m2097607607_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::Contains(TKey)
extern "C"  bool ListKeys_Contains_m1772360189_gshared (ListKeys_t1823926944 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListKeys_Contains_m1772360189(__this, ___item0, method) ((  bool (*) (ListKeys_t1823926944 *, Il2CppObject *, const MethodInfo*))ListKeys_Contains_m1772360189_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::IndexOf(TKey)
extern "C"  int32_t ListKeys_IndexOf_m4168868843_gshared (ListKeys_t1823926944 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListKeys_IndexOf_m4168868843(__this, ___item0, method) ((  int32_t (*) (ListKeys_t1823926944 *, Il2CppObject *, const MethodInfo*))ListKeys_IndexOf_m4168868843_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::Insert(System.Int32,TKey)
extern "C"  void ListKeys_Insert_m2280306040_gshared (ListKeys_t1823926944 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define ListKeys_Insert_m2280306040(__this, ___index0, ___item1, method) ((  void (*) (ListKeys_t1823926944 *, int32_t, Il2CppObject *, const MethodInfo*))ListKeys_Insert_m2280306040_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::RemoveAt(System.Int32)
extern "C"  void ListKeys_RemoveAt_m3226719819_gshared (ListKeys_t1823926944 * __this, int32_t ___index0, const MethodInfo* method);
#define ListKeys_RemoveAt_m3226719819(__this, ___index0, method) ((  void (*) (ListKeys_t1823926944 *, int32_t, const MethodInfo*))ListKeys_RemoveAt_m3226719819_gshared)(__this, ___index0, method)
// TKey System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * ListKeys_get_Item_m141454342_gshared (ListKeys_t1823926944 * __this, int32_t ___index0, const MethodInfo* method);
#define ListKeys_get_Item_m141454342(__this, ___index0, method) ((  Il2CppObject * (*) (ListKeys_t1823926944 *, int32_t, const MethodInfo*))ListKeys_get_Item_m141454342_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::set_Item(System.Int32,TKey)
extern "C"  void ListKeys_set_Item_m659525115_gshared (ListKeys_t1823926944 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ListKeys_set_Item_m659525115(__this, ___index0, ___value1, method) ((  void (*) (ListKeys_t1823926944 *, int32_t, Il2CppObject *, const MethodInfo*))ListKeys_set_Item_m659525115_gshared)(__this, ___index0, ___value1, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ListKeys_GetEnumerator_m2249072104_gshared (ListKeys_t1823926944 * __this, const MethodInfo* method);
#define ListKeys_GetEnumerator_m2249072104(__this, method) ((  Il2CppObject* (*) (ListKeys_t1823926944 *, const MethodInfo*))ListKeys_GetEnumerator_m2249072104_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::get_Count()
extern "C"  int32_t ListKeys_get_Count_m731890585_gshared (ListKeys_t1823926944 * __this, const MethodInfo* method);
#define ListKeys_get_Count_m731890585(__this, method) ((  int32_t (*) (ListKeys_t1823926944 *, const MethodInfo*))ListKeys_get_Count_m731890585_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::get_IsSynchronized()
extern "C"  bool ListKeys_get_IsSynchronized_m1494348682_gshared (ListKeys_t1823926944 * __this, const MethodInfo* method);
#define ListKeys_get_IsSynchronized_m1494348682(__this, method) ((  bool (*) (ListKeys_t1823926944 *, const MethodInfo*))ListKeys_get_IsSynchronized_m1494348682_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::get_IsReadOnly()
extern "C"  bool ListKeys_get_IsReadOnly_m1567592094_gshared (ListKeys_t1823926944 * __this, const MethodInfo* method);
#define ListKeys_get_IsReadOnly_m1567592094(__this, method) ((  bool (*) (ListKeys_t1823926944 *, const MethodInfo*))ListKeys_get_IsReadOnly_m1567592094_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::get_SyncRoot()
extern "C"  Il2CppObject * ListKeys_get_SyncRoot_m2849850774_gshared (ListKeys_t1823926944 * __this, const MethodInfo* method);
#define ListKeys_get_SyncRoot_m2849850774(__this, method) ((  Il2CppObject * (*) (ListKeys_t1823926944 *, const MethodInfo*))ListKeys_get_SyncRoot_m2849850774_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::CopyTo(System.Array,System.Int32)
extern "C"  void ListKeys_CopyTo_m2223671254_gshared (ListKeys_t1823926944 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ListKeys_CopyTo_m2223671254(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ListKeys_t1823926944 *, Il2CppArray *, int32_t, const MethodInfo*))ListKeys_CopyTo_m2223671254_gshared)(__this, ___array0, ___arrayIndex1, method)
