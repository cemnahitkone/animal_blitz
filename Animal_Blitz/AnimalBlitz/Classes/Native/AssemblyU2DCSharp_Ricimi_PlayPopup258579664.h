﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;

#include "AssemblyU2DCSharp_Ricimi_Popup1187979376.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.PlayPopup
struct  PlayPopup_t258579664  : public Popup_t1187979376
{
public:
	// UnityEngine.Color Ricimi.PlayPopup::enabledColor
	Color_t2020392075  ___enabledColor_4;
	// UnityEngine.Color Ricimi.PlayPopup::disabledColor
	Color_t2020392075  ___disabledColor_5;
	// UnityEngine.UI.Image Ricimi.PlayPopup::leftStarImage
	Image_t2042527209 * ___leftStarImage_6;
	// UnityEngine.UI.Image Ricimi.PlayPopup::middleStarImage
	Image_t2042527209 * ___middleStarImage_7;
	// UnityEngine.UI.Image Ricimi.PlayPopup::rightStarImage
	Image_t2042527209 * ___rightStarImage_8;

public:
	inline static int32_t get_offset_of_enabledColor_4() { return static_cast<int32_t>(offsetof(PlayPopup_t258579664, ___enabledColor_4)); }
	inline Color_t2020392075  get_enabledColor_4() const { return ___enabledColor_4; }
	inline Color_t2020392075 * get_address_of_enabledColor_4() { return &___enabledColor_4; }
	inline void set_enabledColor_4(Color_t2020392075  value)
	{
		___enabledColor_4 = value;
	}

	inline static int32_t get_offset_of_disabledColor_5() { return static_cast<int32_t>(offsetof(PlayPopup_t258579664, ___disabledColor_5)); }
	inline Color_t2020392075  get_disabledColor_5() const { return ___disabledColor_5; }
	inline Color_t2020392075 * get_address_of_disabledColor_5() { return &___disabledColor_5; }
	inline void set_disabledColor_5(Color_t2020392075  value)
	{
		___disabledColor_5 = value;
	}

	inline static int32_t get_offset_of_leftStarImage_6() { return static_cast<int32_t>(offsetof(PlayPopup_t258579664, ___leftStarImage_6)); }
	inline Image_t2042527209 * get_leftStarImage_6() const { return ___leftStarImage_6; }
	inline Image_t2042527209 ** get_address_of_leftStarImage_6() { return &___leftStarImage_6; }
	inline void set_leftStarImage_6(Image_t2042527209 * value)
	{
		___leftStarImage_6 = value;
		Il2CppCodeGenWriteBarrier(&___leftStarImage_6, value);
	}

	inline static int32_t get_offset_of_middleStarImage_7() { return static_cast<int32_t>(offsetof(PlayPopup_t258579664, ___middleStarImage_7)); }
	inline Image_t2042527209 * get_middleStarImage_7() const { return ___middleStarImage_7; }
	inline Image_t2042527209 ** get_address_of_middleStarImage_7() { return &___middleStarImage_7; }
	inline void set_middleStarImage_7(Image_t2042527209 * value)
	{
		___middleStarImage_7 = value;
		Il2CppCodeGenWriteBarrier(&___middleStarImage_7, value);
	}

	inline static int32_t get_offset_of_rightStarImage_8() { return static_cast<int32_t>(offsetof(PlayPopup_t258579664, ___rightStarImage_8)); }
	inline Image_t2042527209 * get_rightStarImage_8() const { return ___rightStarImage_8; }
	inline Image_t2042527209 ** get_address_of_rightStarImage_8() { return &___rightStarImage_8; }
	inline void set_rightStarImage_8(Image_t2042527209 * value)
	{
		___rightStarImage_8 = value;
		Il2CppCodeGenWriteBarrier(&___rightStarImage_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
