﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TabBase/baseLegID
struct baseLegID_t3972271149;

#include "codegen/il2cpp-codegen.h"

// System.Void TabBase/baseLegID::.ctor()
extern "C"  void baseLegID__ctor_m693884990 (baseLegID_t3972271149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
