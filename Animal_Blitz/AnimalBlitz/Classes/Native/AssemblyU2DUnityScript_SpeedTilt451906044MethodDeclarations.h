﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpeedTilt
struct SpeedTilt_t451906044;

#include "codegen/il2cpp-codegen.h"

// System.Void SpeedTilt::.ctor()
extern "C"  void SpeedTilt__ctor_m164590832 (SpeedTilt_t451906044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedTilt::Start()
extern "C"  void SpeedTilt_Start_m821491888 (SpeedTilt_t451906044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedTilt::Update()
extern "C"  void SpeedTilt_Update_m2355129713 (SpeedTilt_t451906044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedTilt::Main()
extern "C"  void SpeedTilt_Main_m337249227 (SpeedTilt_t451906044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
