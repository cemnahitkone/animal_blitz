﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.Core.DotParams.DotParamsList`1<System.Object>
struct DotParamsList_1_t2282605056;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.Core.DotParams.DotParamsList`1<System.Object>::.ctor(System.Int32)
extern "C"  void DotParamsList_1__ctor_m2564647030_gshared (DotParamsList_1_t2282605056 * __this, int32_t ___capacity0, const MethodInfo* method);
#define DotParamsList_1__ctor_m2564647030(__this, ___capacity0, method) ((  void (*) (DotParamsList_1_t2282605056 *, int32_t, const MethodInfo*))DotParamsList_1__ctor_m2564647030_gshared)(__this, ___capacity0, method)
// System.Int32 Borodar.FarlandSkies.Core.DotParams.DotParamsList`1<System.Object>::FindIndexPerTime(System.Single)
extern "C"  int32_t DotParamsList_1_FindIndexPerTime_m910888095_gshared (DotParamsList_1_t2282605056 * __this, float ___time0, const MethodInfo* method);
#define DotParamsList_1_FindIndexPerTime_m910888095(__this, ___time0, method) ((  int32_t (*) (DotParamsList_1_t2282605056 *, float, const MethodInfo*))DotParamsList_1_FindIndexPerTime_m910888095_gshared)(__this, ___time0, method)
