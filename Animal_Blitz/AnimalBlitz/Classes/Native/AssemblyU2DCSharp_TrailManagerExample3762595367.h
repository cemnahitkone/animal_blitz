﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SpriteTrail[]
struct SpriteTrailU5BU5D_t321135600;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrailManagerExample
struct  TrailManagerExample_t3762595367  : public MonoBehaviour_t1158329972
{
public:
	// SpriteTrail[] TrailManagerExample::m_Trails
	SpriteTrailU5BU5D_t321135600* ___m_Trails_2;
	// System.Int32 TrailManagerExample::m_CurrentTrailIndex
	int32_t ___m_CurrentTrailIndex_3;
	// UnityEngine.GameObject TrailManagerExample::m_Character
	GameObject_t1756533147 * ___m_Character_4;
	// UnityEngine.GameObject TrailManagerExample::m_UI
	GameObject_t1756533147 * ___m_UI_5;

public:
	inline static int32_t get_offset_of_m_Trails_2() { return static_cast<int32_t>(offsetof(TrailManagerExample_t3762595367, ___m_Trails_2)); }
	inline SpriteTrailU5BU5D_t321135600* get_m_Trails_2() const { return ___m_Trails_2; }
	inline SpriteTrailU5BU5D_t321135600** get_address_of_m_Trails_2() { return &___m_Trails_2; }
	inline void set_m_Trails_2(SpriteTrailU5BU5D_t321135600* value)
	{
		___m_Trails_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Trails_2, value);
	}

	inline static int32_t get_offset_of_m_CurrentTrailIndex_3() { return static_cast<int32_t>(offsetof(TrailManagerExample_t3762595367, ___m_CurrentTrailIndex_3)); }
	inline int32_t get_m_CurrentTrailIndex_3() const { return ___m_CurrentTrailIndex_3; }
	inline int32_t* get_address_of_m_CurrentTrailIndex_3() { return &___m_CurrentTrailIndex_3; }
	inline void set_m_CurrentTrailIndex_3(int32_t value)
	{
		___m_CurrentTrailIndex_3 = value;
	}

	inline static int32_t get_offset_of_m_Character_4() { return static_cast<int32_t>(offsetof(TrailManagerExample_t3762595367, ___m_Character_4)); }
	inline GameObject_t1756533147 * get_m_Character_4() const { return ___m_Character_4; }
	inline GameObject_t1756533147 ** get_address_of_m_Character_4() { return &___m_Character_4; }
	inline void set_m_Character_4(GameObject_t1756533147 * value)
	{
		___m_Character_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_Character_4, value);
	}

	inline static int32_t get_offset_of_m_UI_5() { return static_cast<int32_t>(offsetof(TrailManagerExample_t3762595367, ___m_UI_5)); }
	inline GameObject_t1756533147 * get_m_UI_5() const { return ___m_UI_5; }
	inline GameObject_t1756533147 ** get_address_of_m_UI_5() { return &___m_UI_5; }
	inline void set_m_UI_5(GameObject_t1756533147 * value)
	{
		___m_UI_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_UI_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
