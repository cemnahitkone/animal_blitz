﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// tBase
struct tBase_t1720960579;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TabBase/tabID
struct  tabID_t570832297  : public Il2CppObject
{
public:
	// tBase TabBase/tabID::getPos
	tBase_t1720960579 * ___getPos_0;
	// System.Int32 TabBase/tabID::id
	int32_t ___id_1;
	// System.Boolean TabBase/tabID::changedData
	bool ___changedData_2;
	// System.Int32 TabBase/tabID::addCounter
	int32_t ___addCounter_3;
	// System.Int32 TabBase/tabID::removeCounter
	int32_t ___removeCounter_4;
	// System.Int32 TabBase/tabID::posAverageCounter
	int32_t ___posAverageCounter_5;

public:
	inline static int32_t get_offset_of_getPos_0() { return static_cast<int32_t>(offsetof(tabID_t570832297, ___getPos_0)); }
	inline tBase_t1720960579 * get_getPos_0() const { return ___getPos_0; }
	inline tBase_t1720960579 ** get_address_of_getPos_0() { return &___getPos_0; }
	inline void set_getPos_0(tBase_t1720960579 * value)
	{
		___getPos_0 = value;
		Il2CppCodeGenWriteBarrier(&___getPos_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(tabID_t570832297, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_changedData_2() { return static_cast<int32_t>(offsetof(tabID_t570832297, ___changedData_2)); }
	inline bool get_changedData_2() const { return ___changedData_2; }
	inline bool* get_address_of_changedData_2() { return &___changedData_2; }
	inline void set_changedData_2(bool value)
	{
		___changedData_2 = value;
	}

	inline static int32_t get_offset_of_addCounter_3() { return static_cast<int32_t>(offsetof(tabID_t570832297, ___addCounter_3)); }
	inline int32_t get_addCounter_3() const { return ___addCounter_3; }
	inline int32_t* get_address_of_addCounter_3() { return &___addCounter_3; }
	inline void set_addCounter_3(int32_t value)
	{
		___addCounter_3 = value;
	}

	inline static int32_t get_offset_of_removeCounter_4() { return static_cast<int32_t>(offsetof(tabID_t570832297, ___removeCounter_4)); }
	inline int32_t get_removeCounter_4() const { return ___removeCounter_4; }
	inline int32_t* get_address_of_removeCounter_4() { return &___removeCounter_4; }
	inline void set_removeCounter_4(int32_t value)
	{
		___removeCounter_4 = value;
	}

	inline static int32_t get_offset_of_posAverageCounter_5() { return static_cast<int32_t>(offsetof(tabID_t570832297, ___posAverageCounter_5)); }
	inline int32_t get_posAverageCounter_5() const { return ___posAverageCounter_5; }
	inline int32_t* get_address_of_posAverageCounter_5() { return &___posAverageCounter_5; }
	inline void set_posAverageCounter_5(int32_t value)
	{
		___posAverageCounter_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
