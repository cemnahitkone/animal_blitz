﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/ExternalForcesModule
struct ExternalForcesModule_t2596349650;
struct ExternalForcesModule_t2596349650_marshaled_pinvoke;
struct ExternalForcesModule_t2596349650_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ExternalFor2596349650.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/ExternalForcesModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ExternalForcesModule__ctor_m3913332068 (ExternalForcesModule_t2596349650 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct ExternalForcesModule_t2596349650;
struct ExternalForcesModule_t2596349650_marshaled_pinvoke;

extern "C" void ExternalForcesModule_t2596349650_marshal_pinvoke(const ExternalForcesModule_t2596349650& unmarshaled, ExternalForcesModule_t2596349650_marshaled_pinvoke& marshaled);
extern "C" void ExternalForcesModule_t2596349650_marshal_pinvoke_back(const ExternalForcesModule_t2596349650_marshaled_pinvoke& marshaled, ExternalForcesModule_t2596349650& unmarshaled);
extern "C" void ExternalForcesModule_t2596349650_marshal_pinvoke_cleanup(ExternalForcesModule_t2596349650_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ExternalForcesModule_t2596349650;
struct ExternalForcesModule_t2596349650_marshaled_com;

extern "C" void ExternalForcesModule_t2596349650_marshal_com(const ExternalForcesModule_t2596349650& unmarshaled, ExternalForcesModule_t2596349650_marshaled_com& marshaled);
extern "C" void ExternalForcesModule_t2596349650_marshal_com_back(const ExternalForcesModule_t2596349650_marshaled_com& marshaled, ExternalForcesModule_t2596349650& unmarshaled);
extern "C" void ExternalForcesModule_t2596349650_marshal_com_cleanup(ExternalForcesModule_t2596349650_marshaled_com& marshaled);
