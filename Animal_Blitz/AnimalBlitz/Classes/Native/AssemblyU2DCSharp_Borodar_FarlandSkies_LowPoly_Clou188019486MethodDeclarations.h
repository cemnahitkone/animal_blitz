﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.CloudsRotationButton
struct CloudsRotationButton_t188019486;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.CloudsRotationButton::.ctor()
extern "C"  void CloudsRotationButton__ctor_m233199429 (CloudsRotationButton_t188019486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.CloudsRotationButton::Update()
extern "C"  void CloudsRotationButton_Update_m1161644816 (CloudsRotationButton_t188019486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.CloudsRotationButton::ToggleCloudsRotation()
extern "C"  void CloudsRotationButton_ToggleCloudsRotation_m3341412021 (CloudsRotationButton_t188019486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
