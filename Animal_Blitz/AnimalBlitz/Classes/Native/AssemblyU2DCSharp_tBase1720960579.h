﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// tBase
struct  tBase_t1720960579  : public Il2CppObject
{
public:
	// System.Boolean tBase::state
	bool ___state_0;
	// System.Int32 tBase::x
	int32_t ___x_1;
	// System.Int32 tBase::y
	int32_t ___y_2;
	// System.Single tBase::rotation
	float ___rotation_3;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(tBase_t1720960579, ___state_0)); }
	inline bool get_state_0() const { return ___state_0; }
	inline bool* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(bool value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(tBase_t1720960579, ___x_1)); }
	inline int32_t get_x_1() const { return ___x_1; }
	inline int32_t* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(int32_t value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(tBase_t1720960579, ___y_2)); }
	inline int32_t get_y_2() const { return ___y_2; }
	inline int32_t* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(int32_t value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_rotation_3() { return static_cast<int32_t>(offsetof(tBase_t1720960579, ___rotation_3)); }
	inline float get_rotation_3() const { return ___rotation_3; }
	inline float* get_address_of_rotation_3() { return &___rotation_3; }
	inline void set_rotation_3(float value)
	{
		___rotation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
