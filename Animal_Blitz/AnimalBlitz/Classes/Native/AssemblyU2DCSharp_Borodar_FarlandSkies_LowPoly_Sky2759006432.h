﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Cubemap
struct Cubemap_t3472944757;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Light
struct Light_t494725636;
// UnityEngine.LensFlare
struct LensFlare_t529161798;

#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_Helper2900254661.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.SkyboxController
struct  SkyboxController_t2759006432  : public Singleton_1_t2900254661
{
public:
	// UnityEngine.Material Borodar.FarlandSkies.LowPoly.SkyboxController::SkyboxMaterial
	Material_t193706927 * ___SkyboxMaterial_3;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.SkyboxController::_topColor
	Color_t2020392075  ____topColor_4;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.SkyboxController::_middleColor
	Color_t2020392075  ____middleColor_5;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.SkyboxController::_bottomColor
	Color_t2020392075  ____bottomColor_6;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::_topExponent
	float ____topExponent_7;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::_bottomExponent
	float ____bottomExponent_8;
	// System.Boolean Borodar.FarlandSkies.LowPoly.SkyboxController::_starsEnabled
	bool ____starsEnabled_9;
	// UnityEngine.Cubemap Borodar.FarlandSkies.LowPoly.SkyboxController::_starsCubemap
	Cubemap_t3472944757 * ____starsCubemap_10;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.SkyboxController::_starsTint
	Color_t2020392075  ____starsTint_11;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::_starsExtinction
	float ____starsExtinction_12;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::_starsTwinklingSpeed
	float ____starsTwinklingSpeed_13;
	// System.Boolean Borodar.FarlandSkies.LowPoly.SkyboxController::_sunEnabled
	bool ____sunEnabled_14;
	// UnityEngine.Texture2D Borodar.FarlandSkies.LowPoly.SkyboxController::_sunTexture
	Texture2D_t3542995729 * ____sunTexture_15;
	// UnityEngine.Light Borodar.FarlandSkies.LowPoly.SkyboxController::_sunLight
	Light_t494725636 * ____sunLight_16;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.SkyboxController::_sunTint
	Color_t2020392075  ____sunTint_17;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::_sunSize
	float ____sunSize_18;
	// System.Boolean Borodar.FarlandSkies.LowPoly.SkyboxController::_sunFlare
	bool ____sunFlare_19;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::_sunFlareBrightness
	float ____sunFlareBrightness_20;
	// System.Boolean Borodar.FarlandSkies.LowPoly.SkyboxController::_moonEnabled
	bool ____moonEnabled_21;
	// UnityEngine.Texture2D Borodar.FarlandSkies.LowPoly.SkyboxController::_moonTexture
	Texture2D_t3542995729 * ____moonTexture_22;
	// UnityEngine.Light Borodar.FarlandSkies.LowPoly.SkyboxController::_moonLight
	Light_t494725636 * ____moonLight_23;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.SkyboxController::_moonTint
	Color_t2020392075  ____moonTint_24;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::_moonSize
	float ____moonSize_25;
	// System.Boolean Borodar.FarlandSkies.LowPoly.SkyboxController::_moonFlare
	bool ____moonFlare_26;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::_moonFlareBrightness
	float ____moonFlareBrightness_27;
	// System.Boolean Borodar.FarlandSkies.LowPoly.SkyboxController::_cloudsEnabled
	bool ____cloudsEnabled_28;
	// UnityEngine.Cubemap Borodar.FarlandSkies.LowPoly.SkyboxController::_cloudsCubemap
	Cubemap_t3472944757 * ____cloudsCubemap_29;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.SkyboxController::_cloudsTint
	Color_t2020392075  ____cloudsTint_30;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::_cloudsHeight
	float ____cloudsHeight_31;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::_cloudsRotation
	float ____cloudsRotation_32;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::_exposure
	float ____exposure_33;
	// System.Boolean Borodar.FarlandSkies.LowPoly.SkyboxController::_adjustFogColor
	bool ____adjustFogColor_34;
	// UnityEngine.LensFlare Borodar.FarlandSkies.LowPoly.SkyboxController::_sunFlareComponent
	LensFlare_t529161798 * ____sunFlareComponent_35;
	// UnityEngine.LensFlare Borodar.FarlandSkies.LowPoly.SkyboxController::_moonFlareComponent
	LensFlare_t529161798 * ____moonFlareComponent_36;

public:
	inline static int32_t get_offset_of_SkyboxMaterial_3() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ___SkyboxMaterial_3)); }
	inline Material_t193706927 * get_SkyboxMaterial_3() const { return ___SkyboxMaterial_3; }
	inline Material_t193706927 ** get_address_of_SkyboxMaterial_3() { return &___SkyboxMaterial_3; }
	inline void set_SkyboxMaterial_3(Material_t193706927 * value)
	{
		___SkyboxMaterial_3 = value;
		Il2CppCodeGenWriteBarrier(&___SkyboxMaterial_3, value);
	}

	inline static int32_t get_offset_of__topColor_4() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____topColor_4)); }
	inline Color_t2020392075  get__topColor_4() const { return ____topColor_4; }
	inline Color_t2020392075 * get_address_of__topColor_4() { return &____topColor_4; }
	inline void set__topColor_4(Color_t2020392075  value)
	{
		____topColor_4 = value;
	}

	inline static int32_t get_offset_of__middleColor_5() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____middleColor_5)); }
	inline Color_t2020392075  get__middleColor_5() const { return ____middleColor_5; }
	inline Color_t2020392075 * get_address_of__middleColor_5() { return &____middleColor_5; }
	inline void set__middleColor_5(Color_t2020392075  value)
	{
		____middleColor_5 = value;
	}

	inline static int32_t get_offset_of__bottomColor_6() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____bottomColor_6)); }
	inline Color_t2020392075  get__bottomColor_6() const { return ____bottomColor_6; }
	inline Color_t2020392075 * get_address_of__bottomColor_6() { return &____bottomColor_6; }
	inline void set__bottomColor_6(Color_t2020392075  value)
	{
		____bottomColor_6 = value;
	}

	inline static int32_t get_offset_of__topExponent_7() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____topExponent_7)); }
	inline float get__topExponent_7() const { return ____topExponent_7; }
	inline float* get_address_of__topExponent_7() { return &____topExponent_7; }
	inline void set__topExponent_7(float value)
	{
		____topExponent_7 = value;
	}

	inline static int32_t get_offset_of__bottomExponent_8() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____bottomExponent_8)); }
	inline float get__bottomExponent_8() const { return ____bottomExponent_8; }
	inline float* get_address_of__bottomExponent_8() { return &____bottomExponent_8; }
	inline void set__bottomExponent_8(float value)
	{
		____bottomExponent_8 = value;
	}

	inline static int32_t get_offset_of__starsEnabled_9() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____starsEnabled_9)); }
	inline bool get__starsEnabled_9() const { return ____starsEnabled_9; }
	inline bool* get_address_of__starsEnabled_9() { return &____starsEnabled_9; }
	inline void set__starsEnabled_9(bool value)
	{
		____starsEnabled_9 = value;
	}

	inline static int32_t get_offset_of__starsCubemap_10() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____starsCubemap_10)); }
	inline Cubemap_t3472944757 * get__starsCubemap_10() const { return ____starsCubemap_10; }
	inline Cubemap_t3472944757 ** get_address_of__starsCubemap_10() { return &____starsCubemap_10; }
	inline void set__starsCubemap_10(Cubemap_t3472944757 * value)
	{
		____starsCubemap_10 = value;
		Il2CppCodeGenWriteBarrier(&____starsCubemap_10, value);
	}

	inline static int32_t get_offset_of__starsTint_11() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____starsTint_11)); }
	inline Color_t2020392075  get__starsTint_11() const { return ____starsTint_11; }
	inline Color_t2020392075 * get_address_of__starsTint_11() { return &____starsTint_11; }
	inline void set__starsTint_11(Color_t2020392075  value)
	{
		____starsTint_11 = value;
	}

	inline static int32_t get_offset_of__starsExtinction_12() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____starsExtinction_12)); }
	inline float get__starsExtinction_12() const { return ____starsExtinction_12; }
	inline float* get_address_of__starsExtinction_12() { return &____starsExtinction_12; }
	inline void set__starsExtinction_12(float value)
	{
		____starsExtinction_12 = value;
	}

	inline static int32_t get_offset_of__starsTwinklingSpeed_13() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____starsTwinklingSpeed_13)); }
	inline float get__starsTwinklingSpeed_13() const { return ____starsTwinklingSpeed_13; }
	inline float* get_address_of__starsTwinklingSpeed_13() { return &____starsTwinklingSpeed_13; }
	inline void set__starsTwinklingSpeed_13(float value)
	{
		____starsTwinklingSpeed_13 = value;
	}

	inline static int32_t get_offset_of__sunEnabled_14() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____sunEnabled_14)); }
	inline bool get__sunEnabled_14() const { return ____sunEnabled_14; }
	inline bool* get_address_of__sunEnabled_14() { return &____sunEnabled_14; }
	inline void set__sunEnabled_14(bool value)
	{
		____sunEnabled_14 = value;
	}

	inline static int32_t get_offset_of__sunTexture_15() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____sunTexture_15)); }
	inline Texture2D_t3542995729 * get__sunTexture_15() const { return ____sunTexture_15; }
	inline Texture2D_t3542995729 ** get_address_of__sunTexture_15() { return &____sunTexture_15; }
	inline void set__sunTexture_15(Texture2D_t3542995729 * value)
	{
		____sunTexture_15 = value;
		Il2CppCodeGenWriteBarrier(&____sunTexture_15, value);
	}

	inline static int32_t get_offset_of__sunLight_16() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____sunLight_16)); }
	inline Light_t494725636 * get__sunLight_16() const { return ____sunLight_16; }
	inline Light_t494725636 ** get_address_of__sunLight_16() { return &____sunLight_16; }
	inline void set__sunLight_16(Light_t494725636 * value)
	{
		____sunLight_16 = value;
		Il2CppCodeGenWriteBarrier(&____sunLight_16, value);
	}

	inline static int32_t get_offset_of__sunTint_17() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____sunTint_17)); }
	inline Color_t2020392075  get__sunTint_17() const { return ____sunTint_17; }
	inline Color_t2020392075 * get_address_of__sunTint_17() { return &____sunTint_17; }
	inline void set__sunTint_17(Color_t2020392075  value)
	{
		____sunTint_17 = value;
	}

	inline static int32_t get_offset_of__sunSize_18() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____sunSize_18)); }
	inline float get__sunSize_18() const { return ____sunSize_18; }
	inline float* get_address_of__sunSize_18() { return &____sunSize_18; }
	inline void set__sunSize_18(float value)
	{
		____sunSize_18 = value;
	}

	inline static int32_t get_offset_of__sunFlare_19() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____sunFlare_19)); }
	inline bool get__sunFlare_19() const { return ____sunFlare_19; }
	inline bool* get_address_of__sunFlare_19() { return &____sunFlare_19; }
	inline void set__sunFlare_19(bool value)
	{
		____sunFlare_19 = value;
	}

	inline static int32_t get_offset_of__sunFlareBrightness_20() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____sunFlareBrightness_20)); }
	inline float get__sunFlareBrightness_20() const { return ____sunFlareBrightness_20; }
	inline float* get_address_of__sunFlareBrightness_20() { return &____sunFlareBrightness_20; }
	inline void set__sunFlareBrightness_20(float value)
	{
		____sunFlareBrightness_20 = value;
	}

	inline static int32_t get_offset_of__moonEnabled_21() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____moonEnabled_21)); }
	inline bool get__moonEnabled_21() const { return ____moonEnabled_21; }
	inline bool* get_address_of__moonEnabled_21() { return &____moonEnabled_21; }
	inline void set__moonEnabled_21(bool value)
	{
		____moonEnabled_21 = value;
	}

	inline static int32_t get_offset_of__moonTexture_22() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____moonTexture_22)); }
	inline Texture2D_t3542995729 * get__moonTexture_22() const { return ____moonTexture_22; }
	inline Texture2D_t3542995729 ** get_address_of__moonTexture_22() { return &____moonTexture_22; }
	inline void set__moonTexture_22(Texture2D_t3542995729 * value)
	{
		____moonTexture_22 = value;
		Il2CppCodeGenWriteBarrier(&____moonTexture_22, value);
	}

	inline static int32_t get_offset_of__moonLight_23() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____moonLight_23)); }
	inline Light_t494725636 * get__moonLight_23() const { return ____moonLight_23; }
	inline Light_t494725636 ** get_address_of__moonLight_23() { return &____moonLight_23; }
	inline void set__moonLight_23(Light_t494725636 * value)
	{
		____moonLight_23 = value;
		Il2CppCodeGenWriteBarrier(&____moonLight_23, value);
	}

	inline static int32_t get_offset_of__moonTint_24() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____moonTint_24)); }
	inline Color_t2020392075  get__moonTint_24() const { return ____moonTint_24; }
	inline Color_t2020392075 * get_address_of__moonTint_24() { return &____moonTint_24; }
	inline void set__moonTint_24(Color_t2020392075  value)
	{
		____moonTint_24 = value;
	}

	inline static int32_t get_offset_of__moonSize_25() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____moonSize_25)); }
	inline float get__moonSize_25() const { return ____moonSize_25; }
	inline float* get_address_of__moonSize_25() { return &____moonSize_25; }
	inline void set__moonSize_25(float value)
	{
		____moonSize_25 = value;
	}

	inline static int32_t get_offset_of__moonFlare_26() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____moonFlare_26)); }
	inline bool get__moonFlare_26() const { return ____moonFlare_26; }
	inline bool* get_address_of__moonFlare_26() { return &____moonFlare_26; }
	inline void set__moonFlare_26(bool value)
	{
		____moonFlare_26 = value;
	}

	inline static int32_t get_offset_of__moonFlareBrightness_27() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____moonFlareBrightness_27)); }
	inline float get__moonFlareBrightness_27() const { return ____moonFlareBrightness_27; }
	inline float* get_address_of__moonFlareBrightness_27() { return &____moonFlareBrightness_27; }
	inline void set__moonFlareBrightness_27(float value)
	{
		____moonFlareBrightness_27 = value;
	}

	inline static int32_t get_offset_of__cloudsEnabled_28() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____cloudsEnabled_28)); }
	inline bool get__cloudsEnabled_28() const { return ____cloudsEnabled_28; }
	inline bool* get_address_of__cloudsEnabled_28() { return &____cloudsEnabled_28; }
	inline void set__cloudsEnabled_28(bool value)
	{
		____cloudsEnabled_28 = value;
	}

	inline static int32_t get_offset_of__cloudsCubemap_29() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____cloudsCubemap_29)); }
	inline Cubemap_t3472944757 * get__cloudsCubemap_29() const { return ____cloudsCubemap_29; }
	inline Cubemap_t3472944757 ** get_address_of__cloudsCubemap_29() { return &____cloudsCubemap_29; }
	inline void set__cloudsCubemap_29(Cubemap_t3472944757 * value)
	{
		____cloudsCubemap_29 = value;
		Il2CppCodeGenWriteBarrier(&____cloudsCubemap_29, value);
	}

	inline static int32_t get_offset_of__cloudsTint_30() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____cloudsTint_30)); }
	inline Color_t2020392075  get__cloudsTint_30() const { return ____cloudsTint_30; }
	inline Color_t2020392075 * get_address_of__cloudsTint_30() { return &____cloudsTint_30; }
	inline void set__cloudsTint_30(Color_t2020392075  value)
	{
		____cloudsTint_30 = value;
	}

	inline static int32_t get_offset_of__cloudsHeight_31() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____cloudsHeight_31)); }
	inline float get__cloudsHeight_31() const { return ____cloudsHeight_31; }
	inline float* get_address_of__cloudsHeight_31() { return &____cloudsHeight_31; }
	inline void set__cloudsHeight_31(float value)
	{
		____cloudsHeight_31 = value;
	}

	inline static int32_t get_offset_of__cloudsRotation_32() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____cloudsRotation_32)); }
	inline float get__cloudsRotation_32() const { return ____cloudsRotation_32; }
	inline float* get_address_of__cloudsRotation_32() { return &____cloudsRotation_32; }
	inline void set__cloudsRotation_32(float value)
	{
		____cloudsRotation_32 = value;
	}

	inline static int32_t get_offset_of__exposure_33() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____exposure_33)); }
	inline float get__exposure_33() const { return ____exposure_33; }
	inline float* get_address_of__exposure_33() { return &____exposure_33; }
	inline void set__exposure_33(float value)
	{
		____exposure_33 = value;
	}

	inline static int32_t get_offset_of__adjustFogColor_34() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____adjustFogColor_34)); }
	inline bool get__adjustFogColor_34() const { return ____adjustFogColor_34; }
	inline bool* get_address_of__adjustFogColor_34() { return &____adjustFogColor_34; }
	inline void set__adjustFogColor_34(bool value)
	{
		____adjustFogColor_34 = value;
	}

	inline static int32_t get_offset_of__sunFlareComponent_35() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____sunFlareComponent_35)); }
	inline LensFlare_t529161798 * get__sunFlareComponent_35() const { return ____sunFlareComponent_35; }
	inline LensFlare_t529161798 ** get_address_of__sunFlareComponent_35() { return &____sunFlareComponent_35; }
	inline void set__sunFlareComponent_35(LensFlare_t529161798 * value)
	{
		____sunFlareComponent_35 = value;
		Il2CppCodeGenWriteBarrier(&____sunFlareComponent_35, value);
	}

	inline static int32_t get_offset_of__moonFlareComponent_36() { return static_cast<int32_t>(offsetof(SkyboxController_t2759006432, ____moonFlareComponent_36)); }
	inline LensFlare_t529161798 * get__moonFlareComponent_36() const { return ____moonFlareComponent_36; }
	inline LensFlare_t529161798 ** get_address_of__moonFlareComponent_36() { return &____moonFlareComponent_36; }
	inline void set__moonFlareComponent_36(LensFlare_t529161798 * value)
	{
		____moonFlareComponent_36 = value;
		Il2CppCodeGenWriteBarrier(&____moonFlareComponent_36, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
