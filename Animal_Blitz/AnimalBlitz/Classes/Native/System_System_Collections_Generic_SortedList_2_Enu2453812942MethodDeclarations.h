﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>
struct Enumerator_t2453812942;
// System.Collections.Generic.SortedList`2<System.Object,System.Object>
struct SortedList_2_t898252592;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedList_2_Enu3531626521.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>,System.Collections.Generic.SortedList`2/EnumeratorMode<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3212880915_gshared (Enumerator_t2453812942 * __this, SortedList_2_t898252592 * ___host0, int32_t ___mode1, const MethodInfo* method);
#define Enumerator__ctor_m3212880915(__this, ___host0, ___mode1, method) ((  void (*) (Enumerator_t2453812942 *, SortedList_2_t898252592 *, int32_t, const MethodInfo*))Enumerator__ctor_m3212880915_gshared)(__this, ___host0, ___mode1, method)
// System.Void System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::.cctor()
extern "C"  void Enumerator__cctor_m1761099314_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Enumerator__cctor_m1761099314(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Enumerator__cctor_m1761099314_gshared)(__this /* static, unused */, method)
// System.Void System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3421410584_gshared (Enumerator_t2453812942 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3421410584(__this, method) ((  void (*) (Enumerator_t2453812942 *, const MethodInfo*))Enumerator_Reset_m3421410584_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4002649885_gshared (Enumerator_t2453812942 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4002649885(__this, method) ((  bool (*) (Enumerator_t2453812942 *, const MethodInfo*))Enumerator_MoveNext_m4002649885_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_get_Entry_m2285411673_gshared (Enumerator_t2453812942 * __this, const MethodInfo* method);
#define Enumerator_get_Entry_m2285411673(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2453812942 *, const MethodInfo*))Enumerator_get_Entry_m2285411673_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * Enumerator_get_Key_m1808117512_gshared (Enumerator_t2453812942 * __this, const MethodInfo* method);
#define Enumerator_get_Key_m1808117512(__this, method) ((  Il2CppObject * (*) (Enumerator_t2453812942 *, const MethodInfo*))Enumerator_get_Key_m1808117512_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * Enumerator_get_Value_m2100654634_gshared (Enumerator_t2453812942 * __this, const MethodInfo* method);
#define Enumerator_get_Value_m2100654634(__this, method) ((  Il2CppObject * (*) (Enumerator_t2453812942 *, const MethodInfo*))Enumerator_get_Value_m2100654634_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3882251398_gshared (Enumerator_t2453812942 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3882251398(__this, method) ((  Il2CppObject * (*) (Enumerator_t2453812942 *, const MethodInfo*))Enumerator_get_Current_m3882251398_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::Clone()
extern "C"  Il2CppObject * Enumerator_Clone_m1461275889_gshared (Enumerator_t2453812942 * __this, const MethodInfo* method);
#define Enumerator_Clone_m1461275889(__this, method) ((  Il2CppObject * (*) (Enumerator_t2453812942 *, const MethodInfo*))Enumerator_Clone_m1461275889_gshared)(__this, method)
