﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BluetoothDeviceScript
struct BluetoothDeviceScript_t810775987;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BluetoothLEHardwareInterface
struct  BluetoothLEHardwareInterface_t4023789856  : public Il2CppObject
{
public:

public:
};

struct BluetoothLEHardwareInterface_t4023789856_StaticFields
{
public:
	// BluetoothDeviceScript BluetoothLEHardwareInterface::bluetoothDeviceScript
	BluetoothDeviceScript_t810775987 * ___bluetoothDeviceScript_0;

public:
	inline static int32_t get_offset_of_bluetoothDeviceScript_0() { return static_cast<int32_t>(offsetof(BluetoothLEHardwareInterface_t4023789856_StaticFields, ___bluetoothDeviceScript_0)); }
	inline BluetoothDeviceScript_t810775987 * get_bluetoothDeviceScript_0() const { return ___bluetoothDeviceScript_0; }
	inline BluetoothDeviceScript_t810775987 ** get_address_of_bluetoothDeviceScript_0() { return &___bluetoothDeviceScript_0; }
	inline void set_bluetoothDeviceScript_0(BluetoothDeviceScript_t810775987 * value)
	{
		___bluetoothDeviceScript_0 = value;
		Il2CppCodeGenWriteBarrier(&___bluetoothDeviceScript_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
