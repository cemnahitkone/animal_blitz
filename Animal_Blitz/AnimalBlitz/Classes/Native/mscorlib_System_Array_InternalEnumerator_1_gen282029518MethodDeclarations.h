﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen282029518.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H3718244552.h"

// System.Void System.Array/InternalEnumerator`1<HighlightingSystem.HighlighterRenderer/Data>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4101981097_gshared (InternalEnumerator_1_t282029518 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4101981097(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t282029518 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4101981097_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2452967533_gshared (InternalEnumerator_1_t282029518 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2452967533(__this, method) ((  void (*) (InternalEnumerator_1_t282029518 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2452967533_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2352771753_gshared (InternalEnumerator_1_t282029518 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2352771753(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t282029518 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2352771753_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<HighlightingSystem.HighlighterRenderer/Data>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m6405084_gshared (InternalEnumerator_1_t282029518 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m6405084(__this, method) ((  void (*) (InternalEnumerator_1_t282029518 *, const MethodInfo*))InternalEnumerator_1_Dispose_m6405084_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<HighlightingSystem.HighlighterRenderer/Data>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1941111429_gshared (InternalEnumerator_1_t282029518 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1941111429(__this, method) ((  bool (*) (InternalEnumerator_1_t282029518 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1941111429_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<HighlightingSystem.HighlighterRenderer/Data>::get_Current()
extern "C"  Data_t3718244552  InternalEnumerator_1_get_Current_m978465596_gshared (InternalEnumerator_1_t282029518 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m978465596(__this, method) ((  Data_t3718244552  (*) (InternalEnumerator_1_t282029518 *, const MethodInfo*))InternalEnumerator_1_get_Current_m978465596_gshared)(__this, method)
