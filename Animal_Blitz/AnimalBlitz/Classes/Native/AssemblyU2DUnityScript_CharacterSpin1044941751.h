﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterSpin
struct  CharacterSpin_t1044941751  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform CharacterSpin::sceneCamera
	Transform_t3275118058 * ___sceneCamera_2;
	// System.Single CharacterSpin::speed
	float ___speed_3;
	// System.Single CharacterSpin::bias
	float ___bias_4;
	// System.Single CharacterSpin::facingCamera
	float ___facingCamera_5;

public:
	inline static int32_t get_offset_of_sceneCamera_2() { return static_cast<int32_t>(offsetof(CharacterSpin_t1044941751, ___sceneCamera_2)); }
	inline Transform_t3275118058 * get_sceneCamera_2() const { return ___sceneCamera_2; }
	inline Transform_t3275118058 ** get_address_of_sceneCamera_2() { return &___sceneCamera_2; }
	inline void set_sceneCamera_2(Transform_t3275118058 * value)
	{
		___sceneCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___sceneCamera_2, value);
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(CharacterSpin_t1044941751, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_bias_4() { return static_cast<int32_t>(offsetof(CharacterSpin_t1044941751, ___bias_4)); }
	inline float get_bias_4() const { return ___bias_4; }
	inline float* get_address_of_bias_4() { return &___bias_4; }
	inline void set_bias_4(float value)
	{
		___bias_4 = value;
	}

	inline static int32_t get_offset_of_facingCamera_5() { return static_cast<int32_t>(offsetof(CharacterSpin_t1044941751, ___facingCamera_5)); }
	inline float get_facingCamera_5() const { return ___facingCamera_5; }
	inline float* get_address_of_facingCamera_5() { return &___facingCamera_5; }
	inline void set_facingCamera_5(float value)
	{
		___facingCamera_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
