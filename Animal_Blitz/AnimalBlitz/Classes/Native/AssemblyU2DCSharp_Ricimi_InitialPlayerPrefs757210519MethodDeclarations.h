﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.InitialPlayerPrefs
struct InitialPlayerPrefs_t757210519;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.InitialPlayerPrefs::.ctor()
extern "C"  void InitialPlayerPrefs__ctor_m3698642119 (InitialPlayerPrefs_t757210519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.InitialPlayerPrefs::Awake()
extern "C"  void InitialPlayerPrefs_Awake_m2151112454 (InitialPlayerPrefs_t757210519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
