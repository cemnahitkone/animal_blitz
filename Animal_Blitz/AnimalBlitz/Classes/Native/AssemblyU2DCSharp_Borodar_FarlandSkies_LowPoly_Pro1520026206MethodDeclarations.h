﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.PropertySlider
struct PropertySlider_t1520026206;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.PropertySlider::.ctor()
extern "C"  void PropertySlider__ctor_m2521022635 (PropertySlider_t1520026206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.PropertySlider::Awake()
extern "C"  void PropertySlider_Awake_m2565062802 (PropertySlider_t1520026206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.PropertySlider::Start()
extern "C"  void PropertySlider_Start_m959659159 (PropertySlider_t1520026206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.PropertySlider::OnValueChanged(System.Single)
extern "C"  void PropertySlider_OnValueChanged_m1015218472 (PropertySlider_t1520026206 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
