﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>
struct KeyCollection_t4152067831;
// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t1668570060;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key63106202.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m4162630165_gshared (KeyCollection_t4152067831 * __this, Dictionary_2_t1668570060 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m4162630165(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t4152067831 *, Dictionary_2_t1668570060 *, const MethodInfo*))KeyCollection__ctor_m4162630165_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1362498783_gshared (KeyCollection_t4152067831 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1362498783(__this, ___item0, method) ((  void (*) (KeyCollection_t4152067831 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1362498783_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1098554808_gshared (KeyCollection_t4152067831 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1098554808(__this, method) ((  void (*) (KeyCollection_t4152067831 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1098554808_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2910668965_gshared (KeyCollection_t4152067831 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2910668965(__this, ___item0, method) ((  bool (*) (KeyCollection_t4152067831 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2910668965_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m414134344_gshared (KeyCollection_t4152067831 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m414134344(__this, ___item0, method) ((  bool (*) (KeyCollection_t4152067831 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m414134344_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1613695342_gshared (KeyCollection_t4152067831 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1613695342(__this, method) ((  Il2CppObject* (*) (KeyCollection_t4152067831 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1613695342_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m917823878_gshared (KeyCollection_t4152067831 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m917823878(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4152067831 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m917823878_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4278561527_gshared (KeyCollection_t4152067831 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4278561527(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4152067831 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4278561527_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2894302366_gshared (KeyCollection_t4152067831 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2894302366(__this, method) ((  bool (*) (KeyCollection_t4152067831 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2894302366_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2503616650_gshared (KeyCollection_t4152067831 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2503616650(__this, method) ((  bool (*) (KeyCollection_t4152067831 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2503616650_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m40217974_gshared (KeyCollection_t4152067831 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m40217974(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4152067831 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m40217974_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m527511108_gshared (KeyCollection_t4152067831 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m527511108(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4152067831 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m527511108_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::GetEnumerator()
extern "C"  Enumerator_t63106202  KeyCollection_GetEnumerator_m1372342249_gshared (KeyCollection_t4152067831 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1372342249(__this, method) ((  Enumerator_t63106202  (*) (KeyCollection_t4152067831 *, const MethodInfo*))KeyCollection_GetEnumerator_m1372342249_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m196945570_gshared (KeyCollection_t4152067831 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m196945570(__this, method) ((  int32_t (*) (KeyCollection_t4152067831 *, const MethodInfo*))KeyCollection_get_Count_m196945570_gshared)(__this, method)
