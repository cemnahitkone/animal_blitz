﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestSceneManager
struct TestSceneManager_t2345075657;

#include "codegen/il2cpp-codegen.h"

// System.Void TestSceneManager::.ctor()
extern "C"  void TestSceneManager__ctor_m2884217182 (TestSceneManager_t2345075657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestSceneManager::Awake()
extern "C"  void TestSceneManager_Awake_m994355017 (TestSceneManager_t2345075657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestSceneManager::ActivateContainer(System.Int32)
extern "C"  void TestSceneManager_ActivateContainer_m2050603347 (TestSceneManager_t2345075657 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestSceneManager::GoToNext()
extern "C"  void TestSceneManager_GoToNext_m368671678 (TestSceneManager_t2345075657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestSceneManager::GoToPrevious()
extern "C"  void TestSceneManager_GoToPrevious_m2494976322 (TestSceneManager_t2345075657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
