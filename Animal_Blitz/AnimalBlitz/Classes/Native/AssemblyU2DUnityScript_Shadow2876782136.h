﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shadow
struct  Shadow_t2876782136  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Shadow::distanceTolerance
	float ___distanceTolerance_2;
	// System.Single Shadow::maxOpacity
	float ___maxOpacity_3;
	// System.Single Shadow::multiplier
	float ___multiplier_4;
	// System.Single Shadow::opacity
	float ___opacity_5;
	// UnityEngine.Transform Shadow::castingPoint
	Transform_t3275118058 * ___castingPoint_6;
	// System.Single Shadow::buffer
	float ___buffer_7;

public:
	inline static int32_t get_offset_of_distanceTolerance_2() { return static_cast<int32_t>(offsetof(Shadow_t2876782136, ___distanceTolerance_2)); }
	inline float get_distanceTolerance_2() const { return ___distanceTolerance_2; }
	inline float* get_address_of_distanceTolerance_2() { return &___distanceTolerance_2; }
	inline void set_distanceTolerance_2(float value)
	{
		___distanceTolerance_2 = value;
	}

	inline static int32_t get_offset_of_maxOpacity_3() { return static_cast<int32_t>(offsetof(Shadow_t2876782136, ___maxOpacity_3)); }
	inline float get_maxOpacity_3() const { return ___maxOpacity_3; }
	inline float* get_address_of_maxOpacity_3() { return &___maxOpacity_3; }
	inline void set_maxOpacity_3(float value)
	{
		___maxOpacity_3 = value;
	}

	inline static int32_t get_offset_of_multiplier_4() { return static_cast<int32_t>(offsetof(Shadow_t2876782136, ___multiplier_4)); }
	inline float get_multiplier_4() const { return ___multiplier_4; }
	inline float* get_address_of_multiplier_4() { return &___multiplier_4; }
	inline void set_multiplier_4(float value)
	{
		___multiplier_4 = value;
	}

	inline static int32_t get_offset_of_opacity_5() { return static_cast<int32_t>(offsetof(Shadow_t2876782136, ___opacity_5)); }
	inline float get_opacity_5() const { return ___opacity_5; }
	inline float* get_address_of_opacity_5() { return &___opacity_5; }
	inline void set_opacity_5(float value)
	{
		___opacity_5 = value;
	}

	inline static int32_t get_offset_of_castingPoint_6() { return static_cast<int32_t>(offsetof(Shadow_t2876782136, ___castingPoint_6)); }
	inline Transform_t3275118058 * get_castingPoint_6() const { return ___castingPoint_6; }
	inline Transform_t3275118058 ** get_address_of_castingPoint_6() { return &___castingPoint_6; }
	inline void set_castingPoint_6(Transform_t3275118058 * value)
	{
		___castingPoint_6 = value;
		Il2CppCodeGenWriteBarrier(&___castingPoint_6, value);
	}

	inline static int32_t get_offset_of_buffer_7() { return static_cast<int32_t>(offsetof(Shadow_t2876782136, ___buffer_7)); }
	inline float get_buffer_7() const { return ___buffer_7; }
	inline float* get_address_of_buffer_7() { return &___buffer_7; }
	inline void set_buffer_7(float value)
	{
		___buffer_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
