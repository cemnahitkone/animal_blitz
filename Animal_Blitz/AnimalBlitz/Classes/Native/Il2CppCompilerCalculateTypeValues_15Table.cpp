﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Specialized_HybridDiction290043810.h"
#include "System_System_Collections_Specialized_ListDictiona3458713452.h"
#include "System_System_Collections_Specialized_ListDictiona2725637098.h"
#include "System_System_Collections_Specialized_ListDictiona1923170152.h"
#include "System_System_Collections_Specialized_ListDictionar528898270.h"
#include "System_System_Collections_Specialized_ListDictiona2037848305.h"
#include "System_System_Collections_Specialized_NameObjectCo2034248631.h"
#include "System_System_Collections_Specialized_NameObjectCo3244489099.h"
#include "System_System_Collections_Specialized_NameObjectCo1718269396.h"
#include "System_System_Collections_Specialized_NameObjectCol633582367.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "System_System_Collections_Specialized_StringCollect352985975.h"
#include "System_System_Collections_Specialized_StringDictio1070889667.h"
#include "System_System_Collections_Specialized_StringEnumera441637433.h"
#include "System_System_ComponentModel_ArrayConverter2804512129.h"
#include "System_System_ComponentModel_ArrayConverter_ArrayPr599180064.h"
#include "System_System_ComponentModel_AttributeCollection1925812292.h"
#include "System_System_ComponentModel_BaseNumberConverter1130358776.h"
#include "System_System_ComponentModel_BooleanConverter284715810.h"
#include "System_System_ComponentModel_ByteConverter1265255600.h"
#include "System_System_ComponentModel_CharConverter437233350.h"
#include "System_System_ComponentModel_CollectionConverter2459375096.h"
#include "System_System_ComponentModel_ComponentCollection737017907.h"
#include "System_System_ComponentModel_ComponentConverter3121608223.h"
#include "System_System_ComponentModel_CultureInfoConverter2239982248.h"
#include "System_System_ComponentModel_CultureInfoConverter_2714931249.h"
#include "System_System_ComponentModel_DateTimeConverter2436647419.h"
#include "System_System_ComponentModel_DecimalConverter1618403211.h"
#include "System_System_ComponentModel_DefaultValueAttribute1302720498.h"
#include "System_System_ComponentModel_DesignerAttribute2778719479.h"
#include "System_System_ComponentModel_Design_Serialization_2188593799.h"
#include "System_System_ComponentModel_Design_Serialization_1404033120.h"
#include "System_System_ComponentModel_Design_Serialization_1162957127.h"
#include "System_System_ComponentModel_DoubleConverter864652623.h"
#include "System_System_ComponentModel_EditorBrowsableAttrib1050682502.h"
#include "System_System_ComponentModel_EditorBrowsableState373498655.h"
#include "System_System_ComponentModel_EnumConverter2538808523.h"
#include "System_System_ComponentModel_EnumConverter_EnumCom2635778853.h"
#include "System_System_ComponentModel_GuidConverter1547586607.h"
#include "System_System_ComponentModel_Int16Converter903627590.h"
#include "System_System_ComponentModel_Int32Converter957938388.h"
#include "System_System_ComponentModel_Int64Converter3186343659.h"
#include "System_System_ComponentModel_MemberDescriptor3749827553.h"
#include "System_System_ComponentModel_NullableConverter1941973167.h"
#include "System_System_ComponentModel_PropertyDescriptorCol3166009492.h"
#include "System_System_ComponentModel_PropertyDescriptor4250402154.h"
#include "System_System_ComponentModel_ReadOnlyAttribute4102148880.h"
#include "System_System_ComponentModel_ReferenceConverter3131270729.h"
#include "System_System_ComponentModel_ReflectionPropertyDes2570125387.h"
#include "System_System_ComponentModel_SByteConverter4003686519.h"
#include "System_System_ComponentModel_SingleConverter3693313828.h"
#include "System_System_ComponentModel_StringConverter3749524419.h"
#include "System_System_ComponentModel_TimeSpanConverter2149358279.h"
#include "System_System_ComponentModel_TypeConverterAttribute252469870.h"
#include "System_System_ComponentModel_TypeConverter745995970.h"
#include "System_System_ComponentModel_TypeConverter_Standard191679357.h"
#include "System_System_ComponentModel_TypeDescriptionProvid2438624375.h"
#include "System_System_ComponentModel_TypeDescriptor3595688691.h"
#include "System_System_ComponentModel_Info42715200.h"
#include "System_System_ComponentModel_ComponentInfo2742875487.h"
#include "System_System_ComponentModel_TypeInfo1029530608.h"
#include "System_System_ComponentModel_UInt16Converter1747783369.h"
#include "System_System_ComponentModel_UInt32Converter1748821239.h"
#include "System_System_ComponentModel_UInt64Converter977523578.h"
#include "System_System_ComponentModel_WeakObjectWrapper2012978780.h"
#include "System_System_ComponentModel_WeakObjectWrapperComp3891611113.h"
#include "System_System_ComponentModel_Win32Exception1708275760.h"
#include "System_System_Configuration_ConfigurationException3814184945.h"
#include "System_System_Configuration_ConfigurationSettings1600776263.h"
#include "System_System_Configuration_DefaultConfig320482295.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1500[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1501[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1502[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1503[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1504[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1505[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1506[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1507[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1508[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1509[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1510 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1510[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1511 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1511[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1512 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1512[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1513 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1513[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1514 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1514[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1515 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1515[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1516 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1516[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1517 = { sizeof (HybridDictionary_t290043810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1517[3] = 
{
	HybridDictionary_t290043810::get_offset_of_caseInsensitive_0(),
	HybridDictionary_t290043810::get_offset_of_hashtable_1(),
	HybridDictionary_t290043810::get_offset_of_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1518 = { sizeof (ListDictionary_t3458713452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1518[4] = 
{
	ListDictionary_t3458713452::get_offset_of_count_0(),
	ListDictionary_t3458713452::get_offset_of_version_1(),
	ListDictionary_t3458713452::get_offset_of_head_2(),
	ListDictionary_t3458713452::get_offset_of_comparer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1519 = { sizeof (DictionaryNode_t2725637098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1519[3] = 
{
	DictionaryNode_t2725637098::get_offset_of_key_0(),
	DictionaryNode_t2725637098::get_offset_of_value_1(),
	DictionaryNode_t2725637098::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1520 = { sizeof (DictionaryNodeEnumerator_t1923170152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1520[4] = 
{
	DictionaryNodeEnumerator_t1923170152::get_offset_of_dict_0(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_isAtStart_1(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_current_2(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1521 = { sizeof (DictionaryNodeCollection_t528898270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1521[2] = 
{
	DictionaryNodeCollection_t528898270::get_offset_of_dict_0(),
	DictionaryNodeCollection_t528898270::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1522 = { sizeof (DictionaryNodeCollectionEnumerator_t2037848305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1522[2] = 
{
	DictionaryNodeCollectionEnumerator_t2037848305::get_offset_of_inner_0(),
	DictionaryNodeCollectionEnumerator_t2037848305::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1523 = { sizeof (NameObjectCollectionBase_t2034248631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1523[10] = 
{
	NameObjectCollectionBase_t2034248631::get_offset_of_m_ItemsContainer_0(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_NullKeyItem_1(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_ItemsArray_2(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_hashprovider_3(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_comparer_4(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_defCapacity_5(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_readonly_6(),
	NameObjectCollectionBase_t2034248631::get_offset_of_infoCopy_7(),
	NameObjectCollectionBase_t2034248631::get_offset_of_keyscoll_8(),
	NameObjectCollectionBase_t2034248631::get_offset_of_equality_comparer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1524 = { sizeof (_Item_t3244489099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1524[2] = 
{
	_Item_t3244489099::get_offset_of_key_0(),
	_Item_t3244489099::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1525 = { sizeof (_KeysEnumerator_t1718269396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1525[2] = 
{
	_KeysEnumerator_t1718269396::get_offset_of_m_collection_0(),
	_KeysEnumerator_t1718269396::get_offset_of_m_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1526 = { sizeof (KeysCollection_t633582367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1526[1] = 
{
	KeysCollection_t633582367::get_offset_of_m_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1527 = { sizeof (NameValueCollection_t3047564564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1527[2] = 
{
	NameValueCollection_t3047564564::get_offset_of_cachedAllKeys_10(),
	NameValueCollection_t3047564564::get_offset_of_cachedAll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1528 = { sizeof (StringCollection_t352985975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1528[1] = 
{
	StringCollection_t352985975::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1529 = { sizeof (StringDictionary_t1070889667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1529[1] = 
{
	StringDictionary_t1070889667::get_offset_of_contents_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1530 = { sizeof (StringEnumerator_t441637433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1530[1] = 
{
	StringEnumerator_t441637433::get_offset_of_enumerable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1531 = { sizeof (ArrayConverter_t2804512129), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1532 = { sizeof (ArrayPropertyDescriptor_t599180064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1532[2] = 
{
	ArrayPropertyDescriptor_t599180064::get_offset_of_index_3(),
	ArrayPropertyDescriptor_t599180064::get_offset_of_array_type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1533 = { sizeof (AttributeCollection_t1925812292), -1, sizeof(AttributeCollection_t1925812292_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1533[2] = 
{
	AttributeCollection_t1925812292::get_offset_of_attrList_0(),
	AttributeCollection_t1925812292_StaticFields::get_offset_of_Empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1534 = { sizeof (BaseNumberConverter_t1130358776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1534[1] = 
{
	BaseNumberConverter_t1130358776::get_offset_of_InnerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1535 = { sizeof (BooleanConverter_t284715810), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1536 = { sizeof (ByteConverter_t1265255600), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1537 = { sizeof (CharConverter_t437233350), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1538 = { sizeof (CollectionConverter_t2459375096), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1539 = { sizeof (ComponentCollection_t737017907), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1540 = { sizeof (ComponentConverter_t3121608223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1541 = { sizeof (CultureInfoConverter_t2239982248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1541[1] = 
{
	CultureInfoConverter_t2239982248::get_offset_of__standardValues_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1542 = { sizeof (CultureInfoComparer_t2714931249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1543 = { sizeof (DateTimeConverter_t2436647419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1544 = { sizeof (DecimalConverter_t1618403211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1545 = { sizeof (DefaultValueAttribute_t1302720498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1545[1] = 
{
	DefaultValueAttribute_t1302720498::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1546 = { sizeof (DesignerAttribute_t2778719479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1546[2] = 
{
	DesignerAttribute_t2778719479::get_offset_of_name_0(),
	DesignerAttribute_t2778719479::get_offset_of_basetypename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1547 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1548 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1549 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1550 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1551 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1552 = { sizeof (DesignerSerializerAttribute_t2188593799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1552[2] = 
{
	DesignerSerializerAttribute_t2188593799::get_offset_of_serializerTypeName_0(),
	DesignerSerializerAttribute_t2188593799::get_offset_of_baseSerializerTypeName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1553 = { sizeof (InstanceDescriptor_t1404033120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1553[3] = 
{
	InstanceDescriptor_t1404033120::get_offset_of_member_0(),
	InstanceDescriptor_t1404033120::get_offset_of_arguments_1(),
	InstanceDescriptor_t1404033120::get_offset_of_isComplete_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1554 = { sizeof (RootDesignerSerializerAttribute_t1162957127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1554[3] = 
{
	RootDesignerSerializerAttribute_t1162957127::get_offset_of_serializer_0(),
	RootDesignerSerializerAttribute_t1162957127::get_offset_of_baseserializer_1(),
	RootDesignerSerializerAttribute_t1162957127::get_offset_of_reload_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1555 = { sizeof (DoubleConverter_t864652623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1556 = { sizeof (EditorBrowsableAttribute_t1050682502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1556[1] = 
{
	EditorBrowsableAttribute_t1050682502::get_offset_of_state_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1557 = { sizeof (EditorBrowsableState_t373498655)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1557[4] = 
{
	EditorBrowsableState_t373498655::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1558 = { sizeof (EnumConverter_t2538808523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1558[2] = 
{
	EnumConverter_t2538808523::get_offset_of_type_0(),
	EnumConverter_t2538808523::get_offset_of_stdValues_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1559 = { sizeof (EnumComparer_t2635778853), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1560 = { sizeof (GuidConverter_t1547586607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1561 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1562 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1563 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1564 = { sizeof (Int16Converter_t903627590), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1565 = { sizeof (Int32Converter_t957938388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1566 = { sizeof (Int64Converter_t3186343659), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1567 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1568 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1569 = { sizeof (MemberDescriptor_t3749827553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1569[3] = 
{
	MemberDescriptor_t3749827553::get_offset_of_name_0(),
	MemberDescriptor_t3749827553::get_offset_of_attrs_1(),
	MemberDescriptor_t3749827553::get_offset_of_attrCollection_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1570 = { sizeof (NullableConverter_t1941973167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1570[3] = 
{
	NullableConverter_t1941973167::get_offset_of_nullableType_0(),
	NullableConverter_t1941973167::get_offset_of_underlyingType_1(),
	NullableConverter_t1941973167::get_offset_of_underlyingTypeConverter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1571 = { sizeof (PropertyDescriptorCollection_t3166009492), -1, sizeof(PropertyDescriptorCollection_t3166009492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1571[3] = 
{
	PropertyDescriptorCollection_t3166009492_StaticFields::get_offset_of_Empty_0(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_properties_1(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_readOnly_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1572 = { sizeof (PropertyDescriptor_t4250402154), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1573 = { sizeof (ReadOnlyAttribute_t4102148880), -1, sizeof(ReadOnlyAttribute_t4102148880_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1573[4] = 
{
	ReadOnlyAttribute_t4102148880::get_offset_of_read_only_0(),
	ReadOnlyAttribute_t4102148880_StaticFields::get_offset_of_No_1(),
	ReadOnlyAttribute_t4102148880_StaticFields::get_offset_of_Yes_2(),
	ReadOnlyAttribute_t4102148880_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1574 = { sizeof (ReferenceConverter_t3131270729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1574[1] = 
{
	ReferenceConverter_t3131270729::get_offset_of_reference_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1575 = { sizeof (ReflectionPropertyDescriptor_t2570125387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1575[3] = 
{
	ReflectionPropertyDescriptor_t2570125387::get_offset_of__member_3(),
	ReflectionPropertyDescriptor_t2570125387::get_offset_of__componentType_4(),
	ReflectionPropertyDescriptor_t2570125387::get_offset_of__propertyType_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1576 = { sizeof (SByteConverter_t4003686519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1577 = { sizeof (SingleConverter_t3693313828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1578 = { sizeof (StringConverter_t3749524419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1579 = { sizeof (TimeSpanConverter_t2149358279), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1580 = { sizeof (TypeConverterAttribute_t252469870), -1, sizeof(TypeConverterAttribute_t252469870_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1580[2] = 
{
	TypeConverterAttribute_t252469870_StaticFields::get_offset_of_Default_0(),
	TypeConverterAttribute_t252469870::get_offset_of_converter_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1581 = { sizeof (TypeConverter_t745995970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1582 = { sizeof (StandardValuesCollection_t191679357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1582[1] = 
{
	StandardValuesCollection_t191679357::get_offset_of_values_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1583 = { sizeof (TypeDescriptionProvider_t2438624375), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1584 = { sizeof (TypeDescriptor_t3595688691), -1, sizeof(TypeDescriptor_t3595688691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1584[9] = 
{
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_creatingDefaultConverters_0(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_defaultConverters_1(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_componentTable_2(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_typeTable_3(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_typeDescriptionProvidersLock_4(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_typeDescriptionProviders_5(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_componentDescriptionProvidersLock_6(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_componentDescriptionProviders_7(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_onDispose_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1585 = { sizeof (Info_t42715200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1585[2] = 
{
	Info_t42715200::get_offset_of__infoType_0(),
	Info_t42715200::get_offset_of__attributes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1586 = { sizeof (ComponentInfo_t2742875487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1586[2] = 
{
	ComponentInfo_t2742875487::get_offset_of__component_2(),
	ComponentInfo_t2742875487::get_offset_of__properties_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1587 = { sizeof (TypeInfo_t1029530608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1587[1] = 
{
	TypeInfo_t1029530608::get_offset_of__properties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1588 = { sizeof (UInt16Converter_t1747783369), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1589 = { sizeof (UInt32Converter_t1748821239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1590 = { sizeof (UInt64Converter_t977523578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1591 = { sizeof (WeakObjectWrapper_t2012978780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1591[2] = 
{
	WeakObjectWrapper_t2012978780::get_offset_of_U3CTargetHashCodeU3Ek__BackingField_0(),
	WeakObjectWrapper_t2012978780::get_offset_of_U3CWeakU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1592 = { sizeof (WeakObjectWrapperComparer_t3891611113), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1593 = { sizeof (Win32Exception_t1708275760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1593[1] = 
{
	Win32Exception_t1708275760::get_offset_of_native_error_code_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1594 = { sizeof (ConfigurationException_t3814184945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1594[2] = 
{
	ConfigurationException_t3814184945::get_offset_of_filename_11(),
	ConfigurationException_t3814184945::get_offset_of_line_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1595 = { sizeof (ConfigurationSettings_t1600776263), -1, sizeof(ConfigurationSettings_t1600776263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1595[2] = 
{
	ConfigurationSettings_t1600776263_StaticFields::get_offset_of_config_0(),
	ConfigurationSettings_t1600776263_StaticFields::get_offset_of_lockobj_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1596 = { sizeof (DefaultConfig_t320482295), -1, sizeof(DefaultConfig_t320482295_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1596[1] = 
{
	DefaultConfig_t320482295_StaticFields::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1597 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1598 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1599 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
