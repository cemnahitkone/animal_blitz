﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle
struct SkyboxDayNightCycle_t2663460034;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.Core.Helpers.Singleton`1<Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle>
struct  Singleton_1_t2804708263  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t2804708263_StaticFields
{
public:
	// T Borodar.FarlandSkies.Core.Helpers.Singleton`1::_instance
	SkyboxDayNightCycle_t2663460034 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2804708263_StaticFields, ____instance_2)); }
	inline SkyboxDayNightCycle_t2663460034 * get__instance_2() const { return ____instance_2; }
	inline SkyboxDayNightCycle_t2663460034 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(SkyboxDayNightCycle_t2663460034 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
