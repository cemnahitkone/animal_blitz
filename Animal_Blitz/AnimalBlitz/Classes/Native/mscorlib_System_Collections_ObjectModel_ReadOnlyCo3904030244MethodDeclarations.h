﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>
struct ReadOnlyCollection_1_t3904030244;
// System.Collections.Generic.IList`1<HighlightingSystem.HighlighterRenderer/Data>
struct IList_1_t4259185153;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// HighlightingSystem.HighlighterRenderer/Data[]
struct DataU5BU5D_t4234851097;
// System.Collections.Generic.IEnumerator`1<HighlightingSystem.HighlighterRenderer/Data>
struct IEnumerator_1_t1193768379;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H3718244552.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3337602528_gshared (ReadOnlyCollection_1_t3904030244 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3337602528(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3904030244 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3337602528_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2639638528_gshared (ReadOnlyCollection_1_t3904030244 * __this, Data_t3718244552  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2639638528(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3904030244 *, Data_t3718244552 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2639638528_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m485881716_gshared (ReadOnlyCollection_1_t3904030244 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m485881716(__this, method) ((  void (*) (ReadOnlyCollection_1_t3904030244 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m485881716_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3834231679_gshared (ReadOnlyCollection_1_t3904030244 * __this, int32_t ___index0, Data_t3718244552  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3834231679(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3904030244 *, int32_t, Data_t3718244552 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3834231679_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m153656631_gshared (ReadOnlyCollection_1_t3904030244 * __this, Data_t3718244552  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m153656631(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3904030244 *, Data_t3718244552 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m153656631_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1929358971_gshared (ReadOnlyCollection_1_t3904030244 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1929358971(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3904030244 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1929358971_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  Data_t3718244552  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1773442691_gshared (ReadOnlyCollection_1_t3904030244 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1773442691(__this, ___index0, method) ((  Data_t3718244552  (*) (ReadOnlyCollection_1_t3904030244 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1773442691_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m180407346_gshared (ReadOnlyCollection_1_t3904030244 * __this, int32_t ___index0, Data_t3718244552  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m180407346(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3904030244 *, int32_t, Data_t3718244552 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m180407346_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m515767434_gshared (ReadOnlyCollection_1_t3904030244 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m515767434(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3904030244 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m515767434_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1903491485_gshared (ReadOnlyCollection_1_t3904030244 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1903491485(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3904030244 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1903491485_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m543911646_gshared (ReadOnlyCollection_1_t3904030244 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m543911646(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3904030244 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m543911646_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3416264705_gshared (ReadOnlyCollection_1_t3904030244 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3416264705(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3904030244 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3416264705_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3086921621_gshared (ReadOnlyCollection_1_t3904030244 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3086921621(__this, method) ((  void (*) (ReadOnlyCollection_1_t3904030244 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3086921621_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3709341517_gshared (ReadOnlyCollection_1_t3904030244 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3709341517(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3904030244 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3709341517_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2155001907_gshared (ReadOnlyCollection_1_t3904030244 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2155001907(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3904030244 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2155001907_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2680261608_gshared (ReadOnlyCollection_1_t3904030244 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2680261608(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3904030244 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2680261608_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3703602018_gshared (ReadOnlyCollection_1_t3904030244 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3703602018(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3904030244 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3703602018_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1918038946_gshared (ReadOnlyCollection_1_t3904030244 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1918038946(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3904030244 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1918038946_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2501025129_gshared (ReadOnlyCollection_1_t3904030244 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2501025129(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3904030244 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2501025129_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1222632717_gshared (ReadOnlyCollection_1_t3904030244 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1222632717(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3904030244 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1222632717_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m191917872_gshared (ReadOnlyCollection_1_t3904030244 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m191917872(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3904030244 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m191917872_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1536077861_gshared (ReadOnlyCollection_1_t3904030244 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1536077861(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3904030244 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1536077861_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2488343316_gshared (ReadOnlyCollection_1_t3904030244 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2488343316(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3904030244 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2488343316_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2190422275_gshared (ReadOnlyCollection_1_t3904030244 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2190422275(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3904030244 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2190422275_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m992654274_gshared (ReadOnlyCollection_1_t3904030244 * __this, Data_t3718244552  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m992654274(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3904030244 *, Data_t3718244552 , const MethodInfo*))ReadOnlyCollection_1_Contains_m992654274_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m769772684_gshared (ReadOnlyCollection_1_t3904030244 * __this, DataU5BU5D_t4234851097* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m769772684(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3904030244 *, DataU5BU5D_t4234851097*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m769772684_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1521446249_gshared (ReadOnlyCollection_1_t3904030244 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1521446249(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3904030244 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1521446249_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1943399606_gshared (ReadOnlyCollection_1_t3904030244 * __this, Data_t3718244552  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1943399606(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3904030244 *, Data_t3718244552 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1943399606_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3049727857_gshared (ReadOnlyCollection_1_t3904030244 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3049727857(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3904030244 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3049727857_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>::get_Item(System.Int32)
extern "C"  Data_t3718244552  ReadOnlyCollection_1_get_Item_m2277680751_gshared (ReadOnlyCollection_1_t3904030244 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2277680751(__this, ___index0, method) ((  Data_t3718244552  (*) (ReadOnlyCollection_1_t3904030244 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2277680751_gshared)(__this, ___index0, method)
