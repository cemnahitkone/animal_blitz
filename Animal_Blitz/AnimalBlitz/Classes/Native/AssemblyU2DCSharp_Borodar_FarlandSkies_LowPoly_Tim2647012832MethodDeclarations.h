﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.TimeText
struct TimeText_t2647012832;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.TimeText::.ctor()
extern "C"  void TimeText__ctor_m3051690529 (TimeText_t2647012832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.TimeText::Awake()
extern "C"  void TimeText_Awake_m506322400 (TimeText_t2647012832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.TimeText::Update()
extern "C"  void TimeText_Update_m2948785982 (TimeText_t2647012832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
