﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Toggle
struct Toggle_t3976754468;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Pro3212345188.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.PropertyToggle
struct  PropertyToggle_t1681642541  : public MonoBehaviour_t1158329972
{
public:
	// Borodar.FarlandSkies.LowPoly.PropertyToggle/Type Borodar.FarlandSkies.LowPoly.PropertyToggle::ToggleType
	int32_t ___ToggleType_2;
	// UnityEngine.UI.Toggle Borodar.FarlandSkies.LowPoly.PropertyToggle::_toggle
	Toggle_t3976754468 * ____toggle_3;

public:
	inline static int32_t get_offset_of_ToggleType_2() { return static_cast<int32_t>(offsetof(PropertyToggle_t1681642541, ___ToggleType_2)); }
	inline int32_t get_ToggleType_2() const { return ___ToggleType_2; }
	inline int32_t* get_address_of_ToggleType_2() { return &___ToggleType_2; }
	inline void set_ToggleType_2(int32_t value)
	{
		___ToggleType_2 = value;
	}

	inline static int32_t get_offset_of__toggle_3() { return static_cast<int32_t>(offsetof(PropertyToggle_t1681642541, ____toggle_3)); }
	inline Toggle_t3976754468 * get__toggle_3() const { return ____toggle_3; }
	inline Toggle_t3976754468 ** get_address_of__toggle_3() { return &____toggle_3; }
	inline void set__toggle_3(Toggle_t3976754468 * value)
	{
		____toggle_3 = value;
		Il2CppCodeGenWriteBarrier(&____toggle_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
