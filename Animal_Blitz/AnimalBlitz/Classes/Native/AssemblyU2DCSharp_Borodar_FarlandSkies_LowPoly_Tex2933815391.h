﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture[]
struct TextureU5BU5D_t2474608790;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Tex2676527179.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.TexturesDropdown
struct  TexturesDropdown_t2933815391  : public MonoBehaviour_t1158329972
{
public:
	// Borodar.FarlandSkies.LowPoly.TexturesDropdown/TextureType Borodar.FarlandSkies.LowPoly.TexturesDropdown::Type
	int32_t ___Type_2;
	// UnityEngine.Texture[] Borodar.FarlandSkies.LowPoly.TexturesDropdown::Textures
	TextureU5BU5D_t2474608790* ___Textures_3;
	// UnityEngine.UI.Dropdown Borodar.FarlandSkies.LowPoly.TexturesDropdown::_dropdown
	Dropdown_t1985816271 * ____dropdown_4;

public:
	inline static int32_t get_offset_of_Type_2() { return static_cast<int32_t>(offsetof(TexturesDropdown_t2933815391, ___Type_2)); }
	inline int32_t get_Type_2() const { return ___Type_2; }
	inline int32_t* get_address_of_Type_2() { return &___Type_2; }
	inline void set_Type_2(int32_t value)
	{
		___Type_2 = value;
	}

	inline static int32_t get_offset_of_Textures_3() { return static_cast<int32_t>(offsetof(TexturesDropdown_t2933815391, ___Textures_3)); }
	inline TextureU5BU5D_t2474608790* get_Textures_3() const { return ___Textures_3; }
	inline TextureU5BU5D_t2474608790** get_address_of_Textures_3() { return &___Textures_3; }
	inline void set_Textures_3(TextureU5BU5D_t2474608790* value)
	{
		___Textures_3 = value;
		Il2CppCodeGenWriteBarrier(&___Textures_3, value);
	}

	inline static int32_t get_offset_of__dropdown_4() { return static_cast<int32_t>(offsetof(TexturesDropdown_t2933815391, ____dropdown_4)); }
	inline Dropdown_t1985816271 * get__dropdown_4() const { return ____dropdown_4; }
	inline Dropdown_t1985816271 ** get_address_of__dropdown_4() { return &____dropdown_4; }
	inline void set__dropdown_4(Dropdown_t1985816271 * value)
	{
		____dropdown_4 = value;
		Il2CppCodeGenWriteBarrier(&____dropdown_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
