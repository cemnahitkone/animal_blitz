﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestSceneManager
struct  TestSceneManager_t2345075657  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] TestSceneManager::m_Containers
	GameObjectU5BU5D_t3057952154* ___m_Containers_2;
	// System.Int32 TestSceneManager::m_CurrentIndex
	int32_t ___m_CurrentIndex_3;

public:
	inline static int32_t get_offset_of_m_Containers_2() { return static_cast<int32_t>(offsetof(TestSceneManager_t2345075657, ___m_Containers_2)); }
	inline GameObjectU5BU5D_t3057952154* get_m_Containers_2() const { return ___m_Containers_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_Containers_2() { return &___m_Containers_2; }
	inline void set_m_Containers_2(GameObjectU5BU5D_t3057952154* value)
	{
		___m_Containers_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Containers_2, value);
	}

	inline static int32_t get_offset_of_m_CurrentIndex_3() { return static_cast<int32_t>(offsetof(TestSceneManager_t2345075657, ___m_CurrentIndex_3)); }
	inline int32_t get_m_CurrentIndex_3() const { return ___m_CurrentIndex_3; }
	inline int32_t* get_address_of_m_CurrentIndex_3() { return &___m_CurrentIndex_3; }
	inline void set_m_CurrentIndex_3(int32_t value)
	{
		___m_CurrentIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
