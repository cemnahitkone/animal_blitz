﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/CollisionModule
struct CollisionModule_t2408302831;
struct CollisionModule_t2408302831_marshaled_pinvoke;
struct CollisionModule_t2408302831_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_CollisionMo2408302831.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/CollisionModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void CollisionModule__ctor_m3262105593 (CollisionModule_t2408302831 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct CollisionModule_t2408302831;
struct CollisionModule_t2408302831_marshaled_pinvoke;

extern "C" void CollisionModule_t2408302831_marshal_pinvoke(const CollisionModule_t2408302831& unmarshaled, CollisionModule_t2408302831_marshaled_pinvoke& marshaled);
extern "C" void CollisionModule_t2408302831_marshal_pinvoke_back(const CollisionModule_t2408302831_marshaled_pinvoke& marshaled, CollisionModule_t2408302831& unmarshaled);
extern "C" void CollisionModule_t2408302831_marshal_pinvoke_cleanup(CollisionModule_t2408302831_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct CollisionModule_t2408302831;
struct CollisionModule_t2408302831_marshaled_com;

extern "C" void CollisionModule_t2408302831_marshal_com(const CollisionModule_t2408302831& unmarshaled, CollisionModule_t2408302831_marshaled_com& marshaled);
extern "C" void CollisionModule_t2408302831_marshal_com_back(const CollisionModule_t2408302831_marshaled_com& marshaled, CollisionModule_t2408302831& unmarshaled);
extern "C" void CollisionModule_t2408302831_marshal_com_cleanup(CollisionModule_t2408302831_marshaled_com& marshaled);
