﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,tBase>
struct Dictionary_2_t728786214;
// System.Collections.Generic.Dictionary`2<System.Int32,TabBase/tabID>
struct Dictionary_2_t3873625228;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.Int32,TabBase/baseLegID>
struct Dictionary_2_t2980096784;
// System.Collections.Generic.List`1<TabBase/baseLegID>
struct List_1_t3341392281;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TabBase
struct  TabBase_t319583306  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TabBase::idSize
	int32_t ___idSize_2;
	// System.Int32 TabBase::addBaseCounter
	int32_t ___addBaseCounter_3;
	// System.Int32 TabBase::removeBaseCounter
	int32_t ___removeBaseCounter_4;
	// System.Int32 TabBase::PosAverageCounter
	int32_t ___PosAverageCounter_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,TabBase/tabID> TabBase::tabIdData
	Dictionary_2_t3873625228 * ___tabIdData_7;
	// System.Int32 TabBase::buttonCounter
	int32_t ___buttonCounter_13;
	// System.Collections.Generic.Dictionary`2<System.Int32,TabBase/baseLegID> TabBase::idDictionary
	Dictionary_2_t2980096784 * ___idDictionary_14;
	// System.Collections.Generic.List`1<TabBase/baseLegID> TabBase::idList
	List_1_t3341392281 * ___idList_15;
	// System.Int32 TabBase::testBleLastUniqId
	int32_t ___testBleLastUniqId_16;
	// System.String TabBase::testData
	String_t* ___testData_17;
	// System.Boolean TabBase::button_state
	bool ___button_state_21;
	// UnityEngine.GUIStyle TabBase::myStyle
	GUIStyle_t1799908754 * ___myStyle_22;

public:
	inline static int32_t get_offset_of_idSize_2() { return static_cast<int32_t>(offsetof(TabBase_t319583306, ___idSize_2)); }
	inline int32_t get_idSize_2() const { return ___idSize_2; }
	inline int32_t* get_address_of_idSize_2() { return &___idSize_2; }
	inline void set_idSize_2(int32_t value)
	{
		___idSize_2 = value;
	}

	inline static int32_t get_offset_of_addBaseCounter_3() { return static_cast<int32_t>(offsetof(TabBase_t319583306, ___addBaseCounter_3)); }
	inline int32_t get_addBaseCounter_3() const { return ___addBaseCounter_3; }
	inline int32_t* get_address_of_addBaseCounter_3() { return &___addBaseCounter_3; }
	inline void set_addBaseCounter_3(int32_t value)
	{
		___addBaseCounter_3 = value;
	}

	inline static int32_t get_offset_of_removeBaseCounter_4() { return static_cast<int32_t>(offsetof(TabBase_t319583306, ___removeBaseCounter_4)); }
	inline int32_t get_removeBaseCounter_4() const { return ___removeBaseCounter_4; }
	inline int32_t* get_address_of_removeBaseCounter_4() { return &___removeBaseCounter_4; }
	inline void set_removeBaseCounter_4(int32_t value)
	{
		___removeBaseCounter_4 = value;
	}

	inline static int32_t get_offset_of_PosAverageCounter_5() { return static_cast<int32_t>(offsetof(TabBase_t319583306, ___PosAverageCounter_5)); }
	inline int32_t get_PosAverageCounter_5() const { return ___PosAverageCounter_5; }
	inline int32_t* get_address_of_PosAverageCounter_5() { return &___PosAverageCounter_5; }
	inline void set_PosAverageCounter_5(int32_t value)
	{
		___PosAverageCounter_5 = value;
	}

	inline static int32_t get_offset_of_tabIdData_7() { return static_cast<int32_t>(offsetof(TabBase_t319583306, ___tabIdData_7)); }
	inline Dictionary_2_t3873625228 * get_tabIdData_7() const { return ___tabIdData_7; }
	inline Dictionary_2_t3873625228 ** get_address_of_tabIdData_7() { return &___tabIdData_7; }
	inline void set_tabIdData_7(Dictionary_2_t3873625228 * value)
	{
		___tabIdData_7 = value;
		Il2CppCodeGenWriteBarrier(&___tabIdData_7, value);
	}

	inline static int32_t get_offset_of_buttonCounter_13() { return static_cast<int32_t>(offsetof(TabBase_t319583306, ___buttonCounter_13)); }
	inline int32_t get_buttonCounter_13() const { return ___buttonCounter_13; }
	inline int32_t* get_address_of_buttonCounter_13() { return &___buttonCounter_13; }
	inline void set_buttonCounter_13(int32_t value)
	{
		___buttonCounter_13 = value;
	}

	inline static int32_t get_offset_of_idDictionary_14() { return static_cast<int32_t>(offsetof(TabBase_t319583306, ___idDictionary_14)); }
	inline Dictionary_2_t2980096784 * get_idDictionary_14() const { return ___idDictionary_14; }
	inline Dictionary_2_t2980096784 ** get_address_of_idDictionary_14() { return &___idDictionary_14; }
	inline void set_idDictionary_14(Dictionary_2_t2980096784 * value)
	{
		___idDictionary_14 = value;
		Il2CppCodeGenWriteBarrier(&___idDictionary_14, value);
	}

	inline static int32_t get_offset_of_idList_15() { return static_cast<int32_t>(offsetof(TabBase_t319583306, ___idList_15)); }
	inline List_1_t3341392281 * get_idList_15() const { return ___idList_15; }
	inline List_1_t3341392281 ** get_address_of_idList_15() { return &___idList_15; }
	inline void set_idList_15(List_1_t3341392281 * value)
	{
		___idList_15 = value;
		Il2CppCodeGenWriteBarrier(&___idList_15, value);
	}

	inline static int32_t get_offset_of_testBleLastUniqId_16() { return static_cast<int32_t>(offsetof(TabBase_t319583306, ___testBleLastUniqId_16)); }
	inline int32_t get_testBleLastUniqId_16() const { return ___testBleLastUniqId_16; }
	inline int32_t* get_address_of_testBleLastUniqId_16() { return &___testBleLastUniqId_16; }
	inline void set_testBleLastUniqId_16(int32_t value)
	{
		___testBleLastUniqId_16 = value;
	}

	inline static int32_t get_offset_of_testData_17() { return static_cast<int32_t>(offsetof(TabBase_t319583306, ___testData_17)); }
	inline String_t* get_testData_17() const { return ___testData_17; }
	inline String_t** get_address_of_testData_17() { return &___testData_17; }
	inline void set_testData_17(String_t* value)
	{
		___testData_17 = value;
		Il2CppCodeGenWriteBarrier(&___testData_17, value);
	}

	inline static int32_t get_offset_of_button_state_21() { return static_cast<int32_t>(offsetof(TabBase_t319583306, ___button_state_21)); }
	inline bool get_button_state_21() const { return ___button_state_21; }
	inline bool* get_address_of_button_state_21() { return &___button_state_21; }
	inline void set_button_state_21(bool value)
	{
		___button_state_21 = value;
	}

	inline static int32_t get_offset_of_myStyle_22() { return static_cast<int32_t>(offsetof(TabBase_t319583306, ___myStyle_22)); }
	inline GUIStyle_t1799908754 * get_myStyle_22() const { return ___myStyle_22; }
	inline GUIStyle_t1799908754 ** get_address_of_myStyle_22() { return &___myStyle_22; }
	inline void set_myStyle_22(GUIStyle_t1799908754 * value)
	{
		___myStyle_22 = value;
		Il2CppCodeGenWriteBarrier(&___myStyle_22, value);
	}
};

struct TabBase_t319583306_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,tBase> TabBase::tabData
	Dictionary_2_t728786214 * ___tabData_6;
	// System.Boolean TabBase::dataState
	bool ___dataState_8;
	// System.String TabBase::testID
	String_t* ___testID_9;
	// System.Int32 TabBase::_uniqID
	int32_t ____uniqID_10;
	// System.Int32 TabBase::switch_state1
	int32_t ___switch_state1_11;
	// System.Int32 TabBase::switch_state5
	int32_t ___switch_state5_12;
	// System.Int32 TabBase::lastUniqId
	int32_t ___lastUniqId_18;
	// System.Int32 TabBase::lastCount
	int32_t ___lastCount_19;
	// System.String TabBase::lastTestId
	String_t* ___lastTestId_20;

public:
	inline static int32_t get_offset_of_tabData_6() { return static_cast<int32_t>(offsetof(TabBase_t319583306_StaticFields, ___tabData_6)); }
	inline Dictionary_2_t728786214 * get_tabData_6() const { return ___tabData_6; }
	inline Dictionary_2_t728786214 ** get_address_of_tabData_6() { return &___tabData_6; }
	inline void set_tabData_6(Dictionary_2_t728786214 * value)
	{
		___tabData_6 = value;
		Il2CppCodeGenWriteBarrier(&___tabData_6, value);
	}

	inline static int32_t get_offset_of_dataState_8() { return static_cast<int32_t>(offsetof(TabBase_t319583306_StaticFields, ___dataState_8)); }
	inline bool get_dataState_8() const { return ___dataState_8; }
	inline bool* get_address_of_dataState_8() { return &___dataState_8; }
	inline void set_dataState_8(bool value)
	{
		___dataState_8 = value;
	}

	inline static int32_t get_offset_of_testID_9() { return static_cast<int32_t>(offsetof(TabBase_t319583306_StaticFields, ___testID_9)); }
	inline String_t* get_testID_9() const { return ___testID_9; }
	inline String_t** get_address_of_testID_9() { return &___testID_9; }
	inline void set_testID_9(String_t* value)
	{
		___testID_9 = value;
		Il2CppCodeGenWriteBarrier(&___testID_9, value);
	}

	inline static int32_t get_offset_of__uniqID_10() { return static_cast<int32_t>(offsetof(TabBase_t319583306_StaticFields, ____uniqID_10)); }
	inline int32_t get__uniqID_10() const { return ____uniqID_10; }
	inline int32_t* get_address_of__uniqID_10() { return &____uniqID_10; }
	inline void set__uniqID_10(int32_t value)
	{
		____uniqID_10 = value;
	}

	inline static int32_t get_offset_of_switch_state1_11() { return static_cast<int32_t>(offsetof(TabBase_t319583306_StaticFields, ___switch_state1_11)); }
	inline int32_t get_switch_state1_11() const { return ___switch_state1_11; }
	inline int32_t* get_address_of_switch_state1_11() { return &___switch_state1_11; }
	inline void set_switch_state1_11(int32_t value)
	{
		___switch_state1_11 = value;
	}

	inline static int32_t get_offset_of_switch_state5_12() { return static_cast<int32_t>(offsetof(TabBase_t319583306_StaticFields, ___switch_state5_12)); }
	inline int32_t get_switch_state5_12() const { return ___switch_state5_12; }
	inline int32_t* get_address_of_switch_state5_12() { return &___switch_state5_12; }
	inline void set_switch_state5_12(int32_t value)
	{
		___switch_state5_12 = value;
	}

	inline static int32_t get_offset_of_lastUniqId_18() { return static_cast<int32_t>(offsetof(TabBase_t319583306_StaticFields, ___lastUniqId_18)); }
	inline int32_t get_lastUniqId_18() const { return ___lastUniqId_18; }
	inline int32_t* get_address_of_lastUniqId_18() { return &___lastUniqId_18; }
	inline void set_lastUniqId_18(int32_t value)
	{
		___lastUniqId_18 = value;
	}

	inline static int32_t get_offset_of_lastCount_19() { return static_cast<int32_t>(offsetof(TabBase_t319583306_StaticFields, ___lastCount_19)); }
	inline int32_t get_lastCount_19() const { return ___lastCount_19; }
	inline int32_t* get_address_of_lastCount_19() { return &___lastCount_19; }
	inline void set_lastCount_19(int32_t value)
	{
		___lastCount_19 = value;
	}

	inline static int32_t get_offset_of_lastTestId_20() { return static_cast<int32_t>(offsetof(TabBase_t319583306_StaticFields, ___lastTestId_20)); }
	inline String_t* get_lastTestId_20() const { return ___lastTestId_20; }
	inline String_t** get_address_of_lastTestId_20() { return &___lastTestId_20; }
	inline void set_lastTestId_20(String_t* value)
	{
		___lastTestId_20 = value;
		Il2CppCodeGenWriteBarrier(&___lastTestId_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
