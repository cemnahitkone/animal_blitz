﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.TintedButton/<DarkenColor>c__Iterator0
struct U3CDarkenColorU3Ec__Iterator0_t1184020839;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.TintedButton/<DarkenColor>c__Iterator0::.ctor()
extern "C"  void U3CDarkenColorU3Ec__Iterator0__ctor_m3521462818 (U3CDarkenColorU3Ec__Iterator0_t1184020839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ricimi.TintedButton/<DarkenColor>c__Iterator0::MoveNext()
extern "C"  bool U3CDarkenColorU3Ec__Iterator0_MoveNext_m3633669750 (U3CDarkenColorU3Ec__Iterator0_t1184020839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Ricimi.TintedButton/<DarkenColor>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDarkenColorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1295287000 (U3CDarkenColorU3Ec__Iterator0_t1184020839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Ricimi.TintedButton/<DarkenColor>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDarkenColorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3043455616 (U3CDarkenColorU3Ec__Iterator0_t1184020839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.TintedButton/<DarkenColor>c__Iterator0::Dispose()
extern "C"  void U3CDarkenColorU3Ec__Iterator0_Dispose_m1888205297 (U3CDarkenColorU3Ec__Iterator0_t1184020839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.TintedButton/<DarkenColor>c__Iterator0::Reset()
extern "C"  void U3CDarkenColorU3Ec__Iterator0_Reset_m1527765199 (U3CDarkenColorU3Ec__Iterator0_t1184020839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
