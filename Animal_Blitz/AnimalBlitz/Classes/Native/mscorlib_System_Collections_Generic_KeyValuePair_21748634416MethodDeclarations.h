﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23720882578MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2552111938(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1748634416 *, String_t*, float, const MethodInfo*))KeyValuePair_2__ctor_m3796742776_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::get_Key()
#define KeyValuePair_2_get_Key_m839521293(__this, method) ((  String_t* (*) (KeyValuePair_2_t1748634416 *, const MethodInfo*))KeyValuePair_2_get_Key_m1314509062_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3654939995(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1748634416 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m3897037655_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::get_Value()
#define KeyValuePair_2_get_Value_m471842554(__this, method) ((  float (*) (KeyValuePair_2_t1748634416 *, const MethodInfo*))KeyValuePair_2_get_Value_m253835334_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m352013931(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1748634416 *, float, const MethodInfo*))KeyValuePair_2_set_Value_m2368197927_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::ToString()
#define KeyValuePair_2_ToString_m2824289433(__this, method) ((  String_t* (*) (KeyValuePair_2_t1748634416 *, const MethodInfo*))KeyValuePair_2_ToString_m495261565_gshared)(__this, method)
