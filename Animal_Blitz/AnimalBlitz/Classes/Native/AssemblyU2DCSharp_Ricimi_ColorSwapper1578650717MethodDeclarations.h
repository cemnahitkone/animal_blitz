﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.ColorSwapper
struct ColorSwapper_t1578650717;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.ColorSwapper::.ctor()
extern "C"  void ColorSwapper__ctor_m759244675 (ColorSwapper_t1578650717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.ColorSwapper::Awake()
extern "C"  void ColorSwapper_Awake_m2382168796 (ColorSwapper_t1578650717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.ColorSwapper::SwapColor()
extern "C"  void ColorSwapper_SwapColor_m1325442243 (ColorSwapper_t1578650717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
