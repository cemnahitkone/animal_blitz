﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.PlayPopupOpener
struct PlayPopupOpener_t1253928371;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.PlayPopupOpener::.ctor()
extern "C"  void PlayPopupOpener__ctor_m1889241509 (PlayPopupOpener_t1253928371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.PlayPopupOpener::OpenPopup()
extern "C"  void PlayPopupOpener_OpenPopup_m2779263055 (PlayPopupOpener_t1253928371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
