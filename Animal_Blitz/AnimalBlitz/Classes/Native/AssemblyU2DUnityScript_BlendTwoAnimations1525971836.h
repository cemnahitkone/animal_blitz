﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AnimationClip
struct AnimationClip_t3510324950;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlendTwoAnimations
struct  BlendTwoAnimations_t1525971836  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AnimationClip BlendTwoAnimations::firstAnimation
	AnimationClip_t3510324950 * ___firstAnimation_2;
	// UnityEngine.AnimationClip BlendTwoAnimations::secondAnimation
	AnimationClip_t3510324950 * ___secondAnimation_3;
	// System.Single BlendTwoAnimations::blend
	float ___blend_4;

public:
	inline static int32_t get_offset_of_firstAnimation_2() { return static_cast<int32_t>(offsetof(BlendTwoAnimations_t1525971836, ___firstAnimation_2)); }
	inline AnimationClip_t3510324950 * get_firstAnimation_2() const { return ___firstAnimation_2; }
	inline AnimationClip_t3510324950 ** get_address_of_firstAnimation_2() { return &___firstAnimation_2; }
	inline void set_firstAnimation_2(AnimationClip_t3510324950 * value)
	{
		___firstAnimation_2 = value;
		Il2CppCodeGenWriteBarrier(&___firstAnimation_2, value);
	}

	inline static int32_t get_offset_of_secondAnimation_3() { return static_cast<int32_t>(offsetof(BlendTwoAnimations_t1525971836, ___secondAnimation_3)); }
	inline AnimationClip_t3510324950 * get_secondAnimation_3() const { return ___secondAnimation_3; }
	inline AnimationClip_t3510324950 ** get_address_of_secondAnimation_3() { return &___secondAnimation_3; }
	inline void set_secondAnimation_3(AnimationClip_t3510324950 * value)
	{
		___secondAnimation_3 = value;
		Il2CppCodeGenWriteBarrier(&___secondAnimation_3, value);
	}

	inline static int32_t get_offset_of_blend_4() { return static_cast<int32_t>(offsetof(BlendTwoAnimations_t1525971836, ___blend_4)); }
	inline float get_blend_4() const { return ___blend_4; }
	inline float* get_address_of_blend_4() { return &___blend_4; }
	inline void set_blend_4(float value)
	{
		___blend_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
