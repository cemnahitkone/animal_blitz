﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RunForwardandBack
struct RunForwardandBack_t3788415244;

#include "codegen/il2cpp-codegen.h"

// System.Void RunForwardandBack::.ctor()
extern "C"  void RunForwardandBack__ctor_m4113362341 (RunForwardandBack_t3788415244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RunForwardandBack::Start()
extern "C"  void RunForwardandBack_Start_m1280865789 (RunForwardandBack_t3788415244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RunForwardandBack::Update()
extern "C"  void RunForwardandBack_Update_m3659136634 (RunForwardandBack_t3788415244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
