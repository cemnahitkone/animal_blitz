﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3974596833(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2879698341 *, float, SkyParam_t4272832432 *, const MethodInfo*))KeyValuePair_2__ctor_m303434200_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::get_Key()
#define KeyValuePair_2_get_Key_m4251386155(__this, method) ((  float (*) (KeyValuePair_2_t2879698341 *, const MethodInfo*))KeyValuePair_2_get_Key_m4249127398_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2676524708(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2879698341 *, float, const MethodInfo*))KeyValuePair_2_set_Key_m448823159_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::get_Value()
#define KeyValuePair_2_get_Value_m4199710475(__this, method) ((  SkyParam_t4272832432 * (*) (KeyValuePair_2_t2879698341 *, const MethodInfo*))KeyValuePair_2_get_Value_m1014938790_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m907122588(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2879698341 *, SkyParam_t4272832432 *, const MethodInfo*))KeyValuePair_2_set_Value_m1070856647_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::ToString()
#define KeyValuePair_2_ToString_m3553418396(__this, method) ((  String_t* (*) (KeyValuePair_2_t2879698341 *, const MethodInfo*))KeyValuePair_2_ToString_m4220912861_gshared)(__this, method)
