﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.Core.Demo.CameraOrbitController
struct CameraOrbitController_t2546408343;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.Core.Demo.CameraOrbitController::.ctor()
extern "C"  void CameraOrbitController__ctor_m1473003634 (CameraOrbitController_t2546408343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.Core.Demo.CameraOrbitController::Awake()
extern "C"  void CameraOrbitController_Awake_m3360752447 (CameraOrbitController_t2546408343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.Core.Demo.CameraOrbitController::LateUpdate()
extern "C"  void CameraOrbitController_LateUpdate_m804565207 (CameraOrbitController_t2546408343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Borodar.FarlandSkies.Core.Demo.CameraOrbitController::ClampAngle(System.Single,System.Single,System.Single)
extern "C"  float CameraOrbitController_ClampAngle_m2367434235 (Il2CppObject * __this /* static, unused */, float ___angle0, float ___min1, float ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
