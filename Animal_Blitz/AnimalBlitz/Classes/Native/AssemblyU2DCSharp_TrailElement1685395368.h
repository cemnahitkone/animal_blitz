﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Stack`1<UnityEngine.GameObject>
struct Stack_1_t2844261301;
// TrailPreset
struct TrailPreset_t465077881;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// SpriteTrail
struct SpriteTrail_t2284883325;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrailElement
struct  TrailElement_t1685395368  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TrailElement::m_ItemId
	int32_t ___m_ItemId_4;
	// TrailPreset TrailElement::m_TrailSettings
	TrailPreset_t465077881 * ___m_TrailSettings_5;
	// System.Single TrailElement::m_TimeSinceCreation
	float ___m_TimeSinceCreation_6;
	// UnityEngine.SpriteRenderer TrailElement::m_SpRenderer
	SpriteRenderer_t1209076198 * ___m_SpRenderer_7;
	// System.Boolean TrailElement::m_Init
	bool ___m_Init_8;
	// UnityEngine.Vector3 TrailElement::m_InitSize
	Vector3_t2243707580  ___m_InitSize_9;
	// UnityEngine.Vector3 TrailElement::m_InitPos
	Vector3_t2243707580  ___m_InitPos_10;
	// SpriteTrail TrailElement::m_MotherTrail
	SpriteTrail_t2284883325 * ___m_MotherTrail_11;
	// System.Boolean TrailElement::m_NeedLateUpdate
	bool ___m_NeedLateUpdate_12;
	// System.Int32 TrailElement::m_TrailPos
	int32_t ___m_TrailPos_13;
	// System.Boolean TrailElement::m_NeedDequeue
	bool ___m_NeedDequeue_14;
	// UnityEngine.Transform TrailElement::m_Transform
	Transform_t3275118058 * ___m_Transform_15;

public:
	inline static int32_t get_offset_of_m_ItemId_4() { return static_cast<int32_t>(offsetof(TrailElement_t1685395368, ___m_ItemId_4)); }
	inline int32_t get_m_ItemId_4() const { return ___m_ItemId_4; }
	inline int32_t* get_address_of_m_ItemId_4() { return &___m_ItemId_4; }
	inline void set_m_ItemId_4(int32_t value)
	{
		___m_ItemId_4 = value;
	}

	inline static int32_t get_offset_of_m_TrailSettings_5() { return static_cast<int32_t>(offsetof(TrailElement_t1685395368, ___m_TrailSettings_5)); }
	inline TrailPreset_t465077881 * get_m_TrailSettings_5() const { return ___m_TrailSettings_5; }
	inline TrailPreset_t465077881 ** get_address_of_m_TrailSettings_5() { return &___m_TrailSettings_5; }
	inline void set_m_TrailSettings_5(TrailPreset_t465077881 * value)
	{
		___m_TrailSettings_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_TrailSettings_5, value);
	}

	inline static int32_t get_offset_of_m_TimeSinceCreation_6() { return static_cast<int32_t>(offsetof(TrailElement_t1685395368, ___m_TimeSinceCreation_6)); }
	inline float get_m_TimeSinceCreation_6() const { return ___m_TimeSinceCreation_6; }
	inline float* get_address_of_m_TimeSinceCreation_6() { return &___m_TimeSinceCreation_6; }
	inline void set_m_TimeSinceCreation_6(float value)
	{
		___m_TimeSinceCreation_6 = value;
	}

	inline static int32_t get_offset_of_m_SpRenderer_7() { return static_cast<int32_t>(offsetof(TrailElement_t1685395368, ___m_SpRenderer_7)); }
	inline SpriteRenderer_t1209076198 * get_m_SpRenderer_7() const { return ___m_SpRenderer_7; }
	inline SpriteRenderer_t1209076198 ** get_address_of_m_SpRenderer_7() { return &___m_SpRenderer_7; }
	inline void set_m_SpRenderer_7(SpriteRenderer_t1209076198 * value)
	{
		___m_SpRenderer_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_SpRenderer_7, value);
	}

	inline static int32_t get_offset_of_m_Init_8() { return static_cast<int32_t>(offsetof(TrailElement_t1685395368, ___m_Init_8)); }
	inline bool get_m_Init_8() const { return ___m_Init_8; }
	inline bool* get_address_of_m_Init_8() { return &___m_Init_8; }
	inline void set_m_Init_8(bool value)
	{
		___m_Init_8 = value;
	}

	inline static int32_t get_offset_of_m_InitSize_9() { return static_cast<int32_t>(offsetof(TrailElement_t1685395368, ___m_InitSize_9)); }
	inline Vector3_t2243707580  get_m_InitSize_9() const { return ___m_InitSize_9; }
	inline Vector3_t2243707580 * get_address_of_m_InitSize_9() { return &___m_InitSize_9; }
	inline void set_m_InitSize_9(Vector3_t2243707580  value)
	{
		___m_InitSize_9 = value;
	}

	inline static int32_t get_offset_of_m_InitPos_10() { return static_cast<int32_t>(offsetof(TrailElement_t1685395368, ___m_InitPos_10)); }
	inline Vector3_t2243707580  get_m_InitPos_10() const { return ___m_InitPos_10; }
	inline Vector3_t2243707580 * get_address_of_m_InitPos_10() { return &___m_InitPos_10; }
	inline void set_m_InitPos_10(Vector3_t2243707580  value)
	{
		___m_InitPos_10 = value;
	}

	inline static int32_t get_offset_of_m_MotherTrail_11() { return static_cast<int32_t>(offsetof(TrailElement_t1685395368, ___m_MotherTrail_11)); }
	inline SpriteTrail_t2284883325 * get_m_MotherTrail_11() const { return ___m_MotherTrail_11; }
	inline SpriteTrail_t2284883325 ** get_address_of_m_MotherTrail_11() { return &___m_MotherTrail_11; }
	inline void set_m_MotherTrail_11(SpriteTrail_t2284883325 * value)
	{
		___m_MotherTrail_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_MotherTrail_11, value);
	}

	inline static int32_t get_offset_of_m_NeedLateUpdate_12() { return static_cast<int32_t>(offsetof(TrailElement_t1685395368, ___m_NeedLateUpdate_12)); }
	inline bool get_m_NeedLateUpdate_12() const { return ___m_NeedLateUpdate_12; }
	inline bool* get_address_of_m_NeedLateUpdate_12() { return &___m_NeedLateUpdate_12; }
	inline void set_m_NeedLateUpdate_12(bool value)
	{
		___m_NeedLateUpdate_12 = value;
	}

	inline static int32_t get_offset_of_m_TrailPos_13() { return static_cast<int32_t>(offsetof(TrailElement_t1685395368, ___m_TrailPos_13)); }
	inline int32_t get_m_TrailPos_13() const { return ___m_TrailPos_13; }
	inline int32_t* get_address_of_m_TrailPos_13() { return &___m_TrailPos_13; }
	inline void set_m_TrailPos_13(int32_t value)
	{
		___m_TrailPos_13 = value;
	}

	inline static int32_t get_offset_of_m_NeedDequeue_14() { return static_cast<int32_t>(offsetof(TrailElement_t1685395368, ___m_NeedDequeue_14)); }
	inline bool get_m_NeedDequeue_14() const { return ___m_NeedDequeue_14; }
	inline bool* get_address_of_m_NeedDequeue_14() { return &___m_NeedDequeue_14; }
	inline void set_m_NeedDequeue_14(bool value)
	{
		___m_NeedDequeue_14 = value;
	}

	inline static int32_t get_offset_of_m_Transform_15() { return static_cast<int32_t>(offsetof(TrailElement_t1685395368, ___m_Transform_15)); }
	inline Transform_t3275118058 * get_m_Transform_15() const { return ___m_Transform_15; }
	inline Transform_t3275118058 ** get_address_of_m_Transform_15() { return &___m_Transform_15; }
	inline void set_m_Transform_15(Transform_t3275118058 * value)
	{
		___m_Transform_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_Transform_15, value);
	}
};

struct TrailElement_t1685395368_StaticFields
{
public:
	// System.Collections.Generic.Stack`1<UnityEngine.GameObject> TrailElement::m_FreeElements
	Stack_1_t2844261301 * ___m_FreeElements_2;
	// System.Int32 TrailElement::m_ItemCount
	int32_t ___m_ItemCount_3;

public:
	inline static int32_t get_offset_of_m_FreeElements_2() { return static_cast<int32_t>(offsetof(TrailElement_t1685395368_StaticFields, ___m_FreeElements_2)); }
	inline Stack_1_t2844261301 * get_m_FreeElements_2() const { return ___m_FreeElements_2; }
	inline Stack_1_t2844261301 ** get_address_of_m_FreeElements_2() { return &___m_FreeElements_2; }
	inline void set_m_FreeElements_2(Stack_1_t2844261301 * value)
	{
		___m_FreeElements_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_FreeElements_2, value);
	}

	inline static int32_t get_offset_of_m_ItemCount_3() { return static_cast<int32_t>(offsetof(TrailElement_t1685395368_StaticFields, ___m_ItemCount_3)); }
	inline int32_t get_m_ItemCount_3() const { return ___m_ItemCount_3; }
	inline int32_t* get_address_of_m_ItemCount_3() { return &___m_ItemCount_3; }
	inline void set_m_ItemCount_3(int32_t value)
	{
		___m_ItemCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
