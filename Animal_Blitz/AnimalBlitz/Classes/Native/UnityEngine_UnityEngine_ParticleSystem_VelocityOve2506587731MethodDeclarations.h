﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/VelocityOverLifetimeModule
struct VelocityOverLifetimeModule_t2506587731;
struct VelocityOverLifetimeModule_t2506587731_marshaled_pinvoke;
struct VelocityOverLifetimeModule_t2506587731_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_VelocityOve2506587731.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/VelocityOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void VelocityOverLifetimeModule__ctor_m1280667325 (VelocityOverLifetimeModule_t2506587731 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct VelocityOverLifetimeModule_t2506587731;
struct VelocityOverLifetimeModule_t2506587731_marshaled_pinvoke;

extern "C" void VelocityOverLifetimeModule_t2506587731_marshal_pinvoke(const VelocityOverLifetimeModule_t2506587731& unmarshaled, VelocityOverLifetimeModule_t2506587731_marshaled_pinvoke& marshaled);
extern "C" void VelocityOverLifetimeModule_t2506587731_marshal_pinvoke_back(const VelocityOverLifetimeModule_t2506587731_marshaled_pinvoke& marshaled, VelocityOverLifetimeModule_t2506587731& unmarshaled);
extern "C" void VelocityOverLifetimeModule_t2506587731_marshal_pinvoke_cleanup(VelocityOverLifetimeModule_t2506587731_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct VelocityOverLifetimeModule_t2506587731;
struct VelocityOverLifetimeModule_t2506587731_marshaled_com;

extern "C" void VelocityOverLifetimeModule_t2506587731_marshal_com(const VelocityOverLifetimeModule_t2506587731& unmarshaled, VelocityOverLifetimeModule_t2506587731_marshaled_com& marshaled);
extern "C" void VelocityOverLifetimeModule_t2506587731_marshal_com_back(const VelocityOverLifetimeModule_t2506587731_marshaled_com& marshaled, VelocityOverLifetimeModule_t2506587731& unmarshaled);
extern "C" void VelocityOverLifetimeModule_t2506587731_marshal_com_cleanup(VelocityOverLifetimeModule_t2506587731_marshaled_com& marshaled);
