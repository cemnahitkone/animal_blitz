﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Borodar.FarlandSkies.Core.Demo.ColorPicker
struct ColorPicker_t2906165247;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.Core.Demo.BaseColorButton
struct  BaseColorButton_t3091645806  : public MonoBehaviour_t1158329972
{
public:
	// Borodar.FarlandSkies.Core.Demo.ColorPicker Borodar.FarlandSkies.Core.Demo.BaseColorButton::ColorPicker
	ColorPicker_t2906165247 * ___ColorPicker_2;
	// UnityEngine.UI.Image Borodar.FarlandSkies.Core.Demo.BaseColorButton::ColorImage
	Image_t2042527209 * ___ColorImage_3;

public:
	inline static int32_t get_offset_of_ColorPicker_2() { return static_cast<int32_t>(offsetof(BaseColorButton_t3091645806, ___ColorPicker_2)); }
	inline ColorPicker_t2906165247 * get_ColorPicker_2() const { return ___ColorPicker_2; }
	inline ColorPicker_t2906165247 ** get_address_of_ColorPicker_2() { return &___ColorPicker_2; }
	inline void set_ColorPicker_2(ColorPicker_t2906165247 * value)
	{
		___ColorPicker_2 = value;
		Il2CppCodeGenWriteBarrier(&___ColorPicker_2, value);
	}

	inline static int32_t get_offset_of_ColorImage_3() { return static_cast<int32_t>(offsetof(BaseColorButton_t3091645806, ___ColorImage_3)); }
	inline Image_t2042527209 * get_ColorImage_3() const { return ___ColorImage_3; }
	inline Image_t2042527209 ** get_address_of_ColorImage_3() { return &___ColorImage_3; }
	inline void set_ColorImage_3(Image_t2042527209 * value)
	{
		___ColorImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___ColorImage_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
