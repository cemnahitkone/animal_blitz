﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E3806193287MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Camera>::.ctor(System.Collections.Generic.HashSet`1<T>)
#define Enumerator__ctor_m182873302(__this, ___hashset0, method) ((  void (*) (Enumerator_t1306204969 *, HashSet_1_t2817889127 *, const MethodInfo*))Enumerator__ctor_m1279102766_gshared)(__this, ___hashset0, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Camera>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2877948416(__this, method) ((  Il2CppObject * (*) (Enumerator_t1306204969 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2899861010_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Camera>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m747338296(__this, method) ((  void (*) (Enumerator_t1306204969 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2573763156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Camera>::MoveNext()
#define Enumerator_MoveNext_m2356615350(__this, method) ((  bool (*) (Enumerator_t1306204969 *, const MethodInfo*))Enumerator_MoveNext_m2097560514_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Camera>::get_Current()
#define Enumerator_get_Current_m1260261729(__this, method) ((  Camera_t189460977 * (*) (Enumerator_t1306204969 *, const MethodInfo*))Enumerator_get_Current_m3016104593_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Camera>::Dispose()
#define Enumerator_Dispose_m3178320841(__this, method) ((  void (*) (Enumerator_t1306204969 *, const MethodInfo*))Enumerator_Dispose_m2585752265_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Camera>::CheckState()
#define Enumerator_CheckState_m3438092107(__this, method) ((  void (*) (Enumerator_t1306204969 *, const MethodInfo*))Enumerator_CheckState_m1761755727_gshared)(__this, method)
