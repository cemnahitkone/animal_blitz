﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_Demo_B3091645806.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Col3781445085.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.ColorButton
struct  ColorButton_t2067027943  : public BaseColorButton_t3091645806
{
public:
	// Borodar.FarlandSkies.LowPoly.ColorButton/ColorType Borodar.FarlandSkies.LowPoly.ColorButton::SkyColorType
	int32_t ___SkyColorType_4;

public:
	inline static int32_t get_offset_of_SkyColorType_4() { return static_cast<int32_t>(offsetof(ColorButton_t2067027943, ___SkyColorType_4)); }
	inline int32_t get_SkyColorType_4() const { return ___SkyColorType_4; }
	inline int32_t* get_address_of_SkyColorType_4() { return &___SkyColorType_4; }
	inline void set_SkyColorType_4(int32_t value)
	{
		___SkyColorType_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
