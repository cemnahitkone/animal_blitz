﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.SpriteSwapper
struct  SpriteSwapper_t3880336557  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Sprite Ricimi.SpriteSwapper::enabledSprite
	Sprite_t309593783 * ___enabledSprite_2;
	// UnityEngine.Sprite Ricimi.SpriteSwapper::disabledSprite
	Sprite_t309593783 * ___disabledSprite_3;
	// System.Boolean Ricimi.SpriteSwapper::m_swapped
	bool ___m_swapped_4;
	// UnityEngine.UI.Image Ricimi.SpriteSwapper::m_image
	Image_t2042527209 * ___m_image_5;

public:
	inline static int32_t get_offset_of_enabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteSwapper_t3880336557, ___enabledSprite_2)); }
	inline Sprite_t309593783 * get_enabledSprite_2() const { return ___enabledSprite_2; }
	inline Sprite_t309593783 ** get_address_of_enabledSprite_2() { return &___enabledSprite_2; }
	inline void set_enabledSprite_2(Sprite_t309593783 * value)
	{
		___enabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier(&___enabledSprite_2, value);
	}

	inline static int32_t get_offset_of_disabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteSwapper_t3880336557, ___disabledSprite_3)); }
	inline Sprite_t309593783 * get_disabledSprite_3() const { return ___disabledSprite_3; }
	inline Sprite_t309593783 ** get_address_of_disabledSprite_3() { return &___disabledSprite_3; }
	inline void set_disabledSprite_3(Sprite_t309593783 * value)
	{
		___disabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___disabledSprite_3, value);
	}

	inline static int32_t get_offset_of_m_swapped_4() { return static_cast<int32_t>(offsetof(SpriteSwapper_t3880336557, ___m_swapped_4)); }
	inline bool get_m_swapped_4() const { return ___m_swapped_4; }
	inline bool* get_address_of_m_swapped_4() { return &___m_swapped_4; }
	inline void set_m_swapped_4(bool value)
	{
		___m_swapped_4 = value;
	}

	inline static int32_t get_offset_of_m_image_5() { return static_cast<int32_t>(offsetof(SpriteSwapper_t3880336557, ___m_image_5)); }
	inline Image_t2042527209 * get_m_image_5() const { return ___m_image_5; }
	inline Image_t2042527209 ** get_address_of_m_image_5() { return &___m_image_5; }
	inline void set_m_image_5(Image_t2042527209 * value)
	{
		___m_image_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_image_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
