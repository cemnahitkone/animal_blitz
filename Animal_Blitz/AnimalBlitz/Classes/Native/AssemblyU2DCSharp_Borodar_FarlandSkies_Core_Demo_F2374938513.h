﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2810717544;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.Core.Demo.ForegroundToggle
struct  ForegroundToggle_t2374938513  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] Borodar.FarlandSkies.Core.Demo.ForegroundToggle::GameObjects
	GameObjectU5BU5D_t3057952154* ___GameObjects_2;
	// UnityEngine.Renderer[] Borodar.FarlandSkies.Core.Demo.ForegroundToggle::Renderers
	RendererU5BU5D_t2810717544* ___Renderers_3;

public:
	inline static int32_t get_offset_of_GameObjects_2() { return static_cast<int32_t>(offsetof(ForegroundToggle_t2374938513, ___GameObjects_2)); }
	inline GameObjectU5BU5D_t3057952154* get_GameObjects_2() const { return ___GameObjects_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_GameObjects_2() { return &___GameObjects_2; }
	inline void set_GameObjects_2(GameObjectU5BU5D_t3057952154* value)
	{
		___GameObjects_2 = value;
		Il2CppCodeGenWriteBarrier(&___GameObjects_2, value);
	}

	inline static int32_t get_offset_of_Renderers_3() { return static_cast<int32_t>(offsetof(ForegroundToggle_t2374938513, ___Renderers_3)); }
	inline RendererU5BU5D_t2810717544* get_Renderers_3() const { return ___Renderers_3; }
	inline RendererU5BU5D_t2810717544** get_address_of_Renderers_3() { return &___Renderers_3; }
	inline void set_Renderers_3(RendererU5BU5D_t2810717544* value)
	{
		___Renderers_3 = value;
		Il2CppCodeGenWriteBarrier(&___Renderers_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
