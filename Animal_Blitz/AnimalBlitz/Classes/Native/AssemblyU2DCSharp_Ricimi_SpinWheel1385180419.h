﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.SpinWheel
struct  SpinWheel_t1385180419  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AnimationCurve Ricimi.SpinWheel::AnimationCurve
	AnimationCurve_t3306541151 * ___AnimationCurve_2;
	// System.Boolean Ricimi.SpinWheel::m_spinning
	bool ___m_spinning_3;

public:
	inline static int32_t get_offset_of_AnimationCurve_2() { return static_cast<int32_t>(offsetof(SpinWheel_t1385180419, ___AnimationCurve_2)); }
	inline AnimationCurve_t3306541151 * get_AnimationCurve_2() const { return ___AnimationCurve_2; }
	inline AnimationCurve_t3306541151 ** get_address_of_AnimationCurve_2() { return &___AnimationCurve_2; }
	inline void set_AnimationCurve_2(AnimationCurve_t3306541151 * value)
	{
		___AnimationCurve_2 = value;
		Il2CppCodeGenWriteBarrier(&___AnimationCurve_2, value);
	}

	inline static int32_t get_offset_of_m_spinning_3() { return static_cast<int32_t>(offsetof(SpinWheel_t1385180419, ___m_spinning_3)); }
	inline bool get_m_spinning_3() const { return ___m_spinning_3; }
	inline bool* get_address_of_m_spinning_3() { return &___m_spinning_3; }
	inline void set_m_spinning_3(bool value)
	{
		___m_spinning_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
