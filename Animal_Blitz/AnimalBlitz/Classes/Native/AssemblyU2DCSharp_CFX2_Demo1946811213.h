﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX2_Demo
struct  CFX2_Demo_t1946811213  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean CFX2_Demo::orderedSpawns
	bool ___orderedSpawns_2;
	// System.Single CFX2_Demo::step
	float ___step_3;
	// System.Single CFX2_Demo::range
	float ___range_4;
	// System.Single CFX2_Demo::order
	float ___order_5;
	// UnityEngine.Material CFX2_Demo::groundMat
	Material_t193706927 * ___groundMat_6;
	// UnityEngine.Material CFX2_Demo::waterMat
	Material_t193706927 * ___waterMat_7;
	// UnityEngine.GameObject[] CFX2_Demo::ParticleExamples
	GameObjectU5BU5D_t3057952154* ___ParticleExamples_8;
	// System.Int32 CFX2_Demo::exampleIndex
	int32_t ___exampleIndex_9;
	// System.String CFX2_Demo::randomSpawnsDelay
	String_t* ___randomSpawnsDelay_10;
	// System.Boolean CFX2_Demo::randomSpawns
	bool ___randomSpawns_11;
	// System.Boolean CFX2_Demo::slowMo
	bool ___slowMo_12;

public:
	inline static int32_t get_offset_of_orderedSpawns_2() { return static_cast<int32_t>(offsetof(CFX2_Demo_t1946811213, ___orderedSpawns_2)); }
	inline bool get_orderedSpawns_2() const { return ___orderedSpawns_2; }
	inline bool* get_address_of_orderedSpawns_2() { return &___orderedSpawns_2; }
	inline void set_orderedSpawns_2(bool value)
	{
		___orderedSpawns_2 = value;
	}

	inline static int32_t get_offset_of_step_3() { return static_cast<int32_t>(offsetof(CFX2_Demo_t1946811213, ___step_3)); }
	inline float get_step_3() const { return ___step_3; }
	inline float* get_address_of_step_3() { return &___step_3; }
	inline void set_step_3(float value)
	{
		___step_3 = value;
	}

	inline static int32_t get_offset_of_range_4() { return static_cast<int32_t>(offsetof(CFX2_Demo_t1946811213, ___range_4)); }
	inline float get_range_4() const { return ___range_4; }
	inline float* get_address_of_range_4() { return &___range_4; }
	inline void set_range_4(float value)
	{
		___range_4 = value;
	}

	inline static int32_t get_offset_of_order_5() { return static_cast<int32_t>(offsetof(CFX2_Demo_t1946811213, ___order_5)); }
	inline float get_order_5() const { return ___order_5; }
	inline float* get_address_of_order_5() { return &___order_5; }
	inline void set_order_5(float value)
	{
		___order_5 = value;
	}

	inline static int32_t get_offset_of_groundMat_6() { return static_cast<int32_t>(offsetof(CFX2_Demo_t1946811213, ___groundMat_6)); }
	inline Material_t193706927 * get_groundMat_6() const { return ___groundMat_6; }
	inline Material_t193706927 ** get_address_of_groundMat_6() { return &___groundMat_6; }
	inline void set_groundMat_6(Material_t193706927 * value)
	{
		___groundMat_6 = value;
		Il2CppCodeGenWriteBarrier(&___groundMat_6, value);
	}

	inline static int32_t get_offset_of_waterMat_7() { return static_cast<int32_t>(offsetof(CFX2_Demo_t1946811213, ___waterMat_7)); }
	inline Material_t193706927 * get_waterMat_7() const { return ___waterMat_7; }
	inline Material_t193706927 ** get_address_of_waterMat_7() { return &___waterMat_7; }
	inline void set_waterMat_7(Material_t193706927 * value)
	{
		___waterMat_7 = value;
		Il2CppCodeGenWriteBarrier(&___waterMat_7, value);
	}

	inline static int32_t get_offset_of_ParticleExamples_8() { return static_cast<int32_t>(offsetof(CFX2_Demo_t1946811213, ___ParticleExamples_8)); }
	inline GameObjectU5BU5D_t3057952154* get_ParticleExamples_8() const { return ___ParticleExamples_8; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_ParticleExamples_8() { return &___ParticleExamples_8; }
	inline void set_ParticleExamples_8(GameObjectU5BU5D_t3057952154* value)
	{
		___ParticleExamples_8 = value;
		Il2CppCodeGenWriteBarrier(&___ParticleExamples_8, value);
	}

	inline static int32_t get_offset_of_exampleIndex_9() { return static_cast<int32_t>(offsetof(CFX2_Demo_t1946811213, ___exampleIndex_9)); }
	inline int32_t get_exampleIndex_9() const { return ___exampleIndex_9; }
	inline int32_t* get_address_of_exampleIndex_9() { return &___exampleIndex_9; }
	inline void set_exampleIndex_9(int32_t value)
	{
		___exampleIndex_9 = value;
	}

	inline static int32_t get_offset_of_randomSpawnsDelay_10() { return static_cast<int32_t>(offsetof(CFX2_Demo_t1946811213, ___randomSpawnsDelay_10)); }
	inline String_t* get_randomSpawnsDelay_10() const { return ___randomSpawnsDelay_10; }
	inline String_t** get_address_of_randomSpawnsDelay_10() { return &___randomSpawnsDelay_10; }
	inline void set_randomSpawnsDelay_10(String_t* value)
	{
		___randomSpawnsDelay_10 = value;
		Il2CppCodeGenWriteBarrier(&___randomSpawnsDelay_10, value);
	}

	inline static int32_t get_offset_of_randomSpawns_11() { return static_cast<int32_t>(offsetof(CFX2_Demo_t1946811213, ___randomSpawns_11)); }
	inline bool get_randomSpawns_11() const { return ___randomSpawns_11; }
	inline bool* get_address_of_randomSpawns_11() { return &___randomSpawns_11; }
	inline void set_randomSpawns_11(bool value)
	{
		___randomSpawns_11 = value;
	}

	inline static int32_t get_offset_of_slowMo_12() { return static_cast<int32_t>(offsetof(CFX2_Demo_t1946811213, ___slowMo_12)); }
	inline bool get_slowMo_12() const { return ___slowMo_12; }
	inline bool* get_address_of_slowMo_12() { return &___slowMo_12; }
	inline void set_slowMo_12(bool value)
	{
		___slowMo_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
