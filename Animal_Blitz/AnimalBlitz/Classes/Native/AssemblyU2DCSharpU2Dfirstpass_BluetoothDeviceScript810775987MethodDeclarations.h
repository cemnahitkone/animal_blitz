﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BluetoothDeviceScript
struct BluetoothDeviceScript_t810775987;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void BluetoothDeviceScript::.ctor()
extern "C"  void BluetoothDeviceScript__ctor_m3422952948 (BluetoothDeviceScript_t810775987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothDeviceScript::Start()
extern "C"  void BluetoothDeviceScript_Start_m3834806660 (BluetoothDeviceScript_t810775987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothDeviceScript::Update()
extern "C"  void BluetoothDeviceScript_Update_m4257449185 (BluetoothDeviceScript_t810775987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothDeviceScript::OnBluetoothMessage(System.String)
extern "C"  void BluetoothDeviceScript_OnBluetoothMessage_m3706554852 (BluetoothDeviceScript_t810775987 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothDeviceScript::OnBluetoothData(System.String)
extern "C"  void BluetoothDeviceScript_OnBluetoothData_m649667473 (BluetoothDeviceScript_t810775987 * __this, String_t* ___base64Data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothDeviceScript::OnBluetoothData(System.String,System.String,System.String)
extern "C"  void BluetoothDeviceScript_OnBluetoothData_m1904684625 (BluetoothDeviceScript_t810775987 * __this, String_t* ___deviceAddress0, String_t* ___characteristic1, String_t* ___base64Data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BluetoothDeviceScript::OnPeripheralData(System.String,System.String)
extern "C"  void BluetoothDeviceScript_OnPeripheralData_m1475850261 (BluetoothDeviceScript_t810775987 * __this, String_t* ___characteristic0, String_t* ___base64Data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
