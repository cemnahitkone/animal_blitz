﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.Fader
struct  Fader_t3816812854  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Ricimi.Fader::duration
	float ___duration_2;
	// UnityEngine.CanvasGroup Ricimi.Fader::m_canvasGroup
	CanvasGroup_t3296560743 * ___m_canvasGroup_3;

public:
	inline static int32_t get_offset_of_duration_2() { return static_cast<int32_t>(offsetof(Fader_t3816812854, ___duration_2)); }
	inline float get_duration_2() const { return ___duration_2; }
	inline float* get_address_of_duration_2() { return &___duration_2; }
	inline void set_duration_2(float value)
	{
		___duration_2 = value;
	}

	inline static int32_t get_offset_of_m_canvasGroup_3() { return static_cast<int32_t>(offsetof(Fader_t3816812854, ___m_canvasGroup_3)); }
	inline CanvasGroup_t3296560743 * get_m_canvasGroup_3() const { return ___m_canvasGroup_3; }
	inline CanvasGroup_t3296560743 ** get_address_of_m_canvasGroup_3() { return &___m_canvasGroup_3; }
	inline void set_m_canvasGroup_3(CanvasGroup_t3296560743 * value)
	{
		___m_canvasGroup_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_canvasGroup_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
