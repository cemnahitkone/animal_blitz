﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.String
struct String_t;
// Ricimi.Transition
struct Transition_t2490031921;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.Transition/<RunFade>c__Iterator0
struct  U3CRunFadeU3Ec__Iterator0_t4246392297  : public Il2CppObject
{
public:
	// UnityEngine.Texture2D Ricimi.Transition/<RunFade>c__Iterator0::<bgTex>__0
	Texture2D_t3542995729 * ___U3CbgTexU3E__0_0;
	// UnityEngine.Color Ricimi.Transition/<RunFade>c__Iterator0::fadeColor
	Color_t2020392075  ___fadeColor_1;
	// UnityEngine.UI.Image Ricimi.Transition/<RunFade>c__Iterator0::<image>__1
	Image_t2042527209 * ___U3CimageU3E__1_2;
	// UnityEngine.Rect Ricimi.Transition/<RunFade>c__Iterator0::<rect>__2
	Rect_t3681755626  ___U3CrectU3E__2_3;
	// UnityEngine.Sprite Ricimi.Transition/<RunFade>c__Iterator0::<sprite>__3
	Sprite_t309593783 * ___U3CspriteU3E__3_4;
	// UnityEngine.Color Ricimi.Transition/<RunFade>c__Iterator0::<newColor>__4
	Color_t2020392075  ___U3CnewColorU3E__4_5;
	// System.Single Ricimi.Transition/<RunFade>c__Iterator0::<time>__5
	float ___U3CtimeU3E__5_6;
	// System.Single Ricimi.Transition/<RunFade>c__Iterator0::duration
	float ___duration_7;
	// System.Single Ricimi.Transition/<RunFade>c__Iterator0::<halfDuration>__6
	float ___U3ChalfDurationU3E__6_8;
	// System.String Ricimi.Transition/<RunFade>c__Iterator0::level
	String_t* ___level_9;
	// Ricimi.Transition Ricimi.Transition/<RunFade>c__Iterator0::$this
	Transition_t2490031921 * ___U24this_10;
	// System.Object Ricimi.Transition/<RunFade>c__Iterator0::$current
	Il2CppObject * ___U24current_11;
	// System.Boolean Ricimi.Transition/<RunFade>c__Iterator0::$disposing
	bool ___U24disposing_12;
	// System.Int32 Ricimi.Transition/<RunFade>c__Iterator0::$PC
	int32_t ___U24PC_13;

public:
	inline static int32_t get_offset_of_U3CbgTexU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t4246392297, ___U3CbgTexU3E__0_0)); }
	inline Texture2D_t3542995729 * get_U3CbgTexU3E__0_0() const { return ___U3CbgTexU3E__0_0; }
	inline Texture2D_t3542995729 ** get_address_of_U3CbgTexU3E__0_0() { return &___U3CbgTexU3E__0_0; }
	inline void set_U3CbgTexU3E__0_0(Texture2D_t3542995729 * value)
	{
		___U3CbgTexU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbgTexU3E__0_0, value);
	}

	inline static int32_t get_offset_of_fadeColor_1() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t4246392297, ___fadeColor_1)); }
	inline Color_t2020392075  get_fadeColor_1() const { return ___fadeColor_1; }
	inline Color_t2020392075 * get_address_of_fadeColor_1() { return &___fadeColor_1; }
	inline void set_fadeColor_1(Color_t2020392075  value)
	{
		___fadeColor_1 = value;
	}

	inline static int32_t get_offset_of_U3CimageU3E__1_2() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t4246392297, ___U3CimageU3E__1_2)); }
	inline Image_t2042527209 * get_U3CimageU3E__1_2() const { return ___U3CimageU3E__1_2; }
	inline Image_t2042527209 ** get_address_of_U3CimageU3E__1_2() { return &___U3CimageU3E__1_2; }
	inline void set_U3CimageU3E__1_2(Image_t2042527209 * value)
	{
		___U3CimageU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CimageU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CrectU3E__2_3() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t4246392297, ___U3CrectU3E__2_3)); }
	inline Rect_t3681755626  get_U3CrectU3E__2_3() const { return ___U3CrectU3E__2_3; }
	inline Rect_t3681755626 * get_address_of_U3CrectU3E__2_3() { return &___U3CrectU3E__2_3; }
	inline void set_U3CrectU3E__2_3(Rect_t3681755626  value)
	{
		___U3CrectU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CspriteU3E__3_4() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t4246392297, ___U3CspriteU3E__3_4)); }
	inline Sprite_t309593783 * get_U3CspriteU3E__3_4() const { return ___U3CspriteU3E__3_4; }
	inline Sprite_t309593783 ** get_address_of_U3CspriteU3E__3_4() { return &___U3CspriteU3E__3_4; }
	inline void set_U3CspriteU3E__3_4(Sprite_t309593783 * value)
	{
		___U3CspriteU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CspriteU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CnewColorU3E__4_5() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t4246392297, ___U3CnewColorU3E__4_5)); }
	inline Color_t2020392075  get_U3CnewColorU3E__4_5() const { return ___U3CnewColorU3E__4_5; }
	inline Color_t2020392075 * get_address_of_U3CnewColorU3E__4_5() { return &___U3CnewColorU3E__4_5; }
	inline void set_U3CnewColorU3E__4_5(Color_t2020392075  value)
	{
		___U3CnewColorU3E__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CtimeU3E__5_6() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t4246392297, ___U3CtimeU3E__5_6)); }
	inline float get_U3CtimeU3E__5_6() const { return ___U3CtimeU3E__5_6; }
	inline float* get_address_of_U3CtimeU3E__5_6() { return &___U3CtimeU3E__5_6; }
	inline void set_U3CtimeU3E__5_6(float value)
	{
		___U3CtimeU3E__5_6 = value;
	}

	inline static int32_t get_offset_of_duration_7() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t4246392297, ___duration_7)); }
	inline float get_duration_7() const { return ___duration_7; }
	inline float* get_address_of_duration_7() { return &___duration_7; }
	inline void set_duration_7(float value)
	{
		___duration_7 = value;
	}

	inline static int32_t get_offset_of_U3ChalfDurationU3E__6_8() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t4246392297, ___U3ChalfDurationU3E__6_8)); }
	inline float get_U3ChalfDurationU3E__6_8() const { return ___U3ChalfDurationU3E__6_8; }
	inline float* get_address_of_U3ChalfDurationU3E__6_8() { return &___U3ChalfDurationU3E__6_8; }
	inline void set_U3ChalfDurationU3E__6_8(float value)
	{
		___U3ChalfDurationU3E__6_8 = value;
	}

	inline static int32_t get_offset_of_level_9() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t4246392297, ___level_9)); }
	inline String_t* get_level_9() const { return ___level_9; }
	inline String_t** get_address_of_level_9() { return &___level_9; }
	inline void set_level_9(String_t* value)
	{
		___level_9 = value;
		Il2CppCodeGenWriteBarrier(&___level_9, value);
	}

	inline static int32_t get_offset_of_U24this_10() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t4246392297, ___U24this_10)); }
	inline Transition_t2490031921 * get_U24this_10() const { return ___U24this_10; }
	inline Transition_t2490031921 ** get_address_of_U24this_10() { return &___U24this_10; }
	inline void set_U24this_10(Transition_t2490031921 * value)
	{
		___U24this_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_10, value);
	}

	inline static int32_t get_offset_of_U24current_11() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t4246392297, ___U24current_11)); }
	inline Il2CppObject * get_U24current_11() const { return ___U24current_11; }
	inline Il2CppObject ** get_address_of_U24current_11() { return &___U24current_11; }
	inline void set_U24current_11(Il2CppObject * value)
	{
		___U24current_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_11, value);
	}

	inline static int32_t get_offset_of_U24disposing_12() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t4246392297, ___U24disposing_12)); }
	inline bool get_U24disposing_12() const { return ___U24disposing_12; }
	inline bool* get_address_of_U24disposing_12() { return &___U24disposing_12; }
	inline void set_U24disposing_12(bool value)
	{
		___U24disposing_12 = value;
	}

	inline static int32_t get_offset_of_U24PC_13() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t4246392297, ___U24PC_13)); }
	inline int32_t get_U24PC_13() const { return ___U24PC_13; }
	inline int32_t* get_address_of_U24PC_13() { return &___U24PC_13; }
	inline void set_U24PC_13(int32_t value)
	{
		___U24PC_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
