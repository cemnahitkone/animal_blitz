﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityScript.Lang.ListUpdateableEnumerator
struct ListUpdateableEnumerator_t2462779323;
// System.Collections.IList
struct IList_t3321498491;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void UnityScript.Lang.ListUpdateableEnumerator::.ctor(System.Collections.IList)
extern "C"  void ListUpdateableEnumerator__ctor_m2483709717 (ListUpdateableEnumerator_t2462779323 * __this, Il2CppObject * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityScript.Lang.ListUpdateableEnumerator::Reset()
extern "C"  void ListUpdateableEnumerator_Reset_m4186908375 (ListUpdateableEnumerator_t2462779323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityScript.Lang.ListUpdateableEnumerator::MoveNext()
extern "C"  bool ListUpdateableEnumerator_MoveNext_m1727071326 (ListUpdateableEnumerator_t2462779323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityScript.Lang.ListUpdateableEnumerator::get_Current()
extern "C"  Il2CppObject * ListUpdateableEnumerator_get_Current_m1239640447 (ListUpdateableEnumerator_t2462779323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityScript.Lang.ListUpdateableEnumerator::Update(System.Object)
extern "C"  void ListUpdateableEnumerator_Update_m670620225 (ListUpdateableEnumerator_t2462779323 * __this, Il2CppObject * ___newValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
