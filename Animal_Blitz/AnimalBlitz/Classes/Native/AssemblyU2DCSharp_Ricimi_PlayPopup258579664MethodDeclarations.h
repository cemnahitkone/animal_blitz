﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.PlayPopup
struct PlayPopup_t258579664;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.PlayPopup::.ctor()
extern "C"  void PlayPopup__ctor_m886606160 (PlayPopup_t258579664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.PlayPopup::SetAchievedStars(System.Int32)
extern "C"  void PlayPopup_SetAchievedStars_m1968249991 (PlayPopup_t258579664 * __this, int32_t ___starsObtained0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
