﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.Core.Helpers.Singleton`1<System.Object>
struct Singleton_1_t2830697524;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.Core.Helpers.Singleton`1<System.Object>::.ctor()
extern "C"  void Singleton_1__ctor_m3543112861_gshared (Singleton_1_t2830697524 * __this, const MethodInfo* method);
#define Singleton_1__ctor_m3543112861(__this, method) ((  void (*) (Singleton_1_t2830697524 *, const MethodInfo*))Singleton_1__ctor_m3543112861_gshared)(__this, method)
// T Borodar.FarlandSkies.Core.Helpers.Singleton`1<System.Object>::get_Instance()
extern "C"  Il2CppObject * Singleton_1_get_Instance_m4226036052_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Singleton_1_get_Instance_m4226036052(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_Instance_m4226036052_gshared)(__this /* static, unused */, method)
