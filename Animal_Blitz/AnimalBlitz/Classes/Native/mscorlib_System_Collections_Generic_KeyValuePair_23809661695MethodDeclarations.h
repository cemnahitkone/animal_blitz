﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.StarsParam>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m4260067315(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3809661695 *, float, StarsParam_t907828490 *, const MethodInfo*))KeyValuePair_2__ctor_m303434200_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.StarsParam>::get_Key()
#define KeyValuePair_2_get_Key_m1062610717(__this, method) ((  float (*) (KeyValuePair_2_t3809661695 *, const MethodInfo*))KeyValuePair_2_get_Key_m4249127398_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.StarsParam>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m452429828(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3809661695 *, float, const MethodInfo*))KeyValuePair_2_set_Key_m448823159_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.StarsParam>::get_Value()
#define KeyValuePair_2_get_Value_m2677674685(__this, method) ((  StarsParam_t907828490 * (*) (KeyValuePair_2_t3809661695 *, const MethodInfo*))KeyValuePair_2_get_Value_m1014938790_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.StarsParam>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2253421380(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3809661695 *, StarsParam_t907828490 *, const MethodInfo*))KeyValuePair_2_set_Value_m1070856647_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.StarsParam>::ToString()
#define KeyValuePair_2_ToString_m3283307640(__this, method) ((  String_t* (*) (KeyValuePair_2_t3809661695 *, const MethodInfo*))KeyValuePair_2_ToString_m4220912861_gshared)(__this, method)
