﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UILineRenderer
struct UILineRenderer_t1431191837;
// UnityEngine.Texture
struct Texture_t2243626319;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t573379950;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void UILineRenderer::.ctor()
extern "C"  void UILineRenderer__ctor_m1228311854 (UILineRenderer_t1431191837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UILineRenderer::get_mainTexture()
extern "C"  Texture_t2243626319 * UILineRenderer_get_mainTexture_m1097622252 (UILineRenderer_t1431191837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UILineRenderer::get_texture()
extern "C"  Texture_t2243626319 * UILineRenderer_get_texture_m3250786741 (UILineRenderer_t1431191837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILineRenderer::set_texture(UnityEngine.Texture)
extern "C"  void UILineRenderer_set_texture_m1683784838 (UILineRenderer_t1431191837 * __this, Texture_t2243626319 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UILineRenderer::get_uvRect()
extern "C"  Rect_t3681755626  UILineRenderer_get_uvRect_m3166800612 (UILineRenderer_t1431191837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILineRenderer::set_uvRect(UnityEngine.Rect)
extern "C"  void UILineRenderer_set_uvRect_m1994527789 (UILineRenderer_t1431191837 * __this, Rect_t3681755626  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILineRenderer::OnFillVBO(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C"  void UILineRenderer_OnFillVBO_m1039925740 (UILineRenderer_t1431191837 * __this, List_1_t573379950 * ___vbo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILineRenderer::SetVbo(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Vector2[],UnityEngine.Vector2[])
extern "C"  void UILineRenderer_SetVbo_m858010382 (UILineRenderer_t1431191837 * __this, List_1_t573379950 * ___vbo0, Vector2U5BU5D_t686124026* ___vertices1, Vector2U5BU5D_t686124026* ___uvs2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UILineRenderer::RotatePointAroundPivot(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  UILineRenderer_RotatePointAroundPivot_m1756234985 (UILineRenderer_t1431191837 * __this, Vector3_t2243707580  ___point0, Vector3_t2243707580  ___pivot1, Vector3_t2243707580  ___angles2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
