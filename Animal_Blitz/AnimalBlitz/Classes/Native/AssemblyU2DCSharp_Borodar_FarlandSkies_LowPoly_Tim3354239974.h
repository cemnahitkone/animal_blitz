﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Slider
struct Slider_t297367283;
// Borodar.FarlandSkies.LowPoly.SkyboxCycleManager
struct SkyboxCycleManager_t61875227;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.TimeSlider
struct  TimeSlider_t3354239974  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Slider Borodar.FarlandSkies.LowPoly.TimeSlider::_slider
	Slider_t297367283 * ____slider_2;
	// Borodar.FarlandSkies.LowPoly.SkyboxCycleManager Borodar.FarlandSkies.LowPoly.TimeSlider::_cycleManager
	SkyboxCycleManager_t61875227 * ____cycleManager_3;

public:
	inline static int32_t get_offset_of__slider_2() { return static_cast<int32_t>(offsetof(TimeSlider_t3354239974, ____slider_2)); }
	inline Slider_t297367283 * get__slider_2() const { return ____slider_2; }
	inline Slider_t297367283 ** get_address_of__slider_2() { return &____slider_2; }
	inline void set__slider_2(Slider_t297367283 * value)
	{
		____slider_2 = value;
		Il2CppCodeGenWriteBarrier(&____slider_2, value);
	}

	inline static int32_t get_offset_of__cycleManager_3() { return static_cast<int32_t>(offsetof(TimeSlider_t3354239974, ____cycleManager_3)); }
	inline SkyboxCycleManager_t61875227 * get__cycleManager_3() const { return ____cycleManager_3; }
	inline SkyboxCycleManager_t61875227 ** get_address_of__cycleManager_3() { return &____cycleManager_3; }
	inline void set__cycleManager_3(SkyboxCycleManager_t61875227 * value)
	{
		____cycleManager_3 = value;
		Il2CppCodeGenWriteBarrier(&____cycleManager_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
