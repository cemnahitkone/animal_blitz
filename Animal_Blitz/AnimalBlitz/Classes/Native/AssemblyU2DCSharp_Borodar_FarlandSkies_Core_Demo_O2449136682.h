﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.Core.Demo.ObjectRotator
struct  ObjectRotator_t2449136682  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 Borodar.FarlandSkies.Core.Demo.ObjectRotator::EulerAngles
	Vector3_t2243707580  ___EulerAngles_2;

public:
	inline static int32_t get_offset_of_EulerAngles_2() { return static_cast<int32_t>(offsetof(ObjectRotator_t2449136682, ___EulerAngles_2)); }
	inline Vector3_t2243707580  get_EulerAngles_2() const { return ___EulerAngles_2; }
	inline Vector3_t2243707580 * get_address_of_EulerAngles_2() { return &___EulerAngles_2; }
	inline void set_EulerAngles_2(Vector3_t2243707580  value)
	{
		___EulerAngles_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
