﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedList_2_gen2155713151MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::.ctor()
#define SortedList_2__ctor_m3115535410(__this, method) ((  void (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2__ctor_m1729224555_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::.ctor(System.Int32)
#define SortedList_2__ctor_m2307370453(__this, ___capacity0, method) ((  void (*) (SortedList_2_t3739096288 *, int32_t, const MethodInfo*))SortedList_2__ctor_m2711763692_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::.ctor(System.Int32,System.Collections.Generic.IComparer`1<TKey>)
#define SortedList_2__ctor_m1581633582(__this, ___capacity0, ___comparer1, method) ((  void (*) (SortedList_2_t3739096288 *, int32_t, Il2CppObject*, const MethodInfo*))SortedList_2__ctor_m3946252741_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::.cctor()
#define SortedList_2__cctor_m4086373267(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SortedList_2__cctor_m123454876_gshared)(__this /* static, unused */, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.ICollection.get_IsSynchronized()
#define SortedList_2_System_Collections_ICollection_get_IsSynchronized_m2643576670(__this, method) ((  bool (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2_System_Collections_ICollection_get_IsSynchronized_m2883733903_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.ICollection.get_SyncRoot()
#define SortedList_2_System_Collections_ICollection_get_SyncRoot_m1616110194(__this, method) ((  Il2CppObject * (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2_System_Collections_ICollection_get_SyncRoot_m189958735_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.IDictionary.get_Item(System.Object)
#define SortedList_2_System_Collections_IDictionary_get_Item_m161363426(__this, ___key0, method) ((  Il2CppObject * (*) (SortedList_2_t3739096288 *, Il2CppObject *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_Item_m183026243_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define SortedList_2_System_Collections_IDictionary_set_Item_m1262900987(__this, ___key0, ___value1, method) ((  void (*) (SortedList_2_t3739096288 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_set_Item_m1650836330_gshared)(__this, ___key0, ___value1, method)
// System.Collections.ICollection System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.IDictionary.get_Keys()
#define SortedList_2_System_Collections_IDictionary_get_Keys_m588624708(__this, method) ((  Il2CppObject * (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_Keys_m4034771553_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.IDictionary.get_Values()
#define SortedList_2_System_Collections_IDictionary_get_Values_m3288731562(__this, method) ((  Il2CppObject * (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_Values_m2053295209_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3487245620(__this, method) ((  bool (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3659435785_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Clear()
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Clear_m1248589730(__this, method) ((  void (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Clear_m3183542805_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m702036119(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedList_2_t3739096288 *, KeyValuePair_2U5BU5D_t2147208744*, int32_t, const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2333964160_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2183383419(__this, ___keyValuePair0, method) ((  void (*) (SortedList_2_t3739096288 *, KeyValuePair_2_t2879698341 , const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1313454404_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3598840149(__this, ___keyValuePair0, method) ((  bool (*) (SortedList_2_t3739096288 *, KeyValuePair_2_t2879698341 , const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2234949788_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m600821970(__this, ___keyValuePair0, method) ((  bool (*) (SortedList_2_t3739096288 *, KeyValuePair_2_t2879698341 , const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m29089843_gshared)(__this, ___keyValuePair0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define SortedList_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4182198188(__this, method) ((  Il2CppObject* (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2346275313_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.IEnumerable.GetEnumerator()
#define SortedList_2_System_Collections_IEnumerable_GetEnumerator_m349907217(__this, method) ((  Il2CppObject * (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2_System_Collections_IEnumerable_GetEnumerator_m1880108690_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define SortedList_2_System_Collections_IDictionary_Add_m885357292(__this, ___key0, ___value1, method) ((  void (*) (SortedList_2_t3739096288 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_Add_m3124956037_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.IDictionary.Contains(System.Object)
#define SortedList_2_System_Collections_IDictionary_Contains_m2128579052(__this, ___key0, method) ((  bool (*) (SortedList_2_t3739096288 *, Il2CppObject *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_Contains_m3320572661_gshared)(__this, ___key0, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.IDictionary.GetEnumerator()
#define SortedList_2_System_Collections_IDictionary_GetEnumerator_m3882244215(__this, method) ((  Il2CppObject * (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_GetEnumerator_m1020211388_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.IDictionary.Remove(System.Object)
#define SortedList_2_System_Collections_IDictionary_Remove_m2696905311(__this, ___key0, method) ((  void (*) (SortedList_2_t3739096288 *, Il2CppObject *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_Remove_m3985280494_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define SortedList_2_System_Collections_ICollection_CopyTo_m4023931426(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedList_2_t3739096288 *, Il2CppArray *, int32_t, const MethodInfo*))SortedList_2_System_Collections_ICollection_CopyTo_m2678002639_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::get_Count()
#define SortedList_2_get_Count_m1969515413(__this, method) ((  int32_t (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2_get_Count_m3953460655_gshared)(__this, method)
// TValue System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::get_Item(TKey)
#define SortedList_2_get_Item_m2259302855(__this, ___key0, method) ((  SkyParam_t4272832432 * (*) (SortedList_2_t3739096288 *, float, const MethodInfo*))SortedList_2_get_Item_m1048824628_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::set_Item(TKey,TValue)
#define SortedList_2_set_Item_m887053064(__this, ___key0, ___value1, method) ((  void (*) (SortedList_2_t3739096288 *, float, SkyParam_t4272832432 *, const MethodInfo*))SortedList_2_set_Item_m156738219_gshared)(__this, ___key0, ___value1, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::get_Capacity()
#define SortedList_2_get_Capacity_m2741715347(__this, method) ((  int32_t (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2_get_Capacity_m1664848572_gshared)(__this, method)
// System.Collections.Generic.IList`1<TKey> System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::get_Keys()
#define SortedList_2_get_Keys_m1499565401(__this, method) ((  Il2CppObject* (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2_get_Keys_m446487849_gshared)(__this, method)
// System.Collections.Generic.IList`1<TValue> System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::get_Values()
#define SortedList_2_get_Values_m2474289146(__this, method) ((  Il2CppObject* (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2_get_Values_m2693547929_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::Add(TKey,TValue)
#define SortedList_2_Add_m1530525105(__this, ___key0, ___value1, method) ((  void (*) (SortedList_2_t3739096288 *, float, SkyParam_t4272832432 *, const MethodInfo*))SortedList_2_Add_m3259317576_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::GetEnumerator()
#define SortedList_2_GetEnumerator_m4046670421(__this, method) ((  Il2CppObject* (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2_GetEnumerator_m1595810358_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::Clear()
#define SortedList_2_Clear_m3077385853(__this, method) ((  void (*) (SortedList_2_t3739096288 *, const MethodInfo*))SortedList_2_Clear_m325015304_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::RemoveAt(System.Int32)
#define SortedList_2_RemoveAt_m1833685700(__this, ___index0, method) ((  void (*) (SortedList_2_t3739096288 *, int32_t, const MethodInfo*))SortedList_2_RemoveAt_m3066111341_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::IndexOfKey(TKey)
#define SortedList_2_IndexOfKey_m4126751585(__this, ___key0, method) ((  int32_t (*) (SortedList_2_t3739096288 *, float, const MethodInfo*))SortedList_2_IndexOfKey_m1414598972_gshared)(__this, ___key0, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::IndexOfValue(TValue)
#define SortedList_2_IndexOfValue_m1721980681(__this, ___value0, method) ((  int32_t (*) (SortedList_2_t3739096288 *, SkyParam_t4272832432 *, const MethodInfo*))SortedList_2_IndexOfValue_m2882021324_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::EnsureCapacity(System.Int32,System.Int32)
#define SortedList_2_EnsureCapacity_m2942857342(__this, ___n0, ___free1, method) ((  void (*) (SortedList_2_t3739096288 *, int32_t, int32_t, const MethodInfo*))SortedList_2_EnsureCapacity_m807368997_gshared)(__this, ___n0, ___free1, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::PutImpl(TKey,TValue,System.Boolean)
#define SortedList_2_PutImpl_m1146305812(__this, ___key0, ___value1, ___overwrite2, method) ((  void (*) (SortedList_2_t3739096288 *, float, SkyParam_t4272832432 *, bool, const MethodInfo*))SortedList_2_PutImpl_m924608781_gshared)(__this, ___key0, ___value1, ___overwrite2, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::Init(System.Collections.Generic.IComparer`1<TKey>,System.Int32,System.Boolean)
#define SortedList_2_Init_m3055613011(__this, ___comparer0, ___capacity1, ___forceSize2, method) ((  void (*) (SortedList_2_t3739096288 *, Il2CppObject*, int32_t, bool, const MethodInfo*))SortedList_2_Init_m2434447812_gshared)(__this, ___comparer0, ___capacity1, ___forceSize2, method)
// System.Void System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::CopyToArray(System.Array,System.Int32,System.Collections.Generic.SortedList`2/EnumeratorMode<TKey,TValue>)
#define SortedList_2_CopyToArray_m3656358076(__this, ___arr0, ___i1, ___mode2, method) ((  void (*) (SortedList_2_t3739096288 *, Il2CppArray *, int32_t, int32_t, const MethodInfo*))SortedList_2_CopyToArray_m798904113_gshared)(__this, ___arr0, ___i1, ___mode2, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::Find(TKey)
#define SortedList_2_Find_m3569872070(__this, ___key0, method) ((  int32_t (*) (SortedList_2_t3739096288 *, float, const MethodInfo*))SortedList_2_Find_m2937918771_gshared)(__this, ___key0, method)
// TKey System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::ToKey(System.Object)
#define SortedList_2_ToKey_m2584211554(__this, ___key0, method) ((  float (*) (SortedList_2_t3739096288 *, Il2CppObject *, const MethodInfo*))SortedList_2_ToKey_m1801778245_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::ToValue(System.Object)
#define SortedList_2_ToValue_m173059234(__this, ___value0, method) ((  SkyParam_t4272832432 * (*) (SortedList_2_t3739096288 *, Il2CppObject *, const MethodInfo*))SortedList_2_ToValue_m335286821_gshared)(__this, ___value0, method)
// TKey System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::KeyAt(System.Int32)
#define SortedList_2_KeyAt_m1480901051(__this, ___index0, method) ((  float (*) (SortedList_2_t3739096288 *, int32_t, const MethodInfo*))SortedList_2_KeyAt_m300098002_gshared)(__this, ___index0, method)
// TValue System.Collections.Generic.SortedList`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.SkyParam>::ValueAt(System.Int32)
#define SortedList_2_ValueAt_m332373531(__this, ___index0, method) ((  SkyParam_t4272832432 * (*) (SortedList_2_t3739096288 *, int32_t, const MethodInfo*))SortedList_2_ValueAt_m2566210962_gshared)(__this, ___index0, method)
