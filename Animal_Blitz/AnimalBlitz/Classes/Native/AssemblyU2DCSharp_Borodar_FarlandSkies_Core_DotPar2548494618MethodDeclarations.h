﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1<System.Object>
struct SortedParamsList_1_t2548494618;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1<System.Object>::.ctor()
extern "C"  void SortedParamsList_1__ctor_m4109281813_gshared (SortedParamsList_1_t2548494618 * __this, const MethodInfo* method);
#define SortedParamsList_1__ctor_m4109281813(__this, method) ((  void (*) (SortedParamsList_1_t2548494618 *, const MethodInfo*))SortedParamsList_1__ctor_m4109281813_gshared)(__this, method)
// System.Void Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1<System.Object>::Init()
extern "C"  void SortedParamsList_1_Init_m2929646359_gshared (SortedParamsList_1_t2548494618 * __this, const MethodInfo* method);
#define SortedParamsList_1_Init_m2929646359(__this, method) ((  void (*) (SortedParamsList_1_t2548494618 *, const MethodInfo*))SortedParamsList_1_Init_m2929646359_gshared)(__this, method)
// System.Void Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1<System.Object>::Update()
extern "C"  void SortedParamsList_1_Update_m4026540864_gshared (SortedParamsList_1_t2548494618 * __this, const MethodInfo* method);
#define SortedParamsList_1_Update_m4026540864(__this, method) ((  void (*) (SortedParamsList_1_t2548494618 *, const MethodInfo*))SortedParamsList_1_Update_m4026540864_gshared)(__this, method)
