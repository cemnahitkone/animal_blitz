﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HideCursor
struct HideCursor_t48087058;

#include "codegen/il2cpp-codegen.h"

// System.Void HideCursor::.ctor()
extern "C"  void HideCursor__ctor_m272866968 (HideCursor_t48087058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HideCursor::Start()
extern "C"  void HideCursor_Start_m3141370208 (HideCursor_t48087058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HideCursor::Update()
extern "C"  void HideCursor_Update_m3788233949 (HideCursor_t48087058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HideCursor::Main()
extern "C"  void HideCursor_Main_m3625278983 (HideCursor_t48087058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
