﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.Transition
struct Transition_t2490031921;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void Ricimi.Transition::.ctor()
extern "C"  void Transition__ctor_m1835182235 (Transition_t2490031921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.Transition::Awake()
extern "C"  void Transition_Awake_m3044031284 (Transition_t2490031921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.Transition::LoadLevel(System.String,System.Single,UnityEngine.Color)
extern "C"  void Transition_LoadLevel_m3399519432 (Il2CppObject * __this /* static, unused */, String_t* ___level0, float ___duration1, Color_t2020392075  ___color2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.Transition::StartFade(System.String,System.Single,UnityEngine.Color)
extern "C"  void Transition_StartFade_m3181661324 (Transition_t2490031921 * __this, String_t* ___level0, float ___duration1, Color_t2020392075  ___fadeColor2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Ricimi.Transition::RunFade(System.String,System.Single,UnityEngine.Color)
extern "C"  Il2CppObject * Transition_RunFade_m2930965847 (Transition_t2490031921 * __this, String_t* ___level0, float ___duration1, Color_t2020392075  ___fadeColor2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
