﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Single,System.Object>
struct GetEnumeratorU3Ec__Iterator0_t232740582;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"

// System.Void System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Single,System.Object>::.ctor()
extern "C"  void GetEnumeratorU3Ec__Iterator0__ctor_m1279136728_gshared (GetEnumeratorU3Ec__Iterator0_t232740582 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0__ctor_m1279136728(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator0_t232740582 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0__ctor_m1279136728_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Single,System.Object>::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_Current()
extern "C"  KeyValuePair_2_t1296315204  GetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m1135742595_gshared (GetEnumeratorU3Ec__Iterator0_t232740582 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m1135742595(__this, method) ((  KeyValuePair_2_t1296315204  (*) (GetEnumeratorU3Ec__Iterator0_t232740582 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m1135742595_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Single,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * GetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3279351748_gshared (GetEnumeratorU3Ec__Iterator0_t232740582 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3279351748(__this, method) ((  Il2CppObject * (*) (GetEnumeratorU3Ec__Iterator0_t232740582 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3279351748_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Single,System.Object>::MoveNext()
extern "C"  bool GetEnumeratorU3Ec__Iterator0_MoveNext_m3430291642_gshared (GetEnumeratorU3Ec__Iterator0_t232740582 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_MoveNext_m3430291642(__this, method) ((  bool (*) (GetEnumeratorU3Ec__Iterator0_t232740582 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_MoveNext_m3430291642_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Single,System.Object>::Dispose()
extern "C"  void GetEnumeratorU3Ec__Iterator0_Dispose_m1473486327_gshared (GetEnumeratorU3Ec__Iterator0_t232740582 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_Dispose_m1473486327(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator0_t232740582 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_Dispose_m1473486327_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Single,System.Object>::Reset()
extern "C"  void GetEnumeratorU3Ec__Iterator0_Reset_m941033285_gshared (GetEnumeratorU3Ec__Iterator0_t232740582 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_Reset_m941033285(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator0_t232740582 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_Reset_m941033285_gshared)(__this, method)
