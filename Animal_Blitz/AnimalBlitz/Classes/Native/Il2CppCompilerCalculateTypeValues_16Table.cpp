﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_DefaultUriParser1591960796.h"
#include "System_System_Diagnostics_CorrelationManager4016999475.h"
#include "System_System_Diagnostics_DefaultTraceListener1568159610.h"
#include "System_System_Diagnostics_DiagnosticsConfiguration1565268762.h"
#include "System_System_Diagnostics_DiagnosticsConfigurationH610492850.h"
#include "System_System_Diagnostics_DiagnosticsConfiguration1009623312.h"
#include "System_System_Diagnostics_SourceLevels1530190938.h"
#include "System_System_Diagnostics_TraceImplSettings1186465586.h"
#include "System_System_Diagnostics_TraceImpl3585635237.h"
#include "System_System_Diagnostics_TraceListenerCollection2289511703.h"
#include "System_System_Diagnostics_TraceListener3414949279.h"
#include "System_System_Diagnostics_TraceOptions4183547961.h"
#include "System_System_Diagnostics_TraceSourceInfo8795084.h"
#include "System_System_GenericUriParser2599285286.h"
#include "System_System_IO_Compression_CompressionMode1471062003.h"
#include "System_System_IO_Compression_DeflateStream3198596725.h"
#include "System_System_IO_Compression_DeflateStream_Unmanag1990215745.h"
#include "System_System_IO_Compression_DeflateStream_ReadMet3362229488.h"
#include "System_System_IO_Compression_DeflateStream_WriteMe1894833619.h"
#include "System_System_IO_Compression_GZipStream2274754946.h"
#include "System_System_Net_AuthenticationManager3410876775.h"
#include "System_System_Net_Authorization1602399.h"
#include "System_System_Net_Cache_HttpRequestCacheLevel1019742321.h"
#include "System_System_Net_Cache_RequestCacheLevel2979444753.h"
#include "System_System_Net_ChunkStream91719323.h"
#include "System_System_Net_ChunkStream_State4001596355.h"
#include "System_System_Net_ChunkStream_Chunk3860501603.h"
#include "System_System_Net_Configuration_AuthenticationModu1084300802.h"
#include "System_System_Net_Configuration_AuthenticationModu1426459758.h"
#include "System_System_Net_Configuration_AuthenticationModu1750570770.h"
#include "System_System_Net_Configuration_BypassElementColle3411512674.h"
#include "System_System_Net_Configuration_BypassElement4253212366.h"
#include "System_System_Net_Configuration_ConnectionManageme3244012643.h"
#include "System_System_Net_Configuration_ConnectionManageme1734801665.h"
#include "System_System_Net_Configuration_ConnectionManageme2657447783.h"
#include "System_System_Net_Configuration_HandlersUtil1360800625.h"
#include "System_System_Net_Configuration_ConnectionManageme1533889992.h"
#include "System_System_Net_Configuration_DefaultProxySectio2916409848.h"
#include "System_System_Net_Configuration_FtpCachePolicyEleme919314008.h"
#include "System_System_Net_Configuration_HttpCachePolicyElem129882946.h"
#include "System_System_Net_Configuration_HttpWebRequestElem2106051069.h"
#include "System_System_Net_Configuration_Ipv6Element977943121.h"
#include "System_System_Net_Configuration_MailSettingsSectionG25838306.h"
#include "System_System_Net_Configuration_ModuleElement3031348726.h"
#include "System_System_Net_Configuration_NetConfigurationHan415947373.h"
#include "System_System_Net_Configuration_NetSectionGroup2546413291.h"
#include "System_System_Net_Configuration_PerformanceCounter1606215027.h"
#include "System_System_Net_Configuration_ProxyElement1414493002.h"
#include "System_System_Net_Configuration_ProxyElement_Bypas3271087145.h"
#include "System_System_Net_Configuration_ProxyElement_UseSy1457033350.h"
#include "System_System_Net_Configuration_ProxyElement_AutoDe735769531.h"
#include "System_System_Net_Configuration_RequestCachingSect1642835391.h"
#include "System_System_Net_Configuration_ServicePointManage1132364388.h"
#include "System_System_Net_Configuration_SettingsSection2300716058.h"
#include "System_System_Net_Configuration_SocketElement792962077.h"
#include "System_System_Net_Configuration_WebProxyScriptElem1017943775.h"
#include "System_System_Net_Configuration_WebRequestModuleEl2218695785.h"
#include "System_System_Net_Configuration_WebRequestModuleEl4070853259.h"
#include "System_System_Net_Configuration_WebRequestModuleHa3598459681.h"
#include "System_System_Net_Configuration_WebRequestModulesS3717257007.h"
#include "System_System_Net_CookieCollection521422364.h"
#include "System_System_Net_CookieCollection_CookieCollectio3570802680.h"
#include "System_System_Net_CookieContainer2808809223.h"
#include "System_System_Net_Cookie3154017544.h"
#include "System_System_Net_CookieException1505724635.h"
#include "System_System_Net_DecompressionMethods2530166567.h"
#include "System_System_Net_DefaultCertificatePolicy2545332216.h"
#include "System_System_Net_Dns1335526197.h"
#include "System_System_Net_EndPoint4156119363.h"
#include "System_System_Net_FileWebRequestCreator1109072211.h"
#include "System_System_Net_FileWebRequest1571840111.h"
#include "System_System_Net_FileWebRequest_FileWebStream1952319648.h"
#include "System_System_Net_FileWebRequest_GetResponseCallba3725471744.h"
#include "System_System_Net_FileWebResponse1934981865.h"
#include "System_System_Net_FtpAsyncResult770082413.h"
#include "System_System_Net_FtpDataStream3588258764.h"
#include "System_System_Net_FtpDataStream_WriteDelegate888270799.h"
#include "System_System_Net_FtpDataStream_ReadDelegate1559754630.h"
#include "System_System_Net_FtpRequestCreator3711983251.h"
#include "System_System_Net_FtpStatusCode1448112771.h"
#include "System_System_Net_FtpWebRequest3120721823.h"
#include "System_System_Net_FtpWebRequest_RequestState4256633122.h"
#include "System_System_Net_FtpStatus3714482970.h"
#include "System_System_Net_FtpWebResponse2609078769.h"
#include "System_System_Net_GlobalProxySelection2251180943.h"
#include "System_System_Net_HttpRequestCreator1416559589.h"
#include "System_System_Net_HttpStatusCode1898409641.h"
#include "System_System_Net_HttpVersion1276659706.h"
#include "System_System_Net_HttpWebRequest1951404513.h"
#include "System_System_Net_HttpWebResponse2828383075.h"
#include "System_System_Net_CookieParser1405985527.h"
#include "System_System_Net_IPAddress1399971723.h"
#include "System_System_Net_IPEndPoint2615413766.h"
#include "System_System_Net_IPHostEntry994738509.h"
#include "System_System_Net_IPv6Address2596635879.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (DefaultUriParser_t1591960796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (CorrelationManager_t4016999475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1601[1] = 
{
	CorrelationManager_t4016999475::get_offset_of_op_stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (DefaultTraceListener_t1568159610), -1, sizeof(DefaultTraceListener_t1568159610_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1602[5] = 
{
	DefaultTraceListener_t1568159610_StaticFields::get_offset_of_OnWin32_7(),
	DefaultTraceListener_t1568159610_StaticFields::get_offset_of_MonoTracePrefix_8(),
	DefaultTraceListener_t1568159610_StaticFields::get_offset_of_MonoTraceFile_9(),
	DefaultTraceListener_t1568159610::get_offset_of_logFileName_10(),
	DefaultTraceListener_t1568159610::get_offset_of_assertUiEnabled_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (DiagnosticsConfiguration_t1565268762), -1, sizeof(DiagnosticsConfiguration_t1565268762_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1603[1] = 
{
	DiagnosticsConfiguration_t1565268762_StaticFields::get_offset_of_settings_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (DiagnosticsConfigurationHandler_t610492850), -1, sizeof(DiagnosticsConfigurationHandler_t610492850_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1604[5] = 
{
	DiagnosticsConfigurationHandler_t610492850::get_offset_of_configValues_0(),
	DiagnosticsConfigurationHandler_t610492850::get_offset_of_elementHandlers_1(),
	DiagnosticsConfigurationHandler_t610492850_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_2(),
	DiagnosticsConfigurationHandler_t610492850_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_3(),
	DiagnosticsConfigurationHandler_t610492850_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (ElementHandler_t1009623312), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (SourceLevels_t1530190938)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1606[9] = 
{
	SourceLevels_t1530190938::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (TraceImplSettings_t1186465586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1607[4] = 
{
	TraceImplSettings_t1186465586::get_offset_of_AutoFlush_0(),
	TraceImplSettings_t1186465586::get_offset_of_IndentLevel_1(),
	TraceImplSettings_t1186465586::get_offset_of_IndentSize_2(),
	TraceImplSettings_t1186465586::get_offset_of_Listeners_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (TraceImpl_t3585635237), -1, sizeof(TraceImpl_t3585635237_StaticFields), sizeof(TraceImpl_t3585635237_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable1608[6] = 
{
	TraceImpl_t3585635237_StaticFields::get_offset_of_initLock_0(),
	TraceImpl_t3585635237_StaticFields::get_offset_of_autoFlush_1(),
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	TraceImpl_t3585635237_StaticFields::get_offset_of_listeners_4(),
	TraceImpl_t3585635237_StaticFields::get_offset_of_correlation_manager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (TraceListenerCollection_t2289511703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1609[1] = 
{
	TraceListenerCollection_t2289511703::get_offset_of_listeners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (TraceListener_t3414949279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1610[6] = 
{
	TraceListener_t3414949279::get_offset_of_indentLevel_1(),
	TraceListener_t3414949279::get_offset_of_indentSize_2(),
	TraceListener_t3414949279::get_offset_of_attributes_3(),
	TraceListener_t3414949279::get_offset_of_options_4(),
	TraceListener_t3414949279::get_offset_of_name_5(),
	TraceListener_t3414949279::get_offset_of_needIndent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (TraceOptions_t4183547961)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1611[8] = 
{
	TraceOptions_t4183547961::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (TraceSourceInfo_t8795084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1612[3] = 
{
	TraceSourceInfo_t8795084::get_offset_of_name_0(),
	TraceSourceInfo_t8795084::get_offset_of_levels_1(),
	TraceSourceInfo_t8795084::get_offset_of_listeners_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (GenericUriParser_t2599285286), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (CompressionMode_t1471062003)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1614[3] = 
{
	CompressionMode_t1471062003::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (DeflateStream_t3198596725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1615[8] = 
{
	DeflateStream_t3198596725::get_offset_of_base_stream_2(),
	DeflateStream_t3198596725::get_offset_of_mode_3(),
	DeflateStream_t3198596725::get_offset_of_leaveOpen_4(),
	DeflateStream_t3198596725::get_offset_of_disposed_5(),
	DeflateStream_t3198596725::get_offset_of_feeder_6(),
	DeflateStream_t3198596725::get_offset_of_z_stream_7(),
	DeflateStream_t3198596725::get_offset_of_io_buffer_8(),
	DeflateStream_t3198596725::get_offset_of_data_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (UnmanagedReadOrWrite_t1990215745), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (ReadMethod_t3362229488), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (WriteMethod_t1894833619), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (GZipStream_t2274754946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1619[1] = 
{
	GZipStream_t2274754946::get_offset_of_deflateStream_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (AuthenticationManager_t3410876775), -1, sizeof(AuthenticationManager_t3410876775_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1620[3] = 
{
	AuthenticationManager_t3410876775_StaticFields::get_offset_of_modules_0(),
	AuthenticationManager_t3410876775_StaticFields::get_offset_of_locker_1(),
	AuthenticationManager_t3410876775_StaticFields::get_offset_of_credential_policy_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (Authorization_t1602399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1621[3] = 
{
	Authorization_t1602399::get_offset_of_token_0(),
	Authorization_t1602399::get_offset_of_complete_1(),
	Authorization_t1602399::get_offset_of_module_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (HttpRequestCacheLevel_t1019742321)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1622[10] = 
{
	HttpRequestCacheLevel_t1019742321::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (RequestCacheLevel_t2979444753)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1623[8] = 
{
	RequestCacheLevel_t2979444753::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (ChunkStream_t91719323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1624[9] = 
{
	ChunkStream_t91719323::get_offset_of_headers_0(),
	ChunkStream_t91719323::get_offset_of_chunkSize_1(),
	ChunkStream_t91719323::get_offset_of_chunkRead_2(),
	ChunkStream_t91719323::get_offset_of_state_3(),
	ChunkStream_t91719323::get_offset_of_saved_4(),
	ChunkStream_t91719323::get_offset_of_sawCR_5(),
	ChunkStream_t91719323::get_offset_of_gotit_6(),
	ChunkStream_t91719323::get_offset_of_trailerState_7(),
	ChunkStream_t91719323::get_offset_of_chunks_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (State_t4001596355)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1625[5] = 
{
	State_t4001596355::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (Chunk_t3860501603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1626[2] = 
{
	Chunk_t3860501603::get_offset_of_Bytes_0(),
	Chunk_t3860501603::get_offset_of_Offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (AuthenticationModuleElementCollection_t1084300802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (AuthenticationModuleElement_t1426459758), -1, sizeof(AuthenticationModuleElement_t1426459758_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1628[2] = 
{
	AuthenticationModuleElement_t1426459758_StaticFields::get_offset_of_properties_13(),
	AuthenticationModuleElement_t1426459758_StaticFields::get_offset_of_typeProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (AuthenticationModulesSection_t1750570770), -1, sizeof(AuthenticationModulesSection_t1750570770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1629[2] = 
{
	AuthenticationModulesSection_t1750570770_StaticFields::get_offset_of_properties_17(),
	AuthenticationModulesSection_t1750570770_StaticFields::get_offset_of_authenticationModulesProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (BypassElementCollection_t3411512674), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (BypassElement_t4253212366), -1, sizeof(BypassElement_t4253212366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1631[2] = 
{
	BypassElement_t4253212366_StaticFields::get_offset_of_properties_13(),
	BypassElement_t4253212366_StaticFields::get_offset_of_addressProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (ConnectionManagementElementCollection_t3244012643), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (ConnectionManagementElement_t1734801665), -1, sizeof(ConnectionManagementElement_t1734801665_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1633[3] = 
{
	ConnectionManagementElement_t1734801665_StaticFields::get_offset_of_properties_13(),
	ConnectionManagementElement_t1734801665_StaticFields::get_offset_of_addressProp_14(),
	ConnectionManagementElement_t1734801665_StaticFields::get_offset_of_maxConnectionProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (ConnectionManagementData_t2657447783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1634[1] = 
{
	ConnectionManagementData_t2657447783::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (HandlersUtil_t1360800625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (ConnectionManagementSection_t1533889992), -1, sizeof(ConnectionManagementSection_t1533889992_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1636[2] = 
{
	ConnectionManagementSection_t1533889992_StaticFields::get_offset_of_connectionManagementProp_17(),
	ConnectionManagementSection_t1533889992_StaticFields::get_offset_of_properties_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (DefaultProxySection_t2916409848), -1, sizeof(DefaultProxySection_t2916409848_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1637[6] = 
{
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_properties_17(),
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_bypassListProp_18(),
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_enabledProp_19(),
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_moduleProp_20(),
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_proxyProp_21(),
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_useDefaultCredentialsProp_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (FtpCachePolicyElement_t919314008), -1, sizeof(FtpCachePolicyElement_t919314008_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1638[2] = 
{
	FtpCachePolicyElement_t919314008_StaticFields::get_offset_of_policyLevelProp_13(),
	FtpCachePolicyElement_t919314008_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (HttpCachePolicyElement_t129882946), -1, sizeof(HttpCachePolicyElement_t129882946_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1639[5] = 
{
	HttpCachePolicyElement_t129882946_StaticFields::get_offset_of_maximumAgeProp_13(),
	HttpCachePolicyElement_t129882946_StaticFields::get_offset_of_maximumStaleProp_14(),
	HttpCachePolicyElement_t129882946_StaticFields::get_offset_of_minimumFreshProp_15(),
	HttpCachePolicyElement_t129882946_StaticFields::get_offset_of_policyLevelProp_16(),
	HttpCachePolicyElement_t129882946_StaticFields::get_offset_of_properties_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (HttpWebRequestElement_t2106051069), -1, sizeof(HttpWebRequestElement_t2106051069_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1640[5] = 
{
	HttpWebRequestElement_t2106051069_StaticFields::get_offset_of_maximumErrorResponseLengthProp_13(),
	HttpWebRequestElement_t2106051069_StaticFields::get_offset_of_maximumResponseHeadersLengthProp_14(),
	HttpWebRequestElement_t2106051069_StaticFields::get_offset_of_maximumUnauthorizedUploadLengthProp_15(),
	HttpWebRequestElement_t2106051069_StaticFields::get_offset_of_useUnsafeHeaderParsingProp_16(),
	HttpWebRequestElement_t2106051069_StaticFields::get_offset_of_properties_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (Ipv6Element_t977943121), -1, sizeof(Ipv6Element_t977943121_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1641[2] = 
{
	Ipv6Element_t977943121_StaticFields::get_offset_of_properties_13(),
	Ipv6Element_t977943121_StaticFields::get_offset_of_enabledProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (MailSettingsSectionGroup_t25838306), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (ModuleElement_t3031348726), -1, sizeof(ModuleElement_t3031348726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1643[2] = 
{
	ModuleElement_t3031348726_StaticFields::get_offset_of_properties_13(),
	ModuleElement_t3031348726_StaticFields::get_offset_of_typeProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (NetConfigurationHandler_t415947373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (NetSectionGroup_t2546413291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (PerformanceCountersElement_t1606215027), -1, sizeof(PerformanceCountersElement_t1606215027_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1646[2] = 
{
	PerformanceCountersElement_t1606215027_StaticFields::get_offset_of_enabledProp_13(),
	PerformanceCountersElement_t1606215027_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (ProxyElement_t1414493002), -1, sizeof(ProxyElement_t1414493002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1647[6] = 
{
	ProxyElement_t1414493002_StaticFields::get_offset_of_properties_13(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_autoDetectProp_14(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_bypassOnLocalProp_15(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_proxyAddressProp_16(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_scriptLocationProp_17(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_useSystemDefaultProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (BypassOnLocalValues_t3271087145)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1648[4] = 
{
	BypassOnLocalValues_t3271087145::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (UseSystemDefaultValues_t1457033350)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1649[4] = 
{
	UseSystemDefaultValues_t1457033350::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (AutoDetectValues_t735769531)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1650[4] = 
{
	AutoDetectValues_t735769531::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (RequestCachingSection_t1642835391), -1, sizeof(RequestCachingSection_t1642835391_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1651[7] = 
{
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_properties_17(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_defaultFtpCachePolicyProp_18(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_defaultHttpCachePolicyProp_19(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_defaultPolicyLevelProp_20(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_disableAllCachingProp_21(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_isPrivateCacheProp_22(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_unspecifiedMaximumAgeProp_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (ServicePointManagerElement_t1132364388), -1, sizeof(ServicePointManagerElement_t1132364388_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1652[7] = 
{
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_properties_13(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_checkCertificateNameProp_14(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_checkCertificateRevocationListProp_15(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_dnsRefreshTimeoutProp_16(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_enableDnsRoundRobinProp_17(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_expect100ContinueProp_18(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_useNagleAlgorithmProp_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (SettingsSection_t2300716058), -1, sizeof(SettingsSection_t2300716058_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1653[7] = 
{
	SettingsSection_t2300716058_StaticFields::get_offset_of_properties_17(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_httpWebRequestProp_18(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_ipv6Prop_19(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_performanceCountersProp_20(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_servicePointManagerProp_21(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_webProxyScriptProp_22(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_socketProp_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (SocketElement_t792962077), -1, sizeof(SocketElement_t792962077_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1654[3] = 
{
	SocketElement_t792962077_StaticFields::get_offset_of_properties_13(),
	SocketElement_t792962077_StaticFields::get_offset_of_alwaysUseCompletionPortsForAcceptProp_14(),
	SocketElement_t792962077_StaticFields::get_offset_of_alwaysUseCompletionPortsForConnectProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (WebProxyScriptElement_t1017943775), -1, sizeof(WebProxyScriptElement_t1017943775_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1655[2] = 
{
	WebProxyScriptElement_t1017943775_StaticFields::get_offset_of_downloadTimeoutProp_13(),
	WebProxyScriptElement_t1017943775_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (WebRequestModuleElementCollection_t2218695785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (WebRequestModuleElement_t4070853259), -1, sizeof(WebRequestModuleElement_t4070853259_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1657[3] = 
{
	WebRequestModuleElement_t4070853259_StaticFields::get_offset_of_properties_13(),
	WebRequestModuleElement_t4070853259_StaticFields::get_offset_of_prefixProp_14(),
	WebRequestModuleElement_t4070853259_StaticFields::get_offset_of_typeProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (WebRequestModuleHandler_t3598459681), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (WebRequestModulesSection_t3717257007), -1, sizeof(WebRequestModulesSection_t3717257007_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1659[2] = 
{
	WebRequestModulesSection_t3717257007_StaticFields::get_offset_of_properties_17(),
	WebRequestModulesSection_t3717257007_StaticFields::get_offset_of_webRequestModulesProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (CookieCollection_t521422364), -1, sizeof(CookieCollection_t521422364_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1660[2] = 
{
	CookieCollection_t521422364::get_offset_of_list_0(),
	CookieCollection_t521422364_StaticFields::get_offset_of_Comparer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (CookieCollectionComparer_t3570802680), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (CookieContainer_t2808809223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1662[4] = 
{
	CookieContainer_t2808809223::get_offset_of_capacity_0(),
	CookieContainer_t2808809223::get_offset_of_perDomainCapacity_1(),
	CookieContainer_t2808809223::get_offset_of_maxCookieSize_2(),
	CookieContainer_t2808809223::get_offset_of_cookies_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (Cookie_t3154017544), -1, sizeof(Cookie_t3154017544_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1663[18] = 
{
	Cookie_t3154017544::get_offset_of_comment_0(),
	Cookie_t3154017544::get_offset_of_commentUri_1(),
	Cookie_t3154017544::get_offset_of_discard_2(),
	Cookie_t3154017544::get_offset_of_domain_3(),
	Cookie_t3154017544::get_offset_of_expires_4(),
	Cookie_t3154017544::get_offset_of_httpOnly_5(),
	Cookie_t3154017544::get_offset_of_name_6(),
	Cookie_t3154017544::get_offset_of_path_7(),
	Cookie_t3154017544::get_offset_of_port_8(),
	Cookie_t3154017544::get_offset_of_ports_9(),
	Cookie_t3154017544::get_offset_of_secure_10(),
	Cookie_t3154017544::get_offset_of_timestamp_11(),
	Cookie_t3154017544::get_offset_of_val_12(),
	Cookie_t3154017544::get_offset_of_version_13(),
	Cookie_t3154017544_StaticFields::get_offset_of_reservedCharsName_14(),
	Cookie_t3154017544_StaticFields::get_offset_of_portSeparators_15(),
	Cookie_t3154017544_StaticFields::get_offset_of_tspecials_16(),
	Cookie_t3154017544::get_offset_of_exact_domain_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (CookieException_t1505724635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (DecompressionMethods_t2530166567)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1665[4] = 
{
	DecompressionMethods_t2530166567::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (DefaultCertificatePolicy_t2545332216), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (Dns_t1335526197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (EndPoint_t4156119363), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (FileWebRequestCreator_t1109072211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (FileWebRequest_t1571840111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1670[14] = 
{
	FileWebRequest_t1571840111::get_offset_of_uri_6(),
	FileWebRequest_t1571840111::get_offset_of_webHeaders_7(),
	FileWebRequest_t1571840111::get_offset_of_credentials_8(),
	FileWebRequest_t1571840111::get_offset_of_connectionGroup_9(),
	FileWebRequest_t1571840111::get_offset_of_contentLength_10(),
	FileWebRequest_t1571840111::get_offset_of_fileAccess_11(),
	FileWebRequest_t1571840111::get_offset_of_method_12(),
	FileWebRequest_t1571840111::get_offset_of_proxy_13(),
	FileWebRequest_t1571840111::get_offset_of_preAuthenticate_14(),
	FileWebRequest_t1571840111::get_offset_of_timeout_15(),
	FileWebRequest_t1571840111::get_offset_of_webResponse_16(),
	FileWebRequest_t1571840111::get_offset_of_requestEndEvent_17(),
	FileWebRequest_t1571840111::get_offset_of_requesting_18(),
	FileWebRequest_t1571840111::get_offset_of_asyncResponding_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (FileWebStream_t1952319648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1671[1] = 
{
	FileWebStream_t1952319648::get_offset_of_webRequest_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (GetResponseCallback_t3725471744), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (FileWebResponse_t1934981865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1673[5] = 
{
	FileWebResponse_t1934981865::get_offset_of_responseUri_1(),
	FileWebResponse_t1934981865::get_offset_of_fileStream_2(),
	FileWebResponse_t1934981865::get_offset_of_contentLength_3(),
	FileWebResponse_t1934981865::get_offset_of_webHeaders_4(),
	FileWebResponse_t1934981865::get_offset_of_disposed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (FtpAsyncResult_t770082413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1674[9] = 
{
	FtpAsyncResult_t770082413::get_offset_of_response_0(),
	FtpAsyncResult_t770082413::get_offset_of_waitHandle_1(),
	FtpAsyncResult_t770082413::get_offset_of_exception_2(),
	FtpAsyncResult_t770082413::get_offset_of_callback_3(),
	FtpAsyncResult_t770082413::get_offset_of_stream_4(),
	FtpAsyncResult_t770082413::get_offset_of_state_5(),
	FtpAsyncResult_t770082413::get_offset_of_completed_6(),
	FtpAsyncResult_t770082413::get_offset_of_synch_7(),
	FtpAsyncResult_t770082413::get_offset_of_locker_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (FtpDataStream_t3588258764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1675[5] = 
{
	FtpDataStream_t3588258764::get_offset_of_request_2(),
	FtpDataStream_t3588258764::get_offset_of_networkStream_3(),
	FtpDataStream_t3588258764::get_offset_of_disposed_4(),
	FtpDataStream_t3588258764::get_offset_of_isRead_5(),
	FtpDataStream_t3588258764::get_offset_of_totalRead_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (WriteDelegate_t888270799), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (ReadDelegate_t1559754630), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (FtpRequestCreator_t3711983251), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (FtpStatusCode_t1448112771)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1679[38] = 
{
	FtpStatusCode_t1448112771::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (FtpWebRequest_t3120721823), -1, sizeof(FtpWebRequest_t3120721823_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1680[31] = 
{
	FtpWebRequest_t3120721823::get_offset_of_requestUri_6(),
	FtpWebRequest_t3120721823::get_offset_of_file_name_7(),
	FtpWebRequest_t3120721823::get_offset_of_servicePoint_8(),
	FtpWebRequest_t3120721823::get_offset_of_origDataStream_9(),
	FtpWebRequest_t3120721823::get_offset_of_dataStream_10(),
	FtpWebRequest_t3120721823::get_offset_of_controlStream_11(),
	FtpWebRequest_t3120721823::get_offset_of_controlReader_12(),
	FtpWebRequest_t3120721823::get_offset_of_credentials_13(),
	FtpWebRequest_t3120721823::get_offset_of_hostEntry_14(),
	FtpWebRequest_t3120721823::get_offset_of_localEndPoint_15(),
	FtpWebRequest_t3120721823::get_offset_of_proxy_16(),
	FtpWebRequest_t3120721823::get_offset_of_timeout_17(),
	FtpWebRequest_t3120721823::get_offset_of_rwTimeout_18(),
	FtpWebRequest_t3120721823::get_offset_of_offset_19(),
	FtpWebRequest_t3120721823::get_offset_of_binary_20(),
	FtpWebRequest_t3120721823::get_offset_of_enableSsl_21(),
	FtpWebRequest_t3120721823::get_offset_of_usePassive_22(),
	FtpWebRequest_t3120721823::get_offset_of_keepAlive_23(),
	FtpWebRequest_t3120721823::get_offset_of_method_24(),
	FtpWebRequest_t3120721823::get_offset_of_renameTo_25(),
	FtpWebRequest_t3120721823::get_offset_of_locker_26(),
	FtpWebRequest_t3120721823::get_offset_of_requestState_27(),
	FtpWebRequest_t3120721823::get_offset_of_asyncResult_28(),
	FtpWebRequest_t3120721823::get_offset_of_ftpResponse_29(),
	FtpWebRequest_t3120721823::get_offset_of_requestStream_30(),
	FtpWebRequest_t3120721823::get_offset_of_initial_path_31(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_supportedCommands_32(),
	FtpWebRequest_t3120721823::get_offset_of_callback_33(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_34(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_35(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (RequestState_t4256633122)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1681[10] = 
{
	RequestState_t4256633122::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (FtpStatus_t3714482970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1682[2] = 
{
	FtpStatus_t3714482970::get_offset_of_statusCode_0(),
	FtpStatus_t3714482970::get_offset_of_statusDescription_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (FtpWebResponse_t2609078769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1683[12] = 
{
	FtpWebResponse_t2609078769::get_offset_of_stream_1(),
	FtpWebResponse_t2609078769::get_offset_of_uri_2(),
	FtpWebResponse_t2609078769::get_offset_of_statusCode_3(),
	FtpWebResponse_t2609078769::get_offset_of_lastModified_4(),
	FtpWebResponse_t2609078769::get_offset_of_bannerMessage_5(),
	FtpWebResponse_t2609078769::get_offset_of_welcomeMessage_6(),
	FtpWebResponse_t2609078769::get_offset_of_exitMessage_7(),
	FtpWebResponse_t2609078769::get_offset_of_statusDescription_8(),
	FtpWebResponse_t2609078769::get_offset_of_method_9(),
	FtpWebResponse_t2609078769::get_offset_of_disposed_10(),
	FtpWebResponse_t2609078769::get_offset_of_request_11(),
	FtpWebResponse_t2609078769::get_offset_of_contentLength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (GlobalProxySelection_t2251180943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (HttpRequestCreator_t1416559589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (HttpStatusCode_t1898409641)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1686[47] = 
{
	HttpStatusCode_t1898409641::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (HttpVersion_t1276659706), -1, sizeof(HttpVersion_t1276659706_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1687[2] = 
{
	HttpVersion_t1276659706_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t1276659706_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (HttpWebRequest_t1951404513), -1, sizeof(HttpWebRequest_t1951404513_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1688[50] = 
{
	HttpWebRequest_t1951404513::get_offset_of_requestUri_6(),
	HttpWebRequest_t1951404513::get_offset_of_actualUri_7(),
	HttpWebRequest_t1951404513::get_offset_of_hostChanged_8(),
	HttpWebRequest_t1951404513::get_offset_of_allowAutoRedirect_9(),
	HttpWebRequest_t1951404513::get_offset_of_allowBuffering_10(),
	HttpWebRequest_t1951404513::get_offset_of_certificates_11(),
	HttpWebRequest_t1951404513::get_offset_of_connectionGroup_12(),
	HttpWebRequest_t1951404513::get_offset_of_contentLength_13(),
	HttpWebRequest_t1951404513::get_offset_of_continueDelegate_14(),
	HttpWebRequest_t1951404513::get_offset_of_cookieContainer_15(),
	HttpWebRequest_t1951404513::get_offset_of_credentials_16(),
	HttpWebRequest_t1951404513::get_offset_of_haveResponse_17(),
	HttpWebRequest_t1951404513::get_offset_of_haveRequest_18(),
	HttpWebRequest_t1951404513::get_offset_of_requestSent_19(),
	HttpWebRequest_t1951404513::get_offset_of_webHeaders_20(),
	HttpWebRequest_t1951404513::get_offset_of_keepAlive_21(),
	HttpWebRequest_t1951404513::get_offset_of_maxAutoRedirect_22(),
	HttpWebRequest_t1951404513::get_offset_of_mediaType_23(),
	HttpWebRequest_t1951404513::get_offset_of_method_24(),
	HttpWebRequest_t1951404513::get_offset_of_initialMethod_25(),
	HttpWebRequest_t1951404513::get_offset_of_pipelined_26(),
	HttpWebRequest_t1951404513::get_offset_of_preAuthenticate_27(),
	HttpWebRequest_t1951404513::get_offset_of_usedPreAuth_28(),
	HttpWebRequest_t1951404513::get_offset_of_version_29(),
	HttpWebRequest_t1951404513::get_offset_of_actualVersion_30(),
	HttpWebRequest_t1951404513::get_offset_of_proxy_31(),
	HttpWebRequest_t1951404513::get_offset_of_sendChunked_32(),
	HttpWebRequest_t1951404513::get_offset_of_servicePoint_33(),
	HttpWebRequest_t1951404513::get_offset_of_timeout_34(),
	HttpWebRequest_t1951404513::get_offset_of_writeStream_35(),
	HttpWebRequest_t1951404513::get_offset_of_webResponse_36(),
	HttpWebRequest_t1951404513::get_offset_of_asyncWrite_37(),
	HttpWebRequest_t1951404513::get_offset_of_asyncRead_38(),
	HttpWebRequest_t1951404513::get_offset_of_abortHandler_39(),
	HttpWebRequest_t1951404513::get_offset_of_aborted_40(),
	HttpWebRequest_t1951404513::get_offset_of_redirects_41(),
	HttpWebRequest_t1951404513::get_offset_of_expectContinue_42(),
	HttpWebRequest_t1951404513::get_offset_of_authCompleted_43(),
	HttpWebRequest_t1951404513::get_offset_of_bodyBuffer_44(),
	HttpWebRequest_t1951404513::get_offset_of_bodyBufferLength_45(),
	HttpWebRequest_t1951404513::get_offset_of_getResponseCalled_46(),
	HttpWebRequest_t1951404513::get_offset_of_saved_exc_47(),
	HttpWebRequest_t1951404513::get_offset_of_locker_48(),
	HttpWebRequest_t1951404513::get_offset_of_is_ntlm_auth_49(),
	HttpWebRequest_t1951404513::get_offset_of_finished_reading_50(),
	HttpWebRequest_t1951404513::get_offset_of_WebConnection_51(),
	HttpWebRequest_t1951404513::get_offset_of_auto_decomp_52(),
	HttpWebRequest_t1951404513_StaticFields::get_offset_of_defaultMaxResponseHeadersLength_53(),
	HttpWebRequest_t1951404513::get_offset_of_readWriteTimeout_54(),
	HttpWebRequest_t1951404513::get_offset_of_unsafe_auth_blah_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (HttpWebResponse_t2828383075), -1, sizeof(HttpWebResponse_t2828383075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1689[14] = 
{
	HttpWebResponse_t2828383075::get_offset_of_uri_1(),
	HttpWebResponse_t2828383075::get_offset_of_webHeaders_2(),
	HttpWebResponse_t2828383075::get_offset_of_cookieCollection_3(),
	HttpWebResponse_t2828383075::get_offset_of_method_4(),
	HttpWebResponse_t2828383075::get_offset_of_version_5(),
	HttpWebResponse_t2828383075::get_offset_of_statusCode_6(),
	HttpWebResponse_t2828383075::get_offset_of_statusDescription_7(),
	HttpWebResponse_t2828383075::get_offset_of_contentLength_8(),
	HttpWebResponse_t2828383075::get_offset_of_contentType_9(),
	HttpWebResponse_t2828383075::get_offset_of_cookie_container_10(),
	HttpWebResponse_t2828383075::get_offset_of_disposed_11(),
	HttpWebResponse_t2828383075::get_offset_of_stream_12(),
	HttpWebResponse_t2828383075::get_offset_of_cookieExpiresFormats_13(),
	HttpWebResponse_t2828383075_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (CookieParser_t1405985527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1690[3] = 
{
	CookieParser_t1405985527::get_offset_of_header_0(),
	CookieParser_t1405985527::get_offset_of_pos_1(),
	CookieParser_t1405985527::get_offset_of_length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (IPAddress_t1399971723), -1, sizeof(IPAddress_t1399971723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1696[11] = 
{
	IPAddress_t1399971723::get_offset_of_m_Address_0(),
	IPAddress_t1399971723::get_offset_of_m_Family_1(),
	IPAddress_t1399971723::get_offset_of_m_Numbers_2(),
	IPAddress_t1399971723::get_offset_of_m_ScopeId_3(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Any_4(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Broadcast_5(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Loopback_6(),
	IPAddress_t1399971723_StaticFields::get_offset_of_None_7(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6Any_8(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6Loopback_9(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6None_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (IPEndPoint_t2615413766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1697[2] = 
{
	IPEndPoint_t2615413766::get_offset_of_address_0(),
	IPEndPoint_t2615413766::get_offset_of_port_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (IPHostEntry_t994738509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1698[3] = 
{
	IPHostEntry_t994738509::get_offset_of_addressList_0(),
	IPHostEntry_t994738509::get_offset_of_aliases_1(),
	IPHostEntry_t994738509::get_offset_of_hostName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (IPv6Address_t2596635879), -1, sizeof(IPv6Address_t2596635879_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1699[5] = 
{
	IPv6Address_t2596635879::get_offset_of_address_0(),
	IPv6Address_t2596635879::get_offset_of_prefixLength_1(),
	IPv6Address_t2596635879::get_offset_of_scopeId_2(),
	IPv6Address_t2596635879_StaticFields::get_offset_of_Loopback_3(),
	IPv6Address_t2596635879_StaticFields::get_offset_of_Unspecified_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
