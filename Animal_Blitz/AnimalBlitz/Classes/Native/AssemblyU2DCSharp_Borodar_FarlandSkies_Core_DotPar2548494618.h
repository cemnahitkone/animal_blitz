﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t3614634134;
// Borodar.FarlandSkies.Core.DotParams.DotParamsList`1<System.Object>
struct DotParamsList_1_t2282605056;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1<System.Object>
struct  SortedParamsList_1_t2548494618  : public Il2CppObject
{
public:
	// T[] Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1::Params
	ObjectU5BU5D_t3614634134* ___Params_0;
	// Borodar.FarlandSkies.Core.DotParams.DotParamsList`1<T> Borodar.FarlandSkies.Core.DotParams.SortedParamsList`1::SortedParams
	DotParamsList_1_t2282605056 * ___SortedParams_1;

public:
	inline static int32_t get_offset_of_Params_0() { return static_cast<int32_t>(offsetof(SortedParamsList_1_t2548494618, ___Params_0)); }
	inline ObjectU5BU5D_t3614634134* get_Params_0() const { return ___Params_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of_Params_0() { return &___Params_0; }
	inline void set_Params_0(ObjectU5BU5D_t3614634134* value)
	{
		___Params_0 = value;
		Il2CppCodeGenWriteBarrier(&___Params_0, value);
	}

	inline static int32_t get_offset_of_SortedParams_1() { return static_cast<int32_t>(offsetof(SortedParamsList_1_t2548494618, ___SortedParams_1)); }
	inline DotParamsList_1_t2282605056 * get_SortedParams_1() const { return ___SortedParams_1; }
	inline DotParamsList_1_t2282605056 ** get_address_of_SortedParams_1() { return &___SortedParams_1; }
	inline void set_SortedParams_1(DotParamsList_1_t2282605056 * value)
	{
		___SortedParams_1 = value;
		Il2CppCodeGenWriteBarrier(&___SortedParams_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
