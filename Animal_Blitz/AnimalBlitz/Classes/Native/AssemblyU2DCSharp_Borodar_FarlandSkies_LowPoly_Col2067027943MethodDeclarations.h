﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.ColorButton
struct ColorButton_t2067027943;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void Borodar.FarlandSkies.LowPoly.ColorButton::.ctor()
extern "C"  void ColorButton__ctor_m3886452844 (ColorButton_t2067027943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.ColorButton::Start()
extern "C"  void ColorButton_Start_m1652781544 (ColorButton_t2067027943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.ColorButton::ChangeColor(UnityEngine.Color)
extern "C"  void ColorButton_ChangeColor_m4188600999 (ColorButton_t2067027943 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
