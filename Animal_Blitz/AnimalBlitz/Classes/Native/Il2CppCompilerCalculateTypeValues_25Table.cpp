﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility4019374597.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction1525323322.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent2111116400.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis375128448.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState1353336012.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial1630303189.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatE3157325053.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit1114673831.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1896830814.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup1030026315.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry1349564894.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping223789604.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexCli3349113845.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter3114550109.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As1166448724.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2574720772.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMod987318053.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1916789528.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit3220761768.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1325211874.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fi4030874534.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup1515633077.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1077473318.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1431825778.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons3558160636.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2875670365.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical1968298610.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "UnityScript_Lang_U3CModuleU3E3783534214.h"
#include "UnityScript_Lang_UnityScript_Lang_Array1396575355.h"
#include "UnityScript_Lang_UnityScript_Lang_Extensions2406697300.h"
#include "UnityScript_Lang_UnityScript_Lang_ListUpdateableEn2462779323.h"
#include "UnityScript_Lang_UnityScript_Lang_UnityRuntimeServ3303336867.h"
#include "UnityEngine_Analytics_U3CModuleU3E3783534214.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt2191537572.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt1068911718.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka1304606600.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka2256174789.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothDeviceScript810775987.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothLEHardwareI4023789856.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothLEHardwareI1474803024.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BluetoothLEHardwareI1741997128.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BtConnection3662742114.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi958778585.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H1615946202.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi896209287.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H2949110578.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H3709628099.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H4217245640.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H3718244552.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H1938462382.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi336099813.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_S1950236385.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1486305137.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa3894236545.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementat762068664.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_MouseLook2643707144.h"
#include "AssemblyU2DCSharp_MouseLook_RotationAxes2150291266.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_Demo_C2546408343.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_Demo_O2449136682.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_Demo_B3091645806.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_Demo_C2906165247.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_Demo_F2374938513.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_Demo_Pan24103620.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (SetPropertyUtility_t4019374597), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (Slider_t297367283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[15] = 
{
	Slider_t297367283::get_offset_of_m_FillRect_16(),
	Slider_t297367283::get_offset_of_m_HandleRect_17(),
	Slider_t297367283::get_offset_of_m_Direction_18(),
	Slider_t297367283::get_offset_of_m_MinValue_19(),
	Slider_t297367283::get_offset_of_m_MaxValue_20(),
	Slider_t297367283::get_offset_of_m_WholeNumbers_21(),
	Slider_t297367283::get_offset_of_m_Value_22(),
	Slider_t297367283::get_offset_of_m_OnValueChanged_23(),
	Slider_t297367283::get_offset_of_m_FillImage_24(),
	Slider_t297367283::get_offset_of_m_FillTransform_25(),
	Slider_t297367283::get_offset_of_m_FillContainerRect_26(),
	Slider_t297367283::get_offset_of_m_HandleTransform_27(),
	Slider_t297367283::get_offset_of_m_HandleContainerRect_28(),
	Slider_t297367283::get_offset_of_m_Offset_29(),
	Slider_t297367283::get_offset_of_m_Tracker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (Direction_t1525323322)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2502[5] = 
{
	Direction_t1525323322::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (SliderEvent_t2111116400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (Axis_t375128448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2504[3] = 
{
	Axis_t375128448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (SpriteState_t1353336012)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[3] = 
{
	SpriteState_t1353336012::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1353336012::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1353336012::get_offset_of_m_DisabledSprite_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (StencilMaterial_t1630303189), -1, sizeof(StencilMaterial_t1630303189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2506[1] = 
{
	StencilMaterial_t1630303189_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (MatEntry_t3157325053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2507[10] = 
{
	MatEntry_t3157325053::get_offset_of_baseMat_0(),
	MatEntry_t3157325053::get_offset_of_customMat_1(),
	MatEntry_t3157325053::get_offset_of_count_2(),
	MatEntry_t3157325053::get_offset_of_stencilId_3(),
	MatEntry_t3157325053::get_offset_of_operation_4(),
	MatEntry_t3157325053::get_offset_of_compareFunction_5(),
	MatEntry_t3157325053::get_offset_of_readMask_6(),
	MatEntry_t3157325053::get_offset_of_writeMask_7(),
	MatEntry_t3157325053::get_offset_of_useAlphaClip_8(),
	MatEntry_t3157325053::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (Text_t356221433), -1, sizeof(Text_t356221433_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2508[7] = 
{
	Text_t356221433::get_offset_of_m_FontData_28(),
	Text_t356221433::get_offset_of_m_Text_29(),
	Text_t356221433::get_offset_of_m_TextCache_30(),
	Text_t356221433::get_offset_of_m_TextCacheForLayout_31(),
	Text_t356221433_StaticFields::get_offset_of_s_DefaultText_32(),
	Text_t356221433::get_offset_of_m_DisableFontTextureRebuiltCallback_33(),
	Text_t356221433::get_offset_of_m_TempVerts_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (Toggle_t3976754468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[5] = 
{
	Toggle_t3976754468::get_offset_of_toggleTransition_16(),
	Toggle_t3976754468::get_offset_of_graphic_17(),
	Toggle_t3976754468::get_offset_of_m_Group_18(),
	Toggle_t3976754468::get_offset_of_onValueChanged_19(),
	Toggle_t3976754468::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (ToggleTransition_t1114673831)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2510[3] = 
{
	ToggleTransition_t1114673831::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (ToggleEvent_t1896830814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (ToggleGroup_t1030026315), -1, sizeof(ToggleGroup_t1030026315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2512[4] = 
{
	ToggleGroup_t1030026315::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t1030026315::get_offset_of_m_Toggles_3(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (ClipperRegistry_t1349564894), -1, sizeof(ClipperRegistry_t1349564894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2513[2] = 
{
	ClipperRegistry_t1349564894_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t1349564894::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (Clipping_t223789604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (RectangularVertexClipper_t3349113845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[2] = 
{
	RectangularVertexClipper_t3349113845::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t3349113845::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (AspectRatioFitter_t3114550109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2518[4] = 
{
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (AspectMode_t1166448724)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2519[6] = 
{
	AspectMode_t1166448724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (CanvasScaler_t2574720772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2520[14] = 
{
	CanvasScaler_t2574720772::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2574720772::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2574720772::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2574720772::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2574720772::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2574720772::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2574720772::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2574720772::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2574720772::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (ScaleMode_t987318053)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2521[4] = 
{
	ScaleMode_t987318053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (ScreenMatchMode_t1916789528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2522[4] = 
{
	ScreenMatchMode_t1916789528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (Unit_t3220761768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2523[6] = 
{
	Unit_t3220761768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (ContentSizeFitter_t1325211874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[4] = 
{
	ContentSizeFitter_t1325211874::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1325211874::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (FitMode_t4030874534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2525[4] = 
{
	FitMode_t4030874534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (GridLayoutGroup_t1515633077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2526[6] = 
{
	GridLayoutGroup_t1515633077::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t1515633077::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t1515633077::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t1515633077::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (Corner_t1077473318)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2527[5] = 
{
	Corner_t1077473318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (Axis_t1431825778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2528[3] = 
{
	Axis_t1431825778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (Constraint_t3558160636)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2529[4] = 
{
	Constraint_t3558160636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[5] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2538[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2539[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2540[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2541[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2547[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2552[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2554[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2560[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (U3CModuleU3E_t3783534225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (Array_t1396575355), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (Extensions_t2406697300), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (ListUpdateableEnumerator_t2462779323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[2] = 
{
	ListUpdateableEnumerator_t2462779323::get_offset_of__list_0(),
	ListUpdateableEnumerator_t2462779323::get_offset_of__current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (UnityRuntimeServices_t3303336867), -1, sizeof(UnityRuntimeServices_t3303336867_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2566[3] = 
{
	UnityRuntimeServices_t3303336867_StaticFields::get_offset_of_EmptyEnumerator_0(),
	UnityRuntimeServices_t3303336867_StaticFields::get_offset_of_EnumeratorType_1(),
	UnityRuntimeServices_t3303336867_StaticFields::get_offset_of_Initialized_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (U3CModuleU3E_t3783534226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (AnalyticsTracker_t2191537572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2568[5] = 
{
	AnalyticsTracker_t2191537572::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t2191537572::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t2191537572::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (Trigger_t1068911718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2569[8] = 
{
	Trigger_t1068911718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (TrackableProperty_t1304606600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[2] = 
{
	0,
	TrackableProperty_t1304606600::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (FieldWithTarget_t2256174789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2571[6] = 
{
	FieldWithTarget_t2256174789::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t2256174789::get_offset_of_m_Target_1(),
	FieldWithTarget_t2256174789::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t2256174789::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t2256174789::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t2256174789::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (U3CModuleU3E_t3783534227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (BluetoothDeviceScript_t810775987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2573[38] = 
{
	BluetoothDeviceScript_t810775987::get_offset_of_DiscoveredDeviceList_2(),
	BluetoothDeviceScript_t810775987::get_offset_of_InitializedAction_3(),
	BluetoothDeviceScript_t810775987::get_offset_of_DeinitializedAction_4(),
	BluetoothDeviceScript_t810775987::get_offset_of_ErrorAction_5(),
	BluetoothDeviceScript_t810775987::get_offset_of_ServiceAddedAction_6(),
	BluetoothDeviceScript_t810775987::get_offset_of_StartedAdvertisingAction_7(),
	BluetoothDeviceScript_t810775987::get_offset_of_StoppedAdvertisingAction_8(),
	BluetoothDeviceScript_t810775987::get_offset_of_DiscoveredPeripheralAction_9(),
	BluetoothDeviceScript_t810775987::get_offset_of_DiscoveredPeripheralWithAdvertisingInfoAction_10(),
	BluetoothDeviceScript_t810775987::get_offset_of_RetrievedConnectedPeripheralAction_11(),
	BluetoothDeviceScript_t810775987::get_offset_of_PeripheralReceivedWriteDataAction_12(),
	BluetoothDeviceScript_t810775987::get_offset_of_ConnectedPeripheralAction_13(),
	BluetoothDeviceScript_t810775987::get_offset_of_ConnectedDisconnectPeripheralAction_14(),
	BluetoothDeviceScript_t810775987::get_offset_of_DisconnectedPeripheralAction_15(),
	BluetoothDeviceScript_t810775987::get_offset_of_DiscoveredServiceAction_16(),
	BluetoothDeviceScript_t810775987::get_offset_of_DiscoveredCharacteristicAction_17(),
	BluetoothDeviceScript_t810775987::get_offset_of_DidWriteCharacteristicAction_18(),
	BluetoothDeviceScript_t810775987::get_offset_of_DidUpdateNotificationStateForCharacteristicAction_19(),
	BluetoothDeviceScript_t810775987::get_offset_of_DidUpdateNotificationStateForCharacteristicWithDeviceAddressAction_20(),
	BluetoothDeviceScript_t810775987::get_offset_of_DidUpdateCharacteristicValueAction_21(),
	BluetoothDeviceScript_t810775987::get_offset_of_DidUpdateCharacteristicValueWithDeviceAddressAction_22(),
	BluetoothDeviceScript_t810775987::get_offset_of_Initialized_23(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (BluetoothLEHardwareInterface_t4023789856), -1, sizeof(BluetoothLEHardwareInterface_t4023789856_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2574[1] = 
{
	BluetoothLEHardwareInterface_t4023789856_StaticFields::get_offset_of_bluetoothDeviceScript_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (CBCharacteristicProperties_t1474803024)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2575[11] = 
{
	CBCharacteristicProperties_t1474803024::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (CBAttributePermissions_t1741997128)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2576[5] = 
{
	CBAttributePermissions_t1741997128::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (BtConnection_t3662742114), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (Highlighter_t958778585), -1, sizeof(Highlighter_t958778585_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2578[33] = 
{
	Highlighter_t958778585_StaticFields::get_offset_of_types_2(),
	0,
	Highlighter_t958778585::get_offset_of_occluderColor_4(),
	0,
	0,
	Highlighter_t958778585_StaticFields::get_offset_of_renderingOrder_7(),
	Highlighter_t958778585_StaticFields::get_offset_of_highlighters_8(),
	Highlighter_t958778585_StaticFields::get_offset_of_zWrite_9(),
	Highlighter_t958778585_StaticFields::get_offset_of_offsetFactor_10(),
	Highlighter_t958778585_StaticFields::get_offset_of_offsetUnits_11(),
	Highlighter_t958778585::get_offset_of_seeThrough_12(),
	Highlighter_t958778585::get_offset_of_occluder_13(),
	Highlighter_t958778585::get_offset_of_tr_14(),
	Highlighter_t958778585::get_offset_of_highlightableRenderers_15(),
	Highlighter_t958778585::get_offset_of_renderersDirty_16(),
	Highlighter_t958778585_StaticFields::get_offset_of_sComponents_17(),
	Highlighter_t958778585::get_offset_of_mode_18(),
	Highlighter_t958778585::get_offset_of_zTest_19(),
	Highlighter_t958778585::get_offset_of_stencilRef_20(),
	Highlighter_t958778585::get_offset_of__once_21(),
	Highlighter_t958778585::get_offset_of_flashing_22(),
	Highlighter_t958778585::get_offset_of_currentColor_23(),
	Highlighter_t958778585::get_offset_of_transitionValue_24(),
	Highlighter_t958778585::get_offset_of_transitionTarget_25(),
	Highlighter_t958778585::get_offset_of_transitionTime_26(),
	Highlighter_t958778585::get_offset_of_onceColor_27(),
	Highlighter_t958778585::get_offset_of_flashingFreq_28(),
	Highlighter_t958778585::get_offset_of_flashingColorMin_29(),
	Highlighter_t958778585::get_offset_of_flashingColorMax_30(),
	Highlighter_t958778585::get_offset_of_constantColor_31(),
	Highlighter_t958778585_StaticFields::get_offset_of__opaqueShader_32(),
	Highlighter_t958778585_StaticFields::get_offset_of__transparentShader_33(),
	Highlighter_t958778585::get_offset_of__opaqueMaterial_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (Mode_t1615946202)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2579[6] = 
{
	Mode_t1615946202::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (HighlighterBlocker_t896209287), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (HighlightingBlitter_t2949110578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[1] = 
{
	HighlightingBlitter_t2949110578::get_offset_of_renderers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (HighlightingRenderer_t3709628099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (HighlighterRenderer_t4217245640), -1, sizeof(HighlighterRenderer_t4217245640_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2583[13] = 
{
	HighlighterRenderer_t4217245640_StaticFields::get_offset_of_transparentCutoff_2(),
	0,
	0,
	HighlighterRenderer_t4217245640_StaticFields::get_offset_of_sRenderType_5(),
	HighlighterRenderer_t4217245640_StaticFields::get_offset_of_sOpaque_6(),
	HighlighterRenderer_t4217245640_StaticFields::get_offset_of_sTransparent_7(),
	HighlighterRenderer_t4217245640_StaticFields::get_offset_of_sTransparentCutout_8(),
	HighlighterRenderer_t4217245640_StaticFields::get_offset_of_sMainTex_9(),
	HighlighterRenderer_t4217245640::get_offset_of_r_10(),
	HighlighterRenderer_t4217245640::get_offset_of_data_11(),
	HighlighterRenderer_t4217245640::get_offset_of_lastCamera_12(),
	HighlighterRenderer_t4217245640::get_offset_of_isAlive_13(),
	HighlighterRenderer_t4217245640::get_offset_of_endOfFrame_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (Data_t3718244552)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[3] = 
{
	Data_t3718244552::get_offset_of_material_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Data_t3718244552::get_offset_of_submeshIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Data_t3718244552::get_offset_of_transparent_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (U3CEndOfFrameU3Ec__Iterator0_t1938462382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[4] = 
{
	U3CEndOfFrameU3Ec__Iterator0_t1938462382::get_offset_of_U24this_0(),
	U3CEndOfFrameU3Ec__Iterator0_t1938462382::get_offset_of_U24current_1(),
	U3CEndOfFrameU3Ec__Iterator0_t1938462382::get_offset_of_U24disposing_2(),
	U3CEndOfFrameU3Ec__Iterator0_t1938462382::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (HighlightingBase_t336099813), -1, sizeof(HighlightingBase_t336099813_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2586[34] = 
{
	HighlightingBase_t336099813_StaticFields::get_offset_of_colorClear_2(),
	HighlightingBase_t336099813_StaticFields::get_offset_of_renderBufferName_3(),
	HighlightingBase_t336099813_StaticFields::get_offset_of_identityMatrix_4(),
	0,
	HighlightingBase_t336099813_StaticFields::get_offset_of_cameraTargetID_6(),
	HighlightingBase_t336099813_StaticFields::get_offset_of_quad_7(),
	HighlightingBase_t336099813_StaticFields::get_offset_of_device_8(),
	HighlightingBase_t336099813::get_offset_of_offsetFactor_9(),
	HighlightingBase_t336099813::get_offset_of_offsetUnits_10(),
	HighlightingBase_t336099813::get_offset_of_renderBuffer_11(),
	HighlightingBase_t336099813::get_offset_of_cachedWidth_12(),
	HighlightingBase_t336099813::get_offset_of_cachedHeight_13(),
	HighlightingBase_t336099813::get_offset_of_cachedAA_14(),
	HighlightingBase_t336099813::get_offset_of__downsampleFactor_15(),
	HighlightingBase_t336099813::get_offset_of__iterations_16(),
	HighlightingBase_t336099813::get_offset_of__blurMinSpread_17(),
	HighlightingBase_t336099813::get_offset_of__blurSpread_18(),
	HighlightingBase_t336099813::get_offset_of__blurIntensity_19(),
	HighlightingBase_t336099813::get_offset_of__blitter_20(),
	HighlightingBase_t336099813::get_offset_of_highlightingBufferID_21(),
	HighlightingBase_t336099813::get_offset_of_highlightingBuffer_22(),
	HighlightingBase_t336099813::get_offset_of_cam_23(),
	HighlightingBase_t336099813::get_offset_of_isDepthAvailable_24(),
	0,
	0,
	0,
	HighlightingBase_t336099813_StaticFields::get_offset_of_shaderPaths_28(),
	HighlightingBase_t336099813_StaticFields::get_offset_of_shaders_29(),
	HighlightingBase_t336099813_StaticFields::get_offset_of_materials_30(),
	HighlightingBase_t336099813::get_offset_of_blurMaterial_31(),
	HighlightingBase_t336099813::get_offset_of_cutMaterial_32(),
	HighlightingBase_t336099813::get_offset_of_compMaterial_33(),
	HighlightingBase_t336099813_StaticFields::get_offset_of_initialized_34(),
	HighlightingBase_t336099813_StaticFields::get_offset_of_cameras_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (ShaderPropertyID_t1950236385), -1, sizeof(ShaderPropertyID_t1950236385_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2587[17] = 
{
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_MainTexU3Ek__BackingField_0(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_ColorU3Ek__BackingField_1(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_CutoffU3Ek__BackingField_2(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_IntensityU3Ek__BackingField_3(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_ZTestU3Ek__BackingField_4(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_StencilRefU3Ek__BackingField_5(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_CullU3Ek__BackingField_6(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_HighlightingBlur1U3Ek__BackingField_7(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_HighlightingBlur2U3Ek__BackingField_8(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_HighlightingBufferU3Ek__BackingField_9(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_HighlightingBufferTexelSizeU3Ek__BackingField_10(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_HighlightingBlurredU3Ek__BackingField_11(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_HighlightingBlurOffsetU3Ek__BackingField_12(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_HighlightingZWriteU3Ek__BackingField_13(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_HighlightingOffsetFactorU3Ek__BackingField_14(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_U3C_HighlightingOffsetUnitsU3Ek__BackingField_15(),
	ShaderPropertyID_t1950236385_StaticFields::get_offset_of_initialized_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2588[2] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D1456763F890A84558F99AFA687C36B9037697848_0(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (U24ArrayTypeU3D24_t762068664)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D24_t762068664 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (U3CModuleU3E_t3783534228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (MouseLook_t2643707144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2592[8] = 
{
	MouseLook_t2643707144::get_offset_of_axes_2(),
	MouseLook_t2643707144::get_offset_of_sensitivityX_3(),
	MouseLook_t2643707144::get_offset_of_sensitivityY_4(),
	MouseLook_t2643707144::get_offset_of_minimumX_5(),
	MouseLook_t2643707144::get_offset_of_maximumX_6(),
	MouseLook_t2643707144::get_offset_of_minimumY_7(),
	MouseLook_t2643707144::get_offset_of_maximumY_8(),
	MouseLook_t2643707144::get_offset_of_rotationY_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (RotationAxes_t2150291266)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2593[4] = 
{
	RotationAxes_t2150291266::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (CameraOrbitController_t2546408343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2594[8] = 
{
	CameraOrbitController_t2546408343::get_offset_of_Target_2(),
	CameraOrbitController_t2546408343::get_offset_of_Distance_3(),
	CameraOrbitController_t2546408343::get_offset_of_DistanceMin_4(),
	CameraOrbitController_t2546408343::get_offset_of_DistanceMax_5(),
	CameraOrbitController_t2546408343::get_offset_of_Speed_6(),
	CameraOrbitController_t2546408343::get_offset_of_VerticalRotationLimit_7(),
	CameraOrbitController_t2546408343::get_offset_of__angles_8(),
	CameraOrbitController_t2546408343::get_offset_of__isPointerOverGui_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (ObjectRotator_t2449136682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2595[1] = 
{
	ObjectRotator_t2449136682::get_offset_of_EulerAngles_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (BaseColorButton_t3091645806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2596[2] = 
{
	BaseColorButton_t3091645806::get_offset_of_ColorPicker_2(),
	BaseColorButton_t3091645806::get_offset_of_ColorImage_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (ColorPicker_t2906165247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2597[3] = 
{
	ColorPicker_t2906165247::get_offset_of__rectTransform_2(),
	ColorPicker_t2906165247::get_offset_of__image_3(),
	ColorPicker_t2906165247::get_offset_of_U3CColorButtonU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (ForegroundToggle_t2374938513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2598[2] = 
{
	ForegroundToggle_t2374938513::get_offset_of_GameObjects_2(),
	ForegroundToggle_t2374938513::get_offset_of_Renderers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (PanelsDropdown_t24103620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[2] = 
{
	PanelsDropdown_t24103620::get_offset_of_Panels_2(),
	PanelsDropdown_t24103620::get_offset_of__dropdown_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
