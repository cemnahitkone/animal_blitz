﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RunForwardandBack
struct  RunForwardandBack_t3788415244  : public MonoBehaviour_t1158329972
{
public:
	// System.Single RunForwardandBack::speed
	float ___speed_2;
	// UnityEngine.Vector3 RunForwardandBack::right
	Vector3_t2243707580  ___right_3;
	// UnityEngine.Vector3 RunForwardandBack::left
	Vector3_t2243707580  ___left_4;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(RunForwardandBack_t3788415244, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_right_3() { return static_cast<int32_t>(offsetof(RunForwardandBack_t3788415244, ___right_3)); }
	inline Vector3_t2243707580  get_right_3() const { return ___right_3; }
	inline Vector3_t2243707580 * get_address_of_right_3() { return &___right_3; }
	inline void set_right_3(Vector3_t2243707580  value)
	{
		___right_3 = value;
	}

	inline static int32_t get_offset_of_left_4() { return static_cast<int32_t>(offsetof(RunForwardandBack_t3788415244, ___left_4)); }
	inline Vector3_t2243707580  get_left_4() const { return ___left_4; }
	inline Vector3_t2243707580 * get_address_of_left_4() { return &___left_4; }
	inline void set_left_4(Vector3_t2243707580  value)
	{
		___left_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
