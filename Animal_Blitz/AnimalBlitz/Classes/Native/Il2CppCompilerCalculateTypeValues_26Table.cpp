﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_DotPar3767082706.h"
#include "AssemblyU2DCSharp_FPSWalkerEnhanced1211262667.h"
#include "AssemblyU2DCSharp_SmoothMouseLook128928814.h"
#include "AssemblyU2DCSharp_SmoothMouseLook_RotationAxes1370738632.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Clou188019486.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Col2067027943.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Col3781445085.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Pau3683727040.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Res2222487199.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Res3484749624.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Pro1681642541.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Pro3212345188.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Pro1520026206.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Pro1741473933.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Tim3354239974.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Tex2933815391.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Tex2676527179.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Tim2647012832.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Sky2759006432.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Skybo61875227.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Sky2663460034.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot4052651973.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot3739272824.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot3937665775.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot4198706186.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot2066856486.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot1961981937.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot4272832432.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot3054173379.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_DotP907828490.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot1887883603.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_Dot3023881265.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_LowPoly_DotP120226188.h"
#include "AssemblyU2DCSharp_Ricimi_AnimatedButton4037967263.h"
#include "AssemblyU2DCSharp_Ricimi_AnimatedButton_ButtonClic1021215292.h"
#include "AssemblyU2DCSharp_Ricimi_BackgroundMusic2182474183.h"
#include "AssemblyU2DCSharp_Ricimi_BackgroundMusic_Fade3857387351.h"
#include "AssemblyU2DCSharp_Ricimi_BackgroundMusic_U3CFadeAu1632642824.h"
#include "AssemblyU2DCSharp_Ricimi_ColorSwapper1578650717.h"
#include "AssemblyU2DCSharp_Ricimi_Fader3816812854.h"
#include "AssemblyU2DCSharp_Ricimi_Fader_U3CRunFadeInU3Ec__It794606817.h"
#include "AssemblyU2DCSharp_Ricimi_Fader_U3CRunFadeOutU3Ec__I226132861.h"
#include "AssemblyU2DCSharp_Ricimi_InitialPlayerPrefs757210519.h"
#include "AssemblyU2DCSharp_Ricimi_LevelScene288242702.h"
#include "AssemblyU2DCSharp_Ricimi_MusicButton3792610623.h"
#include "AssemblyU2DCSharp_Ricimi_MusicManager1668724344.h"
#include "AssemblyU2DCSharp_Ricimi_MuteBackgroundMusic1333099764.h"
#include "AssemblyU2DCSharp_Ricimi_PlayPopup258579664.h"
#include "AssemblyU2DCSharp_Ricimi_PlayPopupOpener1253928371.h"
#include "AssemblyU2DCSharp_Ricimi_Popup1187979376.h"
#include "AssemblyU2DCSharp_Ricimi_Popup_U3CRunPopupDestroyU3179771350.h"
#include "AssemblyU2DCSharp_Ricimi_PopupOpener3649718601.h"
#include "AssemblyU2DCSharp_Ricimi_SceneTransition2696223135.h"
#include "AssemblyU2DCSharp_Ricimi_SoundButton2589941643.h"
#include "AssemblyU2DCSharp_Ricimi_SoundManager1319472430.h"
#include "AssemblyU2DCSharp_Ricimi_SpinWheel1385180419.h"
#include "AssemblyU2DCSharp_Ricimi_SpinWheel_U3CDoSpinU3Ec__I288164173.h"
#include "AssemblyU2DCSharp_Ricimi_SpriteSwapper3880336557.h"
#include "AssemblyU2DCSharp_Ricimi_TintedButton315702378.h"
#include "AssemblyU2DCSharp_Ricimi_TintedButton_ButtonClicke2671903891.h"
#include "AssemblyU2DCSharp_Ricimi_TintedButton_U3CDarkenCol1184020839.h"
#include "AssemblyU2DCSharp_Ricimi_TintedButton_U3CBrightenC3614062068.h"
#include "AssemblyU2DCSharp_Ricimi_Transition2490031921.h"
#include "AssemblyU2DCSharp_Ricimi_Transition_U3CRunFadeU3Ec4246392297.h"
#include "AssemblyU2DCSharp_CFX2_Demo1946811213.h"
#include "AssemblyU2DCSharp_CFX2_Demo_U3CRandomSpawnsCorouti4111098856.h"
#include "AssemblyU2DCSharp_CFX_AutoStopLoopedEffect3214280257.h"
#include "AssemblyU2DCSharp_CFX_Demo1465385775.h"
#include "AssemblyU2DCSharp_CFX_Demo_U3CRandomSpawnsCoroutin3999667400.h"
#include "AssemblyU2DCSharp_CFX_Demo_New3744422722.h"
#include "AssemblyU2DCSharp_CFX_Demo_New_U3CCheckForDeletedP2558571495.h"
#include "AssemblyU2DCSharp_CFX_Demo_RandomDir1076153330.h"
#include "AssemblyU2DCSharp_CFX_Demo_RandomDirectionTranslate747977598.h"
#include "AssemblyU2DCSharp_CFX_Demo_RotateCamera2665602760.h"
#include "AssemblyU2DCSharp_CFX_Demo_Translate3218432890.h"
#include "AssemblyU2DCSharp_CFX_Demo_GTButton113862661.h"
#include "AssemblyU2DCSharp_CFX_Demo_GTToggle2599739681.h"
#include "AssemblyU2DCSharp_CFX2_AutoRotate42166984.h"
#include "AssemblyU2DCSharp_CFX_AutoDestructShuriken1500114366.h"
#include "AssemblyU2DCSharp_CFX_AutoDestructShuriken_U3CChec3107901010.h"
#include "AssemblyU2DCSharp_CFX_AutoRotate1831446564.h"
#include "AssemblyU2DCSharp_CFX_AutodestructWhenNoChildren3972162727.h"
#include "AssemblyU2DCSharp_CFX_InspectorHelp3280468206.h"
#include "AssemblyU2DCSharp_CFX_LightIntensityFade4221734619.h"
#include "AssemblyU2DCSharp_CFX_ShurikenThreadFix3286853382.h"
#include "AssemblyU2DCSharp_CFX_ShurikenThreadFix_U3CWaitFram410180078.h"
#include "AssemblyU2DCSharp_CFX_SpawnSystem3600628354.h"
#include "AssemblyU2DCSharp_CharacterControl3164190352.h"
#include "AssemblyU2DCSharp_DrawCircle4100096050.h"
#include "AssemblyU2DCSharp_IconControl564227302.h"
#include "AssemblyU2DCSharp_LookAt2293208666.h"
#include "AssemblyU2DCSharp_RunForwardandBack3788415244.h"
#include "AssemblyU2DCSharp_SpaceDetection1772284253.h"
#include "AssemblyU2DCSharp_TabBase319583306.h"
#include "AssemblyU2DCSharp_TabBase_baseLegID3972271149.h"
#include "AssemblyU2DCSharp_TabBase_tabID570832297.h"
#include "AssemblyU2DCSharp_tBase1720960579.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (DotParam_t3767082706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2600[1] = 
{
	DotParam_t3767082706::get_offset_of_Time_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2602[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2603[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (FPSWalkerEnhanced_t1211262667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2604[26] = 
{
	FPSWalkerEnhanced_t1211262667::get_offset_of_walkSpeed_2(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_runSpeed_3(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_limitDiagonalSpeed_4(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_toggleRun_5(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_jumpSpeed_6(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_gravity_7(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_fallingDamageThreshold_8(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_slideWhenOverSlopeLimit_9(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_slideOnTaggedObjects_10(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_slideSpeed_11(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_airControl_12(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_antiBumpFactor_13(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_antiBunnyHopFactor_14(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_moveDirection_15(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_grounded_16(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_controller_17(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_myTransform_18(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_speed_19(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_hit_20(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_fallStartLevel_21(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_falling_22(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_slideLimit_23(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_rayDistance_24(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_contactPoint_25(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_playerControl_26(),
	FPSWalkerEnhanced_t1211262667::get_offset_of_jumpTimer_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (SmoothMouseLook_t128928814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2605[16] = 
{
	SmoothMouseLook_t128928814::get_offset_of_axes_2(),
	SmoothMouseLook_t128928814::get_offset_of_SensitivityX_3(),
	SmoothMouseLook_t128928814::get_offset_of_SensitivityY_4(),
	SmoothMouseLook_t128928814::get_offset_of_MinimumX_5(),
	SmoothMouseLook_t128928814::get_offset_of_MaximumX_6(),
	SmoothMouseLook_t128928814::get_offset_of_MinimumY_7(),
	SmoothMouseLook_t128928814::get_offset_of_MaximumY_8(),
	SmoothMouseLook_t128928814::get_offset_of_FrameCounter_9(),
	SmoothMouseLook_t128928814::get_offset_of__rotationX_10(),
	SmoothMouseLook_t128928814::get_offset_of__rotationY_11(),
	SmoothMouseLook_t128928814::get_offset_of__rotArrayX_12(),
	SmoothMouseLook_t128928814::get_offset_of__rotArrayY_13(),
	SmoothMouseLook_t128928814::get_offset_of__rotAverageX_14(),
	SmoothMouseLook_t128928814::get_offset_of__rotAverageY_15(),
	SmoothMouseLook_t128928814::get_offset_of__originalRotation_16(),
	SmoothMouseLook_t128928814::get_offset_of__parentOriginalRotation_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (RotationAxes_t1370738632)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2606[4] = 
{
	RotationAxes_t1370738632::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (CloudsRotationButton_t188019486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2607[2] = 
{
	CloudsRotationButton_t188019486::get_offset_of_RotationPerSecond_2(),
	CloudsRotationButton_t188019486::get_offset_of__rotate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (ColorButton_t2067027943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2608[1] = 
{
	ColorButton_t2067027943::get_offset_of_SkyColorType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (ColorType_t3781445085)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2609[8] = 
{
	ColorType_t3781445085::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (PausetButton_t3683727040), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (ResetButton_t2222487199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2611[23] = 
{
	ResetButton_t2222487199::get_offset_of_TopColorImage_2(),
	ResetButton_t2222487199::get_offset_of_MiddleColorImage_3(),
	ResetButton_t2222487199::get_offset_of_BottomColorImage_4(),
	ResetButton_t2222487199::get_offset_of_TopExponentSlider_5(),
	ResetButton_t2222487199::get_offset_of_BottomExponentSlider_6(),
	ResetButton_t2222487199::get_offset_of_StarsTintImage_7(),
	ResetButton_t2222487199::get_offset_of_ExtinctionSlider_8(),
	ResetButton_t2222487199::get_offset_of_TwinklingSpeedSlider_9(),
	ResetButton_t2222487199::get_offset_of_SunTintImage_10(),
	ResetButton_t2222487199::get_offset_of_SunAlphaSlider_11(),
	ResetButton_t2222487199::get_offset_of_SunSizeSlider_12(),
	ResetButton_t2222487199::get_offset_of_SunFlareToggle_13(),
	ResetButton_t2222487199::get_offset_of_SunFlareBrightnessSlider_14(),
	ResetButton_t2222487199::get_offset_of_MoonTintImage_15(),
	ResetButton_t2222487199::get_offset_of_MoonAlphaSlider_16(),
	ResetButton_t2222487199::get_offset_of_MoonSizeSlider_17(),
	ResetButton_t2222487199::get_offset_of_MoonFlareToggle_18(),
	ResetButton_t2222487199::get_offset_of_MoonFlareBrightnessSlider_19(),
	ResetButton_t2222487199::get_offset_of_CloudsTintImage_20(),
	ResetButton_t2222487199::get_offset_of_CloudsRotationSlider_21(),
	ResetButton_t2222487199::get_offset_of_CloudsHeightSlider_22(),
	ResetButton_t2222487199::get_offset_of_ExoposureSlider_23(),
	ResetButton_t2222487199::get_offset_of_AdjustFogToggle_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (DefaultValue_t3484749624), -1, sizeof(DefaultValue_t3484749624_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2612[21] = 
{
	DefaultValue_t3484749624_StaticFields::get_offset_of_TopColor_0(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_MiddleColor_1(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_BottomColor_2(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_TopExponent_3(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_BottomExponent_4(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_StarsTint_5(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_StarsExtinction_6(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_StarsTwinklingSpeed_7(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_SunTint_8(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_SunSize_9(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_SunFlare_10(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_SunFlareBrightness_11(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_MoonTint_12(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_MoonSize_13(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_MoonFlare_14(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_MoonFlareBrightness_15(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_CloudsTint_16(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_CloudsRotation_17(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_CloudsHeight_18(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_Exposure_19(),
	DefaultValue_t3484749624_StaticFields::get_offset_of_AdjustFog_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (PropertyToggle_t1681642541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2613[2] = 
{
	PropertyToggle_t1681642541::get_offset_of_ToggleType_2(),
	PropertyToggle_t1681642541::get_offset_of__toggle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (Type_t3212345188)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2614[4] = 
{
	Type_t3212345188::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (PropertySlider_t1520026206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2615[2] = 
{
	PropertySlider_t1520026206::get_offset_of_SliderType_2(),
	PropertySlider_t1520026206::get_offset_of__slider_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (Type_t1741473933)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2616[14] = 
{
	Type_t1741473933::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (TimeSlider_t3354239974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2617[2] = 
{
	TimeSlider_t3354239974::get_offset_of__slider_2(),
	TimeSlider_t3354239974::get_offset_of__cycleManager_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (TexturesDropdown_t2933815391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2618[3] = 
{
	TexturesDropdown_t2933815391::get_offset_of_Type_2(),
	TexturesDropdown_t2933815391::get_offset_of_Textures_3(),
	TexturesDropdown_t2933815391::get_offset_of__dropdown_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (TextureType_t2676527179)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2619[5] = 
{
	TextureType_t2676527179::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (TimeText_t2647012832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2620[1] = 
{
	TimeText_t2647012832::get_offset_of__text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (SkyboxController_t2759006432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2621[34] = 
{
	SkyboxController_t2759006432::get_offset_of_SkyboxMaterial_3(),
	SkyboxController_t2759006432::get_offset_of__topColor_4(),
	SkyboxController_t2759006432::get_offset_of__middleColor_5(),
	SkyboxController_t2759006432::get_offset_of__bottomColor_6(),
	SkyboxController_t2759006432::get_offset_of__topExponent_7(),
	SkyboxController_t2759006432::get_offset_of__bottomExponent_8(),
	SkyboxController_t2759006432::get_offset_of__starsEnabled_9(),
	SkyboxController_t2759006432::get_offset_of__starsCubemap_10(),
	SkyboxController_t2759006432::get_offset_of__starsTint_11(),
	SkyboxController_t2759006432::get_offset_of__starsExtinction_12(),
	SkyboxController_t2759006432::get_offset_of__starsTwinklingSpeed_13(),
	SkyboxController_t2759006432::get_offset_of__sunEnabled_14(),
	SkyboxController_t2759006432::get_offset_of__sunTexture_15(),
	SkyboxController_t2759006432::get_offset_of__sunLight_16(),
	SkyboxController_t2759006432::get_offset_of__sunTint_17(),
	SkyboxController_t2759006432::get_offset_of__sunSize_18(),
	SkyboxController_t2759006432::get_offset_of__sunFlare_19(),
	SkyboxController_t2759006432::get_offset_of__sunFlareBrightness_20(),
	SkyboxController_t2759006432::get_offset_of__moonEnabled_21(),
	SkyboxController_t2759006432::get_offset_of__moonTexture_22(),
	SkyboxController_t2759006432::get_offset_of__moonLight_23(),
	SkyboxController_t2759006432::get_offset_of__moonTint_24(),
	SkyboxController_t2759006432::get_offset_of__moonSize_25(),
	SkyboxController_t2759006432::get_offset_of__moonFlare_26(),
	SkyboxController_t2759006432::get_offset_of__moonFlareBrightness_27(),
	SkyboxController_t2759006432::get_offset_of__cloudsEnabled_28(),
	SkyboxController_t2759006432::get_offset_of__cloudsCubemap_29(),
	SkyboxController_t2759006432::get_offset_of__cloudsTint_30(),
	SkyboxController_t2759006432::get_offset_of__cloudsHeight_31(),
	SkyboxController_t2759006432::get_offset_of__cloudsRotation_32(),
	SkyboxController_t2759006432::get_offset_of__exposure_33(),
	SkyboxController_t2759006432::get_offset_of__adjustFogColor_34(),
	SkyboxController_t2759006432::get_offset_of__sunFlareComponent_35(),
	SkyboxController_t2759006432::get_offset_of__moonFlareComponent_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (SkyboxCycleManager_t61875227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2622[4] = 
{
	SkyboxCycleManager_t61875227::get_offset_of_CycleDuration_3(),
	SkyboxCycleManager_t61875227::get_offset_of_CycleProgress_4(),
	SkyboxCycleManager_t61875227::get_offset_of_Paused_5(),
	SkyboxCycleManager_t61875227::get_offset_of__dayNightCycle_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (SkyboxDayNightCycle_t2663460034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2623[26] = 
{
	SkyboxDayNightCycle_t2663460034::get_offset_of__skyParamsList_3(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__starsParamsList_4(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__sunrise_5(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__sunset_6(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__sunAltitude_7(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__sunLongitude_8(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__sunOrbit_9(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__sunParamsList_10(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__moonrise_11(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__moonset_12(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__moonAltitude_13(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__moonLongitude_14(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__moonOrbit_15(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__moonParamsList_16(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__cloudsParamsList_17(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__skyboxController_18(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__sunDuration_19(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__sunAttitudeVector_20(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__moonDuration_21(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__moonAttitudeVector_22(),
	SkyboxDayNightCycle_t2663460034::get_offset_of__timeOfDay_23(),
	SkyboxDayNightCycle_t2663460034::get_offset_of_U3CCurrentSkyParamU3Ek__BackingField_24(),
	SkyboxDayNightCycle_t2663460034::get_offset_of_U3CCurrentStarsParamU3Ek__BackingField_25(),
	SkyboxDayNightCycle_t2663460034::get_offset_of_U3CCurrentSunParamU3Ek__BackingField_26(),
	SkyboxDayNightCycle_t2663460034::get_offset_of_U3CCurrentMoonParamU3Ek__BackingField_27(),
	SkyboxDayNightCycle_t2663460034::get_offset_of_U3CCurrentCloudsParamU3Ek__BackingField_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (CelestialParam_t4052651973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[3] = 
{
	CelestialParam_t4052651973::get_offset_of_TintColor_1(),
	CelestialParam_t4052651973::get_offset_of_LightColor_2(),
	CelestialParam_t4052651973::get_offset_of_LightIntencity_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (CelestialParamsList_t3739272824), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (CloudsParam_t3937665775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2626[1] = 
{
	CloudsParam_t3937665775::get_offset_of_TintColor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (CloudsParamsList_t4198706186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (MoonParam_t2066856486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2628[4] = 
{
	MoonParam_t2066856486::get_offset_of_Time_0(),
	MoonParam_t2066856486::get_offset_of_TintColor_1(),
	MoonParam_t2066856486::get_offset_of_LightColor_2(),
	MoonParam_t2066856486::get_offset_of_LightIntencity_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (MoonParamsList_t1961981937), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (SkyParam_t4272832432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2630[4] = 
{
	SkyParam_t4272832432::get_offset_of_TopColor_1(),
	SkyParam_t4272832432::get_offset_of_MiddleColor_2(),
	SkyParam_t4272832432::get_offset_of_BottomColor_3(),
	SkyParam_t4272832432::get_offset_of_CloudsTint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (SkyParamsList_t3054173379), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (StarsParam_t907828490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2632[1] = 
{
	StarsParam_t907828490::get_offset_of_TintColor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (StarsParamsList_t1887883603), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (SunParam_t3023881265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2634[4] = 
{
	SunParam_t3023881265::get_offset_of_Time_0(),
	SunParam_t3023881265::get_offset_of_TintColor_1(),
	SunParam_t3023881265::get_offset_of_LightColor_2(),
	SunParam_t3023881265::get_offset_of_LightIntencity_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (SunParamsList_t120226188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (AnimatedButton_t4037967263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2636[3] = 
{
	AnimatedButton_t4037967263::get_offset_of_interactable_2(),
	AnimatedButton_t4037967263::get_offset_of_m_OnClick_3(),
	AnimatedButton_t4037967263::get_offset_of_m_animator_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (ButtonClickedEvent_t1021215292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (BackgroundMusic_t2182474183), -1, sizeof(BackgroundMusic_t2182474183_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2638[2] = 
{
	BackgroundMusic_t2182474183_StaticFields::get_offset_of_Instance_2(),
	BackgroundMusic_t2182474183::get_offset_of_m_audioSource_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (Fade_t3857387351)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2639[3] = 
{
	Fade_t3857387351::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (U3CFadeAudioU3Ec__Iterator0_t1632642824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2640[10] = 
{
	U3CFadeAudioU3Ec__Iterator0_t1632642824::get_offset_of_fadeType_0(),
	U3CFadeAudioU3Ec__Iterator0_t1632642824::get_offset_of_U3CstartU3E__0_1(),
	U3CFadeAudioU3Ec__Iterator0_t1632642824::get_offset_of_U3CendU3E__1_2(),
	U3CFadeAudioU3Ec__Iterator0_t1632642824::get_offset_of_U3CiU3E__2_3(),
	U3CFadeAudioU3Ec__Iterator0_t1632642824::get_offset_of_time_4(),
	U3CFadeAudioU3Ec__Iterator0_t1632642824::get_offset_of_U3CstepU3E__3_5(),
	U3CFadeAudioU3Ec__Iterator0_t1632642824::get_offset_of_U24this_6(),
	U3CFadeAudioU3Ec__Iterator0_t1632642824::get_offset_of_U24current_7(),
	U3CFadeAudioU3Ec__Iterator0_t1632642824::get_offset_of_U24disposing_8(),
	U3CFadeAudioU3Ec__Iterator0_t1632642824::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (ColorSwapper_t1578650717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2641[4] = 
{
	ColorSwapper_t1578650717::get_offset_of_enabledColor_2(),
	ColorSwapper_t1578650717::get_offset_of_disabledColor_3(),
	ColorSwapper_t1578650717::get_offset_of_m_swapped_4(),
	ColorSwapper_t1578650717::get_offset_of_m_image_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (Fader_t3816812854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2642[2] = 
{
	Fader_t3816812854::get_offset_of_duration_2(),
	Fader_t3816812854::get_offset_of_m_canvasGroup_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (U3CRunFadeInU3Ec__Iterator0_t794606817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2643[6] = 
{
	U3CRunFadeInU3Ec__Iterator0_t794606817::get_offset_of_U3CtimeU3E__0_0(),
	U3CRunFadeInU3Ec__Iterator0_t794606817::get_offset_of_U3CinitialAlphaU3E__1_1(),
	U3CRunFadeInU3Ec__Iterator0_t794606817::get_offset_of_U24this_2(),
	U3CRunFadeInU3Ec__Iterator0_t794606817::get_offset_of_U24current_3(),
	U3CRunFadeInU3Ec__Iterator0_t794606817::get_offset_of_U24disposing_4(),
	U3CRunFadeInU3Ec__Iterator0_t794606817::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (U3CRunFadeOutU3Ec__Iterator1_t226132861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2644[6] = 
{
	U3CRunFadeOutU3Ec__Iterator1_t226132861::get_offset_of_U3CtimeU3E__0_0(),
	U3CRunFadeOutU3Ec__Iterator1_t226132861::get_offset_of_U3CinitialAlphaU3E__1_1(),
	U3CRunFadeOutU3Ec__Iterator1_t226132861::get_offset_of_U24this_2(),
	U3CRunFadeOutU3Ec__Iterator1_t226132861::get_offset_of_U24current_3(),
	U3CRunFadeOutU3Ec__Iterator1_t226132861::get_offset_of_U24disposing_4(),
	U3CRunFadeOutU3Ec__Iterator1_t226132861::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (InitialPlayerPrefs_t757210519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (LevelScene_t288242702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2646[7] = 
{
	LevelScene_t288242702::get_offset_of_prevLevelButton_2(),
	LevelScene_t288242702::get_offset_of_nextLevelButton_3(),
	LevelScene_t288242702::get_offset_of_levelGroup_4(),
	LevelScene_t288242702::get_offset_of_levelText_5(),
	0,
	LevelScene_t288242702::get_offset_of_m_currentLevelIndex_7(),
	LevelScene_t288242702::get_offset_of_m_animator_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (MusicButton_t3792610623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2647[2] = 
{
	MusicButton_t3792610623::get_offset_of_m_spriteSwapper_2(),
	MusicButton_t3792610623::get_offset_of_m_on_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (MusicManager_t1668724344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2648[2] = 
{
	MusicManager_t1668724344::get_offset_of_m_musicSlider_2(),
	MusicManager_t1668724344::get_offset_of_m_musicButton_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (MuteBackgroundMusic_t1333099764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2649[1] = 
{
	MuteBackgroundMusic_t1333099764::get_offset_of_m_bgMusic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (PlayPopup_t258579664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2650[5] = 
{
	PlayPopup_t258579664::get_offset_of_enabledColor_4(),
	PlayPopup_t258579664::get_offset_of_disabledColor_5(),
	PlayPopup_t258579664::get_offset_of_leftStarImage_6(),
	PlayPopup_t258579664::get_offset_of_middleStarImage_7(),
	PlayPopup_t258579664::get_offset_of_rightStarImage_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (PlayPopupOpener_t1253928371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2651[1] = 
{
	PlayPopupOpener_t1253928371::get_offset_of_starsObtained_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (Popup_t1187979376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2652[2] = 
{
	Popup_t1187979376::get_offset_of_backgroundColor_2(),
	Popup_t1187979376::get_offset_of_m_background_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (U3CRunPopupDestroyU3Ec__Iterator0_t179771350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2653[4] = 
{
	U3CRunPopupDestroyU3Ec__Iterator0_t179771350::get_offset_of_U24this_0(),
	U3CRunPopupDestroyU3Ec__Iterator0_t179771350::get_offset_of_U24current_1(),
	U3CRunPopupDestroyU3Ec__Iterator0_t179771350::get_offset_of_U24disposing_2(),
	U3CRunPopupDestroyU3Ec__Iterator0_t179771350::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (PopupOpener_t3649718601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2654[2] = 
{
	PopupOpener_t3649718601::get_offset_of_popupPrefab_2(),
	PopupOpener_t3649718601::get_offset_of_m_canvas_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (SceneTransition_t2696223135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2655[3] = 
{
	SceneTransition_t2696223135::get_offset_of_scene_2(),
	SceneTransition_t2696223135::get_offset_of_duration_3(),
	SceneTransition_t2696223135::get_offset_of_color_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (SoundButton_t2589941643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2656[2] = 
{
	SoundButton_t2589941643::get_offset_of_m_spriteSwapper_2(),
	SoundButton_t2589941643::get_offset_of_m_on_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (SoundManager_t1319472430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2657[2] = 
{
	SoundManager_t1319472430::get_offset_of_m_soundSlider_2(),
	SoundManager_t1319472430::get_offset_of_m_soundButton_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (SpinWheel_t1385180419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[2] = 
{
	SpinWheel_t1385180419::get_offset_of_AnimationCurve_2(),
	SpinWheel_t1385180419::get_offset_of_m_spinning_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (U3CDoSpinU3Ec__Iterator0_t288164173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2659[9] = 
{
	U3CDoSpinU3Ec__Iterator0_t288164173::get_offset_of_U3CtimerU3E__0_0(),
	U3CDoSpinU3Ec__Iterator0_t288164173::get_offset_of_U3CstartAngleU3E__1_1(),
	U3CDoSpinU3Ec__Iterator0_t288164173::get_offset_of_U3CtimeU3E__2_2(),
	U3CDoSpinU3Ec__Iterator0_t288164173::get_offset_of_U3CmaxAngleU3E__3_3(),
	U3CDoSpinU3Ec__Iterator0_t288164173::get_offset_of_U3CangleU3E__4_4(),
	U3CDoSpinU3Ec__Iterator0_t288164173::get_offset_of_U24this_5(),
	U3CDoSpinU3Ec__Iterator0_t288164173::get_offset_of_U24current_6(),
	U3CDoSpinU3Ec__Iterator0_t288164173::get_offset_of_U24disposing_7(),
	U3CDoSpinU3Ec__Iterator0_t288164173::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (SpriteSwapper_t3880336557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2660[4] = 
{
	SpriteSwapper_t3880336557::get_offset_of_enabledSprite_2(),
	SpriteSwapper_t3880336557::get_offset_of_disabledSprite_3(),
	SpriteSwapper_t3880336557::get_offset_of_m_swapped_4(),
	SpriteSwapper_t3880336557::get_offset_of_m_image_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (TintedButton_t315702378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[4] = 
{
	TintedButton_t315702378::get_offset_of_m_OnClick_2(),
	0,
	TintedButton_t315702378::get_offset_of_m_pointerInside_4(),
	TintedButton_t315702378::get_offset_of_m_pointerPressed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (ButtonClickedEvent_t2671903891), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (U3CDarkenColorU3Ec__Iterator0_t1184020839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2663[6] = 
{
	U3CDarkenColorU3Ec__Iterator0_t1184020839::get_offset_of_U3CiU3E__0_0(),
	U3CDarkenColorU3Ec__Iterator0_t1184020839::get_offset_of_image_1(),
	U3CDarkenColorU3Ec__Iterator0_t1184020839::get_offset_of_U3CnewColorU3E__1_2(),
	U3CDarkenColorU3Ec__Iterator0_t1184020839::get_offset_of_U24current_3(),
	U3CDarkenColorU3Ec__Iterator0_t1184020839::get_offset_of_U24disposing_4(),
	U3CDarkenColorU3Ec__Iterator0_t1184020839::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (U3CBrightenColorU3Ec__Iterator1_t3614062068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2664[6] = 
{
	U3CBrightenColorU3Ec__Iterator1_t3614062068::get_offset_of_U3CiU3E__0_0(),
	U3CBrightenColorU3Ec__Iterator1_t3614062068::get_offset_of_image_1(),
	U3CBrightenColorU3Ec__Iterator1_t3614062068::get_offset_of_U3CnewColorU3E__1_2(),
	U3CBrightenColorU3Ec__Iterator1_t3614062068::get_offset_of_U24current_3(),
	U3CBrightenColorU3Ec__Iterator1_t3614062068::get_offset_of_U24disposing_4(),
	U3CBrightenColorU3Ec__Iterator1_t3614062068::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (Transition_t2490031921), -1, sizeof(Transition_t2490031921_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2665[2] = 
{
	Transition_t2490031921_StaticFields::get_offset_of_m_canvas_2(),
	Transition_t2490031921::get_offset_of_m_overlay_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (U3CRunFadeU3Ec__Iterator0_t4246392297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2666[14] = 
{
	U3CRunFadeU3Ec__Iterator0_t4246392297::get_offset_of_U3CbgTexU3E__0_0(),
	U3CRunFadeU3Ec__Iterator0_t4246392297::get_offset_of_fadeColor_1(),
	U3CRunFadeU3Ec__Iterator0_t4246392297::get_offset_of_U3CimageU3E__1_2(),
	U3CRunFadeU3Ec__Iterator0_t4246392297::get_offset_of_U3CrectU3E__2_3(),
	U3CRunFadeU3Ec__Iterator0_t4246392297::get_offset_of_U3CspriteU3E__3_4(),
	U3CRunFadeU3Ec__Iterator0_t4246392297::get_offset_of_U3CnewColorU3E__4_5(),
	U3CRunFadeU3Ec__Iterator0_t4246392297::get_offset_of_U3CtimeU3E__5_6(),
	U3CRunFadeU3Ec__Iterator0_t4246392297::get_offset_of_duration_7(),
	U3CRunFadeU3Ec__Iterator0_t4246392297::get_offset_of_U3ChalfDurationU3E__6_8(),
	U3CRunFadeU3Ec__Iterator0_t4246392297::get_offset_of_level_9(),
	U3CRunFadeU3Ec__Iterator0_t4246392297::get_offset_of_U24this_10(),
	U3CRunFadeU3Ec__Iterator0_t4246392297::get_offset_of_U24current_11(),
	U3CRunFadeU3Ec__Iterator0_t4246392297::get_offset_of_U24disposing_12(),
	U3CRunFadeU3Ec__Iterator0_t4246392297::get_offset_of_U24PC_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (CFX2_Demo_t1946811213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2667[11] = 
{
	CFX2_Demo_t1946811213::get_offset_of_orderedSpawns_2(),
	CFX2_Demo_t1946811213::get_offset_of_step_3(),
	CFX2_Demo_t1946811213::get_offset_of_range_4(),
	CFX2_Demo_t1946811213::get_offset_of_order_5(),
	CFX2_Demo_t1946811213::get_offset_of_groundMat_6(),
	CFX2_Demo_t1946811213::get_offset_of_waterMat_7(),
	CFX2_Demo_t1946811213::get_offset_of_ParticleExamples_8(),
	CFX2_Demo_t1946811213::get_offset_of_exampleIndex_9(),
	CFX2_Demo_t1946811213::get_offset_of_randomSpawnsDelay_10(),
	CFX2_Demo_t1946811213::get_offset_of_randomSpawns_11(),
	CFX2_Demo_t1946811213::get_offset_of_slowMo_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (U3CRandomSpawnsCoroutineU3Ec__Iterator0_t4111098856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2668[5] = 
{
	U3CRandomSpawnsCoroutineU3Ec__Iterator0_t4111098856::get_offset_of_U3CparticlesU3E__0_0(),
	U3CRandomSpawnsCoroutineU3Ec__Iterator0_t4111098856::get_offset_of_U24this_1(),
	U3CRandomSpawnsCoroutineU3Ec__Iterator0_t4111098856::get_offset_of_U24current_2(),
	U3CRandomSpawnsCoroutineU3Ec__Iterator0_t4111098856::get_offset_of_U24disposing_3(),
	U3CRandomSpawnsCoroutineU3Ec__Iterator0_t4111098856::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (CFX_AutoStopLoopedEffect_t3214280257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[2] = 
{
	CFX_AutoStopLoopedEffect_t3214280257::get_offset_of_effectDuration_2(),
	CFX_AutoStopLoopedEffect_t3214280257::get_offset_of_d_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (CFX_Demo_t1465385775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2670[12] = 
{
	CFX_Demo_t1465385775::get_offset_of_orderedSpawns_2(),
	CFX_Demo_t1465385775::get_offset_of_step_3(),
	CFX_Demo_t1465385775::get_offset_of_range_4(),
	CFX_Demo_t1465385775::get_offset_of_order_5(),
	CFX_Demo_t1465385775::get_offset_of_groundMat_6(),
	CFX_Demo_t1465385775::get_offset_of_waterMat_7(),
	CFX_Demo_t1465385775::get_offset_of_ParticleExamples_8(),
	CFX_Demo_t1465385775::get_offset_of_ParticlesYOffsetD_9(),
	CFX_Demo_t1465385775::get_offset_of_exampleIndex_10(),
	CFX_Demo_t1465385775::get_offset_of_randomSpawnsDelay_11(),
	CFX_Demo_t1465385775::get_offset_of_randomSpawns_12(),
	CFX_Demo_t1465385775::get_offset_of_slowMo_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[5] = 
{
	U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400::get_offset_of_U3CparticlesU3E__0_0(),
	U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400::get_offset_of_U24this_1(),
	U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400::get_offset_of_U24current_2(),
	U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400::get_offset_of_U24disposing_3(),
	U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (CFX_Demo_New_t3744422722), -1, sizeof(CFX_Demo_New_t3744422722_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2672[11] = 
{
	CFX_Demo_New_t3744422722::get_offset_of_EffectLabel_2(),
	CFX_Demo_New_t3744422722::get_offset_of_EffectIndexLabel_3(),
	CFX_Demo_New_t3744422722::get_offset_of_groundRenderer_4(),
	CFX_Demo_New_t3744422722::get_offset_of_groundCollider_5(),
	CFX_Demo_New_t3744422722::get_offset_of_ParticleExamples_6(),
	CFX_Demo_New_t3744422722::get_offset_of_exampleIndex_7(),
	CFX_Demo_New_t3744422722::get_offset_of_slowMo_8(),
	CFX_Demo_New_t3744422722::get_offset_of_defaultCamPosition_9(),
	CFX_Demo_New_t3744422722::get_offset_of_defaultCamRotation_10(),
	CFX_Demo_New_t3744422722::get_offset_of_onScreenParticles_11(),
	CFX_Demo_New_t3744422722_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (U3CCheckForDeletedParticlesU3Ec__Iterator0_t2558571495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2673[4] = 
{
	U3CCheckForDeletedParticlesU3Ec__Iterator0_t2558571495::get_offset_of_U24this_0(),
	U3CCheckForDeletedParticlesU3Ec__Iterator0_t2558571495::get_offset_of_U24current_1(),
	U3CCheckForDeletedParticlesU3Ec__Iterator0_t2558571495::get_offset_of_U24disposing_2(),
	U3CCheckForDeletedParticlesU3Ec__Iterator0_t2558571495::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (CFX_Demo_RandomDir_t1076153330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2674[2] = 
{
	CFX_Demo_RandomDir_t1076153330::get_offset_of_min_2(),
	CFX_Demo_RandomDir_t1076153330::get_offset_of_max_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (CFX_Demo_RandomDirectionTranslate_t747977598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2675[5] = 
{
	CFX_Demo_RandomDirectionTranslate_t747977598::get_offset_of_speed_2(),
	CFX_Demo_RandomDirectionTranslate_t747977598::get_offset_of_baseDir_3(),
	CFX_Demo_RandomDirectionTranslate_t747977598::get_offset_of_axis_4(),
	CFX_Demo_RandomDirectionTranslate_t747977598::get_offset_of_gravity_5(),
	CFX_Demo_RandomDirectionTranslate_t747977598::get_offset_of_dir_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (CFX_Demo_RotateCamera_t2665602760), -1, sizeof(CFX_Demo_RotateCamera_t2665602760_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2676[3] = 
{
	CFX_Demo_RotateCamera_t2665602760_StaticFields::get_offset_of_rotating_2(),
	CFX_Demo_RotateCamera_t2665602760::get_offset_of_speed_3(),
	CFX_Demo_RotateCamera_t2665602760::get_offset_of_rotationCenter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (CFX_Demo_Translate_t3218432890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2677[5] = 
{
	CFX_Demo_Translate_t3218432890::get_offset_of_speed_2(),
	CFX_Demo_Translate_t3218432890::get_offset_of_rotation_3(),
	CFX_Demo_Translate_t3218432890::get_offset_of_axis_4(),
	CFX_Demo_Translate_t3218432890::get_offset_of_gravity_5(),
	CFX_Demo_Translate_t3218432890::get_offset_of_dir_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (CFX_Demo_GTButton_t113862661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2678[6] = 
{
	CFX_Demo_GTButton_t113862661::get_offset_of_NormalColor_2(),
	CFX_Demo_GTButton_t113862661::get_offset_of_HoverColor_3(),
	CFX_Demo_GTButton_t113862661::get_offset_of_Callback_4(),
	CFX_Demo_GTButton_t113862661::get_offset_of_Receiver_5(),
	CFX_Demo_GTButton_t113862661::get_offset_of_CollisionRect_6(),
	CFX_Demo_GTButton_t113862661::get_offset_of_Over_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (CFX_Demo_GTToggle_t2599739681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2679[10] = 
{
	CFX_Demo_GTToggle_t2599739681::get_offset_of_Normal_2(),
	CFX_Demo_GTToggle_t2599739681::get_offset_of_Hover_3(),
	CFX_Demo_GTToggle_t2599739681::get_offset_of_NormalColor_4(),
	CFX_Demo_GTToggle_t2599739681::get_offset_of_DisabledColor_5(),
	CFX_Demo_GTToggle_t2599739681::get_offset_of_State_6(),
	CFX_Demo_GTToggle_t2599739681::get_offset_of_Callback_7(),
	CFX_Demo_GTToggle_t2599739681::get_offset_of_Receiver_8(),
	CFX_Demo_GTToggle_t2599739681::get_offset_of_CollisionRect_9(),
	CFX_Demo_GTToggle_t2599739681::get_offset_of_Over_10(),
	CFX_Demo_GTToggle_t2599739681::get_offset_of_Label_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (CFX2_AutoRotate_t42166984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2680[1] = 
{
	CFX2_AutoRotate_t42166984::get_offset_of_speed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (CFX_AutoDestructShuriken_t1500114366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[1] = 
{
	CFX_AutoDestructShuriken_t1500114366::get_offset_of_OnlyDeactivate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (U3CCheckIfAliveU3Ec__Iterator0_t3107901010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2682[5] = 
{
	U3CCheckIfAliveU3Ec__Iterator0_t3107901010::get_offset_of_U3CpsU3E__0_0(),
	U3CCheckIfAliveU3Ec__Iterator0_t3107901010::get_offset_of_U24this_1(),
	U3CCheckIfAliveU3Ec__Iterator0_t3107901010::get_offset_of_U24current_2(),
	U3CCheckIfAliveU3Ec__Iterator0_t3107901010::get_offset_of_U24disposing_3(),
	U3CCheckIfAliveU3Ec__Iterator0_t3107901010::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (CFX_AutoRotate_t1831446564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2683[2] = 
{
	CFX_AutoRotate_t1831446564::get_offset_of_rotation_2(),
	CFX_AutoRotate_t1831446564::get_offset_of_space_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (CFX_AutodestructWhenNoChildren_t3972162727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (CFX_InspectorHelp_t3280468206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2685[4] = 
{
	CFX_InspectorHelp_t3280468206::get_offset_of_Locked_2(),
	CFX_InspectorHelp_t3280468206::get_offset_of_Title_3(),
	CFX_InspectorHelp_t3280468206::get_offset_of_HelpText_4(),
	CFX_InspectorHelp_t3280468206::get_offset_of_MsgType_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (CFX_LightIntensityFade_t4221734619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2686[7] = 
{
	CFX_LightIntensityFade_t4221734619::get_offset_of_duration_2(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_delay_3(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_finalIntensity_4(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_baseIntensity_5(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_autodestruct_6(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_p_lifetime_7(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_p_delay_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (CFX_ShurikenThreadFix_t3286853382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[1] = 
{
	CFX_ShurikenThreadFix_t3286853382::get_offset_of_systems_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (U3CWaitFrameU3Ec__Iterator0_t410180078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[6] = 
{
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24locvar0_0(),
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24locvar1_1(),
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24this_2(),
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24current_3(),
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24disposing_4(),
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (CFX_SpawnSystem_t3600628354), -1, sizeof(CFX_SpawnSystem_t3600628354_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2689[10] = 
{
	CFX_SpawnSystem_t3600628354_StaticFields::get_offset_of_instance_2(),
	CFX_SpawnSystem_t3600628354::get_offset_of_objectsToPreload_3(),
	CFX_SpawnSystem_t3600628354::get_offset_of_objectsToPreloadTimes_4(),
	CFX_SpawnSystem_t3600628354::get_offset_of_hideObjectsInHierarchy_5(),
	CFX_SpawnSystem_t3600628354::get_offset_of_spawnAsChildren_6(),
	CFX_SpawnSystem_t3600628354::get_offset_of_onlyGetInactiveObjects_7(),
	CFX_SpawnSystem_t3600628354::get_offset_of_instantiateIfNeeded_8(),
	CFX_SpawnSystem_t3600628354::get_offset_of_allObjectsLoaded_9(),
	CFX_SpawnSystem_t3600628354::get_offset_of_instantiatedObjects_10(),
	CFX_SpawnSystem_t3600628354::get_offset_of_poolCursors_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (CharacterControl_t3164190352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2690[37] = 
{
	CharacterControl_t3164190352::get_offset_of_EventSys_2(),
	CharacterControl_t3164190352::get_offset_of_success_3(),
	CharacterControl_t3164190352::get_offset_of_successSound_4(),
	CharacterControl_t3164190352::get_offset_of__base_5(),
	CharacterControl_t3164190352::get_offset_of_DogCharacter_6(),
	CharacterControl_t3164190352::get_offset_of_dogDestroyed_7(),
	CharacterControl_t3164190352::get_offset_of_dogLanded_8(),
	CharacterControl_t3164190352::get_offset_of_dogSpeed_9(),
	CharacterControl_t3164190352::get_offset_of_CatCharacter_10(),
	CharacterControl_t3164190352::get_offset_of_catDestroyed_11(),
	CharacterControl_t3164190352::get_offset_of_catLanded_12(),
	CharacterControl_t3164190352::get_offset_of_catSpeed_13(),
	CharacterControl_t3164190352::get_offset_of_OwlCharacter_14(),
	CharacterControl_t3164190352::get_offset_of_owlDestroyed_15(),
	CharacterControl_t3164190352::get_offset_of_owlLanded_16(),
	CharacterControl_t3164190352::get_offset_of_owlSpeed_17(),
	CharacterControl_t3164190352::get_offset_of_TurtleCharacter_18(),
	CharacterControl_t3164190352::get_offset_of_turtleDestroyed_19(),
	CharacterControl_t3164190352::get_offset_of_turtleLanded_20(),
	CharacterControl_t3164190352::get_offset_of_turtleSpeed_21(),
	CharacterControl_t3164190352::get_offset_of_RatCharacter_22(),
	CharacterControl_t3164190352::get_offset_of_ratDestroyed_23(),
	CharacterControl_t3164190352::get_offset_of_ratLanded_24(),
	CharacterControl_t3164190352::get_offset_of_ratSpeed_25(),
	CharacterControl_t3164190352::get_offset_of_puff_26(),
	CharacterControl_t3164190352::get_offset_of_dogLastPosition_27(),
	CharacterControl_t3164190352::get_offset_of_catLastPosition_28(),
	CharacterControl_t3164190352::get_offset_of_owlLastPosition_29(),
	CharacterControl_t3164190352::get_offset_of_ratLastPosition_30(),
	CharacterControl_t3164190352::get_offset_of_turtleLastPosition_31(),
	CharacterControl_t3164190352::get_offset_of_dogSpace_32(),
	CharacterControl_t3164190352::get_offset_of_catSpace_33(),
	CharacterControl_t3164190352::get_offset_of_owlSpace_34(),
	CharacterControl_t3164190352::get_offset_of_ratSpace_35(),
	CharacterControl_t3164190352::get_offset_of_turtleSpace_36(),
	CharacterControl_t3164190352::get_offset_of_freeze_37(),
	CharacterControl_t3164190352::get_offset_of_talkDog_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (DrawCircle_t4100096050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[2] = 
{
	DrawCircle_t4100096050::get_offset_of_target_2(),
	DrawCircle_t4100096050::get_offset_of_speed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (IconControl_t564227302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[30] = 
{
	IconControl_t564227302::get_offset_of__base_2(),
	IconControl_t564227302::get_offset_of_EventSys_3(),
	IconControl_t564227302::get_offset_of_catIcon_4(),
	IconControl_t564227302::get_offset_of_catDestroyed_5(),
	IconControl_t564227302::get_offset_of_catSpeed_6(),
	IconControl_t564227302::get_offset_of_ratIcon_7(),
	IconControl_t564227302::get_offset_of_ratDestroyed_8(),
	IconControl_t564227302::get_offset_of_ratSpeed_9(),
	IconControl_t564227302::get_offset_of_dogIcon_10(),
	IconControl_t564227302::get_offset_of_dogDestroyed_11(),
	IconControl_t564227302::get_offset_of_dogSpeed_12(),
	IconControl_t564227302::get_offset_of_owlIcon_13(),
	IconControl_t564227302::get_offset_of_owlDestroyed_14(),
	IconControl_t564227302::get_offset_of_owlSpeed_15(),
	IconControl_t564227302::get_offset_of_turtleIcon_16(),
	IconControl_t564227302::get_offset_of_turtleDestroyed_17(),
	IconControl_t564227302::get_offset_of_turtleSpeed_18(),
	IconControl_t564227302::get_offset_of_Player2Score_19(),
	IconControl_t564227302::get_offset_of_Player1Score_20(),
	IconControl_t564227302::get_offset_of_pointSound_21(),
	IconControl_t564227302::get_offset_of_failSound_22(),
	IconControl_t564227302::get_offset_of_dogLastPosition_23(),
	IconControl_t564227302::get_offset_of_catLastPosition_24(),
	IconControl_t564227302::get_offset_of_owlLastPosition_25(),
	IconControl_t564227302::get_offset_of_ratLastPosition_26(),
	IconControl_t564227302::get_offset_of_turtleLastPosition_27(),
	IconControl_t564227302::get_offset_of_pl1collider_28(),
	IconControl_t564227302::get_offset_of_pl2collider_29(),
	IconControl_t564227302::get_offset_of_success_30(),
	IconControl_t564227302::get_offset_of_correctAnswer_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (LookAt_t2293208666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2693[1] = 
{
	LookAt_t2293208666::get_offset_of_target_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (RunForwardandBack_t3788415244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2694[3] = 
{
	RunForwardandBack_t3788415244::get_offset_of_speed_2(),
	RunForwardandBack_t3788415244::get_offset_of_right_3(),
	RunForwardandBack_t3788415244::get_offset_of_left_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (SpaceDetection_t1772284253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2695[1] = 
{
	SpaceDetection_t1772284253::get_offset_of_CC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (TabBase_t319583306), -1, sizeof(TabBase_t319583306_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2696[21] = 
{
	TabBase_t319583306::get_offset_of_idSize_2(),
	TabBase_t319583306::get_offset_of_addBaseCounter_3(),
	TabBase_t319583306::get_offset_of_removeBaseCounter_4(),
	TabBase_t319583306::get_offset_of_PosAverageCounter_5(),
	TabBase_t319583306_StaticFields::get_offset_of_tabData_6(),
	TabBase_t319583306::get_offset_of_tabIdData_7(),
	TabBase_t319583306_StaticFields::get_offset_of_dataState_8(),
	TabBase_t319583306_StaticFields::get_offset_of_testID_9(),
	TabBase_t319583306_StaticFields::get_offset_of__uniqID_10(),
	TabBase_t319583306_StaticFields::get_offset_of_switch_state1_11(),
	TabBase_t319583306_StaticFields::get_offset_of_switch_state5_12(),
	TabBase_t319583306::get_offset_of_buttonCounter_13(),
	TabBase_t319583306::get_offset_of_idDictionary_14(),
	TabBase_t319583306::get_offset_of_idList_15(),
	TabBase_t319583306::get_offset_of_testBleLastUniqId_16(),
	TabBase_t319583306::get_offset_of_testData_17(),
	TabBase_t319583306_StaticFields::get_offset_of_lastUniqId_18(),
	TabBase_t319583306_StaticFields::get_offset_of_lastCount_19(),
	TabBase_t319583306_StaticFields::get_offset_of_lastTestId_20(),
	TabBase_t319583306::get_offset_of_button_state_21(),
	TabBase_t319583306::get_offset_of_myStyle_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (baseLegID_t3972271149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2697[3] = 
{
	baseLegID_t3972271149::get_offset_of_id_0(),
	baseLegID_t3972271149::get_offset_of_x_1(),
	baseLegID_t3972271149::get_offset_of_y_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (tabID_t570832297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[6] = 
{
	tabID_t570832297::get_offset_of_getPos_0(),
	tabID_t570832297::get_offset_of_id_1(),
	tabID_t570832297::get_offset_of_changedData_2(),
	tabID_t570832297::get_offset_of_addCounter_3(),
	tabID_t570832297::get_offset_of_removeCounter_4(),
	tabID_t570832297::get_offset_of_posAverageCounter_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (tBase_t1720960579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[4] = 
{
	tBase_t1720960579::get_offset_of_state_0(),
	tBase_t1720960579::get_offset_of_x_1(),
	tBase_t1720960579::get_offset_of_y_2(),
	tBase_t1720960579::get_offset_of_rotation_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
