﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Ricimi.SpinWheel
struct SpinWheel_t1385180419;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.SpinWheel/<DoSpin>c__Iterator0
struct  U3CDoSpinU3Ec__Iterator0_t288164173  : public Il2CppObject
{
public:
	// System.Single Ricimi.SpinWheel/<DoSpin>c__Iterator0::<timer>__0
	float ___U3CtimerU3E__0_0;
	// System.Single Ricimi.SpinWheel/<DoSpin>c__Iterator0::<startAngle>__1
	float ___U3CstartAngleU3E__1_1;
	// System.Single Ricimi.SpinWheel/<DoSpin>c__Iterator0::<time>__2
	float ___U3CtimeU3E__2_2;
	// System.Single Ricimi.SpinWheel/<DoSpin>c__Iterator0::<maxAngle>__3
	float ___U3CmaxAngleU3E__3_3;
	// System.Single Ricimi.SpinWheel/<DoSpin>c__Iterator0::<angle>__4
	float ___U3CangleU3E__4_4;
	// Ricimi.SpinWheel Ricimi.SpinWheel/<DoSpin>c__Iterator0::$this
	SpinWheel_t1385180419 * ___U24this_5;
	// System.Object Ricimi.SpinWheel/<DoSpin>c__Iterator0::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean Ricimi.SpinWheel/<DoSpin>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Ricimi.SpinWheel/<DoSpin>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CtimerU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoSpinU3Ec__Iterator0_t288164173, ___U3CtimerU3E__0_0)); }
	inline float get_U3CtimerU3E__0_0() const { return ___U3CtimerU3E__0_0; }
	inline float* get_address_of_U3CtimerU3E__0_0() { return &___U3CtimerU3E__0_0; }
	inline void set_U3CtimerU3E__0_0(float value)
	{
		___U3CtimerU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartAngleU3E__1_1() { return static_cast<int32_t>(offsetof(U3CDoSpinU3Ec__Iterator0_t288164173, ___U3CstartAngleU3E__1_1)); }
	inline float get_U3CstartAngleU3E__1_1() const { return ___U3CstartAngleU3E__1_1; }
	inline float* get_address_of_U3CstartAngleU3E__1_1() { return &___U3CstartAngleU3E__1_1; }
	inline void set_U3CstartAngleU3E__1_1(float value)
	{
		___U3CstartAngleU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CtimeU3E__2_2() { return static_cast<int32_t>(offsetof(U3CDoSpinU3Ec__Iterator0_t288164173, ___U3CtimeU3E__2_2)); }
	inline float get_U3CtimeU3E__2_2() const { return ___U3CtimeU3E__2_2; }
	inline float* get_address_of_U3CtimeU3E__2_2() { return &___U3CtimeU3E__2_2; }
	inline void set_U3CtimeU3E__2_2(float value)
	{
		___U3CtimeU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CmaxAngleU3E__3_3() { return static_cast<int32_t>(offsetof(U3CDoSpinU3Ec__Iterator0_t288164173, ___U3CmaxAngleU3E__3_3)); }
	inline float get_U3CmaxAngleU3E__3_3() const { return ___U3CmaxAngleU3E__3_3; }
	inline float* get_address_of_U3CmaxAngleU3E__3_3() { return &___U3CmaxAngleU3E__3_3; }
	inline void set_U3CmaxAngleU3E__3_3(float value)
	{
		___U3CmaxAngleU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CangleU3E__4_4() { return static_cast<int32_t>(offsetof(U3CDoSpinU3Ec__Iterator0_t288164173, ___U3CangleU3E__4_4)); }
	inline float get_U3CangleU3E__4_4() const { return ___U3CangleU3E__4_4; }
	inline float* get_address_of_U3CangleU3E__4_4() { return &___U3CangleU3E__4_4; }
	inline void set_U3CangleU3E__4_4(float value)
	{
		___U3CangleU3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CDoSpinU3Ec__Iterator0_t288164173, ___U24this_5)); }
	inline SpinWheel_t1385180419 * get_U24this_5() const { return ___U24this_5; }
	inline SpinWheel_t1385180419 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(SpinWheel_t1385180419 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDoSpinU3Ec__Iterator0_t288164173, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CDoSpinU3Ec__Iterator0_t288164173, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CDoSpinU3Ec__Iterator0_t288164173, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
