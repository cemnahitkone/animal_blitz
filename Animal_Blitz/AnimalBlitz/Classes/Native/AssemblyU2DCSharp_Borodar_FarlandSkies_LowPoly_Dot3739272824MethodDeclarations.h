﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParamsList
struct CelestialParamsList_t3739272824;
// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam
struct CelestialParam_t4052651973;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.DotParams.CelestialParamsList::.ctor()
extern "C"  void CelestialParamsList__ctor_m695519456 (CelestialParamsList_t3739272824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam Borodar.FarlandSkies.LowPoly.DotParams.CelestialParamsList::GetParamPerTime(System.Single)
extern "C"  CelestialParam_t4052651973 * CelestialParamsList_GetParamPerTime_m2480843807 (CelestialParamsList_t3739272824 * __this, float ___currentTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
