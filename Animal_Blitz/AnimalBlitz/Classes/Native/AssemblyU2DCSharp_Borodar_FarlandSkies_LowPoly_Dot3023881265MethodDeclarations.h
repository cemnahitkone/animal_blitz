﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.DotParams.SunParam
struct SunParam_t3023881265;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.DotParams.SunParam::.ctor()
extern "C"  void SunParam__ctor_m1787041831 (SunParam_t3023881265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
