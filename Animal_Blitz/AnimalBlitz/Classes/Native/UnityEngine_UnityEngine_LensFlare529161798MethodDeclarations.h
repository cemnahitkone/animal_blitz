﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.LensFlare
struct LensFlare_t529161798;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.LensFlare::set_brightness(System.Single)
extern "C"  void LensFlare_set_brightness_m2781968884 (LensFlare_t529161798 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
