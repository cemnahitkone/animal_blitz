﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/Enumerator<System.Single,System.Object>
struct Enumerator_t3711273501;
// System.Collections.Generic.SortedList`2<System.Single,System.Object>
struct SortedList_2_t2155713151;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedList_2_Enum494119784.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.SortedList`2/Enumerator<System.Single,System.Object>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>,System.Collections.Generic.SortedList`2/EnumeratorMode<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1102728668_gshared (Enumerator_t3711273501 * __this, SortedList_2_t2155713151 * ___host0, int32_t ___mode1, const MethodInfo* method);
#define Enumerator__ctor_m1102728668(__this, ___host0, ___mode1, method) ((  void (*) (Enumerator_t3711273501 *, SortedList_2_t2155713151 *, int32_t, const MethodInfo*))Enumerator__ctor_m1102728668_gshared)(__this, ___host0, ___mode1, method)
// System.Void System.Collections.Generic.SortedList`2/Enumerator<System.Single,System.Object>::.cctor()
extern "C"  void Enumerator__cctor_m692988207_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Enumerator__cctor_m692988207(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Enumerator__cctor_m692988207_gshared)(__this /* static, unused */, method)
// System.Void System.Collections.Generic.SortedList`2/Enumerator<System.Single,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3605702253_gshared (Enumerator_t3711273501 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3605702253(__this, method) ((  void (*) (Enumerator_t3711273501 *, const MethodInfo*))Enumerator_Reset_m3605702253_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/Enumerator<System.Single,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3299316870_gshared (Enumerator_t3711273501 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3299316870(__this, method) ((  bool (*) (Enumerator_t3711273501 *, const MethodInfo*))Enumerator_MoveNext_m3299316870_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedList`2/Enumerator<System.Single,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_get_Entry_m3982561590_gshared (Enumerator_t3711273501 * __this, const MethodInfo* method);
#define Enumerator_get_Entry_m3982561590(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3711273501 *, const MethodInfo*))Enumerator_get_Entry_m3982561590_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Single,System.Object>::get_Key()
extern "C"  Il2CppObject * Enumerator_get_Key_m4278446931_gshared (Enumerator_t3711273501 * __this, const MethodInfo* method);
#define Enumerator_get_Key_m4278446931(__this, method) ((  Il2CppObject * (*) (Enumerator_t3711273501 *, const MethodInfo*))Enumerator_get_Key_m4278446931_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Single,System.Object>::get_Value()
extern "C"  Il2CppObject * Enumerator_get_Value_m1066467803_gshared (Enumerator_t3711273501 * __this, const MethodInfo* method);
#define Enumerator_get_Value_m1066467803(__this, method) ((  Il2CppObject * (*) (Enumerator_t3711273501 *, const MethodInfo*))Enumerator_get_Value_m1066467803_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Single,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3443387721_gshared (Enumerator_t3711273501 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3443387721(__this, method) ((  Il2CppObject * (*) (Enumerator_t3711273501 *, const MethodInfo*))Enumerator_get_Current_m3443387721_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Single,System.Object>::Clone()
extern "C"  Il2CppObject * Enumerator_Clone_m4019305018_gshared (Enumerator_t3711273501 * __this, const MethodInfo* method);
#define Enumerator_Clone_m4019305018(__this, method) ((  Il2CppObject * (*) (Enumerator_t3711273501 *, const MethodInfo*))Enumerator_Clone_m4019305018_gshared)(__this, method)
