﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle
struct SkyboxDayNightCycle_t2663460034;

#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_Helpers203123456.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.SkyboxCycleManager
struct  SkyboxCycleManager_t61875227  : public Singleton_1_t203123456
{
public:
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxCycleManager::CycleDuration
	float ___CycleDuration_3;
	// System.Single Borodar.FarlandSkies.LowPoly.SkyboxCycleManager::CycleProgress
	float ___CycleProgress_4;
	// System.Boolean Borodar.FarlandSkies.LowPoly.SkyboxCycleManager::Paused
	bool ___Paused_5;
	// Borodar.FarlandSkies.LowPoly.SkyboxDayNightCycle Borodar.FarlandSkies.LowPoly.SkyboxCycleManager::_dayNightCycle
	SkyboxDayNightCycle_t2663460034 * ____dayNightCycle_6;

public:
	inline static int32_t get_offset_of_CycleDuration_3() { return static_cast<int32_t>(offsetof(SkyboxCycleManager_t61875227, ___CycleDuration_3)); }
	inline float get_CycleDuration_3() const { return ___CycleDuration_3; }
	inline float* get_address_of_CycleDuration_3() { return &___CycleDuration_3; }
	inline void set_CycleDuration_3(float value)
	{
		___CycleDuration_3 = value;
	}

	inline static int32_t get_offset_of_CycleProgress_4() { return static_cast<int32_t>(offsetof(SkyboxCycleManager_t61875227, ___CycleProgress_4)); }
	inline float get_CycleProgress_4() const { return ___CycleProgress_4; }
	inline float* get_address_of_CycleProgress_4() { return &___CycleProgress_4; }
	inline void set_CycleProgress_4(float value)
	{
		___CycleProgress_4 = value;
	}

	inline static int32_t get_offset_of_Paused_5() { return static_cast<int32_t>(offsetof(SkyboxCycleManager_t61875227, ___Paused_5)); }
	inline bool get_Paused_5() const { return ___Paused_5; }
	inline bool* get_address_of_Paused_5() { return &___Paused_5; }
	inline void set_Paused_5(bool value)
	{
		___Paused_5 = value;
	}

	inline static int32_t get_offset_of__dayNightCycle_6() { return static_cast<int32_t>(offsetof(SkyboxCycleManager_t61875227, ____dayNightCycle_6)); }
	inline SkyboxDayNightCycle_t2663460034 * get__dayNightCycle_6() const { return ____dayNightCycle_6; }
	inline SkyboxDayNightCycle_t2663460034 ** get_address_of__dayNightCycle_6() { return &____dayNightCycle_6; }
	inline void set__dayNightCycle_6(SkyboxDayNightCycle_t2663460034 * value)
	{
		____dayNightCycle_6 = value;
		Il2CppCodeGenWriteBarrier(&____dayNightCycle_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
