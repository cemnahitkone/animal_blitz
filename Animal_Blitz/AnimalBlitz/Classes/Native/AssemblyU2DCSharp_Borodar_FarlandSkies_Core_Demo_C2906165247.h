﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.Image
struct Image_t2042527209;
// Borodar.FarlandSkies.Core.Demo.BaseColorButton
struct BaseColorButton_t3091645806;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.Core.Demo.ColorPicker
struct  ColorPicker_t2906165247  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform Borodar.FarlandSkies.Core.Demo.ColorPicker::_rectTransform
	RectTransform_t3349966182 * ____rectTransform_2;
	// UnityEngine.UI.Image Borodar.FarlandSkies.Core.Demo.ColorPicker::_image
	Image_t2042527209 * ____image_3;
	// Borodar.FarlandSkies.Core.Demo.BaseColorButton Borodar.FarlandSkies.Core.Demo.ColorPicker::<ColorButton>k__BackingField
	BaseColorButton_t3091645806 * ___U3CColorButtonU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of__rectTransform_2() { return static_cast<int32_t>(offsetof(ColorPicker_t2906165247, ____rectTransform_2)); }
	inline RectTransform_t3349966182 * get__rectTransform_2() const { return ____rectTransform_2; }
	inline RectTransform_t3349966182 ** get_address_of__rectTransform_2() { return &____rectTransform_2; }
	inline void set__rectTransform_2(RectTransform_t3349966182 * value)
	{
		____rectTransform_2 = value;
		Il2CppCodeGenWriteBarrier(&____rectTransform_2, value);
	}

	inline static int32_t get_offset_of__image_3() { return static_cast<int32_t>(offsetof(ColorPicker_t2906165247, ____image_3)); }
	inline Image_t2042527209 * get__image_3() const { return ____image_3; }
	inline Image_t2042527209 ** get_address_of__image_3() { return &____image_3; }
	inline void set__image_3(Image_t2042527209 * value)
	{
		____image_3 = value;
		Il2CppCodeGenWriteBarrier(&____image_3, value);
	}

	inline static int32_t get_offset_of_U3CColorButtonU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ColorPicker_t2906165247, ___U3CColorButtonU3Ek__BackingField_4)); }
	inline BaseColorButton_t3091645806 * get_U3CColorButtonU3Ek__BackingField_4() const { return ___U3CColorButtonU3Ek__BackingField_4; }
	inline BaseColorButton_t3091645806 ** get_address_of_U3CColorButtonU3Ek__BackingField_4() { return &___U3CColorButtonU3Ek__BackingField_4; }
	inline void set_U3CColorButtonU3Ek__BackingField_4(BaseColorButton_t3091645806 * value)
	{
		___U3CColorButtonU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CColorButtonU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
