﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.PropertyToggle
struct PropertyToggle_t1681642541;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.PropertyToggle::.ctor()
extern "C"  void PropertyToggle__ctor_m45841590 (PropertyToggle_t1681642541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.PropertyToggle::Awake()
extern "C"  void PropertyToggle_Awake_m89908157 (PropertyToggle_t1681642541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.PropertyToggle::Start()
extern "C"  void PropertyToggle_Start_m2779464678 (PropertyToggle_t1681642541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.PropertyToggle::OnValueChanged(System.Boolean)
extern "C"  void PropertyToggle_OnValueChanged_m88760895 (PropertyToggle_t1681642541 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
