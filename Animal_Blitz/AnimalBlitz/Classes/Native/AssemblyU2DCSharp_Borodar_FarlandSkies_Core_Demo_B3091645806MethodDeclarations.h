﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.Core.Demo.BaseColorButton
struct BaseColorButton_t3091645806;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void Borodar.FarlandSkies.Core.Demo.BaseColorButton::.ctor()
extern "C"  void BaseColorButton__ctor_m1438157891 (BaseColorButton_t3091645806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.Core.Demo.BaseColorButton::Awake()
extern "C"  void BaseColorButton_Awake_m806815814 (BaseColorButton_t3091645806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.Core.Demo.BaseColorButton::OnClick()
extern "C"  void BaseColorButton_OnClick_m950900394 (BaseColorButton_t3091645806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.Core.Demo.BaseColorButton::ChangeColor(UnityEngine.Color)
extern "C"  void BaseColorButton_ChangeColor_m1520367342 (BaseColorButton_t3091645806 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
