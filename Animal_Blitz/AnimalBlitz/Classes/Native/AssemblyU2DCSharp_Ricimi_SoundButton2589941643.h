﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Ricimi.SpriteSwapper
struct SpriteSwapper_t3880336557;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.SoundButton
struct  SoundButton_t2589941643  : public MonoBehaviour_t1158329972
{
public:
	// Ricimi.SpriteSwapper Ricimi.SoundButton::m_spriteSwapper
	SpriteSwapper_t3880336557 * ___m_spriteSwapper_2;
	// System.Boolean Ricimi.SoundButton::m_on
	bool ___m_on_3;

public:
	inline static int32_t get_offset_of_m_spriteSwapper_2() { return static_cast<int32_t>(offsetof(SoundButton_t2589941643, ___m_spriteSwapper_2)); }
	inline SpriteSwapper_t3880336557 * get_m_spriteSwapper_2() const { return ___m_spriteSwapper_2; }
	inline SpriteSwapper_t3880336557 ** get_address_of_m_spriteSwapper_2() { return &___m_spriteSwapper_2; }
	inline void set_m_spriteSwapper_2(SpriteSwapper_t3880336557 * value)
	{
		___m_spriteSwapper_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_spriteSwapper_2, value);
	}

	inline static int32_t get_offset_of_m_on_3() { return static_cast<int32_t>(offsetof(SoundButton_t2589941643, ___m_on_3)); }
	inline bool get_m_on_3() const { return ___m_on_3; }
	inline bool* get_address_of_m_on_3() { return &___m_on_3; }
	inline void set_m_on_3(bool value)
	{
		___m_on_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
