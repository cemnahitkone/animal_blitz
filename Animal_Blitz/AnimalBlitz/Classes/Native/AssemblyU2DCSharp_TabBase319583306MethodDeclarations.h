﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TabBase
struct TabBase_t319583306;
// System.Collections.Generic.Dictionary`2<System.Int32,tBase>
struct Dictionary_2_t728786214;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void TabBase::.ctor()
extern "C"  void TabBase__ctor_m2370470677 (TabBase_t319583306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabBase::Start()
extern "C"  void TabBase_Start_m1508650917 (TabBase_t319583306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabBase::Update()
extern "C"  void TabBase_Update_m1909341032 (TabBase_t319583306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TabBase::checkDataState()
extern "C"  bool TabBase_checkDataState_m3036812730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TabBase::getDataState()
extern "C"  bool TabBase_getDataState_m1438652662 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TabBase::get_uniqId()
extern "C"  int32_t TabBase_get_uniqId_m1739931730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TabBase::GetCount()
extern "C"  int32_t TabBase_GetCount_m1111189072 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,tBase> TabBase::GetBase()
extern "C"  Dictionary_2_t728786214 * TabBase_GetBase_m3096554348 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabBase::SendData(System.String)
extern "C"  void TabBase_SendData_m1632484299 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabBase::CloseTabToy()
extern "C"  void TabBase_CloseTabToy_m99423136 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabBase::CreateId()
extern "C"  void TabBase_CreateId_m2675960116 (TabBase_t319583306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabBase::OnGUI()
extern "C"  void TabBase_OnGUI_m2384831551 (TabBase_t319583306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabBase::.cctor()
extern "C"  void TabBase__cctor_m3860335284 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
