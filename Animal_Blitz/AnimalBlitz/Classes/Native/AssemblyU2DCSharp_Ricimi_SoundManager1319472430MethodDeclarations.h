﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.SoundManager
struct SoundManager_t1319472430;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.SoundManager::.ctor()
extern "C"  void SoundManager__ctor_m1523602440 (SoundManager_t1319472430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.SoundManager::Start()
extern "C"  void SoundManager_Start_m2221953192 (SoundManager_t1319472430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.SoundManager::SwitchSound()
extern "C"  void SoundManager_SwitchSound_m1257331605 (SoundManager_t1319472430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
