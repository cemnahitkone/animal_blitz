﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m303434200_gshared (KeyValuePair_2_t1296315204 * __this, float ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m303434200(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1296315204 *, float, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m303434200_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>::get_Key()
extern "C"  float KeyValuePair_2_get_Key_m4249127398_gshared (KeyValuePair_2_t1296315204 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m4249127398(__this, method) ((  float (*) (KeyValuePair_2_t1296315204 *, const MethodInfo*))KeyValuePair_2_get_Key_m4249127398_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m448823159_gshared (KeyValuePair_2_t1296315204 * __this, float ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m448823159(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1296315204 *, float, const MethodInfo*))KeyValuePair_2_set_Key_m448823159_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1014938790_gshared (KeyValuePair_2_t1296315204 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1014938790(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1296315204 *, const MethodInfo*))KeyValuePair_2_get_Value_m1014938790_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1070856647_gshared (KeyValuePair_2_t1296315204 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1070856647(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1296315204 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1070856647_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4220912861_gshared (KeyValuePair_2_t1296315204 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m4220912861(__this, method) ((  String_t* (*) (KeyValuePair_2_t1296315204 *, const MethodInfo*))KeyValuePair_2_ToString_m4220912861_gshared)(__this, method)
