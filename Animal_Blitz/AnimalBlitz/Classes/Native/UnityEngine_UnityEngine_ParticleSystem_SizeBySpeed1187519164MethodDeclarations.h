﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/SizeBySpeedModule
struct SizeBySpeedModule_t1187519164;
struct SizeBySpeedModule_t1187519164_marshaled_pinvoke;
struct SizeBySpeedModule_t1187519164_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_SizeBySpeed1187519164.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/SizeBySpeedModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void SizeBySpeedModule__ctor_m2280842760 (SizeBySpeedModule_t1187519164 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct SizeBySpeedModule_t1187519164;
struct SizeBySpeedModule_t1187519164_marshaled_pinvoke;

extern "C" void SizeBySpeedModule_t1187519164_marshal_pinvoke(const SizeBySpeedModule_t1187519164& unmarshaled, SizeBySpeedModule_t1187519164_marshaled_pinvoke& marshaled);
extern "C" void SizeBySpeedModule_t1187519164_marshal_pinvoke_back(const SizeBySpeedModule_t1187519164_marshaled_pinvoke& marshaled, SizeBySpeedModule_t1187519164& unmarshaled);
extern "C" void SizeBySpeedModule_t1187519164_marshal_pinvoke_cleanup(SizeBySpeedModule_t1187519164_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SizeBySpeedModule_t1187519164;
struct SizeBySpeedModule_t1187519164_marshaled_com;

extern "C" void SizeBySpeedModule_t1187519164_marshal_com(const SizeBySpeedModule_t1187519164& unmarshaled, SizeBySpeedModule_t1187519164_marshaled_com& marshaled);
extern "C" void SizeBySpeedModule_t1187519164_marshal_com_back(const SizeBySpeedModule_t1187519164_marshaled_com& marshaled, SizeBySpeedModule_t1187519164& unmarshaled);
extern "C" void SizeBySpeedModule_t1187519164_marshal_com_cleanup(SizeBySpeedModule_t1187519164_marshaled_com& marshaled);
