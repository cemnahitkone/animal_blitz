﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.Core.Demo.CameraOrbitController
struct  CameraOrbitController_t2546408343  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Borodar.FarlandSkies.Core.Demo.CameraOrbitController::Target
	Transform_t3275118058 * ___Target_2;
	// System.Single Borodar.FarlandSkies.Core.Demo.CameraOrbitController::Distance
	float ___Distance_3;
	// System.Single Borodar.FarlandSkies.Core.Demo.CameraOrbitController::DistanceMin
	float ___DistanceMin_4;
	// System.Single Borodar.FarlandSkies.Core.Demo.CameraOrbitController::DistanceMax
	float ___DistanceMax_5;
	// UnityEngine.Vector3 Borodar.FarlandSkies.Core.Demo.CameraOrbitController::Speed
	Vector3_t2243707580  ___Speed_6;
	// UnityEngine.Vector2 Borodar.FarlandSkies.Core.Demo.CameraOrbitController::VerticalRotationLimit
	Vector2_t2243707579  ___VerticalRotationLimit_7;
	// UnityEngine.Vector2 Borodar.FarlandSkies.Core.Demo.CameraOrbitController::_angles
	Vector2_t2243707579  ____angles_8;
	// System.Boolean Borodar.FarlandSkies.Core.Demo.CameraOrbitController::_isPointerOverGui
	bool ____isPointerOverGui_9;

public:
	inline static int32_t get_offset_of_Target_2() { return static_cast<int32_t>(offsetof(CameraOrbitController_t2546408343, ___Target_2)); }
	inline Transform_t3275118058 * get_Target_2() const { return ___Target_2; }
	inline Transform_t3275118058 ** get_address_of_Target_2() { return &___Target_2; }
	inline void set_Target_2(Transform_t3275118058 * value)
	{
		___Target_2 = value;
		Il2CppCodeGenWriteBarrier(&___Target_2, value);
	}

	inline static int32_t get_offset_of_Distance_3() { return static_cast<int32_t>(offsetof(CameraOrbitController_t2546408343, ___Distance_3)); }
	inline float get_Distance_3() const { return ___Distance_3; }
	inline float* get_address_of_Distance_3() { return &___Distance_3; }
	inline void set_Distance_3(float value)
	{
		___Distance_3 = value;
	}

	inline static int32_t get_offset_of_DistanceMin_4() { return static_cast<int32_t>(offsetof(CameraOrbitController_t2546408343, ___DistanceMin_4)); }
	inline float get_DistanceMin_4() const { return ___DistanceMin_4; }
	inline float* get_address_of_DistanceMin_4() { return &___DistanceMin_4; }
	inline void set_DistanceMin_4(float value)
	{
		___DistanceMin_4 = value;
	}

	inline static int32_t get_offset_of_DistanceMax_5() { return static_cast<int32_t>(offsetof(CameraOrbitController_t2546408343, ___DistanceMax_5)); }
	inline float get_DistanceMax_5() const { return ___DistanceMax_5; }
	inline float* get_address_of_DistanceMax_5() { return &___DistanceMax_5; }
	inline void set_DistanceMax_5(float value)
	{
		___DistanceMax_5 = value;
	}

	inline static int32_t get_offset_of_Speed_6() { return static_cast<int32_t>(offsetof(CameraOrbitController_t2546408343, ___Speed_6)); }
	inline Vector3_t2243707580  get_Speed_6() const { return ___Speed_6; }
	inline Vector3_t2243707580 * get_address_of_Speed_6() { return &___Speed_6; }
	inline void set_Speed_6(Vector3_t2243707580  value)
	{
		___Speed_6 = value;
	}

	inline static int32_t get_offset_of_VerticalRotationLimit_7() { return static_cast<int32_t>(offsetof(CameraOrbitController_t2546408343, ___VerticalRotationLimit_7)); }
	inline Vector2_t2243707579  get_VerticalRotationLimit_7() const { return ___VerticalRotationLimit_7; }
	inline Vector2_t2243707579 * get_address_of_VerticalRotationLimit_7() { return &___VerticalRotationLimit_7; }
	inline void set_VerticalRotationLimit_7(Vector2_t2243707579  value)
	{
		___VerticalRotationLimit_7 = value;
	}

	inline static int32_t get_offset_of__angles_8() { return static_cast<int32_t>(offsetof(CameraOrbitController_t2546408343, ____angles_8)); }
	inline Vector2_t2243707579  get__angles_8() const { return ____angles_8; }
	inline Vector2_t2243707579 * get_address_of__angles_8() { return &____angles_8; }
	inline void set__angles_8(Vector2_t2243707579  value)
	{
		____angles_8 = value;
	}

	inline static int32_t get_offset_of__isPointerOverGui_9() { return static_cast<int32_t>(offsetof(CameraOrbitController_t2546408343, ____isPointerOverGui_9)); }
	inline bool get__isPointerOverGui_9() const { return ____isPointerOverGui_9; }
	inline bool* get_address_of__isPointerOverGui_9() { return &____isPointerOverGui_9; }
	inline void set__isPointerOverGui_9(bool value)
	{
		____isPointerOverGui_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
