﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FPSWalkerEnhanced
struct FPSWalkerEnhanced_t1211262667;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t4070855101;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101.h"

// System.Void FPSWalkerEnhanced::.ctor()
extern "C"  void FPSWalkerEnhanced__ctor_m2474376202 (FPSWalkerEnhanced_t1211262667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSWalkerEnhanced::Start()
extern "C"  void FPSWalkerEnhanced_Start_m571701350 (FPSWalkerEnhanced_t1211262667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSWalkerEnhanced::FixedUpdate()
extern "C"  void FPSWalkerEnhanced_FixedUpdate_m3238808973 (FPSWalkerEnhanced_t1211262667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSWalkerEnhanced::Update()
extern "C"  void FPSWalkerEnhanced_Update_m3412799663 (FPSWalkerEnhanced_t1211262667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSWalkerEnhanced::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void FPSWalkerEnhanced_OnControllerColliderHit_m2537114598 (FPSWalkerEnhanced_t1211262667 * __this, ControllerColliderHit_t4070855101 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSWalkerEnhanced::FallingDamageAlert(System.Single)
extern "C"  void FPSWalkerEnhanced_FallingDamageAlert_m2072832505 (FPSWalkerEnhanced_t1211262667 * __this, float ___fallDistance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
