﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIActions
struct UIActions_t467790635;

#include "codegen/il2cpp-codegen.h"

// System.Void UIActions::.ctor()
extern "C"  void UIActions__ctor_m1180179564 (UIActions_t467790635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIActions::Awake()
extern "C"  void UIActions_Awake_m113298475 (UIActions_t467790635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIActions::Start()
extern "C"  void UIActions_Start_m4073899760 (UIActions_t467790635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIActions::Update()
extern "C"  void UIActions_Update_m831686391 (UIActions_t467790635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIActions::TwoPlayerGame()
extern "C"  void UIActions_TwoPlayerGame_m3495032287 (UIActions_t467790635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIActions::closeCurtains()
extern "C"  void UIActions_closeCurtains_m4258359545 (UIActions_t467790635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIActions::loadLevel()
extern "C"  void UIActions_loadLevel_m322202434 (UIActions_t467790635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
