﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0
struct U3CFadeAudioU3Ec__Iterator0_t1632642824;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::.ctor()
extern "C"  void U3CFadeAudioU3Ec__Iterator0__ctor_m2086390467 (U3CFadeAudioU3Ec__Iterator0_t1632642824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::MoveNext()
extern "C"  bool U3CFadeAudioU3Ec__Iterator0_MoveNext_m1134125121 (U3CFadeAudioU3Ec__Iterator0_t1632642824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFadeAudioU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4294932741 (U3CFadeAudioU3Ec__Iterator0_t1632642824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFadeAudioU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2487636221 (U3CFadeAudioU3Ec__Iterator0_t1632642824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::Dispose()
extern "C"  void U3CFadeAudioU3Ec__Iterator0_Dispose_m1214359246 (U3CFadeAudioU3Ec__Iterator0_t1632642824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.BackgroundMusic/<FadeAudio>c__Iterator0::Reset()
extern "C"  void U3CFadeAudioU3Ec__Iterator0_Reset_m2268700996 (U3CFadeAudioU3Ec__Iterator0_t1632642824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
