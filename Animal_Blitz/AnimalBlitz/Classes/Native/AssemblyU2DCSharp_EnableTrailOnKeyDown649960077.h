﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SpriteTrail
struct SpriteTrail_t2284883325;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnableTrailOnKeyDown
struct  EnableTrailOnKeyDown_t649960077  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.KeyCode EnableTrailOnKeyDown::m_EnableKey
	int32_t ___m_EnableKey_2;
	// UnityEngine.KeyCode EnableTrailOnKeyDown::m_DisableKey
	int32_t ___m_DisableKey_3;
	// SpriteTrail EnableTrailOnKeyDown::m_Trail
	SpriteTrail_t2284883325 * ___m_Trail_4;

public:
	inline static int32_t get_offset_of_m_EnableKey_2() { return static_cast<int32_t>(offsetof(EnableTrailOnKeyDown_t649960077, ___m_EnableKey_2)); }
	inline int32_t get_m_EnableKey_2() const { return ___m_EnableKey_2; }
	inline int32_t* get_address_of_m_EnableKey_2() { return &___m_EnableKey_2; }
	inline void set_m_EnableKey_2(int32_t value)
	{
		___m_EnableKey_2 = value;
	}

	inline static int32_t get_offset_of_m_DisableKey_3() { return static_cast<int32_t>(offsetof(EnableTrailOnKeyDown_t649960077, ___m_DisableKey_3)); }
	inline int32_t get_m_DisableKey_3() const { return ___m_DisableKey_3; }
	inline int32_t* get_address_of_m_DisableKey_3() { return &___m_DisableKey_3; }
	inline void set_m_DisableKey_3(int32_t value)
	{
		___m_DisableKey_3 = value;
	}

	inline static int32_t get_offset_of_m_Trail_4() { return static_cast<int32_t>(offsetof(EnableTrailOnKeyDown_t649960077, ___m_Trail_4)); }
	inline SpriteTrail_t2284883325 * get_m_Trail_4() const { return ___m_Trail_4; }
	inline SpriteTrail_t2284883325 ** get_address_of_m_Trail_4() { return &___m_Trail_4; }
	inline void set_m_Trail_4(SpriteTrail_t2284883325 * value)
	{
		___m_Trail_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_Trail_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
