﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/TriggerModule
struct TriggerModule_t2774521729;
struct TriggerModule_t2774521729_marshaled_pinvoke;
struct TriggerModule_t2774521729_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_TriggerModu2774521729.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/TriggerModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void TriggerModule__ctor_m317871043 (TriggerModule_t2774521729 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct TriggerModule_t2774521729;
struct TriggerModule_t2774521729_marshaled_pinvoke;

extern "C" void TriggerModule_t2774521729_marshal_pinvoke(const TriggerModule_t2774521729& unmarshaled, TriggerModule_t2774521729_marshaled_pinvoke& marshaled);
extern "C" void TriggerModule_t2774521729_marshal_pinvoke_back(const TriggerModule_t2774521729_marshaled_pinvoke& marshaled, TriggerModule_t2774521729& unmarshaled);
extern "C" void TriggerModule_t2774521729_marshal_pinvoke_cleanup(TriggerModule_t2774521729_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct TriggerModule_t2774521729;
struct TriggerModule_t2774521729_marshaled_com;

extern "C" void TriggerModule_t2774521729_marshal_com(const TriggerModule_t2774521729& unmarshaled, TriggerModule_t2774521729_marshaled_com& marshaled);
extern "C" void TriggerModule_t2774521729_marshal_com_back(const TriggerModule_t2774521729_marshaled_com& marshaled, TriggerModule_t2774521729& unmarshaled);
extern "C" void TriggerModule_t2774521729_marshal_com_cleanup(TriggerModule_t2774521729_marshaled_com& marshaled);
