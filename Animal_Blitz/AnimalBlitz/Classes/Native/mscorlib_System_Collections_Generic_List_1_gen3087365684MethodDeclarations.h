﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>
struct List_1_t3087365684;
// System.Collections.Generic.IEnumerator`1<HighlightingSystem.HighlighterRenderer/Data>
struct IEnumerator_1_t1193768379;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<HighlightingSystem.HighlighterRenderer/Data>
struct ICollection_1_t375352561;
// System.Collections.Generic.IEnumerable`1<HighlightingSystem.HighlighterRenderer/Data>
struct IEnumerable_1_t4010371597;
// System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlighterRenderer/Data>
struct ReadOnlyCollection_1_t3904030244;
// HighlightingSystem.HighlighterRenderer/Data[]
struct DataU5BU5D_t4234851097;
// System.Predicate`1<HighlightingSystem.HighlighterRenderer/Data>
struct Predicate_1_t2161214667;
// System.Collections.Generic.IComparer`1<HighlightingSystem.HighlighterRenderer/Data>
struct IComparer_1_t1672707674;
// System.Comparison`1<HighlightingSystem.HighlighterRenderer/Data>
struct Comparison_1_t685016107;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H3718244552.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2622095358.h"

// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::.ctor()
extern "C"  void List_1__ctor_m3100466721_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1__ctor_m3100466721(__this, method) ((  void (*) (List_1_t3087365684 *, const MethodInfo*))List_1__ctor_m3100466721_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1738888849_gshared (List_1_t3087365684 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1738888849(__this, ___capacity0, method) ((  void (*) (List_1_t3087365684 *, int32_t, const MethodInfo*))List_1__ctor_m1738888849_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::.cctor()
extern "C"  void List_1__cctor_m231225111_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m231225111(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m231225111_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1228918410_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1228918410(__this, method) ((  Il2CppObject* (*) (List_1_t3087365684 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1228918410_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2313094740_gshared (List_1_t3087365684 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2313094740(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3087365684 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2313094740_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3267465265_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3267465265(__this, method) ((  Il2CppObject * (*) (List_1_t3087365684 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3267465265_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1414967788_gshared (List_1_t3087365684 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1414967788(__this, ___item0, method) ((  int32_t (*) (List_1_t3087365684 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1414967788_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1784083114_gshared (List_1_t3087365684 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1784083114(__this, ___item0, method) ((  bool (*) (List_1_t3087365684 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1784083114_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1227433542_gshared (List_1_t3087365684 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1227433542(__this, ___item0, method) ((  int32_t (*) (List_1_t3087365684 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1227433542_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2381209697_gshared (List_1_t3087365684 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2381209697(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3087365684 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2381209697_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2935198177_gshared (List_1_t3087365684 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2935198177(__this, ___item0, method) ((  void (*) (List_1_t3087365684 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2935198177_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3841586037_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3841586037(__this, method) ((  bool (*) (List_1_t3087365684 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3841586037_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m905696564_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m905696564(__this, method) ((  bool (*) (List_1_t3087365684 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m905696564_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1579383732_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1579383732(__this, method) ((  Il2CppObject * (*) (List_1_t3087365684 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1579383732_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1178346661_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1178346661(__this, method) ((  bool (*) (List_1_t3087365684 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1178346661_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m215260904_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m215260904(__this, method) ((  bool (*) (List_1_t3087365684 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m215260904_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2755368989_gshared (List_1_t3087365684 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2755368989(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3087365684 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2755368989_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m739840290_gshared (List_1_t3087365684 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m739840290(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3087365684 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m739840290_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::Add(T)
extern "C"  void List_1_Add_m4144517861_gshared (List_1_t3087365684 * __this, Data_t3718244552  ___item0, const MethodInfo* method);
#define List_1_Add_m4144517861(__this, ___item0, method) ((  void (*) (List_1_t3087365684 *, Data_t3718244552 , const MethodInfo*))List_1_Add_m4144517861_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3447694606_gshared (List_1_t3087365684 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3447694606(__this, ___newCount0, method) ((  void (*) (List_1_t3087365684 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3447694606_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m704973886_gshared (List_1_t3087365684 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m704973886(__this, ___collection0, method) ((  void (*) (List_1_t3087365684 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m704973886_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2198775326_gshared (List_1_t3087365684 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2198775326(__this, ___enumerable0, method) ((  void (*) (List_1_t3087365684 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2198775326_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2296074929_gshared (List_1_t3087365684 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2296074929(__this, ___collection0, method) ((  void (*) (List_1_t3087365684 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2296074929_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3904030244 * List_1_AsReadOnly_m1590540128_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1590540128(__this, method) ((  ReadOnlyCollection_1_t3904030244 * (*) (List_1_t3087365684 *, const MethodInfo*))List_1_AsReadOnly_m1590540128_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::Clear()
extern "C"  void List_1_Clear_m3873295665_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_Clear_m3873295665(__this, method) ((  void (*) (List_1_t3087365684 *, const MethodInfo*))List_1_Clear_m3873295665_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::Contains(T)
extern "C"  bool List_1_Contains_m2550717223_gshared (List_1_t3087365684 * __this, Data_t3718244552  ___item0, const MethodInfo* method);
#define List_1_Contains_m2550717223(__this, ___item0, method) ((  bool (*) (List_1_t3087365684 *, Data_t3718244552 , const MethodInfo*))List_1_Contains_m2550717223_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2886177045_gshared (List_1_t3087365684 * __this, DataU5BU5D_t4234851097* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2886177045(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3087365684 *, DataU5BU5D_t4234851097*, int32_t, const MethodInfo*))List_1_CopyTo_m2886177045_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::Find(System.Predicate`1<T>)
extern "C"  Data_t3718244552  List_1_Find_m91231731_gshared (List_1_t3087365684 * __this, Predicate_1_t2161214667 * ___match0, const MethodInfo* method);
#define List_1_Find_m91231731(__this, ___match0, method) ((  Data_t3718244552  (*) (List_1_t3087365684 *, Predicate_1_t2161214667 *, const MethodInfo*))List_1_Find_m91231731_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2008260524_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2161214667 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2008260524(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2161214667 *, const MethodInfo*))List_1_CheckMatch_m2008260524_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3492253207_gshared (List_1_t3087365684 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2161214667 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3492253207(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3087365684 *, int32_t, int32_t, Predicate_1_t2161214667 *, const MethodInfo*))List_1_GetIndex_m3492253207_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::GetEnumerator()
extern "C"  Enumerator_t2622095358  List_1_GetEnumerator_m1876882860_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1876882860(__this, method) ((  Enumerator_t2622095358  (*) (List_1_t3087365684 *, const MethodInfo*))List_1_GetEnumerator_m1876882860_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2458255793_gshared (List_1_t3087365684 * __this, Data_t3718244552  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2458255793(__this, ___item0, method) ((  int32_t (*) (List_1_t3087365684 *, Data_t3718244552 , const MethodInfo*))List_1_IndexOf_m2458255793_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m4081902472_gshared (List_1_t3087365684 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m4081902472(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3087365684 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m4081902472_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3164309401_gshared (List_1_t3087365684 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3164309401(__this, ___index0, method) ((  void (*) (List_1_t3087365684 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3164309401_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3350086338_gshared (List_1_t3087365684 * __this, int32_t ___index0, Data_t3718244552  ___item1, const MethodInfo* method);
#define List_1_Insert_m3350086338(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3087365684 *, int32_t, Data_t3718244552 , const MethodInfo*))List_1_Insert_m3350086338_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1515502251_gshared (List_1_t3087365684 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1515502251(__this, ___collection0, method) ((  void (*) (List_1_t3087365684 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1515502251_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::Remove(T)
extern "C"  bool List_1_Remove_m2290363508_gshared (List_1_t3087365684 * __this, Data_t3718244552  ___item0, const MethodInfo* method);
#define List_1_Remove_m2290363508(__this, ___item0, method) ((  bool (*) (List_1_t3087365684 *, Data_t3718244552 , const MethodInfo*))List_1_Remove_m2290363508_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1230401676_gshared (List_1_t3087365684 * __this, Predicate_1_t2161214667 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1230401676(__this, ___match0, method) ((  int32_t (*) (List_1_t3087365684 *, Predicate_1_t2161214667 *, const MethodInfo*))List_1_RemoveAll_m1230401676_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3426566614_gshared (List_1_t3087365684 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3426566614(__this, ___index0, method) ((  void (*) (List_1_t3087365684 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3426566614_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::Reverse()
extern "C"  void List_1_Reverse_m1358341970_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_Reverse_m1358341970(__this, method) ((  void (*) (List_1_t3087365684 *, const MethodInfo*))List_1_Reverse_m1358341970_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::Sort()
extern "C"  void List_1_Sort_m2418702366_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_Sort_m2418702366(__this, method) ((  void (*) (List_1_t3087365684 *, const MethodInfo*))List_1_Sort_m2418702366_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1176128130_gshared (List_1_t3087365684 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1176128130(__this, ___comparer0, method) ((  void (*) (List_1_t3087365684 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1176128130_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2459713343_gshared (List_1_t3087365684 * __this, Comparison_1_t685016107 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2459713343(__this, ___comparison0, method) ((  void (*) (List_1_t3087365684 *, Comparison_1_t685016107 *, const MethodInfo*))List_1_Sort_m2459713343_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::ToArray()
extern "C"  DataU5BU5D_t4234851097* List_1_ToArray_m1767740439_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_ToArray_m1767740439(__this, method) ((  DataU5BU5D_t4234851097* (*) (List_1_t3087365684 *, const MethodInfo*))List_1_ToArray_m1767740439_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2347427221_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2347427221(__this, method) ((  void (*) (List_1_t3087365684 *, const MethodInfo*))List_1_TrimExcess_m2347427221_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1144287359_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1144287359(__this, method) ((  int32_t (*) (List_1_t3087365684 *, const MethodInfo*))List_1_get_Capacity_m1144287359_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3157453406_gshared (List_1_t3087365684 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3157453406(__this, ___value0, method) ((  void (*) (List_1_t3087365684 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3157453406_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::get_Count()
extern "C"  int32_t List_1_get_Count_m343165665_gshared (List_1_t3087365684 * __this, const MethodInfo* method);
#define List_1_get_Count_m343165665(__this, method) ((  int32_t (*) (List_1_t3087365684 *, const MethodInfo*))List_1_get_Count_m343165665_gshared)(__this, method)
// T System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::get_Item(System.Int32)
extern "C"  Data_t3718244552  List_1_get_Item_m1623153598_gshared (List_1_t3087365684 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1623153598(__this, ___index0, method) ((  Data_t3718244552  (*) (List_1_t3087365684 *, int32_t, const MethodInfo*))List_1_get_Item_m1623153598_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2020369461_gshared (List_1_t3087365684 * __this, int32_t ___index0, Data_t3718244552  ___value1, const MethodInfo* method);
#define List_1_set_Item_m2020369461(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3087365684 *, int32_t, Data_t3718244552 , const MethodInfo*))List_1_set_Item_m2020369461_gshared)(__this, ___index0, ___value1, method)
