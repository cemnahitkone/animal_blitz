﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m841808038(__this, ___l0, method) ((  void (*) (Enumerator_t3121096446 *, List_1_t3586366772 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2561635652(__this, method) ((  void (*) (Enumerator_t3121096446 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3613171506(__this, method) ((  Il2CppObject * (*) (Enumerator_t3121096446 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer>::Dispose()
#define Enumerator_Dispose_m3284038143(__this, method) ((  void (*) (Enumerator_t3121096446 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer>::VerifyState()
#define Enumerator_VerifyState_m2783296092(__this, method) ((  void (*) (Enumerator_t3121096446 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer>::MoveNext()
#define Enumerator_MoveNext_m3735124300(__this, method) ((  bool (*) (Enumerator_t3121096446 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer>::get_Current()
#define Enumerator_get_Current_m3519992939(__this, method) ((  HighlighterRenderer_t4217245640 * (*) (Enumerator_t3121096446 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
