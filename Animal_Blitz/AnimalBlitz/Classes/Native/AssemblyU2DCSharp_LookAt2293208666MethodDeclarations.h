﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LookAt
struct LookAt_t2293208666;

#include "codegen/il2cpp-codegen.h"

// System.Void LookAt::.ctor()
extern "C"  void LookAt__ctor_m1762265113 (LookAt_t2293208666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LookAt::Start()
extern "C"  void LookAt_Start_m2888736153 (LookAt_t2293208666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LookAt::Update()
extern "C"  void LookAt_Update_m1239339128 (LookAt_t2293208666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
