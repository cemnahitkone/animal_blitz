﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterSpin
struct CharacterSpin_t1044941751;

#include "codegen/il2cpp-codegen.h"

// System.Void CharacterSpin::.ctor()
extern "C"  void CharacterSpin__ctor_m901834203 (CharacterSpin_t1044941751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterSpin::Start()
extern "C"  void CharacterSpin_Start_m1628683855 (CharacterSpin_t1044941751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterSpin::Update()
extern "C"  void CharacterSpin_Update_m1963858220 (CharacterSpin_t1044941751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterSpin::Main()
extern "C"  void CharacterSpin_Main_m2315659268 (CharacterSpin_t1044941751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
