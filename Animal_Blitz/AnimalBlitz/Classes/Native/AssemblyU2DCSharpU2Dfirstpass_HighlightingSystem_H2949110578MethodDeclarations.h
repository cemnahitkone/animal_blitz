﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HighlightingSystem.HighlightingBlitter
struct HighlightingBlitter_t2949110578;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// HighlightingSystem.HighlightingBase
struct HighlightingBase_t336099813;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi336099813.h"

// System.Void HighlightingSystem.HighlightingBlitter::.ctor()
extern "C"  void HighlightingBlitter__ctor_m1718539186 (HighlightingBlitter_t2949110578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBlitter::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void HighlightingBlitter_OnRenderImage_m132934086 (HighlightingBlitter_t2949110578 * __this, RenderTexture_t2666733923 * ___src0, RenderTexture_t2666733923 * ___dst1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBlitter::Register(HighlightingSystem.HighlightingBase)
extern "C"  void HighlightingBlitter_Register_m2013352037 (HighlightingBlitter_t2949110578 * __this, HighlightingBase_t336099813 * ___renderer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlightingBlitter::Unregister(HighlightingSystem.HighlightingBase)
extern "C"  void HighlightingBlitter_Unregister_m3454378352 (HighlightingBlitter_t2949110578 * __this, HighlightingBase_t336099813 * ___renderer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
