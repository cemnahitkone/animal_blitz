﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Single,System.Object>
struct GetEnumeratorU3Ec__Iterator2_t2863832183;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Single,System.Object>::.ctor()
extern "C"  void GetEnumeratorU3Ec__Iterator2__ctor_m244207875_gshared (GetEnumeratorU3Ec__Iterator2_t2863832183 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2__ctor_m244207875(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator2_t2863832183 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2__ctor_m244207875_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Single,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * GetEnumeratorU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2057062525_gshared (GetEnumeratorU3Ec__Iterator2_t2863832183 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2057062525(__this, method) ((  Il2CppObject * (*) (GetEnumeratorU3Ec__Iterator2_t2863832183 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2057062525_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Single,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * GetEnumeratorU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2960088821_gshared (GetEnumeratorU3Ec__Iterator2_t2863832183 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2960088821(__this, method) ((  Il2CppObject * (*) (GetEnumeratorU3Ec__Iterator2_t2863832183 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2960088821_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Single,System.Object>::MoveNext()
extern "C"  bool GetEnumeratorU3Ec__Iterator2_MoveNext_m2902178889_gshared (GetEnumeratorU3Ec__Iterator2_t2863832183 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2_MoveNext_m2902178889(__this, method) ((  bool (*) (GetEnumeratorU3Ec__Iterator2_t2863832183 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2_MoveNext_m2902178889_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Single,System.Object>::Dispose()
extern "C"  void GetEnumeratorU3Ec__Iterator2_Dispose_m1151876192_gshared (GetEnumeratorU3Ec__Iterator2_t2863832183 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2_Dispose_m1151876192(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator2_t2863832183 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2_Dispose_m1151876192_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Single,System.Object>::Reset()
extern "C"  void GetEnumeratorU3Ec__Iterator2_Reset_m3093838378_gshared (GetEnumeratorU3Ec__Iterator2_t2863832183 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2_Reset_m3093838378(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator2_t2863832183 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2_Reset_m3093838378_gshared)(__this, method)
