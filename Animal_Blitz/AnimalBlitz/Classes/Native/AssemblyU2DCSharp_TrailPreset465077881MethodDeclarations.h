﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TrailPreset
struct TrailPreset_t465077881;

#include "codegen/il2cpp-codegen.h"

// System.Void TrailPreset::.ctor()
extern "C"  void TrailPreset__ctor_m2349883794 (TrailPreset_t465077881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
