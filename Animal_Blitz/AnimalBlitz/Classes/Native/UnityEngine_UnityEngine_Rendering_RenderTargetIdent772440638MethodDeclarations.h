﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Texture
struct Texture_t2243626319;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rendering_RenderTargetIdent772440638.h"
#include "UnityEngine_UnityEngine_Rendering_BuiltinRenderText195473098.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"

// System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(UnityEngine.Rendering.BuiltinRenderTextureType)
extern "C"  void RenderTargetIdentifier__ctor_m607511440 (RenderTargetIdentifier_t772440638 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(System.Int32)
extern "C"  void RenderTargetIdentifier__ctor_m1216379536 (RenderTargetIdentifier_t772440638 * __this, int32_t ___nameID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(UnityEngine.Texture)
extern "C"  void RenderTargetIdentifier__ctor_m957202749 (RenderTargetIdentifier_t772440638 * __this, Texture_t2243626319 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
