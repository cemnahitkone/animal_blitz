﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_Helper2830697524MethodDeclarations.h"

// System.Void Borodar.FarlandSkies.Core.Helpers.Singleton`1<Borodar.FarlandSkies.LowPoly.SkyboxCycleManager>::.ctor()
#define Singleton_1__ctor_m2005116848(__this, method) ((  void (*) (Singleton_1_t203123456 *, const MethodInfo*))Singleton_1__ctor_m3543112861_gshared)(__this, method)
// T Borodar.FarlandSkies.Core.Helpers.Singleton`1<Borodar.FarlandSkies.LowPoly.SkyboxCycleManager>::get_Instance()
#define Singleton_1_get_Instance_m2982959757(__this /* static, unused */, method) ((  SkyboxCycleManager_t61875227 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_Instance_m4226036052_gshared)(__this /* static, unused */, method)
