﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterControl
struct CharacterControl_t3164190352;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void CharacterControl::.ctor()
extern "C"  void CharacterControl__ctor_m3708774629 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::Awake()
extern "C"  void CharacterControl_Awake_m41050576 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::Start()
extern "C"  void CharacterControl_Start_m1175716765 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::FixedUpdate()
extern "C"  void CharacterControl_FixedUpdate_m4020304480 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::playAnimations()
extern "C"  void CharacterControl_playAnimations_m1483090562 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::Update()
extern "C"  void CharacterControl_Update_m2179612062 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::successLevel()
extern "C"  void CharacterControl_successLevel_m561962560 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::getOut()
extern "C"  void CharacterControl_getOut_m2163327113 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::BlendAllToZero(UnityEngine.GameObject)
extern "C"  void CharacterControl_BlendAllToZero_m3213993396 (CharacterControl_t3164190352 * __this, GameObject_t1756533147 * ___character0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::destroyDog()
extern "C"  void CharacterControl_destroyDog_m429917371 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::destroyCat()
extern "C"  void CharacterControl_destroyCat_m403324461 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::destroyOwl()
extern "C"  void CharacterControl_destroyOwl_m1208343859 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::destroyTurtle()
extern "C"  void CharacterControl_destroyTurtle_m512107341 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::destroyRat()
extern "C"  void CharacterControl_destroyRat_m541576930 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::DogIdle2()
extern "C"  void CharacterControl_DogIdle2_m2972929539 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::CatIdle2()
extern "C"  void CharacterControl_CatIdle2_m1575434529 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::OwlIdle2()
extern "C"  void CharacterControl_OwlIdle2_m3913293883 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::RatIdle2()
extern "C"  void CharacterControl_RatIdle2_m1558726002 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::TurtleIdle2()
extern "C"  void CharacterControl_TurtleIdle2_m664803525 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterControl::startRecording()
extern "C"  void CharacterControl_startRecording_m1780954600 (CharacterControl_t3164190352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
