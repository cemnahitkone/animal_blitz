﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/TrailModule
struct TrailModule_t3782588419;
struct TrailModule_t3782588419_marshaled_pinvoke;
struct TrailModule_t3782588419_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_TrailModule3782588419.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/TrailModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void TrailModule__ctor_m4120830221 (TrailModule_t3782588419 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct TrailModule_t3782588419;
struct TrailModule_t3782588419_marshaled_pinvoke;

extern "C" void TrailModule_t3782588419_marshal_pinvoke(const TrailModule_t3782588419& unmarshaled, TrailModule_t3782588419_marshaled_pinvoke& marshaled);
extern "C" void TrailModule_t3782588419_marshal_pinvoke_back(const TrailModule_t3782588419_marshaled_pinvoke& marshaled, TrailModule_t3782588419& unmarshaled);
extern "C" void TrailModule_t3782588419_marshal_pinvoke_cleanup(TrailModule_t3782588419_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct TrailModule_t3782588419;
struct TrailModule_t3782588419_marshaled_com;

extern "C" void TrailModule_t3782588419_marshal_com(const TrailModule_t3782588419& unmarshaled, TrailModule_t3782588419_marshaled_com& marshaled);
extern "C" void TrailModule_t3782588419_marshal_com_back(const TrailModule_t3782588419_marshaled_com& marshaled, TrailModule_t3782588419& unmarshaled);
extern "C" void TrailModule_t3782588419_marshal_com_cleanup(TrailModule_t3782588419_marshaled_com& marshaled);
