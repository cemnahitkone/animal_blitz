﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_DotPar3911697296.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParamsList
struct  CelestialParamsList_t3739272824  : public SortedParamsList_1_t3911697296
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
