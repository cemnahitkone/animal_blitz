﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.IO.TextReader
struct TextReader_t1561828458;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_TextReader1561828458.h"

// System.Collections.Generic.IEnumerable`1<System.String> Boo.Lang.Runtime.TextReaderEnumerator::lines(System.IO.TextReader)
extern "C"  Il2CppObject* TextReaderEnumerator_lines_m2873834955 (Il2CppObject * __this /* static, unused */, TextReader_t1561828458 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
