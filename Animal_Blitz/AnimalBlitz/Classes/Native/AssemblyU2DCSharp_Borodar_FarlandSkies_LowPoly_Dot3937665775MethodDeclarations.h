﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam
struct CloudsParam_t3937665775;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam::.ctor()
extern "C"  void CloudsParam__ctor_m2656896361 (CloudsParam_t3937665775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
