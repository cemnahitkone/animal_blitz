﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangeTrailPreset
struct ChangeTrailPreset_t1471293943;

#include "codegen/il2cpp-codegen.h"

// System.Void ChangeTrailPreset::.ctor()
extern "C"  void ChangeTrailPreset__ctor_m418651686 (ChangeTrailPreset_t1471293943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeTrailPreset::Update()
extern "C"  void ChangeTrailPreset_Update_m1233564699 (ChangeTrailPreset_t1471293943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeTrailPreset::NextPreset()
extern "C"  void ChangeTrailPreset_NextPreset_m3796283610 (ChangeTrailPreset_t1471293943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeTrailPreset::PreviousPreset()
extern "C"  void ChangeTrailPreset_PreviousPreset_m1470636566 (ChangeTrailPreset_t1471293943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeTrailPreset::SetPresetIndex(System.Int32)
extern "C"  void ChangeTrailPreset_SetPresetIndex_m2571866872 (ChangeTrailPreset_t1471293943 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
