﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam
struct CelestialParam_t4052651973;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.DotParams.CelestialParam::.ctor()
extern "C"  void CelestialParam__ctor_m2566804371 (CelestialParam_t4052651973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
