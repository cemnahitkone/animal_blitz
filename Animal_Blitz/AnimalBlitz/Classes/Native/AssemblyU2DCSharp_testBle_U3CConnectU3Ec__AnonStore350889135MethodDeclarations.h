﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// testBle/<Connect>c__AnonStorey0
struct U3CConnectU3Ec__AnonStorey0_t350889135;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void testBle/<Connect>c__AnonStorey0::.ctor()
extern "C"  void U3CConnectU3Ec__AnonStorey0__ctor_m3799893872 (U3CConnectU3Ec__AnonStorey0_t350889135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testBle/<Connect>c__AnonStorey0::<>m__0(System.String,System.String,System.Byte[])
extern "C"  void U3CConnectU3Ec__AnonStorey0_U3CU3Em__0_m4203433414 (U3CConnectU3Ec__AnonStorey0_t350889135 * __this, String_t* ___deviceAddress20, String_t* ___characteristic1, ByteU5BU5D_t3397334013* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
