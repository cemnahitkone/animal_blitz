﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen3777177449MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<UnityEngine.GameObject>::.ctor()
#define Stack_1__ctor_m2347528477(__this, method) ((  void (*) (Stack_1_t2844261301 *, const MethodInfo*))Stack_1__ctor_m1041657164_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<UnityEngine.GameObject>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m4234350714(__this, method) ((  bool (*) (Stack_1_t2844261301 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m2076161108_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<UnityEngine.GameObject>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m4008395606(__this, method) ((  Il2CppObject * (*) (Stack_1_t2844261301 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m3151629354_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<UnityEngine.GameObject>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m1992487926(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t2844261301 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m2104527616_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<UnityEngine.GameObject>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m887698976(__this, method) ((  Il2CppObject* (*) (Stack_1_t2844261301 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m680979874_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<UnityEngine.GameObject>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m3905869141(__this, method) ((  Il2CppObject * (*) (Stack_1_t2844261301 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m3875192475_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<UnityEngine.GameObject>::Contains(T)
#define Stack_1_Contains_m1061544927(__this, ___t0, method) ((  bool (*) (Stack_1_t2844261301 *, GameObject_t1756533147 *, const MethodInfo*))Stack_1_Contains_m973625077_gshared)(__this, ___t0, method)
// T System.Collections.Generic.Stack`1<UnityEngine.GameObject>::Peek()
#define Stack_1_Peek_m1767431202(__this, method) ((  GameObject_t1756533147 * (*) (Stack_1_t2844261301 *, const MethodInfo*))Stack_1_Peek_m1548778538_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<UnityEngine.GameObject>::Pop()
#define Stack_1_Pop_m2665221936(__this, method) ((  GameObject_t1756533147 * (*) (Stack_1_t2844261301 *, const MethodInfo*))Stack_1_Pop_m535185982_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<UnityEngine.GameObject>::Push(T)
#define Stack_1_Push_m3004012536(__this, ___t0, method) ((  void (*) (Stack_1_t2844261301 *, GameObject_t1756533147 *, const MethodInfo*))Stack_1_Push_m2122392216_gshared)(__this, ___t0, method)
// System.Int32 System.Collections.Generic.Stack`1<UnityEngine.GameObject>::get_Count()
#define Stack_1_get_Count_m335565545(__this, method) ((  int32_t (*) (Stack_1_t2844261301 *, const MethodInfo*))Stack_1_get_Count_m4101767244_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<UnityEngine.GameObject>::GetEnumerator()
#define Stack_1_GetEnumerator_m1958514232(__this, method) ((  Enumerator_t3494259661  (*) (Stack_1_t2844261301 *, const MethodInfo*))Stack_1_GetEnumerator_m287848754_gshared)(__this, method)
