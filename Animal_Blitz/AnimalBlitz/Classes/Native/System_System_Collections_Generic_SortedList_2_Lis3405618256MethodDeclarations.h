﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Single,System.Object>
struct GetEnumeratorU3Ec__Iterator3_t3405618256;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Single,System.Object>::.ctor()
extern "C"  void GetEnumeratorU3Ec__Iterator3__ctor_m860862820_gshared (GetEnumeratorU3Ec__Iterator3_t3405618256 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3__ctor_m860862820(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator3_t3405618256 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3__ctor_m860862820_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Single,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * GetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m486695886_gshared (GetEnumeratorU3Ec__Iterator3_t3405618256 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m486695886(__this, method) ((  Il2CppObject * (*) (GetEnumeratorU3Ec__Iterator3_t3405618256 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m486695886_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Single,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * GetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3827528134_gshared (GetEnumeratorU3Ec__Iterator3_t3405618256 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3827528134(__this, method) ((  Il2CppObject * (*) (GetEnumeratorU3Ec__Iterator3_t3405618256 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3827528134_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Single,System.Object>::MoveNext()
extern "C"  bool GetEnumeratorU3Ec__Iterator3_MoveNext_m1611680470_gshared (GetEnumeratorU3Ec__Iterator3_t3405618256 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_MoveNext_m1611680470(__this, method) ((  bool (*) (GetEnumeratorU3Ec__Iterator3_t3405618256 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_MoveNext_m1611680470_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Single,System.Object>::Dispose()
extern "C"  void GetEnumeratorU3Ec__Iterator3_Dispose_m4022464025_gshared (GetEnumeratorU3Ec__Iterator3_t3405618256 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_Dispose_m4022464025(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator3_t3405618256 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_Dispose_m4022464025_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Single,System.Object>::Reset()
extern "C"  void GetEnumeratorU3Ec__Iterator3_Reset_m679330067_gshared (GetEnumeratorU3Ec__Iterator3_t3405618256 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_Reset_m679330067(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator3_t3405618256 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_Reset_m679330067_gshared)(__this, method)
