﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnableTrailOnKeyDown
struct EnableTrailOnKeyDown_t649960077;

#include "codegen/il2cpp-codegen.h"

// System.Void EnableTrailOnKeyDown::.ctor()
extern "C"  void EnableTrailOnKeyDown__ctor_m2120757906 (EnableTrailOnKeyDown_t649960077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnableTrailOnKeyDown::Update()
extern "C"  void EnableTrailOnKeyDown_Update_m898738993 (EnableTrailOnKeyDown_t649960077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
