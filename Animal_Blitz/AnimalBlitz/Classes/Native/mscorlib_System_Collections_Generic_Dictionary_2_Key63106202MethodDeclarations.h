﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t1668570060;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key63106202.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m517552420_gshared (Enumerator_t63106202 * __this, Dictionary_2_t1668570060 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m517552420(__this, ___host0, method) ((  void (*) (Enumerator_t63106202 *, Dictionary_2_t1668570060 *, const MethodInfo*))Enumerator__ctor_m517552420_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2298909315_gshared (Enumerator_t63106202 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2298909315(__this, method) ((  Il2CppObject * (*) (Enumerator_t63106202 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2298909315_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1865640495_gshared (Enumerator_t63106202 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1865640495(__this, method) ((  void (*) (Enumerator_t63106202 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1865640495_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m442447088_gshared (Enumerator_t63106202 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m442447088(__this, method) ((  void (*) (Enumerator_t63106202 *, const MethodInfo*))Enumerator_Dispose_m442447088_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2601949291_gshared (Enumerator_t63106202 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2601949291(__this, method) ((  bool (*) (Enumerator_t63106202 *, const MethodInfo*))Enumerator_MoveNext_m2601949291_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m735582309_gshared (Enumerator_t63106202 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m735582309(__this, method) ((  Il2CppObject * (*) (Enumerator_t63106202 *, const MethodInfo*))Enumerator_get_Current_m735582309_gshared)(__this, method)
