﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_AutodestructWhenNoChildren
struct CFX_AutodestructWhenNoChildren_t3972162727;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_AutodestructWhenNoChildren::.ctor()
extern "C"  void CFX_AutodestructWhenNoChildren__ctor_m784963136 (CFX_AutodestructWhenNoChildren_t3972162727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_AutodestructWhenNoChildren::Update()
extern "C"  void CFX_AutodestructWhenNoChildren_Update_m3192207431 (CFX_AutodestructWhenNoChildren_t3972162727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
