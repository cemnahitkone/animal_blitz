﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.TimeText
struct  TimeText_t2647012832  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text Borodar.FarlandSkies.LowPoly.TimeText::_text
	Text_t356221433 * ____text_2;

public:
	inline static int32_t get_offset_of__text_2() { return static_cast<int32_t>(offsetof(TimeText_t2647012832, ____text_2)); }
	inline Text_t356221433 * get__text_2() const { return ____text_2; }
	inline Text_t356221433 ** get_address_of__text_2() { return &____text_2; }
	inline void set__text_2(Text_t356221433 * value)
	{
		____text_2 = value;
		Il2CppCodeGenWriteBarrier(&____text_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
