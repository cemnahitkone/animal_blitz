﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUIElement
struct GUIElement_t3381083099;
// UnityEngine.Camera
struct Camera_t189460977;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_GUIElement3381083099.h"

// UnityEngine.Rect UnityEngine.GUIElement::GetScreenRect(UnityEngine.Camera)
extern "C"  Rect_t3681755626  GUIElement_GetScreenRect_m2408036750 (GUIElement_t3381083099 * __this, Camera_t189460977 * ___camera0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIElement::INTERNAL_CALL_GetScreenRect(UnityEngine.GUIElement,UnityEngine.Camera,UnityEngine.Rect&)
extern "C"  void GUIElement_INTERNAL_CALL_GetScreenRect_m1163453080 (Il2CppObject * __this /* static, unused */, GUIElement_t3381083099 * ___self0, Camera_t189460977 * ___camera1, Rect_t3681755626 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
