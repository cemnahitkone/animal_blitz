﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.MoonParam>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m4159740987(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t673722395 *, float, MoonParam_t2066856486 *, const MethodInfo*))KeyValuePair_2__ctor_m303434200_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.MoonParam>::get_Key()
#define KeyValuePair_2_get_Key_m3689445453(__this, method) ((  float (*) (KeyValuePair_2_t673722395 *, const MethodInfo*))KeyValuePair_2_get_Key_m4249127398_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.MoonParam>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m4292568928(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t673722395 *, float, const MethodInfo*))KeyValuePair_2_set_Key_m448823159_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.MoonParam>::get_Value()
#define KeyValuePair_2_get_Value_m3896621941(__this, method) ((  MoonParam_t2066856486 * (*) (KeyValuePair_2_t673722395 *, const MethodInfo*))KeyValuePair_2_get_Value_m1014938790_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.MoonParam>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3058313624(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t673722395 *, MoonParam_t2066856486 *, const MethodInfo*))KeyValuePair_2_set_Value_m1070856647_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.MoonParam>::ToString()
#define KeyValuePair_2_ToString_m3495482954(__this, method) ((  String_t* (*) (KeyValuePair_2_t673722395 *, const MethodInfo*))KeyValuePair_2_ToString_m4220912861_gshared)(__this, method)
