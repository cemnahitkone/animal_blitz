﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Ricimi.BackgroundMusic
struct BackgroundMusic_t2182474183;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.MuteBackgroundMusic
struct  MuteBackgroundMusic_t1333099764  : public MonoBehaviour_t1158329972
{
public:
	// Ricimi.BackgroundMusic Ricimi.MuteBackgroundMusic::m_bgMusic
	BackgroundMusic_t2182474183 * ___m_bgMusic_2;

public:
	inline static int32_t get_offset_of_m_bgMusic_2() { return static_cast<int32_t>(offsetof(MuteBackgroundMusic_t1333099764, ___m_bgMusic_2)); }
	inline BackgroundMusic_t2182474183 * get_m_bgMusic_2() const { return ___m_bgMusic_2; }
	inline BackgroundMusic_t2182474183 ** get_address_of_m_bgMusic_2() { return &___m_bgMusic_2; }
	inline void set_m_bgMusic_2(BackgroundMusic_t2182474183 * value)
	{
		___m_bgMusic_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_bgMusic_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
