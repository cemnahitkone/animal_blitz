﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>
struct List_1_t3087365684;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2622095358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H3718244552.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1031753825_gshared (Enumerator_t2622095358 * __this, List_1_t3087365684 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m1031753825(__this, ___l0, method) ((  void (*) (Enumerator_t2622095358 *, List_1_t3087365684 *, const MethodInfo*))Enumerator__ctor_m1031753825_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2479734001_gshared (Enumerator_t2622095358 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2479734001(__this, method) ((  void (*) (Enumerator_t2622095358 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2479734001_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m632736369_gshared (Enumerator_t2622095358 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m632736369(__this, method) ((  Il2CppObject * (*) (Enumerator_t2622095358 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m632736369_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::Dispose()
extern "C"  void Enumerator_Dispose_m1865068628_gshared (Enumerator_t2622095358 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1865068628(__this, method) ((  void (*) (Enumerator_t2622095358 *, const MethodInfo*))Enumerator_Dispose_m1865068628_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1693975187_gshared (Enumerator_t2622095358 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1693975187(__this, method) ((  void (*) (Enumerator_t2622095358 *, const MethodInfo*))Enumerator_VerifyState_m1693975187_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2119751617_gshared (Enumerator_t2622095358 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2119751617(__this, method) ((  bool (*) (Enumerator_t2622095358 *, const MethodInfo*))Enumerator_MoveNext_m2119751617_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::get_Current()
extern "C"  Data_t3718244552  Enumerator_get_Current_m4234946358_gshared (Enumerator_t2622095358 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4234946358(__this, method) ((  Data_t3718244552  (*) (Enumerator_t2622095358 *, const MethodInfo*))Enumerator_get_Current_m4234946358_gshared)(__this, method)
