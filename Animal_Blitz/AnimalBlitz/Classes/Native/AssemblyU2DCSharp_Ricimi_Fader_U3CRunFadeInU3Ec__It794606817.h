﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Ricimi.Fader
struct Fader_t3816812854;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.Fader/<RunFadeIn>c__Iterator0
struct  U3CRunFadeInU3Ec__Iterator0_t794606817  : public Il2CppObject
{
public:
	// System.Single Ricimi.Fader/<RunFadeIn>c__Iterator0::<time>__0
	float ___U3CtimeU3E__0_0;
	// System.Single Ricimi.Fader/<RunFadeIn>c__Iterator0::<initialAlpha>__1
	float ___U3CinitialAlphaU3E__1_1;
	// Ricimi.Fader Ricimi.Fader/<RunFadeIn>c__Iterator0::$this
	Fader_t3816812854 * ___U24this_2;
	// System.Object Ricimi.Fader/<RunFadeIn>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean Ricimi.Fader/<RunFadeIn>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Ricimi.Fader/<RunFadeIn>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CtimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRunFadeInU3Ec__Iterator0_t794606817, ___U3CtimeU3E__0_0)); }
	inline float get_U3CtimeU3E__0_0() const { return ___U3CtimeU3E__0_0; }
	inline float* get_address_of_U3CtimeU3E__0_0() { return &___U3CtimeU3E__0_0; }
	inline void set_U3CtimeU3E__0_0(float value)
	{
		___U3CtimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CinitialAlphaU3E__1_1() { return static_cast<int32_t>(offsetof(U3CRunFadeInU3Ec__Iterator0_t794606817, ___U3CinitialAlphaU3E__1_1)); }
	inline float get_U3CinitialAlphaU3E__1_1() const { return ___U3CinitialAlphaU3E__1_1; }
	inline float* get_address_of_U3CinitialAlphaU3E__1_1() { return &___U3CinitialAlphaU3E__1_1; }
	inline void set_U3CinitialAlphaU3E__1_1(float value)
	{
		___U3CinitialAlphaU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CRunFadeInU3Ec__Iterator0_t794606817, ___U24this_2)); }
	inline Fader_t3816812854 * get_U24this_2() const { return ___U24this_2; }
	inline Fader_t3816812854 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Fader_t3816812854 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CRunFadeInU3Ec__Iterator0_t794606817, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CRunFadeInU3Ec__Iterator0_t794606817, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CRunFadeInU3Ec__Iterator0_t794606817, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
