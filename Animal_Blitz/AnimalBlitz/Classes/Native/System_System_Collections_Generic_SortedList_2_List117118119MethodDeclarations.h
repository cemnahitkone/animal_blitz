﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>
struct ListValues_t117118119;
// System.Collections.Generic.SortedList`2<System.Single,System.Object>
struct SortedList_2_t2155713151;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"

// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C"  void ListValues__ctor_m935935624_gshared (ListValues_t117118119 * __this, SortedList_2_t2155713151 * ___host0, const MethodInfo* method);
#define ListValues__ctor_m935935624(__this, ___host0, method) ((  void (*) (ListValues_t117118119 *, SortedList_2_t2155713151 *, const MethodInfo*))ListValues__ctor_m935935624_gshared)(__this, ___host0, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ListValues_System_Collections_IEnumerable_GetEnumerator_m2600495623_gshared (ListValues_t117118119 * __this, const MethodInfo* method);
#define ListValues_System_Collections_IEnumerable_GetEnumerator_m2600495623(__this, method) ((  Il2CppObject * (*) (ListValues_t117118119 *, const MethodInfo*))ListValues_System_Collections_IEnumerable_GetEnumerator_m2600495623_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::Add(TValue)
extern "C"  void ListValues_Add_m474412960_gshared (ListValues_t117118119 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListValues_Add_m474412960(__this, ___item0, method) ((  void (*) (ListValues_t117118119 *, Il2CppObject *, const MethodInfo*))ListValues_Add_m474412960_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::Remove(TValue)
extern "C"  bool ListValues_Remove_m3943551431_gshared (ListValues_t117118119 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ListValues_Remove_m3943551431(__this, ___value0, method) ((  bool (*) (ListValues_t117118119 *, Il2CppObject *, const MethodInfo*))ListValues_Remove_m3943551431_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::Clear()
extern "C"  void ListValues_Clear_m2715504255_gshared (ListValues_t117118119 * __this, const MethodInfo* method);
#define ListValues_Clear_m2715504255(__this, method) ((  void (*) (ListValues_t117118119 *, const MethodInfo*))ListValues_Clear_m2715504255_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ListValues_CopyTo_m1519998728_gshared (ListValues_t117118119 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ListValues_CopyTo_m1519998728(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ListValues_t117118119 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ListValues_CopyTo_m1519998728_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::Contains(TValue)
extern "C"  bool ListValues_Contains_m2185223464_gshared (ListValues_t117118119 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListValues_Contains_m2185223464(__this, ___item0, method) ((  bool (*) (ListValues_t117118119 *, Il2CppObject *, const MethodInfo*))ListValues_Contains_m2185223464_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::IndexOf(TValue)
extern "C"  int32_t ListValues_IndexOf_m912310602_gshared (ListValues_t117118119 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListValues_IndexOf_m912310602(__this, ___item0, method) ((  int32_t (*) (ListValues_t117118119 *, Il2CppObject *, const MethodInfo*))ListValues_IndexOf_m912310602_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::Insert(System.Int32,TValue)
extern "C"  void ListValues_Insert_m2800398007_gshared (ListValues_t117118119 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define ListValues_Insert_m2800398007(__this, ___index0, ___item1, method) ((  void (*) (ListValues_t117118119 *, int32_t, Il2CppObject *, const MethodInfo*))ListValues_Insert_m2800398007_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::RemoveAt(System.Int32)
extern "C"  void ListValues_RemoveAt_m3422449238_gshared (ListValues_t117118119 * __this, int32_t ___index0, const MethodInfo* method);
#define ListValues_RemoveAt_m3422449238(__this, ___index0, method) ((  void (*) (ListValues_t117118119 *, int32_t, const MethodInfo*))ListValues_RemoveAt_m3422449238_gshared)(__this, ___index0, method)
// TValue System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * ListValues_get_Item_m2420914039_gshared (ListValues_t117118119 * __this, int32_t ___index0, const MethodInfo* method);
#define ListValues_get_Item_m2420914039(__this, ___index0, method) ((  Il2CppObject * (*) (ListValues_t117118119 *, int32_t, const MethodInfo*))ListValues_get_Item_m2420914039_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::set_Item(System.Int32,TValue)
extern "C"  void ListValues_set_Item_m2923316034_gshared (ListValues_t117118119 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ListValues_set_Item_m2923316034(__this, ___index0, ___value1, method) ((  void (*) (ListValues_t117118119 *, int32_t, Il2CppObject *, const MethodInfo*))ListValues_set_Item_m2923316034_gshared)(__this, ___index0, ___value1, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ListValues_GetEnumerator_m1529401965_gshared (ListValues_t117118119 * __this, const MethodInfo* method);
#define ListValues_GetEnumerator_m1529401965(__this, method) ((  Il2CppObject* (*) (ListValues_t117118119 *, const MethodInfo*))ListValues_GetEnumerator_m1529401965_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::get_Count()
extern "C"  int32_t ListValues_get_Count_m895649216_gshared (ListValues_t117118119 * __this, const MethodInfo* method);
#define ListValues_get_Count_m895649216(__this, method) ((  int32_t (*) (ListValues_t117118119 *, const MethodInfo*))ListValues_get_Count_m895649216_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::get_IsSynchronized()
extern "C"  bool ListValues_get_IsSynchronized_m3653398499_gshared (ListValues_t117118119 * __this, const MethodInfo* method);
#define ListValues_get_IsSynchronized_m3653398499(__this, method) ((  bool (*) (ListValues_t117118119 *, const MethodInfo*))ListValues_get_IsSynchronized_m3653398499_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::get_IsReadOnly()
extern "C"  bool ListValues_get_IsReadOnly_m1710235659_gshared (ListValues_t117118119 * __this, const MethodInfo* method);
#define ListValues_get_IsReadOnly_m1710235659(__this, method) ((  bool (*) (ListValues_t117118119 *, const MethodInfo*))ListValues_get_IsReadOnly_m1710235659_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::get_SyncRoot()
extern "C"  Il2CppObject * ListValues_get_SyncRoot_m3484599371_gshared (ListValues_t117118119 * __this, const MethodInfo* method);
#define ListValues_get_SyncRoot_m3484599371(__this, method) ((  Il2CppObject * (*) (ListValues_t117118119 *, const MethodInfo*))ListValues_get_SyncRoot_m3484599371_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Single,System.Object>::CopyTo(System.Array,System.Int32)
extern "C"  void ListValues_CopyTo_m3999471627_gshared (ListValues_t117118119 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ListValues_CopyTo_m3999471627(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ListValues_t117118119 *, Il2CppArray *, int32_t, const MethodInfo*))ListValues_CopyTo_m3999471627_gshared)(__this, ___array0, ___arrayIndex1, method)
