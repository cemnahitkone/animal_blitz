﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.Core.DotParams.DotParam
struct  DotParam_t3767082706  : public Il2CppObject
{
public:
	// System.Single Borodar.FarlandSkies.Core.DotParams.DotParam::Time
	float ___Time_0;

public:
	inline static int32_t get_offset_of_Time_0() { return static_cast<int32_t>(offsetof(DotParam_t3767082706, ___Time_0)); }
	inline float get_Time_0() const { return ___Time_0; }
	inline float* get_address_of_Time_0() { return &___Time_0; }
	inline void set_Time_0(float value)
	{
		___Time_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
