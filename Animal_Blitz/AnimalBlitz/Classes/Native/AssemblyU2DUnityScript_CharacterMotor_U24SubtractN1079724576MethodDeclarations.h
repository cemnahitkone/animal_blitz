﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterMotor/$SubtractNewPlatformVelocity$19/$
struct U24_t1079724576;
// CharacterMotor
struct CharacterMotor_t262030084;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_CharacterMotor262030084.h"

// System.Void CharacterMotor/$SubtractNewPlatformVelocity$19/$::.ctor(CharacterMotor)
extern "C"  void U24__ctor_m3521653002 (U24_t1079724576 * __this, CharacterMotor_t262030084 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CharacterMotor/$SubtractNewPlatformVelocity$19/$::MoveNext()
extern "C"  bool U24_MoveNext_m2114066784 (U24_t1079724576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
