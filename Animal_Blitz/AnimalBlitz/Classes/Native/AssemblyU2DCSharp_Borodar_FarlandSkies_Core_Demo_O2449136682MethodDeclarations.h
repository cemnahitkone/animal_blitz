﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.Core.Demo.ObjectRotator
struct ObjectRotator_t2449136682;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.Core.Demo.ObjectRotator::.ctor()
extern "C"  void ObjectRotator__ctor_m1284170633 (ObjectRotator_t2449136682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.Core.Demo.ObjectRotator::Update()
extern "C"  void ObjectRotator_Update_m821593656 (ObjectRotator_t2449136682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
