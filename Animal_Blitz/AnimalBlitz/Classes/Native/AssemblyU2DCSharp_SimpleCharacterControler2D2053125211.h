﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleCharacterControler2D
struct  SimpleCharacterControler2D_t2053125211  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Rigidbody2D SimpleCharacterControler2D::m_RigidBody
	Rigidbody2D_t502193897 * ___m_RigidBody_2;
	// System.Single SimpleCharacterControler2D::m_Force
	float ___m_Force_3;
	// System.Single SimpleCharacterControler2D::m_MaxVelocityToBoost
	float ___m_MaxVelocityToBoost_4;

public:
	inline static int32_t get_offset_of_m_RigidBody_2() { return static_cast<int32_t>(offsetof(SimpleCharacterControler2D_t2053125211, ___m_RigidBody_2)); }
	inline Rigidbody2D_t502193897 * get_m_RigidBody_2() const { return ___m_RigidBody_2; }
	inline Rigidbody2D_t502193897 ** get_address_of_m_RigidBody_2() { return &___m_RigidBody_2; }
	inline void set_m_RigidBody_2(Rigidbody2D_t502193897 * value)
	{
		___m_RigidBody_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_RigidBody_2, value);
	}

	inline static int32_t get_offset_of_m_Force_3() { return static_cast<int32_t>(offsetof(SimpleCharacterControler2D_t2053125211, ___m_Force_3)); }
	inline float get_m_Force_3() const { return ___m_Force_3; }
	inline float* get_address_of_m_Force_3() { return &___m_Force_3; }
	inline void set_m_Force_3(float value)
	{
		___m_Force_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxVelocityToBoost_4() { return static_cast<int32_t>(offsetof(SimpleCharacterControler2D_t2053125211, ___m_MaxVelocityToBoost_4)); }
	inline float get_m_MaxVelocityToBoost_4() const { return ___m_MaxVelocityToBoost_4; }
	inline float* get_address_of_m_MaxVelocityToBoost_4() { return &___m_MaxVelocityToBoost_4; }
	inline void set_m_MaxVelocityToBoost_4(float value)
	{
		___m_MaxVelocityToBoost_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
