﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/ForceOverLifetimeModule
struct ForceOverLifetimeModule_t3255358877;
struct ForceOverLifetimeModule_t3255358877_marshaled_pinvoke;
struct ForceOverLifetimeModule_t3255358877_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ForceOverLi3255358877.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/ForceOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ForceOverLifetimeModule__ctor_m2525424551 (ForceOverLifetimeModule_t3255358877 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct ForceOverLifetimeModule_t3255358877;
struct ForceOverLifetimeModule_t3255358877_marshaled_pinvoke;

extern "C" void ForceOverLifetimeModule_t3255358877_marshal_pinvoke(const ForceOverLifetimeModule_t3255358877& unmarshaled, ForceOverLifetimeModule_t3255358877_marshaled_pinvoke& marshaled);
extern "C" void ForceOverLifetimeModule_t3255358877_marshal_pinvoke_back(const ForceOverLifetimeModule_t3255358877_marshaled_pinvoke& marshaled, ForceOverLifetimeModule_t3255358877& unmarshaled);
extern "C" void ForceOverLifetimeModule_t3255358877_marshal_pinvoke_cleanup(ForceOverLifetimeModule_t3255358877_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ForceOverLifetimeModule_t3255358877;
struct ForceOverLifetimeModule_t3255358877_marshaled_com;

extern "C" void ForceOverLifetimeModule_t3255358877_marshal_com(const ForceOverLifetimeModule_t3255358877& unmarshaled, ForceOverLifetimeModule_t3255358877_marshaled_com& marshaled);
extern "C" void ForceOverLifetimeModule_t3255358877_marshal_com_back(const ForceOverLifetimeModule_t3255358877_marshaled_com& marshaled, ForceOverLifetimeModule_t3255358877& unmarshaled);
extern "C" void ForceOverLifetimeModule_t3255358877_marshal_com_cleanup(ForceOverLifetimeModule_t3255358877_marshaled_com& marshaled);
