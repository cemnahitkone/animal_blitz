﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// SpriteTrail
struct SpriteTrail_t2284883325;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BasicPath
struct  BasicPath_t2288617877  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean BasicPath::m_Lerp
	bool ___m_Lerp_2;
	// System.Single BasicPath::m_CurSpeed
	float ___m_CurSpeed_3;
	// UnityEngine.Transform[] BasicPath::m_Waypoints
	TransformU5BU5D_t3764228911* ___m_Waypoints_4;
	// System.Int32 BasicPath::m_CurWayPointIndex
	int32_t ___m_CurWayPointIndex_5;
	// System.Single BasicPath::m_ClampFloor
	float ___m_ClampFloor_6;
	// SpriteTrail BasicPath::m_Trail
	SpriteTrail_t2284883325 * ___m_Trail_7;
	// System.Boolean BasicPath::m_FlipX
	bool ___m_FlipX_8;
	// System.Boolean BasicPath::FlipXOnEnd
	bool ___FlipXOnEnd_9;
	// UnityEngine.Transform BasicPath::m_ItemToMove
	Transform_t3275118058 * ___m_ItemToMove_10;

public:
	inline static int32_t get_offset_of_m_Lerp_2() { return static_cast<int32_t>(offsetof(BasicPath_t2288617877, ___m_Lerp_2)); }
	inline bool get_m_Lerp_2() const { return ___m_Lerp_2; }
	inline bool* get_address_of_m_Lerp_2() { return &___m_Lerp_2; }
	inline void set_m_Lerp_2(bool value)
	{
		___m_Lerp_2 = value;
	}

	inline static int32_t get_offset_of_m_CurSpeed_3() { return static_cast<int32_t>(offsetof(BasicPath_t2288617877, ___m_CurSpeed_3)); }
	inline float get_m_CurSpeed_3() const { return ___m_CurSpeed_3; }
	inline float* get_address_of_m_CurSpeed_3() { return &___m_CurSpeed_3; }
	inline void set_m_CurSpeed_3(float value)
	{
		___m_CurSpeed_3 = value;
	}

	inline static int32_t get_offset_of_m_Waypoints_4() { return static_cast<int32_t>(offsetof(BasicPath_t2288617877, ___m_Waypoints_4)); }
	inline TransformU5BU5D_t3764228911* get_m_Waypoints_4() const { return ___m_Waypoints_4; }
	inline TransformU5BU5D_t3764228911** get_address_of_m_Waypoints_4() { return &___m_Waypoints_4; }
	inline void set_m_Waypoints_4(TransformU5BU5D_t3764228911* value)
	{
		___m_Waypoints_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_Waypoints_4, value);
	}

	inline static int32_t get_offset_of_m_CurWayPointIndex_5() { return static_cast<int32_t>(offsetof(BasicPath_t2288617877, ___m_CurWayPointIndex_5)); }
	inline int32_t get_m_CurWayPointIndex_5() const { return ___m_CurWayPointIndex_5; }
	inline int32_t* get_address_of_m_CurWayPointIndex_5() { return &___m_CurWayPointIndex_5; }
	inline void set_m_CurWayPointIndex_5(int32_t value)
	{
		___m_CurWayPointIndex_5 = value;
	}

	inline static int32_t get_offset_of_m_ClampFloor_6() { return static_cast<int32_t>(offsetof(BasicPath_t2288617877, ___m_ClampFloor_6)); }
	inline float get_m_ClampFloor_6() const { return ___m_ClampFloor_6; }
	inline float* get_address_of_m_ClampFloor_6() { return &___m_ClampFloor_6; }
	inline void set_m_ClampFloor_6(float value)
	{
		___m_ClampFloor_6 = value;
	}

	inline static int32_t get_offset_of_m_Trail_7() { return static_cast<int32_t>(offsetof(BasicPath_t2288617877, ___m_Trail_7)); }
	inline SpriteTrail_t2284883325 * get_m_Trail_7() const { return ___m_Trail_7; }
	inline SpriteTrail_t2284883325 ** get_address_of_m_Trail_7() { return &___m_Trail_7; }
	inline void set_m_Trail_7(SpriteTrail_t2284883325 * value)
	{
		___m_Trail_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_Trail_7, value);
	}

	inline static int32_t get_offset_of_m_FlipX_8() { return static_cast<int32_t>(offsetof(BasicPath_t2288617877, ___m_FlipX_8)); }
	inline bool get_m_FlipX_8() const { return ___m_FlipX_8; }
	inline bool* get_address_of_m_FlipX_8() { return &___m_FlipX_8; }
	inline void set_m_FlipX_8(bool value)
	{
		___m_FlipX_8 = value;
	}

	inline static int32_t get_offset_of_FlipXOnEnd_9() { return static_cast<int32_t>(offsetof(BasicPath_t2288617877, ___FlipXOnEnd_9)); }
	inline bool get_FlipXOnEnd_9() const { return ___FlipXOnEnd_9; }
	inline bool* get_address_of_FlipXOnEnd_9() { return &___FlipXOnEnd_9; }
	inline void set_FlipXOnEnd_9(bool value)
	{
		___FlipXOnEnd_9 = value;
	}

	inline static int32_t get_offset_of_m_ItemToMove_10() { return static_cast<int32_t>(offsetof(BasicPath_t2288617877, ___m_ItemToMove_10)); }
	inline Transform_t3275118058 * get_m_ItemToMove_10() const { return ___m_ItemToMove_10; }
	inline Transform_t3275118058 ** get_address_of_m_ItemToMove_10() { return &___m_ItemToMove_10; }
	inline void set_m_ItemToMove_10(Transform_t3275118058 * value)
	{
		___m_ItemToMove_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_ItemToMove_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
