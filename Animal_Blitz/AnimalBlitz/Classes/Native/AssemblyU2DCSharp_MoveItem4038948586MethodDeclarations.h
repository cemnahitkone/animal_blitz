﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveItem
struct MoveItem_t4038948586;

#include "codegen/il2cpp-codegen.h"

// System.Void MoveItem::.ctor()
extern "C"  void MoveItem__ctor_m3267823635 (MoveItem_t4038948586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveItem::Update()
extern "C"  void MoveItem_Update_m2758176788 (MoveItem_t4038948586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
