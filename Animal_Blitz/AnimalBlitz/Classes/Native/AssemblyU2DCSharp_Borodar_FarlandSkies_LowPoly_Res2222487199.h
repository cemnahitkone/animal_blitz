﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.ResetButton
struct  ResetButton_t2222487199  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image Borodar.FarlandSkies.LowPoly.ResetButton::TopColorImage
	Image_t2042527209 * ___TopColorImage_2;
	// UnityEngine.UI.Image Borodar.FarlandSkies.LowPoly.ResetButton::MiddleColorImage
	Image_t2042527209 * ___MiddleColorImage_3;
	// UnityEngine.UI.Image Borodar.FarlandSkies.LowPoly.ResetButton::BottomColorImage
	Image_t2042527209 * ___BottomColorImage_4;
	// UnityEngine.UI.Slider Borodar.FarlandSkies.LowPoly.ResetButton::TopExponentSlider
	Slider_t297367283 * ___TopExponentSlider_5;
	// UnityEngine.UI.Slider Borodar.FarlandSkies.LowPoly.ResetButton::BottomExponentSlider
	Slider_t297367283 * ___BottomExponentSlider_6;
	// UnityEngine.UI.Image Borodar.FarlandSkies.LowPoly.ResetButton::StarsTintImage
	Image_t2042527209 * ___StarsTintImage_7;
	// UnityEngine.UI.Slider Borodar.FarlandSkies.LowPoly.ResetButton::ExtinctionSlider
	Slider_t297367283 * ___ExtinctionSlider_8;
	// UnityEngine.UI.Slider Borodar.FarlandSkies.LowPoly.ResetButton::TwinklingSpeedSlider
	Slider_t297367283 * ___TwinklingSpeedSlider_9;
	// UnityEngine.UI.Image Borodar.FarlandSkies.LowPoly.ResetButton::SunTintImage
	Image_t2042527209 * ___SunTintImage_10;
	// UnityEngine.UI.Slider Borodar.FarlandSkies.LowPoly.ResetButton::SunAlphaSlider
	Slider_t297367283 * ___SunAlphaSlider_11;
	// UnityEngine.UI.Slider Borodar.FarlandSkies.LowPoly.ResetButton::SunSizeSlider
	Slider_t297367283 * ___SunSizeSlider_12;
	// UnityEngine.UI.Toggle Borodar.FarlandSkies.LowPoly.ResetButton::SunFlareToggle
	Toggle_t3976754468 * ___SunFlareToggle_13;
	// UnityEngine.UI.Slider Borodar.FarlandSkies.LowPoly.ResetButton::SunFlareBrightnessSlider
	Slider_t297367283 * ___SunFlareBrightnessSlider_14;
	// UnityEngine.UI.Image Borodar.FarlandSkies.LowPoly.ResetButton::MoonTintImage
	Image_t2042527209 * ___MoonTintImage_15;
	// UnityEngine.UI.Slider Borodar.FarlandSkies.LowPoly.ResetButton::MoonAlphaSlider
	Slider_t297367283 * ___MoonAlphaSlider_16;
	// UnityEngine.UI.Slider Borodar.FarlandSkies.LowPoly.ResetButton::MoonSizeSlider
	Slider_t297367283 * ___MoonSizeSlider_17;
	// UnityEngine.UI.Toggle Borodar.FarlandSkies.LowPoly.ResetButton::MoonFlareToggle
	Toggle_t3976754468 * ___MoonFlareToggle_18;
	// UnityEngine.UI.Slider Borodar.FarlandSkies.LowPoly.ResetButton::MoonFlareBrightnessSlider
	Slider_t297367283 * ___MoonFlareBrightnessSlider_19;
	// UnityEngine.UI.Image Borodar.FarlandSkies.LowPoly.ResetButton::CloudsTintImage
	Image_t2042527209 * ___CloudsTintImage_20;
	// UnityEngine.UI.Slider Borodar.FarlandSkies.LowPoly.ResetButton::CloudsRotationSlider
	Slider_t297367283 * ___CloudsRotationSlider_21;
	// UnityEngine.UI.Slider Borodar.FarlandSkies.LowPoly.ResetButton::CloudsHeightSlider
	Slider_t297367283 * ___CloudsHeightSlider_22;
	// UnityEngine.UI.Slider Borodar.FarlandSkies.LowPoly.ResetButton::ExoposureSlider
	Slider_t297367283 * ___ExoposureSlider_23;
	// UnityEngine.UI.Toggle Borodar.FarlandSkies.LowPoly.ResetButton::AdjustFogToggle
	Toggle_t3976754468 * ___AdjustFogToggle_24;

public:
	inline static int32_t get_offset_of_TopColorImage_2() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___TopColorImage_2)); }
	inline Image_t2042527209 * get_TopColorImage_2() const { return ___TopColorImage_2; }
	inline Image_t2042527209 ** get_address_of_TopColorImage_2() { return &___TopColorImage_2; }
	inline void set_TopColorImage_2(Image_t2042527209 * value)
	{
		___TopColorImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___TopColorImage_2, value);
	}

	inline static int32_t get_offset_of_MiddleColorImage_3() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___MiddleColorImage_3)); }
	inline Image_t2042527209 * get_MiddleColorImage_3() const { return ___MiddleColorImage_3; }
	inline Image_t2042527209 ** get_address_of_MiddleColorImage_3() { return &___MiddleColorImage_3; }
	inline void set_MiddleColorImage_3(Image_t2042527209 * value)
	{
		___MiddleColorImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___MiddleColorImage_3, value);
	}

	inline static int32_t get_offset_of_BottomColorImage_4() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___BottomColorImage_4)); }
	inline Image_t2042527209 * get_BottomColorImage_4() const { return ___BottomColorImage_4; }
	inline Image_t2042527209 ** get_address_of_BottomColorImage_4() { return &___BottomColorImage_4; }
	inline void set_BottomColorImage_4(Image_t2042527209 * value)
	{
		___BottomColorImage_4 = value;
		Il2CppCodeGenWriteBarrier(&___BottomColorImage_4, value);
	}

	inline static int32_t get_offset_of_TopExponentSlider_5() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___TopExponentSlider_5)); }
	inline Slider_t297367283 * get_TopExponentSlider_5() const { return ___TopExponentSlider_5; }
	inline Slider_t297367283 ** get_address_of_TopExponentSlider_5() { return &___TopExponentSlider_5; }
	inline void set_TopExponentSlider_5(Slider_t297367283 * value)
	{
		___TopExponentSlider_5 = value;
		Il2CppCodeGenWriteBarrier(&___TopExponentSlider_5, value);
	}

	inline static int32_t get_offset_of_BottomExponentSlider_6() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___BottomExponentSlider_6)); }
	inline Slider_t297367283 * get_BottomExponentSlider_6() const { return ___BottomExponentSlider_6; }
	inline Slider_t297367283 ** get_address_of_BottomExponentSlider_6() { return &___BottomExponentSlider_6; }
	inline void set_BottomExponentSlider_6(Slider_t297367283 * value)
	{
		___BottomExponentSlider_6 = value;
		Il2CppCodeGenWriteBarrier(&___BottomExponentSlider_6, value);
	}

	inline static int32_t get_offset_of_StarsTintImage_7() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___StarsTintImage_7)); }
	inline Image_t2042527209 * get_StarsTintImage_7() const { return ___StarsTintImage_7; }
	inline Image_t2042527209 ** get_address_of_StarsTintImage_7() { return &___StarsTintImage_7; }
	inline void set_StarsTintImage_7(Image_t2042527209 * value)
	{
		___StarsTintImage_7 = value;
		Il2CppCodeGenWriteBarrier(&___StarsTintImage_7, value);
	}

	inline static int32_t get_offset_of_ExtinctionSlider_8() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___ExtinctionSlider_8)); }
	inline Slider_t297367283 * get_ExtinctionSlider_8() const { return ___ExtinctionSlider_8; }
	inline Slider_t297367283 ** get_address_of_ExtinctionSlider_8() { return &___ExtinctionSlider_8; }
	inline void set_ExtinctionSlider_8(Slider_t297367283 * value)
	{
		___ExtinctionSlider_8 = value;
		Il2CppCodeGenWriteBarrier(&___ExtinctionSlider_8, value);
	}

	inline static int32_t get_offset_of_TwinklingSpeedSlider_9() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___TwinklingSpeedSlider_9)); }
	inline Slider_t297367283 * get_TwinklingSpeedSlider_9() const { return ___TwinklingSpeedSlider_9; }
	inline Slider_t297367283 ** get_address_of_TwinklingSpeedSlider_9() { return &___TwinklingSpeedSlider_9; }
	inline void set_TwinklingSpeedSlider_9(Slider_t297367283 * value)
	{
		___TwinklingSpeedSlider_9 = value;
		Il2CppCodeGenWriteBarrier(&___TwinklingSpeedSlider_9, value);
	}

	inline static int32_t get_offset_of_SunTintImage_10() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___SunTintImage_10)); }
	inline Image_t2042527209 * get_SunTintImage_10() const { return ___SunTintImage_10; }
	inline Image_t2042527209 ** get_address_of_SunTintImage_10() { return &___SunTintImage_10; }
	inline void set_SunTintImage_10(Image_t2042527209 * value)
	{
		___SunTintImage_10 = value;
		Il2CppCodeGenWriteBarrier(&___SunTintImage_10, value);
	}

	inline static int32_t get_offset_of_SunAlphaSlider_11() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___SunAlphaSlider_11)); }
	inline Slider_t297367283 * get_SunAlphaSlider_11() const { return ___SunAlphaSlider_11; }
	inline Slider_t297367283 ** get_address_of_SunAlphaSlider_11() { return &___SunAlphaSlider_11; }
	inline void set_SunAlphaSlider_11(Slider_t297367283 * value)
	{
		___SunAlphaSlider_11 = value;
		Il2CppCodeGenWriteBarrier(&___SunAlphaSlider_11, value);
	}

	inline static int32_t get_offset_of_SunSizeSlider_12() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___SunSizeSlider_12)); }
	inline Slider_t297367283 * get_SunSizeSlider_12() const { return ___SunSizeSlider_12; }
	inline Slider_t297367283 ** get_address_of_SunSizeSlider_12() { return &___SunSizeSlider_12; }
	inline void set_SunSizeSlider_12(Slider_t297367283 * value)
	{
		___SunSizeSlider_12 = value;
		Il2CppCodeGenWriteBarrier(&___SunSizeSlider_12, value);
	}

	inline static int32_t get_offset_of_SunFlareToggle_13() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___SunFlareToggle_13)); }
	inline Toggle_t3976754468 * get_SunFlareToggle_13() const { return ___SunFlareToggle_13; }
	inline Toggle_t3976754468 ** get_address_of_SunFlareToggle_13() { return &___SunFlareToggle_13; }
	inline void set_SunFlareToggle_13(Toggle_t3976754468 * value)
	{
		___SunFlareToggle_13 = value;
		Il2CppCodeGenWriteBarrier(&___SunFlareToggle_13, value);
	}

	inline static int32_t get_offset_of_SunFlareBrightnessSlider_14() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___SunFlareBrightnessSlider_14)); }
	inline Slider_t297367283 * get_SunFlareBrightnessSlider_14() const { return ___SunFlareBrightnessSlider_14; }
	inline Slider_t297367283 ** get_address_of_SunFlareBrightnessSlider_14() { return &___SunFlareBrightnessSlider_14; }
	inline void set_SunFlareBrightnessSlider_14(Slider_t297367283 * value)
	{
		___SunFlareBrightnessSlider_14 = value;
		Il2CppCodeGenWriteBarrier(&___SunFlareBrightnessSlider_14, value);
	}

	inline static int32_t get_offset_of_MoonTintImage_15() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___MoonTintImage_15)); }
	inline Image_t2042527209 * get_MoonTintImage_15() const { return ___MoonTintImage_15; }
	inline Image_t2042527209 ** get_address_of_MoonTintImage_15() { return &___MoonTintImage_15; }
	inline void set_MoonTintImage_15(Image_t2042527209 * value)
	{
		___MoonTintImage_15 = value;
		Il2CppCodeGenWriteBarrier(&___MoonTintImage_15, value);
	}

	inline static int32_t get_offset_of_MoonAlphaSlider_16() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___MoonAlphaSlider_16)); }
	inline Slider_t297367283 * get_MoonAlphaSlider_16() const { return ___MoonAlphaSlider_16; }
	inline Slider_t297367283 ** get_address_of_MoonAlphaSlider_16() { return &___MoonAlphaSlider_16; }
	inline void set_MoonAlphaSlider_16(Slider_t297367283 * value)
	{
		___MoonAlphaSlider_16 = value;
		Il2CppCodeGenWriteBarrier(&___MoonAlphaSlider_16, value);
	}

	inline static int32_t get_offset_of_MoonSizeSlider_17() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___MoonSizeSlider_17)); }
	inline Slider_t297367283 * get_MoonSizeSlider_17() const { return ___MoonSizeSlider_17; }
	inline Slider_t297367283 ** get_address_of_MoonSizeSlider_17() { return &___MoonSizeSlider_17; }
	inline void set_MoonSizeSlider_17(Slider_t297367283 * value)
	{
		___MoonSizeSlider_17 = value;
		Il2CppCodeGenWriteBarrier(&___MoonSizeSlider_17, value);
	}

	inline static int32_t get_offset_of_MoonFlareToggle_18() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___MoonFlareToggle_18)); }
	inline Toggle_t3976754468 * get_MoonFlareToggle_18() const { return ___MoonFlareToggle_18; }
	inline Toggle_t3976754468 ** get_address_of_MoonFlareToggle_18() { return &___MoonFlareToggle_18; }
	inline void set_MoonFlareToggle_18(Toggle_t3976754468 * value)
	{
		___MoonFlareToggle_18 = value;
		Il2CppCodeGenWriteBarrier(&___MoonFlareToggle_18, value);
	}

	inline static int32_t get_offset_of_MoonFlareBrightnessSlider_19() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___MoonFlareBrightnessSlider_19)); }
	inline Slider_t297367283 * get_MoonFlareBrightnessSlider_19() const { return ___MoonFlareBrightnessSlider_19; }
	inline Slider_t297367283 ** get_address_of_MoonFlareBrightnessSlider_19() { return &___MoonFlareBrightnessSlider_19; }
	inline void set_MoonFlareBrightnessSlider_19(Slider_t297367283 * value)
	{
		___MoonFlareBrightnessSlider_19 = value;
		Il2CppCodeGenWriteBarrier(&___MoonFlareBrightnessSlider_19, value);
	}

	inline static int32_t get_offset_of_CloudsTintImage_20() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___CloudsTintImage_20)); }
	inline Image_t2042527209 * get_CloudsTintImage_20() const { return ___CloudsTintImage_20; }
	inline Image_t2042527209 ** get_address_of_CloudsTintImage_20() { return &___CloudsTintImage_20; }
	inline void set_CloudsTintImage_20(Image_t2042527209 * value)
	{
		___CloudsTintImage_20 = value;
		Il2CppCodeGenWriteBarrier(&___CloudsTintImage_20, value);
	}

	inline static int32_t get_offset_of_CloudsRotationSlider_21() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___CloudsRotationSlider_21)); }
	inline Slider_t297367283 * get_CloudsRotationSlider_21() const { return ___CloudsRotationSlider_21; }
	inline Slider_t297367283 ** get_address_of_CloudsRotationSlider_21() { return &___CloudsRotationSlider_21; }
	inline void set_CloudsRotationSlider_21(Slider_t297367283 * value)
	{
		___CloudsRotationSlider_21 = value;
		Il2CppCodeGenWriteBarrier(&___CloudsRotationSlider_21, value);
	}

	inline static int32_t get_offset_of_CloudsHeightSlider_22() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___CloudsHeightSlider_22)); }
	inline Slider_t297367283 * get_CloudsHeightSlider_22() const { return ___CloudsHeightSlider_22; }
	inline Slider_t297367283 ** get_address_of_CloudsHeightSlider_22() { return &___CloudsHeightSlider_22; }
	inline void set_CloudsHeightSlider_22(Slider_t297367283 * value)
	{
		___CloudsHeightSlider_22 = value;
		Il2CppCodeGenWriteBarrier(&___CloudsHeightSlider_22, value);
	}

	inline static int32_t get_offset_of_ExoposureSlider_23() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___ExoposureSlider_23)); }
	inline Slider_t297367283 * get_ExoposureSlider_23() const { return ___ExoposureSlider_23; }
	inline Slider_t297367283 ** get_address_of_ExoposureSlider_23() { return &___ExoposureSlider_23; }
	inline void set_ExoposureSlider_23(Slider_t297367283 * value)
	{
		___ExoposureSlider_23 = value;
		Il2CppCodeGenWriteBarrier(&___ExoposureSlider_23, value);
	}

	inline static int32_t get_offset_of_AdjustFogToggle_24() { return static_cast<int32_t>(offsetof(ResetButton_t2222487199, ___AdjustFogToggle_24)); }
	inline Toggle_t3976754468 * get_AdjustFogToggle_24() const { return ___AdjustFogToggle_24; }
	inline Toggle_t3976754468 ** get_address_of_AdjustFogToggle_24() { return &___AdjustFogToggle_24; }
	inline void set_AdjustFogToggle_24(Toggle_t3976754468 * value)
	{
		___AdjustFogToggle_24 = value;
		Il2CppCodeGenWriteBarrier(&___AdjustFogToggle_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
