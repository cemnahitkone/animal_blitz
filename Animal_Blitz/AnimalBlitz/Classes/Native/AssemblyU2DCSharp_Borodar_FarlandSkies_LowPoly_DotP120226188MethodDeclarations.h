﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.DotParams.SunParamsList
struct SunParamsList_t120226188;
// Borodar.FarlandSkies.LowPoly.DotParams.SunParam
struct SunParam_t3023881265;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.DotParams.SunParamsList::.ctor(System.Int32)
extern "C"  void SunParamsList__ctor_m2251540671 (SunParamsList_t120226188 * __this, int32_t ___capacity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Borodar.FarlandSkies.LowPoly.DotParams.SunParam Borodar.FarlandSkies.LowPoly.DotParams.SunParamsList::GetParamPerTime(System.Single)
extern "C"  SunParam_t3023881265 * SunParamsList_GetParamPerTime_m2875046303 (SunParamsList_t120226188 * __this, float ___currentTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
