﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.Core.DotParams.DotParam
struct DotParam_t3767082706;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.Core.DotParams.DotParam::.ctor()
extern "C"  void DotParam__ctor_m759611265 (DotParam_t3767082706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
