﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityScript.Lang.UnityRuntimeServices
struct UnityRuntimeServices_t3303336867;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void UnityScript.Lang.UnityRuntimeServices::.cctor()
extern "C"  void UnityRuntimeServices__cctor_m277244785 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityScript.Lang.UnityRuntimeServices::.ctor()
extern "C"  void UnityRuntimeServices__ctor_m1066678718 (UnityRuntimeServices_t3303336867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityScript.Lang.UnityRuntimeServices::GetEnumerator(System.Object)
extern "C"  Il2CppObject * UnityRuntimeServices_GetEnumerator_m1135949016 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityScript.Lang.UnityRuntimeServices::Update(System.Collections.IEnumerator,System.Object)
extern "C"  void UnityRuntimeServices_Update_m1436857700 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___e0, Il2CppObject * ___newValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityScript.Lang.UnityRuntimeServices::IsValueTypeArray(System.Object)
extern "C"  bool UnityRuntimeServices_IsValueTypeArray_m651527436 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityScript.Lang.UnityRuntimeServices::$static_initializer$()
extern "C"  void UnityRuntimeServices_U24static_initializerU24_m2178156047 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
