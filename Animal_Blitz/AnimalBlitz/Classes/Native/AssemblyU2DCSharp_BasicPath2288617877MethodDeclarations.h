﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BasicPath
struct BasicPath_t2288617877;

#include "codegen/il2cpp-codegen.h"

// System.Void BasicPath::.ctor()
extern "C"  void BasicPath__ctor_m3083187560 (BasicPath_t2288617877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BasicPath::Update()
extern "C"  void BasicPath_Update_m1691169813 (BasicPath_t2288617877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
