﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/ShapeModule
struct ShapeModule_t2888832346;
struct ShapeModule_t2888832346_marshaled_pinvoke;
struct ShapeModule_t2888832346_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ShapeModule2888832346.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/ShapeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ShapeModule__ctor_m2156531686 (ShapeModule_t2888832346 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct ShapeModule_t2888832346;
struct ShapeModule_t2888832346_marshaled_pinvoke;

extern "C" void ShapeModule_t2888832346_marshal_pinvoke(const ShapeModule_t2888832346& unmarshaled, ShapeModule_t2888832346_marshaled_pinvoke& marshaled);
extern "C" void ShapeModule_t2888832346_marshal_pinvoke_back(const ShapeModule_t2888832346_marshaled_pinvoke& marshaled, ShapeModule_t2888832346& unmarshaled);
extern "C" void ShapeModule_t2888832346_marshal_pinvoke_cleanup(ShapeModule_t2888832346_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ShapeModule_t2888832346;
struct ShapeModule_t2888832346_marshaled_com;

extern "C" void ShapeModule_t2888832346_marshal_com(const ShapeModule_t2888832346& unmarshaled, ShapeModule_t2888832346_marshaled_com& marshaled);
extern "C" void ShapeModule_t2888832346_marshal_com_back(const ShapeModule_t2888832346_marshaled_com& marshaled, ShapeModule_t2888832346& unmarshaled);
extern "C" void ShapeModule_t2888832346_marshal_com_cleanup(ShapeModule_t2888832346_marshaled_com& marshaled);
