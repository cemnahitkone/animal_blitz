﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.AnimatedButton
struct AnimatedButton_t4037967263;
// Ricimi.AnimatedButton/ButtonClickedEvent
struct ButtonClickedEvent_t1021215292;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Ricimi_AnimatedButton_ButtonClic1021215292.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void Ricimi.AnimatedButton::.ctor()
extern "C"  void AnimatedButton__ctor_m4105384997 (AnimatedButton_t4037967263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.AnimatedButton::Start()
extern "C"  void AnimatedButton_Start_m1158744109 (AnimatedButton_t4037967263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Ricimi.AnimatedButton/ButtonClickedEvent Ricimi.AnimatedButton::get_onClick()
extern "C"  ButtonClickedEvent_t1021215292 * AnimatedButton_get_onClick_m934551318 (AnimatedButton_t4037967263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.AnimatedButton::set_onClick(Ricimi.AnimatedButton/ButtonClickedEvent)
extern "C"  void AnimatedButton_set_onClick_m1837799887 (AnimatedButton_t4037967263 * __this, ButtonClickedEvent_t1021215292 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.AnimatedButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void AnimatedButton_OnPointerDown_m3200672169 (AnimatedButton_t4037967263 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.AnimatedButton::Press()
extern "C"  void AnimatedButton_Press_m1752899086 (AnimatedButton_t4037967263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.AnimatedButton::InvokeOnClickAction()
extern "C"  void AnimatedButton_InvokeOnClickAction_m3865374204 (AnimatedButton_t4037967263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
