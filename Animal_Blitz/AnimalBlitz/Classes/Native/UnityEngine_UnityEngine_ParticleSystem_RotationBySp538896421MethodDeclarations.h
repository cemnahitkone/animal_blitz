﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/RotationBySpeedModule
struct RotationBySpeedModule_t538896421;
struct RotationBySpeedModule_t538896421_marshaled_pinvoke;
struct RotationBySpeedModule_t538896421_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_RotationBySp538896421.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/RotationBySpeedModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void RotationBySpeedModule__ctor_m577817007 (RotationBySpeedModule_t538896421 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct RotationBySpeedModule_t538896421;
struct RotationBySpeedModule_t538896421_marshaled_pinvoke;

extern "C" void RotationBySpeedModule_t538896421_marshal_pinvoke(const RotationBySpeedModule_t538896421& unmarshaled, RotationBySpeedModule_t538896421_marshaled_pinvoke& marshaled);
extern "C" void RotationBySpeedModule_t538896421_marshal_pinvoke_back(const RotationBySpeedModule_t538896421_marshaled_pinvoke& marshaled, RotationBySpeedModule_t538896421& unmarshaled);
extern "C" void RotationBySpeedModule_t538896421_marshal_pinvoke_cleanup(RotationBySpeedModule_t538896421_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct RotationBySpeedModule_t538896421;
struct RotationBySpeedModule_t538896421_marshaled_com;

extern "C" void RotationBySpeedModule_t538896421_marshal_com(const RotationBySpeedModule_t538896421& unmarshaled, RotationBySpeedModule_t538896421_marshaled_com& marshaled);
extern "C" void RotationBySpeedModule_t538896421_marshal_com_back(const RotationBySpeedModule_t538896421_marshaled_com& marshaled, RotationBySpeedModule_t538896421& unmarshaled);
extern "C" void RotationBySpeedModule_t538896421_marshal_com_cleanup(RotationBySpeedModule_t538896421_marshaled_com& marshaled);
