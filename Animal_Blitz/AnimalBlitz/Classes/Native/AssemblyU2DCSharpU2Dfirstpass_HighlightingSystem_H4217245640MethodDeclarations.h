﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HighlightingSystem.HighlighterRenderer
struct HighlighterRenderer_t4217245640;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t1204166949;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Shader2430389951.h"
#include "UnityEngine_UnityEngine_Rendering_CommandBuffer1204166949.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void HighlightingSystem.HighlighterRenderer::.ctor()
extern "C"  void HighlighterRenderer__ctor_m3750554968 (HighlighterRenderer_t4217245640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlighterRenderer::OnEnable()
extern "C"  void HighlighterRenderer_OnEnable_m1144232048 (HighlighterRenderer_t4217245640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlighterRenderer::OnDisable()
extern "C"  void HighlighterRenderer_OnDisable_m1468183261 (HighlighterRenderer_t4217245640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlighterRenderer::OnWillRenderObject()
extern "C"  void HighlighterRenderer_OnWillRenderObject_m1534154096 (HighlighterRenderer_t4217245640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HighlightingSystem.HighlighterRenderer::EndOfFrame()
extern "C"  Il2CppObject * HighlighterRenderer_EndOfFrame_m3876382929 (HighlighterRenderer_t4217245640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlighterRenderer::Initialize(UnityEngine.Material,UnityEngine.Shader)
extern "C"  void HighlighterRenderer_Initialize_m306395362 (HighlighterRenderer_t4217245640 * __this, Material_t193706927 * ___sharedOpaqueMaterial0, Shader_t2430389951 * ___transparentShader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HighlightingSystem.HighlighterRenderer::FillBuffer(UnityEngine.Rendering.CommandBuffer)
extern "C"  bool HighlighterRenderer_FillBuffer_m392175031 (HighlighterRenderer_t4217245640 * __this, CommandBuffer_t1204166949 * ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlighterRenderer::SetColorForTransparent(UnityEngine.Color)
extern "C"  void HighlighterRenderer_SetColorForTransparent_m4078721542 (HighlighterRenderer_t4217245640 * __this, Color_t2020392075  ___clr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlighterRenderer::SetZTestForTransparent(System.Int32)
extern "C"  void HighlighterRenderer_SetZTestForTransparent_m3290452558 (HighlighterRenderer_t4217245640 * __this, int32_t ___zTest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlighterRenderer::SetStencilRefForTransparent(System.Int32)
extern "C"  void HighlighterRenderer_SetStencilRefForTransparent_m1560370405 (HighlighterRenderer_t4217245640 * __this, int32_t ___stencilRef0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlighterRenderer::SetState(System.Boolean)
extern "C"  void HighlighterRenderer_SetState_m2777729094 (HighlighterRenderer_t4217245640 * __this, bool ___alive0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightingSystem.HighlighterRenderer::.cctor()
extern "C"  void HighlighterRenderer__cctor_m3550548299 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
