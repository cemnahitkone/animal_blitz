﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.LevelScene
struct  LevelScene_t288242702  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Ricimi.LevelScene::prevLevelButton
	GameObject_t1756533147 * ___prevLevelButton_2;
	// UnityEngine.GameObject Ricimi.LevelScene::nextLevelButton
	GameObject_t1756533147 * ___nextLevelButton_3;
	// UnityEngine.GameObject Ricimi.LevelScene::levelGroup
	GameObject_t1756533147 * ___levelGroup_4;
	// UnityEngine.UI.Text Ricimi.LevelScene::levelText
	Text_t356221433 * ___levelText_5;
	// System.Int32 Ricimi.LevelScene::m_currentLevelIndex
	int32_t ___m_currentLevelIndex_7;
	// UnityEngine.Animator Ricimi.LevelScene::m_animator
	Animator_t69676727 * ___m_animator_8;

public:
	inline static int32_t get_offset_of_prevLevelButton_2() { return static_cast<int32_t>(offsetof(LevelScene_t288242702, ___prevLevelButton_2)); }
	inline GameObject_t1756533147 * get_prevLevelButton_2() const { return ___prevLevelButton_2; }
	inline GameObject_t1756533147 ** get_address_of_prevLevelButton_2() { return &___prevLevelButton_2; }
	inline void set_prevLevelButton_2(GameObject_t1756533147 * value)
	{
		___prevLevelButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___prevLevelButton_2, value);
	}

	inline static int32_t get_offset_of_nextLevelButton_3() { return static_cast<int32_t>(offsetof(LevelScene_t288242702, ___nextLevelButton_3)); }
	inline GameObject_t1756533147 * get_nextLevelButton_3() const { return ___nextLevelButton_3; }
	inline GameObject_t1756533147 ** get_address_of_nextLevelButton_3() { return &___nextLevelButton_3; }
	inline void set_nextLevelButton_3(GameObject_t1756533147 * value)
	{
		___nextLevelButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___nextLevelButton_3, value);
	}

	inline static int32_t get_offset_of_levelGroup_4() { return static_cast<int32_t>(offsetof(LevelScene_t288242702, ___levelGroup_4)); }
	inline GameObject_t1756533147 * get_levelGroup_4() const { return ___levelGroup_4; }
	inline GameObject_t1756533147 ** get_address_of_levelGroup_4() { return &___levelGroup_4; }
	inline void set_levelGroup_4(GameObject_t1756533147 * value)
	{
		___levelGroup_4 = value;
		Il2CppCodeGenWriteBarrier(&___levelGroup_4, value);
	}

	inline static int32_t get_offset_of_levelText_5() { return static_cast<int32_t>(offsetof(LevelScene_t288242702, ___levelText_5)); }
	inline Text_t356221433 * get_levelText_5() const { return ___levelText_5; }
	inline Text_t356221433 ** get_address_of_levelText_5() { return &___levelText_5; }
	inline void set_levelText_5(Text_t356221433 * value)
	{
		___levelText_5 = value;
		Il2CppCodeGenWriteBarrier(&___levelText_5, value);
	}

	inline static int32_t get_offset_of_m_currentLevelIndex_7() { return static_cast<int32_t>(offsetof(LevelScene_t288242702, ___m_currentLevelIndex_7)); }
	inline int32_t get_m_currentLevelIndex_7() const { return ___m_currentLevelIndex_7; }
	inline int32_t* get_address_of_m_currentLevelIndex_7() { return &___m_currentLevelIndex_7; }
	inline void set_m_currentLevelIndex_7(int32_t value)
	{
		___m_currentLevelIndex_7 = value;
	}

	inline static int32_t get_offset_of_m_animator_8() { return static_cast<int32_t>(offsetof(LevelScene_t288242702, ___m_animator_8)); }
	inline Animator_t69676727 * get_m_animator_8() const { return ___m_animator_8; }
	inline Animator_t69676727 ** get_address_of_m_animator_8() { return &___m_animator_8; }
	inline void set_m_animator_8(Animator_t69676727 * value)
	{
		___m_animator_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_animator_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
