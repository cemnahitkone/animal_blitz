﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.AnimatedButton/ButtonClickedEvent
struct ButtonClickedEvent_t1021215292;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.AnimatedButton/ButtonClickedEvent::.ctor()
extern "C"  void ButtonClickedEvent__ctor_m954124331 (ButtonClickedEvent_t1021215292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
