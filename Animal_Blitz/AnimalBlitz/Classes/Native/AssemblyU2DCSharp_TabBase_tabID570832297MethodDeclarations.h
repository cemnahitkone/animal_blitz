﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TabBase/tabID
struct tabID_t570832297;

#include "codegen/il2cpp-codegen.h"

// System.Void TabBase/tabID::.ctor()
extern "C"  void tabID__ctor_m1405518218 (tabID_t570832297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
