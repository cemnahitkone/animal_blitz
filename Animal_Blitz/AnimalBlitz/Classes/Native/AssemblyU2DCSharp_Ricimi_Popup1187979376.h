﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.Popup
struct  Popup_t1187979376  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color Ricimi.Popup::backgroundColor
	Color_t2020392075  ___backgroundColor_2;
	// UnityEngine.GameObject Ricimi.Popup::m_background
	GameObject_t1756533147 * ___m_background_3;

public:
	inline static int32_t get_offset_of_backgroundColor_2() { return static_cast<int32_t>(offsetof(Popup_t1187979376, ___backgroundColor_2)); }
	inline Color_t2020392075  get_backgroundColor_2() const { return ___backgroundColor_2; }
	inline Color_t2020392075 * get_address_of_backgroundColor_2() { return &___backgroundColor_2; }
	inline void set_backgroundColor_2(Color_t2020392075  value)
	{
		___backgroundColor_2 = value;
	}

	inline static int32_t get_offset_of_m_background_3() { return static_cast<int32_t>(offsetof(Popup_t1187979376, ___m_background_3)); }
	inline GameObject_t1756533147 * get_m_background_3() const { return ___m_background_3; }
	inline GameObject_t1756533147 ** get_address_of_m_background_3() { return &___m_background_3; }
	inline void set_m_background_3(GameObject_t1756533147 * value)
	{
		___m_background_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_background_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
