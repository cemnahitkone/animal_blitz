﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_SpawnSystem
struct CFX_SpawnSystem_t3600628354;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void CFX_SpawnSystem::.ctor()
extern "C"  void CFX_SpawnSystem__ctor_m1058108967 (CFX_SpawnSystem_t3600628354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject CFX_SpawnSystem::GetNextObject(UnityEngine.GameObject,System.Boolean)
extern "C"  GameObject_t1756533147 * CFX_SpawnSystem_GetNextObject_m2511020167 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___sourceObj0, bool ___activateObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::PreloadObject(UnityEngine.GameObject,System.Int32)
extern "C"  void CFX_SpawnSystem_PreloadObject_m1869696192 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___sourceObj0, int32_t ___poolSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::UnloadObjects(UnityEngine.GameObject)
extern "C"  void CFX_SpawnSystem_UnloadObjects_m1396750606 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___sourceObj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CFX_SpawnSystem::get_AllObjectsLoaded()
extern "C"  bool CFX_SpawnSystem_get_AllObjectsLoaded_m1685608814 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::addObjectToPool(UnityEngine.GameObject,System.Int32)
extern "C"  void CFX_SpawnSystem_addObjectToPool_m3240392253 (CFX_SpawnSystem_t3600628354 * __this, GameObject_t1756533147 * ___sourceObject0, int32_t ___number1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::removeObjectsFromPool(UnityEngine.GameObject)
extern "C"  void CFX_SpawnSystem_removeObjectsFromPool_m4142132593 (CFX_SpawnSystem_t3600628354 * __this, GameObject_t1756533147 * ___sourceObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::increasePoolCursor(System.Int32)
extern "C"  void CFX_SpawnSystem_increasePoolCursor_m989985540 (CFX_SpawnSystem_t3600628354 * __this, int32_t ___uniqueId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::Awake()
extern "C"  void CFX_SpawnSystem_Awake_m1198488558 (CFX_SpawnSystem_t3600628354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::Start()
extern "C"  void CFX_SpawnSystem_Start_m1759789747 (CFX_SpawnSystem_t3600628354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
