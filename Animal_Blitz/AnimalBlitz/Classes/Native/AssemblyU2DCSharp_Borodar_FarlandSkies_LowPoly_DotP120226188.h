﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_DotPar2617037026.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.DotParams.SunParamsList
struct  SunParamsList_t120226188  : public DotParamsList_1_t2617037026
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
