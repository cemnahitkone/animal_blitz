﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/RotationOverLifetimeModule
struct RotationOverLifetimeModule_t2204422354;
struct RotationOverLifetimeModule_t2204422354_marshaled_pinvoke;
struct RotationOverLifetimeModule_t2204422354_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_RotationOve2204422354.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/RotationOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void RotationOverLifetimeModule__ctor_m3980207104 (RotationOverLifetimeModule_t2204422354 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct RotationOverLifetimeModule_t2204422354;
struct RotationOverLifetimeModule_t2204422354_marshaled_pinvoke;

extern "C" void RotationOverLifetimeModule_t2204422354_marshal_pinvoke(const RotationOverLifetimeModule_t2204422354& unmarshaled, RotationOverLifetimeModule_t2204422354_marshaled_pinvoke& marshaled);
extern "C" void RotationOverLifetimeModule_t2204422354_marshal_pinvoke_back(const RotationOverLifetimeModule_t2204422354_marshaled_pinvoke& marshaled, RotationOverLifetimeModule_t2204422354& unmarshaled);
extern "C" void RotationOverLifetimeModule_t2204422354_marshal_pinvoke_cleanup(RotationOverLifetimeModule_t2204422354_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct RotationOverLifetimeModule_t2204422354;
struct RotationOverLifetimeModule_t2204422354_marshaled_com;

extern "C" void RotationOverLifetimeModule_t2204422354_marshal_com(const RotationOverLifetimeModule_t2204422354& unmarshaled, RotationOverLifetimeModule_t2204422354_marshaled_com& marshaled);
extern "C" void RotationOverLifetimeModule_t2204422354_marshal_com_back(const RotationOverLifetimeModule_t2204422354_marshaled_com& marshaled, RotationOverLifetimeModule_t2204422354& unmarshaled);
extern "C" void RotationOverLifetimeModule_t2204422354_marshal_com_cleanup(RotationOverLifetimeModule_t2204422354_marshaled_com& marshaled);
