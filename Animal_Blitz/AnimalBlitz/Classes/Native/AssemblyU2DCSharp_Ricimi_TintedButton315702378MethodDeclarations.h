﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.TintedButton
struct TintedButton_t315702378;
// Ricimi.TintedButton/ButtonClickedEvent
struct ButtonClickedEvent_t2671903891;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Ricimi_TintedButton_ButtonClicke2671903891.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"

// System.Void Ricimi.TintedButton::.ctor()
extern "C"  void TintedButton__ctor_m1521398894 (TintedButton_t315702378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Ricimi.TintedButton/ButtonClickedEvent Ricimi.TintedButton::get_onClick()
extern "C"  ButtonClickedEvent_t2671903891 * TintedButton_get_onClick_m3055813248 (TintedButton_t315702378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.TintedButton::set_onClick(Ricimi.TintedButton/ButtonClickedEvent)
extern "C"  void TintedButton_set_onClick_m959616331 (TintedButton_t315702378 * __this, ButtonClickedEvent_t2671903891 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.TintedButton::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C"  void TintedButton_OnPointerEnter_m1624062428 (TintedButton_t315702378 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.TintedButton::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C"  void TintedButton_OnPointerExit_m250407452 (TintedButton_t315702378 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.TintedButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void TintedButton_OnPointerUp_m1540706215 (TintedButton_t315702378 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.TintedButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void TintedButton_OnPointerDown_m2961715000 (TintedButton_t315702378 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.TintedButton::Press()
extern "C"  void TintedButton_Press_m11217139 (TintedButton_t315702378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.TintedButton::Unpress()
extern "C"  void TintedButton_Unpress_m135402196 (TintedButton_t315702378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.TintedButton::Darken()
extern "C"  void TintedButton_Darken_m3793465363 (TintedButton_t315702378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.TintedButton::Brighten()
extern "C"  void TintedButton_Brighten_m1485808913 (TintedButton_t315702378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Ricimi.TintedButton::DarkenColor(UnityEngine.UI.Image)
extern "C"  Il2CppObject * TintedButton_DarkenColor_m2892116578 (TintedButton_t315702378 * __this, Image_t2042527209 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Ricimi.TintedButton::BrightenColor(UnityEngine.UI.Image)
extern "C"  Il2CppObject * TintedButton_BrightenColor_m1354593218 (TintedButton_t315702378 * __this, Image_t2042527209 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
