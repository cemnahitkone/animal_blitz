﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.DotParams.StarsParamsList
struct StarsParamsList_t1887883603;
// Borodar.FarlandSkies.LowPoly.DotParams.StarsParam
struct StarsParam_t907828490;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.DotParams.StarsParamsList::.ctor()
extern "C"  void StarsParamsList__ctor_m3209683877 (StarsParamsList_t1887883603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Borodar.FarlandSkies.LowPoly.DotParams.StarsParam Borodar.FarlandSkies.LowPoly.DotParams.StarsParamsList::GetParamPerTime(System.Single)
extern "C"  StarsParam_t907828490 * StarsParamsList_GetParamPerTime_m4032126591 (StarsParamsList_t1887883603 * __this, float ___currentTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
