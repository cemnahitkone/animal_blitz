﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JumpTest
struct JumpTest_t640307972;

#include "codegen/il2cpp-codegen.h"

// System.Void JumpTest::.ctor()
extern "C"  void JumpTest__ctor_m2489370914 (JumpTest_t640307972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpTest::.cctor()
extern "C"  void JumpTest__cctor_m3066781921 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpTest::Start()
extern "C"  void JumpTest_Start_m82108526 (JumpTest_t640307972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpTest::Update()
extern "C"  void JumpTest_Update_m2386774551 (JumpTest_t640307972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpTest::OnGUI()
extern "C"  void JumpTest_OnGUI_m3982212818 (JumpTest_t640307972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpTest::Main()
extern "C"  void JumpTest_Main_m1331742801 (JumpTest_t640307972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
