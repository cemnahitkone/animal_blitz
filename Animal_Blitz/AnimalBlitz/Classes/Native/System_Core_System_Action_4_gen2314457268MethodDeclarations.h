﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_4_gen1537300974MethodDeclarations.h"

// System.Void System.Action`4<System.String,System.String,System.Int32,System.Byte[]>::.ctor(System.Object,System.IntPtr)
#define Action_4__ctor_m940517352(__this, ___object0, ___method1, method) ((  void (*) (Action_4_t2314457268 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_4__ctor_m3010512211_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`4<System.String,System.String,System.Int32,System.Byte[]>::Invoke(T1,T2,T3,T4)
#define Action_4_Invoke_m808000806(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  void (*) (Action_4_t2314457268 *, String_t*, String_t*, int32_t, ByteU5BU5D_t3397334013*, const MethodInfo*))Action_4_Invoke_m16726123_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult System.Action`4<System.String,System.String,System.Int32,System.Byte[]>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
#define Action_4_BeginInvoke_m1742990029(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (Action_4_t2314457268 *, String_t*, String_t*, int32_t, ByteU5BU5D_t3397334013*, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_4_BeginInvoke_m3604630106_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// System.Void System.Action`4<System.String,System.String,System.Int32,System.Byte[]>::EndInvoke(System.IAsyncResult)
#define Action_4_EndInvoke_m2922190524(__this, ___result0, method) ((  void (*) (Action_4_t2314457268 *, Il2CppObject *, const MethodInfo*))Action_4_EndInvoke_m2537290505_gshared)(__this, ___result0, method)
