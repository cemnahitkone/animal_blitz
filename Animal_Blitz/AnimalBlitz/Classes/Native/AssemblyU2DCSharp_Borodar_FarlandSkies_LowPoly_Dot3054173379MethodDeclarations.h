﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.DotParams.SkyParamsList
struct SkyParamsList_t3054173379;
// Borodar.FarlandSkies.LowPoly.DotParams.SkyParam
struct SkyParam_t4272832432;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.DotParams.SkyParamsList::.ctor()
extern "C"  void SkyParamsList__ctor_m3849310751 (SkyParamsList_t3054173379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Borodar.FarlandSkies.LowPoly.DotParams.SkyParam Borodar.FarlandSkies.LowPoly.DotParams.SkyParamsList::GetParamPerTime(System.Single)
extern "C"  SkyParam_t4272832432 * SkyParamsList_GetParamPerTime_m3554654495 (SkyParamsList_t3054173379 * __this, float ___currentTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
