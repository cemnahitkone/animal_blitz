﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Space4278750806.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rotate
struct  Rotate_t4255939432  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 Rotate::eulerAngles
	Vector3_t2243707580  ___eulerAngles_2;
	// UnityEngine.Space Rotate::relativeTo
	int32_t ___relativeTo_3;

public:
	inline static int32_t get_offset_of_eulerAngles_2() { return static_cast<int32_t>(offsetof(Rotate_t4255939432, ___eulerAngles_2)); }
	inline Vector3_t2243707580  get_eulerAngles_2() const { return ___eulerAngles_2; }
	inline Vector3_t2243707580 * get_address_of_eulerAngles_2() { return &___eulerAngles_2; }
	inline void set_eulerAngles_2(Vector3_t2243707580  value)
	{
		___eulerAngles_2 = value;
	}

	inline static int32_t get_offset_of_relativeTo_3() { return static_cast<int32_t>(offsetof(Rotate_t4255939432, ___relativeTo_3)); }
	inline int32_t get_relativeTo_3() const { return ___relativeTo_3; }
	inline int32_t* get_address_of_relativeTo_3() { return &___relativeTo_3; }
	inline void set_relativeTo_3(int32_t value)
	{
		___relativeTo_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
