﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa3894236545.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementat762068664.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305143  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-1456763F890A84558F99AFA687C36B9037697848
	U24ArrayTypeU3D16_t3894236545  ___U24fieldU2D1456763F890A84558F99AFA687C36B9037697848_0;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A
	U24ArrayTypeU3D24_t762068664  ___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D1456763F890A84558F99AFA687C36B9037697848_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields, ___U24fieldU2D1456763F890A84558F99AFA687C36B9037697848_0)); }
	inline U24ArrayTypeU3D16_t3894236545  get_U24fieldU2D1456763F890A84558F99AFA687C36B9037697848_0() const { return ___U24fieldU2D1456763F890A84558F99AFA687C36B9037697848_0; }
	inline U24ArrayTypeU3D16_t3894236545 * get_address_of_U24fieldU2D1456763F890A84558F99AFA687C36B9037697848_0() { return &___U24fieldU2D1456763F890A84558F99AFA687C36B9037697848_0; }
	inline void set_U24fieldU2D1456763F890A84558F99AFA687C36B9037697848_0(U24ArrayTypeU3D16_t3894236545  value)
	{
		___U24fieldU2D1456763F890A84558F99AFA687C36B9037697848_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields, ___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1)); }
	inline U24ArrayTypeU3D24_t762068664  get_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1() const { return ___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1; }
	inline U24ArrayTypeU3D24_t762068664 * get_address_of_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1() { return &___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1; }
	inline void set_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1(U24ArrayTypeU3D24_t762068664  value)
	{
		___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
