﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<CFX_LightIntensityFade>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2185666896(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t785519585 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m853313801_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<CFX_LightIntensityFade>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m907878328(__this, method) ((  void (*) (InternalEnumerator_1_t785519585 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<CFX_LightIntensityFade>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3208945428(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t785519585 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<CFX_LightIntensityFade>::Dispose()
#define InternalEnumerator_1_Dispose_m1205875127(__this, method) ((  void (*) (InternalEnumerator_1_t785519585 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1636767846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<CFX_LightIntensityFade>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1775867928(__this, method) ((  bool (*) (InternalEnumerator_1_t785519585 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1047150157_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<CFX_LightIntensityFade>::get_Current()
#define InternalEnumerator_1_get_Current_m2201709263(__this, method) ((  CFX_LightIntensityFade_t4221734619 * (*) (InternalEnumerator_1_t785519585 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3206960238_gshared)(__this, method)
