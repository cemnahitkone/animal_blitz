﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Text_RegularExpressions_Capture4157900610.h"
#include "System_System_Text_RegularExpressions_CILCompiler1740644799.h"
#include "System_System_Text_RegularExpressions_CILCompiler_F997927490.h"
#include "System_System_Text_RegularExpressions_Category1984577050.h"
#include "System_System_Text_RegularExpressions_CategoryUtil3840220623.h"
#include "System_System_Text_RegularExpressions_LinkRef2090853131.h"
#include "System_System_Text_RegularExpressions_InterpreterFa556462562.h"
#include "System_System_Text_RegularExpressions_PatternCompil637049905.h"
#include "System_System_Text_RegularExpressions_PatternCompi3979537293.h"
#include "System_System_Text_RegularExpressions_PatternCompi3337276394.h"
#include "System_System_Text_RegularExpressions_LinkStack954792760.h"
#include "System_System_Text_RegularExpressions_Mark2724874473.h"
#include "System_System_Text_RegularExpressions_GroupCollecti939014605.h"
#include "System_System_Text_RegularExpressions_Group3761430853.h"
#include "System_System_Text_RegularExpressions_Interpreter3731288230.h"
#include "System_System_Text_RegularExpressions_Interpreter_I273560425.h"
#include "System_System_Text_RegularExpressions_Interpreter_1827616978.h"
#include "System_System_Text_RegularExpressions_Interpreter_2395763083.h"
#include "System_System_Text_RegularExpressions_Interval2354235237.h"
#include "System_System_Text_RegularExpressions_IntervalColl4130821325.h"
#include "System_System_Text_RegularExpressions_IntervalColl1928086041.h"
#include "System_System_Text_RegularExpressions_IntervalColl1824458113.h"
#include "System_System_Text_RegularExpressions_MatchCollect3718216671.h"
#include "System_System_Text_RegularExpressions_MatchCollecti501456973.h"
#include "System_System_Text_RegularExpressions_Match3164245899.h"
#include "System_System_Text_RegularExpressions_Syntax_Parse2756028923.h"
#include "System_System_Text_RegularExpressions_QuickSearch1036078825.h"
#include "System_System_Text_RegularExpressions_Regex1803876613.h"
#include "System_System_Text_RegularExpressions_RegexOptions2418259727.h"
#include "System_System_Text_RegularExpressions_RxInterprete2459337652.h"
#include "System_System_Text_RegularExpressions_RxInterprete3288646651.h"
#include "System_System_Text_RegularExpressions_RxInterpreter834810884.h"
#include "System_System_Text_RegularExpressions_RxLinkRef275774985.h"
#include "System_System_Text_RegularExpressions_RxCompiler4215271879.h"
#include "System_System_Text_RegularExpressions_RxInterprete1812879716.h"
#include "System_System_Text_RegularExpressions_RxOp4049298493.h"
#include "System_System_Text_RegularExpressions_ReplacementE1001703513.h"
#include "System_System_Text_RegularExpressions_Syntax_Expres238836340.h"
#include "System_System_Text_RegularExpressions_Syntax_Expres368137076.h"
#include "System_System_Text_RegularExpressions_Syntax_Compo1921307915.h"
#include "System_System_Text_RegularExpressions_Syntax_Group2558408851.h"
#include "System_System_Text_RegularExpressions_Syntax_Regul3083097024.h"
#include "System_System_Text_RegularExpressions_Syntax_Captu3690174926.h"
#include "System_System_Text_RegularExpressions_Syntax_Balan3168604284.h"
#include "System_System_Text_RegularExpressions_Syntax_NonBac607185170.h"
#include "System_System_Text_RegularExpressions_Syntax_Repet3426306051.h"
#include "System_System_Text_RegularExpressions_Syntax_Asser1490870658.h"
#include "System_System_Text_RegularExpressions_Syntax_Captur196851652.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre3255443744.h"
#include "System_System_Text_RegularExpressions_Syntax_Alter3506694545.h"
#include "System_System_Text_RegularExpressions_Syntax_Liter2896011011.h"
#include "System_System_Text_RegularExpressions_Syntax_Posit2152361535.h"
#include "System_System_Text_RegularExpressions_Syntax_Refer1540574699.h"
#include "System_System_Text_RegularExpressions_Syntax_Backs1461652789.h"
#include "System_System_Text_RegularExpressions_Syntax_Charac655244183.h"
#include "System_System_Text_RegularExpressions_Syntax_Ancho1392970135.h"
#include "System_System_UriBuilder2016461725.h"
#include "System_System_Uri19570940.h"
#include "System_System_Uri_UriScheme1876590943.h"
#include "System_System_UriFormatException3682083048.h"
#include "System_System_UriHostNameType2148127109.h"
#include "System_System_UriKind1128731744.h"
#include "System_System_UriParser1012511323.h"
#include "System_System_UriPartial112107391.h"
#include "System_System_UriTypeConverter3912970448.h"
#include "System_System_Net_BindIPEndPoint635820671.h"
#include "System_System_Net_HttpContinueDelegate2713047268.h"
#include "System_System_Net_Security_LocalCertificateSelecti3696771181.h"
#include "System_System_Net_Security_RemoteCertificateValida2756269959.h"
#include "System_System_Text_RegularExpressions_MatchEvaluato710107290.h"
#include "System_System_Text_RegularExpressions_EvalDelegate877898325.h"
#include "System_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array1703410334.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayT116038554.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3672778802.h"
#include "System_Configuration_U3CModuleU3E3783534214.h"
#include "System_Configuration_System_Configuration_Provider2882126354.h"
#include "System_Configuration_System_Configuration_Provider2548499159.h"
#include "System_Configuration_System_Configuration_ClientCo4294641134.h"
#include "System_Configuration_System_Configuration_ConfigNa2395569530.h"
#include "System_Configuration_System_Configuration_ConfigInf546730838.h"
#include "System_Configuration_System_Configuration_Configur3335372970.h"
#include "System_Configuration_System_Configuration_Configur3250313246.h"
#include "System_Configuration_System_Configuration_Configur3860111898.h"
#include "System_Configuration_System_Configuration_Configur2811353736.h"
#include "System_Configuration_System_Configuration_Configur1776195828.h"
#include "System_Configuration_System_Configuration_ElementMa997038224.h"
#include "System_Configuration_System_Configuration_Configur1911180302.h"
#include "System_Configuration_System_Configuration_Configur3305291330.h"
#include "System_Configuration_System_Configuration_Configur1806001494.h"
#include "System_Configuration_System_Configuration_Configur1362721126.h"
#include "System_Configuration_System_Configuration_Configur2625210096.h"
#include "System_Configuration_System_Configuration_Configur1895107553.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (Capture_t4157900610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[3] = 
{
	Capture_t4157900610::get_offset_of_index_0(),
	Capture_t4157900610::get_offset_of_length_1(),
	Capture_t4157900610::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (CILCompiler_t1740644799), -1, sizeof(CILCompiler_t1740644799_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1801[32] = 
{
	CILCompiler_t1740644799::get_offset_of_eval_methods_2(),
	CILCompiler_t1740644799::get_offset_of_eval_methods_defined_3(),
	CILCompiler_t1740644799::get_offset_of_generic_ops_4(),
	CILCompiler_t1740644799::get_offset_of_op_flags_5(),
	CILCompiler_t1740644799::get_offset_of_labels_6(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_str_7(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_string_start_8(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_string_end_9(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_program_10(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_marks_11(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_groups_12(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_deep_13(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_stack_14(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_mark_start_15(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_mark_end_16(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_mark_index_17(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_stack_get_count_18(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_stack_set_count_19(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_stack_push_20(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_stack_pop_21(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_set_start_of_match_22(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_is_word_char_23(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_reset_groups_24(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_checkpoint_25(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_backtrack_26(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_open_27(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_close_28(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_get_last_defined_29(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_mark_get_index_30(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_mark_get_length_31(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_trace_compile_32(),
	CILCompiler_t1740644799::get_offset_of_local_textinfo_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (Frame_t997927490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[2] = 
{
	Frame_t997927490::get_offset_of_label_pass_0(),
	Frame_t997927490::get_offset_of_label_fail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (Category_t1984577050)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1803[146] = 
{
	Category_t1984577050::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (CategoryUtils_t3840220623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (LinkRef_t2090853131), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (InterpreterFactory_t556462562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[4] = 
{
	InterpreterFactory_t556462562::get_offset_of_mapping_0(),
	InterpreterFactory_t556462562::get_offset_of_pattern_1(),
	InterpreterFactory_t556462562::get_offset_of_namesMapping_2(),
	InterpreterFactory_t556462562::get_offset_of_gap_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (PatternCompiler_t637049905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[1] = 
{
	PatternCompiler_t637049905::get_offset_of_pgm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (PatternLinkStack_t3979537293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[1] = 
{
	PatternLinkStack_t3979537293::get_offset_of_link_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (Link_t3337276394)+ sizeof (Il2CppObject), sizeof(Link_t3337276394 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1810[2] = 
{
	Link_t3337276394::get_offset_of_base_addr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Link_t3337276394::get_offset_of_offset_addr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (LinkStack_t954792760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1811[1] = 
{
	LinkStack_t954792760::get_offset_of_stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (Mark_t2724874473)+ sizeof (Il2CppObject), sizeof(Mark_t2724874473 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1812[3] = 
{
	Mark_t2724874473::get_offset_of_Start_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t2724874473::get_offset_of_End_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t2724874473::get_offset_of_Previous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (GroupCollection_t939014605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[2] = 
{
	GroupCollection_t939014605::get_offset_of_list_0(),
	GroupCollection_t939014605::get_offset_of_gap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (Group_t3761430853), -1, sizeof(Group_t3761430853_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1814[3] = 
{
	Group_t3761430853_StaticFields::get_offset_of_Fail_3(),
	Group_t3761430853::get_offset_of_success_4(),
	Group_t3761430853::get_offset_of_captures_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (Interpreter_t3731288230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[16] = 
{
	Interpreter_t3731288230::get_offset_of_program_1(),
	Interpreter_t3731288230::get_offset_of_program_start_2(),
	Interpreter_t3731288230::get_offset_of_text_3(),
	Interpreter_t3731288230::get_offset_of_text_end_4(),
	Interpreter_t3731288230::get_offset_of_group_count_5(),
	Interpreter_t3731288230::get_offset_of_match_min_6(),
	Interpreter_t3731288230::get_offset_of_qs_7(),
	Interpreter_t3731288230::get_offset_of_scan_ptr_8(),
	Interpreter_t3731288230::get_offset_of_repeat_9(),
	Interpreter_t3731288230::get_offset_of_fast_10(),
	Interpreter_t3731288230::get_offset_of_stack_11(),
	Interpreter_t3731288230::get_offset_of_deep_12(),
	Interpreter_t3731288230::get_offset_of_marks_13(),
	Interpreter_t3731288230::get_offset_of_mark_start_14(),
	Interpreter_t3731288230::get_offset_of_mark_end_15(),
	Interpreter_t3731288230::get_offset_of_groups_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (IntStack_t273560425)+ sizeof (Il2CppObject), sizeof(IntStack_t273560425_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1816[2] = 
{
	IntStack_t273560425::get_offset_of_values_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntStack_t273560425::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (RepeatContext_t1827616978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[7] = 
{
	RepeatContext_t1827616978::get_offset_of_start_0(),
	RepeatContext_t1827616978::get_offset_of_min_1(),
	RepeatContext_t1827616978::get_offset_of_max_2(),
	RepeatContext_t1827616978::get_offset_of_lazy_3(),
	RepeatContext_t1827616978::get_offset_of_expr_pc_4(),
	RepeatContext_t1827616978::get_offset_of_previous_5(),
	RepeatContext_t1827616978::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (Mode_t2395763083)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1818[4] = 
{
	Mode_t2395763083::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (Interval_t2354235237)+ sizeof (Il2CppObject), sizeof(Interval_t2354235237_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1819[3] = 
{
	Interval_t2354235237::get_offset_of_low_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t2354235237::get_offset_of_high_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t2354235237::get_offset_of_contiguous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (IntervalCollection_t4130821325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[1] = 
{
	IntervalCollection_t4130821325::get_offset_of_intervals_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (Enumerator_t1928086041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[2] = 
{
	Enumerator_t1928086041::get_offset_of_list_0(),
	Enumerator_t1928086041::get_offset_of_ptr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (CostDelegate_t1824458113), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (MatchCollection_t3718216671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[2] = 
{
	MatchCollection_t3718216671::get_offset_of_current_0(),
	MatchCollection_t3718216671::get_offset_of_list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (Enumerator_t501456973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1824[2] = 
{
	Enumerator_t501456973::get_offset_of_index_0(),
	Enumerator_t501456973::get_offset_of_coll_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (Match_t3164245899), -1, sizeof(Match_t3164245899_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1825[5] = 
{
	Match_t3164245899::get_offset_of_regex_6(),
	Match_t3164245899::get_offset_of_machine_7(),
	Match_t3164245899::get_offset_of_text_length_8(),
	Match_t3164245899::get_offset_of_groups_9(),
	Match_t3164245899_StaticFields::get_offset_of_empty_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (Parser_t2756028923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[6] = 
{
	Parser_t2756028923::get_offset_of_pattern_0(),
	Parser_t2756028923::get_offset_of_ptr_1(),
	Parser_t2756028923::get_offset_of_caps_2(),
	Parser_t2756028923::get_offset_of_refs_3(),
	Parser_t2756028923::get_offset_of_num_groups_4(),
	Parser_t2756028923::get_offset_of_gap_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (QuickSearch_t1036078825), -1, sizeof(QuickSearch_t1036078825_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1827[7] = 
{
	QuickSearch_t1036078825::get_offset_of_str_0(),
	QuickSearch_t1036078825::get_offset_of_len_1(),
	QuickSearch_t1036078825::get_offset_of_ignore_2(),
	QuickSearch_t1036078825::get_offset_of_reverse_3(),
	QuickSearch_t1036078825::get_offset_of_shift_4(),
	QuickSearch_t1036078825::get_offset_of_shiftExtended_5(),
	QuickSearch_t1036078825_StaticFields::get_offset_of_THRESHOLD_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (Regex_t1803876613), -1, sizeof(Regex_t1803876613_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1828[10] = 
{
	Regex_t1803876613_StaticFields::get_offset_of_cache_0(),
	Regex_t1803876613_StaticFields::get_offset_of_old_rx_1(),
	Regex_t1803876613::get_offset_of_machineFactory_2(),
	Regex_t1803876613::get_offset_of_mapping_3(),
	Regex_t1803876613::get_offset_of_group_count_4(),
	Regex_t1803876613::get_offset_of_gap_5(),
	Regex_t1803876613::get_offset_of_group_names_6(),
	Regex_t1803876613::get_offset_of_group_numbers_7(),
	Regex_t1803876613::get_offset_of_pattern_8(),
	Regex_t1803876613::get_offset_of_roptions_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (RegexOptions_t2418259727)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1829[11] = 
{
	RegexOptions_t2418259727::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (RxInterpreter_t2459337652), -1, sizeof(RxInterpreter_t2459337652_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1830[14] = 
{
	RxInterpreter_t2459337652::get_offset_of_program_1(),
	RxInterpreter_t2459337652::get_offset_of_str_2(),
	RxInterpreter_t2459337652::get_offset_of_string_start_3(),
	RxInterpreter_t2459337652::get_offset_of_string_end_4(),
	RxInterpreter_t2459337652::get_offset_of_group_count_5(),
	RxInterpreter_t2459337652::get_offset_of_groups_6(),
	RxInterpreter_t2459337652::get_offset_of_eval_del_7(),
	RxInterpreter_t2459337652::get_offset_of_marks_8(),
	RxInterpreter_t2459337652::get_offset_of_mark_start_9(),
	RxInterpreter_t2459337652::get_offset_of_mark_end_10(),
	RxInterpreter_t2459337652::get_offset_of_stack_11(),
	RxInterpreter_t2459337652::get_offset_of_repeat_12(),
	RxInterpreter_t2459337652::get_offset_of_deep_13(),
	RxInterpreter_t2459337652_StaticFields::get_offset_of_trace_rx_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (IntStack_t3288646651)+ sizeof (Il2CppObject), sizeof(IntStack_t3288646651_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1831[2] = 
{
	IntStack_t3288646651::get_offset_of_values_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntStack_t3288646651::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (RepeatContext_t834810884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[7] = 
{
	RepeatContext_t834810884::get_offset_of_start_0(),
	RepeatContext_t834810884::get_offset_of_min_1(),
	RepeatContext_t834810884::get_offset_of_max_2(),
	RepeatContext_t834810884::get_offset_of_lazy_3(),
	RepeatContext_t834810884::get_offset_of_expr_pc_4(),
	RepeatContext_t834810884::get_offset_of_previous_5(),
	RepeatContext_t834810884::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (RxLinkRef_t275774985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1833[2] = 
{
	RxLinkRef_t275774985::get_offset_of_offsets_0(),
	RxLinkRef_t275774985::get_offset_of_current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (RxCompiler_t4215271879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1834[2] = 
{
	RxCompiler_t4215271879::get_offset_of_program_0(),
	RxCompiler_t4215271879::get_offset_of_curpos_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (RxInterpreterFactory_t1812879716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[5] = 
{
	RxInterpreterFactory_t1812879716::get_offset_of_mapping_0(),
	RxInterpreterFactory_t1812879716::get_offset_of_program_1(),
	RxInterpreterFactory_t1812879716::get_offset_of_eval_del_2(),
	RxInterpreterFactory_t1812879716::get_offset_of_namesMapping_3(),
	RxInterpreterFactory_t1812879716::get_offset_of_gap_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (RxOp_t4049298493)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1836[160] = 
{
	RxOp_t4049298493::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (ReplacementEvaluator_t1001703513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[4] = 
{
	ReplacementEvaluator_t1001703513::get_offset_of_regex_0(),
	ReplacementEvaluator_t1001703513::get_offset_of_n_pieces_1(),
	ReplacementEvaluator_t1001703513::get_offset_of_pieces_2(),
	ReplacementEvaluator_t1001703513::get_offset_of_replacement_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (ExpressionCollection_t238836340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (Expression_t368137076), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (CompositeExpression_t1921307915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1840[1] = 
{
	CompositeExpression_t1921307915::get_offset_of_expressions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (Group_t2558408851), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (RegularExpression_t3083097024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1842[1] = 
{
	RegularExpression_t3083097024::get_offset_of_group_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (CapturingGroup_t3690174926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[2] = 
{
	CapturingGroup_t3690174926::get_offset_of_gid_1(),
	CapturingGroup_t3690174926::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (BalancingGroup_t3168604284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1844[1] = 
{
	BalancingGroup_t3168604284::get_offset_of_balance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (NonBacktrackingGroup_t607185170), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (Repetition_t3426306051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[3] = 
{
	Repetition_t3426306051::get_offset_of_min_1(),
	Repetition_t3426306051::get_offset_of_max_2(),
	Repetition_t3426306051::get_offset_of_lazy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (Assertion_t1490870658), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (CaptureAssertion_t196851652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[3] = 
{
	CaptureAssertion_t196851652::get_offset_of_alternate_1(),
	CaptureAssertion_t196851652::get_offset_of_group_2(),
	CaptureAssertion_t196851652::get_offset_of_literal_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (ExpressionAssertion_t3255443744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1849[2] = 
{
	ExpressionAssertion_t3255443744::get_offset_of_reverse_1(),
	ExpressionAssertion_t3255443744::get_offset_of_negate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (Alternation_t3506694545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (Literal_t2896011011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[2] = 
{
	Literal_t2896011011::get_offset_of_str_0(),
	Literal_t2896011011::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (PositionAssertion_t2152361535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[1] = 
{
	PositionAssertion_t2152361535::get_offset_of_pos_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (Reference_t1540574699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[2] = 
{
	Reference_t1540574699::get_offset_of_group_0(),
	Reference_t1540574699::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (BackslashNumber_t1461652789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[2] = 
{
	BackslashNumber_t1461652789::get_offset_of_literal_2(),
	BackslashNumber_t1461652789::get_offset_of_ecma_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (CharacterClass_t655244183), -1, sizeof(CharacterClass_t655244183_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1855[6] = 
{
	CharacterClass_t655244183_StaticFields::get_offset_of_upper_case_characters_0(),
	CharacterClass_t655244183::get_offset_of_negate_1(),
	CharacterClass_t655244183::get_offset_of_ignore_2(),
	CharacterClass_t655244183::get_offset_of_pos_cats_3(),
	CharacterClass_t655244183::get_offset_of_neg_cats_4(),
	CharacterClass_t655244183::get_offset_of_intervals_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (AnchorInfo_t1392970135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1856[6] = 
{
	AnchorInfo_t1392970135::get_offset_of_expr_0(),
	AnchorInfo_t1392970135::get_offset_of_pos_1(),
	AnchorInfo_t1392970135::get_offset_of_offset_2(),
	AnchorInfo_t1392970135::get_offset_of_str_3(),
	AnchorInfo_t1392970135::get_offset_of_width_4(),
	AnchorInfo_t1392970135::get_offset_of_ignore_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (UriBuilder_t2016461725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[10] = 
{
	UriBuilder_t2016461725::get_offset_of_scheme_0(),
	UriBuilder_t2016461725::get_offset_of_host_1(),
	UriBuilder_t2016461725::get_offset_of_port_2(),
	UriBuilder_t2016461725::get_offset_of_path_3(),
	UriBuilder_t2016461725::get_offset_of_query_4(),
	UriBuilder_t2016461725::get_offset_of_fragment_5(),
	UriBuilder_t2016461725::get_offset_of_username_6(),
	UriBuilder_t2016461725::get_offset_of_password_7(),
	UriBuilder_t2016461725::get_offset_of_uri_8(),
	UriBuilder_t2016461725::get_offset_of_modified_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (Uri_t19570940), -1, sizeof(Uri_t19570940_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1858[36] = 
{
	Uri_t19570940::get_offset_of_isUnixFilePath_0(),
	Uri_t19570940::get_offset_of_source_1(),
	Uri_t19570940::get_offset_of_scheme_2(),
	Uri_t19570940::get_offset_of_host_3(),
	Uri_t19570940::get_offset_of_port_4(),
	Uri_t19570940::get_offset_of_path_5(),
	Uri_t19570940::get_offset_of_query_6(),
	Uri_t19570940::get_offset_of_fragment_7(),
	Uri_t19570940::get_offset_of_userinfo_8(),
	Uri_t19570940::get_offset_of_isUnc_9(),
	Uri_t19570940::get_offset_of_isOpaquePart_10(),
	Uri_t19570940::get_offset_of_isAbsoluteUri_11(),
	Uri_t19570940::get_offset_of_userEscaped_12(),
	Uri_t19570940::get_offset_of_cachedAbsoluteUri_13(),
	Uri_t19570940::get_offset_of_cachedToString_14(),
	Uri_t19570940::get_offset_of_cachedLocalPath_15(),
	Uri_t19570940::get_offset_of_cachedHashCode_16(),
	Uri_t19570940_StaticFields::get_offset_of_hexUpperChars_17(),
	Uri_t19570940_StaticFields::get_offset_of_SchemeDelimiter_18(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeFile_19(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeFtp_20(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeGopher_21(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeHttp_22(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeHttps_23(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeMailto_24(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNews_25(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNntp_26(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNetPipe_27(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNetTcp_28(),
	Uri_t19570940_StaticFields::get_offset_of_schemes_29(),
	Uri_t19570940::get_offset_of_parser_30(),
	Uri_t19570940_StaticFields::get_offset_of_U3CU3Ef__switchU24map1C_31(),
	Uri_t19570940_StaticFields::get_offset_of_U3CU3Ef__switchU24map1D_32(),
	Uri_t19570940_StaticFields::get_offset_of_U3CU3Ef__switchU24map1E_33(),
	Uri_t19570940_StaticFields::get_offset_of_U3CU3Ef__switchU24map1F_34(),
	Uri_t19570940_StaticFields::get_offset_of_U3CU3Ef__switchU24map20_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (UriScheme_t1876590943)+ sizeof (Il2CppObject), sizeof(UriScheme_t1876590943_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1859[3] = 
{
	UriScheme_t1876590943::get_offset_of_scheme_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t1876590943::get_offset_of_delimiter_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t1876590943::get_offset_of_defaultPort_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (UriFormatException_t3682083048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (UriHostNameType_t2148127109)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1861[6] = 
{
	UriHostNameType_t2148127109::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (UriKind_t1128731744)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1862[4] = 
{
	UriKind_t1128731744::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (UriParser_t1012511323), -1, sizeof(UriParser_t1012511323_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1863[6] = 
{
	UriParser_t1012511323_StaticFields::get_offset_of_lock_object_0(),
	UriParser_t1012511323_StaticFields::get_offset_of_table_1(),
	UriParser_t1012511323::get_offset_of_scheme_name_2(),
	UriParser_t1012511323::get_offset_of_default_port_3(),
	UriParser_t1012511323_StaticFields::get_offset_of_uri_regex_4(),
	UriParser_t1012511323_StaticFields::get_offset_of_auth_regex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (UriPartial_t112107391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1864[5] = 
{
	UriPartial_t112107391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (UriTypeConverter_t3912970448), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (BindIPEndPoint_t635820671), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (HttpContinueDelegate_t2713047268), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (LocalCertificateSelectionCallback_t3696771181), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (RemoteCertificateValidationCallback_t2756269959), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (MatchEvaluator_t710107290), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (EvalDelegate_t877898325), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305140), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1872[4] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D1_0(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D3_1(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D4_2(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D5_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (U24ArrayTypeU2416_t1703410336)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t1703410336 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (U24ArrayTypeU24128_t116038555)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t116038555 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (U24ArrayTypeU2412_t3672778807)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778807 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (ProviderBase_t2882126354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1883[3] = 
{
	ProviderBase_t2882126354::get_offset_of_alreadyInitialized_0(),
	ProviderBase_t2882126354::get_offset_of__description_1(),
	ProviderBase_t2882126354::get_offset_of__name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (ProviderCollection_t2548499159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1884[3] = 
{
	ProviderCollection_t2548499159::get_offset_of_lookup_0(),
	ProviderCollection_t2548499159::get_offset_of_readOnly_1(),
	ProviderCollection_t2548499159::get_offset_of_values_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (ClientConfigurationSystem_t4294641134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1885[1] = 
{
	ClientConfigurationSystem_t4294641134::get_offset_of_cfg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (ConfigNameValueCollection_t2395569530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[1] = 
{
	ConfigNameValueCollection_t2395569530::get_offset_of_modified_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (ConfigInfo_t546730838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[6] = 
{
	ConfigInfo_t546730838::get_offset_of_Name_0(),
	ConfigInfo_t546730838::get_offset_of_TypeName_1(),
	ConfigInfo_t546730838::get_offset_of_Type_2(),
	ConfigInfo_t546730838::get_offset_of_streamName_3(),
	ConfigInfo_t546730838::get_offset_of_Parent_4(),
	ConfigInfo_t546730838::get_offset_of_ConfigHost_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (Configuration_t3335372970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1888[12] = 
{
	Configuration_t3335372970::get_offset_of_parent_0(),
	Configuration_t3335372970::get_offset_of_elementData_1(),
	Configuration_t3335372970::get_offset_of_streamName_2(),
	Configuration_t3335372970::get_offset_of_rootSectionGroup_3(),
	Configuration_t3335372970::get_offset_of_locations_4(),
	Configuration_t3335372970::get_offset_of_rootGroup_5(),
	Configuration_t3335372970::get_offset_of_system_6(),
	Configuration_t3335372970::get_offset_of_hasFile_7(),
	Configuration_t3335372970::get_offset_of_rootNamespace_8(),
	Configuration_t3335372970::get_offset_of_configPath_9(),
	Configuration_t3335372970::get_offset_of_locationConfigPath_10(),
	Configuration_t3335372970::get_offset_of_locationSubPath_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (ConfigurationAllowDefinition_t3250313246)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1889[5] = 
{
	ConfigurationAllowDefinition_t3250313246::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (ConfigurationAllowExeDefinition_t3860111898)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1890[5] = 
{
	ConfigurationAllowExeDefinition_t3860111898::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (ConfigurationCollectionAttribute_t2811353736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[5] = 
{
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_addItemName_0(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_clearItemsName_1(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_removeItemName_2(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_collectionType_3(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_itemType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (ConfigurationElement_t1776195828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[13] = 
{
	ConfigurationElement_t1776195828::get_offset_of_rawXml_0(),
	ConfigurationElement_t1776195828::get_offset_of_modified_1(),
	ConfigurationElement_t1776195828::get_offset_of_map_2(),
	ConfigurationElement_t1776195828::get_offset_of_keyProps_3(),
	ConfigurationElement_t1776195828::get_offset_of_defaultCollection_4(),
	ConfigurationElement_t1776195828::get_offset_of_readOnly_5(),
	ConfigurationElement_t1776195828::get_offset_of_elementInfo_6(),
	ConfigurationElement_t1776195828::get_offset_of__configuration_7(),
	ConfigurationElement_t1776195828::get_offset_of_lockAllAttributesExcept_8(),
	ConfigurationElement_t1776195828::get_offset_of_lockAllElementsExcept_9(),
	ConfigurationElement_t1776195828::get_offset_of_lockAttributes_10(),
	ConfigurationElement_t1776195828::get_offset_of_lockElements_11(),
	ConfigurationElement_t1776195828::get_offset_of_lockItem_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (ElementMap_t997038224), -1, sizeof(ElementMap_t997038224_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1893[3] = 
{
	ElementMap_t997038224_StaticFields::get_offset_of_elementMaps_0(),
	ElementMap_t997038224::get_offset_of_properties_1(),
	ElementMap_t997038224::get_offset_of_collectionAttribute_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (ConfigurationElementCollection_t1911180302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[10] = 
{
	ConfigurationElementCollection_t1911180302::get_offset_of_list_13(),
	ConfigurationElementCollection_t1911180302::get_offset_of_removed_14(),
	ConfigurationElementCollection_t1911180302::get_offset_of_inherited_15(),
	ConfigurationElementCollection_t1911180302::get_offset_of_emitClear_16(),
	ConfigurationElementCollection_t1911180302::get_offset_of_modified_17(),
	ConfigurationElementCollection_t1911180302::get_offset_of_comparer_18(),
	ConfigurationElementCollection_t1911180302::get_offset_of_inheritedLimitIndex_19(),
	ConfigurationElementCollection_t1911180302::get_offset_of_addElementName_20(),
	ConfigurationElementCollection_t1911180302::get_offset_of_clearElementName_21(),
	ConfigurationElementCollection_t1911180302::get_offset_of_removeElementName_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (ConfigurationRemoveElement_t3305291330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1895[3] = 
{
	ConfigurationRemoveElement_t3305291330::get_offset_of_properties_13(),
	ConfigurationRemoveElement_t3305291330::get_offset_of__origElement_14(),
	ConfigurationRemoveElement_t3305291330::get_offset_of__origCollection_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (ConfigurationElementCollectionType_t1806001494)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1896[5] = 
{
	ConfigurationElementCollectionType_t1806001494::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (ConfigurationErrorsException_t1362721126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[2] = 
{
	ConfigurationErrorsException_t1362721126::get_offset_of_filename_13(),
	ConfigurationErrorsException_t1362721126::get_offset_of_line_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (ConfigurationFileMap_t2625210096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1898[1] = 
{
	ConfigurationFileMap_t2625210096::get_offset_of_machineConfigFilename_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (ConfigurationLocation_t1895107553), -1, sizeof(ConfigurationLocation_t1895107553_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1899[7] = 
{
	ConfigurationLocation_t1895107553_StaticFields::get_offset_of_pathTrimChars_0(),
	ConfigurationLocation_t1895107553::get_offset_of_path_1(),
	ConfigurationLocation_t1895107553::get_offset_of_configuration_2(),
	ConfigurationLocation_t1895107553::get_offset_of_parent_3(),
	ConfigurationLocation_t1895107553::get_offset_of_xmlContent_4(),
	ConfigurationLocation_t1895107553::get_offset_of_parentResolved_5(),
	ConfigurationLocation_t1895107553::get_offset_of_allowOverride_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
