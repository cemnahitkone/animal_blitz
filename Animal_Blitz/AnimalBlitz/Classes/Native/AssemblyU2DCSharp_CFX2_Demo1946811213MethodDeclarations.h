﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX2_Demo
struct CFX2_Demo_t1946811213;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX2_Demo::.ctor()
extern "C"  void CFX2_Demo__ctor_m162761546 (CFX2_Demo_t1946811213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX2_Demo::OnMouseDown()
extern "C"  void CFX2_Demo_OnMouseDown_m1149549170 (CFX2_Demo_t1946811213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject CFX2_Demo::spawnParticle()
extern "C"  GameObject_t1756533147 * CFX2_Demo_spawnParticle_m3345353110 (CFX2_Demo_t1946811213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX2_Demo::OnGUI()
extern "C"  void CFX2_Demo_OnGUI_m2587140642 (CFX2_Demo_t1946811213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CFX2_Demo::RandomSpawnsCoroutine()
extern "C"  Il2CppObject * CFX2_Demo_RandomSpawnsCoroutine_m992370419 (CFX2_Demo_t1946811213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX2_Demo::Update()
extern "C"  void CFX2_Demo_Update_m2919854709 (CFX2_Demo_t1946811213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX2_Demo::prevParticle()
extern "C"  void CFX2_Demo_prevParticle_m4288714561 (CFX2_Demo_t1946811213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX2_Demo::nextParticle()
extern "C"  void CFX2_Demo_nextParticle_m2827562415 (CFX2_Demo_t1946811213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
