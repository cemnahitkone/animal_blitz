﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.AnimationClip
struct AnimationClip_t3510324950;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JumpTest
struct  JumpTest_t640307972  : public MonoBehaviour_t1158329972
{
public:
	// System.Single JumpTest::startTime
	float ___startTime_2;
	// System.Boolean JumpTest::debug
	bool ___debug_3;
	// UnityEngine.AnimationCurve JumpTest::heightCurve
	AnimationCurve_t3306541151 * ___heightCurve_4;
	// UnityEngine.AnimationClip JumpTest::upAnimation
	AnimationClip_t3510324950 * ___upAnimation_5;
	// UnityEngine.AnimationClip JumpTest::downAnimation
	AnimationClip_t3510324950 * ___downAnimation_6;
	// UnityEngine.AnimationClip JumpTest::landAnimation
	AnimationClip_t3510324950 * ___landAnimation_7;
	// UnityEngine.AnimationClip JumpTest::idleAnimation
	AnimationClip_t3510324950 * ___idleAnimation_8;
	// System.Single JumpTest::multiplier
	float ___multiplier_9;
	// System.Single JumpTest::offset
	float ___offset_10;
	// System.Single JumpTest::verticalSpeed
	float ___verticalSpeed_11;
	// System.Single JumpTest::previousYPosition
	float ___previousYPosition_12;
	// System.Single JumpTest::upDownBlend
	float ___upDownBlend_14;
	// System.Single JumpTest::idleLandBlend
	float ___idleLandBlend_15;
	// UnityEngine.AnimationCurve JumpTest::upDownBlendCurve
	AnimationCurve_t3306541151 * ___upDownBlendCurve_16;
	// System.Boolean JumpTest::isLanding
	bool ___isLanding_17;
	// System.Single JumpTest::landStartTime
	float ___landStartTime_19;
	// System.Boolean JumpTest::isGrounded
	bool ___isGrounded_20;

public:
	inline static int32_t get_offset_of_startTime_2() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___startTime_2)); }
	inline float get_startTime_2() const { return ___startTime_2; }
	inline float* get_address_of_startTime_2() { return &___startTime_2; }
	inline void set_startTime_2(float value)
	{
		___startTime_2 = value;
	}

	inline static int32_t get_offset_of_debug_3() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___debug_3)); }
	inline bool get_debug_3() const { return ___debug_3; }
	inline bool* get_address_of_debug_3() { return &___debug_3; }
	inline void set_debug_3(bool value)
	{
		___debug_3 = value;
	}

	inline static int32_t get_offset_of_heightCurve_4() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___heightCurve_4)); }
	inline AnimationCurve_t3306541151 * get_heightCurve_4() const { return ___heightCurve_4; }
	inline AnimationCurve_t3306541151 ** get_address_of_heightCurve_4() { return &___heightCurve_4; }
	inline void set_heightCurve_4(AnimationCurve_t3306541151 * value)
	{
		___heightCurve_4 = value;
		Il2CppCodeGenWriteBarrier(&___heightCurve_4, value);
	}

	inline static int32_t get_offset_of_upAnimation_5() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___upAnimation_5)); }
	inline AnimationClip_t3510324950 * get_upAnimation_5() const { return ___upAnimation_5; }
	inline AnimationClip_t3510324950 ** get_address_of_upAnimation_5() { return &___upAnimation_5; }
	inline void set_upAnimation_5(AnimationClip_t3510324950 * value)
	{
		___upAnimation_5 = value;
		Il2CppCodeGenWriteBarrier(&___upAnimation_5, value);
	}

	inline static int32_t get_offset_of_downAnimation_6() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___downAnimation_6)); }
	inline AnimationClip_t3510324950 * get_downAnimation_6() const { return ___downAnimation_6; }
	inline AnimationClip_t3510324950 ** get_address_of_downAnimation_6() { return &___downAnimation_6; }
	inline void set_downAnimation_6(AnimationClip_t3510324950 * value)
	{
		___downAnimation_6 = value;
		Il2CppCodeGenWriteBarrier(&___downAnimation_6, value);
	}

	inline static int32_t get_offset_of_landAnimation_7() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___landAnimation_7)); }
	inline AnimationClip_t3510324950 * get_landAnimation_7() const { return ___landAnimation_7; }
	inline AnimationClip_t3510324950 ** get_address_of_landAnimation_7() { return &___landAnimation_7; }
	inline void set_landAnimation_7(AnimationClip_t3510324950 * value)
	{
		___landAnimation_7 = value;
		Il2CppCodeGenWriteBarrier(&___landAnimation_7, value);
	}

	inline static int32_t get_offset_of_idleAnimation_8() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___idleAnimation_8)); }
	inline AnimationClip_t3510324950 * get_idleAnimation_8() const { return ___idleAnimation_8; }
	inline AnimationClip_t3510324950 ** get_address_of_idleAnimation_8() { return &___idleAnimation_8; }
	inline void set_idleAnimation_8(AnimationClip_t3510324950 * value)
	{
		___idleAnimation_8 = value;
		Il2CppCodeGenWriteBarrier(&___idleAnimation_8, value);
	}

	inline static int32_t get_offset_of_multiplier_9() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___multiplier_9)); }
	inline float get_multiplier_9() const { return ___multiplier_9; }
	inline float* get_address_of_multiplier_9() { return &___multiplier_9; }
	inline void set_multiplier_9(float value)
	{
		___multiplier_9 = value;
	}

	inline static int32_t get_offset_of_offset_10() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___offset_10)); }
	inline float get_offset_10() const { return ___offset_10; }
	inline float* get_address_of_offset_10() { return &___offset_10; }
	inline void set_offset_10(float value)
	{
		___offset_10 = value;
	}

	inline static int32_t get_offset_of_verticalSpeed_11() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___verticalSpeed_11)); }
	inline float get_verticalSpeed_11() const { return ___verticalSpeed_11; }
	inline float* get_address_of_verticalSpeed_11() { return &___verticalSpeed_11; }
	inline void set_verticalSpeed_11(float value)
	{
		___verticalSpeed_11 = value;
	}

	inline static int32_t get_offset_of_previousYPosition_12() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___previousYPosition_12)); }
	inline float get_previousYPosition_12() const { return ___previousYPosition_12; }
	inline float* get_address_of_previousYPosition_12() { return &___previousYPosition_12; }
	inline void set_previousYPosition_12(float value)
	{
		___previousYPosition_12 = value;
	}

	inline static int32_t get_offset_of_upDownBlend_14() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___upDownBlend_14)); }
	inline float get_upDownBlend_14() const { return ___upDownBlend_14; }
	inline float* get_address_of_upDownBlend_14() { return &___upDownBlend_14; }
	inline void set_upDownBlend_14(float value)
	{
		___upDownBlend_14 = value;
	}

	inline static int32_t get_offset_of_idleLandBlend_15() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___idleLandBlend_15)); }
	inline float get_idleLandBlend_15() const { return ___idleLandBlend_15; }
	inline float* get_address_of_idleLandBlend_15() { return &___idleLandBlend_15; }
	inline void set_idleLandBlend_15(float value)
	{
		___idleLandBlend_15 = value;
	}

	inline static int32_t get_offset_of_upDownBlendCurve_16() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___upDownBlendCurve_16)); }
	inline AnimationCurve_t3306541151 * get_upDownBlendCurve_16() const { return ___upDownBlendCurve_16; }
	inline AnimationCurve_t3306541151 ** get_address_of_upDownBlendCurve_16() { return &___upDownBlendCurve_16; }
	inline void set_upDownBlendCurve_16(AnimationCurve_t3306541151 * value)
	{
		___upDownBlendCurve_16 = value;
		Il2CppCodeGenWriteBarrier(&___upDownBlendCurve_16, value);
	}

	inline static int32_t get_offset_of_isLanding_17() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___isLanding_17)); }
	inline bool get_isLanding_17() const { return ___isLanding_17; }
	inline bool* get_address_of_isLanding_17() { return &___isLanding_17; }
	inline void set_isLanding_17(bool value)
	{
		___isLanding_17 = value;
	}

	inline static int32_t get_offset_of_landStartTime_19() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___landStartTime_19)); }
	inline float get_landStartTime_19() const { return ___landStartTime_19; }
	inline float* get_address_of_landStartTime_19() { return &___landStartTime_19; }
	inline void set_landStartTime_19(float value)
	{
		___landStartTime_19 = value;
	}

	inline static int32_t get_offset_of_isGrounded_20() { return static_cast<int32_t>(offsetof(JumpTest_t640307972, ___isGrounded_20)); }
	inline bool get_isGrounded_20() const { return ___isGrounded_20; }
	inline bool* get_address_of_isGrounded_20() { return &___isGrounded_20; }
	inline void set_isGrounded_20(bool value)
	{
		___isGrounded_20 = value;
	}
};

struct JumpTest_t640307972_StaticFields
{
public:
	// System.Single JumpTest::maxVerticalSpeed
	float ___maxVerticalSpeed_13;
	// System.Single JumpTest::landBlendDuration
	float ___landBlendDuration_18;

public:
	inline static int32_t get_offset_of_maxVerticalSpeed_13() { return static_cast<int32_t>(offsetof(JumpTest_t640307972_StaticFields, ___maxVerticalSpeed_13)); }
	inline float get_maxVerticalSpeed_13() const { return ___maxVerticalSpeed_13; }
	inline float* get_address_of_maxVerticalSpeed_13() { return &___maxVerticalSpeed_13; }
	inline void set_maxVerticalSpeed_13(float value)
	{
		___maxVerticalSpeed_13 = value;
	}

	inline static int32_t get_offset_of_landBlendDuration_18() { return static_cast<int32_t>(offsetof(JumpTest_t640307972_StaticFields, ___landBlendDuration_18)); }
	inline float get_landBlendDuration_18() const { return ___landBlendDuration_18; }
	inline float* get_address_of_landBlendDuration_18() { return &___landBlendDuration_18; }
	inline void set_landBlendDuration_18(float value)
	{
		___landBlendDuration_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
