﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TittleAnimation
struct  TittleAnimation_t2018927818  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform[] TittleAnimation::letters
	TransformU5BU5D_t3764228911* ___letters_2;
	// UnityEngine.Vector3[] TittleAnimation::letterStartPosition
	Vector3U5BU5D_t1172311765* ___letterStartPosition_3;
	// System.Single TittleAnimation::waveLength
	float ___waveLength_4;
	// System.Single TittleAnimation::waveSpeed
	float ___waveSpeed_5;
	// System.Single TittleAnimation::waveAmplitude
	float ___waveAmplitude_6;

public:
	inline static int32_t get_offset_of_letters_2() { return static_cast<int32_t>(offsetof(TittleAnimation_t2018927818, ___letters_2)); }
	inline TransformU5BU5D_t3764228911* get_letters_2() const { return ___letters_2; }
	inline TransformU5BU5D_t3764228911** get_address_of_letters_2() { return &___letters_2; }
	inline void set_letters_2(TransformU5BU5D_t3764228911* value)
	{
		___letters_2 = value;
		Il2CppCodeGenWriteBarrier(&___letters_2, value);
	}

	inline static int32_t get_offset_of_letterStartPosition_3() { return static_cast<int32_t>(offsetof(TittleAnimation_t2018927818, ___letterStartPosition_3)); }
	inline Vector3U5BU5D_t1172311765* get_letterStartPosition_3() const { return ___letterStartPosition_3; }
	inline Vector3U5BU5D_t1172311765** get_address_of_letterStartPosition_3() { return &___letterStartPosition_3; }
	inline void set_letterStartPosition_3(Vector3U5BU5D_t1172311765* value)
	{
		___letterStartPosition_3 = value;
		Il2CppCodeGenWriteBarrier(&___letterStartPosition_3, value);
	}

	inline static int32_t get_offset_of_waveLength_4() { return static_cast<int32_t>(offsetof(TittleAnimation_t2018927818, ___waveLength_4)); }
	inline float get_waveLength_4() const { return ___waveLength_4; }
	inline float* get_address_of_waveLength_4() { return &___waveLength_4; }
	inline void set_waveLength_4(float value)
	{
		___waveLength_4 = value;
	}

	inline static int32_t get_offset_of_waveSpeed_5() { return static_cast<int32_t>(offsetof(TittleAnimation_t2018927818, ___waveSpeed_5)); }
	inline float get_waveSpeed_5() const { return ___waveSpeed_5; }
	inline float* get_address_of_waveSpeed_5() { return &___waveSpeed_5; }
	inline void set_waveSpeed_5(float value)
	{
		___waveSpeed_5 = value;
	}

	inline static int32_t get_offset_of_waveAmplitude_6() { return static_cast<int32_t>(offsetof(TittleAnimation_t2018927818, ___waveAmplitude_6)); }
	inline float get_waveAmplitude_6() const { return ___waveAmplitude_6; }
	inline float* get_address_of_waveAmplitude_6() { return &___waveAmplitude_6; }
	inline void set_waveAmplitude_6(float value)
	{
		___waveAmplitude_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
