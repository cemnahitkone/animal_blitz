﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// winner
struct winner_t1142905907;

#include "codegen/il2cpp-codegen.h"

// System.Void winner::.ctor()
extern "C"  void winner__ctor_m1117566382 (winner_t1142905907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void winner::Awake()
extern "C"  void winner_Awake_m3002997699 (winner_t1142905907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void winner::Start()
extern "C"  void winner_Start_m2311782962 (winner_t1142905907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void winner::Update()
extern "C"  void winner_Update_m1183961471 (winner_t1142905907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
