﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_DotPar2282605056MethodDeclarations.h"

// System.Void Borodar.FarlandSkies.Core.DotParams.DotParamsList`1<Borodar.FarlandSkies.LowPoly.DotParams.MoonParam>::.ctor(System.Int32)
#define DotParamsList_1__ctor_m3194390645(__this, ___capacity0, method) ((  void (*) (DotParamsList_1_t1660012247 *, int32_t, const MethodInfo*))DotParamsList_1__ctor_m2564647030_gshared)(__this, ___capacity0, method)
// System.Int32 Borodar.FarlandSkies.Core.DotParams.DotParamsList`1<Borodar.FarlandSkies.LowPoly.DotParams.MoonParam>::FindIndexPerTime(System.Single)
#define DotParamsList_1_FindIndexPerTime_m3909672100(__this, ___time0, method) ((  int32_t (*) (DotParamsList_1_t1660012247 *, float, const MethodInfo*))DotParamsList_1_FindIndexPerTime_m910888095_gshared)(__this, ___time0, method)
