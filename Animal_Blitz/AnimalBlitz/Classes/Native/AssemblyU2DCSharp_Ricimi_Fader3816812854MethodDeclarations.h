﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.Fader
struct Fader_t3816812854;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.Fader::.ctor()
extern "C"  void Fader__ctor_m2833502624 (Fader_t3816812854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.Fader::Awake()
extern "C"  void Fader_Awake_m2204424549 (Fader_t3816812854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.Fader::FadeIn()
extern "C"  void Fader_FadeIn_m1302740925 (Fader_t3816812854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.Fader::FadeOut()
extern "C"  void Fader_FadeOut_m3652819066 (Fader_t3816812854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Ricimi.Fader::RunFadeIn()
extern "C"  Il2CppObject * Fader_RunFadeIn_m3737529388 (Fader_t3816812854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Ricimi.Fader::RunFadeOut()
extern "C"  Il2CppObject * Fader_RunFadeOut_m2997532011 (Fader_t3816812854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
