﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.MuteBackgroundMusic
struct MuteBackgroundMusic_t1333099764;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.MuteBackgroundMusic::.ctor()
extern "C"  void MuteBackgroundMusic__ctor_m2205647936 (MuteBackgroundMusic_t1333099764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.MuteBackgroundMusic::Awake()
extern "C"  void MuteBackgroundMusic_Awake_m322964067 (MuteBackgroundMusic_t1333099764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.MuteBackgroundMusic::OnDestroy()
extern "C"  void MuteBackgroundMusic_OnDestroy_m2520306573 (MuteBackgroundMusic_t1333099764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
