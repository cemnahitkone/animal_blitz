﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIActions467790635.h"
#include "AssemblyU2DCSharp_UIDetection3199971681.h"
#include "AssemblyU2DCSharp_UILineRenderer1431191837.h"
#include "AssemblyU2DCSharp_testBle3025851879.h"
#include "AssemblyU2DCSharp_testBle_baseLegID1303619648.h"
#include "AssemblyU2DCSharp_testBle_U3CConnectU3Ec__AnonStore350889135.h"
#include "AssemblyU2DCSharp_TrailActivationCondition372043875.h"
#include "AssemblyU2DCSharp_TrailDisactivationCondition1222614977.h"
#include "AssemblyU2DCSharp_SpriteTrail2284883325.h"
#include "AssemblyU2DCSharp_TrailElement1685395368.h"
#include "AssemblyU2DCSharp_TrailElementSpawnCondition2660531040.h"
#include "AssemblyU2DCSharp_TrailElementDurationCondition3016874467.h"
#include "AssemblyU2DCSharp_TrailPreset465077881.h"
#include "AssemblyU2DCSharp_BasicPath2288617877.h"
#include "AssemblyU2DCSharp_ChangeScene3263655284.h"
#include "AssemblyU2DCSharp_ChangeTrailPreset1471293943.h"
#include "AssemblyU2DCSharp_EnableTrailOnKeyDown649960077.h"
#include "AssemblyU2DCSharp_MoveItem4038948586.h"
#include "AssemblyU2DCSharp_Rotate4255939431.h"
#include "AssemblyU2DCSharp_SimpleCharacterControler2D2053125211.h"
#include "AssemblyU2DCSharp_TestSceneManager2345075657.h"
#include "AssemblyU2DCSharp_TrailManagerExample3762595367.h"
#include "AssemblyU2DCSharp_winner1142905907.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_AnimationSelect1701553070.h"
#include "AssemblyU2DUnityScript_BackgroundScroll1444628405.h"
#include "AssemblyU2DUnityScript_SelectedButton2241840337.h"
#include "AssemblyU2DUnityScript_TittleAnimation2018927818.h"
#include "AssemblyU2DUnityScript_BlendTwoAnimations1525971836.h"
#include "AssemblyU2DUnityScript_CharacterSpin1044941751.h"
#include "AssemblyU2DUnityScript_Cursor2213425178.h"
#include "AssemblyU2DUnityScript_HideCursor48087058.h"
#include "AssemblyU2DUnityScript_JumpOnClick2456543707.h"
#include "AssemblyU2DUnityScript_JumpTest640307972.h"
#include "AssemblyU2DUnityScript_Rotate4255939431.h"
#include "AssemblyU2DUnityScript_Shadow2876782136.h"
#include "AssemblyU2DUnityScript_SpeedTilt451906044.h"
#include "AssemblyU2DUnityScript_Translate3008136214.h"
#include "AssemblyU2DUnityScript_WaveMotion1407033065.h"
#include "AssemblyU2DUnityScript_CharacterMotorMovement944153967.h"
#include "AssemblyU2DUnityScript_MovementTransferOnJump3438008145.h"
#include "AssemblyU2DUnityScript_CharacterMotorJumping1708272304.h"
#include "AssemblyU2DUnityScript_CharacterMotorMovingPlatform365475463.h"
#include "AssemblyU2DUnityScript_CharacterMotorSliding3749388514.h"
#include "AssemblyU2DUnityScript_CharacterMotor262030084.h"
#include "AssemblyU2DUnityScript_CharacterMotor_U24SubtractN3518813419.h"
#include "AssemblyU2DUnityScript_CharacterMotor_U24SubtractN1079724576.h"
#include "AssemblyU2DUnityScript_FPSInputController4241249601.h"
#include "AssemblyU2DUnityScript_PlatformInputController4273899755.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (UIActions_t467790635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[6] = 
{
	UIActions_t467790635::get_offset_of_leftCurtain_2(),
	UIActions_t467790635::get_offset_of_leftCurtainReverse_3(),
	UIActions_t467790635::get_offset_of_rightCurtain_4(),
	UIActions_t467790635::get_offset_of_rightCurtainReverse_5(),
	UIActions_t467790635::get_offset_of_curtainCloseSound_6(),
	UIActions_t467790635::get_offset_of_anim_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (UIDetection_t3199971681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[1] = 
{
	UIDetection_t3199971681::get_offset_of_gameCo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (UILineRenderer_t1431191837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[7] = 
{
	UILineRenderer_t1431191837::get_offset_of_m_Texture_19(),
	UILineRenderer_t1431191837::get_offset_of_m_UVRect_20(),
	UILineRenderer_t1431191837::get_offset_of_LineThickness_21(),
	UILineRenderer_t1431191837::get_offset_of_UseMargins_22(),
	UILineRenderer_t1431191837::get_offset_of_Margin_23(),
	UILineRenderer_t1431191837::get_offset_of_Points_24(),
	UILineRenderer_t1431191837::get_offset_of_relativeSize_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (testBle_t3025851879), -1, sizeof(testBle_t3025851879_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2703[18] = 
{
	testBle_t3025851879_StaticFields::get_offset_of__connectedID_2(),
	testBle_t3025851879_StaticFields::get_offset_of__serviceUUID_3(),
	testBle_t3025851879_StaticFields::get_offset_of__writeCharacteristicUUID_4(),
	testBle_t3025851879::get_offset_of__readCharacteristicUUID_5(),
	testBle_t3025851879::get_offset_of__address_6(),
	testBle_t3025851879::get_offset_of__name_7(),
	testBle_t3025851879_StaticFields::get_offset_of_BleDataDone_8(),
	testBle_t3025851879::get_offset_of_BleDataNew_9(),
	testBle_t3025851879::get_offset_of_dataState_10(),
	testBle_t3025851879::get_offset_of_idList_11(),
	testBle_t3025851879_StaticFields::get_offset_of__uniqID_12(),
	testBle_t3025851879_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
	testBle_t3025851879_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_14(),
	testBle_t3025851879_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_15(),
	testBle_t3025851879_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_16(),
	testBle_t3025851879_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_17(),
	testBle_t3025851879_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_18(),
	testBle_t3025851879_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (baseLegID_t1303619648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[3] = 
{
	baseLegID_t1303619648::get_offset_of_id_0(),
	baseLegID_t1303619648::get_offset_of_x_1(),
	baseLegID_t1303619648::get_offset_of_y_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (U3CConnectU3Ec__AnonStorey0_t350889135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[2] = 
{
	U3CConnectU3Ec__AnonStorey0_t350889135::get_offset_of_characteristicUUID_0(),
	U3CConnectU3Ec__AnonStorey0_t350889135::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (TrailActivationCondition_t372043875)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2706[4] = 
{
	TrailActivationCondition_t372043875::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (TrailDisactivationCondition_t1222614977)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2707[4] = 
{
	TrailDisactivationCondition_t1222614977::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (SpriteTrail_t2284883325), -1, sizeof(SpriteTrail_t2284883325_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2708[38] = 
{
	SpriteTrail_t2284883325::get_offset_of_m_TrailName_2(),
	SpriteTrail_t2284883325::get_offset_of_m_CurrentTrailPreset_3(),
	SpriteTrail_t2284883325::get_offset_of_m_TrailParent_4(),
	SpriteTrail_t2284883325::get_offset_of_m_SpriteToDuplicate_5(),
	SpriteTrail_t2284883325::get_offset_of_m_LayerName_6(),
	SpriteTrail_t2284883325::get_offset_of_m_ZMoveStep_7(),
	SpriteTrail_t2284883325::get_offset_of_m_ZMoveMax_8(),
	SpriteTrail_t2284883325::get_offset_of_m_TrailOrderInLayer_9(),
	SpriteTrail_t2284883325::get_offset_of_m_HideTrailOnDisabled_10(),
	SpriteTrail_t2284883325::get_offset_of_m_TrailActivationCondition_11(),
	SpriteTrail_t2284883325::get_offset_of_m_TrailDisactivationCondition_12(),
	SpriteTrail_t2284883325::get_offset_of_m_StartIfUnderVelocity_13(),
	SpriteTrail_t2284883325::get_offset_of_m_VelocityStartIsLocalSpace_14(),
	SpriteTrail_t2284883325::get_offset_of_m_VelocityNeededToStart_15(),
	SpriteTrail_t2284883325::get_offset_of_m_StopIfOverVelocity_16(),
	SpriteTrail_t2284883325::get_offset_of_m_VelocityStopIsLocalSpace_17(),
	SpriteTrail_t2284883325::get_offset_of_m_VelocityNeededToStop_18(),
	SpriteTrail_t2284883325::get_offset_of_m_TrailActivationDuration_19(),
	SpriteTrail_t2284883325::get_offset_of_m_ElementsInTrail_20(),
	SpriteTrail_t2284883325_StaticFields::get_offset_of_m_TrailCount_21(),
	SpriteTrail_t2284883325::get_offset_of_m_PreviousTrailPreset_22(),
	SpriteTrail_t2284883325_StaticFields::get_offset_of_m_TrailElementPrefab_23(),
	SpriteTrail_t2284883325::get_offset_of_m_LocalTrailContainer_24(),
	SpriteTrail_t2284883325::get_offset_of_m_GlobalTrailContainer_25(),
	SpriteTrail_t2284883325::get_offset_of_m_CurrentDisplacement_26(),
	SpriteTrail_t2284883325::get_offset_of_m_TimeTrailStarted_27(),
	SpriteTrail_t2284883325::get_offset_of_m_PreviousTimeSpawned_28(),
	SpriteTrail_t2284883325::get_offset_of_m_PreviousFrameSpawned_29(),
	SpriteTrail_t2284883325::get_offset_of_m_PreviousPosSpawned_30(),
	SpriteTrail_t2284883325::get_offset_of_m_PreviousFrameLocalPos_31(),
	SpriteTrail_t2284883325::get_offset_of_m_PreviousFrameWorldPos_32(),
	SpriteTrail_t2284883325::get_offset_of_m_VelocityLocal_33(),
	SpriteTrail_t2284883325::get_offset_of_m_VelocityWorld_34(),
	SpriteTrail_t2284883325::get_offset_of_m_FirstVelocityCheck_35(),
	SpriteTrail_t2284883325::get_offset_of_m_CanBeAutomaticallyActivated_36(),
	SpriteTrail_t2284883325::get_offset_of_m_EffectEnabled_37(),
	SpriteTrail_t2284883325::get_offset_of_m_WillBeActivatedOnEnable_38(),
	SpriteTrail_t2284883325_StaticFields::get_offset_of_m_LevelRefreshDone_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (TrailElement_t1685395368), -1, sizeof(TrailElement_t1685395368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2709[14] = 
{
	TrailElement_t1685395368_StaticFields::get_offset_of_m_FreeElements_2(),
	TrailElement_t1685395368_StaticFields::get_offset_of_m_ItemCount_3(),
	TrailElement_t1685395368::get_offset_of_m_ItemId_4(),
	TrailElement_t1685395368::get_offset_of_m_TrailSettings_5(),
	TrailElement_t1685395368::get_offset_of_m_TimeSinceCreation_6(),
	TrailElement_t1685395368::get_offset_of_m_SpRenderer_7(),
	TrailElement_t1685395368::get_offset_of_m_Init_8(),
	TrailElement_t1685395368::get_offset_of_m_InitSize_9(),
	TrailElement_t1685395368::get_offset_of_m_InitPos_10(),
	TrailElement_t1685395368::get_offset_of_m_MotherTrail_11(),
	TrailElement_t1685395368::get_offset_of_m_NeedLateUpdate_12(),
	TrailElement_t1685395368::get_offset_of_m_TrailPos_13(),
	TrailElement_t1685395368::get_offset_of_m_NeedDequeue_14(),
	TrailElement_t1685395368::get_offset_of_m_Transform_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (TrailElementSpawnCondition_t2660531040)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2710[4] = 
{
	TrailElementSpawnCondition_t2660531040::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (TrailElementDurationCondition_t3016874467)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2711[3] = 
{
	TrailElementDurationCondition_t3016874467::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (TrailPreset_t465077881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2712[17] = 
{
	TrailPreset_t465077881::get_offset_of_m_SpecialMat_2(),
	TrailPreset_t465077881::get_offset_of_m_UseOnlyAlpha_3(),
	TrailPreset_t465077881::get_offset_of_m_TrailColor_4(),
	TrailPreset_t465077881::get_offset_of_m_TrailElementDurationCondition_5(),
	TrailPreset_t465077881::get_offset_of_m_TrailMaxLength_6(),
	TrailPreset_t465077881::get_offset_of_m_TrailDuration_7(),
	TrailPreset_t465077881::get_offset_of_m_TrailElementSpawnCondition_8(),
	TrailPreset_t465077881::get_offset_of_m_TimeBetweenSpawns_9(),
	TrailPreset_t465077881::get_offset_of_m_FramesBetweenSpawns_10(),
	TrailPreset_t465077881::get_offset_of_m_DistanceBetweenSpawns_11(),
	TrailPreset_t465077881::get_offset_of_m_DistanceCorrection_12(),
	TrailPreset_t465077881::get_offset_of_m_UseSizeModifier_13(),
	TrailPreset_t465077881::get_offset_of_m_UsePositionModifier_14(),
	TrailPreset_t465077881::get_offset_of_m_TrailSizeX_15(),
	TrailPreset_t465077881::get_offset_of_m_TrailSizeY_16(),
	TrailPreset_t465077881::get_offset_of_m_TrailPositionX_17(),
	TrailPreset_t465077881::get_offset_of_m_TrailPositionY_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (BasicPath_t2288617877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[9] = 
{
	BasicPath_t2288617877::get_offset_of_m_Lerp_2(),
	BasicPath_t2288617877::get_offset_of_m_CurSpeed_3(),
	BasicPath_t2288617877::get_offset_of_m_Waypoints_4(),
	BasicPath_t2288617877::get_offset_of_m_CurWayPointIndex_5(),
	BasicPath_t2288617877::get_offset_of_m_ClampFloor_6(),
	BasicPath_t2288617877::get_offset_of_m_Trail_7(),
	BasicPath_t2288617877::get_offset_of_m_FlipX_8(),
	BasicPath_t2288617877::get_offset_of_FlipXOnEnd_9(),
	BasicPath_t2288617877::get_offset_of_m_ItemToMove_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (ChangeScene_t3263655284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (ChangeTrailPreset_t1471293943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2715[4] = 
{
	ChangeTrailPreset_t1471293943::get_offset_of_m_Trail_2(),
	ChangeTrailPreset_t1471293943::get_offset_of_m_Presets_3(),
	ChangeTrailPreset_t1471293943::get_offset_of_m_CurrentPresetIndex_4(),
	ChangeTrailPreset_t1471293943::get_offset_of_m_PreviousPresetIndex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (EnableTrailOnKeyDown_t649960077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2716[3] = 
{
	EnableTrailOnKeyDown_t649960077::get_offset_of_m_EnableKey_2(),
	EnableTrailOnKeyDown_t649960077::get_offset_of_m_DisableKey_3(),
	EnableTrailOnKeyDown_t649960077::get_offset_of_m_Trail_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (MoveItem_t4038948586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2717[1] = 
{
	MoveItem_t4038948586::get_offset_of_m_Speed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (Rotate_t4255939431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2718[1] = 
{
	Rotate_t4255939431::get_offset_of_m_RotationSpeed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (SimpleCharacterControler2D_t2053125211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[3] = 
{
	SimpleCharacterControler2D_t2053125211::get_offset_of_m_RigidBody_2(),
	SimpleCharacterControler2D_t2053125211::get_offset_of_m_Force_3(),
	SimpleCharacterControler2D_t2053125211::get_offset_of_m_MaxVelocityToBoost_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (TestSceneManager_t2345075657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2720[2] = 
{
	TestSceneManager_t2345075657::get_offset_of_m_Containers_2(),
	TestSceneManager_t2345075657::get_offset_of_m_CurrentIndex_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (TrailManagerExample_t3762595367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2721[4] = 
{
	TrailManagerExample_t3762595367::get_offset_of_m_Trails_2(),
	TrailManagerExample_t3762595367::get_offset_of_m_CurrentTrailIndex_3(),
	TrailManagerExample_t3762595367::get_offset_of_m_Character_4(),
	TrailManagerExample_t3762595367::get_offset_of_m_UI_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (winner_t1142905907), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (U3CModuleU3E_t3783534229), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (AnimationSelect_t1701553070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2724[19] = 
{
	AnimationSelect_t1701553070::get_offset_of_sceneCamera_2(),
	AnimationSelect_t1701553070::get_offset_of_currentAnimation_3(),
	AnimationSelect_t1701553070::get_offset_of_dog_4(),
	AnimationSelect_t1701553070::get_offset_of_cat_5(),
	AnimationSelect_t1701553070::get_offset_of_turtle_6(),
	AnimationSelect_t1701553070::get_offset_of_owl_7(),
	AnimationSelect_t1701553070::get_offset_of_rat_8(),
	AnimationSelect_t1701553070::get_offset_of_idleButton_9(),
	AnimationSelect_t1701553070::get_offset_of_talkButton_10(),
	AnimationSelect_t1701553070::get_offset_of_rollingButton_11(),
	AnimationSelect_t1701553070::get_offset_of_successButton_12(),
	AnimationSelect_t1701553070::get_offset_of_jumpButton_13(),
	AnimationSelect_t1701553070::get_offset_of_idle2Button_14(),
	AnimationSelect_t1701553070::get_offset_of_runButton_15(),
	AnimationSelect_t1701553070::get_offset_of_failureButton_16(),
	AnimationSelect_t1701553070::get_offset_of_sleepButton_17(),
	AnimationSelect_t1701553070::get_offset_of_walkButton_18(),
	AnimationSelect_t1701553070::get_offset_of_selectedButton_19(),
	AnimationSelect_t1701553070::get_offset_of_selectedButtonScript_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (BackgroundScroll_t1444628405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (SelectedButton_t2241840337), -1, sizeof(SelectedButton_t2241840337_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2726[6] = 
{
	SelectedButton_t2241840337::get_offset_of_animationStart_2(),
	SelectedButton_t2241840337::get_offset_of_currentFrame_3(),
	SelectedButton_t2241840337::get_offset_of_animationTime_4(),
	SelectedButton_t2241840337::get_offset_of_textureOffset_5(),
	SelectedButton_t2241840337::get_offset_of_animationSpeed_6(),
	SelectedButton_t2241840337_StaticFields::get_offset_of_maxFrame_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (TittleAnimation_t2018927818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[5] = 
{
	TittleAnimation_t2018927818::get_offset_of_letters_2(),
	TittleAnimation_t2018927818::get_offset_of_letterStartPosition_3(),
	TittleAnimation_t2018927818::get_offset_of_waveLength_4(),
	TittleAnimation_t2018927818::get_offset_of_waveSpeed_5(),
	TittleAnimation_t2018927818::get_offset_of_waveAmplitude_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (BlendTwoAnimations_t1525971836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[3] = 
{
	BlendTwoAnimations_t1525971836::get_offset_of_firstAnimation_2(),
	BlendTwoAnimations_t1525971836::get_offset_of_secondAnimation_3(),
	BlendTwoAnimations_t1525971836::get_offset_of_blend_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (CharacterSpin_t1044941751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2729[4] = 
{
	CharacterSpin_t1044941751::get_offset_of_sceneCamera_2(),
	CharacterSpin_t1044941751::get_offset_of_speed_3(),
	CharacterSpin_t1044941751::get_offset_of_bias_4(),
	CharacterSpin_t1044941751::get_offset_of_facingCamera_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (Cursor_t2213425178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[6] = 
{
	Cursor_t2213425178::get_offset_of_sceneCamera_2(),
	Cursor_t2213425178::get_offset_of_offset_3(),
	Cursor_t2213425178::get_offset_of_defaultTexture_4(),
	Cursor_t2213425178::get_offset_of_clickTexture_5(),
	Cursor_t2213425178::get_offset_of_clickImageDuration_6(),
	Cursor_t2213425178::get_offset_of_lastClickTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (HideCursor_t48087058), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (JumpOnClick_t2456543707), -1, sizeof(JumpOnClick_t2456543707_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2732[5] = 
{
	JumpOnClick_t2456543707::get_offset_of_sceneCamera_2(),
	JumpOnClick_t2456543707::get_offset_of_groundY_3(),
	JumpOnClick_t2456543707::get_offset_of_ySpeed_4(),
	JumpOnClick_t2456543707_StaticFields::get_offset_of_gravity_5(),
	JumpOnClick_t2456543707_StaticFields::get_offset_of_jumpSpeed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (JumpTest_t640307972), -1, sizeof(JumpTest_t640307972_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2733[19] = 
{
	JumpTest_t640307972::get_offset_of_startTime_2(),
	JumpTest_t640307972::get_offset_of_debug_3(),
	JumpTest_t640307972::get_offset_of_heightCurve_4(),
	JumpTest_t640307972::get_offset_of_upAnimation_5(),
	JumpTest_t640307972::get_offset_of_downAnimation_6(),
	JumpTest_t640307972::get_offset_of_landAnimation_7(),
	JumpTest_t640307972::get_offset_of_idleAnimation_8(),
	JumpTest_t640307972::get_offset_of_multiplier_9(),
	JumpTest_t640307972::get_offset_of_offset_10(),
	JumpTest_t640307972::get_offset_of_verticalSpeed_11(),
	JumpTest_t640307972::get_offset_of_previousYPosition_12(),
	JumpTest_t640307972_StaticFields::get_offset_of_maxVerticalSpeed_13(),
	JumpTest_t640307972::get_offset_of_upDownBlend_14(),
	JumpTest_t640307972::get_offset_of_idleLandBlend_15(),
	JumpTest_t640307972::get_offset_of_upDownBlendCurve_16(),
	JumpTest_t640307972::get_offset_of_isLanding_17(),
	JumpTest_t640307972_StaticFields::get_offset_of_landBlendDuration_18(),
	JumpTest_t640307972::get_offset_of_landStartTime_19(),
	JumpTest_t640307972::get_offset_of_isGrounded_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (Rotate_t4255939432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2734[2] = 
{
	Rotate_t4255939432::get_offset_of_eulerAngles_2(),
	Rotate_t4255939432::get_offset_of_relativeTo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (Shadow_t2876782136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[6] = 
{
	Shadow_t2876782136::get_offset_of_distanceTolerance_2(),
	Shadow_t2876782136::get_offset_of_maxOpacity_3(),
	Shadow_t2876782136::get_offset_of_multiplier_4(),
	Shadow_t2876782136::get_offset_of_opacity_5(),
	Shadow_t2876782136::get_offset_of_castingPoint_6(),
	Shadow_t2876782136::get_offset_of_buffer_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (SpeedTilt_t451906044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2736[8] = 
{
	SpeedTilt_t451906044::get_offset_of_multiplier_2(),
	SpeedTilt_t451906044::get_offset_of_tiltDamp_3(),
	SpeedTilt_t451906044::get_offset_of_startingRotation_4(),
	SpeedTilt_t451906044::get_offset_of_currentTilt_5(),
	SpeedTilt_t451906044::get_offset_of_targetTilt_6(),
	SpeedTilt_t451906044::get_offset_of_velocityTilt_7(),
	SpeedTilt_t451906044::get_offset_of_currentXspeed_8(),
	SpeedTilt_t451906044::get_offset_of_previousXPosition_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (Translate_t3008136214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[2] = 
{
	Translate_t3008136214::get_offset_of_translation_2(),
	Translate_t3008136214::get_offset_of_relativeTo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (WaveMotion_t1407033065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2738[5] = 
{
	WaveMotion_t1407033065::get_offset_of_speed_2(),
	WaveMotion_t1407033065::get_offset_of_multiplier_3(),
	WaveMotion_t1407033065::get_offset_of_offset_4(),
	WaveMotion_t1407033065::get_offset_of_startingRotation_5(),
	WaveMotion_t1407033065::get_offset_of_waveAngle_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (CharacterMotorMovement_t944153967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[13] = 
{
	CharacterMotorMovement_t944153967::get_offset_of_maxForwardSpeed_0(),
	CharacterMotorMovement_t944153967::get_offset_of_maxSidewaysSpeed_1(),
	CharacterMotorMovement_t944153967::get_offset_of_maxBackwardsSpeed_2(),
	CharacterMotorMovement_t944153967::get_offset_of_slopeSpeedMultiplier_3(),
	CharacterMotorMovement_t944153967::get_offset_of_maxGroundAcceleration_4(),
	CharacterMotorMovement_t944153967::get_offset_of_maxAirAcceleration_5(),
	CharacterMotorMovement_t944153967::get_offset_of_gravity_6(),
	CharacterMotorMovement_t944153967::get_offset_of_maxFallSpeed_7(),
	CharacterMotorMovement_t944153967::get_offset_of_collisionFlags_8(),
	CharacterMotorMovement_t944153967::get_offset_of_velocity_9(),
	CharacterMotorMovement_t944153967::get_offset_of_frameVelocity_10(),
	CharacterMotorMovement_t944153967::get_offset_of_hitPoint_11(),
	CharacterMotorMovement_t944153967::get_offset_of_lastHitPoint_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (MovementTransferOnJump_t3438008145)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2740[5] = 
{
	MovementTransferOnJump_t3438008145::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (CharacterMotorJumping_t1708272304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2741[10] = 
{
	CharacterMotorJumping_t1708272304::get_offset_of_enabled_0(),
	CharacterMotorJumping_t1708272304::get_offset_of_baseHeight_1(),
	CharacterMotorJumping_t1708272304::get_offset_of_extraHeight_2(),
	CharacterMotorJumping_t1708272304::get_offset_of_perpAmount_3(),
	CharacterMotorJumping_t1708272304::get_offset_of_steepPerpAmount_4(),
	CharacterMotorJumping_t1708272304::get_offset_of_jumping_5(),
	CharacterMotorJumping_t1708272304::get_offset_of_holdingJumpButton_6(),
	CharacterMotorJumping_t1708272304::get_offset_of_lastStartTime_7(),
	CharacterMotorJumping_t1708272304::get_offset_of_lastButtonDownTime_8(),
	CharacterMotorJumping_t1708272304::get_offset_of_jumpDir_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (CharacterMotorMovingPlatform_t365475463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2742[11] = 
{
	CharacterMotorMovingPlatform_t365475463::get_offset_of_enabled_0(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_movementTransfer_1(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_hitPlatform_2(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_activePlatform_3(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_activeLocalPoint_4(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_activeGlobalPoint_5(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_activeLocalRotation_6(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_activeGlobalRotation_7(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_lastMatrix_8(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_platformVelocity_9(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_newPlatform_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (CharacterMotorSliding_t3749388514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[4] = 
{
	CharacterMotorSliding_t3749388514::get_offset_of_enabled_0(),
	CharacterMotorSliding_t3749388514::get_offset_of_slidingSpeed_1(),
	CharacterMotorSliding_t3749388514::get_offset_of_sidewaysControl_2(),
	CharacterMotorSliding_t3749388514::get_offset_of_speedControl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (CharacterMotor_t262030084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2744[13] = 
{
	CharacterMotor_t262030084::get_offset_of_canControl_2(),
	CharacterMotor_t262030084::get_offset_of_useFixedUpdate_3(),
	CharacterMotor_t262030084::get_offset_of_inputMoveDirection_4(),
	CharacterMotor_t262030084::get_offset_of_inputJump_5(),
	CharacterMotor_t262030084::get_offset_of_movement_6(),
	CharacterMotor_t262030084::get_offset_of_jumping_7(),
	CharacterMotor_t262030084::get_offset_of_movingPlatform_8(),
	CharacterMotor_t262030084::get_offset_of_sliding_9(),
	CharacterMotor_t262030084::get_offset_of_grounded_10(),
	CharacterMotor_t262030084::get_offset_of_groundNormal_11(),
	CharacterMotor_t262030084::get_offset_of_lastGroundNormal_12(),
	CharacterMotor_t262030084::get_offset_of_tr_13(),
	CharacterMotor_t262030084::get_offset_of_controller_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (U24SubtractNewPlatformVelocityU2419_t3518813419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[1] = 
{
	U24SubtractNewPlatformVelocityU2419_t3518813419::get_offset_of_U24self_U2422_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (U24_t1079724576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[2] = 
{
	U24_t1079724576::get_offset_of_U24platformU2420_2(),
	U24_t1079724576::get_offset_of_U24self_U2421_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (FPSInputController_t4241249601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[1] = 
{
	FPSInputController_t4241249601::get_offset_of_motor_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (PlatformInputController_t4273899755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2748[3] = 
{
	PlatformInputController_t4273899755::get_offset_of_autoRotate_2(),
	PlatformInputController_t4273899755::get_offset_of_maxRotationSpeed_3(),
	PlatformInputController_t4273899755::get_offset_of_motor_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
