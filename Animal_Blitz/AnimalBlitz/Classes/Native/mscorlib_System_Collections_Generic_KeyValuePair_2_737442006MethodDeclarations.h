﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,TabBase/baseLegID>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3062981931(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t737442006 *, int32_t, baseLegID_t3972271149 *, const MethodInfo*))KeyValuePair_2__ctor_m3201181706_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,TabBase/baseLegID>::get_Key()
#define KeyValuePair_2_get_Key_m3552922781(__this, method) ((  int32_t (*) (KeyValuePair_2_t737442006 *, const MethodInfo*))KeyValuePair_2_get_Key_m1435832840_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,TabBase/baseLegID>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2344542432(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t737442006 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1350990071_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,TabBase/baseLegID>::get_Value()
#define KeyValuePair_2_get_Value_m1320033189(__this, method) ((  baseLegID_t3972271149 * (*) (KeyValuePair_2_t737442006 *, const MethodInfo*))KeyValuePair_2_get_Value_m3690000728_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,TabBase/baseLegID>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2742905112(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t737442006 *, baseLegID_t3972271149 *, const MethodInfo*))KeyValuePair_2_set_Value_m2726037047_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,TabBase/baseLegID>::ToString()
#define KeyValuePair_2_ToString_m4123914730(__this, method) ((  String_t* (*) (KeyValuePair_2_t737442006 *, const MethodInfo*))KeyValuePair_2_ToString_m1391611625_gshared)(__this, method)
