﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,tBase>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3039225447(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2048810916 *, Dictionary_2_t728786214 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,tBase>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m600273270(__this, method) ((  Il2CppObject * (*) (Enumerator_t2048810916 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,tBase>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3778823368(__this, method) ((  void (*) (Enumerator_t2048810916 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,tBase>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3862243409(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2048810916 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,tBase>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1058621916(__this, method) ((  Il2CppObject * (*) (Enumerator_t2048810916 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,tBase>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1397047502(__this, method) ((  Il2CppObject * (*) (Enumerator_t2048810916 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,tBase>::MoveNext()
#define Enumerator_MoveNext_m420253381(__this, method) ((  bool (*) (Enumerator_t2048810916 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,tBase>::get_Current()
#define Enumerator_get_Current_m12816146(__this, method) ((  KeyValuePair_2_t2781098732  (*) (Enumerator_t2048810916 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,tBase>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m10561851(__this, method) ((  int32_t (*) (Enumerator_t2048810916 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,tBase>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4251122619(__this, method) ((  tBase_t1720960579 * (*) (Enumerator_t2048810916 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,tBase>::Reset()
#define Enumerator_Reset_m3779823569(__this, method) ((  void (*) (Enumerator_t2048810916 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,tBase>::VerifyState()
#define Enumerator_VerifyState_m825905916(__this, method) ((  void (*) (Enumerator_t2048810916 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,tBase>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1563512560(__this, method) ((  void (*) (Enumerator_t2048810916 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,tBase>::Dispose()
#define Enumerator_Dispose_m3027320499(__this, method) ((  void (*) (Enumerator_t2048810916 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
