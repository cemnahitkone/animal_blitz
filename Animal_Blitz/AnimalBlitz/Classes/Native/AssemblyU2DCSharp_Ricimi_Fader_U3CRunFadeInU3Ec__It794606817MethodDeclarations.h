﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.Fader/<RunFadeIn>c__Iterator0
struct U3CRunFadeInU3Ec__Iterator0_t794606817;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.Fader/<RunFadeIn>c__Iterator0::.ctor()
extern "C"  void U3CRunFadeInU3Ec__Iterator0__ctor_m2545679218 (U3CRunFadeInU3Ec__Iterator0_t794606817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ricimi.Fader/<RunFadeIn>c__Iterator0::MoveNext()
extern "C"  bool U3CRunFadeInU3Ec__Iterator0_MoveNext_m2439847158 (U3CRunFadeInU3Ec__Iterator0_t794606817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Ricimi.Fader/<RunFadeIn>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRunFadeInU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3837517978 (U3CRunFadeInU3Ec__Iterator0_t794606817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Ricimi.Fader/<RunFadeIn>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRunFadeInU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m496237474 (U3CRunFadeInU3Ec__Iterator0_t794606817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.Fader/<RunFadeIn>c__Iterator0::Dispose()
extern "C"  void U3CRunFadeInU3Ec__Iterator0_Dispose_m620355003 (U3CRunFadeInU3Ec__Iterator0_t794606817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.Fader/<RunFadeIn>c__Iterator0::Reset()
extern "C"  void U3CRunFadeInU3Ec__Iterator0_Reset_m1470911761 (U3CRunFadeInU3Ec__Iterator0_t794606817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
