﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlendTwoAnimations
struct BlendTwoAnimations_t1525971836;

#include "codegen/il2cpp-codegen.h"

// System.Void BlendTwoAnimations::.ctor()
extern "C"  void BlendTwoAnimations__ctor_m1463316726 (BlendTwoAnimations_t1525971836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlendTwoAnimations::Start()
extern "C"  void BlendTwoAnimations_Start_m1690927234 (BlendTwoAnimations_t1525971836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlendTwoAnimations::Update()
extern "C"  void BlendTwoAnimations_Update_m3816926907 (BlendTwoAnimations_t1525971836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlendTwoAnimations::Main()
extern "C"  void BlendTwoAnimations_Main_m2689698233 (BlendTwoAnimations_t1525971836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
