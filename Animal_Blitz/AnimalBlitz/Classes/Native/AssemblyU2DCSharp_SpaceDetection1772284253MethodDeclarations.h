﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpaceDetection
struct SpaceDetection_t1772284253;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void SpaceDetection::.ctor()
extern "C"  void SpaceDetection__ctor_m3473158040 (SpaceDetection_t1772284253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpaceDetection::Start()
extern "C"  void SpaceDetection_Start_m329268168 (SpaceDetection_t1772284253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpaceDetection::Update()
extern "C"  void SpaceDetection_Update_m3454572105 (SpaceDetection_t1772284253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpaceDetection::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void SpaceDetection_OnTriggerEnter_m3888486468 (SpaceDetection_t1772284253 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpaceDetection::OnTriggerExit(UnityEngine.Collider)
extern "C"  void SpaceDetection_OnTriggerExit_m3278577982 (SpaceDetection_t1772284253 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
