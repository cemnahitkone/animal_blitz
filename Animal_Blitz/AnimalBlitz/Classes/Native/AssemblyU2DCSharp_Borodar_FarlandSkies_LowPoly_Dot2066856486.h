﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.DotParams.MoonParam
struct  MoonParam_t2066856486  : public Il2CppObject
{
public:
	// System.Single Borodar.FarlandSkies.LowPoly.DotParams.MoonParam::Time
	float ___Time_0;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.DotParams.MoonParam::TintColor
	Color_t2020392075  ___TintColor_1;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.DotParams.MoonParam::LightColor
	Color_t2020392075  ___LightColor_2;
	// System.Single Borodar.FarlandSkies.LowPoly.DotParams.MoonParam::LightIntencity
	float ___LightIntencity_3;

public:
	inline static int32_t get_offset_of_Time_0() { return static_cast<int32_t>(offsetof(MoonParam_t2066856486, ___Time_0)); }
	inline float get_Time_0() const { return ___Time_0; }
	inline float* get_address_of_Time_0() { return &___Time_0; }
	inline void set_Time_0(float value)
	{
		___Time_0 = value;
	}

	inline static int32_t get_offset_of_TintColor_1() { return static_cast<int32_t>(offsetof(MoonParam_t2066856486, ___TintColor_1)); }
	inline Color_t2020392075  get_TintColor_1() const { return ___TintColor_1; }
	inline Color_t2020392075 * get_address_of_TintColor_1() { return &___TintColor_1; }
	inline void set_TintColor_1(Color_t2020392075  value)
	{
		___TintColor_1 = value;
	}

	inline static int32_t get_offset_of_LightColor_2() { return static_cast<int32_t>(offsetof(MoonParam_t2066856486, ___LightColor_2)); }
	inline Color_t2020392075  get_LightColor_2() const { return ___LightColor_2; }
	inline Color_t2020392075 * get_address_of_LightColor_2() { return &___LightColor_2; }
	inline void set_LightColor_2(Color_t2020392075  value)
	{
		___LightColor_2 = value;
	}

	inline static int32_t get_offset_of_LightIntencity_3() { return static_cast<int32_t>(offsetof(MoonParam_t2066856486, ___LightIntencity_3)); }
	inline float get_LightIntencity_3() const { return ___LightIntencity_3; }
	inline float* get_address_of_LightIntencity_3() { return &___LightIntencity_3; }
	inline void set_LightIntencity_3(float value)
	{
		___LightIntencity_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
