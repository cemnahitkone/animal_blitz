﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.SpriteSwapper
struct SpriteSwapper_t3880336557;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.SpriteSwapper::.ctor()
extern "C"  void SpriteSwapper__ctor_m2309944587 (SpriteSwapper_t3880336557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.SpriteSwapper::Awake()
extern "C"  void SpriteSwapper_Awake_m2936652544 (SpriteSwapper_t3880336557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.SpriteSwapper::SwapSprite()
extern "C"  void SpriteSwapper_SwapSprite_m3095008091 (SpriteSwapper_t3880336557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
