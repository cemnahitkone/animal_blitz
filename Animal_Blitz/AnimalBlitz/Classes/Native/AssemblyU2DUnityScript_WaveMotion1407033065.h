﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WaveMotion
struct  WaveMotion_t1407033065  : public MonoBehaviour_t1158329972
{
public:
	// System.Single WaveMotion::speed
	float ___speed_2;
	// System.Single WaveMotion::multiplier
	float ___multiplier_3;
	// System.Single WaveMotion::offset
	float ___offset_4;
	// UnityEngine.Quaternion WaveMotion::startingRotation
	Quaternion_t4030073918  ___startingRotation_5;
	// System.Single WaveMotion::waveAngle
	float ___waveAngle_6;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(WaveMotion_t1407033065, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_multiplier_3() { return static_cast<int32_t>(offsetof(WaveMotion_t1407033065, ___multiplier_3)); }
	inline float get_multiplier_3() const { return ___multiplier_3; }
	inline float* get_address_of_multiplier_3() { return &___multiplier_3; }
	inline void set_multiplier_3(float value)
	{
		___multiplier_3 = value;
	}

	inline static int32_t get_offset_of_offset_4() { return static_cast<int32_t>(offsetof(WaveMotion_t1407033065, ___offset_4)); }
	inline float get_offset_4() const { return ___offset_4; }
	inline float* get_address_of_offset_4() { return &___offset_4; }
	inline void set_offset_4(float value)
	{
		___offset_4 = value;
	}

	inline static int32_t get_offset_of_startingRotation_5() { return static_cast<int32_t>(offsetof(WaveMotion_t1407033065, ___startingRotation_5)); }
	inline Quaternion_t4030073918  get_startingRotation_5() const { return ___startingRotation_5; }
	inline Quaternion_t4030073918 * get_address_of_startingRotation_5() { return &___startingRotation_5; }
	inline void set_startingRotation_5(Quaternion_t4030073918  value)
	{
		___startingRotation_5 = value;
	}

	inline static int32_t get_offset_of_waveAngle_6() { return static_cast<int32_t>(offsetof(WaveMotion_t1407033065, ___waveAngle_6)); }
	inline float get_waveAngle_6() const { return ___waveAngle_6; }
	inline float* get_address_of_waveAngle_6() { return &___waveAngle_6; }
	inline void set_waveAngle_6(float value)
	{
		___waveAngle_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
