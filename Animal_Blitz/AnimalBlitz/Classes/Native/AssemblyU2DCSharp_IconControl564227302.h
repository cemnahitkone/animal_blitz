﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,tBase>
struct Dictionary_2_t728786214;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IconControl
struct  IconControl_t564227302  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,tBase> IconControl::_base
	Dictionary_2_t728786214 * ____base_2;
	// UnityEngine.GameObject IconControl::EventSys
	GameObject_t1756533147 * ___EventSys_3;
	// UnityEngine.GameObject IconControl::catIcon
	GameObject_t1756533147 * ___catIcon_4;
	// System.Boolean IconControl::catDestroyed
	bool ___catDestroyed_5;
	// System.Single IconControl::catSpeed
	float ___catSpeed_6;
	// UnityEngine.GameObject IconControl::ratIcon
	GameObject_t1756533147 * ___ratIcon_7;
	// System.Boolean IconControl::ratDestroyed
	bool ___ratDestroyed_8;
	// System.Single IconControl::ratSpeed
	float ___ratSpeed_9;
	// UnityEngine.GameObject IconControl::dogIcon
	GameObject_t1756533147 * ___dogIcon_10;
	// System.Boolean IconControl::dogDestroyed
	bool ___dogDestroyed_11;
	// System.Single IconControl::dogSpeed
	float ___dogSpeed_12;
	// UnityEngine.GameObject IconControl::owlIcon
	GameObject_t1756533147 * ___owlIcon_13;
	// System.Boolean IconControl::owlDestroyed
	bool ___owlDestroyed_14;
	// System.Single IconControl::owlSpeed
	float ___owlSpeed_15;
	// UnityEngine.GameObject IconControl::turtleIcon
	GameObject_t1756533147 * ___turtleIcon_16;
	// System.Boolean IconControl::turtleDestroyed
	bool ___turtleDestroyed_17;
	// System.Single IconControl::turtleSpeed
	float ___turtleSpeed_18;
	// UnityEngine.GameObject IconControl::Player2Score
	GameObject_t1756533147 * ___Player2Score_19;
	// UnityEngine.GameObject IconControl::Player1Score
	GameObject_t1756533147 * ___Player1Score_20;
	// UnityEngine.GameObject IconControl::pointSound
	GameObject_t1756533147 * ___pointSound_21;
	// UnityEngine.GameObject IconControl::failSound
	GameObject_t1756533147 * ___failSound_22;
	// UnityEngine.Vector3 IconControl::dogLastPosition
	Vector3_t2243707580  ___dogLastPosition_23;
	// UnityEngine.Vector3 IconControl::catLastPosition
	Vector3_t2243707580  ___catLastPosition_24;
	// UnityEngine.Vector3 IconControl::owlLastPosition
	Vector3_t2243707580  ___owlLastPosition_25;
	// UnityEngine.Vector3 IconControl::ratLastPosition
	Vector3_t2243707580  ___ratLastPosition_26;
	// UnityEngine.Vector3 IconControl::turtleLastPosition
	Vector3_t2243707580  ___turtleLastPosition_27;
	// System.String IconControl::pl1collider
	String_t* ___pl1collider_28;
	// System.String IconControl::pl2collider
	String_t* ___pl2collider_29;
	// System.Boolean IconControl::success
	bool ___success_30;
	// System.String IconControl::correctAnswer
	String_t* ___correctAnswer_31;

public:
	inline static int32_t get_offset_of__base_2() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ____base_2)); }
	inline Dictionary_2_t728786214 * get__base_2() const { return ____base_2; }
	inline Dictionary_2_t728786214 ** get_address_of__base_2() { return &____base_2; }
	inline void set__base_2(Dictionary_2_t728786214 * value)
	{
		____base_2 = value;
		Il2CppCodeGenWriteBarrier(&____base_2, value);
	}

	inline static int32_t get_offset_of_EventSys_3() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___EventSys_3)); }
	inline GameObject_t1756533147 * get_EventSys_3() const { return ___EventSys_3; }
	inline GameObject_t1756533147 ** get_address_of_EventSys_3() { return &___EventSys_3; }
	inline void set_EventSys_3(GameObject_t1756533147 * value)
	{
		___EventSys_3 = value;
		Il2CppCodeGenWriteBarrier(&___EventSys_3, value);
	}

	inline static int32_t get_offset_of_catIcon_4() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___catIcon_4)); }
	inline GameObject_t1756533147 * get_catIcon_4() const { return ___catIcon_4; }
	inline GameObject_t1756533147 ** get_address_of_catIcon_4() { return &___catIcon_4; }
	inline void set_catIcon_4(GameObject_t1756533147 * value)
	{
		___catIcon_4 = value;
		Il2CppCodeGenWriteBarrier(&___catIcon_4, value);
	}

	inline static int32_t get_offset_of_catDestroyed_5() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___catDestroyed_5)); }
	inline bool get_catDestroyed_5() const { return ___catDestroyed_5; }
	inline bool* get_address_of_catDestroyed_5() { return &___catDestroyed_5; }
	inline void set_catDestroyed_5(bool value)
	{
		___catDestroyed_5 = value;
	}

	inline static int32_t get_offset_of_catSpeed_6() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___catSpeed_6)); }
	inline float get_catSpeed_6() const { return ___catSpeed_6; }
	inline float* get_address_of_catSpeed_6() { return &___catSpeed_6; }
	inline void set_catSpeed_6(float value)
	{
		___catSpeed_6 = value;
	}

	inline static int32_t get_offset_of_ratIcon_7() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___ratIcon_7)); }
	inline GameObject_t1756533147 * get_ratIcon_7() const { return ___ratIcon_7; }
	inline GameObject_t1756533147 ** get_address_of_ratIcon_7() { return &___ratIcon_7; }
	inline void set_ratIcon_7(GameObject_t1756533147 * value)
	{
		___ratIcon_7 = value;
		Il2CppCodeGenWriteBarrier(&___ratIcon_7, value);
	}

	inline static int32_t get_offset_of_ratDestroyed_8() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___ratDestroyed_8)); }
	inline bool get_ratDestroyed_8() const { return ___ratDestroyed_8; }
	inline bool* get_address_of_ratDestroyed_8() { return &___ratDestroyed_8; }
	inline void set_ratDestroyed_8(bool value)
	{
		___ratDestroyed_8 = value;
	}

	inline static int32_t get_offset_of_ratSpeed_9() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___ratSpeed_9)); }
	inline float get_ratSpeed_9() const { return ___ratSpeed_9; }
	inline float* get_address_of_ratSpeed_9() { return &___ratSpeed_9; }
	inline void set_ratSpeed_9(float value)
	{
		___ratSpeed_9 = value;
	}

	inline static int32_t get_offset_of_dogIcon_10() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___dogIcon_10)); }
	inline GameObject_t1756533147 * get_dogIcon_10() const { return ___dogIcon_10; }
	inline GameObject_t1756533147 ** get_address_of_dogIcon_10() { return &___dogIcon_10; }
	inline void set_dogIcon_10(GameObject_t1756533147 * value)
	{
		___dogIcon_10 = value;
		Il2CppCodeGenWriteBarrier(&___dogIcon_10, value);
	}

	inline static int32_t get_offset_of_dogDestroyed_11() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___dogDestroyed_11)); }
	inline bool get_dogDestroyed_11() const { return ___dogDestroyed_11; }
	inline bool* get_address_of_dogDestroyed_11() { return &___dogDestroyed_11; }
	inline void set_dogDestroyed_11(bool value)
	{
		___dogDestroyed_11 = value;
	}

	inline static int32_t get_offset_of_dogSpeed_12() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___dogSpeed_12)); }
	inline float get_dogSpeed_12() const { return ___dogSpeed_12; }
	inline float* get_address_of_dogSpeed_12() { return &___dogSpeed_12; }
	inline void set_dogSpeed_12(float value)
	{
		___dogSpeed_12 = value;
	}

	inline static int32_t get_offset_of_owlIcon_13() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___owlIcon_13)); }
	inline GameObject_t1756533147 * get_owlIcon_13() const { return ___owlIcon_13; }
	inline GameObject_t1756533147 ** get_address_of_owlIcon_13() { return &___owlIcon_13; }
	inline void set_owlIcon_13(GameObject_t1756533147 * value)
	{
		___owlIcon_13 = value;
		Il2CppCodeGenWriteBarrier(&___owlIcon_13, value);
	}

	inline static int32_t get_offset_of_owlDestroyed_14() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___owlDestroyed_14)); }
	inline bool get_owlDestroyed_14() const { return ___owlDestroyed_14; }
	inline bool* get_address_of_owlDestroyed_14() { return &___owlDestroyed_14; }
	inline void set_owlDestroyed_14(bool value)
	{
		___owlDestroyed_14 = value;
	}

	inline static int32_t get_offset_of_owlSpeed_15() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___owlSpeed_15)); }
	inline float get_owlSpeed_15() const { return ___owlSpeed_15; }
	inline float* get_address_of_owlSpeed_15() { return &___owlSpeed_15; }
	inline void set_owlSpeed_15(float value)
	{
		___owlSpeed_15 = value;
	}

	inline static int32_t get_offset_of_turtleIcon_16() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___turtleIcon_16)); }
	inline GameObject_t1756533147 * get_turtleIcon_16() const { return ___turtleIcon_16; }
	inline GameObject_t1756533147 ** get_address_of_turtleIcon_16() { return &___turtleIcon_16; }
	inline void set_turtleIcon_16(GameObject_t1756533147 * value)
	{
		___turtleIcon_16 = value;
		Il2CppCodeGenWriteBarrier(&___turtleIcon_16, value);
	}

	inline static int32_t get_offset_of_turtleDestroyed_17() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___turtleDestroyed_17)); }
	inline bool get_turtleDestroyed_17() const { return ___turtleDestroyed_17; }
	inline bool* get_address_of_turtleDestroyed_17() { return &___turtleDestroyed_17; }
	inline void set_turtleDestroyed_17(bool value)
	{
		___turtleDestroyed_17 = value;
	}

	inline static int32_t get_offset_of_turtleSpeed_18() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___turtleSpeed_18)); }
	inline float get_turtleSpeed_18() const { return ___turtleSpeed_18; }
	inline float* get_address_of_turtleSpeed_18() { return &___turtleSpeed_18; }
	inline void set_turtleSpeed_18(float value)
	{
		___turtleSpeed_18 = value;
	}

	inline static int32_t get_offset_of_Player2Score_19() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___Player2Score_19)); }
	inline GameObject_t1756533147 * get_Player2Score_19() const { return ___Player2Score_19; }
	inline GameObject_t1756533147 ** get_address_of_Player2Score_19() { return &___Player2Score_19; }
	inline void set_Player2Score_19(GameObject_t1756533147 * value)
	{
		___Player2Score_19 = value;
		Il2CppCodeGenWriteBarrier(&___Player2Score_19, value);
	}

	inline static int32_t get_offset_of_Player1Score_20() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___Player1Score_20)); }
	inline GameObject_t1756533147 * get_Player1Score_20() const { return ___Player1Score_20; }
	inline GameObject_t1756533147 ** get_address_of_Player1Score_20() { return &___Player1Score_20; }
	inline void set_Player1Score_20(GameObject_t1756533147 * value)
	{
		___Player1Score_20 = value;
		Il2CppCodeGenWriteBarrier(&___Player1Score_20, value);
	}

	inline static int32_t get_offset_of_pointSound_21() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___pointSound_21)); }
	inline GameObject_t1756533147 * get_pointSound_21() const { return ___pointSound_21; }
	inline GameObject_t1756533147 ** get_address_of_pointSound_21() { return &___pointSound_21; }
	inline void set_pointSound_21(GameObject_t1756533147 * value)
	{
		___pointSound_21 = value;
		Il2CppCodeGenWriteBarrier(&___pointSound_21, value);
	}

	inline static int32_t get_offset_of_failSound_22() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___failSound_22)); }
	inline GameObject_t1756533147 * get_failSound_22() const { return ___failSound_22; }
	inline GameObject_t1756533147 ** get_address_of_failSound_22() { return &___failSound_22; }
	inline void set_failSound_22(GameObject_t1756533147 * value)
	{
		___failSound_22 = value;
		Il2CppCodeGenWriteBarrier(&___failSound_22, value);
	}

	inline static int32_t get_offset_of_dogLastPosition_23() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___dogLastPosition_23)); }
	inline Vector3_t2243707580  get_dogLastPosition_23() const { return ___dogLastPosition_23; }
	inline Vector3_t2243707580 * get_address_of_dogLastPosition_23() { return &___dogLastPosition_23; }
	inline void set_dogLastPosition_23(Vector3_t2243707580  value)
	{
		___dogLastPosition_23 = value;
	}

	inline static int32_t get_offset_of_catLastPosition_24() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___catLastPosition_24)); }
	inline Vector3_t2243707580  get_catLastPosition_24() const { return ___catLastPosition_24; }
	inline Vector3_t2243707580 * get_address_of_catLastPosition_24() { return &___catLastPosition_24; }
	inline void set_catLastPosition_24(Vector3_t2243707580  value)
	{
		___catLastPosition_24 = value;
	}

	inline static int32_t get_offset_of_owlLastPosition_25() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___owlLastPosition_25)); }
	inline Vector3_t2243707580  get_owlLastPosition_25() const { return ___owlLastPosition_25; }
	inline Vector3_t2243707580 * get_address_of_owlLastPosition_25() { return &___owlLastPosition_25; }
	inline void set_owlLastPosition_25(Vector3_t2243707580  value)
	{
		___owlLastPosition_25 = value;
	}

	inline static int32_t get_offset_of_ratLastPosition_26() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___ratLastPosition_26)); }
	inline Vector3_t2243707580  get_ratLastPosition_26() const { return ___ratLastPosition_26; }
	inline Vector3_t2243707580 * get_address_of_ratLastPosition_26() { return &___ratLastPosition_26; }
	inline void set_ratLastPosition_26(Vector3_t2243707580  value)
	{
		___ratLastPosition_26 = value;
	}

	inline static int32_t get_offset_of_turtleLastPosition_27() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___turtleLastPosition_27)); }
	inline Vector3_t2243707580  get_turtleLastPosition_27() const { return ___turtleLastPosition_27; }
	inline Vector3_t2243707580 * get_address_of_turtleLastPosition_27() { return &___turtleLastPosition_27; }
	inline void set_turtleLastPosition_27(Vector3_t2243707580  value)
	{
		___turtleLastPosition_27 = value;
	}

	inline static int32_t get_offset_of_pl1collider_28() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___pl1collider_28)); }
	inline String_t* get_pl1collider_28() const { return ___pl1collider_28; }
	inline String_t** get_address_of_pl1collider_28() { return &___pl1collider_28; }
	inline void set_pl1collider_28(String_t* value)
	{
		___pl1collider_28 = value;
		Il2CppCodeGenWriteBarrier(&___pl1collider_28, value);
	}

	inline static int32_t get_offset_of_pl2collider_29() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___pl2collider_29)); }
	inline String_t* get_pl2collider_29() const { return ___pl2collider_29; }
	inline String_t** get_address_of_pl2collider_29() { return &___pl2collider_29; }
	inline void set_pl2collider_29(String_t* value)
	{
		___pl2collider_29 = value;
		Il2CppCodeGenWriteBarrier(&___pl2collider_29, value);
	}

	inline static int32_t get_offset_of_success_30() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___success_30)); }
	inline bool get_success_30() const { return ___success_30; }
	inline bool* get_address_of_success_30() { return &___success_30; }
	inline void set_success_30(bool value)
	{
		___success_30 = value;
	}

	inline static int32_t get_offset_of_correctAnswer_31() { return static_cast<int32_t>(offsetof(IconControl_t564227302, ___correctAnswer_31)); }
	inline String_t* get_correctAnswer_31() const { return ___correctAnswer_31; }
	inline String_t** get_address_of_correctAnswer_31() { return &___correctAnswer_31; }
	inline void set_correctAnswer_31(String_t* value)
	{
		___correctAnswer_31 = value;
		Il2CppCodeGenWriteBarrier(&___correctAnswer_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
