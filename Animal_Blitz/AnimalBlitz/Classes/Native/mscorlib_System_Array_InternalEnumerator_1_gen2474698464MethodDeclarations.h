﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2474698464.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H1615946202.h"

// System.Void System.Array/InternalEnumerator`1<HighlightingSystem.Highlighter/Mode>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3311867635_gshared (InternalEnumerator_1_t2474698464 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3311867635(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2474698464 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3311867635_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<HighlightingSystem.Highlighter/Mode>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2967390371_gshared (InternalEnumerator_1_t2474698464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2967390371(__this, method) ((  void (*) (InternalEnumerator_1_t2474698464 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2967390371_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<HighlightingSystem.Highlighter/Mode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3587354299_gshared (InternalEnumerator_1_t2474698464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3587354299(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2474698464 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3587354299_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<HighlightingSystem.Highlighter/Mode>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4060492158_gshared (InternalEnumerator_1_t2474698464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4060492158(__this, method) ((  void (*) (InternalEnumerator_1_t2474698464 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4060492158_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<HighlightingSystem.Highlighter/Mode>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2572922391_gshared (InternalEnumerator_1_t2474698464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2572922391(__this, method) ((  bool (*) (InternalEnumerator_1_t2474698464 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2572922391_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<HighlightingSystem.Highlighter/Mode>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2023099026_gshared (InternalEnumerator_1_t2474698464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2023099026(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2474698464 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2023099026_gshared)(__this, method)
