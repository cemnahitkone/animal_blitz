﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2<System.Single,System.Object>
struct SortedList_2_t2155713151;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedList_2_Val3160392380.h"

// System.Void System.Collections.Generic.SortedList`2/ValueEnumerator<System.Single,System.Object>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C"  void ValueEnumerator__ctor_m1329016009_gshared (ValueEnumerator_t3160392380 * __this, SortedList_2_t2155713151 * ___l0, const MethodInfo* method);
#define ValueEnumerator__ctor_m1329016009(__this, ___l0, method) ((  void (*) (ValueEnumerator_t3160392380 *, SortedList_2_t2155713151 *, const MethodInfo*))ValueEnumerator__ctor_m1329016009_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.SortedList`2/ValueEnumerator<System.Single,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void ValueEnumerator_System_Collections_IEnumerator_Reset_m3161217281_gshared (ValueEnumerator_t3160392380 * __this, const MethodInfo* method);
#define ValueEnumerator_System_Collections_IEnumerator_Reset_m3161217281(__this, method) ((  void (*) (ValueEnumerator_t3160392380 *, const MethodInfo*))ValueEnumerator_System_Collections_IEnumerator_Reset_m3161217281_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ValueEnumerator<System.Single,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * ValueEnumerator_System_Collections_IEnumerator_get_Current_m1963410333_gshared (ValueEnumerator_t3160392380 * __this, const MethodInfo* method);
#define ValueEnumerator_System_Collections_IEnumerator_get_Current_m1963410333(__this, method) ((  Il2CppObject * (*) (ValueEnumerator_t3160392380 *, const MethodInfo*))ValueEnumerator_System_Collections_IEnumerator_get_Current_m1963410333_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ValueEnumerator<System.Single,System.Object>::Dispose()
extern "C"  void ValueEnumerator_Dispose_m2066657120_gshared (ValueEnumerator_t3160392380 * __this, const MethodInfo* method);
#define ValueEnumerator_Dispose_m2066657120(__this, method) ((  void (*) (ValueEnumerator_t3160392380 *, const MethodInfo*))ValueEnumerator_Dispose_m2066657120_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ValueEnumerator<System.Single,System.Object>::MoveNext()
extern "C"  bool ValueEnumerator_MoveNext_m1971554277_gshared (ValueEnumerator_t3160392380 * __this, const MethodInfo* method);
#define ValueEnumerator_MoveNext_m1971554277(__this, method) ((  bool (*) (ValueEnumerator_t3160392380 *, const MethodInfo*))ValueEnumerator_MoveNext_m1971554277_gshared)(__this, method)
// TValue System.Collections.Generic.SortedList`2/ValueEnumerator<System.Single,System.Object>::get_Current()
extern "C"  Il2CppObject * ValueEnumerator_get_Current_m152689739_gshared (ValueEnumerator_t3160392380 * __this, const MethodInfo* method);
#define ValueEnumerator_get_Current_m152689739(__this, method) ((  Il2CppObject * (*) (ValueEnumerator_t3160392380 *, const MethodInfo*))ValueEnumerator_get_Current_m152689739_gshared)(__this, method)
