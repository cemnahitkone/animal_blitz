﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m938566670(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2544531684 *, float, CloudsParam_t3937665775 *, const MethodInfo*))KeyValuePair_2__ctor_m303434200_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam>::get_Key()
#define KeyValuePair_2_get_Key_m1843862816(__this, method) ((  float (*) (KeyValuePair_2_t2544531684 *, const MethodInfo*))KeyValuePair_2_get_Key_m4249127398_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m526798899(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2544531684 *, float, const MethodInfo*))KeyValuePair_2_set_Key_m448823159_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam>::get_Value()
#define KeyValuePair_2_get_Value_m2383909736(__this, method) ((  CloudsParam_t3937665775 * (*) (KeyValuePair_2_t2544531684 *, const MethodInfo*))KeyValuePair_2_get_Value_m1014938790_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m4280377099(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2544531684 *, CloudsParam_t3937665775 *, const MethodInfo*))KeyValuePair_2_set_Value_m1070856647_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Single,Borodar.FarlandSkies.LowPoly.DotParams.CloudsParam>::ToString()
#define KeyValuePair_2_ToString_m3235502465(__this, method) ((  String_t* (*) (KeyValuePair_2_t2544531684 *, const MethodInfo*))KeyValuePair_2_ToString_m4220912861_gshared)(__this, method)
