﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Single,System.Object>
struct U3CGetEnumeratorU3Ec__Iterator1_t4132747541;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"

// System.Void System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Single,System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1__ctor_m3233715594_gshared (U3CGetEnumeratorU3Ec__Iterator1_t4132747541 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1__ctor_m3233715594(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator1_t4132747541 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1__ctor_m3233715594_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Single,System.Object>::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_Current()
extern "C"  KeyValuePair_2_t1296315204  U3CGetEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m2988265759_gshared (U3CGetEnumeratorU3Ec__Iterator1_t4132747541 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m2988265759(__this, method) ((  KeyValuePair_2_t1296315204  (*) (U3CGetEnumeratorU3Ec__Iterator1_t4132747541 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m2988265759_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Single,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1616209278_gshared (U3CGetEnumeratorU3Ec__Iterator1_t4132747541 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1616209278(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator1_t4132747541 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1616209278_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Single,System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator1_MoveNext_m1692212712_gshared (U3CGetEnumeratorU3Ec__Iterator1_t4132747541 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_MoveNext_m1692212712(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator1_t4132747541 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_MoveNext_m1692212712_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Single,System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1_Dispose_m4158913387_gshared (U3CGetEnumeratorU3Ec__Iterator1_t4132747541 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_Dispose_m4158913387(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator1_t4132747541 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_Dispose_m4158913387_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Single,System.Object>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1_Reset_m1570036429_gshared (U3CGetEnumeratorU3Ec__Iterator1_t4132747541 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_Reset_m1570036429(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator1_t4132747541 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_Reset_m1570036429_gshared)(__this, method)
