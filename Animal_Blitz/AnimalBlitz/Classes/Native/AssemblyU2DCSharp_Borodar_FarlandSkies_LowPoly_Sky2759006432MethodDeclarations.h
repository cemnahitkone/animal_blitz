﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.SkyboxController
struct SkyboxController_t2759006432;
// UnityEngine.Cubemap
struct Cubemap_t3472944757;
// UnityEngine.Light
struct Light_t494725636;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Cubemap3472944757.h"
#include "UnityEngine_UnityEngine_Light494725636.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::.ctor()
extern "C"  void SkyboxController__ctor_m1004803133 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Borodar.FarlandSkies.LowPoly.SkyboxController::get_TopColor()
extern "C"  Color_t2020392075  SkyboxController_get_TopColor_m332685011 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_TopColor(UnityEngine.Color)
extern "C"  void SkyboxController_set_TopColor_m1462305904 (SkyboxController_t2759006432 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Borodar.FarlandSkies.LowPoly.SkyboxController::get_MiddleColor()
extern "C"  Color_t2020392075  SkyboxController_get_MiddleColor_m1007017773 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_MiddleColor(UnityEngine.Color)
extern "C"  void SkyboxController_set_MiddleColor_m1986044516 (SkyboxController_t2759006432 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Borodar.FarlandSkies.LowPoly.SkyboxController::get_BottomColor()
extern "C"  Color_t2020392075  SkyboxController_get_BottomColor_m3418320923 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_BottomColor(UnityEngine.Color)
extern "C"  void SkyboxController_set_BottomColor_m3327852198 (SkyboxController_t2759006432 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::get_TopExponent()
extern "C"  float SkyboxController_get_TopExponent_m817498150 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_TopExponent(System.Single)
extern "C"  void SkyboxController_set_TopExponent_m313816989 (SkyboxController_t2759006432 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::get_BottomExponent()
extern "C"  float SkyboxController_get_BottomExponent_m2205236208 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_BottomExponent(System.Single)
extern "C"  void SkyboxController_set_BottomExponent_m1346722877 (SkyboxController_t2759006432 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Borodar.FarlandSkies.LowPoly.SkyboxController::get_StarsEnabled()
extern "C"  bool SkyboxController_get_StarsEnabled_m3687130018 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Borodar.FarlandSkies.LowPoly.SkyboxController::get_StarsTint()
extern "C"  Color_t2020392075  SkyboxController_get_StarsTint_m2733063053 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_StarsTint(UnityEngine.Color)
extern "C"  void SkyboxController_set_StarsTint_m1531859492 (SkyboxController_t2759006432 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Cubemap Borodar.FarlandSkies.LowPoly.SkyboxController::get_StarsCubemap()
extern "C"  Cubemap_t3472944757 * SkyboxController_get_StarsCubemap_m4029986547 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_StarsCubemap(UnityEngine.Cubemap)
extern "C"  void SkyboxController_set_StarsCubemap_m2216372194 (SkyboxController_t2759006432 * __this, Cubemap_t3472944757 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::get_StarsExtinction()
extern "C"  float SkyboxController_get_StarsExtinction_m4087229604 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_StarsExtinction(System.Single)
extern "C"  void SkyboxController_set_StarsExtinction_m2036631351 (SkyboxController_t2759006432 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::get_StarsTwinklingSpeed()
extern "C"  float SkyboxController_get_StarsTwinklingSpeed_m2882913449 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_StarsTwinklingSpeed(System.Single)
extern "C"  void SkyboxController_set_StarsTwinklingSpeed_m3968895564 (SkyboxController_t2759006432 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Borodar.FarlandSkies.LowPoly.SkyboxController::get_SunEnabled()
extern "C"  bool SkyboxController_get_SunEnabled_m1259747149 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Light Borodar.FarlandSkies.LowPoly.SkyboxController::get_SunLight()
extern "C"  Light_t494725636 * SkyboxController_get_SunLight_m900792506 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_SunLight(UnityEngine.Light)
extern "C"  void SkyboxController_set_SunLight_m2415199943 (SkyboxController_t2759006432 * __this, Light_t494725636 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Borodar.FarlandSkies.LowPoly.SkyboxController::get_SunTexture()
extern "C"  Texture2D_t3542995729 * SkyboxController_get_SunTexture_m3171485284 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_SunTexture(UnityEngine.Texture2D)
extern "C"  void SkyboxController_set_SunTexture_m2529086341 (SkyboxController_t2759006432 * __this, Texture2D_t3542995729 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Borodar.FarlandSkies.LowPoly.SkyboxController::get_SunTint()
extern "C"  Color_t2020392075  SkyboxController_get_SunTint_m722760872 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_SunTint(UnityEngine.Color)
extern "C"  void SkyboxController_set_SunTint_m1515852481 (SkyboxController_t2759006432 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::get_SunSize()
extern "C"  float SkyboxController_get_SunSize_m1169684455 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_SunSize(System.Single)
extern "C"  void SkyboxController_set_SunSize_m2596386336 (SkyboxController_t2759006432 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Borodar.FarlandSkies.LowPoly.SkyboxController::get_SunFlare()
extern "C"  bool SkyboxController_get_SunFlare_m1955059802 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_SunFlare(System.Boolean)
extern "C"  void SkyboxController_set_SunFlare_m694020751 (SkyboxController_t2759006432 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::get_SunFlareBrightness()
extern "C"  float SkyboxController_get_SunFlareBrightness_m48771723 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_SunFlareBrightness(System.Single)
extern "C"  void SkyboxController_set_SunFlareBrightness_m2297077432 (SkyboxController_t2759006432 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Borodar.FarlandSkies.LowPoly.SkyboxController::get_MoonEnabled()
extern "C"  bool SkyboxController_get_MoonEnabled_m3322924852 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Borodar.FarlandSkies.LowPoly.SkyboxController::get_MoonTexture()
extern "C"  Texture2D_t3542995729 * SkyboxController_get_MoonTexture_m1589583989 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_MoonTexture(UnityEngine.Texture2D)
extern "C"  void SkyboxController_set_MoonTexture_m3754405454 (SkyboxController_t2759006432 * __this, Texture2D_t3542995729 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::get_MoonSize()
extern "C"  float SkyboxController_get_MoonSize_m714864928 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_MoonSize(System.Single)
extern "C"  void SkyboxController_set_MoonSize_m4262294323 (SkyboxController_t2759006432 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Borodar.FarlandSkies.LowPoly.SkyboxController::get_MoonTint()
extern "C"  Color_t2020392075  SkyboxController_get_MoonTint_m3015347837 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_MoonTint(UnityEngine.Color)
extern "C"  void SkyboxController_set_MoonTint_m4219640714 (SkyboxController_t2759006432 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Light Borodar.FarlandSkies.LowPoly.SkyboxController::get_MoonLight()
extern "C"  Light_t494725636 * SkyboxController_get_MoonLight_m1769505231 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_MoonLight(UnityEngine.Light)
extern "C"  void SkyboxController_set_MoonLight_m2510360616 (SkyboxController_t2759006432 * __this, Light_t494725636 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Borodar.FarlandSkies.LowPoly.SkyboxController::get_MoonFlare()
extern "C"  bool SkyboxController_get_MoonFlare_m352932895 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_MoonFlare(System.Boolean)
extern "C"  void SkyboxController_set_MoonFlare_m4204307162 (SkyboxController_t2759006432 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::get_MoonFlareBrightness()
extern "C"  float SkyboxController_get_MoonFlareBrightness_m6247870 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_MoonFlareBrightness(System.Single)
extern "C"  void SkyboxController_set_MoonFlareBrightness_m3320951355 (SkyboxController_t2759006432 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Borodar.FarlandSkies.LowPoly.SkyboxController::get_CloudsEnabled()
extern "C"  bool SkyboxController_get_CloudsEnabled_m3037104311 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Cubemap Borodar.FarlandSkies.LowPoly.SkyboxController::get_CloudsCubemap()
extern "C"  Cubemap_t3472944757 * SkyboxController_get_CloudsCubemap_m3172100378 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_CloudsCubemap(UnityEngine.Cubemap)
extern "C"  void SkyboxController_set_CloudsCubemap_m1292205653 (SkyboxController_t2759006432 * __this, Cubemap_t3472944757 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Borodar.FarlandSkies.LowPoly.SkyboxController::get_CloudsTint()
extern "C"  Color_t2020392075  SkyboxController_get_CloudsTint_m3280234160 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_CloudsTint(UnityEngine.Color)
extern "C"  void SkyboxController_set_CloudsTint_m4241826499 (SkyboxController_t2759006432 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::get_CloudsRotation()
extern "C"  float SkyboxController_get_CloudsRotation_m1976303332 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_CloudsRotation(System.Single)
extern "C"  void SkyboxController_set_CloudsRotation_m4212121299 (SkyboxController_t2759006432 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::get_CloudsHeight()
extern "C"  float SkyboxController_get_CloudsHeight_m2913840627 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_CloudsHeight(System.Single)
extern "C"  void SkyboxController_set_CloudsHeight_m2524942448 (SkyboxController_t2759006432 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Borodar.FarlandSkies.LowPoly.SkyboxController::get_Exposure()
extern "C"  float SkyboxController_get_Exposure_m3567882253 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_Exposure(System.Single)
extern "C"  void SkyboxController_set_Exposure_m369246428 (SkyboxController_t2759006432 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Borodar.FarlandSkies.LowPoly.SkyboxController::get_AdjustFogColor()
extern "C"  bool SkyboxController_get_AdjustFogColor_m3529386484 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::set_AdjustFogColor(System.Boolean)
extern "C"  void SkyboxController_set_AdjustFogColor_m400574369 (SkyboxController_t2759006432 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::Awake()
extern "C"  void SkyboxController_Awake_m1207724324 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::OnValidate()
extern "C"  void SkyboxController_OnValidate_m2513765580 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::Update()
extern "C"  void SkyboxController_Update_m1803758466 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.SkyboxController::UpdateSkyboxProperties()
extern "C"  void SkyboxController_UpdateSkyboxProperties_m1080238609 (SkyboxController_t2759006432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
