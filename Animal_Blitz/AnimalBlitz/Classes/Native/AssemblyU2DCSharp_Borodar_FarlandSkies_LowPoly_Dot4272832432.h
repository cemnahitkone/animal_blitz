﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_DotPar3767082706.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.DotParams.SkyParam
struct  SkyParam_t4272832432  : public DotParam_t3767082706
{
public:
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.DotParams.SkyParam::TopColor
	Color_t2020392075  ___TopColor_1;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.DotParams.SkyParam::MiddleColor
	Color_t2020392075  ___MiddleColor_2;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.DotParams.SkyParam::BottomColor
	Color_t2020392075  ___BottomColor_3;
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.DotParams.SkyParam::CloudsTint
	Color_t2020392075  ___CloudsTint_4;

public:
	inline static int32_t get_offset_of_TopColor_1() { return static_cast<int32_t>(offsetof(SkyParam_t4272832432, ___TopColor_1)); }
	inline Color_t2020392075  get_TopColor_1() const { return ___TopColor_1; }
	inline Color_t2020392075 * get_address_of_TopColor_1() { return &___TopColor_1; }
	inline void set_TopColor_1(Color_t2020392075  value)
	{
		___TopColor_1 = value;
	}

	inline static int32_t get_offset_of_MiddleColor_2() { return static_cast<int32_t>(offsetof(SkyParam_t4272832432, ___MiddleColor_2)); }
	inline Color_t2020392075  get_MiddleColor_2() const { return ___MiddleColor_2; }
	inline Color_t2020392075 * get_address_of_MiddleColor_2() { return &___MiddleColor_2; }
	inline void set_MiddleColor_2(Color_t2020392075  value)
	{
		___MiddleColor_2 = value;
	}

	inline static int32_t get_offset_of_BottomColor_3() { return static_cast<int32_t>(offsetof(SkyParam_t4272832432, ___BottomColor_3)); }
	inline Color_t2020392075  get_BottomColor_3() const { return ___BottomColor_3; }
	inline Color_t2020392075 * get_address_of_BottomColor_3() { return &___BottomColor_3; }
	inline void set_BottomColor_3(Color_t2020392075  value)
	{
		___BottomColor_3 = value;
	}

	inline static int32_t get_offset_of_CloudsTint_4() { return static_cast<int32_t>(offsetof(SkyParam_t4272832432, ___CloudsTint_4)); }
	inline Color_t2020392075  get_CloudsTint_4() const { return ___CloudsTint_4; }
	inline Color_t2020392075 * get_address_of_CloudsTint_4() { return &___CloudsTint_4; }
	inline void set_CloudsTint_4(Color_t2020392075  value)
	{
		___CloudsTint_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
