﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct DefaultComparer_t4080552733;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m2917740314_gshared (DefaultComparer_t4080552733 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2917740314(__this, method) ((  void (*) (DefaultComparer_t4080552733 *, const MethodInfo*))DefaultComparer__ctor_m2917740314_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3997813003_gshared (DefaultComparer_t4080552733 * __this, KeyValuePair_2_t1296315204  ___x0, KeyValuePair_2_t1296315204  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3997813003(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t4080552733 *, KeyValuePair_2_t1296315204 , KeyValuePair_2_t1296315204 , const MethodInfo*))DefaultComparer_Compare_m3997813003_gshared)(__this, ___x0, ___y1, method)
