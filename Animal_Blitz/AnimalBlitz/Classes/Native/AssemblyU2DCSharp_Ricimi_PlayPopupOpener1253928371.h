﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Ricimi_PopupOpener3649718601.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ricimi.PlayPopupOpener
struct  PlayPopupOpener_t1253928371  : public PopupOpener_t3649718601
{
public:
	// System.Int32 Ricimi.PlayPopupOpener::starsObtained
	int32_t ___starsObtained_4;

public:
	inline static int32_t get_offset_of_starsObtained_4() { return static_cast<int32_t>(offsetof(PlayPopupOpener_t1253928371, ___starsObtained_4)); }
	inline int32_t get_starsObtained_4() const { return ___starsObtained_4; }
	inline int32_t* get_address_of_starsObtained_4() { return &___starsObtained_4; }
	inline void set_starsObtained_4(int32_t value)
	{
		___starsObtained_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
