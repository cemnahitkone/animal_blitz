﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpriteTrail
struct SpriteTrail_t2284883325;
// UnityEngine.Transform
struct Transform_t3275118058;
// TrailPreset
struct TrailPreset_t465077881;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "AssemblyU2DCSharp_TrailPreset465077881.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void SpriteTrail::.ctor()
extern "C"  void SpriteTrail__ctor_m588023566 (SpriteTrail_t2284883325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::SetTrailParent(UnityEngine.Transform)
extern "C"  void SpriteTrail_SetTrailParent_m3799627059 (SpriteTrail_t2284883325 * __this, Transform_t3275118058 * ___trailParent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::SetTrailPreset(TrailPreset)
extern "C"  void SpriteTrail_SetTrailPreset_m277816234 (SpriteTrail_t2284883325 * __this, TrailPreset_t465077881 * ___preset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::EnableTrail()
extern "C"  void SpriteTrail_EnableTrail_m3387428939 (SpriteTrail_t2284883325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::DisableTrail()
extern "C"  void SpriteTrail_DisableTrail_m2137670676 (SpriteTrail_t2284883325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::EnableTrailEffect(System.Boolean)
extern "C"  void SpriteTrail_EnableTrailEffect_m1251733603 (SpriteTrail_t2284883325 * __this, bool ___forceTrailCreation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::DisableTrailEffect()
extern "C"  void SpriteTrail_DisableTrailEffect_m4082338629 (SpriteTrail_t2284883325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::HideTrail()
extern "C"  void SpriteTrail_HideTrail_m2673586674 (SpriteTrail_t2284883325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpriteTrail::IsEffectEnabled()
extern "C"  bool SpriteTrail_IsEffectEnabled_m236050918 (SpriteTrail_t2284883325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::OnLevelWasLoaded()
extern "C"  void SpriteTrail_OnLevelWasLoaded_m4154238041 (SpriteTrail_t2284883325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::Awake()
extern "C"  void SpriteTrail_Awake_m543085597 (SpriteTrail_t2284883325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::CalculateCurrentVelocity()
extern "C"  void SpriteTrail_CalculateCurrentVelocity_m2497564602 (SpriteTrail_t2284883325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::Update()
extern "C"  void SpriteTrail_Update_m2249259625 (SpriteTrail_t2284883325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::GenerateNewTrailElement(UnityEngine.Vector3)
extern "C"  void SpriteTrail_GenerateNewTrailElement_m2721125526 (SpriteTrail_t2284883325 * __this, Vector3_t2243707580  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::GenerateNewTrailElement()
extern "C"  void SpriteTrail_GenerateNewTrailElement_m447001791 (SpriteTrail_t2284883325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::OnDisable()
extern "C"  void SpriteTrail_OnDisable_m318479869 (SpriteTrail_t2284883325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::OnEnable()
extern "C"  void SpriteTrail_OnEnable_m1193871758 (SpriteTrail_t2284883325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::OnDestroy()
extern "C"  void SpriteTrail_OnDestroy_m3464570531 (SpriteTrail_t2284883325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject SpriteTrail::GetTrailElementPrefab()
extern "C"  GameObject_t1756533147 * SpriteTrail_GetTrailElementPrefab_m1242168547 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteTrail::.cctor()
extern "C"  void SpriteTrail__cctor_m1651259743 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
