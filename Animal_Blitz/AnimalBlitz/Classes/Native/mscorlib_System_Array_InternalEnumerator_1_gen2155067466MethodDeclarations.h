﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2155067466.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3190272581_gshared (InternalEnumerator_1_t2155067466 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3190272581(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2155067466 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3190272581_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m322303073_gshared (InternalEnumerator_1_t2155067466 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m322303073(__this, method) ((  void (*) (InternalEnumerator_1_t2155067466 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m322303073_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1989824777_gshared (InternalEnumerator_1_t2155067466 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1989824777(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2155067466 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1989824777_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2023474086_gshared (InternalEnumerator_1_t2155067466 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2023474086(__this, method) ((  void (*) (InternalEnumerator_1_t2155067466 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2023474086_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m534186417_gshared (InternalEnumerator_1_t2155067466 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m534186417(__this, method) ((  bool (*) (InternalEnumerator_1_t2155067466 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m534186417_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1296315204  InternalEnumerator_1_get_Current_m2004837028_gshared (InternalEnumerator_1_t2155067466 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2004837028(__this, method) ((  KeyValuePair_2_t1296315204  (*) (InternalEnumerator_1_t2155067466 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2004837028_gshared)(__this, method)
