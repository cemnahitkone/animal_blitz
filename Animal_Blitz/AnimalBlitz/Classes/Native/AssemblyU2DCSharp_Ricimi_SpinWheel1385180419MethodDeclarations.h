﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.SpinWheel
struct SpinWheel_t1385180419;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.SpinWheel::.ctor()
extern "C"  void SpinWheel__ctor_m1036564491 (SpinWheel_t1385180419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.SpinWheel::Spin()
extern "C"  void SpinWheel_Spin_m459089239 (SpinWheel_t1385180419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Ricimi.SpinWheel::DoSpin()
extern "C"  Il2CppObject * SpinWheel_DoSpin_m623640408 (SpinWheel_t1385180419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
