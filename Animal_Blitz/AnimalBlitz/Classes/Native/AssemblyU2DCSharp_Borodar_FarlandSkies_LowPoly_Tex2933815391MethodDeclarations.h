﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Borodar.FarlandSkies.LowPoly.TexturesDropdown
struct TexturesDropdown_t2933815391;

#include "codegen/il2cpp-codegen.h"

// System.Void Borodar.FarlandSkies.LowPoly.TexturesDropdown::.ctor()
extern "C"  void TexturesDropdown__ctor_m348586730 (TexturesDropdown_t2933815391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.TexturesDropdown::Awake()
extern "C"  void TexturesDropdown_Awake_m3011809715 (TexturesDropdown_t2933815391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Borodar.FarlandSkies.LowPoly.TexturesDropdown::OnValueChanged()
extern "C"  void TexturesDropdown_OnValueChanged_m1489503030 (TexturesDropdown_t2933815391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
