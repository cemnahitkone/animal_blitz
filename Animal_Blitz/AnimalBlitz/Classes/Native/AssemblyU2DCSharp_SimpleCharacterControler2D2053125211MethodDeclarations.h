﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleCharacterControler2D
struct SimpleCharacterControler2D_t2053125211;

#include "codegen/il2cpp-codegen.h"

// System.Void SimpleCharacterControler2D::.ctor()
extern "C"  void SimpleCharacterControler2D__ctor_m2437712998 (SimpleCharacterControler2D_t2053125211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleCharacterControler2D::Awake()
extern "C"  void SimpleCharacterControler2D_Awake_m3067464971 (SimpleCharacterControler2D_t2053125211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleCharacterControler2D::Update()
extern "C"  void SimpleCharacterControler2D_Update_m2157894855 (SimpleCharacterControler2D_t2053125211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
