﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<HighlightingSystem.HighlighterRenderer/Data>
struct DefaultComparer_t2207514785;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H3718244552.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<HighlightingSystem.HighlighterRenderer/Data>::.ctor()
extern "C"  void DefaultComparer__ctor_m1422005246_gshared (DefaultComparer_t2207514785 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1422005246(__this, method) ((  void (*) (DefaultComparer_t2207514785 *, const MethodInfo*))DefaultComparer__ctor_m1422005246_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<HighlightingSystem.HighlighterRenderer/Data>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m4292518235_gshared (DefaultComparer_t2207514785 * __this, Data_t3718244552  ___x0, Data_t3718244552  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m4292518235(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2207514785 *, Data_t3718244552 , Data_t3718244552 , const MethodInfo*))DefaultComparer_Compare_m4292518235_gshared)(__this, ___x0, ___y1, method)
