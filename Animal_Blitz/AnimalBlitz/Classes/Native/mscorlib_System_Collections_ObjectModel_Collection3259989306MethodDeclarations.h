﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>
struct Collection_1_t3259989306;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// HighlightingSystem.HighlighterRenderer/Data[]
struct DataU5BU5D_t4234851097;
// System.Collections.Generic.IEnumerator`1<HighlightingSystem.HighlighterRenderer/Data>
struct IEnumerator_1_t1193768379;
// System.Collections.Generic.IList`1<HighlightingSystem.HighlighterRenderer/Data>
struct IList_1_t4259185153;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H3718244552.h"

// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::.ctor()
extern "C"  void Collection_1__ctor_m3596161687_gshared (Collection_1_t3259989306 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3596161687(__this, method) ((  void (*) (Collection_1_t3259989306 *, const MethodInfo*))Collection_1__ctor_m3596161687_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1663821388_gshared (Collection_1_t3259989306 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1663821388(__this, method) ((  bool (*) (Collection_1_t3259989306 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1663821388_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1255609075_gshared (Collection_1_t3259989306 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1255609075(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3259989306 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1255609075_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m562433528_gshared (Collection_1_t3259989306 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m562433528(__this, method) ((  Il2CppObject * (*) (Collection_1_t3259989306 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m562433528_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2064793979_gshared (Collection_1_t3259989306 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2064793979(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3259989306 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2064793979_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m670130091_gshared (Collection_1_t3259989306 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m670130091(__this, ___value0, method) ((  bool (*) (Collection_1_t3259989306 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m670130091_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m829887309_gshared (Collection_1_t3259989306 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m829887309(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3259989306 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m829887309_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1341432498_gshared (Collection_1_t3259989306 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1341432498(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3259989306 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1341432498_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3414481028_gshared (Collection_1_t3259989306 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3414481028(__this, ___value0, method) ((  void (*) (Collection_1_t3259989306 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3414481028_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2356929387_gshared (Collection_1_t3259989306 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2356929387(__this, method) ((  bool (*) (Collection_1_t3259989306 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2356929387_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1265106115_gshared (Collection_1_t3259989306 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1265106115(__this, method) ((  Il2CppObject * (*) (Collection_1_t3259989306 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1265106115_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m748278774_gshared (Collection_1_t3259989306 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m748278774(__this, method) ((  bool (*) (Collection_1_t3259989306 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m748278774_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m140668999_gshared (Collection_1_t3259989306 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m140668999(__this, method) ((  bool (*) (Collection_1_t3259989306 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m140668999_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3085297770_gshared (Collection_1_t3259989306 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3085297770(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3259989306 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3085297770_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m491782533_gshared (Collection_1_t3259989306 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m491782533(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3259989306 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m491782533_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::Add(T)
extern "C"  void Collection_1_Add_m2751680774_gshared (Collection_1_t3259989306 * __this, Data_t3718244552  ___item0, const MethodInfo* method);
#define Collection_1_Add_m2751680774(__this, ___item0, method) ((  void (*) (Collection_1_t3259989306 *, Data_t3718244552 , const MethodInfo*))Collection_1_Add_m2751680774_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::Clear()
extern "C"  void Collection_1_Clear_m3322231418_gshared (Collection_1_t3259989306 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3322231418(__this, method) ((  void (*) (Collection_1_t3259989306 *, const MethodInfo*))Collection_1_Clear_m3322231418_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1054711484_gshared (Collection_1_t3259989306 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1054711484(__this, method) ((  void (*) (Collection_1_t3259989306 *, const MethodInfo*))Collection_1_ClearItems_m1054711484_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::Contains(T)
extern "C"  bool Collection_1_Contains_m3771631520_gshared (Collection_1_t3259989306 * __this, Data_t3718244552  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3771631520(__this, ___item0, method) ((  bool (*) (Collection_1_t3259989306 *, Data_t3718244552 , const MethodInfo*))Collection_1_Contains_m3771631520_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2829992450_gshared (Collection_1_t3259989306 * __this, DataU5BU5D_t4234851097* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2829992450(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3259989306 *, DataU5BU5D_t4234851097*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2829992450_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1928446323_gshared (Collection_1_t3259989306 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1928446323(__this, method) ((  Il2CppObject* (*) (Collection_1_t3259989306 *, const MethodInfo*))Collection_1_GetEnumerator_m1928446323_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3930095836_gshared (Collection_1_t3259989306 * __this, Data_t3718244552  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3930095836(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3259989306 *, Data_t3718244552 , const MethodInfo*))Collection_1_IndexOf_m3930095836_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3694330657_gshared (Collection_1_t3259989306 * __this, int32_t ___index0, Data_t3718244552  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3694330657(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3259989306 *, int32_t, Data_t3718244552 , const MethodInfo*))Collection_1_Insert_m3694330657_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2305994732_gshared (Collection_1_t3259989306 * __this, int32_t ___index0, Data_t3718244552  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2305994732(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3259989306 *, int32_t, Data_t3718244552 , const MethodInfo*))Collection_1_InsertItem_m2305994732_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::Remove(T)
extern "C"  bool Collection_1_Remove_m1367595249_gshared (Collection_1_t3259989306 * __this, Data_t3718244552  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1367595249(__this, ___item0, method) ((  bool (*) (Collection_1_t3259989306 *, Data_t3718244552 , const MethodInfo*))Collection_1_Remove_m1367595249_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3484633333_gshared (Collection_1_t3259989306 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3484633333(__this, ___index0, method) ((  void (*) (Collection_1_t3259989306 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3484633333_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1096167345_gshared (Collection_1_t3259989306 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1096167345(__this, ___index0, method) ((  void (*) (Collection_1_t3259989306 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1096167345_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m2282933515_gshared (Collection_1_t3259989306 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m2282933515(__this, method) ((  int32_t (*) (Collection_1_t3259989306 *, const MethodInfo*))Collection_1_get_Count_m2282933515_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::get_Item(System.Int32)
extern "C"  Data_t3718244552  Collection_1_get_Item_m540090737_gshared (Collection_1_t3259989306 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m540090737(__this, ___index0, method) ((  Data_t3718244552  (*) (Collection_1_t3259989306 *, int32_t, const MethodInfo*))Collection_1_get_Item_m540090737_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m200702152_gshared (Collection_1_t3259989306 * __this, int32_t ___index0, Data_t3718244552  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m200702152(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3259989306 *, int32_t, Data_t3718244552 , const MethodInfo*))Collection_1_set_Item_m200702152_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1148161829_gshared (Collection_1_t3259989306 * __this, int32_t ___index0, Data_t3718244552  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1148161829(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3259989306 *, int32_t, Data_t3718244552 , const MethodInfo*))Collection_1_SetItem_m1148161829_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m632074438_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m632074438(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m632074438_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::ConvertItem(System.Object)
extern "C"  Data_t3718244552  Collection_1_ConvertItem_m1196034004_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1196034004(__this /* static, unused */, ___item0, method) ((  Data_t3718244552  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1196034004_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m881724650_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m881724650(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m881724650_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3915922804_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3915922804(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3915922804_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HighlightingSystem.HighlighterRenderer/Data>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m471180017_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m471180017(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m471180017_gshared)(__this /* static, unused */, ___list0, method)
