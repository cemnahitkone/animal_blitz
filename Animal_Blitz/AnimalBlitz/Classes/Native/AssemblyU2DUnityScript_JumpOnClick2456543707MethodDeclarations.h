﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JumpOnClick
struct JumpOnClick_t2456543707;

#include "codegen/il2cpp-codegen.h"

// System.Void JumpOnClick::.ctor()
extern "C"  void JumpOnClick__ctor_m1234540515 (JumpOnClick_t2456543707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpOnClick::.cctor()
extern "C"  void JumpOnClick__cctor_m2206386944 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpOnClick::Start()
extern "C"  void JumpOnClick_Start_m1567940455 (JumpOnClick_t2456543707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpOnClick::Update()
extern "C"  void JumpOnClick_Update_m453552980 (JumpOnClick_t2456543707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpOnClick::Main()
extern "C"  void JumpOnClick_Main_m4282046944 (JumpOnClick_t2456543707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
