﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/LightsModule
struct LightsModule_t3164805776;
struct LightsModule_t3164805776_marshaled_pinvoke;
struct LightsModule_t3164805776_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleSystem_LightsModul3164805776.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"

// System.Void UnityEngine.ParticleSystem/LightsModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void LightsModule__ctor_m2493994446 (LightsModule_t3164805776 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct LightsModule_t3164805776;
struct LightsModule_t3164805776_marshaled_pinvoke;

extern "C" void LightsModule_t3164805776_marshal_pinvoke(const LightsModule_t3164805776& unmarshaled, LightsModule_t3164805776_marshaled_pinvoke& marshaled);
extern "C" void LightsModule_t3164805776_marshal_pinvoke_back(const LightsModule_t3164805776_marshaled_pinvoke& marshaled, LightsModule_t3164805776& unmarshaled);
extern "C" void LightsModule_t3164805776_marshal_pinvoke_cleanup(LightsModule_t3164805776_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct LightsModule_t3164805776;
struct LightsModule_t3164805776_marshaled_com;

extern "C" void LightsModule_t3164805776_marshal_com(const LightsModule_t3164805776& unmarshaled, LightsModule_t3164805776_marshaled_com& marshaled);
extern "C" void LightsModule_t3164805776_marshal_com_back(const LightsModule_t3164805776_marshaled_com& marshaled, LightsModule_t3164805776& unmarshaled);
extern "C" void LightsModule_t3164805776_marshal_com_cleanup(LightsModule_t3164805776_marshaled_com& marshaled);
