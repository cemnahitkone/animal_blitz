﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Remoting_ChannelData1489610737.h"
#include "mscorlib_System_Runtime_Remoting_ProviderData2518653487.h"
#include "mscorlib_System_Runtime_Remoting_FormatterData12176916.h"
#include "mscorlib_System_Runtime_Remoting_RemotingException109604560.h"
#include "mscorlib_System_Runtime_Remoting_RemotingServices2399536837.h"
#include "mscorlib_System_Runtime_Remoting_ServerIdentity1656058977.h"
#include "mscorlib_System_Runtime_Remoting_ClientActivatedId1467784146.h"
#include "mscorlib_System_Runtime_Remoting_SingletonIdentity164722255.h"
#include "mscorlib_System_Runtime_Remoting_SingleCallIdentit3377680076.h"
#include "mscorlib_System_Runtime_Remoting_SoapServices3397513225.h"
#include "mscorlib_System_Runtime_Remoting_SoapServices_TypeIn59877052.h"
#include "mscorlib_System_Runtime_Remoting_TypeEntry3321373506.h"
#include "mscorlib_System_Runtime_Remoting_TypeInfo942537562.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMo2630225581.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownClientTy3314744170.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownServiceT1712728956.h"
#include "mscorlib_System_Runtime_Remoting_Activation_Activa1532663650.h"
#include "mscorlib_System_Runtime_Remoting_Activation_AppDoma834876328.h"
#include "mscorlib_System_Runtime_Remoting_Activation_Constr2284932402.h"
#include "mscorlib_System_Runtime_Remoting_Activation_Contex1784331636.h"
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteA213750447.h"
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAtt1544437301.h"
#include "mscorlib_System_Runtime_Remoting_ChannelInfo709892715.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelS2007814595.h"
#include "mscorlib_System_Runtime_Remoting_Channels_SinkProv2645445792.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppD816071813.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossApp2471623380.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossApp2368859578.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_Context502196753.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_DynamicP2282532998.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_DynamicP1839195831.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextC3978189709.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAt197102333.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossCon2302426108.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_Synchron3073724998.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_Synchron3779986825.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_Synchroni462987365.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_Lease3663008028.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_Lease_Ren194360041.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseMan1025868639.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseSin3007073869.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseState83447469.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_Lifetime2939669377.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfo3252846202.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfo688271106.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncRe2232356043.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ClientC3236389774.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Constru1254994451.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Constru2993650247.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTe3043186997.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Header2756440555.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalC725724420.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_CallCon2648008188.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodC2461541281.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodC1516131009.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodD1742974787.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDi492828146.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodRe981009581.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMeth771543475.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_CallTyp2486906258.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_OneWayA2539461443.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Remotin2821375126.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Remotin3248446683.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefS3912784830.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnM3411975905.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ServerC1054294306.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ServerO4261369100.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_StackBu1613771438.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapAttr1982224933.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapFiel3073759685.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapMeth2381910676.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize600 = { sizeof (ChannelData_t1489610737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable600[7] = 
{
	ChannelData_t1489610737::get_offset_of_Ref_0(),
	ChannelData_t1489610737::get_offset_of_Type_1(),
	ChannelData_t1489610737::get_offset_of_Id_2(),
	ChannelData_t1489610737::get_offset_of_DelayLoadAsClientChannel_3(),
	ChannelData_t1489610737::get_offset_of__serverProviders_4(),
	ChannelData_t1489610737::get_offset_of__clientProviders_5(),
	ChannelData_t1489610737::get_offset_of__customProperties_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize601 = { sizeof (ProviderData_t2518653487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable601[5] = 
{
	ProviderData_t2518653487::get_offset_of_Ref_0(),
	ProviderData_t2518653487::get_offset_of_Type_1(),
	ProviderData_t2518653487::get_offset_of_Id_2(),
	ProviderData_t2518653487::get_offset_of_CustomProperties_3(),
	ProviderData_t2518653487::get_offset_of_CustomData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize602 = { sizeof (FormatterData_t12176916), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize603 = { sizeof (RemotingException_t109604560), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize604 = { sizeof (RemotingServices_t2399536837), -1, sizeof(RemotingServices_t2399536837_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable604[8] = 
{
	RemotingServices_t2399536837_StaticFields::get_offset_of_uri_hash_0(),
	RemotingServices_t2399536837_StaticFields::get_offset_of__serializationFormatter_1(),
	RemotingServices_t2399536837_StaticFields::get_offset_of__deserializationFormatter_2(),
	RemotingServices_t2399536837_StaticFields::get_offset_of_app_id_3(),
	RemotingServices_t2399536837_StaticFields::get_offset_of_next_id_4(),
	RemotingServices_t2399536837_StaticFields::get_offset_of_methodBindings_5(),
	RemotingServices_t2399536837_StaticFields::get_offset_of_FieldSetterMethod_6(),
	RemotingServices_t2399536837_StaticFields::get_offset_of_FieldGetterMethod_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize605 = { sizeof (ServerIdentity_t1656058977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable605[5] = 
{
	ServerIdentity_t1656058977::get_offset_of__objectType_7(),
	ServerIdentity_t1656058977::get_offset_of__serverObject_8(),
	ServerIdentity_t1656058977::get_offset_of__serverSink_9(),
	ServerIdentity_t1656058977::get_offset_of__context_10(),
	ServerIdentity_t1656058977::get_offset_of__lease_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize606 = { sizeof (ClientActivatedIdentity_t1467784146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize607 = { sizeof (SingletonIdentity_t164722255), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize608 = { sizeof (SingleCallIdentity_t3377680076), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize609 = { sizeof (SoapServices_t3397513225), -1, sizeof(SoapServices_t3397513225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable609[5] = 
{
	SoapServices_t3397513225_StaticFields::get_offset_of__xmlTypes_0(),
	SoapServices_t3397513225_StaticFields::get_offset_of__xmlElements_1(),
	SoapServices_t3397513225_StaticFields::get_offset_of__soapActions_2(),
	SoapServices_t3397513225_StaticFields::get_offset_of__soapActionsMethods_3(),
	SoapServices_t3397513225_StaticFields::get_offset_of__typeInfos_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize610 = { sizeof (TypeInfo_t59877052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable610[2] = 
{
	TypeInfo_t59877052::get_offset_of_Attributes_0(),
	TypeInfo_t59877052::get_offset_of_Elements_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize611 = { sizeof (TypeEntry_t3321373506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable611[2] = 
{
	TypeEntry_t3321373506::get_offset_of_assembly_name_0(),
	TypeEntry_t3321373506::get_offset_of_type_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize612 = { sizeof (TypeInfo_t942537562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable612[3] = 
{
	TypeInfo_t942537562::get_offset_of_serverType_0(),
	TypeInfo_t942537562::get_offset_of_serverHierarchy_1(),
	TypeInfo_t942537562::get_offset_of_interfacesImplemented_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize613 = { sizeof (WellKnownObjectMode_t2630225581)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable613[3] = 
{
	WellKnownObjectMode_t2630225581::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize614 = { sizeof (WellKnownClientTypeEntry_t3314744170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable614[3] = 
{
	WellKnownClientTypeEntry_t3314744170::get_offset_of_obj_type_2(),
	WellKnownClientTypeEntry_t3314744170::get_offset_of_obj_url_3(),
	WellKnownClientTypeEntry_t3314744170::get_offset_of_app_url_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize615 = { sizeof (WellKnownServiceTypeEntry_t1712728956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable615[3] = 
{
	WellKnownServiceTypeEntry_t1712728956::get_offset_of_obj_type_2(),
	WellKnownServiceTypeEntry_t1712728956::get_offset_of_obj_uri_3(),
	WellKnownServiceTypeEntry_t1712728956::get_offset_of_obj_mode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize616 = { sizeof (ActivationServices_t1532663650), -1, sizeof(ActivationServices_t1532663650_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable616[1] = 
{
	ActivationServices_t1532663650_StaticFields::get_offset_of__constructionActivator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize617 = { sizeof (AppDomainLevelActivator_t834876328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable617[2] = 
{
	AppDomainLevelActivator_t834876328::get_offset_of__activationUrl_0(),
	AppDomainLevelActivator_t834876328::get_offset_of__next_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize618 = { sizeof (ConstructionLevelActivator_t2284932402), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize619 = { sizeof (ContextLevelActivator_t1784331636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable619[1] = 
{
	ContextLevelActivator_t1784331636::get_offset_of_m_NextActivator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize620 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize621 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize622 = { sizeof (RemoteActivator_t213750447), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize623 = { sizeof (UrlAttribute_t1544437301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable623[1] = 
{
	UrlAttribute_t1544437301::get_offset_of_url_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize624 = { sizeof (ChannelInfo_t709892715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable624[1] = 
{
	ChannelInfo_t709892715::get_offset_of_channelData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize625 = { sizeof (ChannelServices_t2007814595), -1, sizeof(ChannelServices_t2007814595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable625[5] = 
{
	ChannelServices_t2007814595_StaticFields::get_offset_of_registeredChannels_0(),
	ChannelServices_t2007814595_StaticFields::get_offset_of_delayedClientChannels_1(),
	ChannelServices_t2007814595_StaticFields::get_offset_of__crossContextSink_2(),
	ChannelServices_t2007814595_StaticFields::get_offset_of_CrossContextUrl_3(),
	ChannelServices_t2007814595_StaticFields::get_offset_of_oldStartModeTypes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize626 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize627 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize628 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize629 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize630 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize631 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize632 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize633 = { sizeof (SinkProviderData_t2645445792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable633[3] = 
{
	SinkProviderData_t2645445792::get_offset_of_sinkName_0(),
	SinkProviderData_t2645445792::get_offset_of_children_1(),
	SinkProviderData_t2645445792::get_offset_of_properties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize634 = { sizeof (CrossAppDomainData_t816071813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable634[3] = 
{
	CrossAppDomainData_t816071813::get_offset_of__ContextID_0(),
	CrossAppDomainData_t816071813::get_offset_of__DomainID_1(),
	CrossAppDomainData_t816071813::get_offset_of__processGuid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize635 = { sizeof (CrossAppDomainChannel_t2471623380), -1, sizeof(CrossAppDomainChannel_t2471623380_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable635[1] = 
{
	CrossAppDomainChannel_t2471623380_StaticFields::get_offset_of_s_lock_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize636 = { sizeof (CrossAppDomainSink_t2368859578), -1, sizeof(CrossAppDomainSink_t2368859578_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable636[3] = 
{
	CrossAppDomainSink_t2368859578_StaticFields::get_offset_of_s_sinks_0(),
	CrossAppDomainSink_t2368859578_StaticFields::get_offset_of_processMessageMethod_1(),
	CrossAppDomainSink_t2368859578::get_offset_of__domainID_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize637 = { sizeof (Context_t502196753), -1, sizeof(Context_t502196753_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable637[14] = 
{
	Context_t502196753::get_offset_of_domain_id_0(),
	Context_t502196753::get_offset_of_context_id_1(),
	Context_t502196753::get_offset_of_static_data_2(),
	Context_t502196753_StaticFields::get_offset_of_default_server_context_sink_3(),
	Context_t502196753::get_offset_of_server_context_sink_chain_4(),
	Context_t502196753::get_offset_of_client_context_sink_chain_5(),
	Context_t502196753::get_offset_of_datastore_6(),
	Context_t502196753::get_offset_of_context_properties_7(),
	Context_t502196753::get_offset_of_frozen_8(),
	Context_t502196753_StaticFields::get_offset_of_global_count_9(),
	Context_t502196753_StaticFields::get_offset_of_namedSlots_10(),
	Context_t502196753_StaticFields::get_offset_of_global_dynamic_properties_11(),
	Context_t502196753::get_offset_of_context_dynamic_properties_12(),
	Context_t502196753::get_offset_of_callback_object_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize638 = { sizeof (DynamicPropertyCollection_t2282532998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable638[1] = 
{
	DynamicPropertyCollection_t2282532998::get_offset_of__properties_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize639 = { sizeof (DynamicPropertyReg_t1839195831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable639[2] = 
{
	DynamicPropertyReg_t1839195831::get_offset_of_Property_0(),
	DynamicPropertyReg_t1839195831::get_offset_of_Sink_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize640 = { sizeof (ContextCallbackObject_t3978189709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize641 = { sizeof (ContextAttribute_t197102333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable641[1] = 
{
	ContextAttribute_t197102333::get_offset_of_AttributeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize642 = { sizeof (CrossContextChannel_t2302426108), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize643 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize644 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize645 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize646 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize647 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize648 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize649 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize650 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize651 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize652 = { sizeof (SynchronizationAttribute_t3073724998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable652[5] = 
{
	SynchronizationAttribute_t3073724998::get_offset_of__bReEntrant_1(),
	SynchronizationAttribute_t3073724998::get_offset_of__flavor_2(),
	SynchronizationAttribute_t3073724998::get_offset_of__lockCount_3(),
	SynchronizationAttribute_t3073724998::get_offset_of__mutex_4(),
	SynchronizationAttribute_t3073724998::get_offset_of__ownerThread_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize653 = { sizeof (SynchronizedClientContextSink_t3779986825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable653[2] = 
{
	SynchronizedClientContextSink_t3779986825::get_offset_of__next_0(),
	SynchronizedClientContextSink_t3779986825::get_offset_of__att_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize654 = { sizeof (SynchronizedServerContextSink_t462987365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable654[2] = 
{
	SynchronizedServerContextSink_t462987365::get_offset_of__next_0(),
	SynchronizedServerContextSink_t462987365::get_offset_of__att_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize655 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize656 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize657 = { sizeof (Lease_t3663008028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable657[8] = 
{
	Lease_t3663008028::get_offset_of__leaseExpireTime_1(),
	Lease_t3663008028::get_offset_of__currentState_2(),
	Lease_t3663008028::get_offset_of__initialLeaseTime_3(),
	Lease_t3663008028::get_offset_of__renewOnCallTime_4(),
	Lease_t3663008028::get_offset_of__sponsorshipTimeout_5(),
	Lease_t3663008028::get_offset_of__sponsors_6(),
	Lease_t3663008028::get_offset_of__renewingSponsors_7(),
	Lease_t3663008028::get_offset_of__renewalDelegate_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize658 = { sizeof (RenewalDelegate_t194360041), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize659 = { sizeof (LeaseManager_t1025868639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable659[2] = 
{
	LeaseManager_t1025868639::get_offset_of__objects_0(),
	LeaseManager_t1025868639::get_offset_of__timer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize660 = { sizeof (LeaseSink_t3007073869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable660[1] = 
{
	LeaseSink_t3007073869::get_offset_of__nextSink_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize661 = { sizeof (LeaseState_t83447469)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable661[6] = 
{
	LeaseState_t83447469::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize662 = { sizeof (LifetimeServices_t2939669377), -1, sizeof(LifetimeServices_t2939669377_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable662[5] = 
{
	LifetimeServices_t2939669377_StaticFields::get_offset_of__leaseManagerPollTime_0(),
	LifetimeServices_t2939669377_StaticFields::get_offset_of__leaseTime_1(),
	LifetimeServices_t2939669377_StaticFields::get_offset_of__renewOnCallTime_2(),
	LifetimeServices_t2939669377_StaticFields::get_offset_of__sponsorshipTimeout_3(),
	LifetimeServices_t2939669377_StaticFields::get_offset_of__leaseManager_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize663 = { sizeof (ArgInfoType_t3252846202)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable663[3] = 
{
	ArgInfoType_t3252846202::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize664 = { sizeof (ArgInfo_t688271106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable664[3] = 
{
	ArgInfo_t688271106::get_offset_of__paramMap_0(),
	ArgInfo_t688271106::get_offset_of__inoutArgCount_1(),
	ArgInfo_t688271106::get_offset_of__method_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize665 = { sizeof (AsyncResult_t2232356043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable665[15] = 
{
	AsyncResult_t2232356043::get_offset_of_async_state_0(),
	AsyncResult_t2232356043::get_offset_of_handle_1(),
	AsyncResult_t2232356043::get_offset_of_async_delegate_2(),
	AsyncResult_t2232356043::get_offset_of_data_3(),
	AsyncResult_t2232356043::get_offset_of_object_data_4(),
	AsyncResult_t2232356043::get_offset_of_sync_completed_5(),
	AsyncResult_t2232356043::get_offset_of_completed_6(),
	AsyncResult_t2232356043::get_offset_of_endinvoke_called_7(),
	AsyncResult_t2232356043::get_offset_of_async_callback_8(),
	AsyncResult_t2232356043::get_offset_of_current_9(),
	AsyncResult_t2232356043::get_offset_of_original_10(),
	AsyncResult_t2232356043::get_offset_of_gchandle_11(),
	AsyncResult_t2232356043::get_offset_of_call_message_12(),
	AsyncResult_t2232356043::get_offset_of_message_ctrl_13(),
	AsyncResult_t2232356043::get_offset_of_reply_message_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize666 = { sizeof (ClientContextTerminatorSink_t3236389774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable666[1] = 
{
	ClientContextTerminatorSink_t3236389774::get_offset_of__context_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize667 = { sizeof (ConstructionCall_t1254994451), -1, sizeof(ConstructionCall_t1254994451_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable667[7] = 
{
	ConstructionCall_t1254994451::get_offset_of__activator_11(),
	ConstructionCall_t1254994451::get_offset_of__activationAttributes_12(),
	ConstructionCall_t1254994451::get_offset_of__contextProperties_13(),
	ConstructionCall_t1254994451::get_offset_of__activationType_14(),
	ConstructionCall_t1254994451::get_offset_of__activationTypeName_15(),
	ConstructionCall_t1254994451::get_offset_of__isContextOk_16(),
	ConstructionCall_t1254994451_StaticFields::get_offset_of_U3CU3Ef__switchU24map25_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize668 = { sizeof (ConstructionCallDictionary_t2993650247), -1, sizeof(ConstructionCallDictionary_t2993650247_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable668[3] = 
{
	ConstructionCallDictionary_t2993650247_StaticFields::get_offset_of_InternalKeys_6(),
	ConstructionCallDictionary_t2993650247_StaticFields::get_offset_of_U3CU3Ef__switchU24map28_7(),
	ConstructionCallDictionary_t2993650247_StaticFields::get_offset_of_U3CU3Ef__switchU24map29_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize669 = { sizeof (EnvoyTerminatorSink_t3043186997), -1, sizeof(EnvoyTerminatorSink_t3043186997_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable669[1] = 
{
	EnvoyTerminatorSink_t3043186997_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize670 = { sizeof (Header_t2756440555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable670[4] = 
{
	Header_t2756440555::get_offset_of_HeaderNamespace_0(),
	Header_t2756440555::get_offset_of_MustUnderstand_1(),
	Header_t2756440555::get_offset_of_Name_2(),
	Header_t2756440555::get_offset_of_Value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize671 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize672 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize673 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize674 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize675 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize676 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize677 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize678 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize679 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize680 = { sizeof (LogicalCallContext_t725724420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable680[2] = 
{
	LogicalCallContext_t725724420::get_offset_of__data_0(),
	LogicalCallContext_t725724420::get_offset_of__remotingData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize681 = { sizeof (CallContextRemotingData_t2648008188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable681[1] = 
{
	CallContextRemotingData_t2648008188::get_offset_of__logicalCallID_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize682 = { sizeof (MethodCall_t2461541281), -1, sizeof(MethodCall_t2461541281_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable682[11] = 
{
	MethodCall_t2461541281::get_offset_of__uri_0(),
	MethodCall_t2461541281::get_offset_of__typeName_1(),
	MethodCall_t2461541281::get_offset_of__methodName_2(),
	MethodCall_t2461541281::get_offset_of__args_3(),
	MethodCall_t2461541281::get_offset_of__methodSignature_4(),
	MethodCall_t2461541281::get_offset_of__methodBase_5(),
	MethodCall_t2461541281::get_offset_of__callContext_6(),
	MethodCall_t2461541281::get_offset_of__genericArguments_7(),
	MethodCall_t2461541281::get_offset_of_ExternalProperties_8(),
	MethodCall_t2461541281::get_offset_of_InternalProperties_9(),
	MethodCall_t2461541281_StaticFields::get_offset_of_U3CU3Ef__switchU24map24_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize683 = { sizeof (MethodCallDictionary_t1516131009), -1, sizeof(MethodCallDictionary_t1516131009_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable683[1] = 
{
	MethodCallDictionary_t1516131009_StaticFields::get_offset_of_InternalKeys_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize684 = { sizeof (MethodDictionary_t1742974787), -1, sizeof(MethodDictionary_t1742974787_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable684[6] = 
{
	MethodDictionary_t1742974787::get_offset_of__internalProperties_0(),
	MethodDictionary_t1742974787::get_offset_of__message_1(),
	MethodDictionary_t1742974787::get_offset_of__methodKeys_2(),
	MethodDictionary_t1742974787::get_offset_of__ownProperties_3(),
	MethodDictionary_t1742974787_StaticFields::get_offset_of_U3CU3Ef__switchU24map26_4(),
	MethodDictionary_t1742974787_StaticFields::get_offset_of_U3CU3Ef__switchU24map27_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize685 = { sizeof (DictionaryEnumerator_t492828146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable685[3] = 
{
	DictionaryEnumerator_t492828146::get_offset_of__methodDictionary_0(),
	DictionaryEnumerator_t492828146::get_offset_of__hashtableEnum_1(),
	DictionaryEnumerator_t492828146::get_offset_of__posMethod_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize686 = { sizeof (MethodReturnDictionary_t981009581), -1, sizeof(MethodReturnDictionary_t981009581_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable686[2] = 
{
	MethodReturnDictionary_t981009581_StaticFields::get_offset_of_InternalReturnKeys_6(),
	MethodReturnDictionary_t981009581_StaticFields::get_offset_of_InternalExceptionKeys_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize687 = { sizeof (MonoMethodMessage_t771543475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable687[10] = 
{
	MonoMethodMessage_t771543475::get_offset_of_method_0(),
	MonoMethodMessage_t771543475::get_offset_of_args_1(),
	MonoMethodMessage_t771543475::get_offset_of_arg_types_2(),
	MonoMethodMessage_t771543475::get_offset_of_ctx_3(),
	MonoMethodMessage_t771543475::get_offset_of_rval_4(),
	MonoMethodMessage_t771543475::get_offset_of_exc_5(),
	MonoMethodMessage_t771543475::get_offset_of_call_type_6(),
	MonoMethodMessage_t771543475::get_offset_of_uri_7(),
	MonoMethodMessage_t771543475::get_offset_of_properties_8(),
	MonoMethodMessage_t771543475::get_offset_of_methodSignature_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize688 = { sizeof (CallType_t2486906258)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable688[5] = 
{
	CallType_t2486906258::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize689 = { sizeof (OneWayAttribute_t2539461443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize690 = { sizeof (RemotingSurrogateSelector_t2821375126), -1, sizeof(RemotingSurrogateSelector_t2821375126_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable690[4] = 
{
	RemotingSurrogateSelector_t2821375126_StaticFields::get_offset_of_s_cachedTypeObjRef_0(),
	RemotingSurrogateSelector_t2821375126_StaticFields::get_offset_of__objRefSurrogate_1(),
	RemotingSurrogateSelector_t2821375126_StaticFields::get_offset_of__objRemotingSurrogate_2(),
	RemotingSurrogateSelector_t2821375126::get_offset_of__next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize691 = { sizeof (RemotingSurrogate_t3248446683), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize692 = { sizeof (ObjRefSurrogate_t3912784830), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize693 = { sizeof (ReturnMessage_t3411975905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable693[13] = 
{
	ReturnMessage_t3411975905::get_offset_of__outArgs_0(),
	ReturnMessage_t3411975905::get_offset_of__args_1(),
	ReturnMessage_t3411975905::get_offset_of__outArgsCount_2(),
	ReturnMessage_t3411975905::get_offset_of__callCtx_3(),
	ReturnMessage_t3411975905::get_offset_of__returnValue_4(),
	ReturnMessage_t3411975905::get_offset_of__uri_5(),
	ReturnMessage_t3411975905::get_offset_of__exception_6(),
	ReturnMessage_t3411975905::get_offset_of__methodBase_7(),
	ReturnMessage_t3411975905::get_offset_of__methodName_8(),
	ReturnMessage_t3411975905::get_offset_of__methodSignature_9(),
	ReturnMessage_t3411975905::get_offset_of__typeName_10(),
	ReturnMessage_t3411975905::get_offset_of__properties_11(),
	ReturnMessage_t3411975905::get_offset_of__inArgInfo_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize694 = { sizeof (ServerContextTerminatorSink_t1054294306), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize695 = { sizeof (ServerObjectTerminatorSink_t4261369100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable695[1] = 
{
	ServerObjectTerminatorSink_t4261369100::get_offset_of__nextSink_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize696 = { sizeof (StackBuilderSink_t1613771438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable696[2] = 
{
	StackBuilderSink_t1613771438::get_offset_of__target_0(),
	StackBuilderSink_t1613771438::get_offset_of__rp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize697 = { sizeof (SoapAttribute_t1982224933), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable697[3] = 
{
	SoapAttribute_t1982224933::get_offset_of__useAttribute_0(),
	SoapAttribute_t1982224933::get_offset_of_ProtXmlNamespace_1(),
	SoapAttribute_t1982224933::get_offset_of_ReflectInfo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize698 = { sizeof (SoapFieldAttribute_t3073759685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable698[2] = 
{
	SoapFieldAttribute_t3073759685::get_offset_of__elementName_3(),
	SoapFieldAttribute_t3073759685::get_offset_of__isElement_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize699 = { sizeof (SoapMethodAttribute_t2381910676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable699[6] = 
{
	SoapMethodAttribute_t2381910676::get_offset_of__responseElement_3(),
	SoapMethodAttribute_t2381910676::get_offset_of__responseNamespace_4(),
	SoapMethodAttribute_t2381910676::get_offset_of__returnElement_5(),
	SoapMethodAttribute_t2381910676::get_offset_of__soapAction_6(),
	SoapMethodAttribute_t2381910676::get_offset_of__useAttribute_7(),
	SoapMethodAttribute_t2381910676::get_offset_of__namespace_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
