﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.BackgroundMusic
struct BackgroundMusic_t2182474183;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Ricimi_BackgroundMusic_Fade3857387351.h"

// System.Void Ricimi.BackgroundMusic::.ctor()
extern "C"  void BackgroundMusic__ctor_m2055270755 (BackgroundMusic_t2182474183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.BackgroundMusic::Awake()
extern "C"  void BackgroundMusic_Awake_m1426372390 (BackgroundMusic_t2182474183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.BackgroundMusic::FadeIn()
extern "C"  void BackgroundMusic_FadeIn_m2024330092 (BackgroundMusic_t2182474183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.BackgroundMusic::FadeOut()
extern "C"  void BackgroundMusic_FadeOut_m2103303851 (BackgroundMusic_t2182474183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Ricimi.BackgroundMusic::FadeAudio(System.Single,Ricimi.BackgroundMusic/Fade)
extern "C"  Il2CppObject * BackgroundMusic_FadeAudio_m1405195809 (BackgroundMusic_t2182474183 * __this, float ___time0, int32_t ___fadeType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
