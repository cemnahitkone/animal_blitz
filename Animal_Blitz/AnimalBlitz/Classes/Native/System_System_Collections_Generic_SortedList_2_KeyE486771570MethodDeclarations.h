﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2<System.Single,System.Object>
struct SortedList_2_t2155713151;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedList_2_KeyE486771570.h"

// System.Void System.Collections.Generic.SortedList`2/KeyEnumerator<System.Single,System.Object>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C"  void KeyEnumerator__ctor_m3969580785_gshared (KeyEnumerator_t486771570 * __this, SortedList_2_t2155713151 * ___l0, const MethodInfo* method);
#define KeyEnumerator__ctor_m3969580785(__this, ___l0, method) ((  void (*) (KeyEnumerator_t486771570 *, SortedList_2_t2155713151 *, const MethodInfo*))KeyEnumerator__ctor_m3969580785_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.SortedList`2/KeyEnumerator<System.Single,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void KeyEnumerator_System_Collections_IEnumerator_Reset_m1456984081_gshared (KeyEnumerator_t486771570 * __this, const MethodInfo* method);
#define KeyEnumerator_System_Collections_IEnumerator_Reset_m1456984081(__this, method) ((  void (*) (KeyEnumerator_t486771570 *, const MethodInfo*))KeyEnumerator_System_Collections_IEnumerator_Reset_m1456984081_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/KeyEnumerator<System.Single,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * KeyEnumerator_System_Collections_IEnumerator_get_Current_m2446053701_gshared (KeyEnumerator_t486771570 * __this, const MethodInfo* method);
#define KeyEnumerator_System_Collections_IEnumerator_get_Current_m2446053701(__this, method) ((  Il2CppObject * (*) (KeyEnumerator_t486771570 *, const MethodInfo*))KeyEnumerator_System_Collections_IEnumerator_get_Current_m2446053701_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/KeyEnumerator<System.Single,System.Object>::Dispose()
extern "C"  void KeyEnumerator_Dispose_m392491370_gshared (KeyEnumerator_t486771570 * __this, const MethodInfo* method);
#define KeyEnumerator_Dispose_m392491370(__this, method) ((  void (*) (KeyEnumerator_t486771570 *, const MethodInfo*))KeyEnumerator_Dispose_m392491370_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/KeyEnumerator<System.Single,System.Object>::MoveNext()
extern "C"  bool KeyEnumerator_MoveNext_m2761269221_gshared (KeyEnumerator_t486771570 * __this, const MethodInfo* method);
#define KeyEnumerator_MoveNext_m2761269221(__this, method) ((  bool (*) (KeyEnumerator_t486771570 *, const MethodInfo*))KeyEnumerator_MoveNext_m2761269221_gshared)(__this, method)
// TKey System.Collections.Generic.SortedList`2/KeyEnumerator<System.Single,System.Object>::get_Current()
extern "C"  float KeyEnumerator_get_Current_m2099953675_gshared (KeyEnumerator_t486771570 * __this, const MethodInfo* method);
#define KeyEnumerator_get_Current_m2099953675(__this, method) ((  float (*) (KeyEnumerator_t486771570 *, const MethodInfo*))KeyEnumerator_get_Current_m2099953675_gshared)(__this, method)
