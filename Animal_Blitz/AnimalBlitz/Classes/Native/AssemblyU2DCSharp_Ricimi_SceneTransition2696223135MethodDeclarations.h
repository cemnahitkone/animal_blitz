﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ricimi.SceneTransition
struct SceneTransition_t2696223135;

#include "codegen/il2cpp-codegen.h"

// System.Void Ricimi.SceneTransition::.ctor()
extern "C"  void SceneTransition__ctor_m3733795521 (SceneTransition_t2696223135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ricimi.SceneTransition::PerformTransition()
extern "C"  void SceneTransition_PerformTransition_m1802362347 (SceneTransition_t2696223135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
