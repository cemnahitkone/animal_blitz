﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Borodar_FarlandSkies_Core_DotPar3767082706.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Borodar.FarlandSkies.LowPoly.DotParams.StarsParam
struct  StarsParam_t907828490  : public DotParam_t3767082706
{
public:
	// UnityEngine.Color Borodar.FarlandSkies.LowPoly.DotParams.StarsParam::TintColor
	Color_t2020392075  ___TintColor_1;

public:
	inline static int32_t get_offset_of_TintColor_1() { return static_cast<int32_t>(offsetof(StarsParam_t907828490, ___TintColor_1)); }
	inline Color_t2020392075  get_TintColor_1() const { return ___TintColor_1; }
	inline Color_t2020392075 * get_address_of_TintColor_1() { return &___TintColor_1; }
	inline void set_TintColor_1(Color_t2020392075  value)
	{
		___TintColor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
